<%@page import="com.addcel.common.HttpRequest"%>
<%-- 
    Document   : comercio_con
    Created on : 23-sep-2012, 20:12:17
    Author     : Salvador
--%>
<%@page import="com.addcel.tools.encryption.SHA1"%>
<%@page import="com.addcel.procom.TransactionControl"%>
<%@page import="com.addcel.common.MCUser"%>
<%@page import="java.io.PrintWriter"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
    String varMerchant ="7412827";
    String varTotal = "0";
    String varOrderId = "";
    String varStore ="1234";
    String varTerm = "001";
    String varCurrency = "484";
    
    String varAddress = "PROSA";    
    String digest = "";
    PrintWriter out2 = response.getWriter();
    
    //MC
    String user = "";
    String monto = "";
    
    
    if(request.getParameter("user")!=null)
    {
        user = request.getParameter("user").toString();
    }
    if(request.getParameter("merchantId")!=null)
    {
        varMerchant = request.getParameter("merchantId").toString();
    }
    if(request.getParameter("monto")!=null)
    {
        monto = request.getParameter("monto").toString();
        if(monto.equals("0"))
            monto ="0.00";
        System.out.println("Monto:"+monto);
        
        String[] numero = monto.split("\\.");
        System.out.println("Lenght:"+numero.length);

        if(numero[1].length()>2)
            numero[1] = numero[1].substring(0,2);
        
        
        varTotal = numero[0] + numero[1];
        //varTotal = monto.replaceAll(".", "");
    }
    if(request.getParameter("referencia")!=null)
    {
        varOrderId = request.getParameter("referencia").toString();
    }

    //varOrderId = TransactionControl.createTransaction( varTotal, varMerchant)+"";
    MCUser mcUser = TransactionControl.getData(user);
    
    SHA1 sha = new SHA1();
    digest = sha.encrypt(varMerchant + varStore + varTerm + varTotal + varCurrency + varOrderId);
    
    
    //HttpRequest
    HttpRequest req = new HttpRequest();
    String variables="";
    variables += "total="+varTotal+"&";
    variables += "currency="+varCurrency+"&";
    variables += "address="+varAddress+"&";
    variables += "order_id=&";
    variables += "merchant="+varMerchant+"&";
    variables += "store="+varStore+"&";
    variables += "term="+varTerm+"&";
    variables += "digest="+digest+"&";
    variables += "return_target=&";
    variables += "urlBack=https://www.mobilecard.mx:8080/ProCom/comercio_con.jsp";
 
    
    String res = req.httpPostResponse("https://www.procom.prosa.com.mx/eMerch2/7412827_interjet.jsp", variables);
    
    res = res.replaceAll("./validaciones/valida.do", "https://www.procom.prosa.com.mx/eMerch2/validaciones/valida.do");
    res = res.replaceAll("Portal 3D Secure para Pago de Interjet", "Portal 3D Secure para Pago de Interjet<br/><br/><h3>Favor de revisar sus datos</h3>");
    //res+= mcUser.getUserNombre().toUpperCase()+ " " + mcUser.getUserApellido().toUpperCase()+"<br><br>\n";
    
    res+="<script language=\"JavaScript\"> \n";
    res+="    var nombre = document.getElementsByName(\"cc_name\")\n";
    res+="    nombre[0].value=\""+ mcUser.getUserNombre().toUpperCase().trim()+ " " + mcUser.getUserApellido().toUpperCase().trim() + "\";";
    //res+="    name[0].value='"+ mcUser.getUserNombre().toUpperCase()+ " " + mcUser.getUserApellido().toUpperCase() + "'";
    res+="    \n";
    res+="    var card = document.getElementsByName(\"cc_number\")\n";
    res+="    card[0].value=\"" + mcUser.getUserTdc() + "\";";
    res+="    \n";
    res+="    var cardTypeMC = '" + mcUser.getUserTipoTarjeta().toLowerCase() + "'";
    res+="    \n";    
    res+="    if(cardTypeMC=='visa') cardTypeMC = 'Visa';\n";    
    res+="    if(cardTypeMC=='master card') cardTypeMC = 'Mastercard';\n";    
    res+="    \n";    
    res+="    var cardType = document.getElementsByName(\"cc_type\");\n";
    res+="    cardType[0].value=cardTypeMC;\n";
    res+="    \n";
    res+="    var cardExp = '"+mcUser.getUserVig()+"';\n";    
    res+="    var mes = cardExp.substring(0,2);\n";
    res+="    var anio = '20' + cardExp.substring(3,5);\n";    
    res+="    \n";    
    res+="    \n";    
    res+="    \n";    
    res+="    var cardExpMonth = document.getElementsByName(\"_cc_expmonth\");\n";    
    res+="    cardExpMonth[0].value = mes;\n";    
    res+="    \n";    
    res+="    var cardExpYear = document.getElementsByName(\"_cc_expyear\");\n";    
    res+="    cardExpYear[0].value = anio;\n";    
    res+="    \n";    
    res+="    var index=0;\n";    
    res+="    for(i=0;i<cardExpYear[0].length;i++)\n";    
    res+="    {\n";    
    res+="      if(cardExpYear[0].options[i].text == anio)\n";    
    res+="      {\n";    
    res+="          index=i;\n";    
    res+="          break;\n";    
    res+="      }\n";    
    res+="    }\n";    
    res+="    cardExpYear[0].selectedIndex=index;\n";    
    res+="    \n";    
    res+="</script>";
    out2.write(res);
    
    
%>
</html>
