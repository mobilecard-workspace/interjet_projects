<%@page import="java.io.PrintWriter"%>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">-->
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html version="-//W3C//DTD HTML 4.01 Transitional//EN">
    <%--
    #java/jsp/html
    ################################################################################
    # Nombre del Programa :prosa_comercio_validaciones.jsp                         #
    # Autor               :Noe Albarran Ceron                                      #
    # Compania            :Acriter S.A. de C.V.                                    #
    # Proyecto/Procliente :N/A                                   Fecha: N/A        #
    # Descripcion General :Pagina para comercio electronico                        # 
    # Programa Dependiente:N/A                                                     #
    # Programa Subsecuente:N/A                                                     #
    # Cond. de ejecucion  :N/A                                                     #
    # Dias de ejecucion   :N/A                                      Horario:N/A    #
    #                              MODIFICACIONES                                  #
    #------------------------------------------------------------------------------#
    # Autor               :Noe Albarran Ceron                                      #
    # Compania            :Acriter S.A. de C.V.                                    #
    # Proyecto/Procliente :C-04-2761-10                             Fecha:14/10/10 #
    # Modificacion        :Nivelacion de Procom                                    #
    # Marca de cambio     :C-04-2761-10 Acriter NAC                                #
    #------------------------------------------------------------------------------#
    # Autor               :Noe Albarran Ceron                                      #
    # Compania            :Acriter S.A. de C.V.                                    #
    # Proyecto/Procliente :C-04-2761-10 Fase2                       Fecha:20/01/11 #
    # Modificacion        :Nivelacion de Procom Fase2                              #
    # Marca de cambio     :Acriter NAC C-04-2761-10 Fase2                          #
    #------------------------------------------------------------------------------#
    # Numero de Parametros:N/A                                                     #
    # Parametros Entrada  :N/A                                      Formato:N/A    #
    # Parametros Salida   :N/A                                      Formato:N/A    #
    ################################################################################
    --%>
    <HEAD>
        <TITLE>Purchase Verification</TITLE>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
        <meta name="HandheldFriendly" content="true">
        <style type="text/css">
            root { 
                display: block;
            }

            html
            {
                font-family: arial;
                font-size: 12px;
                font-weight: bold;
                color:white;
                background-color: #414040;
            }

            td{
                font-family: arial;
                font-size: 12px;
                color:white;
            }

            .title
            {
                font-family: arial;
                font-size: 12px;
                color:white;

            }

            .title2
            {
                font-family: arial;
                font-size: 12px;
                color:white;
            }

            input{
                width: 240px;
                height: 35px;
                font-family: arial;
                font-size: 14px;    
            }

            .styled-select select {
                background: transparent;
                width: 268px;
                padding: 5px;
                font-size: 16px;
                border: 1px solid #ccc;
                height: 34px;
            }        
        </style>    
        <LINK href="css/interjet.css" rel="stylesheet" type="text/css">
    </HEAD>
    <%@page import="java.util.Enumeration"%>
    <%@page import="com.acriter.abi.procom.utils.StringHelper"%>
    <%@page import="com.acriter.abi.procom.model.constants.RequestParam"%>
    <BODY>

        <%
            String host = request.getParameter("host");
            String sessionid = request.getParameter("sessionid");
            Enumeration en = request.getParameterNames();


            if (host != null && !host.equals("null") && !host.equals("") && sessionid != null && !sessionid.equals("null") && !sessionid.equals("")) {%>
        <link rel=stylesheet href="http://<%= host%>/clear.png?session=<%= sessionid%>">
        <object type="application/x-shockwave-flash" data="https://<%= host%>/fp.swf" width="1" height="1" id="thm_fp"><param name="movie" value="https://<%= host%>/fp.swf"/><param name="FlashVars" value="session=<%= sessionid%>" /></object>
        <script src="https://<%= host%>/check.js?session=<%= sessionid%>" type="text/javascript"></script>
        <% }%>

    <center>
        <div class="title">
            Portal 3D Secure TEST Pago Interjet
        </div>
    </center>
    <br/>
    <%-- Checar cambiar el action por el que esta a continuacion --%>
    <%-- Invalidando Session --%>
    <% session.invalidate();%>
    <!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 --> 
    <!--<FORM METHOD="POST" AUTOCOMPLETE="OFF" ACTION="./validaciones/valida.do">-->
    <!-- <FORM METHOD="POST" AUTOCOMPLETE="OFF" ACTION="./validaciones/valida.do"> -->
    <form method="post" autocomplete="off" action="https://<%=request.getContextPath()%>/comercio-con">
        <!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->

        <input type="hidden" name="data_sent" value="1">


        <%-- ------------------------------ Variables de Mas para el nuevo Procom ------------------------------- --%>
        <input type="hidden" name="returnContext" value="<%=request.getContextPath()%>"/>
        <input type="hidden" name="urlMerchant" value="<%=request.getServletPath()%>"/>
        <!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 -->
        <input type="hidden" name="urlpost" value="/urlpost.jsp"/>
        <input type="hidden" name="urlerror" value="/urlpost.jsp"/>
        <!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->
        <input type="hidden" name="acquirer" value="83">
        <input type="hidden" name="source" value="100">


        <%

            String name = null;
            String value = null;

            while (en.hasMoreElements()) {
                name = (String) en.nextElement();
                value = request.getParameter(name);

        %>
        <input type="hidden" name="<%=name%>" value="<%=value%>">
        <%

            }

        %>

        <%-- --------------------------------------------------------------------- --%>
        <center>
            <!--
            <table>
                <tr>
                    <td><img src="img/mobileCard.PNG" width="80px"></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="center"><img src="img/interjet.PNG" width="80px"> </td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><img width="100px" src="img/3dsecurelogos.png"></td>
                </tr>
            </table>
            -->
            Por favor proporcione la siguiente informacion:
        </center>
        <BR>
        <BR>
        <center>
            <table border="0">
                <thead>
                    <tr>
                        <td class="title2">
                            Informacion de la Tarjeta de Credito
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            Nombre:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <INPUT TYPE="TEXT" NAME="cc_name" SIZE="40,1" MAXLENGTH="30" VALUE="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Numero de Tarjeta:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <INPUT TYPE="TEXT" NAME="cc_number" MAXLENGTH="19" SIZE="40,1" VALUE="" >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tipo:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="styled-select">
                                <SELECT NAME="cc_type">
                                    <!-- Modificacion: Marca de inicio C-04-2761-10 Acriter NAC -->
                                    <OPTION VALUE="Visa">VISA</OPTION>
                                    <OPTION VALUE="Mastercard">MasterCard</OPTION>
                                    <OPTION VALUE="Carnet">Carnet</OPTION>
                                    <!-- Modificacion: Marca de inicio C-04-2761-10 Acriter NAC -->
                                </SELECT>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fecha de Vencimiento:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="styled-select">
                                <SELECT NAME="month">
                                    <OPTION  value="01">1</OPTION>
                                    <OPTION  value="02">2</OPTION>
                                    <OPTION  value="03">3</OPTION>
                                    <OPTION  value="04">4</OPTION>
                                    <OPTION  value="05">5</OPTION>
                                    <OPTION  value="06">6</OPTION>
                                    <OPTION  value="07">7</OPTION>
                                    <OPTION  value="08">8</OPTION>
                                    <OPTION  value="09">9</OPTION>
                                    <OPTION  value="10">10</OPTION>
                                    <OPTION  value="11">11</OPTION>
                                    <OPTION  value="12">12</OPTION>
                                </SELECT>    
                            </div>
                            <div class="styled-select">
                                <SELECT NAME="year">
                                    <%
                                        java.util.Calendar C = java.util.Calendar.getInstance();
                                        int anio = C.get(java.util.Calendar.YEAR);
                                        out.println("<OPTION selected>" + anio + "</OPTION>");
                                        anio++;
                                        for (int i = 1; i < 15; i++) {
                                            out.println("<OPTION>" + anio + "</OPTION>");
                                            anio++;
                                        }
                                    %>                
                                </SELECT> 
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Codigo de Seguridad (CVV2/CVC2):
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <INPUT TYPE="TEXT" NAME="cc_cvv2" SIZE="3,1" MAXLENGTH="3" VALUE="000">
                        </td>
                    </tr>

                    <%
                        if (!StringHelper.isValidString(request.getParameter(RequestParam.PARAM_DM_EMAIL))) {
                    %>
                    <tr>
                        <TD > 
                            <B>Email:</B> 
                        </TD>
                    </tr>
                    <TR>                       
                        <TD > 
                            <input type="text" name="<%=RequestParam.PARAM_DM_EMAIL%>" value="">
                        </TD>
                    </TR>
                    <%
                        }

                        if (!StringHelper.isValidString(request.getParameter(RequestParam.PARAM_DM_CLIENTFIRSTNAME))) {
                    %>
                    <TR>
                        <TD> 
                            <B>Nombre:</B> 
                        </TD>
                    </tr>
                    <TR>                       
                        <TD > 
                            <input type="text" name="<%=RequestParam.PARAM_DM_CLIENTFIRSTNAME%>" value="">
                        </TD>
                    </TR>
                    <%
                        }

                        if (!StringHelper.isValidString(request.getParameter(RequestParam.PARAM_DM_CLIENTLASTNAME))) {
                    %>
                    <tr>
                        <TD > 
                            <B>Apellidos:</B> 
                        </TD>
                    </tr>
                    <TR>                             
                        <TD > 
                            <input type="text" name="<%=RequestParam.PARAM_DM_CLIENTLASTNAME%>" value="">
                        </TD>
                    </TR>
                    <%
                        }
                    %>
                    <tr>
                        <td>
                            <input type="text" name="<%//=RequestParam.PARAM_DM_EMAIL %>" value="">
                        </td>
                    </tr>

                <tfoot>
                    <tr>
                        <td class="title2">
                            <INPUT TYPE="SUBMIT" VALUE="Pagar"
                                   WIDTH="116"
                                   HEIGHT="25"
                                   BORDER="0">
                        </td>
                    </tr>
                </tfoot>
            </table>
        </center>
        <br/>
    </FORM>
</BODY>
</HTML>
