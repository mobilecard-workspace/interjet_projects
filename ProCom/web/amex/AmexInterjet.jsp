<%-- 
    Document   : AmexInterjet
    Created on : 27-feb-2013, 11:13:47
    Author     : Salvador
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <TITLE>Purchase Verification</TITLE>
    <head>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
        <meta name="HandheldFriendly" content="true">
        <style type="text/css">
            root { 
                display: block;
            }

            html
            {
                font-family: arial;
                font-size: 12px;
                font-weight: bold;
                color:white;
                background-color: #414040;
            }

            td{
                font-family: arial;
                font-size: 12px;
                color:white;
            }

            .title
            {
                font-family: arial;
                font-size: 12px;
                color:white;

            }

            .title2
            {
                font-family: arial;
                font-size: 12px;
                color:white;
            }

            input{
                width: 240px;
                height: 35px;
                font-family: arial;
                font-size: 14px;    
            }

            .styled-select select {
                background: transparent;
                width: 268px;
                padding: 5px;
                font-size: 16px;
                border: 1px solid #ccc;
                height: 34px;
            }        
        </style>    
        <LINK href="css/interjet.css" rel="stylesheet" type="text/css">
    </HEAD>
    <body>
        <form name="form1" id="form1" method="post">
            <table>
                <tr>
                    <td>Tarjeta AMEX:</td>
                    <td><input size="18" name="amexTrj"/></td>
                </tr>
                <tr>
                    <td>Monto:</td>
                    <td><input size="10" name="amexMnt"/></td>
                </tr>
                <tr>
                    <td colspan="2">Pagar</td>                    
                </tr>
            </table>
            
        </form>
    </body>
</html>
