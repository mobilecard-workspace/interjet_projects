<%-- 
    Document   : comercio_con
    Created on : 23-sep-2012, 20:12:17
    Author     : Salvador
--%>
<%@page import="java.util.Date"%>
<%@page import="com.addcel.common.BitacoraDTO"%>
<%@page import="com.addcel.common.ConexionUrlRemota"%>
<%@page import="com.addcel.tools.util.Crypto"%>
<%@page import="com.addcel.common.HttpRequest"%>
<%@page import="com.addcel.tools.encryption.SHA1"%>
<%@page import="com.addcel.procom.TransactionControl"%>
<%@page import="com.addcel.common.MCUser"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="org.slf4j.Logger"%>
<%@page import="org.slf4j.LoggerFactory"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
	//static final Logger logger = LoggerFactory.getLogger(comercio);

    String varMerchant ="7412827";
    String varTotal = "0";
    String varOrderId = "";
    String varStore ="1234";
    String varTerm = "001";
    String varCurrency = "484";
    
    String varAddress = "PROSA";    
    String digest = "";
    PrintWriter out2 = response.getWriter();
    
    //MC
    String user = null;
    String monto = "";
    String idPago = null;
    MCUser mcUser = null;
    String tipoTarjeta = null;
    String tarjetaDes = null;
    String tarjeta = null;    
    String email = null;
    long idBitacora = 0;
    BitacoraDTO bit = null;
    HttpRequest req = new HttpRequest();
    
    if(request.getParameter("user")!=null){
        user = request.getParameter("user").toString();
    }
    if(request.getParameter("merchantId")!=null){
        varMerchant = request.getParameter("merchantId").toString();
    }
    if(request.getParameter("monto")!=null){
        monto = request.getParameter("monto").toString();
        System.out.println("Monto:"+monto);
        
        monto = (Double.parseDouble(monto)+25d) + "";
        
        varTotal = Integer.toString((int)(Double.parseDouble(monto) * 100));
        
        //String[] numero = monto.split("\\.");
//        System.out.println("Lenght:"+numero.length);
  //      if(numero[1].length()>2)
    //        numero[1] = numero[1].substring(0,2);
        //varTotal = numero[0] + numero[1];
        
        System.out.println("varTotal:"+varTotal);
        //varTotal = monto.replaceAll(".", "");
    }
    if(request.getParameter("referencia")!=null){
        varOrderId = request.getParameter("referencia").toString();
    }
    
    //Datos para validar pago desde OnComercio
    if(request.getParameter("idPago")!=null){
    	idPago = request.getParameter("idPago").toString();
    }
    if(request.getParameter("tarjeta")!=null){
    	tarjeta = request.getParameter("tarjeta").toString();
    }
    if(request.getParameter("email")!=null){
    	email = request.getParameter("email").toString();
    }
    //varOrderId = TransactionControl.createTransaction( varTotal, varMerchant)+"";
    //varTotal="100";
    SHA1 sha = new SHA1();
    digest = sha.encrypt(varMerchant + varStore + varTerm + varTotal + varCurrency + varOrderId);
    
    if(idPago == null){
    	mcUser = TransactionControl.getData(user);
        tipoTarjeta = mcUser.getUserTipoTarjetaId();
        tarjetaDes = Crypto.aesDecrypt(mcUser.getUserTdc(), "5525963513");
        tarjeta = Crypto.aesEncrypt(tarjetaDes);    
        email = mcUser.getEmail();
        
        bit = new BitacoraDTO(mcUser.getUserId(), 11, "111", new Date(), new Date(), "Pago reservación INTERJET", monto,"0", mcUser.getUserTdc());        
                
    }else if(idPago != null){
    	tipoTarjeta = "1";
    	tarjetaDes = Crypto.aesDecrypt(tarjeta.replaceAll(" ", "+"), "5525963513");
        tarjeta = Crypto.aesEncrypt(tarjetaDes); 
    	bit = new BitacoraDTO("0", 11, "111", new Date(), new Date(), "Pago reservación INTERJET", monto,"0", tarjeta);
    }
    System.out.println("pnr: " + varOrderId);
    System.out.println("tarjeta: " + tarjeta);
    System.out.println("Email: " + email);
    System.out.println("Total a pagar: "+varTotal);
    
    idBitacora = TransactionControl.insertaBitacora(bit);
    System.out.println("idBitacora: "+idBitacora);
    
    if(idPago != null){
    	System.out.println("Actualizando GPO_RICHARD_detalle.");
    	TransactionControl.updateGrupoRichardDetalle(idPago, String.valueOf(idBitacora), null, "0");
    }
    
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
        <script language="JavaScript">
        var tipoTarjeta = '<%=tipoTarjeta%>';
        var monto = '<%=monto%>';
        var idbitacora = '<%=idBitacora%>';
        var varOrderId = '<%=varOrderId%>';
        var pnr = '<%=varOrderId%>';
        var user = '<%=user%>';
        
        function sendform(){
            if(self.name.length == 0) {
                self.name = "gotoProsa";
            }
            //var twnd = window.open("","wnd","toolbar=0,location=0,directories=0,status=0,menubar=0,resizable=1,copyhistory=0,width=760,height=750");
            document.form1.return_target.value=self.name.toString();
            //document.form1.target = "wnd";
            monto = monto.replace(".","%2E");
            if(tipoTarjeta == '3')                
                document.location.href = "/InterjetWeb/pagoAMEX.jsp?user="+user+"&monto="+monto+"&pnr="+varOrderId+"&bitacora="+idbitacora;
            else
                document.form1.submit();
        }
        </script>        
    </head>
    <body onload="sendform();">
        
        <!--URL PRODUCCION-->
        <FORM METHOD="POST" NAME ="form1" ACTION="https://www.procom.prosa.com.mx/eMerchant/7412827_interjet.jsp">
        <!-- FORM METHOD="POST" NAME ="form1" ACTION="https://50.57.192.214/ProCom/prosa_comercio.jsp"> -->
        <!-- <FORM METHOD="POST" NAME ="form1" ACTION="https://localhost:8443/ProCom/prosa_comercio.jsp"> -->

        <!--URL TESTING-->
        <!--<FORM METHOD="POST" NAME ="form1" ACTION="prosa_comercio.jsp">-->
            <input type="hidden" name="total" value="<%=varTotal%>">
            <input type="hidden" name="currency" value="<%=varCurrency%>">
            <input type="hidden" name="address" value="<%=varAddress%>">
            <input type="hidden" name="order_id" value="<%=varOrderId%>">
            <input type="hidden" name="merchant" value="<%=varMerchant%>">
            <input type="hidden" name="store" value="<%=varStore%>">
            <input type="hidden" name="term" value="<%=varTerm%>">
            <input type="hidden" name="digest" value="<%=digest%>">
            <input type="hidden" name="return_target" value="">
            <input type="hidden" name="numus" value="<%=tarjeta%>">
            <input type="hidden" name="correo" value="<%=email%>">
            <input type="hidden" name="idbitacora" value="<%=idBitacora%>">
            <%
            if(request.getParameter("idPago")!=null){ %>
            	<input type="hidden" name="idPago" value="<%=idPago%>">
            <%}%>
            <input type="hidden" name="urlBack" value="http://www.mobilecard.mx:8080/ProCom/comercio_con.jsp">
           <!-- <input type="hidden" name="urlBack" value="http://192.168.0.150:8080/demo/comercio_con.php">-->
            
                        
        </FORM>
    </body>
</html>
