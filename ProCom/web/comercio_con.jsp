<%@page import="com.addcel.common.TransaccionManager"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="com.addcel.common.BitacoraDTO"%>
<%@page import="com.addcel.tools.util.Crypto"%>
<%@page import="com.addcel.procom.TransactionControl"%>
<%@page import="com.addcel.common.ConexionUrlRemota"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<%-- 
    Document   : comercio_con
    Created on : 24-sep-2012, 0:39:57
    Author     : Salvador
--%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    String emResponse = "";
    String emTotal = "";
    String emOrderId = "";
    String emMerchant = "";
    String emStore = "";
    String emTerm = "";
    String emRefNum = "";
    String emAuth = "";
    String emDigest = "";
    String num="";
    String email="";
    String idPago = null;
    
    String idbitacora="";
    if(request.getParameter("EM_Response")!=null)
    {
        emResponse = request.getParameter("EM_Response").toString();
    }
    if(request.getParameter("EM_Total")!=null)
    {
        emTotal = request.getParameter("EM_Total").toString();
    }
    if(request.getParameter("EM_OrderID")!=null)
    {
        emOrderId = request.getParameter("EM_OrderID").toString();
    }
    if(request.getParameter("EM_Merchant")!=null)
    {
        emMerchant = request.getParameter("EM_Merchant").toString();
    }
    if(request.getParameter("EM_Store")!=null)
    {
        emStore = request.getParameter("EM_Store").toString();
    }
    if(request.getParameter("EM_Term")!=null)
    {
        emTerm = request.getParameter("EM_Term").toString();
    }
    if(request.getParameter("EM_RefNum")!=null)
    {
        emRefNum = request.getParameter("EM_RefNum").toString();
    }
    if(request.getParameter("EM_Auth")!=null)
    {
        emAuth = request.getParameter("EM_Auth").toString();
    }
    if(request.getParameter("EM_Digest")!=null)
    {
        emDigest = request.getParameter("EM_Digest").toString();
    }
    if(request.getParameter("numus")!=null)
    {
        num = Crypto.aesDecrypt(request.getParameter("numus").toString(),null);
    }
    if(request.getParameter("correo")!=null)
    {
        email=request.getParameter("correo").toString();
    }   
    if(request.getParameter("idbitacora")!=null)
    {
        idbitacora=request.getParameter("idbitacora").toString();
    }
    
  //Datos para validar pago desde OnComercio
    if(request.getParameter("idPago")!=null){
    	idPago = request.getParameter("idPago").toString();
    }

    System.out.print("Numero Tarjeta: "+num);
    //Inserccion en la tabla transaccionProsa ecommerce
    TransactionControl.createTransaction(emTotal, emMerchant, emResponse, emTotal, emOrderId, emMerchant, emStore, emTerm, emRefNum, emAuth, emDigest);
    
    BitacoraDTO bit = new BitacoraDTO(idbitacora, new Date(), new Date(), "Pago reservación INTERJET exitosa", emAuth, "0");
    Map<String, String> dtoBitProsa=new HashMap<String,String>();
    dtoBitProsa.put("idbitacora",idbitacora );
    dtoBitProsa.put("idusuario",idbitacora );
    dtoBitProsa.put("tarjeta",num );
    dtoBitProsa.put("transaccion", emRefNum);
    dtoBitProsa.put("autorizacion",emAuth );
    dtoBitProsa.put("concepto","Pago reservación INTERJET" );
    dtoBitProsa.put("cargo",emTotal );
    dtoBitProsa.put("comision","0" );
    dtoBitProsa.put("cx","" );
    dtoBitProsa.put("cy","" );
    TransactionControl.insertaBitacoraProsa(dtoBitProsa);
    if(!emAuth.equalsIgnoreCase("000000")){
        bit.setBitStatus("1");
        TransactionControl.actualizaBitacora(bit);                  
    }else{
        bit.setBitConcepto("Pago reservación INTERJET con error. "+emResponse);
        TransactionControl.actualizaBitacora(bit);        
    }
    
    if(idPago != null){
    	TransactionControl.updateGrupoRichardDetalle(idPago, String.valueOf(idbitacora), !emAuth.equalsIgnoreCase("000000")?emAuth: null, !emAuth.equalsIgnoreCase("000000")?"1": "0");
    }
    
   
    num = (String) session.getAttribute("num");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <LINK href="css/interjet.css" rel="stylesheet" type="text/css">
        <meta name="HandheldFriendly" content="true">
        <title>Pago Procesado</title>
    </head>
    <body>
        <center>
            <br/>
            <br/>
            <br/>
            <div class="">
                <INPUT TYPE="hidden" ID="emResponse" VALUE="<%=emResponse%>">
                <INPUT TYPE="hidden" ID="emTotal" VALUE="<%=emTotal%>">
                <INPUT TYPE="hidden" ID="emOrderId" VALUE="<%=emOrderId%>">
                <INPUT TYPE="hidden" ID="emMerchant" VALUE="<%=emMerchant%>">
                <INPUT TYPE="hidden" ID="emStore" VALUE="<%=emStore%>">
                <INPUT TYPE="hidden" ID="emTerm" VALUE="<%=emTerm%>">
                <INPUT TYPE="hidden" ID="emRefNum" VALUE="<%=emRefNum%>">
                <INPUT TYPE="hidden" ID="emAuth" VALUE="<%=emAuth%>">
                <INPUT TYPE="hidden" ID="emDigest" VALUE="<%=emDigest%>">
                <table >
                    <tr>
                        <td colspan="2">
                            <%
                                if(emResponse.equals("deny"))
                                {
                            %>
                                    Su pago NO fue procesado
                            <%
                                }
                                if(emResponse.equals("approved"))
                                {
                            %>
                                    Su pago fue procesado
                            <%
                                }
                            %>
                        </td>
                    </tr>
                    <%
                        if(emResponse!=null && !emResponse.equals(""))  //QUITAR EL FALSE
                        {
                    %>
                    <tr>
                        <td>
                            Respuesta:
                        </td>
                        <td>
                            <%=emResponse%>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                    <%
                        if(emRefNum!=null && !emRefNum.equals(""))
                        {
                    %>
                    <tr>
                        <td>
                            Referencia:
                        </td>
                        <td>
                            <%=emRefNum%>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                    <%
                    
                        if(emAuth!=null && !emAuth.equals("") && !emAuth.equals("000000"))
                        {
                            //Commit a interjet
                            String respuesta ="";
                            String pnr = "";
                            ConexionUrlRemota req = new ConexionUrlRemota();
                            if(emOrderId.length()<=6){              
                                pnr=emOrderId;
                            }else{
	                            pnr = new TransaccionManager().getTransactionByPNR(emOrderId);
	                            
                            }
                            respuesta = req.peticionHttpsUrlParams("https://localhost:8443/InterjetWeb/setCommit",
                            		"pnr=" + pnr + "&transaccion=" + emOrderId + "&auth=" + emAuth + "&num=" + num);
                            System.out.println("** TRANSACCION INTERJET: Order:"+emOrderId+" PNR:"+pnr+" NUM:" +num + " RESPUESTA:"+respuesta);
                            System.out.println("** Envio mail INTERJET: Order:"+"http://mobilecard.mx:8080/AddCelBridge/EnvioMail?mail="+email+"&cveAsunto=@ASUNTO_COMPRAINTERJET&cveMensaje=@MENSAJE_COMPRAINTERJET&monto="+emTotal+"&autorizacion="+emAuth);                                                      
                            req.peticionHttpsUrlParams("http://localhost:8080/AddCelBridge/EnvioMail",
                            		"mail="+email+"&cveAsunto=@ASUNTO_COMPRAINTERJET&cveMensaje=@MENSAJE_COMPRAINTERJET&monto="+emTotal+"&autorizacion="+emAuth);
                    %>
                    <tr>
                        <td>
                            Autorizaci&oacute;n:
                        </td>
                        <td>
                            <%=emAuth%>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                    <tr>
                        <td colspan="2">Presione OK para terminar su transacci&oacute;n</td>
                    </tr>
                </table>
            </div>
        </center>
    </body>
</html>
