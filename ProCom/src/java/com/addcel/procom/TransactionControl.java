/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.procom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.common.BitacoraDTO;
import com.addcel.common.MCUser;
import com.addcel.tools.util.DatabaseImpl;

/**
 *
 * @author ADDCEL13
 */
public class TransactionControl {

	private static final Logger logger = LoggerFactory.getLogger(TransactionControl.class);

    public static void updateTransaction(int prosaTransactionId, int transactionId) {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        String query = null;
        try {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Actualizando mensaje de AMex
            query = "UPDATE TransactionProcom SET "
                    + "TransactionProsaId = " + transactionId + " "
                    + " WHERE TransactionProcomId=" + prosaTransactionId;
            logger.debug("Insert: {}", query);
            st.executeUpdate(query);

        } catch (Exception e) {
            logger.debug("ECOMMERCE POOL: General Exception: {}",  e);
        } finally {
            ds.closeConSt(st, conn);
        }

    }
    
    public static void updateGrupoRichardDetalle(String idPago, String idBitacora, String numAutorizacion, String status) {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        String query = null;
        try {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Actualizando mensaje de AMex
            query = "UPDATE mobilecard.GPO_RICHARD_detalle " +
            		"SET " +
            		"	ID_BITACORA = " + idBitacora + ", " +
            		"	STATUS = " + status + ", " +
            		"	FECHA_MODIFICA = NOW() ";
            if(numAutorizacion != null){
            	query += ",	NUMAUTORIZACION = " + numAutorizacion;
            }
            query += "	WHERE ID_PAGO = "  + idPago;	
            
            logger.debug("UPDATE: {}", query);
            st.executeUpdate(query);

        } catch (Exception e) {
            logger.debug("ECOMMERCE POOL: General Exception: {}",  e);
        } finally {
            ds.closeConSt(st, conn);
        }

    }
    
    public static long createTransaction(String monto, String merchant,
            String em_response, String em_total, String em_orderId, String em_merchant, String em_store,
            String em_term, String em_refNum, String em_auth, String em_digest) {
    	DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet resultSet = null;
        String query;
        long lastIdn = 0;

        try {
            conn = ds.getConnection();
            query = "INSERT INTO TransactionProcom "
                    + "(TransactionProcomMnt, TransactionProcomMrchId,"
                    + " EM_Response, EM_Total, EM_OrderID,EM_Merchant,EM_Store,EM_Term,EM_RefNum,"
                    + " EM_Auth,EM_Digest"
                    + ")"
                    + " VALUES "
                    + "('" + monto + "','" + merchant + "','" + em_response + "','" + em_total + "','" + em_orderId + "',"
                    + "'" + em_merchant + "','" + em_store + "','" + em_term + "','" + em_refNum + "','" + em_auth + "','" + em_digest + "')";
            logger.info("Insert:" + query);
            st = conn.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            st.execute();
            resultSet=st.getGeneratedKeys();
            if(resultSet.next()){
            	lastIdn=resultSet.getLong(1);
            }
            logger.info("ID TransactionProcom: " + lastIdn);
        } catch (Exception e) {
            logger.debug("ECOMMERCE POOL: General Exception: {}", e);
        } finally {
            ds.closeConSt(st, conn);
        }
        return lastIdn;
    }

    public static MCUser getData(String userId) {
        DatabaseImpl ds = new DatabaseImpl("mobilecard");
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = null;
        MCUser mobileCardUser = null;

        try {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Obteniendo ultimo id
            query = "select id_usuario,usr_login,usr_nombre, usr_apellido, usr_materno, usr_tdc_numero, usr_tdc_vigencia, desc_tipo_tarjeta, t_usuarios.id_tipo_tarjeta, t_usuarios.eMail "
                    + "from t_usuarios left join t_tipo_tarjeta on t_usuarios.id_tipo_tarjeta=t_tipo_tarjeta.id_tipo_tarjeta "
                    + "where id_usr_status = 1 and usr_login='" + userId + "'";
            rs = st.executeQuery(query);
            logger.debug("SELECT: {}", query);
            if (rs.next()) {
                mobileCardUser = new MCUser();
                mobileCardUser.setUserId(rs.getString(1));
                mobileCardUser.setUserLogin(rs.getString(2));
                mobileCardUser.setUserNombre(rs.getString(3));
                mobileCardUser.setUserApellido(rs.getString(4));
                //mobileCardUser.setUserId(rs.getString(5)); Apellido materno
                mobileCardUser.setUserTdc(rs.getString(6));
                mobileCardUser.setUserVig(rs.getString(7));
                mobileCardUser.setUserTipoTarjeta(rs.getString(8));
                mobileCardUser.setUserTipoTarjetaId(rs.getString(9));
                mobileCardUser.setEmail(rs.getString(10));

            }
        } catch (Exception e) {
            logger.debug("ECOMMERCE POOL: General Exception: {}", e);
        } finally {
            ds.closeConPrepStResSet(st, rs, conn);
        }
        return mobileCardUser;

    }
    
    public static long insertaBitacora(BitacoraDTO bitacora) {
        DatabaseImpl di = new DatabaseImpl("mobilecard");
        Connection conexion = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        long idBitacora = 0;
        try {
            conexion = di.getConnection();
            String query = "INSERT INTO t_bitacora "
                    + "(id_usuario,id_proveedor,id_producto,bit_fecha,bit_hora,bit_concepto,bit_cargo,bit_status,tarjeta_compra) "
                    + "values(?,?,?,NOW(),NOW(),?,?,?,?)";
            
            ps = conexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, bitacora.getIdUsuario());
            ps.setInt(2, bitacora.getIdProveedor());
            ps.setString(3, bitacora.getIdProducto());
//            ps.setDate(4, new java.sql.Date(bitacora.getBitFecha().getTime()));
//            ps.setTimestamp(5, new java.sql.Timestamp(bitacora.getBitHora().getTime()));
            ps.setString(4, bitacora.getBitConcepto());
            ps.setString(5, bitacora.getBitCargo());
            ps.setString(6, bitacora.getBitStatus());
            ps.setString(7, bitacora.getTarjetaCompra());
            int executeUpdate = ps.executeUpdate();
            
            rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                idBitacora = rs.getLong(1);
            }
            logger.info( "insert rs: {}", executeUpdate);
            logger.info( "insert idBitacora: {}", idBitacora);
        } catch (ClassNotFoundException ex) {
            logger.error("Error  General Exception: {}", ex);       
        } catch (SQLException ex) {
            logger.error("Error  General Exception: {}", ex);       
        }finally{
        	di.closeConPrepStResSet(ps, rs, conexion);
        }
        return idBitacora;
    }

    public static int actualizaBitacora(BitacoraDTO bitacora) {
        DatabaseImpl di = new DatabaseImpl("mobilecard");
        Connection conexion = null;
        PreparedStatement ps = null;
        int update = 0;
        try {
            conexion = di.getConnection();
            String query = "UPDATE t_bitacora SET "
                    + "bit_fecha=NOW(),bit_hora=NOW(),bit_concepto=?,bit_status=?, bit_no_autorizacion=? WHERE id_bitacora=?";
            
            ps = conexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
//            ps.setDate(1, new java.sql.Date(bitacora.getBitFecha().getTime()));
//            ps.setTimestamp(2, new java.sql.Timestamp(bitacora.getBitHora().getTime()));
            ps.setString(1, bitacora.getBitConcepto());
            ps.setString(2, bitacora.getBitStatus());
            ps.setString(3, bitacora.getBitNoAutorizacion());
            ps.setString(4, bitacora.getIdBitacora());
            update = ps.executeUpdate();
        } catch (ClassNotFoundException ex) {
            logger.error("Error  General Exception: {}", ex);       
        } catch (SQLException ex) {
            logger.error("Error  General Exception: {}", ex);       
        }finally{
        	di.closeConPreparedSt(ps, conexion);
        }
        return update;
    }

    /**
     * Inserta en la tabla t_bitacoraProsa de la bd mobilecard
     *
     * @param Map con los datos
     * @return id del registro insertado
     */
    public static long insertaBitacoraProsa(Map<String, String> campos) {
        DatabaseImpl di = new DatabaseImpl("mobilecard");
        Connection conexion;
        PreparedStatement ps0 = null;
        PreparedStatement ps = null; 
        ResultSet eq = null;
        ResultSet rs = null;
        long idBitacora = 0;
        try {
            conexion = di.getConnection();
            ps0 = conexion.prepareStatement("select id_bitacoraProsa from t_bitacoraProsa where id_bitacora=?");
            ps0.setString(1, campos.get("idbitacora"));
            eq = ps0.executeQuery();
            
            if (eq != null && eq.next()) {
                String idBitacoraProsa = eq.getString(1);
                String query = "UPDATE t_bitacoraProsa SET id_bitacora=?,id_usuario=?,tarjeta=?,transaccion=?,autorizacion=?,fecha=NOW(),Concepto=?,TarjetaIAVE=?,Cargo=?,Comision=?,cx=?,cy=? WHERE id_bitacoraProsa=?";
                ps = conexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, campos.get("idbitacora"));
                ps.setString(2, campos.get("idusuario"));
                ps.setString(3, campos.get("tarjeta"));
                ps.setString(4, campos.get("transaccion"));
                ps.setString(5, campos.get("autorizacion"));
                //ps.setDate(6, new java.sql.Date(new Date().getTime()));
                ps.setString(6, campos.get("concepto"));
                ps.setString(7, campos.get("tarjeta"));
                ps.setString(8, campos.get("cargo"));
                ps.setString(9, campos.get("comision"));
                ps.setString(10, campos.get("cx"));
                ps.setString(11, campos.get("cy"));
                ps.setString(12, idBitacoraProsa);
                logger.debug("Update: {}" , query);
                int executeUpdate = ps.executeUpdate();
                logger.info( "UPDATE bitacoraProsa rs: {}", executeUpdate);
            } else {
                String query = "INSERT INTO t_bitacoraProsa (id_bitacora,id_usuario,tarjeta,transaccion,autorizacion,fecha,Concepto,TarjetaIAVE,Cargo,Comision,cx,cy) VALUES(?,?,?,?,?,NOW(),?,?,?,?,?,?)";
                ps = conexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, campos.get("idbitacora"));
                ps.setString(2, campos.get("idusuario"));
                ps.setString(3, campos.get("tarjeta"));
                ps.setString(4, campos.get("transaccion"));
                ps.setString(5, campos.get("autorizacion"));
                //ps.setDate(6, new java.sql.Date(new Date().getTime()));
                ps.setString(6, campos.get("concepto"));
                ps.setString(7, campos.get("tarjeta"));
                ps.setString(8, campos.get("cargo"));
                ps.setString(9, campos.get("comision"));
                ps.setString(10, campos.get("cx"));
                ps.setString(11, campos.get("cy"));
                logger.debug("Insert: {}" , query);
                int executeUpdate = ps.executeUpdate();
                
                rs = ps.getGeneratedKeys();
                if (rs != null && rs.next()) {
                    idBitacora = rs.getLong(1);
                    rs.close();
                }
                ps.close();
                conexion.close();
                logger.info( "INSERT bitacoraProsa rs: {}", executeUpdate);
            }           
        } catch (ClassNotFoundException ex) {
            logger.error("Error  General Exception: {}", ex);       
        } catch (SQLException ex) {
            logger.error("Error  General Exception: {}", ex);       
        }
        return idBitacora;
    }

    /************************************************************
     *                  INICIO DE CAMBIOS
     * Metodos para sustituir las llamadas a los servidios de .net
     *************************************************************/
    public static String getTransactionByPNR(String transaccion) {
    	DatabaseImpl dbImpl = new DatabaseImpl("mobilecard");
        Connection cnn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT transaccionPnr FROM ijet_transaccion WHERE transaccionIdn = ?";
        String pnr = "";
        try {
            cnn = dbImpl.getConnection();
            ps = cnn.prepareStatement(sql);
            ps.setString(1, transaccion);
            rs = ps.executeQuery();
            
            if (rs.next()){
            	pnr = rs.getString("transaccionPnr");
            }
            logger.debug("PNR: {}", pnr);
        } catch (Exception ex) {
            logger.error("Error  General Exception: {}", ex);       
        }finally{
        	dbImpl.closeConPrepStResSet(ps, rs, cnn);
        }
        
        return pnr;
    }
    
    /**********************************
     *          FIN DE CAMBIOS
     *********************************/
}
