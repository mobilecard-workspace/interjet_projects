package com.addcel.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.GeneralSecurityException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.procom.TransactionControl;

public class ConexionUrlRemota {
	private static final Logger logger = LoggerFactory.getLogger(TransactionControl.class);

    public static String peticionUrlPost(String urlRemota, String jsonParametros,
            boolean resEsp) {
        String respuesta = "Sin respuesta";
        try {
            URL url = new URL(urlRemota);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            // Se indica que se escribira en la conexi�n
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            // Se escribe los parametros enviados a la url
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            String parametros = "json=" + jsonParametros;
            wr.write(parametros);
            wr.close();
            // Si se espera una respuesta se crea un buffer para leerla.
            if (resEsp) {
                if (con.getResponseCode() != 200) {
                    logger.error("Error de comunicacion con el servidor :{}", con.getResponseCode());                                 
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String jsonString;
                while ((jsonString = br.readLine()) != null) {
                    sb.append(jsonString);
                }
                respuesta = sb.toString();
            }
            con.disconnect();           
        } catch (MalformedURLException ex) {
            logger.error("Ocurrio un error: {}", ex);            
        } catch (IOException ex) {
            logger.error("Ocurrio un error: {}", ex);         
        }
        return respuesta;
    }    

    public static String peticionUrlPostJson(String urlRemota, String jsonParametros,
            boolean resEsp) {
        String respuesta = "Sin respuesta";
        try {
            URL url = new URL(urlRemota);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            // Se indica que se escribira en la conexi�n
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

            // Se escribe los parametros enviados a la url
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(jsonParametros);
            wr.close();
            // Si se espera una respuesta se crea un buffer para leerla.
            if (resEsp) {
                if (con.getResponseCode() != 200) {
                    logger.error( "Error de comunicaci\u00f3n con el servidor :{}", con.getResponseCode());                   				
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String jsonString;
                while ((jsonString = br.readLine()) != null) {
                    sb.append(jsonString);
                }
                respuesta = sb.toString();               
            } else {
                logger.info("*** No se esperaba ninguna respuesta de la url: {} ***", urlRemota);                
            }
            con.disconnect();
        } catch (MalformedURLException ex) {
            logger.error("Ocurrio un error: {}", ex);            
        } catch (IOException ex) {
            logger.error("Ocurrio un error: {}", ex);            
        }
        return respuesta;
    }
    
    public String peticionHttpsUrlParams(String urlServicio, String parametros) {
		InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
		URL url = null;
		String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        
		HostnameVerifier hv = new HostnameVerifier() {
			public boolean verify(String urlHostName, SSLSession session) {
				logger.debug("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
				return true;
			}
		};	        
	    
        try {
        	trustAllHttpsCertificates();
    	    HttpsURLConnection.setDefaultHostnameVerifier(hv);
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.flush();
                    writer.close();
                    os.flush();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
//                logger.debug("Respuesta notificacion: {}", sbf);
            }
        } catch (GeneralSecurityException e) {
        	logger.info("Error Al generar certificados...");
        	e.printStackTrace();
        }catch (MalformedURLException e) {
        	logger.info("error al conectar con la URL: "+urlServicio);
        	e.printStackTrace();
        }catch (ProtocolException e) {
        	logger.info("Error al enviar datos.. ");
			e.printStackTrace();
		} catch (IOException e) {
			logger.info("Error al algo... ");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
        }
//        System.out.println(response.toString());
        return sbf.toString(); 
	}
	
	public static class miTM implements javax.net.ssl.TrustManager,
			javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}

	private static void trustAllHttpsCertificates() throws Exception {
		// Create a trust manager that does not validate certificate chains:
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
		sc.getSocketFactory());

	}
}
