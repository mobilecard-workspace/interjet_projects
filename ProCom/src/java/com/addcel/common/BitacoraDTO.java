/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.common;

import java.util.Date;

/**
 *
 * @author JORGE
 */
public class BitacoraDTO {
    private String idBitacora;              
    private String idUsuario;            
    private int idProveedor;
    private String idProducto;
    private Date bitFecha;
    private Date bitHora;
    private String bitConcepto;
    private String bitCargo;
    private String bitTicket;
    private String bitNoAutorizacion;
    private String bitCodigoError;
    private String bitCardId;
    private String bitStatus;
    private String imei;
    private String destino;
    private String tarjetaCompra;
    private String tipo;
    private String software;
    private String modelo;
    private String wkey;
    private String pase;

    public BitacoraDTO() {
    }

    public BitacoraDTO(String idUsuario, int idProveedor, String idProducto, Date bitFecha, Date bitHora, String bitConcepto, String bitCargo, String bitStatus, String tarjetaCompra) {
        this.idUsuario = idUsuario;
        this.idProveedor = idProveedor;
        this.idProducto = idProducto;
        this.bitFecha = bitFecha;
        this.bitHora = bitHora;
        this.bitConcepto = bitConcepto;
        this.bitCargo = bitCargo;
        this.bitStatus = bitStatus;
        this.tarjetaCompra = tarjetaCompra;
    }

    public BitacoraDTO(String idBitacora, Date bitFecha, Date bitHora, String bitConcepto, String bitNoAutorizacion, String bitStatus) {
        this.idBitacora = idBitacora;
        this.bitFecha = bitFecha;
        this.bitHora = bitHora;
        this.bitConcepto = bitConcepto;
        this.bitNoAutorizacion = bitNoAutorizacion;
        this.bitStatus = bitStatus;
    }        

    public String getIdBitacora() {
        return idBitacora;
    }        

    public void setIdBitacora(String idBitacora) {
        this.idBitacora = idBitacora;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public Date getBitFecha() {
        return bitFecha;
    }

    public void setBitFecha(Date bitFecha) {
        this.bitFecha = bitFecha;
    }

    public Date getBitHora() {
        return bitHora;
    }

    public void setBitHora(Date bitHora) {
        this.bitHora = bitHora;
    }
    public String getBitConcepto() {
        return bitConcepto;
    }

    public void setBitConcepto(String bitConcepto) {
        this.bitConcepto = bitConcepto;
    }

    public String getBitCargo() {
        return bitCargo;
    }

    public void setBitCargo(String bitCargo) {
        this.bitCargo = bitCargo;
    }

    public String getBitTicket() {
        return bitTicket;
    }

    public void setBitTicket(String bitTicket) {
        this.bitTicket = bitTicket;
    }

    public String getBitNoAutorizacion() {
        return bitNoAutorizacion;
    }

    public void setBitNoAutorizacion(String bitNoAutorizacion) {
        this.bitNoAutorizacion = bitNoAutorizacion;
    }

    public String getBitCodigoError() {
        return bitCodigoError;
    }

    public void setBitCodigoError(String bitCodigoError) {
        this.bitCodigoError = bitCodigoError;
    }

    public String getBitCardId() {
        return bitCardId;
    }

    public void setBitCardId(String bitCardId) {
        this.bitCardId = bitCardId;
    }

    public String getBitStatus() {
        return bitStatus;
    }

    public void setBitStatus(String bitStatus) {
        this.bitStatus = bitStatus;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getTarjetaCompra() {
        return tarjetaCompra;
    }

    public void setTarjetaCompra(String tarjetaCompra) {
        this.tarjetaCompra = tarjetaCompra;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getWkey() {
        return wkey;
    }

    public void setWkey(String wkey) {
        this.wkey = wkey;
    }

    public String getPase() {
        return pase;
    }

    public void setPase(String pase) {
        this.pase = pase;
    }            
}
