/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.tools.util;

/**
 *
 * @author JORGE
 */
public class CryptoEnc {

    public static String aesEncrypt(String seed, String cleartext) {
        String encryptedText;

        try {
            return AESBinFormat.encode(replaceConAcento(cleartext), seed);
        } catch (Exception e) {
            encryptedText = "";
        }

        return encryptedText;
    }

    public static String aesDecrypt(String seed, String encrypted) {
        String decryptedText;

        try {
            return replaceHTMLAcento(AESBinFormat.decode(encrypted, seed));
        } catch (Exception e) {
            decryptedText = "";
        }

        return decryptedText;
    }

    /*public static String sha1(String s) {
     if (s != null){
     try {
     SHA1Digest digest = new SHA1Digest();
     byte[] bb = s.getBytes();
     digest.update(bb, 0, bb.length);

     byte[] digestValue = new byte[digest.getDigestSize()];
     digest.doFinal(digestValue, 0);

     return MD5.toHex(digestValue);
     } catch (Exception e) {
     e.printStackTrace();
     }
     }

     return "";
     }*/
    public static String replaceHTMLAcento(String text) {

        String Res = text.replace("&Ntilde;", "�");
        Res = Res.replace("&ntilde;", "�");
        Res = Res.replace("&Aacute;", "�");
        Res = Res.replace("&aacute;", "�");
        Res = Res.replace("&Eacute;", "�");
        Res = Res.replace("&eacute;", "�");
        Res = Res.replace("&Iacute;", "�");
        Res = Res.replace("&iacute;", "�");
        Res = Res.replace("&Oacute;", "�");
        Res = Res.replace("&oacute;", "�");
        Res = Res.replace("&Uacute;", "�");
        Res = Res.replace("&uacute;", "�");

        return Res.toString();
    }

    public static String replaceConAcento(String text) {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '�') {
                sBuffer.append("&Ntilde;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&ntilde;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&Aacute;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&aacute;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&Eacute;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&eacute;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&Iacute;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&iacute;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&Oacute;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&oacute;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&Uacute;");
            } else if (text.charAt(i) == '�') {
                sBuffer.append("&uacute;");
            } else {
                sBuffer.append(text.charAt(i));
            }
        }

        return sBuffer.toString();
    }

    public static String parsePass(String pass) {
        int len = pass.length();
        String key = "";

        for (int i = 0; i < 32 / len; i++) {
            key += pass;
        }

        int carry = 0;
        while (key.length() < 32) {
            key += pass.charAt(carry);
            carry++;
        }
        return key;
    }
}
