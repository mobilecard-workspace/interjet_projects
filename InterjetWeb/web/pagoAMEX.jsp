
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="com.addcel.interjet.TransactionControl"%>
<%@page import="com.addcel.common.MCUser"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    String user="sburgara";
    String monto="5";
    String pnr="TYVB4A";
    String montoStr="5.0";
    String idbitacora="";

    Locale locale = new Locale("es", "MX");
    NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
      
    
    if(request.getParameter("user")!=null)
    {
        user = request.getParameter("user").toString();
    }
    if(request.getParameter("monto")!=null)
    {
        monto = request.getParameter("monto").toString();
    }
    if(request.getParameter("bitacora")!=null)
    {
        idbitacora = request.getParameter("bitacora").toString();
    }
    double dMonto = Double.parseDouble(monto);
    montoStr = fmt.format(dMonto);
    
    if(request.getParameter("pnr")!=null)
    {
        pnr = request.getParameter("pnr").toString();
    }

    System.out.println("user="+user);
    System.out.println("monto="+montoStr);
    System.out.println("pnr="+pnr);
    
    MCUser mcUser = null;
    mcUser= TransactionControl.getData(user);
    String card = mcUser.getUserTdc();
    String lastCard = card.substring(card.length()-5, card.length());

    /*
    String card = "0000000000000000000";
    String lastCard = card.substring(card.length()-5, card.length());
 * */
    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Pago Interjet AMEX</title>
        <meta name="HandheldFriendly" content="true">
        <LINK href="css/interjet.css" rel="stylesheet" type="text/css">
        <style>
            .big{
                font-size: 24px;
            }
            .notoobig{
                font-size: 20px;
            }
            .biggger{
                font-size: 30px;
            }
        </style>
        <script type="text/javascript">
            function sendSale()
            {
                document.location.href="regresoPago.jsp?user="+user+"&monto="+monto+"&pnr="+pnr;
            }
        </script>
    </head>
    <body>
        <form name="form1" action="regresoPago.jsp" method="post">
        <input type="hidden" value="<%=user%>" name="user"/>
        <input type="hidden" value="<%=pnr%>" name="pnr"/>
        <input type="hidden" value="<%=monto%>" name="monto"/>
         <input type="hidden" value="<%=idbitacora%>" name="bitacora"/>
        <center>
            <table>
                <tr>
                    <td align="center" class="biggger">
                        Pago con Tarjeta <br>American Express
                    </td>
                </tr>
                <tr>
                    <td class="big">
                        Tarjeta:
                    </td>
                </tr>
                <tr>
                    <td class="notoobig">
                        **** ****** <span id="card"><%=lastCard%></span> 
                    </td>
                </tr>
                <tr>
                    <td align="center" class="biggger">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td class="big">
                        Monto:
                    </td>
                </tr>
                <tr>
                    <td class="notoobig">
                        <span name="monto"><%=montoStr%></span> 
                    </td>
                </tr>
                <tr>
                    <td align="center" class="biggger">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td class="big">
                        Reservaci&oacute;n:
                    </td>
                </tr>
                <tr>
                    <td class="notoobig">
                        <span name="pnr"><%=pnr%></span> 
                    </td>
                </tr>
                <tr>
                    <td align="center" class="biggger">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td class="big">
                        Vigencia:
                    </td>
                </tr>
                <tr>
                    <td class="styled-select">
                        <SELECT NAME="month" >
                            <OPTION  value="01">1</OPTION>
                            <OPTION  value="02">2</OPTION>
                            <OPTION  value="03">3</OPTION>
                            <OPTION  value="04">4</OPTION>
                            <OPTION  value="05">5</OPTION>
                            <OPTION  value="06">6</OPTION>
                            <OPTION  value="07">7</OPTION>
                            <OPTION  value="08">8</OPTION>
                            <OPTION  value="09">9</OPTION>
                            <OPTION  value="10">10</OPTION>
                            <OPTION  value="11">11</OPTION>
                            <OPTION  value="12">12</OPTION>
                        </SELECT>                     
                    </td>
                </tr>
                <tr>
                    <td class="styled-select">
                        <SELECT NAME="year"  class="styled-select">
                            <%
                                java.util.Calendar C= java.util.Calendar.getInstance();
                                int anio=C.get(java.util.Calendar.YEAR);
                                out.println("<OPTION selected>"+anio+"</OPTION>");
                                anio++;
                                for(int i=1;i<15;i++)
                                {
                                    out.println("<OPTION>"+anio+"</OPTION>");
                                    anio++;
                                }
                            %>                
                        </SELECT> 
                    </td>
                </tr>
                <tr>
                    <td align="center" class="biggger">
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td class="big">
                        CVV2:
                    </td>
                </tr>
                <tr>
                    <td class="notoobig">
                        <input type="text" name="cid" id="cid" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="big">
                        <input type="submit" value="Pagar" />
                    </td>
                </tr>
            </table>
        </center>
    </form>
    </body>
</html>
