<%-- 
    Document   : regresoPago
    Created on : 04-mar-2013, 17:58:20
    Author     : Salvador
--%>
<%@page import="com.addcel.common.TransaccionManager"%>
<%@page import="java.util.Date"%>
<%@page import="com.addcel.interjet.amex.AmexResponse"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.addcel.interjet.TransactionControl"%>
<%@page import="com.addcel.common.MCUser"%>
<%@page import="com.addcel.common.BitacoraDTO"%>
<%@page import="com.addcel.common.HttpRequest"%>
<%
    String user="";
    String monto="";
    String pnr="";
    String month="";
    String year="";
    String vigencia="";
    String cvv2="";
    
    String codigoRespuesta="";
    String numRespuesta = "";
    String idbitacora="";
    System.out.println("regresoPago.jsp:Init");
       
    if(request.getParameter("user")!=null)
    {
        user = request.getParameter("user").toString();
    }
    if(request.getParameter("monto")!=null)
    {
        monto = request.getParameter("monto").toString();
    }
    
    if(request.getParameter("pnr")!=null)
    {
        pnr = request.getParameter("pnr").toString();
    }

    if(request.getParameter("month")!=null)
    {
        month = request.getParameter("month").toString();
    }
    if(request.getParameter("year")!=null)
    {
        year = request.getParameter("year").toString();
    }
    
    if(request.getParameter("cid")!=null)
    {
        cvv2 = request.getParameter("cid").toString();
    }
    
    if(request.getParameter("bitacora")!=null)
    {
        idbitacora = request.getParameter("bitacora").toString();
    }

    System.out.println("regresoPago.jsp:2");
       
    MCUser mcUser = null;
    mcUser = TransactionControl.getData(user);
    String tdc = mcUser.getUserTdc();
    year = year.trim().substring(2,4);
    vigencia = month + year;
    
    System.out.println("regresoPago.jsp:3");

    //tdc = "376668938502036";
    //vigencia = "1016";    
    //cvv2 = "5432";
            
    HttpRequest req = new HttpRequest();    
    String sRequest = "http://localhost:8080/AmexWeb/AmexAuth?tarjeta=" + tdc.trim() + "&vigencia="+vigencia.trim()+"&monto=" + monto.replace(".", "%2E").trim() +"&cid="+ cvv2.trim() +"&producto=INTERJET";
    //String sRequest = "http://mobilecard.mx:8080/AmexWeb/AmexAuthorization?tarjeta=376705645901001&vigencia=1114&monto=30%2E0&cid=9999&producto=INTERJET";
    
    System.out.println("regresoPago.jsp:"+sRequest);
    
    String respuesta = req.httpResponse(sRequest, "");
    
    System.out.println("regresoPago.jsp:Respuesta:"+respuesta);
    
    Gson gson = new Gson();
    
    AmexResponse amexResponse = (AmexResponse)gson.fromJson(respuesta, AmexResponse.class);    
   
    if(amexResponse.getApproval()!=null && !amexResponse.getApproval().equals("") && !amexResponse.getApproval().equals("0"))
    //if(true)
    {
        numRespuesta = amexResponse.getTransaction();
        codigoRespuesta = amexResponse.getDsc();
        
        //pnr = req.httpResponse("http://addcelapp.com:12345/getTransactionByPNR.aspx?transaccion="+pnr,"");
        pnr = new TransaccionManager().getTransactionByPNR(pnr);
        pnr = pnr.trim();
        
        BitacoraDTO bit=new BitacoraDTO(idbitacora, new Date(), new Date(), "Pago reservación INTERJET exitosa(prueba)", amexResponse.getTransaction(), "0");
                                    
        try
        {
            System.out.println("Intenta Commit.."+idbitacora);
            //respuesta = req.httpResponse("http://addcelapp.com:12345/setCommit.aspx?transaccion="+amexResponse.getTransaction()+
              //      "&pnr="+pnr+"&auth="+amexResponse.getTransaction()+"&num="+tdc,"");
            respuesta = new TransaccionManager().setCommit(amexResponse.getTransaction(), pnr, amexResponse.getTransaction(), tdc);
            //respuesta = req.httpResponse("http://addcelapp.com:12345/setCommit.aspx?transaccion="+emOrderId+"&pnr="+pnr+"&auth="+emAuth+"&num="+num,"");
            bit.setBitStatus("1");
            TransactionControl.actualizaBitacora(bit);
            System.out.println("Fin de la transaccion: "+idbitacora);
            
        }
        catch(Exception ex)
        {
            sRequest = "http://localhost:8080/AmexWeb/AmexRev?transaccion=" + amexResponse.getTransaction();
            req.httpResponse(sRequest,"");
            bit.setBitConcepto("Pago reservación INTERJET con error (REVERSO AMEX). ");
            TransactionControl.actualizaBitacora(bit);
        }
    }
    else
    {
        if(amexResponse.getError()!=null && !amexResponse.getError().equals("") && !amexResponse.getError().equals("0"))
        {
            numRespuesta = amexResponse.getError();
            codigoRespuesta = amexResponse.getErrorDsc();
        }
        else
        {
            numRespuesta = amexResponse.getCode();
            codigoRespuesta = amexResponse.getDsc();
        }
    }
    
            
    
    /*
    if(amexResponse.getCode().equals("100"))
        codigoRespuesta = amexResponse.getTransaction().toString();
    else if(amexResponse.getError()!=null && !amexResponse.getError().equals(""))
        codigoRespuesta = amexResponse.getError();
 * */
%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Pago Interjet AMEX</title>
        <meta name="HandheldFriendly" content="true">
        <LINK href="css/interjet.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <center>
            <table>
                <tr>
                    <td align="center">
                        <h3>Respuesta de Pago</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        Codigo de Autorización :
                    </td>
                </tr>
                <tr>
                    <td>
                        <span id="res"><%=numRespuesta %></span> 
                    </td>
                </tr>
                <tr>
                    <td>
                        Respuesta:
                    </td>
                </tr>
                <tr>
                    <td>
                        <span id="monto"><%=codigoRespuesta%></span> 
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
