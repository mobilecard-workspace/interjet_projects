<%-- 
    Document   : index
    Created on : 26-nov-2012, 22:14:53
    Author     : Salvador
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html version="-//W3C//DTD HTML 4.01 Transitional//EN">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Pago Interjet por Referencia</title>
        <meta name="HandheldFriendly" content="true">
        <LINK href="css/interjet.css" rel="stylesheet" type="text/css">
        <script src="js/interjet.js"></script>
        <script type="text/javascript" src="http://www.json.org/json2.js"></script>
        <script type="text/javascript">
            var retVar;
            var retVarIdn;
            function revisar()
            {
                overlay();
                alert('Envia');
                var pnr = document.getElementById("txtPNR").value;
                document.getElementById("ifReservacion").src="ajax/ajaxPNR.jsp?pnr="+pnr;                
            }
            function recibe()
            {
                alert('recibe');
                overlay();   
                if(retVar  && retVar.Monto)
                {
                    document.getElementById("txtMonto").value = "$ "+retVar.Monto;
                }
                
            }

            function enviar()
            {
                overlay();
                var pnr = document.getElementById("txtPNR").value;
                var monto = document.getElementById("txtMonto").value;
                var nMonto = 0;
                monto = monto.replace("$","");
                monto = trim(monto);
                nMonto = parseFloat(monto);
                nMonto = nMonto + 25;
                monto = nMonto + "";
                
                
                var aMonto = monto.split(".");
                if(aMonto[1])
                {
                    if(aMonto[1].length>2)
                    {
                        aMonto[1] = aMonto[1].substr(0, 2);                
                        monto = aMonto[0] + "." + aMonto[1];
                    }
                    else if(aMonto[1].length==2)
                    {
                        monto = aMonto[0] + "." + aMonto[1];
                    }
                    else
                        monto = aMonto[0] + ".00";
                }
                
                monto = monto.replace(".","%2E");
                //alert(monto);
                document.getElementById("ifTransaccion").src="ajax/ajaxTransaccion.jsp?pnr="+pnr+"&monto="+monto;                
                
            }


            function submit()
            {
                overlay();
                var monto = document.getElementById("txtMonto").value;
                monto = monto.replace("$","");
                monto = trim(monto);
                monto = monto.replace(".","%2E");
                //alert(retVarIdn.id + " " + monto);
                document.location.href = "http://mobilecard.mx:8080/ProCom/comercio_fin.jsp?user=sburgara&referencia="+retVarIdn.id+"&monto="+monto;
            }
            
            
            
            function trim (myString)
            {
                return myString.replace(/^\s+/g,'').replace(/\s+$/g,'')
            }            
        </script>
        
    </head>
    <body>
        <!-- Loader -->
        <div id="overlay">
            <div>
                <img src="img/loader.gif"/>
            </div>
        </div>        
        <!-- -->
        <iframe id="ifReservacion" src="" width="600" height="400" style="display: inline"/></iframe>
        <iframe id="ifTransaccion" src="" width="600" height="400" style="display: none"/></iframe>
        <center>
            <table>
                <tr>
                    <td><img src="img/mobileCard.PNG" width="80px"></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="center"><img src="img/interjet.PNG" width="80px"> </td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><img width="100px" src="img/3dsecurelogos.png"></td>
                </tr>
            </table>
            <span style="font-size: 14px;">Bienvenido al pago de su Reservación Interjet por medio de MobileCard 3D Secure</span><br><br>
            <table>
                <tr><td align="left">1.-</td><td align="left">Escribe tu reservaci&oacute;n</td></tr>
                <tr><td align="left">2.-</td><td align="left">Presiona "Validar"</td></tr>
                <tr><td align="left">3.-</td><td align="left">Al mostrar el monto en el campo presiona "Enviar"</td></tr>
                <tr><td align="left">4.-</td><td align="left">Llena los datos de tu tarjeta y codigo de validación</td></tr>
                <tr><td align="left">5.-</td><td align="left">Sigue las instrucciones te tu banco para realizar el pago</td></tr>
                <tr><td align="left">6.-</td><td align="left">Verifica que tengas número de autorización de tu pago</td></tr>
            </table><br/>
            Por la operación se cobra una comisi&oacute;n de $25.00 adicionales a tu boleto 
            Por favor proporcione la siguiente informacion:
        </center>
        <BR>
        <BR>
        <center>
            <table border="0">
                <tbody>
                    <tr>
                        <td>
                            Reservaci&oacute;n:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <INPUT TYPE="TEXT" ID="txtPNR" NAME="txtPNR" SIZE="40,1" MAXLENGTH="30" VALUE="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <INPUT TYPE="Button" VALUE="Revisar" NAME="btnEnviar"  VALUE="Enviar" onclick="revisar()">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Monto:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <INPUT TYPE="TEXT" NAME="txtMonto" id="txtMonto"  MAXLENGTH="19" SIZE="40,1" VALUE="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <INPUT TYPE="Button" VALUE="Enviar" NAME="btnEnviar"  VALUE="Enviar" onclick="enviar()">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="legal.html"/><b>Legal</b></a>
                        </td>
                    </tr>

            </TABLE>
        </center>
    </body>
</html>
