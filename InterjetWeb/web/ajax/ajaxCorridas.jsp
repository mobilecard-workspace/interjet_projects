<%@page import="java.util.Date"%>
<%@page import="com.addcel.etn.CorridaDto"%>
<%@page import="com.addcel.etn.CorridaPeticionDto"%>
<%@page import="com.google.gson.Gson" %>
<%@page import="com.google.gson.JsonObject" %>
<%@page import="com.addcel.common.SimpleMessage" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.List" %>
<%@page import="com.addcel.etn.DestinoDto" %>
<%
            String origen = (request.getParameter("origen")==null?"":request.getParameter("origen"));
            String destino = (request.getParameter("destino")==null?"":request.getParameter("destino"));
            String empresa = (request.getParameter("empresa")==null?"":request.getParameter("empresa"));
            String fecha = (request.getParameter("fecha")==null?"":request.getParameter("fecha"));
            String adulto = (request.getParameter("adultos")==null?"":request.getParameter("adultos"));
            String estudiante = (request.getParameter("estudiantes")==null?"":request.getParameter("estudiantes"));
            String insen = (request.getParameter("insen")==null?"":request.getParameter("insen"));
            String maestro = (request.getParameter("maestros")==null?"":request.getParameter("maestros"));
            String ninio = (request.getParameter("ninios")==null?"":request.getParameter("ninios"));
            String paisano = (request.getParameter("paisano")==null?"":request.getParameter("paisano"));
            String tipo = (request.getParameter("tipo")==null?"":request.getParameter("tipo"));
            
            session.setAttribute("adulto", adulto);
            session.setAttribute("estudiante", estudiante);
            session.setAttribute("insen", insen);
            session.setAttribute("maestro", maestro);
            session.setAttribute("ninio", ninio);
            session.setAttribute("paisano", paisano);
            
            
            Double numAdulto = new Double(adulto);
            Double numEstudiante = new Double(estudiante);
            Double numInsen = new Double(insen);
            Double numMaestro = new Double(maestro);
            Double numNinio = new Double(ninio);
            Double numPaisano = new Double(paisano);
            
            //Petici�n Corrida
            CorridaPeticionDto peticionCorrida = new CorridaPeticionDto();
            peticionCorrida.setOrigen(origen);
            peticionCorrida.setDestino(destino);
            peticionCorrida.setEmpresa(empresa);
            peticionCorrida.setFecha(fecha);
            peticionCorrida.setNumAdulto(numAdulto);
            peticionCorrida.setNumEstudiante(numEstudiante);
            peticionCorrida.setNumInsen(numInsen);
            peticionCorrida.setNumMaestro(numMaestro);
            peticionCorrida.setNumNino(numNinio);
            peticionCorrida.setNumPaisano(numPaisano);
            
            //regreso de corrida
            List corridas = new ArrayList();
            
            CorridaDto corrida;
            
            corrida = new CorridaDto();
            corrida.setAnden("A");
            corrida.setCorrida("Origen-Destino");
            corrida.setCupoAutobus(10);
            corrida.setCveCtl("0");
            corrida.setCveDestino(destino);
            corrida.setDestino("Destino");
            corrida.setDispEstudiante(2);
            corrida.setDispGeneral(10);
            corrida.setDispInsen(3);
            corrida.setDispMaestro(2);
            corrida.setDispNinos(2);
            corrida.setDispPaisano(3);
            corrida.setDispPromo(1);
            corrida.setEmpresaCorrida("ETN");
            corrida.setErrorCadena("");
            corrida.setErrorNumero(0);
            corrida.setFechaInicial((new Date()));
            corrida.setFechaLlegada((new Date()));
            corrida.setHoraCorrida("9:00");
            corrida.setTarifa(1000.00f);
            corrida.setTarifaAdulto(1000.00f);
            corrida.setTarifaEstudiante(900.00f);
            corrida.setTarifaInsen(800f);
            corrida.setTarifaMaestro(800f);
            corrida.setTarifaNino(700f);
            corrida.setTarifaPromo(500f);            
            corridas.add(corrida);
            
            corrida = new CorridaDto();
            corrida.setAnden("B");
            corrida.setCorrida("Origen-Destino");
            corrida.setCupoAutobus(10);
            corrida.setCveCtl("1");
            corrida.setCveDestino(destino);
            corrida.setDestino("Destino");
            corrida.setDispEstudiante(2);
            corrida.setDispGeneral(10);
            corrida.setDispInsen(3);
            corrida.setDispMaestro(2);
            corrida.setDispNinos(2);
            corrida.setDispPaisano(3);
            corrida.setDispPromo(1);
            corrida.setEmpresaCorrida("ETN");
            corrida.setErrorCadena("");
            corrida.setErrorNumero(0);
            corrida.setFechaInicial((new Date()));
            corrida.setFechaLlegada((new Date()));
            corrida.setHoraCorrida("10:00");
            corrida.setTarifa(1000.00f);
            corrida.setTarifaAdulto(1000.00f);
            corrida.setTarifaEstudiante(900.00f);
            corrida.setTarifaInsen(800f);
            corrida.setTarifaMaestro(800f);
            corrida.setTarifaNino(700f);
            corrida.setTarifaPromo(500f);            
            corridas.add(corrida);
            
            corrida = new CorridaDto();
            corrida.setAnden("C");
            corrida.setCorrida("Origen-Destino");
            corrida.setCupoAutobus(10);
            corrida.setCveCtl("2");
            corrida.setCveDestino(destino);
            corrida.setDestino("Destino");
            corrida.setDispEstudiante(2);
            corrida.setDispGeneral(10);
            corrida.setDispInsen(3);
            corrida.setDispMaestro(2);
            corrida.setDispNinos(2);
            corrida.setDispPaisano(3);
            corrida.setDispPromo(1);
            corrida.setEmpresaCorrida("ETN");
            corrida.setErrorCadena("");
            corrida.setErrorNumero(0);
            corrida.setFechaInicial((new Date()));
            corrida.setFechaLlegada((new Date()));
            corrida.setHoraCorrida("11:00");
            corrida.setTarifa(1000.00f);
            corrida.setTarifaAdulto(1000.00f);
            corrida.setTarifaEstudiante(900.00f);
            corrida.setTarifaInsen(800f);
            corrida.setTarifaMaestro(800f);
            corrida.setTarifaNino(700f);
            corrida.setTarifaPromo(500f);            
            corridas.add(corrida);
            
            session.setAttribute("corridas", corridas);
            Gson gson = new Gson();
            JsonObject jsonObject = new JsonObject();
            
            
            //Cuando funcione hazlo con la linea de abajo
            //SimpleMessage ret = new SimpleMessage("login", (TransactionControl.login(user, pwd)+"") );
            
            jsonObject.add("respuesta", gson.toJsonTree(corridas));
            //System.out.println(jsonObject.toString());

%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <script type="text/javascript">
            var regreso = <%=jsonObject.toString()%>;
            //window.parent.overlay();          
            window.parent.retVarCorrida = regreso;
            window.parent.recibeCorrida();
        </script>
    </head>
    <body>
        <%=jsonObject.toString()%>
    </body>
</html>
