<%@page import="com.addcel.etn.CorridaDtoImpl"%>
<%@page import="com.addcel.etn.CorridaDto"%>
<%@page import="com.addcel.etn.DestinoPeticionDto"%>
<%@page import="com.google.gson.Gson" %>
<%@page import="com.google.gson.JsonObject" %>
<%@page import="com.addcel.common.SimpleMessage" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.List" %>
<%@page import="com.addcel.etn.DestinoDto" %>
<%
    CorridaDto corrida = (CorridaDto)session.getAttribute("corrida");
    String cveCtl="-";
    double costo = 0.00d;
    int adulto=0;
    int ninio=0;
    int insen=0;
    int maestro=0;
    int estudiante=0;
    int paisano=0;
    
    CorridaDtoImpl corridaImpl = new CorridaDtoImpl(corrida);
    
    if(corrida!=null)
    {
        cveCtl = corrida.getCveCtl();
        costo = corrida.getTarifa();        
        
        
        adulto = Integer.parseInt(session.getAttribute("adulto").toString());
        ninio = Integer.parseInt(session.getAttribute("ninio").toString());
        insen = Integer.parseInt(session.getAttribute("insen").toString());
        maestro = Integer.parseInt(session.getAttribute("maestro").toString());
        estudiante = Integer.parseInt(session.getAttribute("estudiante").toString());
        paisano = Integer.parseInt(session.getAttribute("paisano").toString());
        
        
        
        corridaImpl.setAdulto(adulto);
        corridaImpl.setNinio(ninio);
        corridaImpl.setInsen(insen);
        corridaImpl.setMaestro(maestro);
        corridaImpl.setEstudiante(estudiante);
        corridaImpl.setPaisano(paisano);
        corridaImpl.setCosto(costo);
    }


    
    Gson gson = new Gson();
    JsonObject jsonObject = new JsonObject();
    
    session.setAttribute("corridaImpl", corridaImpl);


    //Cuando funcione hazlo con la linea de abajo
    //SimpleMessage ret = new SimpleMessage("login", (TransactionControl.login(user, pwd)+"") );

    jsonObject.add("respuesta", gson.toJsonTree(corridaImpl));
    //System.out.println(jsonObject.toString());

%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript">
            var regreso = <%=jsonObject.toString()%>;
            
            window.parent.numAdultos = <%=adulto%>;
            window.parent.numNinios = <%=ninio%>;
            window.parent.numInsen = <%=insen%>;
            window.parent.numMaestros = <%=maestro%>;
            window.parent.numEstudiantes = <%=estudiante%>;
            window.parent.numPaisano = <%=paisano %>;
            
            window.parent.overlay();          
            window.parent.retVar = regreso;
            window.parent.generarPaxes();
        </script>
    </head>
    <body>
        <%=jsonObject.toString()%>
    </body>
</html>
