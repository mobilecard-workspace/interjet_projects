<%@page import="com.addcel.common.TransaccionManager"%>
<%@page import="com.addcel.interjet.TransactionControl"%>
<%@page import="com.addcel.common.TransaccionDTO"%>
<%@page import="com.addcel.common.HttpRequest"%>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="com.google.gson.JsonObject" %>
<%@ page import="com.addcel.common.SimpleMessage" %>
<%
    String pnr = (request.getParameter("pnr")==null?"":request.getParameter("pnr"));
    String monto = (request.getParameter("monto")==null?"":request.getParameter("monto"));
    pnr = pnr.trim();

    monto = monto.replaceAll("\\.", "%2E1");

    //String jsonResponse = req.httpResponse("http://www.addcelapp.com:12345/getInterjetTransaction.aspx?user=sburgara&pnr="+pnr+"&monto="+monto,"");
    String jsonResponse = new TransaccionManager().getInterjetTransaction("sburgara", pnr, monto);
    if(jsonResponse!= null && !jsonResponse.equals("") && jsonResponse.length() > 1)
    {
        jsonResponse=jsonResponse.substring(2, jsonResponse.length());
    }
%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <script type="text/javascript">
            var regreso = <%=jsonResponse%>;
            window.parent.retVarIdn = regreso;
            window.parent.submit();
        </script>
    </head>
    <body>
        <%=jsonResponse%>
    </body>
</html>
