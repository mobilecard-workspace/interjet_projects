/**
 * LoyaltyFilter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations;

public class LoyaltyFilter implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected LoyaltyFilter(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _MonetaryOnly = "MonetaryOnly";
    public static final java.lang.String _PointsOnly = "PointsOnly";
    public static final java.lang.String _PointsAndMonetary = "PointsAndMonetary";
    public static final java.lang.String _PreserveCurrent = "PreserveCurrent";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final LoyaltyFilter MonetaryOnly = new LoyaltyFilter(_MonetaryOnly);
    public static final LoyaltyFilter PointsOnly = new LoyaltyFilter(_PointsOnly);
    public static final LoyaltyFilter PointsAndMonetary = new LoyaltyFilter(_PointsAndMonetary);
    public static final LoyaltyFilter PreserveCurrent = new LoyaltyFilter(_PreserveCurrent);
    public static final LoyaltyFilter Unmapped = new LoyaltyFilter(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static LoyaltyFilter fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        LoyaltyFilter enumeration = (LoyaltyFilter)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static LoyaltyFilter fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LoyaltyFilter.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LoyaltyFilter"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
