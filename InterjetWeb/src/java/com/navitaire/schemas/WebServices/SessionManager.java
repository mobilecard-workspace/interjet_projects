/**
 * SessionManager.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices;

public interface SessionManager extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpBinding_ISessionManagerAddress();

    public com.navitaire.schemas.WebServices.ISessionManager getBasicHttpBinding_ISessionManager() throws javax.xml.rpc.ServiceException;

    public com.navitaire.schemas.WebServices.ISessionManager getBasicHttpBinding_ISessionManager(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
