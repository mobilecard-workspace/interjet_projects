/**
 * ISessionManager.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices;

public interface ISessionManager extends java.rmi.Remote {
    public void changePassword(com.navitaire.schemas.WebServices.ServiceContracts.SessionService.ChangePasswordRequest parameters) throws java.rmi.RemoteException;
    public com.navitaire.schemas.WebServices.LogonResponse logon(com.navitaire.schemas.WebServices.ServiceContracts.SessionService.LogonRequest parameters) throws java.rmi.RemoteException;
    public void logout(com.navitaire.schemas.WebServices.ServiceContracts.SessionService.LogoutRequest parameters) throws java.rmi.RemoteException;
    public com.navitaire.schemas.WebServices.ServiceContracts.SessionService.TransferSessionResponse transferSession(com.navitaire.schemas.WebServices.ServiceContracts.SessionService.TransferSessionRequest parameters) throws java.rmi.RemoteException;
}
