/**
 * LogonRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.SessionService;

public class LogonRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Session.LogonRequestData logonRequestData;

    public LogonRequest() {
    }

    public LogonRequest(
           com.navitaire.schemas.WebServices.DataContracts.Session.LogonRequestData logonRequestData) {
           this.logonRequestData = logonRequestData;
    }


    /**
     * Gets the logonRequestData value for this LogonRequest.
     * 
     * @return logonRequestData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Session.LogonRequestData getLogonRequestData() {
        return logonRequestData;
    }


    /**
     * Sets the logonRequestData value for this LogonRequest.
     * 
     * @param logonRequestData
     */
    public void setLogonRequestData(com.navitaire.schemas.WebServices.DataContracts.Session.LogonRequestData logonRequestData) {
        this.logonRequestData = logonRequestData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LogonRequest)) return false;
        LogonRequest other = (LogonRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.logonRequestData==null && other.getLogonRequestData()==null) || 
             (this.logonRequestData!=null &&
              this.logonRequestData.equals(other.getLogonRequestData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLogonRequestData() != null) {
            _hashCode += getLogonRequestData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LogonRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", ">LogonRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logonRequestData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", "logonRequestData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "LogonRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
