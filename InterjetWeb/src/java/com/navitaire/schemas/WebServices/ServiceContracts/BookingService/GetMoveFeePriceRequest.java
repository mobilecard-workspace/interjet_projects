/**
 * GetMoveFeePriceRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class GetMoveFeePriceRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceRequest moveFeePriceRequest;

    public GetMoveFeePriceRequest() {
    }

    public GetMoveFeePriceRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceRequest moveFeePriceRequest) {
           this.moveFeePriceRequest = moveFeePriceRequest;
    }


    /**
     * Gets the moveFeePriceRequest value for this GetMoveFeePriceRequest.
     * 
     * @return moveFeePriceRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceRequest getMoveFeePriceRequest() {
        return moveFeePriceRequest;
    }


    /**
     * Sets the moveFeePriceRequest value for this GetMoveFeePriceRequest.
     * 
     * @param moveFeePriceRequest
     */
    public void setMoveFeePriceRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceRequest moveFeePriceRequest) {
        this.moveFeePriceRequest = moveFeePriceRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMoveFeePriceRequest)) return false;
        GetMoveFeePriceRequest other = (GetMoveFeePriceRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.moveFeePriceRequest==null && other.getMoveFeePriceRequest()==null) || 
             (this.moveFeePriceRequest!=null &&
              this.moveFeePriceRequest.equals(other.getMoveFeePriceRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMoveFeePriceRequest() != null) {
            _hashCode += getMoveFeePriceRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMoveFeePriceRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveFeePriceRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moveFeePriceRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
