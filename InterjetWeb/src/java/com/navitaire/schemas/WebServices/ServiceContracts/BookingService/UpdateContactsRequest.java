/**
 * UpdateContactsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class UpdateContactsRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateContactsRequestData updateContactsRequestData;

    public UpdateContactsRequest() {
    }

    public UpdateContactsRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateContactsRequestData updateContactsRequestData) {
           this.updateContactsRequestData = updateContactsRequestData;
    }


    /**
     * Gets the updateContactsRequestData value for this UpdateContactsRequest.
     * 
     * @return updateContactsRequestData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateContactsRequestData getUpdateContactsRequestData() {
        return updateContactsRequestData;
    }


    /**
     * Sets the updateContactsRequestData value for this UpdateContactsRequest.
     * 
     * @param updateContactsRequestData
     */
    public void setUpdateContactsRequestData(com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateContactsRequestData updateContactsRequestData) {
        this.updateContactsRequestData = updateContactsRequestData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateContactsRequest)) return false;
        UpdateContactsRequest other = (UpdateContactsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.updateContactsRequestData==null && other.getUpdateContactsRequestData()==null) || 
             (this.updateContactsRequestData!=null &&
              this.updateContactsRequestData.equals(other.getUpdateContactsRequestData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUpdateContactsRequestData() != null) {
            _hashCode += getUpdateContactsRequestData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateContactsRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateContactsRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateContactsRequestData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "updateContactsRequestData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateContactsRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
