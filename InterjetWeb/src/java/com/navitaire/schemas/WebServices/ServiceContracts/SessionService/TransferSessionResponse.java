/**
 * TransferSessionResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.SessionService;

public class TransferSessionResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Session.TransferSessionResponseData transferSessionResponseData;

    public TransferSessionResponse() {
    }

    public TransferSessionResponse(
           com.navitaire.schemas.WebServices.DataContracts.Session.TransferSessionResponseData transferSessionResponseData) {
           this.transferSessionResponseData = transferSessionResponseData;
    }


    /**
     * Gets the transferSessionResponseData value for this TransferSessionResponse.
     * 
     * @return transferSessionResponseData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Session.TransferSessionResponseData getTransferSessionResponseData() {
        return transferSessionResponseData;
    }


    /**
     * Sets the transferSessionResponseData value for this TransferSessionResponse.
     * 
     * @param transferSessionResponseData
     */
    public void setTransferSessionResponseData(com.navitaire.schemas.WebServices.DataContracts.Session.TransferSessionResponseData transferSessionResponseData) {
        this.transferSessionResponseData = transferSessionResponseData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransferSessionResponse)) return false;
        TransferSessionResponse other = (TransferSessionResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.transferSessionResponseData==null && other.getTransferSessionResponseData()==null) || 
             (this.transferSessionResponseData!=null &&
              this.transferSessionResponseData.equals(other.getTransferSessionResponseData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTransferSessionResponseData() != null) {
            _hashCode += getTransferSessionResponseData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransferSessionResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", ">TransferSessionResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transferSessionResponseData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", "TransferSessionResponseData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "TransferSessionResponseData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
