/**
 * AddBookingCommentsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class AddBookingCommentsRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.AddBookingCommentsRequestData addBookingCommentsReqData;

    public AddBookingCommentsRequest() {
    }

    public AddBookingCommentsRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.AddBookingCommentsRequestData addBookingCommentsReqData) {
           this.addBookingCommentsReqData = addBookingCommentsReqData;
    }


    /**
     * Gets the addBookingCommentsReqData value for this AddBookingCommentsRequest.
     * 
     * @return addBookingCommentsReqData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.AddBookingCommentsRequestData getAddBookingCommentsReqData() {
        return addBookingCommentsReqData;
    }


    /**
     * Sets the addBookingCommentsReqData value for this AddBookingCommentsRequest.
     * 
     * @param addBookingCommentsReqData
     */
    public void setAddBookingCommentsReqData(com.navitaire.schemas.WebServices.DataContracts.Booking.AddBookingCommentsRequestData addBookingCommentsReqData) {
        this.addBookingCommentsReqData = addBookingCommentsReqData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddBookingCommentsRequest)) return false;
        AddBookingCommentsRequest other = (AddBookingCommentsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addBookingCommentsReqData==null && other.getAddBookingCommentsReqData()==null) || 
             (this.addBookingCommentsReqData!=null &&
              this.addBookingCommentsReqData.equals(other.getAddBookingCommentsReqData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddBookingCommentsReqData() != null) {
            _hashCode += getAddBookingCommentsReqData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddBookingCommentsRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddBookingCommentsRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addBookingCommentsReqData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AddBookingCommentsReqData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AddBookingCommentsRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
