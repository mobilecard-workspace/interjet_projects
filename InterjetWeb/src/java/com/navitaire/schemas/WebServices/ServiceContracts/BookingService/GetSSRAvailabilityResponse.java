/**
 * GetSSRAvailabilityResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class GetSSRAvailabilityResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.SSRAvailabilityResponse SSRAvailabilityResponse;

    public GetSSRAvailabilityResponse() {
    }

    public GetSSRAvailabilityResponse(
           com.navitaire.schemas.WebServices.DataContracts.Booking.SSRAvailabilityResponse SSRAvailabilityResponse) {
           this.SSRAvailabilityResponse = SSRAvailabilityResponse;
    }


    /**
     * Gets the SSRAvailabilityResponse value for this GetSSRAvailabilityResponse.
     * 
     * @return SSRAvailabilityResponse
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SSRAvailabilityResponse getSSRAvailabilityResponse() {
        return SSRAvailabilityResponse;
    }


    /**
     * Sets the SSRAvailabilityResponse value for this GetSSRAvailabilityResponse.
     * 
     * @param SSRAvailabilityResponse
     */
    public void setSSRAvailabilityResponse(com.navitaire.schemas.WebServices.DataContracts.Booking.SSRAvailabilityResponse SSRAvailabilityResponse) {
        this.SSRAvailabilityResponse = SSRAvailabilityResponse;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSSRAvailabilityResponse)) return false;
        GetSSRAvailabilityResponse other = (GetSSRAvailabilityResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.SSRAvailabilityResponse==null && other.getSSRAvailabilityResponse()==null) || 
             (this.SSRAvailabilityResponse!=null &&
              this.SSRAvailabilityResponse.equals(other.getSSRAvailabilityResponse())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSSRAvailabilityResponse() != null) {
            _hashCode += getSSRAvailabilityResponse().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSSRAvailabilityResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSSRAvailabilityResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRAvailabilityResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRAvailabilityResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRAvailabilityResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
