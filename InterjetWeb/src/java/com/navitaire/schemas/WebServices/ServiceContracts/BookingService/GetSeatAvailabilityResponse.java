/**
 * GetSeatAvailabilityResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class GetSeatAvailabilityResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.SeatAvailabilityResponse seatAvailabilityResponse;

    public GetSeatAvailabilityResponse() {
    }

    public GetSeatAvailabilityResponse(
           com.navitaire.schemas.WebServices.DataContracts.Booking.SeatAvailabilityResponse seatAvailabilityResponse) {
           this.seatAvailabilityResponse = seatAvailabilityResponse;
    }


    /**
     * Gets the seatAvailabilityResponse value for this GetSeatAvailabilityResponse.
     * 
     * @return seatAvailabilityResponse
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SeatAvailabilityResponse getSeatAvailabilityResponse() {
        return seatAvailabilityResponse;
    }


    /**
     * Sets the seatAvailabilityResponse value for this GetSeatAvailabilityResponse.
     * 
     * @param seatAvailabilityResponse
     */
    public void setSeatAvailabilityResponse(com.navitaire.schemas.WebServices.DataContracts.Booking.SeatAvailabilityResponse seatAvailabilityResponse) {
        this.seatAvailabilityResponse = seatAvailabilityResponse;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSeatAvailabilityResponse)) return false;
        GetSeatAvailabilityResponse other = (GetSeatAvailabilityResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.seatAvailabilityResponse==null && other.getSeatAvailabilityResponse()==null) || 
             (this.seatAvailabilityResponse!=null &&
              this.seatAvailabilityResponse.equals(other.getSeatAvailabilityResponse())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSeatAvailabilityResponse() != null) {
            _hashCode += getSeatAvailabilityResponse().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSeatAvailabilityResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSeatAvailabilityResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatAvailabilityResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailabilityResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailabilityResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
