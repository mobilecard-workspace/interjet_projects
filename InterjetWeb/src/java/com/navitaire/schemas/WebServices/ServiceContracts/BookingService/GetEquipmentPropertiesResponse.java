/**
 * GetEquipmentPropertiesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class GetEquipmentPropertiesResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentListResponse equipmentListResponse;

    public GetEquipmentPropertiesResponse() {
    }

    public GetEquipmentPropertiesResponse(
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentListResponse equipmentListResponse) {
           this.equipmentListResponse = equipmentListResponse;
    }


    /**
     * Gets the equipmentListResponse value for this GetEquipmentPropertiesResponse.
     * 
     * @return equipmentListResponse
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentListResponse getEquipmentListResponse() {
        return equipmentListResponse;
    }


    /**
     * Sets the equipmentListResponse value for this GetEquipmentPropertiesResponse.
     * 
     * @param equipmentListResponse
     */
    public void setEquipmentListResponse(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentListResponse equipmentListResponse) {
        this.equipmentListResponse = equipmentListResponse;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEquipmentPropertiesResponse)) return false;
        GetEquipmentPropertiesResponse other = (GetEquipmentPropertiesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.equipmentListResponse==null && other.getEquipmentListResponse()==null) || 
             (this.equipmentListResponse!=null &&
              this.equipmentListResponse.equals(other.getEquipmentListResponse())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEquipmentListResponse() != null) {
            _hashCode += getEquipmentListResponse().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEquipmentPropertiesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetEquipmentPropertiesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentListResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentListResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentListResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
