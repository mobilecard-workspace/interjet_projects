/**
 * ScorePassengersRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class ScorePassengersRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScoresRequest passengerScoresRequest;

    public ScorePassengersRequest() {
    }

    public ScorePassengersRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScoresRequest passengerScoresRequest) {
           this.passengerScoresRequest = passengerScoresRequest;
    }


    /**
     * Gets the passengerScoresRequest value for this ScorePassengersRequest.
     * 
     * @return passengerScoresRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScoresRequest getPassengerScoresRequest() {
        return passengerScoresRequest;
    }


    /**
     * Sets the passengerScoresRequest value for this ScorePassengersRequest.
     * 
     * @param passengerScoresRequest
     */
    public void setPassengerScoresRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScoresRequest passengerScoresRequest) {
        this.passengerScoresRequest = passengerScoresRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScorePassengersRequest)) return false;
        ScorePassengersRequest other = (ScorePassengersRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.passengerScoresRequest==null && other.getPassengerScoresRequest()==null) || 
             (this.passengerScoresRequest!=null &&
              this.passengerScoresRequest.equals(other.getPassengerScoresRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPassengerScoresRequest() != null) {
            _hashCode += getPassengerScoresRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScorePassengersRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ScorePassengersRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerScoresRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScoresRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScoresRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
