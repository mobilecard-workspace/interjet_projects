/**
 * UpdatePassengersRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class UpdatePassengersRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.UpdatePassengersRequestData updatePassengersRequestData;

    public UpdatePassengersRequest() {
    }

    public UpdatePassengersRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.UpdatePassengersRequestData updatePassengersRequestData) {
           this.updatePassengersRequestData = updatePassengersRequestData;
    }


    /**
     * Gets the updatePassengersRequestData value for this UpdatePassengersRequest.
     * 
     * @return updatePassengersRequestData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.UpdatePassengersRequestData getUpdatePassengersRequestData() {
        return updatePassengersRequestData;
    }


    /**
     * Sets the updatePassengersRequestData value for this UpdatePassengersRequest.
     * 
     * @param updatePassengersRequestData
     */
    public void setUpdatePassengersRequestData(com.navitaire.schemas.WebServices.DataContracts.Booking.UpdatePassengersRequestData updatePassengersRequestData) {
        this.updatePassengersRequestData = updatePassengersRequestData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdatePassengersRequest)) return false;
        UpdatePassengersRequest other = (UpdatePassengersRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.updatePassengersRequestData==null && other.getUpdatePassengersRequestData()==null) || 
             (this.updatePassengersRequestData!=null &&
              this.updatePassengersRequestData.equals(other.getUpdatePassengersRequestData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUpdatePassengersRequestData() != null) {
            _hashCode += getUpdatePassengersRequestData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdatePassengersRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePassengersRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatePassengersRequestData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "updatePassengersRequestData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdatePassengersRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
