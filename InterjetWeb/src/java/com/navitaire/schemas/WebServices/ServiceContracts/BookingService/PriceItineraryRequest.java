/**
 * PriceItineraryRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class PriceItineraryRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.ItineraryPriceRequest itineraryPriceRequest;

    public PriceItineraryRequest() {
    }

    public PriceItineraryRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.ItineraryPriceRequest itineraryPriceRequest) {
           this.itineraryPriceRequest = itineraryPriceRequest;
    }


    /**
     * Gets the itineraryPriceRequest value for this PriceItineraryRequest.
     * 
     * @return itineraryPriceRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.ItineraryPriceRequest getItineraryPriceRequest() {
        return itineraryPriceRequest;
    }


    /**
     * Sets the itineraryPriceRequest value for this PriceItineraryRequest.
     * 
     * @param itineraryPriceRequest
     */
    public void setItineraryPriceRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.ItineraryPriceRequest itineraryPriceRequest) {
        this.itineraryPriceRequest = itineraryPriceRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PriceItineraryRequest)) return false;
        PriceItineraryRequest other = (PriceItineraryRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.itineraryPriceRequest==null && other.getItineraryPriceRequest()==null) || 
             (this.itineraryPriceRequest!=null &&
              this.itineraryPriceRequest.equals(other.getItineraryPriceRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getItineraryPriceRequest() != null) {
            _hashCode += getItineraryPriceRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PriceItineraryRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">PriceItineraryRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itineraryPriceRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "ItineraryPriceRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ItineraryPriceRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
