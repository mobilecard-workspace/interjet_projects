/**
 * GetBookingPaymentsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class GetBookingPaymentsRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingPaymentsRequestData getBookingPaymentsReqData;

    public GetBookingPaymentsRequest() {
    }

    public GetBookingPaymentsRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingPaymentsRequestData getBookingPaymentsReqData) {
           this.getBookingPaymentsReqData = getBookingPaymentsReqData;
    }


    /**
     * Gets the getBookingPaymentsReqData value for this GetBookingPaymentsRequest.
     * 
     * @return getBookingPaymentsReqData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingPaymentsRequestData getGetBookingPaymentsReqData() {
        return getBookingPaymentsReqData;
    }


    /**
     * Sets the getBookingPaymentsReqData value for this GetBookingPaymentsRequest.
     * 
     * @param getBookingPaymentsReqData
     */
    public void setGetBookingPaymentsReqData(com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingPaymentsRequestData getBookingPaymentsReqData) {
        this.getBookingPaymentsReqData = getBookingPaymentsReqData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBookingPaymentsRequest)) return false;
        GetBookingPaymentsRequest other = (GetBookingPaymentsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getBookingPaymentsReqData==null && other.getGetBookingPaymentsReqData()==null) || 
             (this.getBookingPaymentsReqData!=null &&
              this.getBookingPaymentsReqData.equals(other.getGetBookingPaymentsReqData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetBookingPaymentsReqData() != null) {
            _hashCode += getGetBookingPaymentsReqData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBookingPaymentsRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingPaymentsRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getBookingPaymentsReqData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingPaymentsReqData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingPaymentsRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
