/**
 * UpdatePriceRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class UpdatePriceRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.PriceRequestData priceRequestData;

    public UpdatePriceRequest() {
    }

    public UpdatePriceRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.PriceRequestData priceRequestData) {
           this.priceRequestData = priceRequestData;
    }


    /**
     * Gets the priceRequestData value for this UpdatePriceRequest.
     * 
     * @return priceRequestData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PriceRequestData getPriceRequestData() {
        return priceRequestData;
    }


    /**
     * Sets the priceRequestData value for this UpdatePriceRequest.
     * 
     * @param priceRequestData
     */
    public void setPriceRequestData(com.navitaire.schemas.WebServices.DataContracts.Booking.PriceRequestData priceRequestData) {
        this.priceRequestData = priceRequestData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdatePriceRequest)) return false;
        UpdatePriceRequest other = (UpdatePriceRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.priceRequestData==null && other.getPriceRequestData()==null) || 
             (this.priceRequestData!=null &&
              this.priceRequestData.equals(other.getPriceRequestData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPriceRequestData() != null) {
            _hashCode += getPriceRequestData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdatePriceRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePriceRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priceRequestData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "PriceRequestData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
