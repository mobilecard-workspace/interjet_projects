/**
 * GetMoveAvailabilityRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class GetMoveAvailabilityRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.MoveAvailabilityRequest moveAvailabilityRequest;

    public GetMoveAvailabilityRequest() {
    }

    public GetMoveAvailabilityRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.MoveAvailabilityRequest moveAvailabilityRequest) {
           this.moveAvailabilityRequest = moveAvailabilityRequest;
    }


    /**
     * Gets the moveAvailabilityRequest value for this GetMoveAvailabilityRequest.
     * 
     * @return moveAvailabilityRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.MoveAvailabilityRequest getMoveAvailabilityRequest() {
        return moveAvailabilityRequest;
    }


    /**
     * Sets the moveAvailabilityRequest value for this GetMoveAvailabilityRequest.
     * 
     * @param moveAvailabilityRequest
     */
    public void setMoveAvailabilityRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.MoveAvailabilityRequest moveAvailabilityRequest) {
        this.moveAvailabilityRequest = moveAvailabilityRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMoveAvailabilityRequest)) return false;
        GetMoveAvailabilityRequest other = (GetMoveAvailabilityRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.moveAvailabilityRequest==null && other.getMoveAvailabilityRequest()==null) || 
             (this.moveAvailabilityRequest!=null &&
              this.moveAvailabilityRequest.equals(other.getMoveAvailabilityRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMoveAvailabilityRequest() != null) {
            _hashCode += getMoveAvailabilityRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMoveAvailabilityRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveAvailabilityRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moveAvailabilityRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveAvailabilityRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveAvailabilityRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
