/**
 * TravelCommerceSellRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class TravelCommerceSellRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.TravelCommerceSellRequestData travelCommerceSellRequestData;

    public TravelCommerceSellRequest() {
    }

    public TravelCommerceSellRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.TravelCommerceSellRequestData travelCommerceSellRequestData) {
           this.travelCommerceSellRequestData = travelCommerceSellRequestData;
    }


    /**
     * Gets the travelCommerceSellRequestData value for this TravelCommerceSellRequest.
     * 
     * @return travelCommerceSellRequestData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.TravelCommerceSellRequestData getTravelCommerceSellRequestData() {
        return travelCommerceSellRequestData;
    }


    /**
     * Sets the travelCommerceSellRequestData value for this TravelCommerceSellRequest.
     * 
     * @param travelCommerceSellRequestData
     */
    public void setTravelCommerceSellRequestData(com.navitaire.schemas.WebServices.DataContracts.Booking.TravelCommerceSellRequestData travelCommerceSellRequestData) {
        this.travelCommerceSellRequestData = travelCommerceSellRequestData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TravelCommerceSellRequest)) return false;
        TravelCommerceSellRequest other = (TravelCommerceSellRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.travelCommerceSellRequestData==null && other.getTravelCommerceSellRequestData()==null) || 
             (this.travelCommerceSellRequestData!=null &&
              this.travelCommerceSellRequestData.equals(other.getTravelCommerceSellRequestData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTravelCommerceSellRequestData() != null) {
            _hashCode += getTravelCommerceSellRequestData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TravelCommerceSellRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceSellRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("travelCommerceSellRequestData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelCommerceSellRequestData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelCommerceSellRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
