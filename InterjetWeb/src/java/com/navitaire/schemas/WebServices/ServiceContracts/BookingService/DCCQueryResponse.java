/**
 * DCCQueryResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class DCCQueryResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.ValidationPayment DCCQueryPaymentResponse;

    public DCCQueryResponse() {
    }

    public DCCQueryResponse(
           com.navitaire.schemas.WebServices.DataContracts.Booking.ValidationPayment DCCQueryPaymentResponse) {
           this.DCCQueryPaymentResponse = DCCQueryPaymentResponse;
    }


    /**
     * Gets the DCCQueryPaymentResponse value for this DCCQueryResponse.
     * 
     * @return DCCQueryPaymentResponse
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.ValidationPayment getDCCQueryPaymentResponse() {
        return DCCQueryPaymentResponse;
    }


    /**
     * Sets the DCCQueryPaymentResponse value for this DCCQueryResponse.
     * 
     * @param DCCQueryPaymentResponse
     */
    public void setDCCQueryPaymentResponse(com.navitaire.schemas.WebServices.DataContracts.Booking.ValidationPayment DCCQueryPaymentResponse) {
        this.DCCQueryPaymentResponse = DCCQueryPaymentResponse;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DCCQueryResponse)) return false;
        DCCQueryResponse other = (DCCQueryResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DCCQueryPaymentResponse==null && other.getDCCQueryPaymentResponse()==null) || 
             (this.DCCQueryPaymentResponse!=null &&
              this.DCCQueryPaymentResponse.equals(other.getDCCQueryPaymentResponse())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDCCQueryPaymentResponse() != null) {
            _hashCode += getDCCQueryPaymentResponse().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DCCQueryResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DCCQueryResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DCCQueryPaymentResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DCCQueryPaymentResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationPayment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
