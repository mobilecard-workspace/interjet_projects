/**
 * TravelCommerceCancelServiceRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class TravelCommerceCancelServiceRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.TravelCommerce.ServiceCancelRequest serviceCancelRequest;

    public TravelCommerceCancelServiceRequest() {
    }

    public TravelCommerceCancelServiceRequest(
           com.navitaire.schemas.WebServices.DataContracts.TravelCommerce.ServiceCancelRequest serviceCancelRequest) {
           this.serviceCancelRequest = serviceCancelRequest;
    }


    /**
     * Gets the serviceCancelRequest value for this TravelCommerceCancelServiceRequest.
     * 
     * @return serviceCancelRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.TravelCommerce.ServiceCancelRequest getServiceCancelRequest() {
        return serviceCancelRequest;
    }


    /**
     * Sets the serviceCancelRequest value for this TravelCommerceCancelServiceRequest.
     * 
     * @param serviceCancelRequest
     */
    public void setServiceCancelRequest(com.navitaire.schemas.WebServices.DataContracts.TravelCommerce.ServiceCancelRequest serviceCancelRequest) {
        this.serviceCancelRequest = serviceCancelRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TravelCommerceCancelServiceRequest)) return false;
        TravelCommerceCancelServiceRequest other = (TravelCommerceCancelServiceRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.serviceCancelRequest==null && other.getServiceCancelRequest()==null) || 
             (this.serviceCancelRequest!=null &&
              this.serviceCancelRequest.equals(other.getServiceCancelRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getServiceCancelRequest() != null) {
            _hashCode += getServiceCancelRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TravelCommerceCancelServiceRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceCancelServiceRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceCancelRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/TravelCommerce", "ServiceCancelRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/TravelCommerce", "ServiceCancelRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
