/**
 * BookingCommitRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class BookingCommitRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingCommitRequestData bookingCommitRequestData;

    public BookingCommitRequest() {
    }

    public BookingCommitRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingCommitRequestData bookingCommitRequestData) {
           this.bookingCommitRequestData = bookingCommitRequestData;
    }


    /**
     * Gets the bookingCommitRequestData value for this BookingCommitRequest.
     * 
     * @return bookingCommitRequestData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingCommitRequestData getBookingCommitRequestData() {
        return bookingCommitRequestData;
    }


    /**
     * Sets the bookingCommitRequestData value for this BookingCommitRequest.
     * 
     * @param bookingCommitRequestData
     */
    public void setBookingCommitRequestData(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingCommitRequestData bookingCommitRequestData) {
        this.bookingCommitRequestData = bookingCommitRequestData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingCommitRequest)) return false;
        BookingCommitRequest other = (BookingCommitRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingCommitRequestData==null && other.getBookingCommitRequestData()==null) || 
             (this.bookingCommitRequestData!=null &&
              this.bookingCommitRequestData.equals(other.getBookingCommitRequestData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingCommitRequestData() != null) {
            _hashCode += getBookingCommitRequestData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingCommitRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">BookingCommitRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingCommitRequestData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingCommitRequestData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingCommitRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
