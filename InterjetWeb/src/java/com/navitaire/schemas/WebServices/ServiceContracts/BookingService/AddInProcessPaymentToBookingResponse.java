/**
 * AddInProcessPaymentToBookingResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.ServiceContracts.BookingService;

public class AddInProcessPaymentToBookingResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.AddPaymentToBookingResponseData bookingPaymentResponse;

    public AddInProcessPaymentToBookingResponse() {
    }

    public AddInProcessPaymentToBookingResponse(
           com.navitaire.schemas.WebServices.DataContracts.Booking.AddPaymentToBookingResponseData bookingPaymentResponse) {
           this.bookingPaymentResponse = bookingPaymentResponse;
    }


    /**
     * Gets the bookingPaymentResponse value for this AddInProcessPaymentToBookingResponse.
     * 
     * @return bookingPaymentResponse
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.AddPaymentToBookingResponseData getBookingPaymentResponse() {
        return bookingPaymentResponse;
    }


    /**
     * Sets the bookingPaymentResponse value for this AddInProcessPaymentToBookingResponse.
     * 
     * @param bookingPaymentResponse
     */
    public void setBookingPaymentResponse(com.navitaire.schemas.WebServices.DataContracts.Booking.AddPaymentToBookingResponseData bookingPaymentResponse) {
        this.bookingPaymentResponse = bookingPaymentResponse;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddInProcessPaymentToBookingResponse)) return false;
        AddInProcessPaymentToBookingResponse other = (AddInProcessPaymentToBookingResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingPaymentResponse==null && other.getBookingPaymentResponse()==null) || 
             (this.bookingPaymentResponse!=null &&
              this.bookingPaymentResponse.equals(other.getBookingPaymentResponse())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingPaymentResponse() != null) {
            _hashCode += getBookingPaymentResponse().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddInProcessPaymentToBookingResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddInProcessPaymentToBookingResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingPaymentResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingPaymentResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AddPaymentToBookingResponseData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
