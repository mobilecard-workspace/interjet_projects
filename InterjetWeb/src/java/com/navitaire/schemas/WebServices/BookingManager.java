/**
 * BookingManager.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices;

public interface BookingManager extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpBinding_IBookingManagerAddress();

    public com.navitaire.schemas.WebServices.IBookingManager getBasicHttpBinding_IBookingManager() throws javax.xml.rpc.ServiceException;

    public com.navitaire.schemas.WebServices.IBookingManager getBasicHttpBinding_IBookingManager(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
