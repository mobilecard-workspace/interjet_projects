/**
 * SegmentTicketRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SegmentTicketRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator;

    private java.util.Calendar STD;

    private java.lang.String departureStation;

    private java.lang.String arrivalStation;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicketUpdate[] paxTickets;

    public SegmentTicketRequest() {
    }

    public SegmentTicketRequest(
           com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator,
           java.util.Calendar STD,
           java.lang.String departureStation,
           java.lang.String arrivalStation,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicketUpdate[] paxTickets) {
           this.flightDesignator = flightDesignator;
           this.STD = STD;
           this.departureStation = departureStation;
           this.arrivalStation = arrivalStation;
           this.paxTickets = paxTickets;
    }


    /**
     * Gets the flightDesignator value for this SegmentTicketRequest.
     * 
     * @return flightDesignator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator getFlightDesignator() {
        return flightDesignator;
    }


    /**
     * Sets the flightDesignator value for this SegmentTicketRequest.
     * 
     * @param flightDesignator
     */
    public void setFlightDesignator(com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator) {
        this.flightDesignator = flightDesignator;
    }


    /**
     * Gets the STD value for this SegmentTicketRequest.
     * 
     * @return STD
     */
    public java.util.Calendar getSTD() {
        return STD;
    }


    /**
     * Sets the STD value for this SegmentTicketRequest.
     * 
     * @param STD
     */
    public void setSTD(java.util.Calendar STD) {
        this.STD = STD;
    }


    /**
     * Gets the departureStation value for this SegmentTicketRequest.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this SegmentTicketRequest.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the arrivalStation value for this SegmentTicketRequest.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this SegmentTicketRequest.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the paxTickets value for this SegmentTicketRequest.
     * 
     * @return paxTickets
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicketUpdate[] getPaxTickets() {
        return paxTickets;
    }


    /**
     * Sets the paxTickets value for this SegmentTicketRequest.
     * 
     * @param paxTickets
     */
    public void setPaxTickets(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicketUpdate[] paxTickets) {
        this.paxTickets = paxTickets;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SegmentTicketRequest)) return false;
        SegmentTicketRequest other = (SegmentTicketRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flightDesignator==null && other.getFlightDesignator()==null) || 
             (this.flightDesignator!=null &&
              this.flightDesignator.equals(other.getFlightDesignator()))) &&
            ((this.STD==null && other.getSTD()==null) || 
             (this.STD!=null &&
              this.STD.equals(other.getSTD()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.paxTickets==null && other.getPaxTickets()==null) || 
             (this.paxTickets!=null &&
              java.util.Arrays.equals(this.paxTickets, other.getPaxTickets())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlightDesignator() != null) {
            _hashCode += getFlightDesignator().hashCode();
        }
        if (getSTD() != null) {
            _hashCode += getSTD().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getPaxTickets() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxTickets());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxTickets(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SegmentTicketRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentTicketRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FlightDesignator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxTickets");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTickets"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicketUpdate"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicketUpdate"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
