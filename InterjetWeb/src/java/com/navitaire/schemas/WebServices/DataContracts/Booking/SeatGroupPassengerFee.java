/**
 * SeatGroupPassengerFee.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SeatGroupPassengerFee  implements java.io.Serializable {
    private java.lang.Short seatGroup;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee passengerFee;

    private java.lang.Short passengerNumber;

    public SeatGroupPassengerFee() {
    }

    public SeatGroupPassengerFee(
           java.lang.Short seatGroup,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee passengerFee,
           java.lang.Short passengerNumber) {
           this.seatGroup = seatGroup;
           this.passengerFee = passengerFee;
           this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the seatGroup value for this SeatGroupPassengerFee.
     * 
     * @return seatGroup
     */
    public java.lang.Short getSeatGroup() {
        return seatGroup;
    }


    /**
     * Sets the seatGroup value for this SeatGroupPassengerFee.
     * 
     * @param seatGroup
     */
    public void setSeatGroup(java.lang.Short seatGroup) {
        this.seatGroup = seatGroup;
    }


    /**
     * Gets the passengerFee value for this SeatGroupPassengerFee.
     * 
     * @return passengerFee
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee getPassengerFee() {
        return passengerFee;
    }


    /**
     * Sets the passengerFee value for this SeatGroupPassengerFee.
     * 
     * @param passengerFee
     */
    public void setPassengerFee(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee passengerFee) {
        this.passengerFee = passengerFee;
    }


    /**
     * Gets the passengerNumber value for this SeatGroupPassengerFee.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this SeatGroupPassengerFee.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SeatGroupPassengerFee)) return false;
        SeatGroupPassengerFee other = (SeatGroupPassengerFee) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.seatGroup==null && other.getSeatGroup()==null) || 
             (this.seatGroup!=null &&
              this.seatGroup.equals(other.getSeatGroup()))) &&
            ((this.passengerFee==null && other.getPassengerFee()==null) || 
             (this.passengerFee!=null &&
              this.passengerFee.equals(other.getPassengerFee()))) &&
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSeatGroup() != null) {
            _hashCode += getSeatGroup().hashCode();
        }
        if (getPassengerFee() != null) {
            _hashCode += getPassengerFee().hashCode();
        }
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SeatGroupPassengerFee.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatGroupPassengerFee"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatGroup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatGroup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
