/**
 * FindByBookingDate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class FindByBookingDate  implements java.io.Serializable {
    private java.util.Calendar startBookingUTC;

    private java.util.Calendar endBookingUTC;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Filter filter;

    public FindByBookingDate() {
    }

    public FindByBookingDate(
           java.util.Calendar startBookingUTC,
           java.util.Calendar endBookingUTC,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Filter filter) {
           this.startBookingUTC = startBookingUTC;
           this.endBookingUTC = endBookingUTC;
           this.filter = filter;
    }


    /**
     * Gets the startBookingUTC value for this FindByBookingDate.
     * 
     * @return startBookingUTC
     */
    public java.util.Calendar getStartBookingUTC() {
        return startBookingUTC;
    }


    /**
     * Sets the startBookingUTC value for this FindByBookingDate.
     * 
     * @param startBookingUTC
     */
    public void setStartBookingUTC(java.util.Calendar startBookingUTC) {
        this.startBookingUTC = startBookingUTC;
    }


    /**
     * Gets the endBookingUTC value for this FindByBookingDate.
     * 
     * @return endBookingUTC
     */
    public java.util.Calendar getEndBookingUTC() {
        return endBookingUTC;
    }


    /**
     * Sets the endBookingUTC value for this FindByBookingDate.
     * 
     * @param endBookingUTC
     */
    public void setEndBookingUTC(java.util.Calendar endBookingUTC) {
        this.endBookingUTC = endBookingUTC;
    }


    /**
     * Gets the filter value for this FindByBookingDate.
     * 
     * @return filter
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Filter getFilter() {
        return filter;
    }


    /**
     * Sets the filter value for this FindByBookingDate.
     * 
     * @param filter
     */
    public void setFilter(com.navitaire.schemas.WebServices.DataContracts.Booking.Filter filter) {
        this.filter = filter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindByBookingDate)) return false;
        FindByBookingDate other = (FindByBookingDate) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.startBookingUTC==null && other.getStartBookingUTC()==null) || 
             (this.startBookingUTC!=null &&
              this.startBookingUTC.equals(other.getStartBookingUTC()))) &&
            ((this.endBookingUTC==null && other.getEndBookingUTC()==null) || 
             (this.endBookingUTC!=null &&
              this.endBookingUTC.equals(other.getEndBookingUTC()))) &&
            ((this.filter==null && other.getFilter()==null) || 
             (this.filter!=null &&
              this.filter.equals(other.getFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStartBookingUTC() != null) {
            _hashCode += getStartBookingUTC().hashCode();
        }
        if (getEndBookingUTC() != null) {
            _hashCode += getEndBookingUTC().hashCode();
        }
        if (getFilter() != null) {
            _hashCode += getFilter().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindByBookingDate.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByBookingDate"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startBookingUTC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "StartBookingUTC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endBookingUTC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndBookingUTC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Filter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Filter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
