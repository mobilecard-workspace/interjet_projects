/**
 * DateMarketSegment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DateMarketSegment  implements java.io.Serializable {
    private java.lang.String departureCity;

    private java.lang.String arrivalCity;

    private java.util.Calendar departureDate;

    private java.lang.String carrierCode;

    private java.lang.String dateMarketSegmentType;

    public DateMarketSegment() {
    }

    public DateMarketSegment(
           java.lang.String departureCity,
           java.lang.String arrivalCity,
           java.util.Calendar departureDate,
           java.lang.String carrierCode,
           java.lang.String dateMarketSegmentType) {
           this.departureCity = departureCity;
           this.arrivalCity = arrivalCity;
           this.departureDate = departureDate;
           this.carrierCode = carrierCode;
           this.dateMarketSegmentType = dateMarketSegmentType;
    }


    /**
     * Gets the departureCity value for this DateMarketSegment.
     * 
     * @return departureCity
     */
    public java.lang.String getDepartureCity() {
        return departureCity;
    }


    /**
     * Sets the departureCity value for this DateMarketSegment.
     * 
     * @param departureCity
     */
    public void setDepartureCity(java.lang.String departureCity) {
        this.departureCity = departureCity;
    }


    /**
     * Gets the arrivalCity value for this DateMarketSegment.
     * 
     * @return arrivalCity
     */
    public java.lang.String getArrivalCity() {
        return arrivalCity;
    }


    /**
     * Sets the arrivalCity value for this DateMarketSegment.
     * 
     * @param arrivalCity
     */
    public void setArrivalCity(java.lang.String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }


    /**
     * Gets the departureDate value for this DateMarketSegment.
     * 
     * @return departureDate
     */
    public java.util.Calendar getDepartureDate() {
        return departureDate;
    }


    /**
     * Sets the departureDate value for this DateMarketSegment.
     * 
     * @param departureDate
     */
    public void setDepartureDate(java.util.Calendar departureDate) {
        this.departureDate = departureDate;
    }


    /**
     * Gets the carrierCode value for this DateMarketSegment.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this DateMarketSegment.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the dateMarketSegmentType value for this DateMarketSegment.
     * 
     * @return dateMarketSegmentType
     */
    public java.lang.String getDateMarketSegmentType() {
        return dateMarketSegmentType;
    }


    /**
     * Sets the dateMarketSegmentType value for this DateMarketSegment.
     * 
     * @param dateMarketSegmentType
     */
    public void setDateMarketSegmentType(java.lang.String dateMarketSegmentType) {
        this.dateMarketSegmentType = dateMarketSegmentType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DateMarketSegment)) return false;
        DateMarketSegment other = (DateMarketSegment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departureCity==null && other.getDepartureCity()==null) || 
             (this.departureCity!=null &&
              this.departureCity.equals(other.getDepartureCity()))) &&
            ((this.arrivalCity==null && other.getArrivalCity()==null) || 
             (this.arrivalCity!=null &&
              this.arrivalCity.equals(other.getArrivalCity()))) &&
            ((this.departureDate==null && other.getDepartureDate()==null) || 
             (this.departureDate!=null &&
              this.departureDate.equals(other.getDepartureDate()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.dateMarketSegmentType==null && other.getDateMarketSegmentType()==null) || 
             (this.dateMarketSegmentType!=null &&
              this.dateMarketSegmentType.equals(other.getDateMarketSegmentType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartureCity() != null) {
            _hashCode += getDepartureCity().hashCode();
        }
        if (getArrivalCity() != null) {
            _hashCode += getArrivalCity().hashCode();
        }
        if (getDepartureDate() != null) {
            _hashCode += getDepartureDate().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getDateMarketSegmentType() != null) {
            _hashCode += getDateMarketSegmentType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DateMarketSegment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketSegment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateMarketSegmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketSegmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
