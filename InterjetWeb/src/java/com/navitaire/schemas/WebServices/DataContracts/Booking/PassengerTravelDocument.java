/**
 * PassengerTravelDocument.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PassengerTravelDocument  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String docTypeCode;

    private java.lang.String issuedByCode;

    private java.lang.String docSuffix;

    private java.lang.String docNumber;

    private java.util.Calendar DOB;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.Gender gender;

    private java.lang.String nationality;

    private java.util.Calendar expirationDate;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName[] names;

    private java.lang.String birthCountry;

    private java.util.Calendar issuedDate;

    public PassengerTravelDocument() {
    }

    public PassengerTravelDocument(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String docTypeCode,
           java.lang.String issuedByCode,
           java.lang.String docSuffix,
           java.lang.String docNumber,
           java.util.Calendar DOB,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.Gender gender,
           java.lang.String nationality,
           java.util.Calendar expirationDate,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName[] names,
           java.lang.String birthCountry,
           java.util.Calendar issuedDate) {
        super(
            state);
        this.docTypeCode = docTypeCode;
        this.issuedByCode = issuedByCode;
        this.docSuffix = docSuffix;
        this.docNumber = docNumber;
        this.DOB = DOB;
        this.gender = gender;
        this.nationality = nationality;
        this.expirationDate = expirationDate;
        this.names = names;
        this.birthCountry = birthCountry;
        this.issuedDate = issuedDate;
    }


    /**
     * Gets the docTypeCode value for this PassengerTravelDocument.
     * 
     * @return docTypeCode
     */
    public java.lang.String getDocTypeCode() {
        return docTypeCode;
    }


    /**
     * Sets the docTypeCode value for this PassengerTravelDocument.
     * 
     * @param docTypeCode
     */
    public void setDocTypeCode(java.lang.String docTypeCode) {
        this.docTypeCode = docTypeCode;
    }


    /**
     * Gets the issuedByCode value for this PassengerTravelDocument.
     * 
     * @return issuedByCode
     */
    public java.lang.String getIssuedByCode() {
        return issuedByCode;
    }


    /**
     * Sets the issuedByCode value for this PassengerTravelDocument.
     * 
     * @param issuedByCode
     */
    public void setIssuedByCode(java.lang.String issuedByCode) {
        this.issuedByCode = issuedByCode;
    }


    /**
     * Gets the docSuffix value for this PassengerTravelDocument.
     * 
     * @return docSuffix
     */
    public java.lang.String getDocSuffix() {
        return docSuffix;
    }


    /**
     * Sets the docSuffix value for this PassengerTravelDocument.
     * 
     * @param docSuffix
     */
    public void setDocSuffix(java.lang.String docSuffix) {
        this.docSuffix = docSuffix;
    }


    /**
     * Gets the docNumber value for this PassengerTravelDocument.
     * 
     * @return docNumber
     */
    public java.lang.String getDocNumber() {
        return docNumber;
    }


    /**
     * Sets the docNumber value for this PassengerTravelDocument.
     * 
     * @param docNumber
     */
    public void setDocNumber(java.lang.String docNumber) {
        this.docNumber = docNumber;
    }


    /**
     * Gets the DOB value for this PassengerTravelDocument.
     * 
     * @return DOB
     */
    public java.util.Calendar getDOB() {
        return DOB;
    }


    /**
     * Sets the DOB value for this PassengerTravelDocument.
     * 
     * @param DOB
     */
    public void setDOB(java.util.Calendar DOB) {
        this.DOB = DOB;
    }


    /**
     * Gets the gender value for this PassengerTravelDocument.
     * 
     * @return gender
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.Gender getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this PassengerTravelDocument.
     * 
     * @param gender
     */
    public void setGender(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.Gender gender) {
        this.gender = gender;
    }


    /**
     * Gets the nationality value for this PassengerTravelDocument.
     * 
     * @return nationality
     */
    public java.lang.String getNationality() {
        return nationality;
    }


    /**
     * Sets the nationality value for this PassengerTravelDocument.
     * 
     * @param nationality
     */
    public void setNationality(java.lang.String nationality) {
        this.nationality = nationality;
    }


    /**
     * Gets the expirationDate value for this PassengerTravelDocument.
     * 
     * @return expirationDate
     */
    public java.util.Calendar getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this PassengerTravelDocument.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.util.Calendar expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the names value for this PassengerTravelDocument.
     * 
     * @return names
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName[] getNames() {
        return names;
    }


    /**
     * Sets the names value for this PassengerTravelDocument.
     * 
     * @param names
     */
    public void setNames(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName[] names) {
        this.names = names;
    }


    /**
     * Gets the birthCountry value for this PassengerTravelDocument.
     * 
     * @return birthCountry
     */
    public java.lang.String getBirthCountry() {
        return birthCountry;
    }


    /**
     * Sets the birthCountry value for this PassengerTravelDocument.
     * 
     * @param birthCountry
     */
    public void setBirthCountry(java.lang.String birthCountry) {
        this.birthCountry = birthCountry;
    }


    /**
     * Gets the issuedDate value for this PassengerTravelDocument.
     * 
     * @return issuedDate
     */
    public java.util.Calendar getIssuedDate() {
        return issuedDate;
    }


    /**
     * Sets the issuedDate value for this PassengerTravelDocument.
     * 
     * @param issuedDate
     */
    public void setIssuedDate(java.util.Calendar issuedDate) {
        this.issuedDate = issuedDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PassengerTravelDocument)) return false;
        PassengerTravelDocument other = (PassengerTravelDocument) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.docTypeCode==null && other.getDocTypeCode()==null) || 
             (this.docTypeCode!=null &&
              this.docTypeCode.equals(other.getDocTypeCode()))) &&
            ((this.issuedByCode==null && other.getIssuedByCode()==null) || 
             (this.issuedByCode!=null &&
              this.issuedByCode.equals(other.getIssuedByCode()))) &&
            ((this.docSuffix==null && other.getDocSuffix()==null) || 
             (this.docSuffix!=null &&
              this.docSuffix.equals(other.getDocSuffix()))) &&
            ((this.docNumber==null && other.getDocNumber()==null) || 
             (this.docNumber!=null &&
              this.docNumber.equals(other.getDocNumber()))) &&
            ((this.DOB==null && other.getDOB()==null) || 
             (this.DOB!=null &&
              this.DOB.equals(other.getDOB()))) &&
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              this.gender.equals(other.getGender()))) &&
            ((this.nationality==null && other.getNationality()==null) || 
             (this.nationality!=null &&
              this.nationality.equals(other.getNationality()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.names==null && other.getNames()==null) || 
             (this.names!=null &&
              java.util.Arrays.equals(this.names, other.getNames()))) &&
            ((this.birthCountry==null && other.getBirthCountry()==null) || 
             (this.birthCountry!=null &&
              this.birthCountry.equals(other.getBirthCountry()))) &&
            ((this.issuedDate==null && other.getIssuedDate()==null) || 
             (this.issuedDate!=null &&
              this.issuedDate.equals(other.getIssuedDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDocTypeCode() != null) {
            _hashCode += getDocTypeCode().hashCode();
        }
        if (getIssuedByCode() != null) {
            _hashCode += getIssuedByCode().hashCode();
        }
        if (getDocSuffix() != null) {
            _hashCode += getDocSuffix().hashCode();
        }
        if (getDocNumber() != null) {
            _hashCode += getDocNumber().hashCode();
        }
        if (getDOB() != null) {
            _hashCode += getDOB().hashCode();
        }
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getNationality() != null) {
            _hashCode += getNationality().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBirthCountry() != null) {
            _hashCode += getBirthCountry().hashCode();
        }
        if (getIssuedDate() != null) {
            _hashCode += getIssuedDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PassengerTravelDocument.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTravelDocument"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("docTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DocTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuedByCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IssuedByCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("docSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DocSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("docNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DocNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOB");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DOB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Gender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "Gender"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nationality");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Nationality"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("names");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Names"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingName"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BirthCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IssuedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
