/**
 * DateMarketAVS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DateMarketAVS  implements java.io.Serializable {
    private java.lang.String departureCity;

    private java.lang.String arrivalCity;

    private java.util.Calendar beginDepartureDate;

    private java.util.Calendar endDepartureDate;

    private java.lang.String marketAVSType;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightAVS[] dateFlightAVSList;

    public DateMarketAVS() {
    }

    public DateMarketAVS(
           java.lang.String departureCity,
           java.lang.String arrivalCity,
           java.util.Calendar beginDepartureDate,
           java.util.Calendar endDepartureDate,
           java.lang.String marketAVSType,
           com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightAVS[] dateFlightAVSList) {
           this.departureCity = departureCity;
           this.arrivalCity = arrivalCity;
           this.beginDepartureDate = beginDepartureDate;
           this.endDepartureDate = endDepartureDate;
           this.marketAVSType = marketAVSType;
           this.dateFlightAVSList = dateFlightAVSList;
    }


    /**
     * Gets the departureCity value for this DateMarketAVS.
     * 
     * @return departureCity
     */
    public java.lang.String getDepartureCity() {
        return departureCity;
    }


    /**
     * Sets the departureCity value for this DateMarketAVS.
     * 
     * @param departureCity
     */
    public void setDepartureCity(java.lang.String departureCity) {
        this.departureCity = departureCity;
    }


    /**
     * Gets the arrivalCity value for this DateMarketAVS.
     * 
     * @return arrivalCity
     */
    public java.lang.String getArrivalCity() {
        return arrivalCity;
    }


    /**
     * Sets the arrivalCity value for this DateMarketAVS.
     * 
     * @param arrivalCity
     */
    public void setArrivalCity(java.lang.String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }


    /**
     * Gets the beginDepartureDate value for this DateMarketAVS.
     * 
     * @return beginDepartureDate
     */
    public java.util.Calendar getBeginDepartureDate() {
        return beginDepartureDate;
    }


    /**
     * Sets the beginDepartureDate value for this DateMarketAVS.
     * 
     * @param beginDepartureDate
     */
    public void setBeginDepartureDate(java.util.Calendar beginDepartureDate) {
        this.beginDepartureDate = beginDepartureDate;
    }


    /**
     * Gets the endDepartureDate value for this DateMarketAVS.
     * 
     * @return endDepartureDate
     */
    public java.util.Calendar getEndDepartureDate() {
        return endDepartureDate;
    }


    /**
     * Sets the endDepartureDate value for this DateMarketAVS.
     * 
     * @param endDepartureDate
     */
    public void setEndDepartureDate(java.util.Calendar endDepartureDate) {
        this.endDepartureDate = endDepartureDate;
    }


    /**
     * Gets the marketAVSType value for this DateMarketAVS.
     * 
     * @return marketAVSType
     */
    public java.lang.String getMarketAVSType() {
        return marketAVSType;
    }


    /**
     * Sets the marketAVSType value for this DateMarketAVS.
     * 
     * @param marketAVSType
     */
    public void setMarketAVSType(java.lang.String marketAVSType) {
        this.marketAVSType = marketAVSType;
    }


    /**
     * Gets the dateFlightAVSList value for this DateMarketAVS.
     * 
     * @return dateFlightAVSList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightAVS[] getDateFlightAVSList() {
        return dateFlightAVSList;
    }


    /**
     * Sets the dateFlightAVSList value for this DateMarketAVS.
     * 
     * @param dateFlightAVSList
     */
    public void setDateFlightAVSList(com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightAVS[] dateFlightAVSList) {
        this.dateFlightAVSList = dateFlightAVSList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DateMarketAVS)) return false;
        DateMarketAVS other = (DateMarketAVS) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departureCity==null && other.getDepartureCity()==null) || 
             (this.departureCity!=null &&
              this.departureCity.equals(other.getDepartureCity()))) &&
            ((this.arrivalCity==null && other.getArrivalCity()==null) || 
             (this.arrivalCity!=null &&
              this.arrivalCity.equals(other.getArrivalCity()))) &&
            ((this.beginDepartureDate==null && other.getBeginDepartureDate()==null) || 
             (this.beginDepartureDate!=null &&
              this.beginDepartureDate.equals(other.getBeginDepartureDate()))) &&
            ((this.endDepartureDate==null && other.getEndDepartureDate()==null) || 
             (this.endDepartureDate!=null &&
              this.endDepartureDate.equals(other.getEndDepartureDate()))) &&
            ((this.marketAVSType==null && other.getMarketAVSType()==null) || 
             (this.marketAVSType!=null &&
              this.marketAVSType.equals(other.getMarketAVSType()))) &&
            ((this.dateFlightAVSList==null && other.getDateFlightAVSList()==null) || 
             (this.dateFlightAVSList!=null &&
              java.util.Arrays.equals(this.dateFlightAVSList, other.getDateFlightAVSList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartureCity() != null) {
            _hashCode += getDepartureCity().hashCode();
        }
        if (getArrivalCity() != null) {
            _hashCode += getArrivalCity().hashCode();
        }
        if (getBeginDepartureDate() != null) {
            _hashCode += getBeginDepartureDate().hashCode();
        }
        if (getEndDepartureDate() != null) {
            _hashCode += getEndDepartureDate().hashCode();
        }
        if (getMarketAVSType() != null) {
            _hashCode += getMarketAVSType().hashCode();
        }
        if (getDateFlightAVSList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDateFlightAVSList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDateFlightAVSList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DateMarketAVS.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketAVS"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDepartureDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BeginDepartureDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDepartureDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndDepartureDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketAVSType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MarketAVSType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateFlightAVSList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightAVSList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightAVS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightAVS"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
