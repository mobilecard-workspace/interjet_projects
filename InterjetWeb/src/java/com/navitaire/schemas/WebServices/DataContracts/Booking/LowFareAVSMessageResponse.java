/**
 * LowFareAVSMessageResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class LowFareAVSMessageResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityStatus[] availabilityStatusList;

    public LowFareAVSMessageResponse() {
    }

    public LowFareAVSMessageResponse(
           com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityStatus[] availabilityStatusList) {
           this.availabilityStatusList = availabilityStatusList;
    }


    /**
     * Gets the availabilityStatusList value for this LowFareAVSMessageResponse.
     * 
     * @return availabilityStatusList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityStatus[] getAvailabilityStatusList() {
        return availabilityStatusList;
    }


    /**
     * Sets the availabilityStatusList value for this LowFareAVSMessageResponse.
     * 
     * @param availabilityStatusList
     */
    public void setAvailabilityStatusList(com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityStatus[] availabilityStatusList) {
        this.availabilityStatusList = availabilityStatusList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LowFareAVSMessageResponse)) return false;
        LowFareAVSMessageResponse other = (LowFareAVSMessageResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.availabilityStatusList==null && other.getAvailabilityStatusList()==null) || 
             (this.availabilityStatusList!=null &&
              java.util.Arrays.equals(this.availabilityStatusList, other.getAvailabilityStatusList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAvailabilityStatusList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAvailabilityStatusList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAvailabilityStatusList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LowFareAVSMessageResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAVSMessageResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availabilityStatusList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityStatusList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityStatus"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
