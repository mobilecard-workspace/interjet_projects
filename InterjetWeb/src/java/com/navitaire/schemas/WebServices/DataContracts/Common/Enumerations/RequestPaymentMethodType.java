/**
 * RequestPaymentMethodType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class RequestPaymentMethodType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected RequestPaymentMethodType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _PrePaid = "PrePaid";
    public static final java.lang.String _ExternalAccount = "ExternalAccount";
    public static final java.lang.String _AgencyAccount = "AgencyAccount";
    public static final java.lang.String _CreditShell = "CreditShell";
    public static final java.lang.String _CreditFile = "CreditFile";
    public static final java.lang.String _Voucher = "Voucher";
    public static final java.lang.String _Loyalty = "Loyalty";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final RequestPaymentMethodType PrePaid = new RequestPaymentMethodType(_PrePaid);
    public static final RequestPaymentMethodType ExternalAccount = new RequestPaymentMethodType(_ExternalAccount);
    public static final RequestPaymentMethodType AgencyAccount = new RequestPaymentMethodType(_AgencyAccount);
    public static final RequestPaymentMethodType CreditShell = new RequestPaymentMethodType(_CreditShell);
    public static final RequestPaymentMethodType CreditFile = new RequestPaymentMethodType(_CreditFile);
    public static final RequestPaymentMethodType Voucher = new RequestPaymentMethodType(_Voucher);
    public static final RequestPaymentMethodType Loyalty = new RequestPaymentMethodType(_Loyalty);
    public static final RequestPaymentMethodType Unmapped = new RequestPaymentMethodType(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static RequestPaymentMethodType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        RequestPaymentMethodType enumeration = (RequestPaymentMethodType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static RequestPaymentMethodType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestPaymentMethodType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "RequestPaymentMethodType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
