/**
 * BookingInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingInfo  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingStatus bookingStatus;

    private java.lang.String bookingType;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType;

    private java.util.Calendar createdDate;

    private java.util.Calendar expiredDate;

    private java.util.Calendar modifiedDate;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceStatus priceStatus;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingProfileStatus profileStatus;

    private java.lang.Boolean changeAllowed;

    private java.lang.Long createdAgentID;

    private java.lang.Long modifiedAgentID;

    private java.util.Calendar bookingDate;

    private java.lang.String owningCarrierCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaidStatus paidStatus;

    public BookingInfo() {
    }

    public BookingInfo(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingStatus bookingStatus,
           java.lang.String bookingType,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType,
           java.util.Calendar createdDate,
           java.util.Calendar expiredDate,
           java.util.Calendar modifiedDate,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceStatus priceStatus,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingProfileStatus profileStatus,
           java.lang.Boolean changeAllowed,
           java.lang.Long createdAgentID,
           java.lang.Long modifiedAgentID,
           java.util.Calendar bookingDate,
           java.lang.String owningCarrierCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaidStatus paidStatus) {
        super(
            state);
        this.bookingStatus = bookingStatus;
        this.bookingType = bookingType;
        this.channelType = channelType;
        this.createdDate = createdDate;
        this.expiredDate = expiredDate;
        this.modifiedDate = modifiedDate;
        this.priceStatus = priceStatus;
        this.profileStatus = profileStatus;
        this.changeAllowed = changeAllowed;
        this.createdAgentID = createdAgentID;
        this.modifiedAgentID = modifiedAgentID;
        this.bookingDate = bookingDate;
        this.owningCarrierCode = owningCarrierCode;
        this.paidStatus = paidStatus;
    }


    /**
     * Gets the bookingStatus value for this BookingInfo.
     * 
     * @return bookingStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingStatus getBookingStatus() {
        return bookingStatus;
    }


    /**
     * Sets the bookingStatus value for this BookingInfo.
     * 
     * @param bookingStatus
     */
    public void setBookingStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }


    /**
     * Gets the bookingType value for this BookingInfo.
     * 
     * @return bookingType
     */
    public java.lang.String getBookingType() {
        return bookingType;
    }


    /**
     * Sets the bookingType value for this BookingInfo.
     * 
     * @param bookingType
     */
    public void setBookingType(java.lang.String bookingType) {
        this.bookingType = bookingType;
    }


    /**
     * Gets the channelType value for this BookingInfo.
     * 
     * @return channelType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType getChannelType() {
        return channelType;
    }


    /**
     * Sets the channelType value for this BookingInfo.
     * 
     * @param channelType
     */
    public void setChannelType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType) {
        this.channelType = channelType;
    }


    /**
     * Gets the createdDate value for this BookingInfo.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this BookingInfo.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }


    /**
     * Gets the expiredDate value for this BookingInfo.
     * 
     * @return expiredDate
     */
    public java.util.Calendar getExpiredDate() {
        return expiredDate;
    }


    /**
     * Sets the expiredDate value for this BookingInfo.
     * 
     * @param expiredDate
     */
    public void setExpiredDate(java.util.Calendar expiredDate) {
        this.expiredDate = expiredDate;
    }


    /**
     * Gets the modifiedDate value for this BookingInfo.
     * 
     * @return modifiedDate
     */
    public java.util.Calendar getModifiedDate() {
        return modifiedDate;
    }


    /**
     * Sets the modifiedDate value for this BookingInfo.
     * 
     * @param modifiedDate
     */
    public void setModifiedDate(java.util.Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    /**
     * Gets the priceStatus value for this BookingInfo.
     * 
     * @return priceStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceStatus getPriceStatus() {
        return priceStatus;
    }


    /**
     * Sets the priceStatus value for this BookingInfo.
     * 
     * @param priceStatus
     */
    public void setPriceStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceStatus priceStatus) {
        this.priceStatus = priceStatus;
    }


    /**
     * Gets the profileStatus value for this BookingInfo.
     * 
     * @return profileStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingProfileStatus getProfileStatus() {
        return profileStatus;
    }


    /**
     * Sets the profileStatus value for this BookingInfo.
     * 
     * @param profileStatus
     */
    public void setProfileStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingProfileStatus profileStatus) {
        this.profileStatus = profileStatus;
    }


    /**
     * Gets the changeAllowed value for this BookingInfo.
     * 
     * @return changeAllowed
     */
    public java.lang.Boolean getChangeAllowed() {
        return changeAllowed;
    }


    /**
     * Sets the changeAllowed value for this BookingInfo.
     * 
     * @param changeAllowed
     */
    public void setChangeAllowed(java.lang.Boolean changeAllowed) {
        this.changeAllowed = changeAllowed;
    }


    /**
     * Gets the createdAgentID value for this BookingInfo.
     * 
     * @return createdAgentID
     */
    public java.lang.Long getCreatedAgentID() {
        return createdAgentID;
    }


    /**
     * Sets the createdAgentID value for this BookingInfo.
     * 
     * @param createdAgentID
     */
    public void setCreatedAgentID(java.lang.Long createdAgentID) {
        this.createdAgentID = createdAgentID;
    }


    /**
     * Gets the modifiedAgentID value for this BookingInfo.
     * 
     * @return modifiedAgentID
     */
    public java.lang.Long getModifiedAgentID() {
        return modifiedAgentID;
    }


    /**
     * Sets the modifiedAgentID value for this BookingInfo.
     * 
     * @param modifiedAgentID
     */
    public void setModifiedAgentID(java.lang.Long modifiedAgentID) {
        this.modifiedAgentID = modifiedAgentID;
    }


    /**
     * Gets the bookingDate value for this BookingInfo.
     * 
     * @return bookingDate
     */
    public java.util.Calendar getBookingDate() {
        return bookingDate;
    }


    /**
     * Sets the bookingDate value for this BookingInfo.
     * 
     * @param bookingDate
     */
    public void setBookingDate(java.util.Calendar bookingDate) {
        this.bookingDate = bookingDate;
    }


    /**
     * Gets the owningCarrierCode value for this BookingInfo.
     * 
     * @return owningCarrierCode
     */
    public java.lang.String getOwningCarrierCode() {
        return owningCarrierCode;
    }


    /**
     * Sets the owningCarrierCode value for this BookingInfo.
     * 
     * @param owningCarrierCode
     */
    public void setOwningCarrierCode(java.lang.String owningCarrierCode) {
        this.owningCarrierCode = owningCarrierCode;
    }


    /**
     * Gets the paidStatus value for this BookingInfo.
     * 
     * @return paidStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaidStatus getPaidStatus() {
        return paidStatus;
    }


    /**
     * Sets the paidStatus value for this BookingInfo.
     * 
     * @param paidStatus
     */
    public void setPaidStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaidStatus paidStatus) {
        this.paidStatus = paidStatus;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingInfo)) return false;
        BookingInfo other = (BookingInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.bookingStatus==null && other.getBookingStatus()==null) || 
             (this.bookingStatus!=null &&
              this.bookingStatus.equals(other.getBookingStatus()))) &&
            ((this.bookingType==null && other.getBookingType()==null) || 
             (this.bookingType!=null &&
              this.bookingType.equals(other.getBookingType()))) &&
            ((this.channelType==null && other.getChannelType()==null) || 
             (this.channelType!=null &&
              this.channelType.equals(other.getChannelType()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate()))) &&
            ((this.expiredDate==null && other.getExpiredDate()==null) || 
             (this.expiredDate!=null &&
              this.expiredDate.equals(other.getExpiredDate()))) &&
            ((this.modifiedDate==null && other.getModifiedDate()==null) || 
             (this.modifiedDate!=null &&
              this.modifiedDate.equals(other.getModifiedDate()))) &&
            ((this.priceStatus==null && other.getPriceStatus()==null) || 
             (this.priceStatus!=null &&
              this.priceStatus.equals(other.getPriceStatus()))) &&
            ((this.profileStatus==null && other.getProfileStatus()==null) || 
             (this.profileStatus!=null &&
              this.profileStatus.equals(other.getProfileStatus()))) &&
            ((this.changeAllowed==null && other.getChangeAllowed()==null) || 
             (this.changeAllowed!=null &&
              this.changeAllowed.equals(other.getChangeAllowed()))) &&
            ((this.createdAgentID==null && other.getCreatedAgentID()==null) || 
             (this.createdAgentID!=null &&
              this.createdAgentID.equals(other.getCreatedAgentID()))) &&
            ((this.modifiedAgentID==null && other.getModifiedAgentID()==null) || 
             (this.modifiedAgentID!=null &&
              this.modifiedAgentID.equals(other.getModifiedAgentID()))) &&
            ((this.bookingDate==null && other.getBookingDate()==null) || 
             (this.bookingDate!=null &&
              this.bookingDate.equals(other.getBookingDate()))) &&
            ((this.owningCarrierCode==null && other.getOwningCarrierCode()==null) || 
             (this.owningCarrierCode!=null &&
              this.owningCarrierCode.equals(other.getOwningCarrierCode()))) &&
            ((this.paidStatus==null && other.getPaidStatus()==null) || 
             (this.paidStatus!=null &&
              this.paidStatus.equals(other.getPaidStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBookingStatus() != null) {
            _hashCode += getBookingStatus().hashCode();
        }
        if (getBookingType() != null) {
            _hashCode += getBookingType().hashCode();
        }
        if (getChannelType() != null) {
            _hashCode += getChannelType().hashCode();
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        if (getExpiredDate() != null) {
            _hashCode += getExpiredDate().hashCode();
        }
        if (getModifiedDate() != null) {
            _hashCode += getModifiedDate().hashCode();
        }
        if (getPriceStatus() != null) {
            _hashCode += getPriceStatus().hashCode();
        }
        if (getProfileStatus() != null) {
            _hashCode += getProfileStatus().hashCode();
        }
        if (getChangeAllowed() != null) {
            _hashCode += getChangeAllowed().hashCode();
        }
        if (getCreatedAgentID() != null) {
            _hashCode += getCreatedAgentID().hashCode();
        }
        if (getModifiedAgentID() != null) {
            _hashCode += getModifiedAgentID().hashCode();
        }
        if (getBookingDate() != null) {
            _hashCode += getBookingDate().hashCode();
        }
        if (getOwningCarrierCode() != null) {
            _hashCode += getOwningCarrierCode().hashCode();
        }
        if (getPaidStatus() != null) {
            _hashCode += getPaidStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChannelType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChannelType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expiredDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ExpiredDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ModifiedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priceStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PriceStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProfileStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingProfileStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeAllowed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChangeAllowed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdAgentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedAgentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedAgentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ModifiedAgentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("owningCarrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OwningCarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paidStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaidStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaidStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
