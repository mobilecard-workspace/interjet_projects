/**
 * CreditShell.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class CreditShell  extends com.navitaire.schemas.WebServices.DataContracts.Booking.AgencyAccount  implements java.io.Serializable {
    private java.lang.String accountTransactionCode;

    public CreditShell() {
    }

    public CreditShell(
           java.lang.Long accountID,
           java.lang.Long accountTransactionID,
           java.lang.String password,
           java.lang.String accountTransactionCode) {
        super(
            accountID,
            accountTransactionID,
            password);
        this.accountTransactionCode = accountTransactionCode;
    }


    /**
     * Gets the accountTransactionCode value for this CreditShell.
     * 
     * @return accountTransactionCode
     */
    public java.lang.String getAccountTransactionCode() {
        return accountTransactionCode;
    }


    /**
     * Sets the accountTransactionCode value for this CreditShell.
     * 
     * @param accountTransactionCode
     */
    public void setAccountTransactionCode(java.lang.String accountTransactionCode) {
        this.accountTransactionCode = accountTransactionCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreditShell)) return false;
        CreditShell other = (CreditShell) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.accountTransactionCode==null && other.getAccountTransactionCode()==null) || 
             (this.accountTransactionCode!=null &&
              this.accountTransactionCode.equals(other.getAccountTransactionCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAccountTransactionCode() != null) {
            _hashCode += getAccountTransactionCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreditShell.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreditShell"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountTransactionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AccountTransactionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
