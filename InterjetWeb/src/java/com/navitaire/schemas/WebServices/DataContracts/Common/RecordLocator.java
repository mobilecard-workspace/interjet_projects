/**
 * RecordLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class RecordLocator  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String systemDomainCode;

    private java.lang.String systemCode;

    private java.lang.String recordCode;

    private java.lang.String interactionPurpose;

    private java.lang.String hostedCarrierCode;

    public RecordLocator() {
    }

    public RecordLocator(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String systemDomainCode,
           java.lang.String systemCode,
           java.lang.String recordCode,
           java.lang.String interactionPurpose,
           java.lang.String hostedCarrierCode) {
        super(
            state);
        this.systemDomainCode = systemDomainCode;
        this.systemCode = systemCode;
        this.recordCode = recordCode;
        this.interactionPurpose = interactionPurpose;
        this.hostedCarrierCode = hostedCarrierCode;
    }


    /**
     * Gets the systemDomainCode value for this RecordLocator.
     * 
     * @return systemDomainCode
     */
    public java.lang.String getSystemDomainCode() {
        return systemDomainCode;
    }


    /**
     * Sets the systemDomainCode value for this RecordLocator.
     * 
     * @param systemDomainCode
     */
    public void setSystemDomainCode(java.lang.String systemDomainCode) {
        this.systemDomainCode = systemDomainCode;
    }


    /**
     * Gets the systemCode value for this RecordLocator.
     * 
     * @return systemCode
     */
    public java.lang.String getSystemCode() {
        return systemCode;
    }


    /**
     * Sets the systemCode value for this RecordLocator.
     * 
     * @param systemCode
     */
    public void setSystemCode(java.lang.String systemCode) {
        this.systemCode = systemCode;
    }


    /**
     * Gets the recordCode value for this RecordLocator.
     * 
     * @return recordCode
     */
    public java.lang.String getRecordCode() {
        return recordCode;
    }


    /**
     * Sets the recordCode value for this RecordLocator.
     * 
     * @param recordCode
     */
    public void setRecordCode(java.lang.String recordCode) {
        this.recordCode = recordCode;
    }


    /**
     * Gets the interactionPurpose value for this RecordLocator.
     * 
     * @return interactionPurpose
     */
    public java.lang.String getInteractionPurpose() {
        return interactionPurpose;
    }


    /**
     * Sets the interactionPurpose value for this RecordLocator.
     * 
     * @param interactionPurpose
     */
    public void setInteractionPurpose(java.lang.String interactionPurpose) {
        this.interactionPurpose = interactionPurpose;
    }


    /**
     * Gets the hostedCarrierCode value for this RecordLocator.
     * 
     * @return hostedCarrierCode
     */
    public java.lang.String getHostedCarrierCode() {
        return hostedCarrierCode;
    }


    /**
     * Sets the hostedCarrierCode value for this RecordLocator.
     * 
     * @param hostedCarrierCode
     */
    public void setHostedCarrierCode(java.lang.String hostedCarrierCode) {
        this.hostedCarrierCode = hostedCarrierCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RecordLocator)) return false;
        RecordLocator other = (RecordLocator) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.systemDomainCode==null && other.getSystemDomainCode()==null) || 
             (this.systemDomainCode!=null &&
              this.systemDomainCode.equals(other.getSystemDomainCode()))) &&
            ((this.systemCode==null && other.getSystemCode()==null) || 
             (this.systemCode!=null &&
              this.systemCode.equals(other.getSystemCode()))) &&
            ((this.recordCode==null && other.getRecordCode()==null) || 
             (this.recordCode!=null &&
              this.recordCode.equals(other.getRecordCode()))) &&
            ((this.interactionPurpose==null && other.getInteractionPurpose()==null) || 
             (this.interactionPurpose!=null &&
              this.interactionPurpose.equals(other.getInteractionPurpose()))) &&
            ((this.hostedCarrierCode==null && other.getHostedCarrierCode()==null) || 
             (this.hostedCarrierCode!=null &&
              this.hostedCarrierCode.equals(other.getHostedCarrierCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSystemDomainCode() != null) {
            _hashCode += getSystemDomainCode().hashCode();
        }
        if (getSystemCode() != null) {
            _hashCode += getSystemCode().hashCode();
        }
        if (getRecordCode() != null) {
            _hashCode += getRecordCode().hashCode();
        }
        if (getInteractionPurpose() != null) {
            _hashCode += getInteractionPurpose().hashCode();
        }
        if (getHostedCarrierCode() != null) {
            _hashCode += getHostedCarrierCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RecordLocator.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemDomainCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SystemDomainCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SystemCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactionPurpose");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "InteractionPurpose"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostedCarrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "HostedCarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
