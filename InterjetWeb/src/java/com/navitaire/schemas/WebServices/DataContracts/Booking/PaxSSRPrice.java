/**
 * PaxSSRPrice.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxSSRPrice  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee paxFee;

    private short[] passengerNumberList;

    public PaxSSRPrice() {
    }

    public PaxSSRPrice(
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee paxFee,
           short[] passengerNumberList) {
           this.paxFee = paxFee;
           this.passengerNumberList = passengerNumberList;
    }


    /**
     * Gets the paxFee value for this PaxSSRPrice.
     * 
     * @return paxFee
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee getPaxFee() {
        return paxFee;
    }


    /**
     * Sets the paxFee value for this PaxSSRPrice.
     * 
     * @param paxFee
     */
    public void setPaxFee(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee paxFee) {
        this.paxFee = paxFee;
    }


    /**
     * Gets the passengerNumberList value for this PaxSSRPrice.
     * 
     * @return passengerNumberList
     */
    public short[] getPassengerNumberList() {
        return passengerNumberList;
    }


    /**
     * Sets the passengerNumberList value for this PaxSSRPrice.
     * 
     * @param passengerNumberList
     */
    public void setPassengerNumberList(short[] passengerNumberList) {
        this.passengerNumberList = passengerNumberList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxSSRPrice)) return false;
        PaxSSRPrice other = (PaxSSRPrice) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paxFee==null && other.getPaxFee()==null) || 
             (this.paxFee!=null &&
              this.paxFee.equals(other.getPaxFee()))) &&
            ((this.passengerNumberList==null && other.getPassengerNumberList()==null) || 
             (this.passengerNumberList!=null &&
              java.util.Arrays.equals(this.passengerNumberList, other.getPassengerNumberList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaxFee() != null) {
            _hashCode += getPaxFee().hashCode();
        }
        if (getPassengerNumberList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerNumberList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerNumberList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxSSRPrice.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSRPrice"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumberList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumberList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "short"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
