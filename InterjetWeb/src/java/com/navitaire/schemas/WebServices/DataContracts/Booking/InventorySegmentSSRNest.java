/**
 * InventorySegmentSSRNest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class InventorySegmentSSRNest  implements java.io.Serializable {
    private java.lang.String SSRNestCode;

    private java.lang.Short minLegLid;

    private java.lang.Short minLegAvailable;

    public InventorySegmentSSRNest() {
    }

    public InventorySegmentSSRNest(
           java.lang.String SSRNestCode,
           java.lang.Short minLegLid,
           java.lang.Short minLegAvailable) {
           this.SSRNestCode = SSRNestCode;
           this.minLegLid = minLegLid;
           this.minLegAvailable = minLegAvailable;
    }


    /**
     * Gets the SSRNestCode value for this InventorySegmentSSRNest.
     * 
     * @return SSRNestCode
     */
    public java.lang.String getSSRNestCode() {
        return SSRNestCode;
    }


    /**
     * Sets the SSRNestCode value for this InventorySegmentSSRNest.
     * 
     * @param SSRNestCode
     */
    public void setSSRNestCode(java.lang.String SSRNestCode) {
        this.SSRNestCode = SSRNestCode;
    }


    /**
     * Gets the minLegLid value for this InventorySegmentSSRNest.
     * 
     * @return minLegLid
     */
    public java.lang.Short getMinLegLid() {
        return minLegLid;
    }


    /**
     * Sets the minLegLid value for this InventorySegmentSSRNest.
     * 
     * @param minLegLid
     */
    public void setMinLegLid(java.lang.Short minLegLid) {
        this.minLegLid = minLegLid;
    }


    /**
     * Gets the minLegAvailable value for this InventorySegmentSSRNest.
     * 
     * @return minLegAvailable
     */
    public java.lang.Short getMinLegAvailable() {
        return minLegAvailable;
    }


    /**
     * Sets the minLegAvailable value for this InventorySegmentSSRNest.
     * 
     * @param minLegAvailable
     */
    public void setMinLegAvailable(java.lang.Short minLegAvailable) {
        this.minLegAvailable = minLegAvailable;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InventorySegmentSSRNest)) return false;
        InventorySegmentSSRNest other = (InventorySegmentSSRNest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.SSRNestCode==null && other.getSSRNestCode()==null) || 
             (this.SSRNestCode!=null &&
              this.SSRNestCode.equals(other.getSSRNestCode()))) &&
            ((this.minLegLid==null && other.getMinLegLid()==null) || 
             (this.minLegLid!=null &&
              this.minLegLid.equals(other.getMinLegLid()))) &&
            ((this.minLegAvailable==null && other.getMinLegAvailable()==null) || 
             (this.minLegAvailable!=null &&
              this.minLegAvailable.equals(other.getMinLegAvailable())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSSRNestCode() != null) {
            _hashCode += getSSRNestCode().hashCode();
        }
        if (getMinLegLid() != null) {
            _hashCode += getMinLegLid().hashCode();
        }
        if (getMinLegAvailable() != null) {
            _hashCode += getMinLegAvailable().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InventorySegmentSSRNest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InventorySegmentSSRNest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRNestCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRNestCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("minLegLid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MinLegLid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("minLegAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MinLegAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
