/**
 * LowFareAvailabilityResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class LowFareAvailabilityResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketLowFare[] dateMarketLowFareList;

    public LowFareAvailabilityResponse() {
    }

    public LowFareAvailabilityResponse(
           com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketLowFare[] dateMarketLowFareList) {
           this.dateMarketLowFareList = dateMarketLowFareList;
    }


    /**
     * Gets the dateMarketLowFareList value for this LowFareAvailabilityResponse.
     * 
     * @return dateMarketLowFareList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketLowFare[] getDateMarketLowFareList() {
        return dateMarketLowFareList;
    }


    /**
     * Sets the dateMarketLowFareList value for this LowFareAvailabilityResponse.
     * 
     * @param dateMarketLowFareList
     */
    public void setDateMarketLowFareList(com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketLowFare[] dateMarketLowFareList) {
        this.dateMarketLowFareList = dateMarketLowFareList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LowFareAvailabilityResponse)) return false;
        LowFareAvailabilityResponse other = (LowFareAvailabilityResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dateMarketLowFareList==null && other.getDateMarketLowFareList()==null) || 
             (this.dateMarketLowFareList!=null &&
              java.util.Arrays.equals(this.dateMarketLowFareList, other.getDateMarketLowFareList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDateMarketLowFareList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDateMarketLowFareList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDateMarketLowFareList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LowFareAvailabilityResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateMarketLowFareList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketLowFareList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketLowFare"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketLowFare"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
