/**
 * SeatAvailabilityRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SeatAvailabilityRequest  implements java.io.Serializable {
    private java.util.Calendar STD;

    private java.lang.String departureStation;

    private java.lang.String arrivalStation;

    private java.lang.Boolean includeSeatFees;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAssignmentMode seatAssignmentMode;

    private java.lang.String flightNumber;

    private java.lang.String carrierCode;

    private java.lang.String opSuffix;

    private java.lang.Boolean compressProperties;

    private java.lang.Boolean enforceSeatGroupRestrictions;

    private long[] passengerIDs;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] passengerSeatPreferences;

    private java.lang.Short seatGroup;

    private short[] seatGroupSettings;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation[] equipmentDeviations;

    private java.lang.Boolean includePropertyLookup;

    private java.lang.String overrideCarrierCode;

    private java.lang.String overrideFlightNumber;

    private java.lang.String overrideOpSuffix;

    private java.util.Calendar overrideSTD;

    private java.lang.String overrideDepartureStation;

    private java.lang.String overrideArrivalStation;

    private java.lang.String collectedCurrencyCode;

    private java.lang.Boolean excludeEquipmentConfiguration;

    public SeatAvailabilityRequest() {
    }

    public SeatAvailabilityRequest(
           java.util.Calendar STD,
           java.lang.String departureStation,
           java.lang.String arrivalStation,
           java.lang.Boolean includeSeatFees,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAssignmentMode seatAssignmentMode,
           java.lang.String flightNumber,
           java.lang.String carrierCode,
           java.lang.String opSuffix,
           java.lang.Boolean compressProperties,
           java.lang.Boolean enforceSeatGroupRestrictions,
           long[] passengerIDs,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] passengerSeatPreferences,
           java.lang.Short seatGroup,
           short[] seatGroupSettings,
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation[] equipmentDeviations,
           java.lang.Boolean includePropertyLookup,
           java.lang.String overrideCarrierCode,
           java.lang.String overrideFlightNumber,
           java.lang.String overrideOpSuffix,
           java.util.Calendar overrideSTD,
           java.lang.String overrideDepartureStation,
           java.lang.String overrideArrivalStation,
           java.lang.String collectedCurrencyCode,
           java.lang.Boolean excludeEquipmentConfiguration) {
           this.STD = STD;
           this.departureStation = departureStation;
           this.arrivalStation = arrivalStation;
           this.includeSeatFees = includeSeatFees;
           this.seatAssignmentMode = seatAssignmentMode;
           this.flightNumber = flightNumber;
           this.carrierCode = carrierCode;
           this.opSuffix = opSuffix;
           this.compressProperties = compressProperties;
           this.enforceSeatGroupRestrictions = enforceSeatGroupRestrictions;
           this.passengerIDs = passengerIDs;
           this.passengerSeatPreferences = passengerSeatPreferences;
           this.seatGroup = seatGroup;
           this.seatGroupSettings = seatGroupSettings;
           this.equipmentDeviations = equipmentDeviations;
           this.includePropertyLookup = includePropertyLookup;
           this.overrideCarrierCode = overrideCarrierCode;
           this.overrideFlightNumber = overrideFlightNumber;
           this.overrideOpSuffix = overrideOpSuffix;
           this.overrideSTD = overrideSTD;
           this.overrideDepartureStation = overrideDepartureStation;
           this.overrideArrivalStation = overrideArrivalStation;
           this.collectedCurrencyCode = collectedCurrencyCode;
           this.excludeEquipmentConfiguration = excludeEquipmentConfiguration;
    }


    /**
     * Gets the STD value for this SeatAvailabilityRequest.
     * 
     * @return STD
     */
    public java.util.Calendar getSTD() {
        return STD;
    }


    /**
     * Sets the STD value for this SeatAvailabilityRequest.
     * 
     * @param STD
     */
    public void setSTD(java.util.Calendar STD) {
        this.STD = STD;
    }


    /**
     * Gets the departureStation value for this SeatAvailabilityRequest.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this SeatAvailabilityRequest.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the arrivalStation value for this SeatAvailabilityRequest.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this SeatAvailabilityRequest.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the includeSeatFees value for this SeatAvailabilityRequest.
     * 
     * @return includeSeatFees
     */
    public java.lang.Boolean getIncludeSeatFees() {
        return includeSeatFees;
    }


    /**
     * Sets the includeSeatFees value for this SeatAvailabilityRequest.
     * 
     * @param includeSeatFees
     */
    public void setIncludeSeatFees(java.lang.Boolean includeSeatFees) {
        this.includeSeatFees = includeSeatFees;
    }


    /**
     * Gets the seatAssignmentMode value for this SeatAvailabilityRequest.
     * 
     * @return seatAssignmentMode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAssignmentMode getSeatAssignmentMode() {
        return seatAssignmentMode;
    }


    /**
     * Sets the seatAssignmentMode value for this SeatAvailabilityRequest.
     * 
     * @param seatAssignmentMode
     */
    public void setSeatAssignmentMode(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAssignmentMode seatAssignmentMode) {
        this.seatAssignmentMode = seatAssignmentMode;
    }


    /**
     * Gets the flightNumber value for this SeatAvailabilityRequest.
     * 
     * @return flightNumber
     */
    public java.lang.String getFlightNumber() {
        return flightNumber;
    }


    /**
     * Sets the flightNumber value for this SeatAvailabilityRequest.
     * 
     * @param flightNumber
     */
    public void setFlightNumber(java.lang.String flightNumber) {
        this.flightNumber = flightNumber;
    }


    /**
     * Gets the carrierCode value for this SeatAvailabilityRequest.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this SeatAvailabilityRequest.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the opSuffix value for this SeatAvailabilityRequest.
     * 
     * @return opSuffix
     */
    public java.lang.String getOpSuffix() {
        return opSuffix;
    }


    /**
     * Sets the opSuffix value for this SeatAvailabilityRequest.
     * 
     * @param opSuffix
     */
    public void setOpSuffix(java.lang.String opSuffix) {
        this.opSuffix = opSuffix;
    }


    /**
     * Gets the compressProperties value for this SeatAvailabilityRequest.
     * 
     * @return compressProperties
     */
    public java.lang.Boolean getCompressProperties() {
        return compressProperties;
    }


    /**
     * Sets the compressProperties value for this SeatAvailabilityRequest.
     * 
     * @param compressProperties
     */
    public void setCompressProperties(java.lang.Boolean compressProperties) {
        this.compressProperties = compressProperties;
    }


    /**
     * Gets the enforceSeatGroupRestrictions value for this SeatAvailabilityRequest.
     * 
     * @return enforceSeatGroupRestrictions
     */
    public java.lang.Boolean getEnforceSeatGroupRestrictions() {
        return enforceSeatGroupRestrictions;
    }


    /**
     * Sets the enforceSeatGroupRestrictions value for this SeatAvailabilityRequest.
     * 
     * @param enforceSeatGroupRestrictions
     */
    public void setEnforceSeatGroupRestrictions(java.lang.Boolean enforceSeatGroupRestrictions) {
        this.enforceSeatGroupRestrictions = enforceSeatGroupRestrictions;
    }


    /**
     * Gets the passengerIDs value for this SeatAvailabilityRequest.
     * 
     * @return passengerIDs
     */
    public long[] getPassengerIDs() {
        return passengerIDs;
    }


    /**
     * Sets the passengerIDs value for this SeatAvailabilityRequest.
     * 
     * @param passengerIDs
     */
    public void setPassengerIDs(long[] passengerIDs) {
        this.passengerIDs = passengerIDs;
    }


    /**
     * Gets the passengerSeatPreferences value for this SeatAvailabilityRequest.
     * 
     * @return passengerSeatPreferences
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] getPassengerSeatPreferences() {
        return passengerSeatPreferences;
    }


    /**
     * Sets the passengerSeatPreferences value for this SeatAvailabilityRequest.
     * 
     * @param passengerSeatPreferences
     */
    public void setPassengerSeatPreferences(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] passengerSeatPreferences) {
        this.passengerSeatPreferences = passengerSeatPreferences;
    }


    /**
     * Gets the seatGroup value for this SeatAvailabilityRequest.
     * 
     * @return seatGroup
     */
    public java.lang.Short getSeatGroup() {
        return seatGroup;
    }


    /**
     * Sets the seatGroup value for this SeatAvailabilityRequest.
     * 
     * @param seatGroup
     */
    public void setSeatGroup(java.lang.Short seatGroup) {
        this.seatGroup = seatGroup;
    }


    /**
     * Gets the seatGroupSettings value for this SeatAvailabilityRequest.
     * 
     * @return seatGroupSettings
     */
    public short[] getSeatGroupSettings() {
        return seatGroupSettings;
    }


    /**
     * Sets the seatGroupSettings value for this SeatAvailabilityRequest.
     * 
     * @param seatGroupSettings
     */
    public void setSeatGroupSettings(short[] seatGroupSettings) {
        this.seatGroupSettings = seatGroupSettings;
    }


    /**
     * Gets the equipmentDeviations value for this SeatAvailabilityRequest.
     * 
     * @return equipmentDeviations
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation[] getEquipmentDeviations() {
        return equipmentDeviations;
    }


    /**
     * Sets the equipmentDeviations value for this SeatAvailabilityRequest.
     * 
     * @param equipmentDeviations
     */
    public void setEquipmentDeviations(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation[] equipmentDeviations) {
        this.equipmentDeviations = equipmentDeviations;
    }


    /**
     * Gets the includePropertyLookup value for this SeatAvailabilityRequest.
     * 
     * @return includePropertyLookup
     */
    public java.lang.Boolean getIncludePropertyLookup() {
        return includePropertyLookup;
    }


    /**
     * Sets the includePropertyLookup value for this SeatAvailabilityRequest.
     * 
     * @param includePropertyLookup
     */
    public void setIncludePropertyLookup(java.lang.Boolean includePropertyLookup) {
        this.includePropertyLookup = includePropertyLookup;
    }


    /**
     * Gets the overrideCarrierCode value for this SeatAvailabilityRequest.
     * 
     * @return overrideCarrierCode
     */
    public java.lang.String getOverrideCarrierCode() {
        return overrideCarrierCode;
    }


    /**
     * Sets the overrideCarrierCode value for this SeatAvailabilityRequest.
     * 
     * @param overrideCarrierCode
     */
    public void setOverrideCarrierCode(java.lang.String overrideCarrierCode) {
        this.overrideCarrierCode = overrideCarrierCode;
    }


    /**
     * Gets the overrideFlightNumber value for this SeatAvailabilityRequest.
     * 
     * @return overrideFlightNumber
     */
    public java.lang.String getOverrideFlightNumber() {
        return overrideFlightNumber;
    }


    /**
     * Sets the overrideFlightNumber value for this SeatAvailabilityRequest.
     * 
     * @param overrideFlightNumber
     */
    public void setOverrideFlightNumber(java.lang.String overrideFlightNumber) {
        this.overrideFlightNumber = overrideFlightNumber;
    }


    /**
     * Gets the overrideOpSuffix value for this SeatAvailabilityRequest.
     * 
     * @return overrideOpSuffix
     */
    public java.lang.String getOverrideOpSuffix() {
        return overrideOpSuffix;
    }


    /**
     * Sets the overrideOpSuffix value for this SeatAvailabilityRequest.
     * 
     * @param overrideOpSuffix
     */
    public void setOverrideOpSuffix(java.lang.String overrideOpSuffix) {
        this.overrideOpSuffix = overrideOpSuffix;
    }


    /**
     * Gets the overrideSTD value for this SeatAvailabilityRequest.
     * 
     * @return overrideSTD
     */
    public java.util.Calendar getOverrideSTD() {
        return overrideSTD;
    }


    /**
     * Sets the overrideSTD value for this SeatAvailabilityRequest.
     * 
     * @param overrideSTD
     */
    public void setOverrideSTD(java.util.Calendar overrideSTD) {
        this.overrideSTD = overrideSTD;
    }


    /**
     * Gets the overrideDepartureStation value for this SeatAvailabilityRequest.
     * 
     * @return overrideDepartureStation
     */
    public java.lang.String getOverrideDepartureStation() {
        return overrideDepartureStation;
    }


    /**
     * Sets the overrideDepartureStation value for this SeatAvailabilityRequest.
     * 
     * @param overrideDepartureStation
     */
    public void setOverrideDepartureStation(java.lang.String overrideDepartureStation) {
        this.overrideDepartureStation = overrideDepartureStation;
    }


    /**
     * Gets the overrideArrivalStation value for this SeatAvailabilityRequest.
     * 
     * @return overrideArrivalStation
     */
    public java.lang.String getOverrideArrivalStation() {
        return overrideArrivalStation;
    }


    /**
     * Sets the overrideArrivalStation value for this SeatAvailabilityRequest.
     * 
     * @param overrideArrivalStation
     */
    public void setOverrideArrivalStation(java.lang.String overrideArrivalStation) {
        this.overrideArrivalStation = overrideArrivalStation;
    }


    /**
     * Gets the collectedCurrencyCode value for this SeatAvailabilityRequest.
     * 
     * @return collectedCurrencyCode
     */
    public java.lang.String getCollectedCurrencyCode() {
        return collectedCurrencyCode;
    }


    /**
     * Sets the collectedCurrencyCode value for this SeatAvailabilityRequest.
     * 
     * @param collectedCurrencyCode
     */
    public void setCollectedCurrencyCode(java.lang.String collectedCurrencyCode) {
        this.collectedCurrencyCode = collectedCurrencyCode;
    }


    /**
     * Gets the excludeEquipmentConfiguration value for this SeatAvailabilityRequest.
     * 
     * @return excludeEquipmentConfiguration
     */
    public java.lang.Boolean getExcludeEquipmentConfiguration() {
        return excludeEquipmentConfiguration;
    }


    /**
     * Sets the excludeEquipmentConfiguration value for this SeatAvailabilityRequest.
     * 
     * @param excludeEquipmentConfiguration
     */
    public void setExcludeEquipmentConfiguration(java.lang.Boolean excludeEquipmentConfiguration) {
        this.excludeEquipmentConfiguration = excludeEquipmentConfiguration;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SeatAvailabilityRequest)) return false;
        SeatAvailabilityRequest other = (SeatAvailabilityRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.STD==null && other.getSTD()==null) || 
             (this.STD!=null &&
              this.STD.equals(other.getSTD()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.includeSeatFees==null && other.getIncludeSeatFees()==null) || 
             (this.includeSeatFees!=null &&
              this.includeSeatFees.equals(other.getIncludeSeatFees()))) &&
            ((this.seatAssignmentMode==null && other.getSeatAssignmentMode()==null) || 
             (this.seatAssignmentMode!=null &&
              this.seatAssignmentMode.equals(other.getSeatAssignmentMode()))) &&
            ((this.flightNumber==null && other.getFlightNumber()==null) || 
             (this.flightNumber!=null &&
              this.flightNumber.equals(other.getFlightNumber()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.opSuffix==null && other.getOpSuffix()==null) || 
             (this.opSuffix!=null &&
              this.opSuffix.equals(other.getOpSuffix()))) &&
            ((this.compressProperties==null && other.getCompressProperties()==null) || 
             (this.compressProperties!=null &&
              this.compressProperties.equals(other.getCompressProperties()))) &&
            ((this.enforceSeatGroupRestrictions==null && other.getEnforceSeatGroupRestrictions()==null) || 
             (this.enforceSeatGroupRestrictions!=null &&
              this.enforceSeatGroupRestrictions.equals(other.getEnforceSeatGroupRestrictions()))) &&
            ((this.passengerIDs==null && other.getPassengerIDs()==null) || 
             (this.passengerIDs!=null &&
              java.util.Arrays.equals(this.passengerIDs, other.getPassengerIDs()))) &&
            ((this.passengerSeatPreferences==null && other.getPassengerSeatPreferences()==null) || 
             (this.passengerSeatPreferences!=null &&
              java.util.Arrays.equals(this.passengerSeatPreferences, other.getPassengerSeatPreferences()))) &&
            ((this.seatGroup==null && other.getSeatGroup()==null) || 
             (this.seatGroup!=null &&
              this.seatGroup.equals(other.getSeatGroup()))) &&
            ((this.seatGroupSettings==null && other.getSeatGroupSettings()==null) || 
             (this.seatGroupSettings!=null &&
              java.util.Arrays.equals(this.seatGroupSettings, other.getSeatGroupSettings()))) &&
            ((this.equipmentDeviations==null && other.getEquipmentDeviations()==null) || 
             (this.equipmentDeviations!=null &&
              java.util.Arrays.equals(this.equipmentDeviations, other.getEquipmentDeviations()))) &&
            ((this.includePropertyLookup==null && other.getIncludePropertyLookup()==null) || 
             (this.includePropertyLookup!=null &&
              this.includePropertyLookup.equals(other.getIncludePropertyLookup()))) &&
            ((this.overrideCarrierCode==null && other.getOverrideCarrierCode()==null) || 
             (this.overrideCarrierCode!=null &&
              this.overrideCarrierCode.equals(other.getOverrideCarrierCode()))) &&
            ((this.overrideFlightNumber==null && other.getOverrideFlightNumber()==null) || 
             (this.overrideFlightNumber!=null &&
              this.overrideFlightNumber.equals(other.getOverrideFlightNumber()))) &&
            ((this.overrideOpSuffix==null && other.getOverrideOpSuffix()==null) || 
             (this.overrideOpSuffix!=null &&
              this.overrideOpSuffix.equals(other.getOverrideOpSuffix()))) &&
            ((this.overrideSTD==null && other.getOverrideSTD()==null) || 
             (this.overrideSTD!=null &&
              this.overrideSTD.equals(other.getOverrideSTD()))) &&
            ((this.overrideDepartureStation==null && other.getOverrideDepartureStation()==null) || 
             (this.overrideDepartureStation!=null &&
              this.overrideDepartureStation.equals(other.getOverrideDepartureStation()))) &&
            ((this.overrideArrivalStation==null && other.getOverrideArrivalStation()==null) || 
             (this.overrideArrivalStation!=null &&
              this.overrideArrivalStation.equals(other.getOverrideArrivalStation()))) &&
            ((this.collectedCurrencyCode==null && other.getCollectedCurrencyCode()==null) || 
             (this.collectedCurrencyCode!=null &&
              this.collectedCurrencyCode.equals(other.getCollectedCurrencyCode()))) &&
            ((this.excludeEquipmentConfiguration==null && other.getExcludeEquipmentConfiguration()==null) || 
             (this.excludeEquipmentConfiguration!=null &&
              this.excludeEquipmentConfiguration.equals(other.getExcludeEquipmentConfiguration())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSTD() != null) {
            _hashCode += getSTD().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getIncludeSeatFees() != null) {
            _hashCode += getIncludeSeatFees().hashCode();
        }
        if (getSeatAssignmentMode() != null) {
            _hashCode += getSeatAssignmentMode().hashCode();
        }
        if (getFlightNumber() != null) {
            _hashCode += getFlightNumber().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getOpSuffix() != null) {
            _hashCode += getOpSuffix().hashCode();
        }
        if (getCompressProperties() != null) {
            _hashCode += getCompressProperties().hashCode();
        }
        if (getEnforceSeatGroupRestrictions() != null) {
            _hashCode += getEnforceSeatGroupRestrictions().hashCode();
        }
        if (getPassengerIDs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerIDs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerIDs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerSeatPreferences() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerSeatPreferences());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerSeatPreferences(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSeatGroup() != null) {
            _hashCode += getSeatGroup().hashCode();
        }
        if (getSeatGroupSettings() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSeatGroupSettings());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSeatGroupSettings(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEquipmentDeviations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEquipmentDeviations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEquipmentDeviations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIncludePropertyLookup() != null) {
            _hashCode += getIncludePropertyLookup().hashCode();
        }
        if (getOverrideCarrierCode() != null) {
            _hashCode += getOverrideCarrierCode().hashCode();
        }
        if (getOverrideFlightNumber() != null) {
            _hashCode += getOverrideFlightNumber().hashCode();
        }
        if (getOverrideOpSuffix() != null) {
            _hashCode += getOverrideOpSuffix().hashCode();
        }
        if (getOverrideSTD() != null) {
            _hashCode += getOverrideSTD().hashCode();
        }
        if (getOverrideDepartureStation() != null) {
            _hashCode += getOverrideDepartureStation().hashCode();
        }
        if (getOverrideArrivalStation() != null) {
            _hashCode += getOverrideArrivalStation().hashCode();
        }
        if (getCollectedCurrencyCode() != null) {
            _hashCode += getCollectedCurrencyCode().hashCode();
        }
        if (getExcludeEquipmentConfiguration() != null) {
            _hashCode += getExcludeEquipmentConfiguration().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SeatAvailabilityRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailabilityRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeSeatFees");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeSeatFees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatAssignmentMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAssignmentMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SeatAssignmentMode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OpSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("compressProperties");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompressProperties"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enforceSeatGroupRestrictions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EnforceSeatGroupRestrictions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerIDs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerIDs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "long"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerSeatPreferences");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerSeatPreferences"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatGroup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatGroup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatGroupSettings");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatGroupSettings"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "short"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentDeviations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviation"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePropertyLookup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludePropertyLookup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideCarrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideCarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideFlightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideFlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideOpSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideOpSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideSTD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideSTD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideDepartureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideDepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideArrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("excludeEquipmentConfiguration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ExcludeEquipmentConfiguration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
