/**
 * TermsConditions.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class TermsConditions  implements java.io.Serializable {
    private java.lang.String termsConditionsCode;

    private java.lang.String cultureCode;

    private java.lang.String description;

    private java.lang.String terms;

    private java.lang.Integer displaySequence;

    public TermsConditions() {
    }

    public TermsConditions(
           java.lang.String termsConditionsCode,
           java.lang.String cultureCode,
           java.lang.String description,
           java.lang.String terms,
           java.lang.Integer displaySequence) {
           this.termsConditionsCode = termsConditionsCode;
           this.cultureCode = cultureCode;
           this.description = description;
           this.terms = terms;
           this.displaySequence = displaySequence;
    }


    /**
     * Gets the termsConditionsCode value for this TermsConditions.
     * 
     * @return termsConditionsCode
     */
    public java.lang.String getTermsConditionsCode() {
        return termsConditionsCode;
    }


    /**
     * Sets the termsConditionsCode value for this TermsConditions.
     * 
     * @param termsConditionsCode
     */
    public void setTermsConditionsCode(java.lang.String termsConditionsCode) {
        this.termsConditionsCode = termsConditionsCode;
    }


    /**
     * Gets the cultureCode value for this TermsConditions.
     * 
     * @return cultureCode
     */
    public java.lang.String getCultureCode() {
        return cultureCode;
    }


    /**
     * Sets the cultureCode value for this TermsConditions.
     * 
     * @param cultureCode
     */
    public void setCultureCode(java.lang.String cultureCode) {
        this.cultureCode = cultureCode;
    }


    /**
     * Gets the description value for this TermsConditions.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this TermsConditions.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the terms value for this TermsConditions.
     * 
     * @return terms
     */
    public java.lang.String getTerms() {
        return terms;
    }


    /**
     * Sets the terms value for this TermsConditions.
     * 
     * @param terms
     */
    public void setTerms(java.lang.String terms) {
        this.terms = terms;
    }


    /**
     * Gets the displaySequence value for this TermsConditions.
     * 
     * @return displaySequence
     */
    public java.lang.Integer getDisplaySequence() {
        return displaySequence;
    }


    /**
     * Sets the displaySequence value for this TermsConditions.
     * 
     * @param displaySequence
     */
    public void setDisplaySequence(java.lang.Integer displaySequence) {
        this.displaySequence = displaySequence;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TermsConditions)) return false;
        TermsConditions other = (TermsConditions) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.termsConditionsCode==null && other.getTermsConditionsCode()==null) || 
             (this.termsConditionsCode!=null &&
              this.termsConditionsCode.equals(other.getTermsConditionsCode()))) &&
            ((this.cultureCode==null && other.getCultureCode()==null) || 
             (this.cultureCode!=null &&
              this.cultureCode.equals(other.getCultureCode()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.terms==null && other.getTerms()==null) || 
             (this.terms!=null &&
              this.terms.equals(other.getTerms()))) &&
            ((this.displaySequence==null && other.getDisplaySequence()==null) || 
             (this.displaySequence!=null &&
              this.displaySequence.equals(other.getDisplaySequence())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTermsConditionsCode() != null) {
            _hashCode += getTermsConditionsCode().hashCode();
        }
        if (getCultureCode() != null) {
            _hashCode += getCultureCode().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getTerms() != null) {
            _hashCode += getTerms().hashCode();
        }
        if (getDisplaySequence() != null) {
            _hashCode += getDisplaySequence().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TermsConditions.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditions"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("termsConditionsCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditionsCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cultureCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CultureCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terms");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Terms"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displaySequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DisplaySequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
