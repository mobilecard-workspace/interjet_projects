/**
 * PaymentVoucher.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaymentVoucher  implements java.io.Serializable {
    private java.lang.Long voucherIDField;

    private java.lang.Long voucherTransaction;

    private java.lang.Boolean overrideVoucherRestrictions;

    private java.lang.Boolean overrideAmount;

    private java.lang.String recordLocator;

    public PaymentVoucher() {
    }

    public PaymentVoucher(
           java.lang.Long voucherIDField,
           java.lang.Long voucherTransaction,
           java.lang.Boolean overrideVoucherRestrictions,
           java.lang.Boolean overrideAmount,
           java.lang.String recordLocator) {
           this.voucherIDField = voucherIDField;
           this.voucherTransaction = voucherTransaction;
           this.overrideVoucherRestrictions = overrideVoucherRestrictions;
           this.overrideAmount = overrideAmount;
           this.recordLocator = recordLocator;
    }


    /**
     * Gets the voucherIDField value for this PaymentVoucher.
     * 
     * @return voucherIDField
     */
    public java.lang.Long getVoucherIDField() {
        return voucherIDField;
    }


    /**
     * Sets the voucherIDField value for this PaymentVoucher.
     * 
     * @param voucherIDField
     */
    public void setVoucherIDField(java.lang.Long voucherIDField) {
        this.voucherIDField = voucherIDField;
    }


    /**
     * Gets the voucherTransaction value for this PaymentVoucher.
     * 
     * @return voucherTransaction
     */
    public java.lang.Long getVoucherTransaction() {
        return voucherTransaction;
    }


    /**
     * Sets the voucherTransaction value for this PaymentVoucher.
     * 
     * @param voucherTransaction
     */
    public void setVoucherTransaction(java.lang.Long voucherTransaction) {
        this.voucherTransaction = voucherTransaction;
    }


    /**
     * Gets the overrideVoucherRestrictions value for this PaymentVoucher.
     * 
     * @return overrideVoucherRestrictions
     */
    public java.lang.Boolean getOverrideVoucherRestrictions() {
        return overrideVoucherRestrictions;
    }


    /**
     * Sets the overrideVoucherRestrictions value for this PaymentVoucher.
     * 
     * @param overrideVoucherRestrictions
     */
    public void setOverrideVoucherRestrictions(java.lang.Boolean overrideVoucherRestrictions) {
        this.overrideVoucherRestrictions = overrideVoucherRestrictions;
    }


    /**
     * Gets the overrideAmount value for this PaymentVoucher.
     * 
     * @return overrideAmount
     */
    public java.lang.Boolean getOverrideAmount() {
        return overrideAmount;
    }


    /**
     * Sets the overrideAmount value for this PaymentVoucher.
     * 
     * @param overrideAmount
     */
    public void setOverrideAmount(java.lang.Boolean overrideAmount) {
        this.overrideAmount = overrideAmount;
    }


    /**
     * Gets the recordLocator value for this PaymentVoucher.
     * 
     * @return recordLocator
     */
    public java.lang.String getRecordLocator() {
        return recordLocator;
    }


    /**
     * Sets the recordLocator value for this PaymentVoucher.
     * 
     * @param recordLocator
     */
    public void setRecordLocator(java.lang.String recordLocator) {
        this.recordLocator = recordLocator;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentVoucher)) return false;
        PaymentVoucher other = (PaymentVoucher) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.voucherIDField==null && other.getVoucherIDField()==null) || 
             (this.voucherIDField!=null &&
              this.voucherIDField.equals(other.getVoucherIDField()))) &&
            ((this.voucherTransaction==null && other.getVoucherTransaction()==null) || 
             (this.voucherTransaction!=null &&
              this.voucherTransaction.equals(other.getVoucherTransaction()))) &&
            ((this.overrideVoucherRestrictions==null && other.getOverrideVoucherRestrictions()==null) || 
             (this.overrideVoucherRestrictions!=null &&
              this.overrideVoucherRestrictions.equals(other.getOverrideVoucherRestrictions()))) &&
            ((this.overrideAmount==null && other.getOverrideAmount()==null) || 
             (this.overrideAmount!=null &&
              this.overrideAmount.equals(other.getOverrideAmount()))) &&
            ((this.recordLocator==null && other.getRecordLocator()==null) || 
             (this.recordLocator!=null &&
              this.recordLocator.equals(other.getRecordLocator())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVoucherIDField() != null) {
            _hashCode += getVoucherIDField().hashCode();
        }
        if (getVoucherTransaction() != null) {
            _hashCode += getVoucherTransaction().hashCode();
        }
        if (getOverrideVoucherRestrictions() != null) {
            _hashCode += getOverrideVoucherRestrictions().hashCode();
        }
        if (getOverrideAmount() != null) {
            _hashCode += getOverrideAmount().hashCode();
        }
        if (getRecordLocator() != null) {
            _hashCode += getRecordLocator().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentVoucher.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentVoucher"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voucherIDField");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "VoucherIDField"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voucherTransaction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "VoucherTransaction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideVoucherRestrictions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideVoucherRestrictions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
