/**
 * DiscountTarget.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class DiscountTarget  implements java.io.Serializable {
    private java.lang.Boolean listPrice;

    private java.lang.Boolean fees;

    private java.lang.Boolean taxes;

    private java.lang.Boolean personalizations;

    public DiscountTarget() {
    }

    public DiscountTarget(
           java.lang.Boolean listPrice,
           java.lang.Boolean fees,
           java.lang.Boolean taxes,
           java.lang.Boolean personalizations) {
           this.listPrice = listPrice;
           this.fees = fees;
           this.taxes = taxes;
           this.personalizations = personalizations;
    }


    /**
     * Gets the listPrice value for this DiscountTarget.
     * 
     * @return listPrice
     */
    public java.lang.Boolean getListPrice() {
        return listPrice;
    }


    /**
     * Sets the listPrice value for this DiscountTarget.
     * 
     * @param listPrice
     */
    public void setListPrice(java.lang.Boolean listPrice) {
        this.listPrice = listPrice;
    }


    /**
     * Gets the fees value for this DiscountTarget.
     * 
     * @return fees
     */
    public java.lang.Boolean getFees() {
        return fees;
    }


    /**
     * Sets the fees value for this DiscountTarget.
     * 
     * @param fees
     */
    public void setFees(java.lang.Boolean fees) {
        this.fees = fees;
    }


    /**
     * Gets the taxes value for this DiscountTarget.
     * 
     * @return taxes
     */
    public java.lang.Boolean getTaxes() {
        return taxes;
    }


    /**
     * Sets the taxes value for this DiscountTarget.
     * 
     * @param taxes
     */
    public void setTaxes(java.lang.Boolean taxes) {
        this.taxes = taxes;
    }


    /**
     * Gets the personalizations value for this DiscountTarget.
     * 
     * @return personalizations
     */
    public java.lang.Boolean getPersonalizations() {
        return personalizations;
    }


    /**
     * Sets the personalizations value for this DiscountTarget.
     * 
     * @param personalizations
     */
    public void setPersonalizations(java.lang.Boolean personalizations) {
        this.personalizations = personalizations;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DiscountTarget)) return false;
        DiscountTarget other = (DiscountTarget) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.listPrice==null && other.getListPrice()==null) || 
             (this.listPrice!=null &&
              this.listPrice.equals(other.getListPrice()))) &&
            ((this.fees==null && other.getFees()==null) || 
             (this.fees!=null &&
              this.fees.equals(other.getFees()))) &&
            ((this.taxes==null && other.getTaxes()==null) || 
             (this.taxes!=null &&
              this.taxes.equals(other.getTaxes()))) &&
            ((this.personalizations==null && other.getPersonalizations()==null) || 
             (this.personalizations!=null &&
              this.personalizations.equals(other.getPersonalizations())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getListPrice() != null) {
            _hashCode += getListPrice().hashCode();
        }
        if (getFees() != null) {
            _hashCode += getFees().hashCode();
        }
        if (getTaxes() != null) {
            _hashCode += getTaxes().hashCode();
        }
        if (getPersonalizations() != null) {
            _hashCode += getPersonalizations().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DiscountTarget.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DiscountTarget"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ListPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fees");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Fees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Taxes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personalizations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Personalizations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
