/**
 * PassengerFee.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PassengerFee  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String actionStatusCode;

    private java.lang.String feeCode;

    private java.lang.String feeDetail;

    private java.lang.Short feeNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FeeType feeType;

    private java.lang.Boolean feeOverride;

    private java.lang.String flightReference;

    private java.lang.String note;

    private java.lang.String SSRCode;

    private java.lang.Short SSRNumber;

    private java.lang.Short paymentNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge[] serviceCharges;

    private java.util.Calendar createdDate;

    private java.lang.Boolean isProtected;

    public PassengerFee() {
    }

    public PassengerFee(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String actionStatusCode,
           java.lang.String feeCode,
           java.lang.String feeDetail,
           java.lang.Short feeNumber,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FeeType feeType,
           java.lang.Boolean feeOverride,
           java.lang.String flightReference,
           java.lang.String note,
           java.lang.String SSRCode,
           java.lang.Short SSRNumber,
           java.lang.Short paymentNumber,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge[] serviceCharges,
           java.util.Calendar createdDate,
           java.lang.Boolean isProtected) {
        super(
            state);
        this.actionStatusCode = actionStatusCode;
        this.feeCode = feeCode;
        this.feeDetail = feeDetail;
        this.feeNumber = feeNumber;
        this.feeType = feeType;
        this.feeOverride = feeOverride;
        this.flightReference = flightReference;
        this.note = note;
        this.SSRCode = SSRCode;
        this.SSRNumber = SSRNumber;
        this.paymentNumber = paymentNumber;
        this.serviceCharges = serviceCharges;
        this.createdDate = createdDate;
        this.isProtected = isProtected;
    }


    /**
     * Gets the actionStatusCode value for this PassengerFee.
     * 
     * @return actionStatusCode
     */
    public java.lang.String getActionStatusCode() {
        return actionStatusCode;
    }


    /**
     * Sets the actionStatusCode value for this PassengerFee.
     * 
     * @param actionStatusCode
     */
    public void setActionStatusCode(java.lang.String actionStatusCode) {
        this.actionStatusCode = actionStatusCode;
    }


    /**
     * Gets the feeCode value for this PassengerFee.
     * 
     * @return feeCode
     */
    public java.lang.String getFeeCode() {
        return feeCode;
    }


    /**
     * Sets the feeCode value for this PassengerFee.
     * 
     * @param feeCode
     */
    public void setFeeCode(java.lang.String feeCode) {
        this.feeCode = feeCode;
    }


    /**
     * Gets the feeDetail value for this PassengerFee.
     * 
     * @return feeDetail
     */
    public java.lang.String getFeeDetail() {
        return feeDetail;
    }


    /**
     * Sets the feeDetail value for this PassengerFee.
     * 
     * @param feeDetail
     */
    public void setFeeDetail(java.lang.String feeDetail) {
        this.feeDetail = feeDetail;
    }


    /**
     * Gets the feeNumber value for this PassengerFee.
     * 
     * @return feeNumber
     */
    public java.lang.Short getFeeNumber() {
        return feeNumber;
    }


    /**
     * Sets the feeNumber value for this PassengerFee.
     * 
     * @param feeNumber
     */
    public void setFeeNumber(java.lang.Short feeNumber) {
        this.feeNumber = feeNumber;
    }


    /**
     * Gets the feeType value for this PassengerFee.
     * 
     * @return feeType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FeeType getFeeType() {
        return feeType;
    }


    /**
     * Sets the feeType value for this PassengerFee.
     * 
     * @param feeType
     */
    public void setFeeType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FeeType feeType) {
        this.feeType = feeType;
    }


    /**
     * Gets the feeOverride value for this PassengerFee.
     * 
     * @return feeOverride
     */
    public java.lang.Boolean getFeeOverride() {
        return feeOverride;
    }


    /**
     * Sets the feeOverride value for this PassengerFee.
     * 
     * @param feeOverride
     */
    public void setFeeOverride(java.lang.Boolean feeOverride) {
        this.feeOverride = feeOverride;
    }


    /**
     * Gets the flightReference value for this PassengerFee.
     * 
     * @return flightReference
     */
    public java.lang.String getFlightReference() {
        return flightReference;
    }


    /**
     * Sets the flightReference value for this PassengerFee.
     * 
     * @param flightReference
     */
    public void setFlightReference(java.lang.String flightReference) {
        this.flightReference = flightReference;
    }


    /**
     * Gets the note value for this PassengerFee.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this PassengerFee.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }


    /**
     * Gets the SSRCode value for this PassengerFee.
     * 
     * @return SSRCode
     */
    public java.lang.String getSSRCode() {
        return SSRCode;
    }


    /**
     * Sets the SSRCode value for this PassengerFee.
     * 
     * @param SSRCode
     */
    public void setSSRCode(java.lang.String SSRCode) {
        this.SSRCode = SSRCode;
    }


    /**
     * Gets the SSRNumber value for this PassengerFee.
     * 
     * @return SSRNumber
     */
    public java.lang.Short getSSRNumber() {
        return SSRNumber;
    }


    /**
     * Sets the SSRNumber value for this PassengerFee.
     * 
     * @param SSRNumber
     */
    public void setSSRNumber(java.lang.Short SSRNumber) {
        this.SSRNumber = SSRNumber;
    }


    /**
     * Gets the paymentNumber value for this PassengerFee.
     * 
     * @return paymentNumber
     */
    public java.lang.Short getPaymentNumber() {
        return paymentNumber;
    }


    /**
     * Sets the paymentNumber value for this PassengerFee.
     * 
     * @param paymentNumber
     */
    public void setPaymentNumber(java.lang.Short paymentNumber) {
        this.paymentNumber = paymentNumber;
    }


    /**
     * Gets the serviceCharges value for this PassengerFee.
     * 
     * @return serviceCharges
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge[] getServiceCharges() {
        return serviceCharges;
    }


    /**
     * Sets the serviceCharges value for this PassengerFee.
     * 
     * @param serviceCharges
     */
    public void setServiceCharges(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge[] serviceCharges) {
        this.serviceCharges = serviceCharges;
    }


    /**
     * Gets the createdDate value for this PassengerFee.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this PassengerFee.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }


    /**
     * Gets the isProtected value for this PassengerFee.
     * 
     * @return isProtected
     */
    public java.lang.Boolean getIsProtected() {
        return isProtected;
    }


    /**
     * Sets the isProtected value for this PassengerFee.
     * 
     * @param isProtected
     */
    public void setIsProtected(java.lang.Boolean isProtected) {
        this.isProtected = isProtected;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PassengerFee)) return false;
        PassengerFee other = (PassengerFee) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.actionStatusCode==null && other.getActionStatusCode()==null) || 
             (this.actionStatusCode!=null &&
              this.actionStatusCode.equals(other.getActionStatusCode()))) &&
            ((this.feeCode==null && other.getFeeCode()==null) || 
             (this.feeCode!=null &&
              this.feeCode.equals(other.getFeeCode()))) &&
            ((this.feeDetail==null && other.getFeeDetail()==null) || 
             (this.feeDetail!=null &&
              this.feeDetail.equals(other.getFeeDetail()))) &&
            ((this.feeNumber==null && other.getFeeNumber()==null) || 
             (this.feeNumber!=null &&
              this.feeNumber.equals(other.getFeeNumber()))) &&
            ((this.feeType==null && other.getFeeType()==null) || 
             (this.feeType!=null &&
              this.feeType.equals(other.getFeeType()))) &&
            ((this.feeOverride==null && other.getFeeOverride()==null) || 
             (this.feeOverride!=null &&
              this.feeOverride.equals(other.getFeeOverride()))) &&
            ((this.flightReference==null && other.getFlightReference()==null) || 
             (this.flightReference!=null &&
              this.flightReference.equals(other.getFlightReference()))) &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote()))) &&
            ((this.SSRCode==null && other.getSSRCode()==null) || 
             (this.SSRCode!=null &&
              this.SSRCode.equals(other.getSSRCode()))) &&
            ((this.SSRNumber==null && other.getSSRNumber()==null) || 
             (this.SSRNumber!=null &&
              this.SSRNumber.equals(other.getSSRNumber()))) &&
            ((this.paymentNumber==null && other.getPaymentNumber()==null) || 
             (this.paymentNumber!=null &&
              this.paymentNumber.equals(other.getPaymentNumber()))) &&
            ((this.serviceCharges==null && other.getServiceCharges()==null) || 
             (this.serviceCharges!=null &&
              java.util.Arrays.equals(this.serviceCharges, other.getServiceCharges()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate()))) &&
            ((this.isProtected==null && other.getIsProtected()==null) || 
             (this.isProtected!=null &&
              this.isProtected.equals(other.getIsProtected())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getActionStatusCode() != null) {
            _hashCode += getActionStatusCode().hashCode();
        }
        if (getFeeCode() != null) {
            _hashCode += getFeeCode().hashCode();
        }
        if (getFeeDetail() != null) {
            _hashCode += getFeeDetail().hashCode();
        }
        if (getFeeNumber() != null) {
            _hashCode += getFeeNumber().hashCode();
        }
        if (getFeeType() != null) {
            _hashCode += getFeeType().hashCode();
        }
        if (getFeeOverride() != null) {
            _hashCode += getFeeOverride().hashCode();
        }
        if (getFlightReference() != null) {
            _hashCode += getFlightReference().hashCode();
        }
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        if (getSSRCode() != null) {
            _hashCode += getSSRCode().hashCode();
        }
        if (getSSRNumber() != null) {
            _hashCode += getSSRNumber().hashCode();
        }
        if (getPaymentNumber() != null) {
            _hashCode += getPaymentNumber().hashCode();
        }
        if (getServiceCharges() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getServiceCharges());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getServiceCharges(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        if (getIsProtected() != null) {
            _hashCode += getIsProtected().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PassengerFee.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FeeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeOverride");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeOverride"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceCharges");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ServiceCharges"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingServiceCharge"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingServiceCharge"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isProtected");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IsProtected"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
