/**
 * GetBookingStatelessFilteredRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GetBookingStatelessFilteredRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingFilteredBy getBookingStatelessFilteredBy;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetByID getByID;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFilter getBookingFilter;

    public GetBookingStatelessFilteredRequestData() {
    }

    public GetBookingStatelessFilteredRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingFilteredBy getBookingStatelessFilteredBy,
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetByID getByID,
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFilter getBookingFilter) {
           this.getBookingStatelessFilteredBy = getBookingStatelessFilteredBy;
           this.getByRecordLocator = getByRecordLocator;
           this.getByID = getByID;
           this.getBookingFilter = getBookingFilter;
    }


    /**
     * Gets the getBookingStatelessFilteredBy value for this GetBookingStatelessFilteredRequestData.
     * 
     * @return getBookingStatelessFilteredBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingFilteredBy getGetBookingStatelessFilteredBy() {
        return getBookingStatelessFilteredBy;
    }


    /**
     * Sets the getBookingStatelessFilteredBy value for this GetBookingStatelessFilteredRequestData.
     * 
     * @param getBookingStatelessFilteredBy
     */
    public void setGetBookingStatelessFilteredBy(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingFilteredBy getBookingStatelessFilteredBy) {
        this.getBookingStatelessFilteredBy = getBookingStatelessFilteredBy;
    }


    /**
     * Gets the getByRecordLocator value for this GetBookingStatelessFilteredRequestData.
     * 
     * @return getByRecordLocator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getGetByRecordLocator() {
        return getByRecordLocator;
    }


    /**
     * Sets the getByRecordLocator value for this GetBookingStatelessFilteredRequestData.
     * 
     * @param getByRecordLocator
     */
    public void setGetByRecordLocator(com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator) {
        this.getByRecordLocator = getByRecordLocator;
    }


    /**
     * Gets the getByID value for this GetBookingStatelessFilteredRequestData.
     * 
     * @return getByID
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetByID getGetByID() {
        return getByID;
    }


    /**
     * Sets the getByID value for this GetBookingStatelessFilteredRequestData.
     * 
     * @param getByID
     */
    public void setGetByID(com.navitaire.schemas.WebServices.DataContracts.Booking.GetByID getByID) {
        this.getByID = getByID;
    }


    /**
     * Gets the getBookingFilter value for this GetBookingStatelessFilteredRequestData.
     * 
     * @return getBookingFilter
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFilter getGetBookingFilter() {
        return getBookingFilter;
    }


    /**
     * Sets the getBookingFilter value for this GetBookingStatelessFilteredRequestData.
     * 
     * @param getBookingFilter
     */
    public void setGetBookingFilter(com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFilter getBookingFilter) {
        this.getBookingFilter = getBookingFilter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBookingStatelessFilteredRequestData)) return false;
        GetBookingStatelessFilteredRequestData other = (GetBookingStatelessFilteredRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getBookingStatelessFilteredBy==null && other.getGetBookingStatelessFilteredBy()==null) || 
             (this.getBookingStatelessFilteredBy!=null &&
              this.getBookingStatelessFilteredBy.equals(other.getGetBookingStatelessFilteredBy()))) &&
            ((this.getByRecordLocator==null && other.getGetByRecordLocator()==null) || 
             (this.getByRecordLocator!=null &&
              this.getByRecordLocator.equals(other.getGetByRecordLocator()))) &&
            ((this.getByID==null && other.getGetByID()==null) || 
             (this.getByID!=null &&
              this.getByID.equals(other.getGetByID()))) &&
            ((this.getBookingFilter==null && other.getGetBookingFilter()==null) || 
             (this.getBookingFilter!=null &&
              this.getBookingFilter.equals(other.getGetBookingFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetBookingStatelessFilteredBy() != null) {
            _hashCode += getGetBookingStatelessFilteredBy().hashCode();
        }
        if (getGetByRecordLocator() != null) {
            _hashCode += getGetByRecordLocator().hashCode();
        }
        if (getGetByID() != null) {
            _hashCode += getGetByID().hashCode();
        }
        if (getGetBookingFilter() != null) {
            _hashCode += getGetBookingFilter().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBookingStatelessFilteredRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingStatelessFilteredRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getBookingStatelessFilteredBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingStatelessFilteredBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "GetBookingFilteredBy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getByRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByRecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getByID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByID"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getBookingFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
