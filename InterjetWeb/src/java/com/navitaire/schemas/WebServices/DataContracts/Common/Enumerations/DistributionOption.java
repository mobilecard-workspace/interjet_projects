/**
 * DistributionOption.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class DistributionOption implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected DistributionOption(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _None = "None";
    public static final java.lang.String _Mail = "Mail";
    public static final java.lang.String _Email = "Email";
    public static final java.lang.String _Fax = "Fax";
    public static final java.lang.String _MailFax = "MailFax";
    public static final java.lang.String _Airport = "Airport";
    public static final java.lang.String _Hold = "Hold";
    public static final java.lang.String _Print = "Print";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final DistributionOption None = new DistributionOption(_None);
    public static final DistributionOption Mail = new DistributionOption(_Mail);
    public static final DistributionOption Email = new DistributionOption(_Email);
    public static final DistributionOption Fax = new DistributionOption(_Fax);
    public static final DistributionOption MailFax = new DistributionOption(_MailFax);
    public static final DistributionOption Airport = new DistributionOption(_Airport);
    public static final DistributionOption Hold = new DistributionOption(_Hold);
    public static final DistributionOption Print = new DistributionOption(_Print);
    public static final DistributionOption Unmapped = new DistributionOption(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static DistributionOption fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        DistributionOption enumeration = (DistributionOption)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static DistributionOption fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DistributionOption.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DistributionOption"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
