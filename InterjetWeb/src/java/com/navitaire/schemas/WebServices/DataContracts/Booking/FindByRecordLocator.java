/**
 * FindByRecordLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class FindByRecordLocator  implements java.io.Serializable {
    private java.lang.String recordLocator;

    private java.lang.String sourceOrganization;

    private java.lang.String organizationGroupCode;

    public FindByRecordLocator() {
    }

    public FindByRecordLocator(
           java.lang.String recordLocator,
           java.lang.String sourceOrganization,
           java.lang.String organizationGroupCode) {
           this.recordLocator = recordLocator;
           this.sourceOrganization = sourceOrganization;
           this.organizationGroupCode = organizationGroupCode;
    }


    /**
     * Gets the recordLocator value for this FindByRecordLocator.
     * 
     * @return recordLocator
     */
    public java.lang.String getRecordLocator() {
        return recordLocator;
    }


    /**
     * Sets the recordLocator value for this FindByRecordLocator.
     * 
     * @param recordLocator
     */
    public void setRecordLocator(java.lang.String recordLocator) {
        this.recordLocator = recordLocator;
    }


    /**
     * Gets the sourceOrganization value for this FindByRecordLocator.
     * 
     * @return sourceOrganization
     */
    public java.lang.String getSourceOrganization() {
        return sourceOrganization;
    }


    /**
     * Sets the sourceOrganization value for this FindByRecordLocator.
     * 
     * @param sourceOrganization
     */
    public void setSourceOrganization(java.lang.String sourceOrganization) {
        this.sourceOrganization = sourceOrganization;
    }


    /**
     * Gets the organizationGroupCode value for this FindByRecordLocator.
     * 
     * @return organizationGroupCode
     */
    public java.lang.String getOrganizationGroupCode() {
        return organizationGroupCode;
    }


    /**
     * Sets the organizationGroupCode value for this FindByRecordLocator.
     * 
     * @param organizationGroupCode
     */
    public void setOrganizationGroupCode(java.lang.String organizationGroupCode) {
        this.organizationGroupCode = organizationGroupCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindByRecordLocator)) return false;
        FindByRecordLocator other = (FindByRecordLocator) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.recordLocator==null && other.getRecordLocator()==null) || 
             (this.recordLocator!=null &&
              this.recordLocator.equals(other.getRecordLocator()))) &&
            ((this.sourceOrganization==null && other.getSourceOrganization()==null) || 
             (this.sourceOrganization!=null &&
              this.sourceOrganization.equals(other.getSourceOrganization()))) &&
            ((this.organizationGroupCode==null && other.getOrganizationGroupCode()==null) || 
             (this.organizationGroupCode!=null &&
              this.organizationGroupCode.equals(other.getOrganizationGroupCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRecordLocator() != null) {
            _hashCode += getRecordLocator().hashCode();
        }
        if (getSourceOrganization() != null) {
            _hashCode += getSourceOrganization().hashCode();
        }
        if (getOrganizationGroupCode() != null) {
            _hashCode += getOrganizationGroupCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindByRecordLocator.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByRecordLocator"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceOrganization");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceOrganization"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organizationGroupCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OrganizationGroupCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
