/**
 * LowFareTripAvailabilityRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class LowFareTripAvailabilityRequest  implements java.io.Serializable {
    private java.lang.Boolean bypassCache;

    private java.lang.Boolean includeTaxesAndFees;

    private java.lang.Boolean groupBydate;

    private java.lang.Integer parameterSetID;

    private java.lang.String currencyCode;

    private java.lang.String sourceOrganizationCode;

    private java.lang.String paxResidentCountry;

    private java.lang.String promotionCode;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAvailabilityRequest[] lowFareAvailabilityRequestList;

    private java.lang.String[] bookingClassList;

    private java.lang.String[] productClassList;

    private java.lang.String[] fareTypeList;

    private org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter;

    private org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LowFareFlightFilter flightFilter;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFareRequest[] alternateLowFareRequestList;

    private java.lang.Boolean getAllDetails;

    public LowFareTripAvailabilityRequest() {
    }

    public LowFareTripAvailabilityRequest(
           java.lang.Boolean bypassCache,
           java.lang.Boolean includeTaxesAndFees,
           java.lang.Boolean groupBydate,
           java.lang.Integer parameterSetID,
           java.lang.String currencyCode,
           java.lang.String sourceOrganizationCode,
           java.lang.String paxResidentCountry,
           java.lang.String promotionCode,
           com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAvailabilityRequest[] lowFareAvailabilityRequestList,
           java.lang.String[] bookingClassList,
           java.lang.String[] productClassList,
           java.lang.String[] fareTypeList,
           org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter,
           org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LowFareFlightFilter flightFilter,
           com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFareRequest[] alternateLowFareRequestList,
           java.lang.Boolean getAllDetails) {
           this.bypassCache = bypassCache;
           this.includeTaxesAndFees = includeTaxesAndFees;
           this.groupBydate = groupBydate;
           this.parameterSetID = parameterSetID;
           this.currencyCode = currencyCode;
           this.sourceOrganizationCode = sourceOrganizationCode;
           this.paxResidentCountry = paxResidentCountry;
           this.promotionCode = promotionCode;
           this.lowFareAvailabilityRequestList = lowFareAvailabilityRequestList;
           this.bookingClassList = bookingClassList;
           this.productClassList = productClassList;
           this.fareTypeList = fareTypeList;
           this.loyaltyFilter = loyaltyFilter;
           this.flightFilter = flightFilter;
           this.alternateLowFareRequestList = alternateLowFareRequestList;
           this.getAllDetails = getAllDetails;
    }


    /**
     * Gets the bypassCache value for this LowFareTripAvailabilityRequest.
     * 
     * @return bypassCache
     */
    public java.lang.Boolean getBypassCache() {
        return bypassCache;
    }


    /**
     * Sets the bypassCache value for this LowFareTripAvailabilityRequest.
     * 
     * @param bypassCache
     */
    public void setBypassCache(java.lang.Boolean bypassCache) {
        this.bypassCache = bypassCache;
    }


    /**
     * Gets the includeTaxesAndFees value for this LowFareTripAvailabilityRequest.
     * 
     * @return includeTaxesAndFees
     */
    public java.lang.Boolean getIncludeTaxesAndFees() {
        return includeTaxesAndFees;
    }


    /**
     * Sets the includeTaxesAndFees value for this LowFareTripAvailabilityRequest.
     * 
     * @param includeTaxesAndFees
     */
    public void setIncludeTaxesAndFees(java.lang.Boolean includeTaxesAndFees) {
        this.includeTaxesAndFees = includeTaxesAndFees;
    }


    /**
     * Gets the groupBydate value for this LowFareTripAvailabilityRequest.
     * 
     * @return groupBydate
     */
    public java.lang.Boolean getGroupBydate() {
        return groupBydate;
    }


    /**
     * Sets the groupBydate value for this LowFareTripAvailabilityRequest.
     * 
     * @param groupBydate
     */
    public void setGroupBydate(java.lang.Boolean groupBydate) {
        this.groupBydate = groupBydate;
    }


    /**
     * Gets the parameterSetID value for this LowFareTripAvailabilityRequest.
     * 
     * @return parameterSetID
     */
    public java.lang.Integer getParameterSetID() {
        return parameterSetID;
    }


    /**
     * Sets the parameterSetID value for this LowFareTripAvailabilityRequest.
     * 
     * @param parameterSetID
     */
    public void setParameterSetID(java.lang.Integer parameterSetID) {
        this.parameterSetID = parameterSetID;
    }


    /**
     * Gets the currencyCode value for this LowFareTripAvailabilityRequest.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this LowFareTripAvailabilityRequest.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the sourceOrganizationCode value for this LowFareTripAvailabilityRequest.
     * 
     * @return sourceOrganizationCode
     */
    public java.lang.String getSourceOrganizationCode() {
        return sourceOrganizationCode;
    }


    /**
     * Sets the sourceOrganizationCode value for this LowFareTripAvailabilityRequest.
     * 
     * @param sourceOrganizationCode
     */
    public void setSourceOrganizationCode(java.lang.String sourceOrganizationCode) {
        this.sourceOrganizationCode = sourceOrganizationCode;
    }


    /**
     * Gets the paxResidentCountry value for this LowFareTripAvailabilityRequest.
     * 
     * @return paxResidentCountry
     */
    public java.lang.String getPaxResidentCountry() {
        return paxResidentCountry;
    }


    /**
     * Sets the paxResidentCountry value for this LowFareTripAvailabilityRequest.
     * 
     * @param paxResidentCountry
     */
    public void setPaxResidentCountry(java.lang.String paxResidentCountry) {
        this.paxResidentCountry = paxResidentCountry;
    }


    /**
     * Gets the promotionCode value for this LowFareTripAvailabilityRequest.
     * 
     * @return promotionCode
     */
    public java.lang.String getPromotionCode() {
        return promotionCode;
    }


    /**
     * Sets the promotionCode value for this LowFareTripAvailabilityRequest.
     * 
     * @param promotionCode
     */
    public void setPromotionCode(java.lang.String promotionCode) {
        this.promotionCode = promotionCode;
    }


    /**
     * Gets the lowFareAvailabilityRequestList value for this LowFareTripAvailabilityRequest.
     * 
     * @return lowFareAvailabilityRequestList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAvailabilityRequest[] getLowFareAvailabilityRequestList() {
        return lowFareAvailabilityRequestList;
    }


    /**
     * Sets the lowFareAvailabilityRequestList value for this LowFareTripAvailabilityRequest.
     * 
     * @param lowFareAvailabilityRequestList
     */
    public void setLowFareAvailabilityRequestList(com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAvailabilityRequest[] lowFareAvailabilityRequestList) {
        this.lowFareAvailabilityRequestList = lowFareAvailabilityRequestList;
    }


    /**
     * Gets the bookingClassList value for this LowFareTripAvailabilityRequest.
     * 
     * @return bookingClassList
     */
    public java.lang.String[] getBookingClassList() {
        return bookingClassList;
    }


    /**
     * Sets the bookingClassList value for this LowFareTripAvailabilityRequest.
     * 
     * @param bookingClassList
     */
    public void setBookingClassList(java.lang.String[] bookingClassList) {
        this.bookingClassList = bookingClassList;
    }


    /**
     * Gets the productClassList value for this LowFareTripAvailabilityRequest.
     * 
     * @return productClassList
     */
    public java.lang.String[] getProductClassList() {
        return productClassList;
    }


    /**
     * Sets the productClassList value for this LowFareTripAvailabilityRequest.
     * 
     * @param productClassList
     */
    public void setProductClassList(java.lang.String[] productClassList) {
        this.productClassList = productClassList;
    }


    /**
     * Gets the fareTypeList value for this LowFareTripAvailabilityRequest.
     * 
     * @return fareTypeList
     */
    public java.lang.String[] getFareTypeList() {
        return fareTypeList;
    }


    /**
     * Sets the fareTypeList value for this LowFareTripAvailabilityRequest.
     * 
     * @param fareTypeList
     */
    public void setFareTypeList(java.lang.String[] fareTypeList) {
        this.fareTypeList = fareTypeList;
    }


    /**
     * Gets the loyaltyFilter value for this LowFareTripAvailabilityRequest.
     * 
     * @return loyaltyFilter
     */
    public org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter getLoyaltyFilter() {
        return loyaltyFilter;
    }


    /**
     * Sets the loyaltyFilter value for this LowFareTripAvailabilityRequest.
     * 
     * @param loyaltyFilter
     */
    public void setLoyaltyFilter(org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
        this.loyaltyFilter = loyaltyFilter;
    }


    /**
     * Gets the flightFilter value for this LowFareTripAvailabilityRequest.
     * 
     * @return flightFilter
     */
    public org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LowFareFlightFilter getFlightFilter() {
        return flightFilter;
    }


    /**
     * Sets the flightFilter value for this LowFareTripAvailabilityRequest.
     * 
     * @param flightFilter
     */
    public void setFlightFilter(org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LowFareFlightFilter flightFilter) {
        this.flightFilter = flightFilter;
    }


    /**
     * Gets the alternateLowFareRequestList value for this LowFareTripAvailabilityRequest.
     * 
     * @return alternateLowFareRequestList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFareRequest[] getAlternateLowFareRequestList() {
        return alternateLowFareRequestList;
    }


    /**
     * Sets the alternateLowFareRequestList value for this LowFareTripAvailabilityRequest.
     * 
     * @param alternateLowFareRequestList
     */
    public void setAlternateLowFareRequestList(com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFareRequest[] alternateLowFareRequestList) {
        this.alternateLowFareRequestList = alternateLowFareRequestList;
    }


    /**
     * Gets the getAllDetails value for this LowFareTripAvailabilityRequest.
     * 
     * @return getAllDetails
     */
    public java.lang.Boolean getGetAllDetails() {
        return getAllDetails;
    }


    /**
     * Sets the getAllDetails value for this LowFareTripAvailabilityRequest.
     * 
     * @param getAllDetails
     */
    public void setGetAllDetails(java.lang.Boolean getAllDetails) {
        this.getAllDetails = getAllDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LowFareTripAvailabilityRequest)) return false;
        LowFareTripAvailabilityRequest other = (LowFareTripAvailabilityRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bypassCache==null && other.getBypassCache()==null) || 
             (this.bypassCache!=null &&
              this.bypassCache.equals(other.getBypassCache()))) &&
            ((this.includeTaxesAndFees==null && other.getIncludeTaxesAndFees()==null) || 
             (this.includeTaxesAndFees!=null &&
              this.includeTaxesAndFees.equals(other.getIncludeTaxesAndFees()))) &&
            ((this.groupBydate==null && other.getGroupBydate()==null) || 
             (this.groupBydate!=null &&
              this.groupBydate.equals(other.getGroupBydate()))) &&
            ((this.parameterSetID==null && other.getParameterSetID()==null) || 
             (this.parameterSetID!=null &&
              this.parameterSetID.equals(other.getParameterSetID()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.sourceOrganizationCode==null && other.getSourceOrganizationCode()==null) || 
             (this.sourceOrganizationCode!=null &&
              this.sourceOrganizationCode.equals(other.getSourceOrganizationCode()))) &&
            ((this.paxResidentCountry==null && other.getPaxResidentCountry()==null) || 
             (this.paxResidentCountry!=null &&
              this.paxResidentCountry.equals(other.getPaxResidentCountry()))) &&
            ((this.promotionCode==null && other.getPromotionCode()==null) || 
             (this.promotionCode!=null &&
              this.promotionCode.equals(other.getPromotionCode()))) &&
            ((this.lowFareAvailabilityRequestList==null && other.getLowFareAvailabilityRequestList()==null) || 
             (this.lowFareAvailabilityRequestList!=null &&
              java.util.Arrays.equals(this.lowFareAvailabilityRequestList, other.getLowFareAvailabilityRequestList()))) &&
            ((this.bookingClassList==null && other.getBookingClassList()==null) || 
             (this.bookingClassList!=null &&
              java.util.Arrays.equals(this.bookingClassList, other.getBookingClassList()))) &&
            ((this.productClassList==null && other.getProductClassList()==null) || 
             (this.productClassList!=null &&
              java.util.Arrays.equals(this.productClassList, other.getProductClassList()))) &&
            ((this.fareTypeList==null && other.getFareTypeList()==null) || 
             (this.fareTypeList!=null &&
              java.util.Arrays.equals(this.fareTypeList, other.getFareTypeList()))) &&
            ((this.loyaltyFilter==null && other.getLoyaltyFilter()==null) || 
             (this.loyaltyFilter!=null &&
              this.loyaltyFilter.equals(other.getLoyaltyFilter()))) &&
            ((this.flightFilter==null && other.getFlightFilter()==null) || 
             (this.flightFilter!=null &&
              this.flightFilter.equals(other.getFlightFilter()))) &&
            ((this.alternateLowFareRequestList==null && other.getAlternateLowFareRequestList()==null) || 
             (this.alternateLowFareRequestList!=null &&
              java.util.Arrays.equals(this.alternateLowFareRequestList, other.getAlternateLowFareRequestList()))) &&
            ((this.getAllDetails==null && other.getGetAllDetails()==null) || 
             (this.getAllDetails!=null &&
              this.getAllDetails.equals(other.getGetAllDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBypassCache() != null) {
            _hashCode += getBypassCache().hashCode();
        }
        if (getIncludeTaxesAndFees() != null) {
            _hashCode += getIncludeTaxesAndFees().hashCode();
        }
        if (getGroupBydate() != null) {
            _hashCode += getGroupBydate().hashCode();
        }
        if (getParameterSetID() != null) {
            _hashCode += getParameterSetID().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getSourceOrganizationCode() != null) {
            _hashCode += getSourceOrganizationCode().hashCode();
        }
        if (getPaxResidentCountry() != null) {
            _hashCode += getPaxResidentCountry().hashCode();
        }
        if (getPromotionCode() != null) {
            _hashCode += getPromotionCode().hashCode();
        }
        if (getLowFareAvailabilityRequestList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLowFareAvailabilityRequestList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLowFareAvailabilityRequestList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBookingClassList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingClassList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingClassList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProductClassList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProductClassList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProductClassList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFareTypeList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFareTypeList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFareTypeList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLoyaltyFilter() != null) {
            _hashCode += getLoyaltyFilter().hashCode();
        }
        if (getFlightFilter() != null) {
            _hashCode += getFlightFilter().hashCode();
        }
        if (getAlternateLowFareRequestList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAlternateLowFareRequestList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAlternateLowFareRequestList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGetAllDetails() != null) {
            _hashCode += getGetAllDetails().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LowFareTripAvailabilityRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareTripAvailabilityRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bypassCache");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BypassCache"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeTaxesAndFees");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeTaxesAndFees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupBydate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GroupBydate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameterSetID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ParameterSetID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceOrganizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceOrganizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxResidentCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxResidentCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promotionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PromotionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lowFareAvailabilityRequestList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityRequestList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityRequest"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingClassList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingClassList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productClassList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProductClassList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareTypeList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareTypeList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loyaltyFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LoyaltyFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LoyaltyFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LowFareFlightFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternateLowFareRequestList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFareRequestList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFareRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFareRequest"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getAllDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetAllDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
