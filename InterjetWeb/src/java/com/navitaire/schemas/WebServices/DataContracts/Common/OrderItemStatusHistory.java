/**
 * OrderItemStatusHistory.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class OrderItemStatusHistory  implements java.io.Serializable {
    private java.lang.Integer historySequence;

    private java.lang.String orderStatusCode;

    private java.lang.String noteText;

    private java.util.Calendar dateCreated;

    private java.lang.Boolean hasError;

    private java.lang.String previousOrderStatusCode;

    public OrderItemStatusHistory() {
    }

    public OrderItemStatusHistory(
           java.lang.Integer historySequence,
           java.lang.String orderStatusCode,
           java.lang.String noteText,
           java.util.Calendar dateCreated,
           java.lang.Boolean hasError,
           java.lang.String previousOrderStatusCode) {
           this.historySequence = historySequence;
           this.orderStatusCode = orderStatusCode;
           this.noteText = noteText;
           this.dateCreated = dateCreated;
           this.hasError = hasError;
           this.previousOrderStatusCode = previousOrderStatusCode;
    }


    /**
     * Gets the historySequence value for this OrderItemStatusHistory.
     * 
     * @return historySequence
     */
    public java.lang.Integer getHistorySequence() {
        return historySequence;
    }


    /**
     * Sets the historySequence value for this OrderItemStatusHistory.
     * 
     * @param historySequence
     */
    public void setHistorySequence(java.lang.Integer historySequence) {
        this.historySequence = historySequence;
    }


    /**
     * Gets the orderStatusCode value for this OrderItemStatusHistory.
     * 
     * @return orderStatusCode
     */
    public java.lang.String getOrderStatusCode() {
        return orderStatusCode;
    }


    /**
     * Sets the orderStatusCode value for this OrderItemStatusHistory.
     * 
     * @param orderStatusCode
     */
    public void setOrderStatusCode(java.lang.String orderStatusCode) {
        this.orderStatusCode = orderStatusCode;
    }


    /**
     * Gets the noteText value for this OrderItemStatusHistory.
     * 
     * @return noteText
     */
    public java.lang.String getNoteText() {
        return noteText;
    }


    /**
     * Sets the noteText value for this OrderItemStatusHistory.
     * 
     * @param noteText
     */
    public void setNoteText(java.lang.String noteText) {
        this.noteText = noteText;
    }


    /**
     * Gets the dateCreated value for this OrderItemStatusHistory.
     * 
     * @return dateCreated
     */
    public java.util.Calendar getDateCreated() {
        return dateCreated;
    }


    /**
     * Sets the dateCreated value for this OrderItemStatusHistory.
     * 
     * @param dateCreated
     */
    public void setDateCreated(java.util.Calendar dateCreated) {
        this.dateCreated = dateCreated;
    }


    /**
     * Gets the hasError value for this OrderItemStatusHistory.
     * 
     * @return hasError
     */
    public java.lang.Boolean getHasError() {
        return hasError;
    }


    /**
     * Sets the hasError value for this OrderItemStatusHistory.
     * 
     * @param hasError
     */
    public void setHasError(java.lang.Boolean hasError) {
        this.hasError = hasError;
    }


    /**
     * Gets the previousOrderStatusCode value for this OrderItemStatusHistory.
     * 
     * @return previousOrderStatusCode
     */
    public java.lang.String getPreviousOrderStatusCode() {
        return previousOrderStatusCode;
    }


    /**
     * Sets the previousOrderStatusCode value for this OrderItemStatusHistory.
     * 
     * @param previousOrderStatusCode
     */
    public void setPreviousOrderStatusCode(java.lang.String previousOrderStatusCode) {
        this.previousOrderStatusCode = previousOrderStatusCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderItemStatusHistory)) return false;
        OrderItemStatusHistory other = (OrderItemStatusHistory) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.historySequence==null && other.getHistorySequence()==null) || 
             (this.historySequence!=null &&
              this.historySequence.equals(other.getHistorySequence()))) &&
            ((this.orderStatusCode==null && other.getOrderStatusCode()==null) || 
             (this.orderStatusCode!=null &&
              this.orderStatusCode.equals(other.getOrderStatusCode()))) &&
            ((this.noteText==null && other.getNoteText()==null) || 
             (this.noteText!=null &&
              this.noteText.equals(other.getNoteText()))) &&
            ((this.dateCreated==null && other.getDateCreated()==null) || 
             (this.dateCreated!=null &&
              this.dateCreated.equals(other.getDateCreated()))) &&
            ((this.hasError==null && other.getHasError()==null) || 
             (this.hasError!=null &&
              this.hasError.equals(other.getHasError()))) &&
            ((this.previousOrderStatusCode==null && other.getPreviousOrderStatusCode()==null) || 
             (this.previousOrderStatusCode!=null &&
              this.previousOrderStatusCode.equals(other.getPreviousOrderStatusCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHistorySequence() != null) {
            _hashCode += getHistorySequence().hashCode();
        }
        if (getOrderStatusCode() != null) {
            _hashCode += getOrderStatusCode().hashCode();
        }
        if (getNoteText() != null) {
            _hashCode += getNoteText().hashCode();
        }
        if (getDateCreated() != null) {
            _hashCode += getDateCreated().hashCode();
        }
        if (getHasError() != null) {
            _hashCode += getHasError().hashCode();
        }
        if (getPreviousOrderStatusCode() != null) {
            _hashCode += getPreviousOrderStatusCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderItemStatusHistory.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemStatusHistory"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("historySequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "HistorySequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("noteText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "NoteText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateCreated");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DateCreated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hasError");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "HasError"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("previousOrderStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PreviousOrderStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
