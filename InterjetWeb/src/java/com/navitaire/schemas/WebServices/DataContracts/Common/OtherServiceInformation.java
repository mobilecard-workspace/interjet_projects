/**
 * OtherServiceInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class OtherServiceInformation  implements java.io.Serializable {
    private java.lang.String text;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.OSISeverity osiSeverity;

    private java.lang.String OSITypeCode;

    private java.lang.String subType;

    public OtherServiceInformation() {
    }

    public OtherServiceInformation(
           java.lang.String text,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.OSISeverity osiSeverity,
           java.lang.String OSITypeCode,
           java.lang.String subType) {
           this.text = text;
           this.osiSeverity = osiSeverity;
           this.OSITypeCode = OSITypeCode;
           this.subType = subType;
    }


    /**
     * Gets the text value for this OtherServiceInformation.
     * 
     * @return text
     */
    public java.lang.String getText() {
        return text;
    }


    /**
     * Sets the text value for this OtherServiceInformation.
     * 
     * @param text
     */
    public void setText(java.lang.String text) {
        this.text = text;
    }


    /**
     * Gets the osiSeverity value for this OtherServiceInformation.
     * 
     * @return osiSeverity
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.OSISeverity getOsiSeverity() {
        return osiSeverity;
    }


    /**
     * Sets the osiSeverity value for this OtherServiceInformation.
     * 
     * @param osiSeverity
     */
    public void setOsiSeverity(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.OSISeverity osiSeverity) {
        this.osiSeverity = osiSeverity;
    }


    /**
     * Gets the OSITypeCode value for this OtherServiceInformation.
     * 
     * @return OSITypeCode
     */
    public java.lang.String getOSITypeCode() {
        return OSITypeCode;
    }


    /**
     * Sets the OSITypeCode value for this OtherServiceInformation.
     * 
     * @param OSITypeCode
     */
    public void setOSITypeCode(java.lang.String OSITypeCode) {
        this.OSITypeCode = OSITypeCode;
    }


    /**
     * Gets the subType value for this OtherServiceInformation.
     * 
     * @return subType
     */
    public java.lang.String getSubType() {
        return subType;
    }


    /**
     * Sets the subType value for this OtherServiceInformation.
     * 
     * @param subType
     */
    public void setSubType(java.lang.String subType) {
        this.subType = subType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OtherServiceInformation)) return false;
        OtherServiceInformation other = (OtherServiceInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.text==null && other.getText()==null) || 
             (this.text!=null &&
              this.text.equals(other.getText()))) &&
            ((this.osiSeverity==null && other.getOsiSeverity()==null) || 
             (this.osiSeverity!=null &&
              this.osiSeverity.equals(other.getOsiSeverity()))) &&
            ((this.OSITypeCode==null && other.getOSITypeCode()==null) || 
             (this.OSITypeCode!=null &&
              this.OSITypeCode.equals(other.getOSITypeCode()))) &&
            ((this.subType==null && other.getSubType()==null) || 
             (this.subType!=null &&
              this.subType.equals(other.getSubType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getText() != null) {
            _hashCode += getText().hashCode();
        }
        if (getOsiSeverity() != null) {
            _hashCode += getOsiSeverity().hashCode();
        }
        if (getOSITypeCode() != null) {
            _hashCode += getOSITypeCode().hashCode();
        }
        if (getSubType() != null) {
            _hashCode += getSubType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OtherServiceInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OtherServiceInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("text");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Text"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("osiSeverity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OsiSeverity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "OSISeverity"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSITypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OSITypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SubType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
