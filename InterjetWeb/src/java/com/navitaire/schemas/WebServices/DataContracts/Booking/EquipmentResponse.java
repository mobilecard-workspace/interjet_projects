/**
 * EquipmentResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class EquipmentResponse  extends com.navitaire.schemas.WebServices.DataContracts.Booking.ServiceMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentInfo equipmentInfo;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey flightKey;

    public EquipmentResponse() {
    }

    public EquipmentResponse(
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInfoList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentInfo equipmentInfo,
           com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey flightKey) {
        super(
            otherServiceInfoList);
        this.equipmentInfo = equipmentInfo;
        this.flightKey = flightKey;
    }


    /**
     * Gets the equipmentInfo value for this EquipmentResponse.
     * 
     * @return equipmentInfo
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentInfo getEquipmentInfo() {
        return equipmentInfo;
    }


    /**
     * Sets the equipmentInfo value for this EquipmentResponse.
     * 
     * @param equipmentInfo
     */
    public void setEquipmentInfo(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentInfo equipmentInfo) {
        this.equipmentInfo = equipmentInfo;
    }


    /**
     * Gets the flightKey value for this EquipmentResponse.
     * 
     * @return flightKey
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey getFlightKey() {
        return flightKey;
    }


    /**
     * Sets the flightKey value for this EquipmentResponse.
     * 
     * @param flightKey
     */
    public void setFlightKey(com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey flightKey) {
        this.flightKey = flightKey;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EquipmentResponse)) return false;
        EquipmentResponse other = (EquipmentResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.equipmentInfo==null && other.getEquipmentInfo()==null) || 
             (this.equipmentInfo!=null &&
              this.equipmentInfo.equals(other.getEquipmentInfo()))) &&
            ((this.flightKey==null && other.getFlightKey()==null) || 
             (this.flightKey!=null &&
              this.flightKey.equals(other.getFlightKey())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEquipmentInfo() != null) {
            _hashCode += getEquipmentInfo().hashCode();
        }
        if (getFlightKey() != null) {
            _hashCode += getFlightKey().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EquipmentResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegKey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
