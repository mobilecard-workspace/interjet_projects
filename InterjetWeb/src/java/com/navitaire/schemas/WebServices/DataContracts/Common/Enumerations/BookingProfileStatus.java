/**
 * BookingProfileStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class BookingProfileStatus implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected BookingProfileStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Default = "Default";
    public static final java.lang.String _KnownIndividual = "KnownIndividual";
    public static final java.lang.String _ResolutionGroup = "ResolutionGroup";
    public static final java.lang.String _SelecteeGroup = "SelecteeGroup";
    public static final java.lang.String _NotUsed = "NotUsed";
    public static final java.lang.String _FailureGroup = "FailureGroup";
    public static final java.lang.String _RandomSelectee = "RandomSelectee";
    public static final java.lang.String _Exempt = "Exempt";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final BookingProfileStatus Default = new BookingProfileStatus(_Default);
    public static final BookingProfileStatus KnownIndividual = new BookingProfileStatus(_KnownIndividual);
    public static final BookingProfileStatus ResolutionGroup = new BookingProfileStatus(_ResolutionGroup);
    public static final BookingProfileStatus SelecteeGroup = new BookingProfileStatus(_SelecteeGroup);
    public static final BookingProfileStatus NotUsed = new BookingProfileStatus(_NotUsed);
    public static final BookingProfileStatus FailureGroup = new BookingProfileStatus(_FailureGroup);
    public static final BookingProfileStatus RandomSelectee = new BookingProfileStatus(_RandomSelectee);
    public static final BookingProfileStatus Exempt = new BookingProfileStatus(_Exempt);
    public static final BookingProfileStatus Unmapped = new BookingProfileStatus(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static BookingProfileStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        BookingProfileStatus enumeration = (BookingProfileStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static BookingProfileStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingProfileStatus.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingProfileStatus"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
