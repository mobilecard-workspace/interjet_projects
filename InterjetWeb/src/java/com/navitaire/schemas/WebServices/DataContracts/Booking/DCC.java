/**
 * DCC.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DCC  implements java.io.Serializable {
    private java.lang.String DCCRateID;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DCCStatus DCCStatus;

    private java.lang.Boolean validationDCCApplicable;

    private java.math.BigDecimal validationDCCRateValue;

    private java.lang.String validationDCCCurrency;

    private java.math.BigDecimal validationDCCAmount;

    private java.lang.String validationDCCPutInState;

    public DCC() {
    }

    public DCC(
           java.lang.String DCCRateID,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DCCStatus DCCStatus,
           java.lang.Boolean validationDCCApplicable,
           java.math.BigDecimal validationDCCRateValue,
           java.lang.String validationDCCCurrency,
           java.math.BigDecimal validationDCCAmount,
           java.lang.String validationDCCPutInState) {
           this.DCCRateID = DCCRateID;
           this.DCCStatus = DCCStatus;
           this.validationDCCApplicable = validationDCCApplicable;
           this.validationDCCRateValue = validationDCCRateValue;
           this.validationDCCCurrency = validationDCCCurrency;
           this.validationDCCAmount = validationDCCAmount;
           this.validationDCCPutInState = validationDCCPutInState;
    }


    /**
     * Gets the DCCRateID value for this DCC.
     * 
     * @return DCCRateID
     */
    public java.lang.String getDCCRateID() {
        return DCCRateID;
    }


    /**
     * Sets the DCCRateID value for this DCC.
     * 
     * @param DCCRateID
     */
    public void setDCCRateID(java.lang.String DCCRateID) {
        this.DCCRateID = DCCRateID;
    }


    /**
     * Gets the DCCStatus value for this DCC.
     * 
     * @return DCCStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DCCStatus getDCCStatus() {
        return DCCStatus;
    }


    /**
     * Sets the DCCStatus value for this DCC.
     * 
     * @param DCCStatus
     */
    public void setDCCStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DCCStatus DCCStatus) {
        this.DCCStatus = DCCStatus;
    }


    /**
     * Gets the validationDCCApplicable value for this DCC.
     * 
     * @return validationDCCApplicable
     */
    public java.lang.Boolean getValidationDCCApplicable() {
        return validationDCCApplicable;
    }


    /**
     * Sets the validationDCCApplicable value for this DCC.
     * 
     * @param validationDCCApplicable
     */
    public void setValidationDCCApplicable(java.lang.Boolean validationDCCApplicable) {
        this.validationDCCApplicable = validationDCCApplicable;
    }


    /**
     * Gets the validationDCCRateValue value for this DCC.
     * 
     * @return validationDCCRateValue
     */
    public java.math.BigDecimal getValidationDCCRateValue() {
        return validationDCCRateValue;
    }


    /**
     * Sets the validationDCCRateValue value for this DCC.
     * 
     * @param validationDCCRateValue
     */
    public void setValidationDCCRateValue(java.math.BigDecimal validationDCCRateValue) {
        this.validationDCCRateValue = validationDCCRateValue;
    }


    /**
     * Gets the validationDCCCurrency value for this DCC.
     * 
     * @return validationDCCCurrency
     */
    public java.lang.String getValidationDCCCurrency() {
        return validationDCCCurrency;
    }


    /**
     * Sets the validationDCCCurrency value for this DCC.
     * 
     * @param validationDCCCurrency
     */
    public void setValidationDCCCurrency(java.lang.String validationDCCCurrency) {
        this.validationDCCCurrency = validationDCCCurrency;
    }


    /**
     * Gets the validationDCCAmount value for this DCC.
     * 
     * @return validationDCCAmount
     */
    public java.math.BigDecimal getValidationDCCAmount() {
        return validationDCCAmount;
    }


    /**
     * Sets the validationDCCAmount value for this DCC.
     * 
     * @param validationDCCAmount
     */
    public void setValidationDCCAmount(java.math.BigDecimal validationDCCAmount) {
        this.validationDCCAmount = validationDCCAmount;
    }


    /**
     * Gets the validationDCCPutInState value for this DCC.
     * 
     * @return validationDCCPutInState
     */
    public java.lang.String getValidationDCCPutInState() {
        return validationDCCPutInState;
    }


    /**
     * Sets the validationDCCPutInState value for this DCC.
     * 
     * @param validationDCCPutInState
     */
    public void setValidationDCCPutInState(java.lang.String validationDCCPutInState) {
        this.validationDCCPutInState = validationDCCPutInState;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DCC)) return false;
        DCC other = (DCC) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DCCRateID==null && other.getDCCRateID()==null) || 
             (this.DCCRateID!=null &&
              this.DCCRateID.equals(other.getDCCRateID()))) &&
            ((this.DCCStatus==null && other.getDCCStatus()==null) || 
             (this.DCCStatus!=null &&
              this.DCCStatus.equals(other.getDCCStatus()))) &&
            ((this.validationDCCApplicable==null && other.getValidationDCCApplicable()==null) || 
             (this.validationDCCApplicable!=null &&
              this.validationDCCApplicable.equals(other.getValidationDCCApplicable()))) &&
            ((this.validationDCCRateValue==null && other.getValidationDCCRateValue()==null) || 
             (this.validationDCCRateValue!=null &&
              this.validationDCCRateValue.equals(other.getValidationDCCRateValue()))) &&
            ((this.validationDCCCurrency==null && other.getValidationDCCCurrency()==null) || 
             (this.validationDCCCurrency!=null &&
              this.validationDCCCurrency.equals(other.getValidationDCCCurrency()))) &&
            ((this.validationDCCAmount==null && other.getValidationDCCAmount()==null) || 
             (this.validationDCCAmount!=null &&
              this.validationDCCAmount.equals(other.getValidationDCCAmount()))) &&
            ((this.validationDCCPutInState==null && other.getValidationDCCPutInState()==null) || 
             (this.validationDCCPutInState!=null &&
              this.validationDCCPutInState.equals(other.getValidationDCCPutInState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDCCRateID() != null) {
            _hashCode += getDCCRateID().hashCode();
        }
        if (getDCCStatus() != null) {
            _hashCode += getDCCStatus().hashCode();
        }
        if (getValidationDCCApplicable() != null) {
            _hashCode += getValidationDCCApplicable().hashCode();
        }
        if (getValidationDCCRateValue() != null) {
            _hashCode += getValidationDCCRateValue().hashCode();
        }
        if (getValidationDCCCurrency() != null) {
            _hashCode += getValidationDCCCurrency().hashCode();
        }
        if (getValidationDCCAmount() != null) {
            _hashCode += getValidationDCCAmount().hashCode();
        }
        if (getValidationDCCPutInState() != null) {
            _hashCode += getValidationDCCPutInState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DCC.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DCC"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DCCRateID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DCCRateID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DCCStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DCCStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DCCStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationDCCApplicable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationDCCApplicable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationDCCRateValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationDCCRateValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationDCCCurrency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationDCCCurrency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationDCCAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationDCCAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationDCCPutInState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationDCCPutInState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
