/**
 * CompartmentInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class CompartmentInfo  implements java.io.Serializable {
    private java.lang.String compartmentDesignator;

    private java.lang.Short deck;

    private java.lang.Integer length;

    private java.lang.Integer width;

    private java.lang.Integer availableUnits;

    private java.lang.Short orientation;

    private java.lang.Short sequence;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SeatInfo[] seats;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] propertyList;

    private org.apache.axis.types.UnsignedInt[] propertyBits;

    private int[] propertyInts;

    private java.util.Calendar propertyTimestamp;

    public CompartmentInfo() {
    }

    public CompartmentInfo(
           java.lang.String compartmentDesignator,
           java.lang.Short deck,
           java.lang.Integer length,
           java.lang.Integer width,
           java.lang.Integer availableUnits,
           java.lang.Short orientation,
           java.lang.Short sequence,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SeatInfo[] seats,
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] propertyList,
           org.apache.axis.types.UnsignedInt[] propertyBits,
           int[] propertyInts,
           java.util.Calendar propertyTimestamp) {
           this.compartmentDesignator = compartmentDesignator;
           this.deck = deck;
           this.length = length;
           this.width = width;
           this.availableUnits = availableUnits;
           this.orientation = orientation;
           this.sequence = sequence;
           this.seats = seats;
           this.propertyList = propertyList;
           this.propertyBits = propertyBits;
           this.propertyInts = propertyInts;
           this.propertyTimestamp = propertyTimestamp;
    }


    /**
     * Gets the compartmentDesignator value for this CompartmentInfo.
     * 
     * @return compartmentDesignator
     */
    public java.lang.String getCompartmentDesignator() {
        return compartmentDesignator;
    }


    /**
     * Sets the compartmentDesignator value for this CompartmentInfo.
     * 
     * @param compartmentDesignator
     */
    public void setCompartmentDesignator(java.lang.String compartmentDesignator) {
        this.compartmentDesignator = compartmentDesignator;
    }


    /**
     * Gets the deck value for this CompartmentInfo.
     * 
     * @return deck
     */
    public java.lang.Short getDeck() {
        return deck;
    }


    /**
     * Sets the deck value for this CompartmentInfo.
     * 
     * @param deck
     */
    public void setDeck(java.lang.Short deck) {
        this.deck = deck;
    }


    /**
     * Gets the length value for this CompartmentInfo.
     * 
     * @return length
     */
    public java.lang.Integer getLength() {
        return length;
    }


    /**
     * Sets the length value for this CompartmentInfo.
     * 
     * @param length
     */
    public void setLength(java.lang.Integer length) {
        this.length = length;
    }


    /**
     * Gets the width value for this CompartmentInfo.
     * 
     * @return width
     */
    public java.lang.Integer getWidth() {
        return width;
    }


    /**
     * Sets the width value for this CompartmentInfo.
     * 
     * @param width
     */
    public void setWidth(java.lang.Integer width) {
        this.width = width;
    }


    /**
     * Gets the availableUnits value for this CompartmentInfo.
     * 
     * @return availableUnits
     */
    public java.lang.Integer getAvailableUnits() {
        return availableUnits;
    }


    /**
     * Sets the availableUnits value for this CompartmentInfo.
     * 
     * @param availableUnits
     */
    public void setAvailableUnits(java.lang.Integer availableUnits) {
        this.availableUnits = availableUnits;
    }


    /**
     * Gets the orientation value for this CompartmentInfo.
     * 
     * @return orientation
     */
    public java.lang.Short getOrientation() {
        return orientation;
    }


    /**
     * Sets the orientation value for this CompartmentInfo.
     * 
     * @param orientation
     */
    public void setOrientation(java.lang.Short orientation) {
        this.orientation = orientation;
    }


    /**
     * Gets the sequence value for this CompartmentInfo.
     * 
     * @return sequence
     */
    public java.lang.Short getSequence() {
        return sequence;
    }


    /**
     * Sets the sequence value for this CompartmentInfo.
     * 
     * @param sequence
     */
    public void setSequence(java.lang.Short sequence) {
        this.sequence = sequence;
    }


    /**
     * Gets the seats value for this CompartmentInfo.
     * 
     * @return seats
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SeatInfo[] getSeats() {
        return seats;
    }


    /**
     * Sets the seats value for this CompartmentInfo.
     * 
     * @param seats
     */
    public void setSeats(com.navitaire.schemas.WebServices.DataContracts.Booking.SeatInfo[] seats) {
        this.seats = seats;
    }


    /**
     * Gets the propertyList value for this CompartmentInfo.
     * 
     * @return propertyList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] getPropertyList() {
        return propertyList;
    }


    /**
     * Sets the propertyList value for this CompartmentInfo.
     * 
     * @param propertyList
     */
    public void setPropertyList(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] propertyList) {
        this.propertyList = propertyList;
    }


    /**
     * Gets the propertyBits value for this CompartmentInfo.
     * 
     * @return propertyBits
     */
    public org.apache.axis.types.UnsignedInt[] getPropertyBits() {
        return propertyBits;
    }


    /**
     * Sets the propertyBits value for this CompartmentInfo.
     * 
     * @param propertyBits
     */
    public void setPropertyBits(org.apache.axis.types.UnsignedInt[] propertyBits) {
        this.propertyBits = propertyBits;
    }


    /**
     * Gets the propertyInts value for this CompartmentInfo.
     * 
     * @return propertyInts
     */
    public int[] getPropertyInts() {
        return propertyInts;
    }


    /**
     * Sets the propertyInts value for this CompartmentInfo.
     * 
     * @param propertyInts
     */
    public void setPropertyInts(int[] propertyInts) {
        this.propertyInts = propertyInts;
    }


    /**
     * Gets the propertyTimestamp value for this CompartmentInfo.
     * 
     * @return propertyTimestamp
     */
    public java.util.Calendar getPropertyTimestamp() {
        return propertyTimestamp;
    }


    /**
     * Sets the propertyTimestamp value for this CompartmentInfo.
     * 
     * @param propertyTimestamp
     */
    public void setPropertyTimestamp(java.util.Calendar propertyTimestamp) {
        this.propertyTimestamp = propertyTimestamp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CompartmentInfo)) return false;
        CompartmentInfo other = (CompartmentInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.compartmentDesignator==null && other.getCompartmentDesignator()==null) || 
             (this.compartmentDesignator!=null &&
              this.compartmentDesignator.equals(other.getCompartmentDesignator()))) &&
            ((this.deck==null && other.getDeck()==null) || 
             (this.deck!=null &&
              this.deck.equals(other.getDeck()))) &&
            ((this.length==null && other.getLength()==null) || 
             (this.length!=null &&
              this.length.equals(other.getLength()))) &&
            ((this.width==null && other.getWidth()==null) || 
             (this.width!=null &&
              this.width.equals(other.getWidth()))) &&
            ((this.availableUnits==null && other.getAvailableUnits()==null) || 
             (this.availableUnits!=null &&
              this.availableUnits.equals(other.getAvailableUnits()))) &&
            ((this.orientation==null && other.getOrientation()==null) || 
             (this.orientation!=null &&
              this.orientation.equals(other.getOrientation()))) &&
            ((this.sequence==null && other.getSequence()==null) || 
             (this.sequence!=null &&
              this.sequence.equals(other.getSequence()))) &&
            ((this.seats==null && other.getSeats()==null) || 
             (this.seats!=null &&
              java.util.Arrays.equals(this.seats, other.getSeats()))) &&
            ((this.propertyList==null && other.getPropertyList()==null) || 
             (this.propertyList!=null &&
              java.util.Arrays.equals(this.propertyList, other.getPropertyList()))) &&
            ((this.propertyBits==null && other.getPropertyBits()==null) || 
             (this.propertyBits!=null &&
              java.util.Arrays.equals(this.propertyBits, other.getPropertyBits()))) &&
            ((this.propertyInts==null && other.getPropertyInts()==null) || 
             (this.propertyInts!=null &&
              java.util.Arrays.equals(this.propertyInts, other.getPropertyInts()))) &&
            ((this.propertyTimestamp==null && other.getPropertyTimestamp()==null) || 
             (this.propertyTimestamp!=null &&
              this.propertyTimestamp.equals(other.getPropertyTimestamp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCompartmentDesignator() != null) {
            _hashCode += getCompartmentDesignator().hashCode();
        }
        if (getDeck() != null) {
            _hashCode += getDeck().hashCode();
        }
        if (getLength() != null) {
            _hashCode += getLength().hashCode();
        }
        if (getWidth() != null) {
            _hashCode += getWidth().hashCode();
        }
        if (getAvailableUnits() != null) {
            _hashCode += getAvailableUnits().hashCode();
        }
        if (getOrientation() != null) {
            _hashCode += getOrientation().hashCode();
        }
        if (getSequence() != null) {
            _hashCode += getSequence().hashCode();
        }
        if (getSeats() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSeats());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSeats(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyBits() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyBits());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyBits(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyInts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyInts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyInts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyTimestamp() != null) {
            _hashCode += getPropertyTimestamp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CompartmentInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("compartmentDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deck");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Deck"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("length");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Length"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("width");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Width"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableUnits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailableUnits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orientation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Orientation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Sequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seats");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Seats"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatInfo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyBits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyBits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "unsignedInt"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyInts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyInts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyTimestamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyTimestamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
