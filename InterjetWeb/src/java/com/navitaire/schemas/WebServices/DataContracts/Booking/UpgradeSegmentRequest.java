/**
 * UpgradeSegmentRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class UpgradeSegmentRequest  implements java.io.Serializable {
    private java.lang.String actionStatusCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator;

    private java.util.Calendar STD;

    private java.lang.String departureStation;

    private java.lang.String arrivalStation;

    private java.lang.String classOfService;

    private java.lang.Boolean feeOverride;

    public UpgradeSegmentRequest() {
    }

    public UpgradeSegmentRequest(
           java.lang.String actionStatusCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator,
           java.util.Calendar STD,
           java.lang.String departureStation,
           java.lang.String arrivalStation,
           java.lang.String classOfService,
           java.lang.Boolean feeOverride) {
           this.actionStatusCode = actionStatusCode;
           this.flightDesignator = flightDesignator;
           this.STD = STD;
           this.departureStation = departureStation;
           this.arrivalStation = arrivalStation;
           this.classOfService = classOfService;
           this.feeOverride = feeOverride;
    }


    /**
     * Gets the actionStatusCode value for this UpgradeSegmentRequest.
     * 
     * @return actionStatusCode
     */
    public java.lang.String getActionStatusCode() {
        return actionStatusCode;
    }


    /**
     * Sets the actionStatusCode value for this UpgradeSegmentRequest.
     * 
     * @param actionStatusCode
     */
    public void setActionStatusCode(java.lang.String actionStatusCode) {
        this.actionStatusCode = actionStatusCode;
    }


    /**
     * Gets the flightDesignator value for this UpgradeSegmentRequest.
     * 
     * @return flightDesignator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator getFlightDesignator() {
        return flightDesignator;
    }


    /**
     * Sets the flightDesignator value for this UpgradeSegmentRequest.
     * 
     * @param flightDesignator
     */
    public void setFlightDesignator(com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator) {
        this.flightDesignator = flightDesignator;
    }


    /**
     * Gets the STD value for this UpgradeSegmentRequest.
     * 
     * @return STD
     */
    public java.util.Calendar getSTD() {
        return STD;
    }


    /**
     * Sets the STD value for this UpgradeSegmentRequest.
     * 
     * @param STD
     */
    public void setSTD(java.util.Calendar STD) {
        this.STD = STD;
    }


    /**
     * Gets the departureStation value for this UpgradeSegmentRequest.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this UpgradeSegmentRequest.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the arrivalStation value for this UpgradeSegmentRequest.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this UpgradeSegmentRequest.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the classOfService value for this UpgradeSegmentRequest.
     * 
     * @return classOfService
     */
    public java.lang.String getClassOfService() {
        return classOfService;
    }


    /**
     * Sets the classOfService value for this UpgradeSegmentRequest.
     * 
     * @param classOfService
     */
    public void setClassOfService(java.lang.String classOfService) {
        this.classOfService = classOfService;
    }


    /**
     * Gets the feeOverride value for this UpgradeSegmentRequest.
     * 
     * @return feeOverride
     */
    public java.lang.Boolean getFeeOverride() {
        return feeOverride;
    }


    /**
     * Sets the feeOverride value for this UpgradeSegmentRequest.
     * 
     * @param feeOverride
     */
    public void setFeeOverride(java.lang.Boolean feeOverride) {
        this.feeOverride = feeOverride;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpgradeSegmentRequest)) return false;
        UpgradeSegmentRequest other = (UpgradeSegmentRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actionStatusCode==null && other.getActionStatusCode()==null) || 
             (this.actionStatusCode!=null &&
              this.actionStatusCode.equals(other.getActionStatusCode()))) &&
            ((this.flightDesignator==null && other.getFlightDesignator()==null) || 
             (this.flightDesignator!=null &&
              this.flightDesignator.equals(other.getFlightDesignator()))) &&
            ((this.STD==null && other.getSTD()==null) || 
             (this.STD!=null &&
              this.STD.equals(other.getSTD()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.classOfService==null && other.getClassOfService()==null) || 
             (this.classOfService!=null &&
              this.classOfService.equals(other.getClassOfService()))) &&
            ((this.feeOverride==null && other.getFeeOverride()==null) || 
             (this.feeOverride!=null &&
              this.feeOverride.equals(other.getFeeOverride())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActionStatusCode() != null) {
            _hashCode += getActionStatusCode().hashCode();
        }
        if (getFlightDesignator() != null) {
            _hashCode += getFlightDesignator().hashCode();
        }
        if (getSTD() != null) {
            _hashCode += getSTD().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getClassOfService() != null) {
            _hashCode += getClassOfService().hashCode();
        }
        if (getFeeOverride() != null) {
            _hashCode += getFeeOverride().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpgradeSegmentRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegmentRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FlightDesignator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeOverride");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeOverride"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
