/**
 * PaxSeatInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxSeatInfo  implements java.io.Serializable {
    private java.lang.Short seatSet;

    private java.lang.Short deck;

    private org.datacontract.schemas._2004._07.System_Collections_Generic.KeyValuePairOfstringstring[] properties;

    public PaxSeatInfo() {
    }

    public PaxSeatInfo(
           java.lang.Short seatSet,
           java.lang.Short deck,
           org.datacontract.schemas._2004._07.System_Collections_Generic.KeyValuePairOfstringstring[] properties) {
           this.seatSet = seatSet;
           this.deck = deck;
           this.properties = properties;
    }


    /**
     * Gets the seatSet value for this PaxSeatInfo.
     * 
     * @return seatSet
     */
    public java.lang.Short getSeatSet() {
        return seatSet;
    }


    /**
     * Sets the seatSet value for this PaxSeatInfo.
     * 
     * @param seatSet
     */
    public void setSeatSet(java.lang.Short seatSet) {
        this.seatSet = seatSet;
    }


    /**
     * Gets the deck value for this PaxSeatInfo.
     * 
     * @return deck
     */
    public java.lang.Short getDeck() {
        return deck;
    }


    /**
     * Sets the deck value for this PaxSeatInfo.
     * 
     * @param deck
     */
    public void setDeck(java.lang.Short deck) {
        this.deck = deck;
    }


    /**
     * Gets the properties value for this PaxSeatInfo.
     * 
     * @return properties
     */
    public org.datacontract.schemas._2004._07.System_Collections_Generic.KeyValuePairOfstringstring[] getProperties() {
        return properties;
    }


    /**
     * Sets the properties value for this PaxSeatInfo.
     * 
     * @param properties
     */
    public void setProperties(org.datacontract.schemas._2004._07.System_Collections_Generic.KeyValuePairOfstringstring[] properties) {
        this.properties = properties;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxSeatInfo)) return false;
        PaxSeatInfo other = (PaxSeatInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.seatSet==null && other.getSeatSet()==null) || 
             (this.seatSet!=null &&
              this.seatSet.equals(other.getSeatSet()))) &&
            ((this.deck==null && other.getDeck()==null) || 
             (this.deck!=null &&
              this.deck.equals(other.getDeck()))) &&
            ((this.properties==null && other.getProperties()==null) || 
             (this.properties!=null &&
              java.util.Arrays.equals(this.properties, other.getProperties())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSeatSet() != null) {
            _hashCode += getSeatSet().hashCode();
        }
        if (getDeck() != null) {
            _hashCode += getDeck().hashCode();
        }
        if (getProperties() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProperties());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProperties(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxSeatInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatSet");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatSet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deck");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Deck"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("properties");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Properties"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System.Collections.Generic", "KeyValuePairOfstringstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System.Collections.Generic", "KeyValuePairOfstringstring"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
