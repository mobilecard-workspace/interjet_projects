/**
 * CancelRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class CancelRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CancelBy cancelBy;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.CancelJourney cancelJourney;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.CancelFee cancelFee;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.CancelSSR cancelSSR;

    public CancelRequestData() {
    }

    public CancelRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CancelBy cancelBy,
           com.navitaire.schemas.WebServices.DataContracts.Booking.CancelJourney cancelJourney,
           com.navitaire.schemas.WebServices.DataContracts.Booking.CancelFee cancelFee,
           com.navitaire.schemas.WebServices.DataContracts.Booking.CancelSSR cancelSSR) {
           this.cancelBy = cancelBy;
           this.cancelJourney = cancelJourney;
           this.cancelFee = cancelFee;
           this.cancelSSR = cancelSSR;
    }


    /**
     * Gets the cancelBy value for this CancelRequestData.
     * 
     * @return cancelBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CancelBy getCancelBy() {
        return cancelBy;
    }


    /**
     * Sets the cancelBy value for this CancelRequestData.
     * 
     * @param cancelBy
     */
    public void setCancelBy(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CancelBy cancelBy) {
        this.cancelBy = cancelBy;
    }


    /**
     * Gets the cancelJourney value for this CancelRequestData.
     * 
     * @return cancelJourney
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.CancelJourney getCancelJourney() {
        return cancelJourney;
    }


    /**
     * Sets the cancelJourney value for this CancelRequestData.
     * 
     * @param cancelJourney
     */
    public void setCancelJourney(com.navitaire.schemas.WebServices.DataContracts.Booking.CancelJourney cancelJourney) {
        this.cancelJourney = cancelJourney;
    }


    /**
     * Gets the cancelFee value for this CancelRequestData.
     * 
     * @return cancelFee
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.CancelFee getCancelFee() {
        return cancelFee;
    }


    /**
     * Sets the cancelFee value for this CancelRequestData.
     * 
     * @param cancelFee
     */
    public void setCancelFee(com.navitaire.schemas.WebServices.DataContracts.Booking.CancelFee cancelFee) {
        this.cancelFee = cancelFee;
    }


    /**
     * Gets the cancelSSR value for this CancelRequestData.
     * 
     * @return cancelSSR
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.CancelSSR getCancelSSR() {
        return cancelSSR;
    }


    /**
     * Sets the cancelSSR value for this CancelRequestData.
     * 
     * @param cancelSSR
     */
    public void setCancelSSR(com.navitaire.schemas.WebServices.DataContracts.Booking.CancelSSR cancelSSR) {
        this.cancelSSR = cancelSSR;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CancelRequestData)) return false;
        CancelRequestData other = (CancelRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cancelBy==null && other.getCancelBy()==null) || 
             (this.cancelBy!=null &&
              this.cancelBy.equals(other.getCancelBy()))) &&
            ((this.cancelJourney==null && other.getCancelJourney()==null) || 
             (this.cancelJourney!=null &&
              this.cancelJourney.equals(other.getCancelJourney()))) &&
            ((this.cancelFee==null && other.getCancelFee()==null) || 
             (this.cancelFee!=null &&
              this.cancelFee.equals(other.getCancelFee()))) &&
            ((this.cancelSSR==null && other.getCancelSSR()==null) || 
             (this.cancelSSR!=null &&
              this.cancelSSR.equals(other.getCancelSSR())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCancelBy() != null) {
            _hashCode += getCancelBy().hashCode();
        }
        if (getCancelJourney() != null) {
            _hashCode += getCancelJourney().hashCode();
        }
        if (getCancelFee() != null) {
            _hashCode += getCancelFee().hashCode();
        }
        if (getCancelSSR() != null) {
            _hashCode += getCancelSSR().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CancelRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "CancelBy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelJourney");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelJourney"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelJourney"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelFee"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelSSR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelSSR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelSSR"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
