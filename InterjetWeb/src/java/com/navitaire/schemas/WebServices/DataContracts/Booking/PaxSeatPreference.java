/**
 * PaxSeatPreference.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxSeatPreference  implements java.io.Serializable {
    private java.lang.String actionStatusCode;

    private java.lang.Short passengerNumber;

    private java.lang.String propertyTypeCode;

    private java.lang.String propertyCode;

    private java.lang.String met;

    public PaxSeatPreference() {
    }

    public PaxSeatPreference(
           java.lang.String actionStatusCode,
           java.lang.Short passengerNumber,
           java.lang.String propertyTypeCode,
           java.lang.String propertyCode,
           java.lang.String met) {
           this.actionStatusCode = actionStatusCode;
           this.passengerNumber = passengerNumber;
           this.propertyTypeCode = propertyTypeCode;
           this.propertyCode = propertyCode;
           this.met = met;
    }


    /**
     * Gets the actionStatusCode value for this PaxSeatPreference.
     * 
     * @return actionStatusCode
     */
    public java.lang.String getActionStatusCode() {
        return actionStatusCode;
    }


    /**
     * Sets the actionStatusCode value for this PaxSeatPreference.
     * 
     * @param actionStatusCode
     */
    public void setActionStatusCode(java.lang.String actionStatusCode) {
        this.actionStatusCode = actionStatusCode;
    }


    /**
     * Gets the passengerNumber value for this PaxSeatPreference.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this PaxSeatPreference.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the propertyTypeCode value for this PaxSeatPreference.
     * 
     * @return propertyTypeCode
     */
    public java.lang.String getPropertyTypeCode() {
        return propertyTypeCode;
    }


    /**
     * Sets the propertyTypeCode value for this PaxSeatPreference.
     * 
     * @param propertyTypeCode
     */
    public void setPropertyTypeCode(java.lang.String propertyTypeCode) {
        this.propertyTypeCode = propertyTypeCode;
    }


    /**
     * Gets the propertyCode value for this PaxSeatPreference.
     * 
     * @return propertyCode
     */
    public java.lang.String getPropertyCode() {
        return propertyCode;
    }


    /**
     * Sets the propertyCode value for this PaxSeatPreference.
     * 
     * @param propertyCode
     */
    public void setPropertyCode(java.lang.String propertyCode) {
        this.propertyCode = propertyCode;
    }


    /**
     * Gets the met value for this PaxSeatPreference.
     * 
     * @return met
     */
    public java.lang.String getMet() {
        return met;
    }


    /**
     * Sets the met value for this PaxSeatPreference.
     * 
     * @param met
     */
    public void setMet(java.lang.String met) {
        this.met = met;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxSeatPreference)) return false;
        PaxSeatPreference other = (PaxSeatPreference) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actionStatusCode==null && other.getActionStatusCode()==null) || 
             (this.actionStatusCode!=null &&
              this.actionStatusCode.equals(other.getActionStatusCode()))) &&
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.propertyTypeCode==null && other.getPropertyTypeCode()==null) || 
             (this.propertyTypeCode!=null &&
              this.propertyTypeCode.equals(other.getPropertyTypeCode()))) &&
            ((this.propertyCode==null && other.getPropertyCode()==null) || 
             (this.propertyCode!=null &&
              this.propertyCode.equals(other.getPropertyCode()))) &&
            ((this.met==null && other.getMet()==null) || 
             (this.met!=null &&
              this.met.equals(other.getMet())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActionStatusCode() != null) {
            _hashCode += getActionStatusCode().hashCode();
        }
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getPropertyTypeCode() != null) {
            _hashCode += getPropertyTypeCode().hashCode();
        }
        if (getPropertyCode() != null) {
            _hashCode += getPropertyCode().hashCode();
        }
        if (getMet() != null) {
            _hashCode += getMet().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxSeatPreference.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("met");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Met"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
