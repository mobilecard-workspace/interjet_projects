/**
 * SSRAvailabilityResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SSRAvailabilityResponse  extends com.navitaire.schemas.WebServices.DataContracts.Booking.ServiceMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[] legs;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.InventorySegmentSSRNest[] segmentSSRs;

    public SSRAvailabilityResponse() {
    }

    public SSRAvailabilityResponse(
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInfoList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[] legs,
           com.navitaire.schemas.WebServices.DataContracts.Booking.InventorySegmentSSRNest[] segmentSSRs) {
        super(
            otherServiceInfoList);
        this.legs = legs;
        this.segmentSSRs = segmentSSRs;
    }


    /**
     * Gets the legs value for this SSRAvailabilityResponse.
     * 
     * @return legs
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[] getLegs() {
        return legs;
    }


    /**
     * Sets the legs value for this SSRAvailabilityResponse.
     * 
     * @param legs
     */
    public void setLegs(com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[] legs) {
        this.legs = legs;
    }


    /**
     * Gets the segmentSSRs value for this SSRAvailabilityResponse.
     * 
     * @return segmentSSRs
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.InventorySegmentSSRNest[] getSegmentSSRs() {
        return segmentSSRs;
    }


    /**
     * Sets the segmentSSRs value for this SSRAvailabilityResponse.
     * 
     * @param segmentSSRs
     */
    public void setSegmentSSRs(com.navitaire.schemas.WebServices.DataContracts.Booking.InventorySegmentSSRNest[] segmentSSRs) {
        this.segmentSSRs = segmentSSRs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SSRAvailabilityResponse)) return false;
        SSRAvailabilityResponse other = (SSRAvailabilityResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.legs==null && other.getLegs()==null) || 
             (this.legs!=null &&
              java.util.Arrays.equals(this.legs, other.getLegs()))) &&
            ((this.segmentSSRs==null && other.getSegmentSSRs()==null) || 
             (this.segmentSSRs!=null &&
              java.util.Arrays.equals(this.segmentSSRs, other.getSegmentSSRs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getLegs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLegs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLegs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSegmentSSRs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegmentSSRs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegmentSSRs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SSRAvailabilityResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRAvailabilityResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("legs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Legs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Leg"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Leg"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentSSRs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSSRs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InventorySegmentSSRNest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InventorySegmentSSRNest"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
