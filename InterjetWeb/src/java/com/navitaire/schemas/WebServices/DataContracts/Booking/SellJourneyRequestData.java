/**
 * SellJourneyRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SellJourneyRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourney[] journeys;

    private java.lang.Short paxCount;

    private java.lang.String currencyCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers;

    private java.lang.Boolean suppressPaxAgeValidation;

    private org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter;

    public SellJourneyRequestData() {
    }

    public SellJourneyRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourney[] journeys,
           java.lang.Short paxCount,
           java.lang.String currencyCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS,
           com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers,
           java.lang.Boolean suppressPaxAgeValidation,
           org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
           this.journeys = journeys;
           this.paxCount = paxCount;
           this.currencyCode = currencyCode;
           this.sourcePOS = sourcePOS;
           this.typeOfSale = typeOfSale;
           this.passengers = passengers;
           this.suppressPaxAgeValidation = suppressPaxAgeValidation;
           this.loyaltyFilter = loyaltyFilter;
    }


    /**
     * Gets the journeys value for this SellJourneyRequestData.
     * 
     * @return journeys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourney[] getJourneys() {
        return journeys;
    }


    /**
     * Sets the journeys value for this SellJourneyRequestData.
     * 
     * @param journeys
     */
    public void setJourneys(com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourney[] journeys) {
        this.journeys = journeys;
    }


    /**
     * Gets the paxCount value for this SellJourneyRequestData.
     * 
     * @return paxCount
     */
    public java.lang.Short getPaxCount() {
        return paxCount;
    }


    /**
     * Sets the paxCount value for this SellJourneyRequestData.
     * 
     * @param paxCount
     */
    public void setPaxCount(java.lang.Short paxCount) {
        this.paxCount = paxCount;
    }


    /**
     * Gets the currencyCode value for this SellJourneyRequestData.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this SellJourneyRequestData.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the sourcePOS value for this SellJourneyRequestData.
     * 
     * @return sourcePOS
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePOS() {
        return sourcePOS;
    }


    /**
     * Sets the sourcePOS value for this SellJourneyRequestData.
     * 
     * @param sourcePOS
     */
    public void setSourcePOS(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS) {
        this.sourcePOS = sourcePOS;
    }


    /**
     * Gets the typeOfSale value for this SellJourneyRequestData.
     * 
     * @return typeOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale getTypeOfSale() {
        return typeOfSale;
    }


    /**
     * Sets the typeOfSale value for this SellJourneyRequestData.
     * 
     * @param typeOfSale
     */
    public void setTypeOfSale(com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale) {
        this.typeOfSale = typeOfSale;
    }


    /**
     * Gets the passengers value for this SellJourneyRequestData.
     * 
     * @return passengers
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] getPassengers() {
        return passengers;
    }


    /**
     * Sets the passengers value for this SellJourneyRequestData.
     * 
     * @param passengers
     */
    public void setPassengers(com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers) {
        this.passengers = passengers;
    }


    /**
     * Gets the suppressPaxAgeValidation value for this SellJourneyRequestData.
     * 
     * @return suppressPaxAgeValidation
     */
    public java.lang.Boolean getSuppressPaxAgeValidation() {
        return suppressPaxAgeValidation;
    }


    /**
     * Sets the suppressPaxAgeValidation value for this SellJourneyRequestData.
     * 
     * @param suppressPaxAgeValidation
     */
    public void setSuppressPaxAgeValidation(java.lang.Boolean suppressPaxAgeValidation) {
        this.suppressPaxAgeValidation = suppressPaxAgeValidation;
    }


    /**
     * Gets the loyaltyFilter value for this SellJourneyRequestData.
     * 
     * @return loyaltyFilter
     */
    public org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter getLoyaltyFilter() {
        return loyaltyFilter;
    }


    /**
     * Sets the loyaltyFilter value for this SellJourneyRequestData.
     * 
     * @param loyaltyFilter
     */
    public void setLoyaltyFilter(org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
        this.loyaltyFilter = loyaltyFilter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SellJourneyRequestData)) return false;
        SellJourneyRequestData other = (SellJourneyRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.journeys==null && other.getJourneys()==null) || 
             (this.journeys!=null &&
              java.util.Arrays.equals(this.journeys, other.getJourneys()))) &&
            ((this.paxCount==null && other.getPaxCount()==null) || 
             (this.paxCount!=null &&
              this.paxCount.equals(other.getPaxCount()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.sourcePOS==null && other.getSourcePOS()==null) || 
             (this.sourcePOS!=null &&
              this.sourcePOS.equals(other.getSourcePOS()))) &&
            ((this.typeOfSale==null && other.getTypeOfSale()==null) || 
             (this.typeOfSale!=null &&
              this.typeOfSale.equals(other.getTypeOfSale()))) &&
            ((this.passengers==null && other.getPassengers()==null) || 
             (this.passengers!=null &&
              java.util.Arrays.equals(this.passengers, other.getPassengers()))) &&
            ((this.suppressPaxAgeValidation==null && other.getSuppressPaxAgeValidation()==null) || 
             (this.suppressPaxAgeValidation!=null &&
              this.suppressPaxAgeValidation.equals(other.getSuppressPaxAgeValidation()))) &&
            ((this.loyaltyFilter==null && other.getLoyaltyFilter()==null) || 
             (this.loyaltyFilter!=null &&
              this.loyaltyFilter.equals(other.getLoyaltyFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getJourneys() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJourneys());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJourneys(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxCount() != null) {
            _hashCode += getPaxCount().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getSourcePOS() != null) {
            _hashCode += getSourcePOS().hashCode();
        }
        if (getTypeOfSale() != null) {
            _hashCode += getTypeOfSale().hashCode();
        }
        if (getPassengers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSuppressPaxAgeValidation() != null) {
            _hashCode += getSuppressPaxAgeValidation().hashCode();
        }
        if (getLoyaltyFilter() != null) {
            _hashCode += getLoyaltyFilter().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SellJourneyRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journeys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourney"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourney"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePOS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passengers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("suppressPaxAgeValidation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SuppressPaxAgeValidation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loyaltyFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LoyaltyFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LoyaltyFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
