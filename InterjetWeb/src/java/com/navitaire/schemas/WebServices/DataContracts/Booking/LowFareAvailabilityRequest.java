/**
 * LowFareAvailabilityRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class LowFareAvailabilityRequest  implements java.io.Serializable {
    private java.lang.String[] departureStationList;

    private java.lang.String[] arrivalStationList;

    private java.util.Calendar beginDate;

    private java.util.Calendar endDate;

    public LowFareAvailabilityRequest() {
    }

    public LowFareAvailabilityRequest(
           java.lang.String[] departureStationList,
           java.lang.String[] arrivalStationList,
           java.util.Calendar beginDate,
           java.util.Calendar endDate) {
           this.departureStationList = departureStationList;
           this.arrivalStationList = arrivalStationList;
           this.beginDate = beginDate;
           this.endDate = endDate;
    }


    /**
     * Gets the departureStationList value for this LowFareAvailabilityRequest.
     * 
     * @return departureStationList
     */
    public java.lang.String[] getDepartureStationList() {
        return departureStationList;
    }


    /**
     * Sets the departureStationList value for this LowFareAvailabilityRequest.
     * 
     * @param departureStationList
     */
    public void setDepartureStationList(java.lang.String[] departureStationList) {
        this.departureStationList = departureStationList;
    }


    /**
     * Gets the arrivalStationList value for this LowFareAvailabilityRequest.
     * 
     * @return arrivalStationList
     */
    public java.lang.String[] getArrivalStationList() {
        return arrivalStationList;
    }


    /**
     * Sets the arrivalStationList value for this LowFareAvailabilityRequest.
     * 
     * @param arrivalStationList
     */
    public void setArrivalStationList(java.lang.String[] arrivalStationList) {
        this.arrivalStationList = arrivalStationList;
    }


    /**
     * Gets the beginDate value for this LowFareAvailabilityRequest.
     * 
     * @return beginDate
     */
    public java.util.Calendar getBeginDate() {
        return beginDate;
    }


    /**
     * Sets the beginDate value for this LowFareAvailabilityRequest.
     * 
     * @param beginDate
     */
    public void setBeginDate(java.util.Calendar beginDate) {
        this.beginDate = beginDate;
    }


    /**
     * Gets the endDate value for this LowFareAvailabilityRequest.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this LowFareAvailabilityRequest.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LowFareAvailabilityRequest)) return false;
        LowFareAvailabilityRequest other = (LowFareAvailabilityRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departureStationList==null && other.getDepartureStationList()==null) || 
             (this.departureStationList!=null &&
              java.util.Arrays.equals(this.departureStationList, other.getDepartureStationList()))) &&
            ((this.arrivalStationList==null && other.getArrivalStationList()==null) || 
             (this.arrivalStationList!=null &&
              java.util.Arrays.equals(this.arrivalStationList, other.getArrivalStationList()))) &&
            ((this.beginDate==null && other.getBeginDate()==null) || 
             (this.beginDate!=null &&
              this.beginDate.equals(other.getBeginDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartureStationList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDepartureStationList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDepartureStationList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getArrivalStationList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getArrivalStationList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getArrivalStationList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBeginDate() != null) {
            _hashCode += getBeginDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LowFareAvailabilityRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStationList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStationList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStationList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStationList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BeginDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
