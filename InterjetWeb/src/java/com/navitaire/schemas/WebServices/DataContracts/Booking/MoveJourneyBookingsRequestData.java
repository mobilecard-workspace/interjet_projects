/**
 * MoveJourneyBookingsRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class MoveJourneyBookingsRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.Journey fromJourney;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] toJourneys;

    private java.lang.String[] recordLocators;

    private java.lang.String changeReasonCode;

    private java.lang.Boolean keepWaitListStatus;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Segment effectedSegment;

    private java.lang.Short toleranceMinutes;

    private java.lang.Boolean addAdHocConnection;

    private java.lang.Boolean adHocIsForGeneralUse;

    private java.lang.String bookingComment;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType movePassengerJourneyType;

    private java.lang.Short boardingSequenceOffset;

    private java.lang.Boolean ignorePNRsWithInvalidFromJourney;

    public MoveJourneyBookingsRequestData() {
    }

    public MoveJourneyBookingsRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.Journey fromJourney,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] toJourneys,
           java.lang.String[] recordLocators,
           java.lang.String changeReasonCode,
           java.lang.Boolean keepWaitListStatus,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Segment effectedSegment,
           java.lang.Short toleranceMinutes,
           java.lang.Boolean addAdHocConnection,
           java.lang.Boolean adHocIsForGeneralUse,
           java.lang.String bookingComment,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType movePassengerJourneyType,
           java.lang.Short boardingSequenceOffset,
           java.lang.Boolean ignorePNRsWithInvalidFromJourney) {
           this.fromJourney = fromJourney;
           this.toJourneys = toJourneys;
           this.recordLocators = recordLocators;
           this.changeReasonCode = changeReasonCode;
           this.keepWaitListStatus = keepWaitListStatus;
           this.effectedSegment = effectedSegment;
           this.toleranceMinutes = toleranceMinutes;
           this.addAdHocConnection = addAdHocConnection;
           this.adHocIsForGeneralUse = adHocIsForGeneralUse;
           this.bookingComment = bookingComment;
           this.movePassengerJourneyType = movePassengerJourneyType;
           this.boardingSequenceOffset = boardingSequenceOffset;
           this.ignorePNRsWithInvalidFromJourney = ignorePNRsWithInvalidFromJourney;
    }


    /**
     * Gets the fromJourney value for this MoveJourneyBookingsRequestData.
     * 
     * @return fromJourney
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Journey getFromJourney() {
        return fromJourney;
    }


    /**
     * Sets the fromJourney value for this MoveJourneyBookingsRequestData.
     * 
     * @param fromJourney
     */
    public void setFromJourney(com.navitaire.schemas.WebServices.DataContracts.Booking.Journey fromJourney) {
        this.fromJourney = fromJourney;
    }


    /**
     * Gets the toJourneys value for this MoveJourneyBookingsRequestData.
     * 
     * @return toJourneys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] getToJourneys() {
        return toJourneys;
    }


    /**
     * Sets the toJourneys value for this MoveJourneyBookingsRequestData.
     * 
     * @param toJourneys
     */
    public void setToJourneys(com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] toJourneys) {
        this.toJourneys = toJourneys;
    }


    /**
     * Gets the recordLocators value for this MoveJourneyBookingsRequestData.
     * 
     * @return recordLocators
     */
    public java.lang.String[] getRecordLocators() {
        return recordLocators;
    }


    /**
     * Sets the recordLocators value for this MoveJourneyBookingsRequestData.
     * 
     * @param recordLocators
     */
    public void setRecordLocators(java.lang.String[] recordLocators) {
        this.recordLocators = recordLocators;
    }


    /**
     * Gets the changeReasonCode value for this MoveJourneyBookingsRequestData.
     * 
     * @return changeReasonCode
     */
    public java.lang.String getChangeReasonCode() {
        return changeReasonCode;
    }


    /**
     * Sets the changeReasonCode value for this MoveJourneyBookingsRequestData.
     * 
     * @param changeReasonCode
     */
    public void setChangeReasonCode(java.lang.String changeReasonCode) {
        this.changeReasonCode = changeReasonCode;
    }


    /**
     * Gets the keepWaitListStatus value for this MoveJourneyBookingsRequestData.
     * 
     * @return keepWaitListStatus
     */
    public java.lang.Boolean getKeepWaitListStatus() {
        return keepWaitListStatus;
    }


    /**
     * Sets the keepWaitListStatus value for this MoveJourneyBookingsRequestData.
     * 
     * @param keepWaitListStatus
     */
    public void setKeepWaitListStatus(java.lang.Boolean keepWaitListStatus) {
        this.keepWaitListStatus = keepWaitListStatus;
    }


    /**
     * Gets the effectedSegment value for this MoveJourneyBookingsRequestData.
     * 
     * @return effectedSegment
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Segment getEffectedSegment() {
        return effectedSegment;
    }


    /**
     * Sets the effectedSegment value for this MoveJourneyBookingsRequestData.
     * 
     * @param effectedSegment
     */
    public void setEffectedSegment(com.navitaire.schemas.WebServices.DataContracts.Booking.Segment effectedSegment) {
        this.effectedSegment = effectedSegment;
    }


    /**
     * Gets the toleranceMinutes value for this MoveJourneyBookingsRequestData.
     * 
     * @return toleranceMinutes
     */
    public java.lang.Short getToleranceMinutes() {
        return toleranceMinutes;
    }


    /**
     * Sets the toleranceMinutes value for this MoveJourneyBookingsRequestData.
     * 
     * @param toleranceMinutes
     */
    public void setToleranceMinutes(java.lang.Short toleranceMinutes) {
        this.toleranceMinutes = toleranceMinutes;
    }


    /**
     * Gets the addAdHocConnection value for this MoveJourneyBookingsRequestData.
     * 
     * @return addAdHocConnection
     */
    public java.lang.Boolean getAddAdHocConnection() {
        return addAdHocConnection;
    }


    /**
     * Sets the addAdHocConnection value for this MoveJourneyBookingsRequestData.
     * 
     * @param addAdHocConnection
     */
    public void setAddAdHocConnection(java.lang.Boolean addAdHocConnection) {
        this.addAdHocConnection = addAdHocConnection;
    }


    /**
     * Gets the adHocIsForGeneralUse value for this MoveJourneyBookingsRequestData.
     * 
     * @return adHocIsForGeneralUse
     */
    public java.lang.Boolean getAdHocIsForGeneralUse() {
        return adHocIsForGeneralUse;
    }


    /**
     * Sets the adHocIsForGeneralUse value for this MoveJourneyBookingsRequestData.
     * 
     * @param adHocIsForGeneralUse
     */
    public void setAdHocIsForGeneralUse(java.lang.Boolean adHocIsForGeneralUse) {
        this.adHocIsForGeneralUse = adHocIsForGeneralUse;
    }


    /**
     * Gets the bookingComment value for this MoveJourneyBookingsRequestData.
     * 
     * @return bookingComment
     */
    public java.lang.String getBookingComment() {
        return bookingComment;
    }


    /**
     * Sets the bookingComment value for this MoveJourneyBookingsRequestData.
     * 
     * @param bookingComment
     */
    public void setBookingComment(java.lang.String bookingComment) {
        this.bookingComment = bookingComment;
    }


    /**
     * Gets the movePassengerJourneyType value for this MoveJourneyBookingsRequestData.
     * 
     * @return movePassengerJourneyType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType getMovePassengerJourneyType() {
        return movePassengerJourneyType;
    }


    /**
     * Sets the movePassengerJourneyType value for this MoveJourneyBookingsRequestData.
     * 
     * @param movePassengerJourneyType
     */
    public void setMovePassengerJourneyType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType movePassengerJourneyType) {
        this.movePassengerJourneyType = movePassengerJourneyType;
    }


    /**
     * Gets the boardingSequenceOffset value for this MoveJourneyBookingsRequestData.
     * 
     * @return boardingSequenceOffset
     */
    public java.lang.Short getBoardingSequenceOffset() {
        return boardingSequenceOffset;
    }


    /**
     * Sets the boardingSequenceOffset value for this MoveJourneyBookingsRequestData.
     * 
     * @param boardingSequenceOffset
     */
    public void setBoardingSequenceOffset(java.lang.Short boardingSequenceOffset) {
        this.boardingSequenceOffset = boardingSequenceOffset;
    }


    /**
     * Gets the ignorePNRsWithInvalidFromJourney value for this MoveJourneyBookingsRequestData.
     * 
     * @return ignorePNRsWithInvalidFromJourney
     */
    public java.lang.Boolean getIgnorePNRsWithInvalidFromJourney() {
        return ignorePNRsWithInvalidFromJourney;
    }


    /**
     * Sets the ignorePNRsWithInvalidFromJourney value for this MoveJourneyBookingsRequestData.
     * 
     * @param ignorePNRsWithInvalidFromJourney
     */
    public void setIgnorePNRsWithInvalidFromJourney(java.lang.Boolean ignorePNRsWithInvalidFromJourney) {
        this.ignorePNRsWithInvalidFromJourney = ignorePNRsWithInvalidFromJourney;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MoveJourneyBookingsRequestData)) return false;
        MoveJourneyBookingsRequestData other = (MoveJourneyBookingsRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fromJourney==null && other.getFromJourney()==null) || 
             (this.fromJourney!=null &&
              this.fromJourney.equals(other.getFromJourney()))) &&
            ((this.toJourneys==null && other.getToJourneys()==null) || 
             (this.toJourneys!=null &&
              java.util.Arrays.equals(this.toJourneys, other.getToJourneys()))) &&
            ((this.recordLocators==null && other.getRecordLocators()==null) || 
             (this.recordLocators!=null &&
              java.util.Arrays.equals(this.recordLocators, other.getRecordLocators()))) &&
            ((this.changeReasonCode==null && other.getChangeReasonCode()==null) || 
             (this.changeReasonCode!=null &&
              this.changeReasonCode.equals(other.getChangeReasonCode()))) &&
            ((this.keepWaitListStatus==null && other.getKeepWaitListStatus()==null) || 
             (this.keepWaitListStatus!=null &&
              this.keepWaitListStatus.equals(other.getKeepWaitListStatus()))) &&
            ((this.effectedSegment==null && other.getEffectedSegment()==null) || 
             (this.effectedSegment!=null &&
              this.effectedSegment.equals(other.getEffectedSegment()))) &&
            ((this.toleranceMinutes==null && other.getToleranceMinutes()==null) || 
             (this.toleranceMinutes!=null &&
              this.toleranceMinutes.equals(other.getToleranceMinutes()))) &&
            ((this.addAdHocConnection==null && other.getAddAdHocConnection()==null) || 
             (this.addAdHocConnection!=null &&
              this.addAdHocConnection.equals(other.getAddAdHocConnection()))) &&
            ((this.adHocIsForGeneralUse==null && other.getAdHocIsForGeneralUse()==null) || 
             (this.adHocIsForGeneralUse!=null &&
              this.adHocIsForGeneralUse.equals(other.getAdHocIsForGeneralUse()))) &&
            ((this.bookingComment==null && other.getBookingComment()==null) || 
             (this.bookingComment!=null &&
              this.bookingComment.equals(other.getBookingComment()))) &&
            ((this.movePassengerJourneyType==null && other.getMovePassengerJourneyType()==null) || 
             (this.movePassengerJourneyType!=null &&
              this.movePassengerJourneyType.equals(other.getMovePassengerJourneyType()))) &&
            ((this.boardingSequenceOffset==null && other.getBoardingSequenceOffset()==null) || 
             (this.boardingSequenceOffset!=null &&
              this.boardingSequenceOffset.equals(other.getBoardingSequenceOffset()))) &&
            ((this.ignorePNRsWithInvalidFromJourney==null && other.getIgnorePNRsWithInvalidFromJourney()==null) || 
             (this.ignorePNRsWithInvalidFromJourney!=null &&
              this.ignorePNRsWithInvalidFromJourney.equals(other.getIgnorePNRsWithInvalidFromJourney())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFromJourney() != null) {
            _hashCode += getFromJourney().hashCode();
        }
        if (getToJourneys() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getToJourneys());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getToJourneys(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRecordLocators() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRecordLocators());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRecordLocators(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getChangeReasonCode() != null) {
            _hashCode += getChangeReasonCode().hashCode();
        }
        if (getKeepWaitListStatus() != null) {
            _hashCode += getKeepWaitListStatus().hashCode();
        }
        if (getEffectedSegment() != null) {
            _hashCode += getEffectedSegment().hashCode();
        }
        if (getToleranceMinutes() != null) {
            _hashCode += getToleranceMinutes().hashCode();
        }
        if (getAddAdHocConnection() != null) {
            _hashCode += getAddAdHocConnection().hashCode();
        }
        if (getAdHocIsForGeneralUse() != null) {
            _hashCode += getAdHocIsForGeneralUse().hashCode();
        }
        if (getBookingComment() != null) {
            _hashCode += getBookingComment().hashCode();
        }
        if (getMovePassengerJourneyType() != null) {
            _hashCode += getMovePassengerJourneyType().hashCode();
        }
        if (getBoardingSequenceOffset() != null) {
            _hashCode += getBoardingSequenceOffset().hashCode();
        }
        if (getIgnorePNRsWithInvalidFromJourney() != null) {
            _hashCode += getIgnorePNRsWithInvalidFromJourney().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MoveJourneyBookingsRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyBookingsRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromJourney");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FromJourney"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toJourneys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ToJourneys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocators");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocators"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeReasonCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChangeReasonCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keepWaitListStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "KeepWaitListStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("effectedSegment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EffectedSegment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toleranceMinutes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ToleranceMinutes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addAdHocConnection");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AddAdHocConnection"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adHocIsForGeneralUse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AdHocIsForGeneralUse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingComment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("movePassengerJourneyType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MovePassengerJourneyType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "MovePassengerJourneyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("boardingSequenceOffset");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BoardingSequenceOffset"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ignorePNRsWithInvalidFromJourney");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IgnorePNRsWithInvalidFromJourney"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
