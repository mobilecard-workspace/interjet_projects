/**
 * GetBookingHistoryRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GetBookingHistoryRequestData  implements java.io.Serializable {
    private java.lang.Long bookingID;

    private java.lang.String historyCode;

    private java.lang.Boolean getTotalCount;

    private java.lang.Long lastID;

    private java.lang.Short pageSize;

    private java.lang.Boolean retrieveFromArchive;

    public GetBookingHistoryRequestData() {
    }

    public GetBookingHistoryRequestData(
           java.lang.Long bookingID,
           java.lang.String historyCode,
           java.lang.Boolean getTotalCount,
           java.lang.Long lastID,
           java.lang.Short pageSize,
           java.lang.Boolean retrieveFromArchive) {
           this.bookingID = bookingID;
           this.historyCode = historyCode;
           this.getTotalCount = getTotalCount;
           this.lastID = lastID;
           this.pageSize = pageSize;
           this.retrieveFromArchive = retrieveFromArchive;
    }


    /**
     * Gets the bookingID value for this GetBookingHistoryRequestData.
     * 
     * @return bookingID
     */
    public java.lang.Long getBookingID() {
        return bookingID;
    }


    /**
     * Sets the bookingID value for this GetBookingHistoryRequestData.
     * 
     * @param bookingID
     */
    public void setBookingID(java.lang.Long bookingID) {
        this.bookingID = bookingID;
    }


    /**
     * Gets the historyCode value for this GetBookingHistoryRequestData.
     * 
     * @return historyCode
     */
    public java.lang.String getHistoryCode() {
        return historyCode;
    }


    /**
     * Sets the historyCode value for this GetBookingHistoryRequestData.
     * 
     * @param historyCode
     */
    public void setHistoryCode(java.lang.String historyCode) {
        this.historyCode = historyCode;
    }


    /**
     * Gets the getTotalCount value for this GetBookingHistoryRequestData.
     * 
     * @return getTotalCount
     */
    public java.lang.Boolean getGetTotalCount() {
        return getTotalCount;
    }


    /**
     * Sets the getTotalCount value for this GetBookingHistoryRequestData.
     * 
     * @param getTotalCount
     */
    public void setGetTotalCount(java.lang.Boolean getTotalCount) {
        this.getTotalCount = getTotalCount;
    }


    /**
     * Gets the lastID value for this GetBookingHistoryRequestData.
     * 
     * @return lastID
     */
    public java.lang.Long getLastID() {
        return lastID;
    }


    /**
     * Sets the lastID value for this GetBookingHistoryRequestData.
     * 
     * @param lastID
     */
    public void setLastID(java.lang.Long lastID) {
        this.lastID = lastID;
    }


    /**
     * Gets the pageSize value for this GetBookingHistoryRequestData.
     * 
     * @return pageSize
     */
    public java.lang.Short getPageSize() {
        return pageSize;
    }


    /**
     * Sets the pageSize value for this GetBookingHistoryRequestData.
     * 
     * @param pageSize
     */
    public void setPageSize(java.lang.Short pageSize) {
        this.pageSize = pageSize;
    }


    /**
     * Gets the retrieveFromArchive value for this GetBookingHistoryRequestData.
     * 
     * @return retrieveFromArchive
     */
    public java.lang.Boolean getRetrieveFromArchive() {
        return retrieveFromArchive;
    }


    /**
     * Sets the retrieveFromArchive value for this GetBookingHistoryRequestData.
     * 
     * @param retrieveFromArchive
     */
    public void setRetrieveFromArchive(java.lang.Boolean retrieveFromArchive) {
        this.retrieveFromArchive = retrieveFromArchive;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBookingHistoryRequestData)) return false;
        GetBookingHistoryRequestData other = (GetBookingHistoryRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingID==null && other.getBookingID()==null) || 
             (this.bookingID!=null &&
              this.bookingID.equals(other.getBookingID()))) &&
            ((this.historyCode==null && other.getHistoryCode()==null) || 
             (this.historyCode!=null &&
              this.historyCode.equals(other.getHistoryCode()))) &&
            ((this.getTotalCount==null && other.getGetTotalCount()==null) || 
             (this.getTotalCount!=null &&
              this.getTotalCount.equals(other.getGetTotalCount()))) &&
            ((this.lastID==null && other.getLastID()==null) || 
             (this.lastID!=null &&
              this.lastID.equals(other.getLastID()))) &&
            ((this.pageSize==null && other.getPageSize()==null) || 
             (this.pageSize!=null &&
              this.pageSize.equals(other.getPageSize()))) &&
            ((this.retrieveFromArchive==null && other.getRetrieveFromArchive()==null) || 
             (this.retrieveFromArchive!=null &&
              this.retrieveFromArchive.equals(other.getRetrieveFromArchive())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingID() != null) {
            _hashCode += getBookingID().hashCode();
        }
        if (getHistoryCode() != null) {
            _hashCode += getHistoryCode().hashCode();
        }
        if (getGetTotalCount() != null) {
            _hashCode += getGetTotalCount().hashCode();
        }
        if (getLastID() != null) {
            _hashCode += getLastID().hashCode();
        }
        if (getPageSize() != null) {
            _hashCode += getPageSize().hashCode();
        }
        if (getRetrieveFromArchive() != null) {
            _hashCode += getRetrieveFromArchive().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBookingHistoryRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingHistoryRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("historyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "HistoryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getTotalCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetTotalCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LastID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageSize");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PageSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retrieveFromArchive");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RetrieveFromArchive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
