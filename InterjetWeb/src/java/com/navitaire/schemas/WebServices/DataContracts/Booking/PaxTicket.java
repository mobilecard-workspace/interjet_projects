/**
 * PaxTicket.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxTicket  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Short passengerNumber;

    private java.lang.String ticketIndicator;

    private java.lang.String ticketNumber;

    private java.lang.String ticketStatus;

    private java.lang.String infantTicketNumber;

    public PaxTicket() {
    }

    public PaxTicket(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Short passengerNumber,
           java.lang.String ticketIndicator,
           java.lang.String ticketNumber,
           java.lang.String ticketStatus,
           java.lang.String infantTicketNumber) {
        super(
            state);
        this.passengerNumber = passengerNumber;
        this.ticketIndicator = ticketIndicator;
        this.ticketNumber = ticketNumber;
        this.ticketStatus = ticketStatus;
        this.infantTicketNumber = infantTicketNumber;
    }


    /**
     * Gets the passengerNumber value for this PaxTicket.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this PaxTicket.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the ticketIndicator value for this PaxTicket.
     * 
     * @return ticketIndicator
     */
    public java.lang.String getTicketIndicator() {
        return ticketIndicator;
    }


    /**
     * Sets the ticketIndicator value for this PaxTicket.
     * 
     * @param ticketIndicator
     */
    public void setTicketIndicator(java.lang.String ticketIndicator) {
        this.ticketIndicator = ticketIndicator;
    }


    /**
     * Gets the ticketNumber value for this PaxTicket.
     * 
     * @return ticketNumber
     */
    public java.lang.String getTicketNumber() {
        return ticketNumber;
    }


    /**
     * Sets the ticketNumber value for this PaxTicket.
     * 
     * @param ticketNumber
     */
    public void setTicketNumber(java.lang.String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }


    /**
     * Gets the ticketStatus value for this PaxTicket.
     * 
     * @return ticketStatus
     */
    public java.lang.String getTicketStatus() {
        return ticketStatus;
    }


    /**
     * Sets the ticketStatus value for this PaxTicket.
     * 
     * @param ticketStatus
     */
    public void setTicketStatus(java.lang.String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }


    /**
     * Gets the infantTicketNumber value for this PaxTicket.
     * 
     * @return infantTicketNumber
     */
    public java.lang.String getInfantTicketNumber() {
        return infantTicketNumber;
    }


    /**
     * Sets the infantTicketNumber value for this PaxTicket.
     * 
     * @param infantTicketNumber
     */
    public void setInfantTicketNumber(java.lang.String infantTicketNumber) {
        this.infantTicketNumber = infantTicketNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxTicket)) return false;
        PaxTicket other = (PaxTicket) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.ticketIndicator==null && other.getTicketIndicator()==null) || 
             (this.ticketIndicator!=null &&
              this.ticketIndicator.equals(other.getTicketIndicator()))) &&
            ((this.ticketNumber==null && other.getTicketNumber()==null) || 
             (this.ticketNumber!=null &&
              this.ticketNumber.equals(other.getTicketNumber()))) &&
            ((this.ticketStatus==null && other.getTicketStatus()==null) || 
             (this.ticketStatus!=null &&
              this.ticketStatus.equals(other.getTicketStatus()))) &&
            ((this.infantTicketNumber==null && other.getInfantTicketNumber()==null) || 
             (this.infantTicketNumber!=null &&
              this.infantTicketNumber.equals(other.getInfantTicketNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getTicketIndicator() != null) {
            _hashCode += getTicketIndicator().hashCode();
        }
        if (getTicketNumber() != null) {
            _hashCode += getTicketNumber().hashCode();
        }
        if (getTicketStatus() != null) {
            _hashCode += getTicketStatus().hashCode();
        }
        if (getInfantTicketNumber() != null) {
            _hashCode += getInfantTicketNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxTicket.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicket"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TicketIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TicketNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TicketStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infantTicketNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InfantTicketNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
