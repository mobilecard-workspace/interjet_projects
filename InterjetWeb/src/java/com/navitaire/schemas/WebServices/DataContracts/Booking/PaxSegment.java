/**
 * PaxSegment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxSegment  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String boardingSequence;

    private java.util.Calendar createdDate;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LiftStatus liftStatus;

    private java.lang.String overBookIndicator;

    private java.lang.Short passengerNumber;

    private java.util.Calendar priorityDate;

    private java.lang.String[] tripType;

    private java.lang.Boolean timeChanged;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale POS;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS;

    private java.lang.String verifiedTravelDocs;

    private java.util.Calendar modifiedDate;

    private java.util.Calendar activityDate;

    private java.lang.Short baggageAllowanceWeight;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightType baggageAllowanceWeightType;

    private java.lang.Boolean baggageAllowanceUsed;

    public PaxSegment() {
    }

    public PaxSegment(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String boardingSequence,
           java.util.Calendar createdDate,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LiftStatus liftStatus,
           java.lang.String overBookIndicator,
           java.lang.Short passengerNumber,
           java.util.Calendar priorityDate,
           java.lang.String[] tripType,
           java.lang.Boolean timeChanged,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale POS,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS,
           java.lang.String verifiedTravelDocs,
           java.util.Calendar modifiedDate,
           java.util.Calendar activityDate,
           java.lang.Short baggageAllowanceWeight,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightType baggageAllowanceWeightType,
           java.lang.Boolean baggageAllowanceUsed) {
        super(
            state);
        this.boardingSequence = boardingSequence;
        this.createdDate = createdDate;
        this.liftStatus = liftStatus;
        this.overBookIndicator = overBookIndicator;
        this.passengerNumber = passengerNumber;
        this.priorityDate = priorityDate;
        this.tripType = tripType;
        this.timeChanged = timeChanged;
        this.POS = POS;
        this.sourcePOS = sourcePOS;
        this.verifiedTravelDocs = verifiedTravelDocs;
        this.modifiedDate = modifiedDate;
        this.activityDate = activityDate;
        this.baggageAllowanceWeight = baggageAllowanceWeight;
        this.baggageAllowanceWeightType = baggageAllowanceWeightType;
        this.baggageAllowanceUsed = baggageAllowanceUsed;
    }


    /**
     * Gets the boardingSequence value for this PaxSegment.
     * 
     * @return boardingSequence
     */
    public java.lang.String getBoardingSequence() {
        return boardingSequence;
    }


    /**
     * Sets the boardingSequence value for this PaxSegment.
     * 
     * @param boardingSequence
     */
    public void setBoardingSequence(java.lang.String boardingSequence) {
        this.boardingSequence = boardingSequence;
    }


    /**
     * Gets the createdDate value for this PaxSegment.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this PaxSegment.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }


    /**
     * Gets the liftStatus value for this PaxSegment.
     * 
     * @return liftStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LiftStatus getLiftStatus() {
        return liftStatus;
    }


    /**
     * Sets the liftStatus value for this PaxSegment.
     * 
     * @param liftStatus
     */
    public void setLiftStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LiftStatus liftStatus) {
        this.liftStatus = liftStatus;
    }


    /**
     * Gets the overBookIndicator value for this PaxSegment.
     * 
     * @return overBookIndicator
     */
    public java.lang.String getOverBookIndicator() {
        return overBookIndicator;
    }


    /**
     * Sets the overBookIndicator value for this PaxSegment.
     * 
     * @param overBookIndicator
     */
    public void setOverBookIndicator(java.lang.String overBookIndicator) {
        this.overBookIndicator = overBookIndicator;
    }


    /**
     * Gets the passengerNumber value for this PaxSegment.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this PaxSegment.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the priorityDate value for this PaxSegment.
     * 
     * @return priorityDate
     */
    public java.util.Calendar getPriorityDate() {
        return priorityDate;
    }


    /**
     * Sets the priorityDate value for this PaxSegment.
     * 
     * @param priorityDate
     */
    public void setPriorityDate(java.util.Calendar priorityDate) {
        this.priorityDate = priorityDate;
    }


    /**
     * Gets the tripType value for this PaxSegment.
     * 
     * @return tripType
     */
    public java.lang.String[] getTripType() {
        return tripType;
    }


    /**
     * Sets the tripType value for this PaxSegment.
     * 
     * @param tripType
     */
    public void setTripType(java.lang.String[] tripType) {
        this.tripType = tripType;
    }


    /**
     * Gets the timeChanged value for this PaxSegment.
     * 
     * @return timeChanged
     */
    public java.lang.Boolean getTimeChanged() {
        return timeChanged;
    }


    /**
     * Sets the timeChanged value for this PaxSegment.
     * 
     * @param timeChanged
     */
    public void setTimeChanged(java.lang.Boolean timeChanged) {
        this.timeChanged = timeChanged;
    }


    /**
     * Gets the POS value for this PaxSegment.
     * 
     * @return POS
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getPOS() {
        return POS;
    }


    /**
     * Sets the POS value for this PaxSegment.
     * 
     * @param POS
     */
    public void setPOS(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale POS) {
        this.POS = POS;
    }


    /**
     * Gets the sourcePOS value for this PaxSegment.
     * 
     * @return sourcePOS
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePOS() {
        return sourcePOS;
    }


    /**
     * Sets the sourcePOS value for this PaxSegment.
     * 
     * @param sourcePOS
     */
    public void setSourcePOS(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS) {
        this.sourcePOS = sourcePOS;
    }


    /**
     * Gets the verifiedTravelDocs value for this PaxSegment.
     * 
     * @return verifiedTravelDocs
     */
    public java.lang.String getVerifiedTravelDocs() {
        return verifiedTravelDocs;
    }


    /**
     * Sets the verifiedTravelDocs value for this PaxSegment.
     * 
     * @param verifiedTravelDocs
     */
    public void setVerifiedTravelDocs(java.lang.String verifiedTravelDocs) {
        this.verifiedTravelDocs = verifiedTravelDocs;
    }


    /**
     * Gets the modifiedDate value for this PaxSegment.
     * 
     * @return modifiedDate
     */
    public java.util.Calendar getModifiedDate() {
        return modifiedDate;
    }


    /**
     * Sets the modifiedDate value for this PaxSegment.
     * 
     * @param modifiedDate
     */
    public void setModifiedDate(java.util.Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    /**
     * Gets the activityDate value for this PaxSegment.
     * 
     * @return activityDate
     */
    public java.util.Calendar getActivityDate() {
        return activityDate;
    }


    /**
     * Sets the activityDate value for this PaxSegment.
     * 
     * @param activityDate
     */
    public void setActivityDate(java.util.Calendar activityDate) {
        this.activityDate = activityDate;
    }


    /**
     * Gets the baggageAllowanceWeight value for this PaxSegment.
     * 
     * @return baggageAllowanceWeight
     */
    public java.lang.Short getBaggageAllowanceWeight() {
        return baggageAllowanceWeight;
    }


    /**
     * Sets the baggageAllowanceWeight value for this PaxSegment.
     * 
     * @param baggageAllowanceWeight
     */
    public void setBaggageAllowanceWeight(java.lang.Short baggageAllowanceWeight) {
        this.baggageAllowanceWeight = baggageAllowanceWeight;
    }


    /**
     * Gets the baggageAllowanceWeightType value for this PaxSegment.
     * 
     * @return baggageAllowanceWeightType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightType getBaggageAllowanceWeightType() {
        return baggageAllowanceWeightType;
    }


    /**
     * Sets the baggageAllowanceWeightType value for this PaxSegment.
     * 
     * @param baggageAllowanceWeightType
     */
    public void setBaggageAllowanceWeightType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightType baggageAllowanceWeightType) {
        this.baggageAllowanceWeightType = baggageAllowanceWeightType;
    }


    /**
     * Gets the baggageAllowanceUsed value for this PaxSegment.
     * 
     * @return baggageAllowanceUsed
     */
    public java.lang.Boolean getBaggageAllowanceUsed() {
        return baggageAllowanceUsed;
    }


    /**
     * Sets the baggageAllowanceUsed value for this PaxSegment.
     * 
     * @param baggageAllowanceUsed
     */
    public void setBaggageAllowanceUsed(java.lang.Boolean baggageAllowanceUsed) {
        this.baggageAllowanceUsed = baggageAllowanceUsed;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxSegment)) return false;
        PaxSegment other = (PaxSegment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.boardingSequence==null && other.getBoardingSequence()==null) || 
             (this.boardingSequence!=null &&
              this.boardingSequence.equals(other.getBoardingSequence()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate()))) &&
            ((this.liftStatus==null && other.getLiftStatus()==null) || 
             (this.liftStatus!=null &&
              this.liftStatus.equals(other.getLiftStatus()))) &&
            ((this.overBookIndicator==null && other.getOverBookIndicator()==null) || 
             (this.overBookIndicator!=null &&
              this.overBookIndicator.equals(other.getOverBookIndicator()))) &&
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.priorityDate==null && other.getPriorityDate()==null) || 
             (this.priorityDate!=null &&
              this.priorityDate.equals(other.getPriorityDate()))) &&
            ((this.tripType==null && other.getTripType()==null) || 
             (this.tripType!=null &&
              java.util.Arrays.equals(this.tripType, other.getTripType()))) &&
            ((this.timeChanged==null && other.getTimeChanged()==null) || 
             (this.timeChanged!=null &&
              this.timeChanged.equals(other.getTimeChanged()))) &&
            ((this.POS==null && other.getPOS()==null) || 
             (this.POS!=null &&
              this.POS.equals(other.getPOS()))) &&
            ((this.sourcePOS==null && other.getSourcePOS()==null) || 
             (this.sourcePOS!=null &&
              this.sourcePOS.equals(other.getSourcePOS()))) &&
            ((this.verifiedTravelDocs==null && other.getVerifiedTravelDocs()==null) || 
             (this.verifiedTravelDocs!=null &&
              this.verifiedTravelDocs.equals(other.getVerifiedTravelDocs()))) &&
            ((this.modifiedDate==null && other.getModifiedDate()==null) || 
             (this.modifiedDate!=null &&
              this.modifiedDate.equals(other.getModifiedDate()))) &&
            ((this.activityDate==null && other.getActivityDate()==null) || 
             (this.activityDate!=null &&
              this.activityDate.equals(other.getActivityDate()))) &&
            ((this.baggageAllowanceWeight==null && other.getBaggageAllowanceWeight()==null) || 
             (this.baggageAllowanceWeight!=null &&
              this.baggageAllowanceWeight.equals(other.getBaggageAllowanceWeight()))) &&
            ((this.baggageAllowanceWeightType==null && other.getBaggageAllowanceWeightType()==null) || 
             (this.baggageAllowanceWeightType!=null &&
              this.baggageAllowanceWeightType.equals(other.getBaggageAllowanceWeightType()))) &&
            ((this.baggageAllowanceUsed==null && other.getBaggageAllowanceUsed()==null) || 
             (this.baggageAllowanceUsed!=null &&
              this.baggageAllowanceUsed.equals(other.getBaggageAllowanceUsed())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBoardingSequence() != null) {
            _hashCode += getBoardingSequence().hashCode();
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        if (getLiftStatus() != null) {
            _hashCode += getLiftStatus().hashCode();
        }
        if (getOverBookIndicator() != null) {
            _hashCode += getOverBookIndicator().hashCode();
        }
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getPriorityDate() != null) {
            _hashCode += getPriorityDate().hashCode();
        }
        if (getTripType() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTripType());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTripType(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTimeChanged() != null) {
            _hashCode += getTimeChanged().hashCode();
        }
        if (getPOS() != null) {
            _hashCode += getPOS().hashCode();
        }
        if (getSourcePOS() != null) {
            _hashCode += getSourcePOS().hashCode();
        }
        if (getVerifiedTravelDocs() != null) {
            _hashCode += getVerifiedTravelDocs().hashCode();
        }
        if (getModifiedDate() != null) {
            _hashCode += getModifiedDate().hashCode();
        }
        if (getActivityDate() != null) {
            _hashCode += getActivityDate().hashCode();
        }
        if (getBaggageAllowanceWeight() != null) {
            _hashCode += getBaggageAllowanceWeight().hashCode();
        }
        if (getBaggageAllowanceWeightType() != null) {
            _hashCode += getBaggageAllowanceWeightType().hashCode();
        }
        if (getBaggageAllowanceUsed() != null) {
            _hashCode += getBaggageAllowanceUsed().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxSegment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSegment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("boardingSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BoardingSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("liftStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LiftStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "LiftStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overBookIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverBookIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priorityDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriorityDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tripType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TripType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "TripType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeChanged");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TimeChanged"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "POS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePOS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verifiedTravelDocs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "VerifiedTravelDocs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ModifiedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activityDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActivityDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageAllowanceWeight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageAllowanceWeight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageAllowanceWeightType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageAllowanceWeightType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "WeightType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageAllowanceUsed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageAllowanceUsed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
