/**
 * PriceRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PriceRequestData  implements java.io.Serializable {
    private java.lang.Boolean refareItinerary;

    private java.lang.String currencyCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale;

    private java.lang.String[] fareTypes;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PricingType pricingType;

    private java.lang.Boolean allowNoFare;

    private org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter;

    public PriceRequestData() {
    }

    public PriceRequestData(
           java.lang.Boolean refareItinerary,
           java.lang.String currencyCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS,
           com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale,
           java.lang.String[] fareTypes,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PricingType pricingType,
           java.lang.Boolean allowNoFare,
           org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
           this.refareItinerary = refareItinerary;
           this.currencyCode = currencyCode;
           this.sourcePOS = sourcePOS;
           this.typeOfSale = typeOfSale;
           this.fareTypes = fareTypes;
           this.passengers = passengers;
           this.pricingType = pricingType;
           this.allowNoFare = allowNoFare;
           this.loyaltyFilter = loyaltyFilter;
    }


    /**
     * Gets the refareItinerary value for this PriceRequestData.
     * 
     * @return refareItinerary
     */
    public java.lang.Boolean getRefareItinerary() {
        return refareItinerary;
    }


    /**
     * Sets the refareItinerary value for this PriceRequestData.
     * 
     * @param refareItinerary
     */
    public void setRefareItinerary(java.lang.Boolean refareItinerary) {
        this.refareItinerary = refareItinerary;
    }


    /**
     * Gets the currencyCode value for this PriceRequestData.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this PriceRequestData.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the sourcePOS value for this PriceRequestData.
     * 
     * @return sourcePOS
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePOS() {
        return sourcePOS;
    }


    /**
     * Sets the sourcePOS value for this PriceRequestData.
     * 
     * @param sourcePOS
     */
    public void setSourcePOS(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS) {
        this.sourcePOS = sourcePOS;
    }


    /**
     * Gets the typeOfSale value for this PriceRequestData.
     * 
     * @return typeOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale getTypeOfSale() {
        return typeOfSale;
    }


    /**
     * Sets the typeOfSale value for this PriceRequestData.
     * 
     * @param typeOfSale
     */
    public void setTypeOfSale(com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale) {
        this.typeOfSale = typeOfSale;
    }


    /**
     * Gets the fareTypes value for this PriceRequestData.
     * 
     * @return fareTypes
     */
    public java.lang.String[] getFareTypes() {
        return fareTypes;
    }


    /**
     * Sets the fareTypes value for this PriceRequestData.
     * 
     * @param fareTypes
     */
    public void setFareTypes(java.lang.String[] fareTypes) {
        this.fareTypes = fareTypes;
    }


    /**
     * Gets the passengers value for this PriceRequestData.
     * 
     * @return passengers
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] getPassengers() {
        return passengers;
    }


    /**
     * Sets the passengers value for this PriceRequestData.
     * 
     * @param passengers
     */
    public void setPassengers(com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers) {
        this.passengers = passengers;
    }


    /**
     * Gets the pricingType value for this PriceRequestData.
     * 
     * @return pricingType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PricingType getPricingType() {
        return pricingType;
    }


    /**
     * Sets the pricingType value for this PriceRequestData.
     * 
     * @param pricingType
     */
    public void setPricingType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PricingType pricingType) {
        this.pricingType = pricingType;
    }


    /**
     * Gets the allowNoFare value for this PriceRequestData.
     * 
     * @return allowNoFare
     */
    public java.lang.Boolean getAllowNoFare() {
        return allowNoFare;
    }


    /**
     * Sets the allowNoFare value for this PriceRequestData.
     * 
     * @param allowNoFare
     */
    public void setAllowNoFare(java.lang.Boolean allowNoFare) {
        this.allowNoFare = allowNoFare;
    }


    /**
     * Gets the loyaltyFilter value for this PriceRequestData.
     * 
     * @return loyaltyFilter
     */
    public org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter getLoyaltyFilter() {
        return loyaltyFilter;
    }


    /**
     * Sets the loyaltyFilter value for this PriceRequestData.
     * 
     * @param loyaltyFilter
     */
    public void setLoyaltyFilter(org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
        this.loyaltyFilter = loyaltyFilter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PriceRequestData)) return false;
        PriceRequestData other = (PriceRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.refareItinerary==null && other.getRefareItinerary()==null) || 
             (this.refareItinerary!=null &&
              this.refareItinerary.equals(other.getRefareItinerary()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.sourcePOS==null && other.getSourcePOS()==null) || 
             (this.sourcePOS!=null &&
              this.sourcePOS.equals(other.getSourcePOS()))) &&
            ((this.typeOfSale==null && other.getTypeOfSale()==null) || 
             (this.typeOfSale!=null &&
              this.typeOfSale.equals(other.getTypeOfSale()))) &&
            ((this.fareTypes==null && other.getFareTypes()==null) || 
             (this.fareTypes!=null &&
              java.util.Arrays.equals(this.fareTypes, other.getFareTypes()))) &&
            ((this.passengers==null && other.getPassengers()==null) || 
             (this.passengers!=null &&
              java.util.Arrays.equals(this.passengers, other.getPassengers()))) &&
            ((this.pricingType==null && other.getPricingType()==null) || 
             (this.pricingType!=null &&
              this.pricingType.equals(other.getPricingType()))) &&
            ((this.allowNoFare==null && other.getAllowNoFare()==null) || 
             (this.allowNoFare!=null &&
              this.allowNoFare.equals(other.getAllowNoFare()))) &&
            ((this.loyaltyFilter==null && other.getLoyaltyFilter()==null) || 
             (this.loyaltyFilter!=null &&
              this.loyaltyFilter.equals(other.getLoyaltyFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRefareItinerary() != null) {
            _hashCode += getRefareItinerary().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getSourcePOS() != null) {
            _hashCode += getSourcePOS().hashCode();
        }
        if (getTypeOfSale() != null) {
            _hashCode += getTypeOfSale().hashCode();
        }
        if (getFareTypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFareTypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFareTypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPricingType() != null) {
            _hashCode += getPricingType().hashCode();
        }
        if (getAllowNoFare() != null) {
            _hashCode += getAllowNoFare().hashCode();
        }
        if (getLoyaltyFilter() != null) {
            _hashCode += getLoyaltyFilter().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PriceRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refareItinerary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RefareItinerary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePOS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passengers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pricingType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PricingType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PricingType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowNoFare");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AllowNoFare"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loyaltyFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LoyaltyFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LoyaltyFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
