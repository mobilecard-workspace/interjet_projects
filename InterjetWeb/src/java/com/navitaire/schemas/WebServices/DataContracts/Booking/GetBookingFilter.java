/**
 * GetBookingFilter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GetBookingFilter  implements java.io.Serializable {
    private java.lang.Boolean includeBookingComponents;

    private java.lang.Boolean includeThirdPartyRecordLocators;

    private java.lang.Boolean includePayments;

    private java.lang.Boolean includeContacts;

    private java.lang.Boolean includeBookingComments;

    private java.lang.Boolean includeBookingQueues;

    private java.lang.Boolean includePassengers;

    private java.lang.Boolean includePassengerAddresses;

    private java.lang.Boolean includePassengerTravelDocs;

    private java.lang.Boolean includePassengerFees;

    private java.lang.Boolean includePassengerJourneySSRs;

    private java.lang.Boolean includeBaggage;

    private java.lang.Boolean includeJourneys;

    private java.lang.Boolean includeFares;

    private java.lang.Boolean includePaxSeatInfo;

    public GetBookingFilter() {
    }

    public GetBookingFilter(
           java.lang.Boolean includeBookingComponents,
           java.lang.Boolean includeThirdPartyRecordLocators,
           java.lang.Boolean includePayments,
           java.lang.Boolean includeContacts,
           java.lang.Boolean includeBookingComments,
           java.lang.Boolean includeBookingQueues,
           java.lang.Boolean includePassengers,
           java.lang.Boolean includePassengerAddresses,
           java.lang.Boolean includePassengerTravelDocs,
           java.lang.Boolean includePassengerFees,
           java.lang.Boolean includePassengerJourneySSRs,
           java.lang.Boolean includeBaggage,
           java.lang.Boolean includeJourneys,
           java.lang.Boolean includeFares,
           java.lang.Boolean includePaxSeatInfo) {
           this.includeBookingComponents = includeBookingComponents;
           this.includeThirdPartyRecordLocators = includeThirdPartyRecordLocators;
           this.includePayments = includePayments;
           this.includeContacts = includeContacts;
           this.includeBookingComments = includeBookingComments;
           this.includeBookingQueues = includeBookingQueues;
           this.includePassengers = includePassengers;
           this.includePassengerAddresses = includePassengerAddresses;
           this.includePassengerTravelDocs = includePassengerTravelDocs;
           this.includePassengerFees = includePassengerFees;
           this.includePassengerJourneySSRs = includePassengerJourneySSRs;
           this.includeBaggage = includeBaggage;
           this.includeJourneys = includeJourneys;
           this.includeFares = includeFares;
           this.includePaxSeatInfo = includePaxSeatInfo;
    }


    /**
     * Gets the includeBookingComponents value for this GetBookingFilter.
     * 
     * @return includeBookingComponents
     */
    public java.lang.Boolean getIncludeBookingComponents() {
        return includeBookingComponents;
    }


    /**
     * Sets the includeBookingComponents value for this GetBookingFilter.
     * 
     * @param includeBookingComponents
     */
    public void setIncludeBookingComponents(java.lang.Boolean includeBookingComponents) {
        this.includeBookingComponents = includeBookingComponents;
    }


    /**
     * Gets the includeThirdPartyRecordLocators value for this GetBookingFilter.
     * 
     * @return includeThirdPartyRecordLocators
     */
    public java.lang.Boolean getIncludeThirdPartyRecordLocators() {
        return includeThirdPartyRecordLocators;
    }


    /**
     * Sets the includeThirdPartyRecordLocators value for this GetBookingFilter.
     * 
     * @param includeThirdPartyRecordLocators
     */
    public void setIncludeThirdPartyRecordLocators(java.lang.Boolean includeThirdPartyRecordLocators) {
        this.includeThirdPartyRecordLocators = includeThirdPartyRecordLocators;
    }


    /**
     * Gets the includePayments value for this GetBookingFilter.
     * 
     * @return includePayments
     */
    public java.lang.Boolean getIncludePayments() {
        return includePayments;
    }


    /**
     * Sets the includePayments value for this GetBookingFilter.
     * 
     * @param includePayments
     */
    public void setIncludePayments(java.lang.Boolean includePayments) {
        this.includePayments = includePayments;
    }


    /**
     * Gets the includeContacts value for this GetBookingFilter.
     * 
     * @return includeContacts
     */
    public java.lang.Boolean getIncludeContacts() {
        return includeContacts;
    }


    /**
     * Sets the includeContacts value for this GetBookingFilter.
     * 
     * @param includeContacts
     */
    public void setIncludeContacts(java.lang.Boolean includeContacts) {
        this.includeContacts = includeContacts;
    }


    /**
     * Gets the includeBookingComments value for this GetBookingFilter.
     * 
     * @return includeBookingComments
     */
    public java.lang.Boolean getIncludeBookingComments() {
        return includeBookingComments;
    }


    /**
     * Sets the includeBookingComments value for this GetBookingFilter.
     * 
     * @param includeBookingComments
     */
    public void setIncludeBookingComments(java.lang.Boolean includeBookingComments) {
        this.includeBookingComments = includeBookingComments;
    }


    /**
     * Gets the includeBookingQueues value for this GetBookingFilter.
     * 
     * @return includeBookingQueues
     */
    public java.lang.Boolean getIncludeBookingQueues() {
        return includeBookingQueues;
    }


    /**
     * Sets the includeBookingQueues value for this GetBookingFilter.
     * 
     * @param includeBookingQueues
     */
    public void setIncludeBookingQueues(java.lang.Boolean includeBookingQueues) {
        this.includeBookingQueues = includeBookingQueues;
    }


    /**
     * Gets the includePassengers value for this GetBookingFilter.
     * 
     * @return includePassengers
     */
    public java.lang.Boolean getIncludePassengers() {
        return includePassengers;
    }


    /**
     * Sets the includePassengers value for this GetBookingFilter.
     * 
     * @param includePassengers
     */
    public void setIncludePassengers(java.lang.Boolean includePassengers) {
        this.includePassengers = includePassengers;
    }


    /**
     * Gets the includePassengerAddresses value for this GetBookingFilter.
     * 
     * @return includePassengerAddresses
     */
    public java.lang.Boolean getIncludePassengerAddresses() {
        return includePassengerAddresses;
    }


    /**
     * Sets the includePassengerAddresses value for this GetBookingFilter.
     * 
     * @param includePassengerAddresses
     */
    public void setIncludePassengerAddresses(java.lang.Boolean includePassengerAddresses) {
        this.includePassengerAddresses = includePassengerAddresses;
    }


    /**
     * Gets the includePassengerTravelDocs value for this GetBookingFilter.
     * 
     * @return includePassengerTravelDocs
     */
    public java.lang.Boolean getIncludePassengerTravelDocs() {
        return includePassengerTravelDocs;
    }


    /**
     * Sets the includePassengerTravelDocs value for this GetBookingFilter.
     * 
     * @param includePassengerTravelDocs
     */
    public void setIncludePassengerTravelDocs(java.lang.Boolean includePassengerTravelDocs) {
        this.includePassengerTravelDocs = includePassengerTravelDocs;
    }


    /**
     * Gets the includePassengerFees value for this GetBookingFilter.
     * 
     * @return includePassengerFees
     */
    public java.lang.Boolean getIncludePassengerFees() {
        return includePassengerFees;
    }


    /**
     * Sets the includePassengerFees value for this GetBookingFilter.
     * 
     * @param includePassengerFees
     */
    public void setIncludePassengerFees(java.lang.Boolean includePassengerFees) {
        this.includePassengerFees = includePassengerFees;
    }


    /**
     * Gets the includePassengerJourneySSRs value for this GetBookingFilter.
     * 
     * @return includePassengerJourneySSRs
     */
    public java.lang.Boolean getIncludePassengerJourneySSRs() {
        return includePassengerJourneySSRs;
    }


    /**
     * Sets the includePassengerJourneySSRs value for this GetBookingFilter.
     * 
     * @param includePassengerJourneySSRs
     */
    public void setIncludePassengerJourneySSRs(java.lang.Boolean includePassengerJourneySSRs) {
        this.includePassengerJourneySSRs = includePassengerJourneySSRs;
    }


    /**
     * Gets the includeBaggage value for this GetBookingFilter.
     * 
     * @return includeBaggage
     */
    public java.lang.Boolean getIncludeBaggage() {
        return includeBaggage;
    }


    /**
     * Sets the includeBaggage value for this GetBookingFilter.
     * 
     * @param includeBaggage
     */
    public void setIncludeBaggage(java.lang.Boolean includeBaggage) {
        this.includeBaggage = includeBaggage;
    }


    /**
     * Gets the includeJourneys value for this GetBookingFilter.
     * 
     * @return includeJourneys
     */
    public java.lang.Boolean getIncludeJourneys() {
        return includeJourneys;
    }


    /**
     * Sets the includeJourneys value for this GetBookingFilter.
     * 
     * @param includeJourneys
     */
    public void setIncludeJourneys(java.lang.Boolean includeJourneys) {
        this.includeJourneys = includeJourneys;
    }


    /**
     * Gets the includeFares value for this GetBookingFilter.
     * 
     * @return includeFares
     */
    public java.lang.Boolean getIncludeFares() {
        return includeFares;
    }


    /**
     * Sets the includeFares value for this GetBookingFilter.
     * 
     * @param includeFares
     */
    public void setIncludeFares(java.lang.Boolean includeFares) {
        this.includeFares = includeFares;
    }


    /**
     * Gets the includePaxSeatInfo value for this GetBookingFilter.
     * 
     * @return includePaxSeatInfo
     */
    public java.lang.Boolean getIncludePaxSeatInfo() {
        return includePaxSeatInfo;
    }


    /**
     * Sets the includePaxSeatInfo value for this GetBookingFilter.
     * 
     * @param includePaxSeatInfo
     */
    public void setIncludePaxSeatInfo(java.lang.Boolean includePaxSeatInfo) {
        this.includePaxSeatInfo = includePaxSeatInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBookingFilter)) return false;
        GetBookingFilter other = (GetBookingFilter) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.includeBookingComponents==null && other.getIncludeBookingComponents()==null) || 
             (this.includeBookingComponents!=null &&
              this.includeBookingComponents.equals(other.getIncludeBookingComponents()))) &&
            ((this.includeThirdPartyRecordLocators==null && other.getIncludeThirdPartyRecordLocators()==null) || 
             (this.includeThirdPartyRecordLocators!=null &&
              this.includeThirdPartyRecordLocators.equals(other.getIncludeThirdPartyRecordLocators()))) &&
            ((this.includePayments==null && other.getIncludePayments()==null) || 
             (this.includePayments!=null &&
              this.includePayments.equals(other.getIncludePayments()))) &&
            ((this.includeContacts==null && other.getIncludeContacts()==null) || 
             (this.includeContacts!=null &&
              this.includeContacts.equals(other.getIncludeContacts()))) &&
            ((this.includeBookingComments==null && other.getIncludeBookingComments()==null) || 
             (this.includeBookingComments!=null &&
              this.includeBookingComments.equals(other.getIncludeBookingComments()))) &&
            ((this.includeBookingQueues==null && other.getIncludeBookingQueues()==null) || 
             (this.includeBookingQueues!=null &&
              this.includeBookingQueues.equals(other.getIncludeBookingQueues()))) &&
            ((this.includePassengers==null && other.getIncludePassengers()==null) || 
             (this.includePassengers!=null &&
              this.includePassengers.equals(other.getIncludePassengers()))) &&
            ((this.includePassengerAddresses==null && other.getIncludePassengerAddresses()==null) || 
             (this.includePassengerAddresses!=null &&
              this.includePassengerAddresses.equals(other.getIncludePassengerAddresses()))) &&
            ((this.includePassengerTravelDocs==null && other.getIncludePassengerTravelDocs()==null) || 
             (this.includePassengerTravelDocs!=null &&
              this.includePassengerTravelDocs.equals(other.getIncludePassengerTravelDocs()))) &&
            ((this.includePassengerFees==null && other.getIncludePassengerFees()==null) || 
             (this.includePassengerFees!=null &&
              this.includePassengerFees.equals(other.getIncludePassengerFees()))) &&
            ((this.includePassengerJourneySSRs==null && other.getIncludePassengerJourneySSRs()==null) || 
             (this.includePassengerJourneySSRs!=null &&
              this.includePassengerJourneySSRs.equals(other.getIncludePassengerJourneySSRs()))) &&
            ((this.includeBaggage==null && other.getIncludeBaggage()==null) || 
             (this.includeBaggage!=null &&
              this.includeBaggage.equals(other.getIncludeBaggage()))) &&
            ((this.includeJourneys==null && other.getIncludeJourneys()==null) || 
             (this.includeJourneys!=null &&
              this.includeJourneys.equals(other.getIncludeJourneys()))) &&
            ((this.includeFares==null && other.getIncludeFares()==null) || 
             (this.includeFares!=null &&
              this.includeFares.equals(other.getIncludeFares()))) &&
            ((this.includePaxSeatInfo==null && other.getIncludePaxSeatInfo()==null) || 
             (this.includePaxSeatInfo!=null &&
              this.includePaxSeatInfo.equals(other.getIncludePaxSeatInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIncludeBookingComponents() != null) {
            _hashCode += getIncludeBookingComponents().hashCode();
        }
        if (getIncludeThirdPartyRecordLocators() != null) {
            _hashCode += getIncludeThirdPartyRecordLocators().hashCode();
        }
        if (getIncludePayments() != null) {
            _hashCode += getIncludePayments().hashCode();
        }
        if (getIncludeContacts() != null) {
            _hashCode += getIncludeContacts().hashCode();
        }
        if (getIncludeBookingComments() != null) {
            _hashCode += getIncludeBookingComments().hashCode();
        }
        if (getIncludeBookingQueues() != null) {
            _hashCode += getIncludeBookingQueues().hashCode();
        }
        if (getIncludePassengers() != null) {
            _hashCode += getIncludePassengers().hashCode();
        }
        if (getIncludePassengerAddresses() != null) {
            _hashCode += getIncludePassengerAddresses().hashCode();
        }
        if (getIncludePassengerTravelDocs() != null) {
            _hashCode += getIncludePassengerTravelDocs().hashCode();
        }
        if (getIncludePassengerFees() != null) {
            _hashCode += getIncludePassengerFees().hashCode();
        }
        if (getIncludePassengerJourneySSRs() != null) {
            _hashCode += getIncludePassengerJourneySSRs().hashCode();
        }
        if (getIncludeBaggage() != null) {
            _hashCode += getIncludeBaggage().hashCode();
        }
        if (getIncludeJourneys() != null) {
            _hashCode += getIncludeJourneys().hashCode();
        }
        if (getIncludeFares() != null) {
            _hashCode += getIncludeFares().hashCode();
        }
        if (getIncludePaxSeatInfo() != null) {
            _hashCode += getIncludePaxSeatInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBookingFilter.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFilter"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeBookingComponents");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeBookingComponents"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeThirdPartyRecordLocators");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeThirdPartyRecordLocators"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePayments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludePayments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeContacts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeContacts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeBookingComments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeBookingComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeBookingQueues");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeBookingQueues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePassengers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludePassengers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePassengerAddresses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludePassengerAddresses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePassengerTravelDocs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludePassengerTravelDocs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePassengerFees");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludePassengerFees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePassengerJourneySSRs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludePassengerJourneySSRs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeBaggage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeBaggage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeJourneys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeJourneys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeFares");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeFares"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePaxSeatInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludePaxSeatInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
