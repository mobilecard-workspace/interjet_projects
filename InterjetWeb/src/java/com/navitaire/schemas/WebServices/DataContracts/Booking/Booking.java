/**
 * Booking.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class Booking  extends com.navitaire.schemas.WebServices.DataContracts.Booking.ServiceMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state;

    private java.lang.String recordLocator;

    private java.lang.String currencyCode;

    private java.lang.Short paxCount;

    private java.lang.String systemCode;

    private java.lang.Long bookingID;

    private java.lang.Long bookingParentID;

    private java.lang.String parentRecordLocator;

    private java.lang.String bookingChangeCode;

    private java.lang.String groupName;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingInfo bookingInfo;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale POS;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHold bookingHold;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingSum bookingSum;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.ReceivedByInfo receivedBy;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] recordLocators;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] journeys;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment[] bookingComments;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingQueueInfo[] bookingQueueInfos;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] bookingContacts;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Payment[] payments;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent[] bookingComponents;

    private java.lang.String numericRecordLocator;

    public Booking() {
    }

    public Booking(
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInfoList,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String recordLocator,
           java.lang.String currencyCode,
           java.lang.Short paxCount,
           java.lang.String systemCode,
           java.lang.Long bookingID,
           java.lang.Long bookingParentID,
           java.lang.String parentRecordLocator,
           java.lang.String bookingChangeCode,
           java.lang.String groupName,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingInfo bookingInfo,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale POS,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS,
           com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHold bookingHold,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingSum bookingSum,
           com.navitaire.schemas.WebServices.DataContracts.Booking.ReceivedByInfo receivedBy,
           com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] recordLocators,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] journeys,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment[] bookingComments,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingQueueInfo[] bookingQueueInfos,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] bookingContacts,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Payment[] payments,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent[] bookingComponents,
           java.lang.String numericRecordLocator) {
        super(
            otherServiceInfoList);
        this.state = state;
        this.recordLocator = recordLocator;
        this.currencyCode = currencyCode;
        this.paxCount = paxCount;
        this.systemCode = systemCode;
        this.bookingID = bookingID;
        this.bookingParentID = bookingParentID;
        this.parentRecordLocator = parentRecordLocator;
        this.bookingChangeCode = bookingChangeCode;
        this.groupName = groupName;
        this.bookingInfo = bookingInfo;
        this.POS = POS;
        this.sourcePOS = sourcePOS;
        this.typeOfSale = typeOfSale;
        this.bookingHold = bookingHold;
        this.bookingSum = bookingSum;
        this.receivedBy = receivedBy;
        this.recordLocators = recordLocators;
        this.passengers = passengers;
        this.journeys = journeys;
        this.bookingComments = bookingComments;
        this.bookingQueueInfos = bookingQueueInfos;
        this.bookingContacts = bookingContacts;
        this.payments = payments;
        this.bookingComponents = bookingComponents;
        this.numericRecordLocator = numericRecordLocator;
    }


    /**
     * Gets the state value for this Booking.
     * 
     * @return state
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState getState() {
        return state;
    }


    /**
     * Sets the state value for this Booking.
     * 
     * @param state
     */
    public void setState(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state) {
        this.state = state;
    }


    /**
     * Gets the recordLocator value for this Booking.
     * 
     * @return recordLocator
     */
    public java.lang.String getRecordLocator() {
        return recordLocator;
    }


    /**
     * Sets the recordLocator value for this Booking.
     * 
     * @param recordLocator
     */
    public void setRecordLocator(java.lang.String recordLocator) {
        this.recordLocator = recordLocator;
    }


    /**
     * Gets the currencyCode value for this Booking.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this Booking.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the paxCount value for this Booking.
     * 
     * @return paxCount
     */
    public java.lang.Short getPaxCount() {
        return paxCount;
    }


    /**
     * Sets the paxCount value for this Booking.
     * 
     * @param paxCount
     */
    public void setPaxCount(java.lang.Short paxCount) {
        this.paxCount = paxCount;
    }


    /**
     * Gets the systemCode value for this Booking.
     * 
     * @return systemCode
     */
    public java.lang.String getSystemCode() {
        return systemCode;
    }


    /**
     * Sets the systemCode value for this Booking.
     * 
     * @param systemCode
     */
    public void setSystemCode(java.lang.String systemCode) {
        this.systemCode = systemCode;
    }


    /**
     * Gets the bookingID value for this Booking.
     * 
     * @return bookingID
     */
    public java.lang.Long getBookingID() {
        return bookingID;
    }


    /**
     * Sets the bookingID value for this Booking.
     * 
     * @param bookingID
     */
    public void setBookingID(java.lang.Long bookingID) {
        this.bookingID = bookingID;
    }


    /**
     * Gets the bookingParentID value for this Booking.
     * 
     * @return bookingParentID
     */
    public java.lang.Long getBookingParentID() {
        return bookingParentID;
    }


    /**
     * Sets the bookingParentID value for this Booking.
     * 
     * @param bookingParentID
     */
    public void setBookingParentID(java.lang.Long bookingParentID) {
        this.bookingParentID = bookingParentID;
    }


    /**
     * Gets the parentRecordLocator value for this Booking.
     * 
     * @return parentRecordLocator
     */
    public java.lang.String getParentRecordLocator() {
        return parentRecordLocator;
    }


    /**
     * Sets the parentRecordLocator value for this Booking.
     * 
     * @param parentRecordLocator
     */
    public void setParentRecordLocator(java.lang.String parentRecordLocator) {
        this.parentRecordLocator = parentRecordLocator;
    }


    /**
     * Gets the bookingChangeCode value for this Booking.
     * 
     * @return bookingChangeCode
     */
    public java.lang.String getBookingChangeCode() {
        return bookingChangeCode;
    }


    /**
     * Sets the bookingChangeCode value for this Booking.
     * 
     * @param bookingChangeCode
     */
    public void setBookingChangeCode(java.lang.String bookingChangeCode) {
        this.bookingChangeCode = bookingChangeCode;
    }


    /**
     * Gets the groupName value for this Booking.
     * 
     * @return groupName
     */
    public java.lang.String getGroupName() {
        return groupName;
    }


    /**
     * Sets the groupName value for this Booking.
     * 
     * @param groupName
     */
    public void setGroupName(java.lang.String groupName) {
        this.groupName = groupName;
    }


    /**
     * Gets the bookingInfo value for this Booking.
     * 
     * @return bookingInfo
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingInfo getBookingInfo() {
        return bookingInfo;
    }


    /**
     * Sets the bookingInfo value for this Booking.
     * 
     * @param bookingInfo
     */
    public void setBookingInfo(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingInfo bookingInfo) {
        this.bookingInfo = bookingInfo;
    }


    /**
     * Gets the POS value for this Booking.
     * 
     * @return POS
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getPOS() {
        return POS;
    }


    /**
     * Sets the POS value for this Booking.
     * 
     * @param POS
     */
    public void setPOS(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale POS) {
        this.POS = POS;
    }


    /**
     * Gets the sourcePOS value for this Booking.
     * 
     * @return sourcePOS
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePOS() {
        return sourcePOS;
    }


    /**
     * Sets the sourcePOS value for this Booking.
     * 
     * @param sourcePOS
     */
    public void setSourcePOS(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS) {
        this.sourcePOS = sourcePOS;
    }


    /**
     * Gets the typeOfSale value for this Booking.
     * 
     * @return typeOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale getTypeOfSale() {
        return typeOfSale;
    }


    /**
     * Sets the typeOfSale value for this Booking.
     * 
     * @param typeOfSale
     */
    public void setTypeOfSale(com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale) {
        this.typeOfSale = typeOfSale;
    }


    /**
     * Gets the bookingHold value for this Booking.
     * 
     * @return bookingHold
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHold getBookingHold() {
        return bookingHold;
    }


    /**
     * Sets the bookingHold value for this Booking.
     * 
     * @param bookingHold
     */
    public void setBookingHold(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHold bookingHold) {
        this.bookingHold = bookingHold;
    }


    /**
     * Gets the bookingSum value for this Booking.
     * 
     * @return bookingSum
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingSum getBookingSum() {
        return bookingSum;
    }


    /**
     * Sets the bookingSum value for this Booking.
     * 
     * @param bookingSum
     */
    public void setBookingSum(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingSum bookingSum) {
        this.bookingSum = bookingSum;
    }


    /**
     * Gets the receivedBy value for this Booking.
     * 
     * @return receivedBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.ReceivedByInfo getReceivedBy() {
        return receivedBy;
    }


    /**
     * Sets the receivedBy value for this Booking.
     * 
     * @param receivedBy
     */
    public void setReceivedBy(com.navitaire.schemas.WebServices.DataContracts.Booking.ReceivedByInfo receivedBy) {
        this.receivedBy = receivedBy;
    }


    /**
     * Gets the recordLocators value for this Booking.
     * 
     * @return recordLocators
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] getRecordLocators() {
        return recordLocators;
    }


    /**
     * Sets the recordLocators value for this Booking.
     * 
     * @param recordLocators
     */
    public void setRecordLocators(com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] recordLocators) {
        this.recordLocators = recordLocators;
    }


    /**
     * Gets the passengers value for this Booking.
     * 
     * @return passengers
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] getPassengers() {
        return passengers;
    }


    /**
     * Sets the passengers value for this Booking.
     * 
     * @param passengers
     */
    public void setPassengers(com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers) {
        this.passengers = passengers;
    }


    /**
     * Gets the journeys value for this Booking.
     * 
     * @return journeys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] getJourneys() {
        return journeys;
    }


    /**
     * Sets the journeys value for this Booking.
     * 
     * @param journeys
     */
    public void setJourneys(com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] journeys) {
        this.journeys = journeys;
    }


    /**
     * Gets the bookingComments value for this Booking.
     * 
     * @return bookingComments
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment[] getBookingComments() {
        return bookingComments;
    }


    /**
     * Sets the bookingComments value for this Booking.
     * 
     * @param bookingComments
     */
    public void setBookingComments(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment[] bookingComments) {
        this.bookingComments = bookingComments;
    }


    /**
     * Gets the bookingQueueInfos value for this Booking.
     * 
     * @return bookingQueueInfos
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingQueueInfo[] getBookingQueueInfos() {
        return bookingQueueInfos;
    }


    /**
     * Sets the bookingQueueInfos value for this Booking.
     * 
     * @param bookingQueueInfos
     */
    public void setBookingQueueInfos(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingQueueInfo[] bookingQueueInfos) {
        this.bookingQueueInfos = bookingQueueInfos;
    }


    /**
     * Gets the bookingContacts value for this Booking.
     * 
     * @return bookingContacts
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] getBookingContacts() {
        return bookingContacts;
    }


    /**
     * Sets the bookingContacts value for this Booking.
     * 
     * @param bookingContacts
     */
    public void setBookingContacts(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] bookingContacts) {
        this.bookingContacts = bookingContacts;
    }


    /**
     * Gets the payments value for this Booking.
     * 
     * @return payments
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Payment[] getPayments() {
        return payments;
    }


    /**
     * Sets the payments value for this Booking.
     * 
     * @param payments
     */
    public void setPayments(com.navitaire.schemas.WebServices.DataContracts.Booking.Payment[] payments) {
        this.payments = payments;
    }


    /**
     * Gets the bookingComponents value for this Booking.
     * 
     * @return bookingComponents
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent[] getBookingComponents() {
        return bookingComponents;
    }


    /**
     * Sets the bookingComponents value for this Booking.
     * 
     * @param bookingComponents
     */
    public void setBookingComponents(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent[] bookingComponents) {
        this.bookingComponents = bookingComponents;
    }


    /**
     * Gets the numericRecordLocator value for this Booking.
     * 
     * @return numericRecordLocator
     */
    public java.lang.String getNumericRecordLocator() {
        return numericRecordLocator;
    }


    /**
     * Sets the numericRecordLocator value for this Booking.
     * 
     * @param numericRecordLocator
     */
    public void setNumericRecordLocator(java.lang.String numericRecordLocator) {
        this.numericRecordLocator = numericRecordLocator;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Booking)) return false;
        Booking other = (Booking) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.recordLocator==null && other.getRecordLocator()==null) || 
             (this.recordLocator!=null &&
              this.recordLocator.equals(other.getRecordLocator()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.paxCount==null && other.getPaxCount()==null) || 
             (this.paxCount!=null &&
              this.paxCount.equals(other.getPaxCount()))) &&
            ((this.systemCode==null && other.getSystemCode()==null) || 
             (this.systemCode!=null &&
              this.systemCode.equals(other.getSystemCode()))) &&
            ((this.bookingID==null && other.getBookingID()==null) || 
             (this.bookingID!=null &&
              this.bookingID.equals(other.getBookingID()))) &&
            ((this.bookingParentID==null && other.getBookingParentID()==null) || 
             (this.bookingParentID!=null &&
              this.bookingParentID.equals(other.getBookingParentID()))) &&
            ((this.parentRecordLocator==null && other.getParentRecordLocator()==null) || 
             (this.parentRecordLocator!=null &&
              this.parentRecordLocator.equals(other.getParentRecordLocator()))) &&
            ((this.bookingChangeCode==null && other.getBookingChangeCode()==null) || 
             (this.bookingChangeCode!=null &&
              this.bookingChangeCode.equals(other.getBookingChangeCode()))) &&
            ((this.groupName==null && other.getGroupName()==null) || 
             (this.groupName!=null &&
              this.groupName.equals(other.getGroupName()))) &&
            ((this.bookingInfo==null && other.getBookingInfo()==null) || 
             (this.bookingInfo!=null &&
              this.bookingInfo.equals(other.getBookingInfo()))) &&
            ((this.POS==null && other.getPOS()==null) || 
             (this.POS!=null &&
              this.POS.equals(other.getPOS()))) &&
            ((this.sourcePOS==null && other.getSourcePOS()==null) || 
             (this.sourcePOS!=null &&
              this.sourcePOS.equals(other.getSourcePOS()))) &&
            ((this.typeOfSale==null && other.getTypeOfSale()==null) || 
             (this.typeOfSale!=null &&
              this.typeOfSale.equals(other.getTypeOfSale()))) &&
            ((this.bookingHold==null && other.getBookingHold()==null) || 
             (this.bookingHold!=null &&
              this.bookingHold.equals(other.getBookingHold()))) &&
            ((this.bookingSum==null && other.getBookingSum()==null) || 
             (this.bookingSum!=null &&
              this.bookingSum.equals(other.getBookingSum()))) &&
            ((this.receivedBy==null && other.getReceivedBy()==null) || 
             (this.receivedBy!=null &&
              this.receivedBy.equals(other.getReceivedBy()))) &&
            ((this.recordLocators==null && other.getRecordLocators()==null) || 
             (this.recordLocators!=null &&
              java.util.Arrays.equals(this.recordLocators, other.getRecordLocators()))) &&
            ((this.passengers==null && other.getPassengers()==null) || 
             (this.passengers!=null &&
              java.util.Arrays.equals(this.passengers, other.getPassengers()))) &&
            ((this.journeys==null && other.getJourneys()==null) || 
             (this.journeys!=null &&
              java.util.Arrays.equals(this.journeys, other.getJourneys()))) &&
            ((this.bookingComments==null && other.getBookingComments()==null) || 
             (this.bookingComments!=null &&
              java.util.Arrays.equals(this.bookingComments, other.getBookingComments()))) &&
            ((this.bookingQueueInfos==null && other.getBookingQueueInfos()==null) || 
             (this.bookingQueueInfos!=null &&
              java.util.Arrays.equals(this.bookingQueueInfos, other.getBookingQueueInfos()))) &&
            ((this.bookingContacts==null && other.getBookingContacts()==null) || 
             (this.bookingContacts!=null &&
              java.util.Arrays.equals(this.bookingContacts, other.getBookingContacts()))) &&
            ((this.payments==null && other.getPayments()==null) || 
             (this.payments!=null &&
              java.util.Arrays.equals(this.payments, other.getPayments()))) &&
            ((this.bookingComponents==null && other.getBookingComponents()==null) || 
             (this.bookingComponents!=null &&
              java.util.Arrays.equals(this.bookingComponents, other.getBookingComponents()))) &&
            ((this.numericRecordLocator==null && other.getNumericRecordLocator()==null) || 
             (this.numericRecordLocator!=null &&
              this.numericRecordLocator.equals(other.getNumericRecordLocator())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getRecordLocator() != null) {
            _hashCode += getRecordLocator().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getPaxCount() != null) {
            _hashCode += getPaxCount().hashCode();
        }
        if (getSystemCode() != null) {
            _hashCode += getSystemCode().hashCode();
        }
        if (getBookingID() != null) {
            _hashCode += getBookingID().hashCode();
        }
        if (getBookingParentID() != null) {
            _hashCode += getBookingParentID().hashCode();
        }
        if (getParentRecordLocator() != null) {
            _hashCode += getParentRecordLocator().hashCode();
        }
        if (getBookingChangeCode() != null) {
            _hashCode += getBookingChangeCode().hashCode();
        }
        if (getGroupName() != null) {
            _hashCode += getGroupName().hashCode();
        }
        if (getBookingInfo() != null) {
            _hashCode += getBookingInfo().hashCode();
        }
        if (getPOS() != null) {
            _hashCode += getPOS().hashCode();
        }
        if (getSourcePOS() != null) {
            _hashCode += getSourcePOS().hashCode();
        }
        if (getTypeOfSale() != null) {
            _hashCode += getTypeOfSale().hashCode();
        }
        if (getBookingHold() != null) {
            _hashCode += getBookingHold().hashCode();
        }
        if (getBookingSum() != null) {
            _hashCode += getBookingSum().hashCode();
        }
        if (getReceivedBy() != null) {
            _hashCode += getReceivedBy().hashCode();
        }
        if (getRecordLocators() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRecordLocators());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRecordLocators(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getJourneys() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJourneys());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJourneys(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBookingComments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingComments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingComments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBookingQueueInfos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingQueueInfos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingQueueInfos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBookingContacts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingContacts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingContacts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPayments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPayments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPayments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBookingComponents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingComponents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingComponents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNumericRecordLocator() != null) {
            _hashCode += getNumericRecordLocator().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Booking.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Booking"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "MessageState"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SystemCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingParentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingParentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ParentRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingChangeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingChangeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GroupName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "POS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePOS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingHold");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHold"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHold"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingSum");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingSum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingSum"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receivedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedByInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocators");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocators"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passengers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journeys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingComments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingQueueInfos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingQueueInfos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingQueueInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingQueueInfo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingContacts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContacts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContact"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContact"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingComponents");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponents"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponent"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponent"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numericRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NumericRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
