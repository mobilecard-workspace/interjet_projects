/**
 * MCCRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class MCCRequest  implements java.io.Serializable {
    private java.lang.Boolean MCCInUse;

    private java.lang.String collectedCurrencyCode;

    private java.math.BigDecimal collectedAmount;

    public MCCRequest() {
    }

    public MCCRequest(
           java.lang.Boolean MCCInUse,
           java.lang.String collectedCurrencyCode,
           java.math.BigDecimal collectedAmount) {
           this.MCCInUse = MCCInUse;
           this.collectedCurrencyCode = collectedCurrencyCode;
           this.collectedAmount = collectedAmount;
    }


    /**
     * Gets the MCCInUse value for this MCCRequest.
     * 
     * @return MCCInUse
     */
    public java.lang.Boolean getMCCInUse() {
        return MCCInUse;
    }


    /**
     * Sets the MCCInUse value for this MCCRequest.
     * 
     * @param MCCInUse
     */
    public void setMCCInUse(java.lang.Boolean MCCInUse) {
        this.MCCInUse = MCCInUse;
    }


    /**
     * Gets the collectedCurrencyCode value for this MCCRequest.
     * 
     * @return collectedCurrencyCode
     */
    public java.lang.String getCollectedCurrencyCode() {
        return collectedCurrencyCode;
    }


    /**
     * Sets the collectedCurrencyCode value for this MCCRequest.
     * 
     * @param collectedCurrencyCode
     */
    public void setCollectedCurrencyCode(java.lang.String collectedCurrencyCode) {
        this.collectedCurrencyCode = collectedCurrencyCode;
    }


    /**
     * Gets the collectedAmount value for this MCCRequest.
     * 
     * @return collectedAmount
     */
    public java.math.BigDecimal getCollectedAmount() {
        return collectedAmount;
    }


    /**
     * Sets the collectedAmount value for this MCCRequest.
     * 
     * @param collectedAmount
     */
    public void setCollectedAmount(java.math.BigDecimal collectedAmount) {
        this.collectedAmount = collectedAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MCCRequest)) return false;
        MCCRequest other = (MCCRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MCCInUse==null && other.getMCCInUse()==null) || 
             (this.MCCInUse!=null &&
              this.MCCInUse.equals(other.getMCCInUse()))) &&
            ((this.collectedCurrencyCode==null && other.getCollectedCurrencyCode()==null) || 
             (this.collectedCurrencyCode!=null &&
              this.collectedCurrencyCode.equals(other.getCollectedCurrencyCode()))) &&
            ((this.collectedAmount==null && other.getCollectedAmount()==null) || 
             (this.collectedAmount!=null &&
              this.collectedAmount.equals(other.getCollectedAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMCCInUse() != null) {
            _hashCode += getMCCInUse().hashCode();
        }
        if (getCollectedCurrencyCode() != null) {
            _hashCode += getCollectedCurrencyCode().hashCode();
        }
        if (getCollectedAmount() != null) {
            _hashCode += getCollectedAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MCCRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MCCRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MCCInUse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MCCInUse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
