/**
 * SellRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SellRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellBy sellBy;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequest sellJourneyByKeyRequest;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequest sellJourneyRequest;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellSSR sellSSR;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellFee sellFee;

    public SellRequestData() {
    }

    public SellRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellBy sellBy,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequest sellJourneyByKeyRequest,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequest sellJourneyRequest,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellSSR sellSSR,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellFee sellFee) {
           this.sellBy = sellBy;
           this.sellJourneyByKeyRequest = sellJourneyByKeyRequest;
           this.sellJourneyRequest = sellJourneyRequest;
           this.sellSSR = sellSSR;
           this.sellFee = sellFee;
    }


    /**
     * Gets the sellBy value for this SellRequestData.
     * 
     * @return sellBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellBy getSellBy() {
        return sellBy;
    }


    /**
     * Sets the sellBy value for this SellRequestData.
     * 
     * @param sellBy
     */
    public void setSellBy(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellBy sellBy) {
        this.sellBy = sellBy;
    }


    /**
     * Gets the sellJourneyByKeyRequest value for this SellRequestData.
     * 
     * @return sellJourneyByKeyRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequest getSellJourneyByKeyRequest() {
        return sellJourneyByKeyRequest;
    }


    /**
     * Sets the sellJourneyByKeyRequest value for this SellRequestData.
     * 
     * @param sellJourneyByKeyRequest
     */
    public void setSellJourneyByKeyRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequest sellJourneyByKeyRequest) {
        this.sellJourneyByKeyRequest = sellJourneyByKeyRequest;
    }


    /**
     * Gets the sellJourneyRequest value for this SellRequestData.
     * 
     * @return sellJourneyRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequest getSellJourneyRequest() {
        return sellJourneyRequest;
    }


    /**
     * Sets the sellJourneyRequest value for this SellRequestData.
     * 
     * @param sellJourneyRequest
     */
    public void setSellJourneyRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequest sellJourneyRequest) {
        this.sellJourneyRequest = sellJourneyRequest;
    }


    /**
     * Gets the sellSSR value for this SellRequestData.
     * 
     * @return sellSSR
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellSSR getSellSSR() {
        return sellSSR;
    }


    /**
     * Sets the sellSSR value for this SellRequestData.
     * 
     * @param sellSSR
     */
    public void setSellSSR(com.navitaire.schemas.WebServices.DataContracts.Booking.SellSSR sellSSR) {
        this.sellSSR = sellSSR;
    }


    /**
     * Gets the sellFee value for this SellRequestData.
     * 
     * @return sellFee
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellFee getSellFee() {
        return sellFee;
    }


    /**
     * Sets the sellFee value for this SellRequestData.
     * 
     * @param sellFee
     */
    public void setSellFee(com.navitaire.schemas.WebServices.DataContracts.Booking.SellFee sellFee) {
        this.sellFee = sellFee;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SellRequestData)) return false;
        SellRequestData other = (SellRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sellBy==null && other.getSellBy()==null) || 
             (this.sellBy!=null &&
              this.sellBy.equals(other.getSellBy()))) &&
            ((this.sellJourneyByKeyRequest==null && other.getSellJourneyByKeyRequest()==null) || 
             (this.sellJourneyByKeyRequest!=null &&
              this.sellJourneyByKeyRequest.equals(other.getSellJourneyByKeyRequest()))) &&
            ((this.sellJourneyRequest==null && other.getSellJourneyRequest()==null) || 
             (this.sellJourneyRequest!=null &&
              this.sellJourneyRequest.equals(other.getSellJourneyRequest()))) &&
            ((this.sellSSR==null && other.getSellSSR()==null) || 
             (this.sellSSR!=null &&
              this.sellSSR.equals(other.getSellSSR()))) &&
            ((this.sellFee==null && other.getSellFee()==null) || 
             (this.sellFee!=null &&
              this.sellFee.equals(other.getSellFee())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSellBy() != null) {
            _hashCode += getSellBy().hashCode();
        }
        if (getSellJourneyByKeyRequest() != null) {
            _hashCode += getSellJourneyByKeyRequest().hashCode();
        }
        if (getSellJourneyRequest() != null) {
            _hashCode += getSellJourneyRequest().hashCode();
        }
        if (getSellSSR() != null) {
            _hashCode += getSellSSR().hashCode();
        }
        if (getSellFee() != null) {
            _hashCode += getSellFee().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SellRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SellBy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellJourneyByKeyRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyByKeyRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyByKeyRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellJourneyRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellSSR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellSSR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellSSR"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellFee"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
