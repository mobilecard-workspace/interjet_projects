/**
 * Filter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class Filter  implements java.io.Serializable {
    private java.util.Calendar departureDate;

    private java.lang.String flightOrigin;

    private java.lang.String flightDestination;

    private java.lang.String flightNumber;

    private java.lang.String sourceOrganization;

    private java.lang.String organizationGroupCode;

    public Filter() {
    }

    public Filter(
           java.util.Calendar departureDate,
           java.lang.String flightOrigin,
           java.lang.String flightDestination,
           java.lang.String flightNumber,
           java.lang.String sourceOrganization,
           java.lang.String organizationGroupCode) {
           this.departureDate = departureDate;
           this.flightOrigin = flightOrigin;
           this.flightDestination = flightDestination;
           this.flightNumber = flightNumber;
           this.sourceOrganization = sourceOrganization;
           this.organizationGroupCode = organizationGroupCode;
    }


    /**
     * Gets the departureDate value for this Filter.
     * 
     * @return departureDate
     */
    public java.util.Calendar getDepartureDate() {
        return departureDate;
    }


    /**
     * Sets the departureDate value for this Filter.
     * 
     * @param departureDate
     */
    public void setDepartureDate(java.util.Calendar departureDate) {
        this.departureDate = departureDate;
    }


    /**
     * Gets the flightOrigin value for this Filter.
     * 
     * @return flightOrigin
     */
    public java.lang.String getFlightOrigin() {
        return flightOrigin;
    }


    /**
     * Sets the flightOrigin value for this Filter.
     * 
     * @param flightOrigin
     */
    public void setFlightOrigin(java.lang.String flightOrigin) {
        this.flightOrigin = flightOrigin;
    }


    /**
     * Gets the flightDestination value for this Filter.
     * 
     * @return flightDestination
     */
    public java.lang.String getFlightDestination() {
        return flightDestination;
    }


    /**
     * Sets the flightDestination value for this Filter.
     * 
     * @param flightDestination
     */
    public void setFlightDestination(java.lang.String flightDestination) {
        this.flightDestination = flightDestination;
    }


    /**
     * Gets the flightNumber value for this Filter.
     * 
     * @return flightNumber
     */
    public java.lang.String getFlightNumber() {
        return flightNumber;
    }


    /**
     * Sets the flightNumber value for this Filter.
     * 
     * @param flightNumber
     */
    public void setFlightNumber(java.lang.String flightNumber) {
        this.flightNumber = flightNumber;
    }


    /**
     * Gets the sourceOrganization value for this Filter.
     * 
     * @return sourceOrganization
     */
    public java.lang.String getSourceOrganization() {
        return sourceOrganization;
    }


    /**
     * Sets the sourceOrganization value for this Filter.
     * 
     * @param sourceOrganization
     */
    public void setSourceOrganization(java.lang.String sourceOrganization) {
        this.sourceOrganization = sourceOrganization;
    }


    /**
     * Gets the organizationGroupCode value for this Filter.
     * 
     * @return organizationGroupCode
     */
    public java.lang.String getOrganizationGroupCode() {
        return organizationGroupCode;
    }


    /**
     * Sets the organizationGroupCode value for this Filter.
     * 
     * @param organizationGroupCode
     */
    public void setOrganizationGroupCode(java.lang.String organizationGroupCode) {
        this.organizationGroupCode = organizationGroupCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Filter)) return false;
        Filter other = (Filter) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departureDate==null && other.getDepartureDate()==null) || 
             (this.departureDate!=null &&
              this.departureDate.equals(other.getDepartureDate()))) &&
            ((this.flightOrigin==null && other.getFlightOrigin()==null) || 
             (this.flightOrigin!=null &&
              this.flightOrigin.equals(other.getFlightOrigin()))) &&
            ((this.flightDestination==null && other.getFlightDestination()==null) || 
             (this.flightDestination!=null &&
              this.flightDestination.equals(other.getFlightDestination()))) &&
            ((this.flightNumber==null && other.getFlightNumber()==null) || 
             (this.flightNumber!=null &&
              this.flightNumber.equals(other.getFlightNumber()))) &&
            ((this.sourceOrganization==null && other.getSourceOrganization()==null) || 
             (this.sourceOrganization!=null &&
              this.sourceOrganization.equals(other.getSourceOrganization()))) &&
            ((this.organizationGroupCode==null && other.getOrganizationGroupCode()==null) || 
             (this.organizationGroupCode!=null &&
              this.organizationGroupCode.equals(other.getOrganizationGroupCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartureDate() != null) {
            _hashCode += getDepartureDate().hashCode();
        }
        if (getFlightOrigin() != null) {
            _hashCode += getFlightOrigin().hashCode();
        }
        if (getFlightDestination() != null) {
            _hashCode += getFlightDestination().hashCode();
        }
        if (getFlightNumber() != null) {
            _hashCode += getFlightNumber().hashCode();
        }
        if (getSourceOrganization() != null) {
            _hashCode += getSourceOrganization().hashCode();
        }
        if (getOrganizationGroupCode() != null) {
            _hashCode += getOrganizationGroupCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Filter.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Filter"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightOrigin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightOrigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightDestination");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightDestination"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceOrganization");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceOrganization"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organizationGroupCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OrganizationGroupCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
