/**
 * ThreeDSecure.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class ThreeDSecure  implements java.io.Serializable {
    private java.lang.String browserUserAgent;

    private java.lang.String browserAccept;

    private java.lang.String remoteIpAddress;

    private java.lang.String termUrl;

    private java.lang.String proxyVia;

    private java.lang.Boolean validationTDSApplicable;

    private java.lang.String validationTDSPaReq;

    private java.lang.String validationTDSAcsUrl;

    private java.lang.String validationTDSPaRes;

    private java.lang.Boolean validationTDSSuccessful;

    private java.lang.String validationTDSAuthResult;

    private java.lang.String validationTDSCavv;

    private java.lang.String validationTDSCavvAlgorithm;

    private java.lang.String validationTDSEci;

    private java.lang.String validationTDSXid;

    public ThreeDSecure() {
    }

    public ThreeDSecure(
           java.lang.String browserUserAgent,
           java.lang.String browserAccept,
           java.lang.String remoteIpAddress,
           java.lang.String termUrl,
           java.lang.String proxyVia,
           java.lang.Boolean validationTDSApplicable,
           java.lang.String validationTDSPaReq,
           java.lang.String validationTDSAcsUrl,
           java.lang.String validationTDSPaRes,
           java.lang.Boolean validationTDSSuccessful,
           java.lang.String validationTDSAuthResult,
           java.lang.String validationTDSCavv,
           java.lang.String validationTDSCavvAlgorithm,
           java.lang.String validationTDSEci,
           java.lang.String validationTDSXid) {
           this.browserUserAgent = browserUserAgent;
           this.browserAccept = browserAccept;
           this.remoteIpAddress = remoteIpAddress;
           this.termUrl = termUrl;
           this.proxyVia = proxyVia;
           this.validationTDSApplicable = validationTDSApplicable;
           this.validationTDSPaReq = validationTDSPaReq;
           this.validationTDSAcsUrl = validationTDSAcsUrl;
           this.validationTDSPaRes = validationTDSPaRes;
           this.validationTDSSuccessful = validationTDSSuccessful;
           this.validationTDSAuthResult = validationTDSAuthResult;
           this.validationTDSCavv = validationTDSCavv;
           this.validationTDSCavvAlgorithm = validationTDSCavvAlgorithm;
           this.validationTDSEci = validationTDSEci;
           this.validationTDSXid = validationTDSXid;
    }


    /**
     * Gets the browserUserAgent value for this ThreeDSecure.
     * 
     * @return browserUserAgent
     */
    public java.lang.String getBrowserUserAgent() {
        return browserUserAgent;
    }


    /**
     * Sets the browserUserAgent value for this ThreeDSecure.
     * 
     * @param browserUserAgent
     */
    public void setBrowserUserAgent(java.lang.String browserUserAgent) {
        this.browserUserAgent = browserUserAgent;
    }


    /**
     * Gets the browserAccept value for this ThreeDSecure.
     * 
     * @return browserAccept
     */
    public java.lang.String getBrowserAccept() {
        return browserAccept;
    }


    /**
     * Sets the browserAccept value for this ThreeDSecure.
     * 
     * @param browserAccept
     */
    public void setBrowserAccept(java.lang.String browserAccept) {
        this.browserAccept = browserAccept;
    }


    /**
     * Gets the remoteIpAddress value for this ThreeDSecure.
     * 
     * @return remoteIpAddress
     */
    public java.lang.String getRemoteIpAddress() {
        return remoteIpAddress;
    }


    /**
     * Sets the remoteIpAddress value for this ThreeDSecure.
     * 
     * @param remoteIpAddress
     */
    public void setRemoteIpAddress(java.lang.String remoteIpAddress) {
        this.remoteIpAddress = remoteIpAddress;
    }


    /**
     * Gets the termUrl value for this ThreeDSecure.
     * 
     * @return termUrl
     */
    public java.lang.String getTermUrl() {
        return termUrl;
    }


    /**
     * Sets the termUrl value for this ThreeDSecure.
     * 
     * @param termUrl
     */
    public void setTermUrl(java.lang.String termUrl) {
        this.termUrl = termUrl;
    }


    /**
     * Gets the proxyVia value for this ThreeDSecure.
     * 
     * @return proxyVia
     */
    public java.lang.String getProxyVia() {
        return proxyVia;
    }


    /**
     * Sets the proxyVia value for this ThreeDSecure.
     * 
     * @param proxyVia
     */
    public void setProxyVia(java.lang.String proxyVia) {
        this.proxyVia = proxyVia;
    }


    /**
     * Gets the validationTDSApplicable value for this ThreeDSecure.
     * 
     * @return validationTDSApplicable
     */
    public java.lang.Boolean getValidationTDSApplicable() {
        return validationTDSApplicable;
    }


    /**
     * Sets the validationTDSApplicable value for this ThreeDSecure.
     * 
     * @param validationTDSApplicable
     */
    public void setValidationTDSApplicable(java.lang.Boolean validationTDSApplicable) {
        this.validationTDSApplicable = validationTDSApplicable;
    }


    /**
     * Gets the validationTDSPaReq value for this ThreeDSecure.
     * 
     * @return validationTDSPaReq
     */
    public java.lang.String getValidationTDSPaReq() {
        return validationTDSPaReq;
    }


    /**
     * Sets the validationTDSPaReq value for this ThreeDSecure.
     * 
     * @param validationTDSPaReq
     */
    public void setValidationTDSPaReq(java.lang.String validationTDSPaReq) {
        this.validationTDSPaReq = validationTDSPaReq;
    }


    /**
     * Gets the validationTDSAcsUrl value for this ThreeDSecure.
     * 
     * @return validationTDSAcsUrl
     */
    public java.lang.String getValidationTDSAcsUrl() {
        return validationTDSAcsUrl;
    }


    /**
     * Sets the validationTDSAcsUrl value for this ThreeDSecure.
     * 
     * @param validationTDSAcsUrl
     */
    public void setValidationTDSAcsUrl(java.lang.String validationTDSAcsUrl) {
        this.validationTDSAcsUrl = validationTDSAcsUrl;
    }


    /**
     * Gets the validationTDSPaRes value for this ThreeDSecure.
     * 
     * @return validationTDSPaRes
     */
    public java.lang.String getValidationTDSPaRes() {
        return validationTDSPaRes;
    }


    /**
     * Sets the validationTDSPaRes value for this ThreeDSecure.
     * 
     * @param validationTDSPaRes
     */
    public void setValidationTDSPaRes(java.lang.String validationTDSPaRes) {
        this.validationTDSPaRes = validationTDSPaRes;
    }


    /**
     * Gets the validationTDSSuccessful value for this ThreeDSecure.
     * 
     * @return validationTDSSuccessful
     */
    public java.lang.Boolean getValidationTDSSuccessful() {
        return validationTDSSuccessful;
    }


    /**
     * Sets the validationTDSSuccessful value for this ThreeDSecure.
     * 
     * @param validationTDSSuccessful
     */
    public void setValidationTDSSuccessful(java.lang.Boolean validationTDSSuccessful) {
        this.validationTDSSuccessful = validationTDSSuccessful;
    }


    /**
     * Gets the validationTDSAuthResult value for this ThreeDSecure.
     * 
     * @return validationTDSAuthResult
     */
    public java.lang.String getValidationTDSAuthResult() {
        return validationTDSAuthResult;
    }


    /**
     * Sets the validationTDSAuthResult value for this ThreeDSecure.
     * 
     * @param validationTDSAuthResult
     */
    public void setValidationTDSAuthResult(java.lang.String validationTDSAuthResult) {
        this.validationTDSAuthResult = validationTDSAuthResult;
    }


    /**
     * Gets the validationTDSCavv value for this ThreeDSecure.
     * 
     * @return validationTDSCavv
     */
    public java.lang.String getValidationTDSCavv() {
        return validationTDSCavv;
    }


    /**
     * Sets the validationTDSCavv value for this ThreeDSecure.
     * 
     * @param validationTDSCavv
     */
    public void setValidationTDSCavv(java.lang.String validationTDSCavv) {
        this.validationTDSCavv = validationTDSCavv;
    }


    /**
     * Gets the validationTDSCavvAlgorithm value for this ThreeDSecure.
     * 
     * @return validationTDSCavvAlgorithm
     */
    public java.lang.String getValidationTDSCavvAlgorithm() {
        return validationTDSCavvAlgorithm;
    }


    /**
     * Sets the validationTDSCavvAlgorithm value for this ThreeDSecure.
     * 
     * @param validationTDSCavvAlgorithm
     */
    public void setValidationTDSCavvAlgorithm(java.lang.String validationTDSCavvAlgorithm) {
        this.validationTDSCavvAlgorithm = validationTDSCavvAlgorithm;
    }


    /**
     * Gets the validationTDSEci value for this ThreeDSecure.
     * 
     * @return validationTDSEci
     */
    public java.lang.String getValidationTDSEci() {
        return validationTDSEci;
    }


    /**
     * Sets the validationTDSEci value for this ThreeDSecure.
     * 
     * @param validationTDSEci
     */
    public void setValidationTDSEci(java.lang.String validationTDSEci) {
        this.validationTDSEci = validationTDSEci;
    }


    /**
     * Gets the validationTDSXid value for this ThreeDSecure.
     * 
     * @return validationTDSXid
     */
    public java.lang.String getValidationTDSXid() {
        return validationTDSXid;
    }


    /**
     * Sets the validationTDSXid value for this ThreeDSecure.
     * 
     * @param validationTDSXid
     */
    public void setValidationTDSXid(java.lang.String validationTDSXid) {
        this.validationTDSXid = validationTDSXid;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ThreeDSecure)) return false;
        ThreeDSecure other = (ThreeDSecure) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browserUserAgent==null && other.getBrowserUserAgent()==null) || 
             (this.browserUserAgent!=null &&
              this.browserUserAgent.equals(other.getBrowserUserAgent()))) &&
            ((this.browserAccept==null && other.getBrowserAccept()==null) || 
             (this.browserAccept!=null &&
              this.browserAccept.equals(other.getBrowserAccept()))) &&
            ((this.remoteIpAddress==null && other.getRemoteIpAddress()==null) || 
             (this.remoteIpAddress!=null &&
              this.remoteIpAddress.equals(other.getRemoteIpAddress()))) &&
            ((this.termUrl==null && other.getTermUrl()==null) || 
             (this.termUrl!=null &&
              this.termUrl.equals(other.getTermUrl()))) &&
            ((this.proxyVia==null && other.getProxyVia()==null) || 
             (this.proxyVia!=null &&
              this.proxyVia.equals(other.getProxyVia()))) &&
            ((this.validationTDSApplicable==null && other.getValidationTDSApplicable()==null) || 
             (this.validationTDSApplicable!=null &&
              this.validationTDSApplicable.equals(other.getValidationTDSApplicable()))) &&
            ((this.validationTDSPaReq==null && other.getValidationTDSPaReq()==null) || 
             (this.validationTDSPaReq!=null &&
              this.validationTDSPaReq.equals(other.getValidationTDSPaReq()))) &&
            ((this.validationTDSAcsUrl==null && other.getValidationTDSAcsUrl()==null) || 
             (this.validationTDSAcsUrl!=null &&
              this.validationTDSAcsUrl.equals(other.getValidationTDSAcsUrl()))) &&
            ((this.validationTDSPaRes==null && other.getValidationTDSPaRes()==null) || 
             (this.validationTDSPaRes!=null &&
              this.validationTDSPaRes.equals(other.getValidationTDSPaRes()))) &&
            ((this.validationTDSSuccessful==null && other.getValidationTDSSuccessful()==null) || 
             (this.validationTDSSuccessful!=null &&
              this.validationTDSSuccessful.equals(other.getValidationTDSSuccessful()))) &&
            ((this.validationTDSAuthResult==null && other.getValidationTDSAuthResult()==null) || 
             (this.validationTDSAuthResult!=null &&
              this.validationTDSAuthResult.equals(other.getValidationTDSAuthResult()))) &&
            ((this.validationTDSCavv==null && other.getValidationTDSCavv()==null) || 
             (this.validationTDSCavv!=null &&
              this.validationTDSCavv.equals(other.getValidationTDSCavv()))) &&
            ((this.validationTDSCavvAlgorithm==null && other.getValidationTDSCavvAlgorithm()==null) || 
             (this.validationTDSCavvAlgorithm!=null &&
              this.validationTDSCavvAlgorithm.equals(other.getValidationTDSCavvAlgorithm()))) &&
            ((this.validationTDSEci==null && other.getValidationTDSEci()==null) || 
             (this.validationTDSEci!=null &&
              this.validationTDSEci.equals(other.getValidationTDSEci()))) &&
            ((this.validationTDSXid==null && other.getValidationTDSXid()==null) || 
             (this.validationTDSXid!=null &&
              this.validationTDSXid.equals(other.getValidationTDSXid())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowserUserAgent() != null) {
            _hashCode += getBrowserUserAgent().hashCode();
        }
        if (getBrowserAccept() != null) {
            _hashCode += getBrowserAccept().hashCode();
        }
        if (getRemoteIpAddress() != null) {
            _hashCode += getRemoteIpAddress().hashCode();
        }
        if (getTermUrl() != null) {
            _hashCode += getTermUrl().hashCode();
        }
        if (getProxyVia() != null) {
            _hashCode += getProxyVia().hashCode();
        }
        if (getValidationTDSApplicable() != null) {
            _hashCode += getValidationTDSApplicable().hashCode();
        }
        if (getValidationTDSPaReq() != null) {
            _hashCode += getValidationTDSPaReq().hashCode();
        }
        if (getValidationTDSAcsUrl() != null) {
            _hashCode += getValidationTDSAcsUrl().hashCode();
        }
        if (getValidationTDSPaRes() != null) {
            _hashCode += getValidationTDSPaRes().hashCode();
        }
        if (getValidationTDSSuccessful() != null) {
            _hashCode += getValidationTDSSuccessful().hashCode();
        }
        if (getValidationTDSAuthResult() != null) {
            _hashCode += getValidationTDSAuthResult().hashCode();
        }
        if (getValidationTDSCavv() != null) {
            _hashCode += getValidationTDSCavv().hashCode();
        }
        if (getValidationTDSCavvAlgorithm() != null) {
            _hashCode += getValidationTDSCavvAlgorithm().hashCode();
        }
        if (getValidationTDSEci() != null) {
            _hashCode += getValidationTDSEci().hashCode();
        }
        if (getValidationTDSXid() != null) {
            _hashCode += getValidationTDSXid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ThreeDSecure.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ThreeDSecure"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browserUserAgent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BrowserUserAgent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browserAccept");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BrowserAccept"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remoteIpAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RemoteIpAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("termUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TermUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proxyVia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProxyVia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSApplicable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSApplicable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSPaReq");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSPaReq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSAcsUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSAcsUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSPaRes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSPaRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSSuccessful");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSSuccessful"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSAuthResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSAuthResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSCavv");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSCavv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSCavvAlgorithm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSCavvAlgorithm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSEci");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSEci"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationTDSXid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationTDSXid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
