/**
 * OrderItemElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class OrderItemElement  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem basePrice;

    private java.lang.String externalInventoryId;

    private java.lang.Integer inventoryId;

    private java.lang.Integer inventoryQuantity;

    private java.lang.Integer itemSkuCatalogSequence;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderDiscount discount;

    private com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem markup;

    private com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem itemMarkup;

    private com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem vendorMarkup;

    private com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem rateMarkup;

    private java.lang.Integer sellQuantity;

    private java.util.Calendar usageDate;

    public OrderItemElement() {
    }

    public OrderItemElement(
           com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem basePrice,
           java.lang.String externalInventoryId,
           java.lang.Integer inventoryId,
           java.lang.Integer inventoryQuantity,
           java.lang.Integer itemSkuCatalogSequence,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderDiscount discount,
           com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem markup,
           com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem itemMarkup,
           com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem vendorMarkup,
           com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem rateMarkup,
           java.lang.Integer sellQuantity,
           java.util.Calendar usageDate) {
           this.basePrice = basePrice;
           this.externalInventoryId = externalInventoryId;
           this.inventoryId = inventoryId;
           this.inventoryQuantity = inventoryQuantity;
           this.itemSkuCatalogSequence = itemSkuCatalogSequence;
           this.discount = discount;
           this.markup = markup;
           this.itemMarkup = itemMarkup;
           this.vendorMarkup = vendorMarkup;
           this.rateMarkup = rateMarkup;
           this.sellQuantity = sellQuantity;
           this.usageDate = usageDate;
    }


    /**
     * Gets the basePrice value for this OrderItemElement.
     * 
     * @return basePrice
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem getBasePrice() {
        return basePrice;
    }


    /**
     * Sets the basePrice value for this OrderItemElement.
     * 
     * @param basePrice
     */
    public void setBasePrice(com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem basePrice) {
        this.basePrice = basePrice;
    }


    /**
     * Gets the externalInventoryId value for this OrderItemElement.
     * 
     * @return externalInventoryId
     */
    public java.lang.String getExternalInventoryId() {
        return externalInventoryId;
    }


    /**
     * Sets the externalInventoryId value for this OrderItemElement.
     * 
     * @param externalInventoryId
     */
    public void setExternalInventoryId(java.lang.String externalInventoryId) {
        this.externalInventoryId = externalInventoryId;
    }


    /**
     * Gets the inventoryId value for this OrderItemElement.
     * 
     * @return inventoryId
     */
    public java.lang.Integer getInventoryId() {
        return inventoryId;
    }


    /**
     * Sets the inventoryId value for this OrderItemElement.
     * 
     * @param inventoryId
     */
    public void setInventoryId(java.lang.Integer inventoryId) {
        this.inventoryId = inventoryId;
    }


    /**
     * Gets the inventoryQuantity value for this OrderItemElement.
     * 
     * @return inventoryQuantity
     */
    public java.lang.Integer getInventoryQuantity() {
        return inventoryQuantity;
    }


    /**
     * Sets the inventoryQuantity value for this OrderItemElement.
     * 
     * @param inventoryQuantity
     */
    public void setInventoryQuantity(java.lang.Integer inventoryQuantity) {
        this.inventoryQuantity = inventoryQuantity;
    }


    /**
     * Gets the itemSkuCatalogSequence value for this OrderItemElement.
     * 
     * @return itemSkuCatalogSequence
     */
    public java.lang.Integer getItemSkuCatalogSequence() {
        return itemSkuCatalogSequence;
    }


    /**
     * Sets the itemSkuCatalogSequence value for this OrderItemElement.
     * 
     * @param itemSkuCatalogSequence
     */
    public void setItemSkuCatalogSequence(java.lang.Integer itemSkuCatalogSequence) {
        this.itemSkuCatalogSequence = itemSkuCatalogSequence;
    }


    /**
     * Gets the discount value for this OrderItemElement.
     * 
     * @return discount
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderDiscount getDiscount() {
        return discount;
    }


    /**
     * Sets the discount value for this OrderItemElement.
     * 
     * @param discount
     */
    public void setDiscount(com.navitaire.schemas.WebServices.DataContracts.Common.OrderDiscount discount) {
        this.discount = discount;
    }


    /**
     * Gets the markup value for this OrderItemElement.
     * 
     * @return markup
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem getMarkup() {
        return markup;
    }


    /**
     * Sets the markup value for this OrderItemElement.
     * 
     * @param markup
     */
    public void setMarkup(com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem markup) {
        this.markup = markup;
    }


    /**
     * Gets the itemMarkup value for this OrderItemElement.
     * 
     * @return itemMarkup
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem getItemMarkup() {
        return itemMarkup;
    }


    /**
     * Sets the itemMarkup value for this OrderItemElement.
     * 
     * @param itemMarkup
     */
    public void setItemMarkup(com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem itemMarkup) {
        this.itemMarkup = itemMarkup;
    }


    /**
     * Gets the vendorMarkup value for this OrderItemElement.
     * 
     * @return vendorMarkup
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem getVendorMarkup() {
        return vendorMarkup;
    }


    /**
     * Sets the vendorMarkup value for this OrderItemElement.
     * 
     * @param vendorMarkup
     */
    public void setVendorMarkup(com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem vendorMarkup) {
        this.vendorMarkup = vendorMarkup;
    }


    /**
     * Gets the rateMarkup value for this OrderItemElement.
     * 
     * @return rateMarkup
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem getRateMarkup() {
        return rateMarkup;
    }


    /**
     * Sets the rateMarkup value for this OrderItemElement.
     * 
     * @param rateMarkup
     */
    public void setRateMarkup(com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem rateMarkup) {
        this.rateMarkup = rateMarkup;
    }


    /**
     * Gets the sellQuantity value for this OrderItemElement.
     * 
     * @return sellQuantity
     */
    public java.lang.Integer getSellQuantity() {
        return sellQuantity;
    }


    /**
     * Sets the sellQuantity value for this OrderItemElement.
     * 
     * @param sellQuantity
     */
    public void setSellQuantity(java.lang.Integer sellQuantity) {
        this.sellQuantity = sellQuantity;
    }


    /**
     * Gets the usageDate value for this OrderItemElement.
     * 
     * @return usageDate
     */
    public java.util.Calendar getUsageDate() {
        return usageDate;
    }


    /**
     * Sets the usageDate value for this OrderItemElement.
     * 
     * @param usageDate
     */
    public void setUsageDate(java.util.Calendar usageDate) {
        this.usageDate = usageDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderItemElement)) return false;
        OrderItemElement other = (OrderItemElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.basePrice==null && other.getBasePrice()==null) || 
             (this.basePrice!=null &&
              this.basePrice.equals(other.getBasePrice()))) &&
            ((this.externalInventoryId==null && other.getExternalInventoryId()==null) || 
             (this.externalInventoryId!=null &&
              this.externalInventoryId.equals(other.getExternalInventoryId()))) &&
            ((this.inventoryId==null && other.getInventoryId()==null) || 
             (this.inventoryId!=null &&
              this.inventoryId.equals(other.getInventoryId()))) &&
            ((this.inventoryQuantity==null && other.getInventoryQuantity()==null) || 
             (this.inventoryQuantity!=null &&
              this.inventoryQuantity.equals(other.getInventoryQuantity()))) &&
            ((this.itemSkuCatalogSequence==null && other.getItemSkuCatalogSequence()==null) || 
             (this.itemSkuCatalogSequence!=null &&
              this.itemSkuCatalogSequence.equals(other.getItemSkuCatalogSequence()))) &&
            ((this.discount==null && other.getDiscount()==null) || 
             (this.discount!=null &&
              this.discount.equals(other.getDiscount()))) &&
            ((this.markup==null && other.getMarkup()==null) || 
             (this.markup!=null &&
              this.markup.equals(other.getMarkup()))) &&
            ((this.itemMarkup==null && other.getItemMarkup()==null) || 
             (this.itemMarkup!=null &&
              this.itemMarkup.equals(other.getItemMarkup()))) &&
            ((this.vendorMarkup==null && other.getVendorMarkup()==null) || 
             (this.vendorMarkup!=null &&
              this.vendorMarkup.equals(other.getVendorMarkup()))) &&
            ((this.rateMarkup==null && other.getRateMarkup()==null) || 
             (this.rateMarkup!=null &&
              this.rateMarkup.equals(other.getRateMarkup()))) &&
            ((this.sellQuantity==null && other.getSellQuantity()==null) || 
             (this.sellQuantity!=null &&
              this.sellQuantity.equals(other.getSellQuantity()))) &&
            ((this.usageDate==null && other.getUsageDate()==null) || 
             (this.usageDate!=null &&
              this.usageDate.equals(other.getUsageDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBasePrice() != null) {
            _hashCode += getBasePrice().hashCode();
        }
        if (getExternalInventoryId() != null) {
            _hashCode += getExternalInventoryId().hashCode();
        }
        if (getInventoryId() != null) {
            _hashCode += getInventoryId().hashCode();
        }
        if (getInventoryQuantity() != null) {
            _hashCode += getInventoryQuantity().hashCode();
        }
        if (getItemSkuCatalogSequence() != null) {
            _hashCode += getItemSkuCatalogSequence().hashCode();
        }
        if (getDiscount() != null) {
            _hashCode += getDiscount().hashCode();
        }
        if (getMarkup() != null) {
            _hashCode += getMarkup().hashCode();
        }
        if (getItemMarkup() != null) {
            _hashCode += getItemMarkup().hashCode();
        }
        if (getVendorMarkup() != null) {
            _hashCode += getVendorMarkup().hashCode();
        }
        if (getRateMarkup() != null) {
            _hashCode += getRateMarkup().hashCode();
        }
        if (getSellQuantity() != null) {
            _hashCode += getSellQuantity().hashCode();
        }
        if (getUsageDate() != null) {
            _hashCode += getUsageDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderItemElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basePrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "BasePrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ChargeableItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalInventoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ExternalInventoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inventoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "InventoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inventoryQuantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "InventoryQuantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemSkuCatalogSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ItemSkuCatalogSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Discount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderDiscount"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("markup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Markup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ChargeableItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemMarkup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ItemMarkup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ChargeableItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendorMarkup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "VendorMarkup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ChargeableItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateMarkup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RateMarkup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ChargeableItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellQuantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SellQuantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usageDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "UsageDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
