/**
 * SystemType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class SystemType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SystemType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Default = "Default";
    public static final java.lang.String _WinRez = "WinRez";
    public static final java.lang.String _FareManager = "FareManager";
    public static final java.lang.String _ScheduleManager = "ScheduleManager";
    public static final java.lang.String _WinManager = "WinManager";
    public static final java.lang.String _ConsoleRez = "ConsoleRez";
    public static final java.lang.String _WebRez = "WebRez";
    public static final java.lang.String _WebServicesAPI = "WebServicesAPI";
    public static final java.lang.String _WebServicesESC = "WebServicesESC";
    public static final java.lang.String _InternalService = "InternalService";
    public static final java.lang.String _WebReporting = "WebReporting";
    public static final java.lang.String _TaxAndFeeManager = "TaxAndFeeManager";
    public static final java.lang.String _DCS = "DCS";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final SystemType Default = new SystemType(_Default);
    public static final SystemType WinRez = new SystemType(_WinRez);
    public static final SystemType FareManager = new SystemType(_FareManager);
    public static final SystemType ScheduleManager = new SystemType(_ScheduleManager);
    public static final SystemType WinManager = new SystemType(_WinManager);
    public static final SystemType ConsoleRez = new SystemType(_ConsoleRez);
    public static final SystemType WebRez = new SystemType(_WebRez);
    public static final SystemType WebServicesAPI = new SystemType(_WebServicesAPI);
    public static final SystemType WebServicesESC = new SystemType(_WebServicesESC);
    public static final SystemType InternalService = new SystemType(_InternalService);
    public static final SystemType WebReporting = new SystemType(_WebReporting);
    public static final SystemType TaxAndFeeManager = new SystemType(_TaxAndFeeManager);
    public static final SystemType DCS = new SystemType(_DCS);
    public static final SystemType Unmapped = new SystemType(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static SystemType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SystemType enumeration = (SystemType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SystemType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SystemType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SystemType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
