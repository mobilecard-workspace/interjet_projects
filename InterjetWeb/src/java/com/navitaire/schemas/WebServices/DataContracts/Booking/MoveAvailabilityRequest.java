/**
 * MoveAvailabilityRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class MoveAvailabilityRequest  implements java.io.Serializable {
    private java.lang.String departureStation;

    private java.lang.String arrivalStation;

    private java.util.Calendar beginDate;

    private java.util.Calendar endDate;

    private java.lang.String carrierCode;

    private java.lang.String flightNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlightType flightType;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DOW dow;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityType availabilityType;

    private java.lang.Short maximumConnectingFlights;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityFilter availabilityFilter;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SSRCollectionsMode SSRCollectionsMode;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Time beginTime;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Time endTime;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Journey fromJourney;

    private java.lang.String[] departureStations;

    private java.lang.String[] arrivalStations;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey[] journeySortKeys;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType movePassengerJourneyType;

    private short[] passengerNumberList;

    public MoveAvailabilityRequest() {
    }

    public MoveAvailabilityRequest(
           java.lang.String departureStation,
           java.lang.String arrivalStation,
           java.util.Calendar beginDate,
           java.util.Calendar endDate,
           java.lang.String carrierCode,
           java.lang.String flightNumber,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlightType flightType,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DOW dow,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityType availabilityType,
           java.lang.Short maximumConnectingFlights,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityFilter availabilityFilter,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SSRCollectionsMode SSRCollectionsMode,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Time beginTime,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Time endTime,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Journey fromJourney,
           java.lang.String[] departureStations,
           java.lang.String[] arrivalStations,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey[] journeySortKeys,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType movePassengerJourneyType,
           short[] passengerNumberList) {
           this.departureStation = departureStation;
           this.arrivalStation = arrivalStation;
           this.beginDate = beginDate;
           this.endDate = endDate;
           this.carrierCode = carrierCode;
           this.flightNumber = flightNumber;
           this.flightType = flightType;
           this.dow = dow;
           this.availabilityType = availabilityType;
           this.maximumConnectingFlights = maximumConnectingFlights;
           this.availabilityFilter = availabilityFilter;
           this.SSRCollectionsMode = SSRCollectionsMode;
           this.beginTime = beginTime;
           this.endTime = endTime;
           this.fromJourney = fromJourney;
           this.departureStations = departureStations;
           this.arrivalStations = arrivalStations;
           this.journeySortKeys = journeySortKeys;
           this.movePassengerJourneyType = movePassengerJourneyType;
           this.passengerNumberList = passengerNumberList;
    }


    /**
     * Gets the departureStation value for this MoveAvailabilityRequest.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this MoveAvailabilityRequest.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the arrivalStation value for this MoveAvailabilityRequest.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this MoveAvailabilityRequest.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the beginDate value for this MoveAvailabilityRequest.
     * 
     * @return beginDate
     */
    public java.util.Calendar getBeginDate() {
        return beginDate;
    }


    /**
     * Sets the beginDate value for this MoveAvailabilityRequest.
     * 
     * @param beginDate
     */
    public void setBeginDate(java.util.Calendar beginDate) {
        this.beginDate = beginDate;
    }


    /**
     * Gets the endDate value for this MoveAvailabilityRequest.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this MoveAvailabilityRequest.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the carrierCode value for this MoveAvailabilityRequest.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this MoveAvailabilityRequest.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the flightNumber value for this MoveAvailabilityRequest.
     * 
     * @return flightNumber
     */
    public java.lang.String getFlightNumber() {
        return flightNumber;
    }


    /**
     * Sets the flightNumber value for this MoveAvailabilityRequest.
     * 
     * @param flightNumber
     */
    public void setFlightNumber(java.lang.String flightNumber) {
        this.flightNumber = flightNumber;
    }


    /**
     * Gets the flightType value for this MoveAvailabilityRequest.
     * 
     * @return flightType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlightType getFlightType() {
        return flightType;
    }


    /**
     * Sets the flightType value for this MoveAvailabilityRequest.
     * 
     * @param flightType
     */
    public void setFlightType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlightType flightType) {
        this.flightType = flightType;
    }


    /**
     * Gets the dow value for this MoveAvailabilityRequest.
     * 
     * @return dow
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DOW getDow() {
        return dow;
    }


    /**
     * Sets the dow value for this MoveAvailabilityRequest.
     * 
     * @param dow
     */
    public void setDow(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DOW dow) {
        this.dow = dow;
    }


    /**
     * Gets the availabilityType value for this MoveAvailabilityRequest.
     * 
     * @return availabilityType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityType getAvailabilityType() {
        return availabilityType;
    }


    /**
     * Sets the availabilityType value for this MoveAvailabilityRequest.
     * 
     * @param availabilityType
     */
    public void setAvailabilityType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityType availabilityType) {
        this.availabilityType = availabilityType;
    }


    /**
     * Gets the maximumConnectingFlights value for this MoveAvailabilityRequest.
     * 
     * @return maximumConnectingFlights
     */
    public java.lang.Short getMaximumConnectingFlights() {
        return maximumConnectingFlights;
    }


    /**
     * Sets the maximumConnectingFlights value for this MoveAvailabilityRequest.
     * 
     * @param maximumConnectingFlights
     */
    public void setMaximumConnectingFlights(java.lang.Short maximumConnectingFlights) {
        this.maximumConnectingFlights = maximumConnectingFlights;
    }


    /**
     * Gets the availabilityFilter value for this MoveAvailabilityRequest.
     * 
     * @return availabilityFilter
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityFilter getAvailabilityFilter() {
        return availabilityFilter;
    }


    /**
     * Sets the availabilityFilter value for this MoveAvailabilityRequest.
     * 
     * @param availabilityFilter
     */
    public void setAvailabilityFilter(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityFilter availabilityFilter) {
        this.availabilityFilter = availabilityFilter;
    }


    /**
     * Gets the SSRCollectionsMode value for this MoveAvailabilityRequest.
     * 
     * @return SSRCollectionsMode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SSRCollectionsMode getSSRCollectionsMode() {
        return SSRCollectionsMode;
    }


    /**
     * Sets the SSRCollectionsMode value for this MoveAvailabilityRequest.
     * 
     * @param SSRCollectionsMode
     */
    public void setSSRCollectionsMode(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SSRCollectionsMode SSRCollectionsMode) {
        this.SSRCollectionsMode = SSRCollectionsMode;
    }


    /**
     * Gets the beginTime value for this MoveAvailabilityRequest.
     * 
     * @return beginTime
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Time getBeginTime() {
        return beginTime;
    }


    /**
     * Sets the beginTime value for this MoveAvailabilityRequest.
     * 
     * @param beginTime
     */
    public void setBeginTime(com.navitaire.schemas.WebServices.DataContracts.Booking.Time beginTime) {
        this.beginTime = beginTime;
    }


    /**
     * Gets the endTime value for this MoveAvailabilityRequest.
     * 
     * @return endTime
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Time getEndTime() {
        return endTime;
    }


    /**
     * Sets the endTime value for this MoveAvailabilityRequest.
     * 
     * @param endTime
     */
    public void setEndTime(com.navitaire.schemas.WebServices.DataContracts.Booking.Time endTime) {
        this.endTime = endTime;
    }


    /**
     * Gets the fromJourney value for this MoveAvailabilityRequest.
     * 
     * @return fromJourney
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Journey getFromJourney() {
        return fromJourney;
    }


    /**
     * Sets the fromJourney value for this MoveAvailabilityRequest.
     * 
     * @param fromJourney
     */
    public void setFromJourney(com.navitaire.schemas.WebServices.DataContracts.Booking.Journey fromJourney) {
        this.fromJourney = fromJourney;
    }


    /**
     * Gets the departureStations value for this MoveAvailabilityRequest.
     * 
     * @return departureStations
     */
    public java.lang.String[] getDepartureStations() {
        return departureStations;
    }


    /**
     * Sets the departureStations value for this MoveAvailabilityRequest.
     * 
     * @param departureStations
     */
    public void setDepartureStations(java.lang.String[] departureStations) {
        this.departureStations = departureStations;
    }


    /**
     * Gets the arrivalStations value for this MoveAvailabilityRequest.
     * 
     * @return arrivalStations
     */
    public java.lang.String[] getArrivalStations() {
        return arrivalStations;
    }


    /**
     * Sets the arrivalStations value for this MoveAvailabilityRequest.
     * 
     * @param arrivalStations
     */
    public void setArrivalStations(java.lang.String[] arrivalStations) {
        this.arrivalStations = arrivalStations;
    }


    /**
     * Gets the journeySortKeys value for this MoveAvailabilityRequest.
     * 
     * @return journeySortKeys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey[] getJourneySortKeys() {
        return journeySortKeys;
    }


    /**
     * Sets the journeySortKeys value for this MoveAvailabilityRequest.
     * 
     * @param journeySortKeys
     */
    public void setJourneySortKeys(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey[] journeySortKeys) {
        this.journeySortKeys = journeySortKeys;
    }


    /**
     * Gets the movePassengerJourneyType value for this MoveAvailabilityRequest.
     * 
     * @return movePassengerJourneyType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType getMovePassengerJourneyType() {
        return movePassengerJourneyType;
    }


    /**
     * Sets the movePassengerJourneyType value for this MoveAvailabilityRequest.
     * 
     * @param movePassengerJourneyType
     */
    public void setMovePassengerJourneyType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType movePassengerJourneyType) {
        this.movePassengerJourneyType = movePassengerJourneyType;
    }


    /**
     * Gets the passengerNumberList value for this MoveAvailabilityRequest.
     * 
     * @return passengerNumberList
     */
    public short[] getPassengerNumberList() {
        return passengerNumberList;
    }


    /**
     * Sets the passengerNumberList value for this MoveAvailabilityRequest.
     * 
     * @param passengerNumberList
     */
    public void setPassengerNumberList(short[] passengerNumberList) {
        this.passengerNumberList = passengerNumberList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MoveAvailabilityRequest)) return false;
        MoveAvailabilityRequest other = (MoveAvailabilityRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.beginDate==null && other.getBeginDate()==null) || 
             (this.beginDate!=null &&
              this.beginDate.equals(other.getBeginDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.flightNumber==null && other.getFlightNumber()==null) || 
             (this.flightNumber!=null &&
              this.flightNumber.equals(other.getFlightNumber()))) &&
            ((this.flightType==null && other.getFlightType()==null) || 
             (this.flightType!=null &&
              this.flightType.equals(other.getFlightType()))) &&
            ((this.dow==null && other.getDow()==null) || 
             (this.dow!=null &&
              this.dow.equals(other.getDow()))) &&
            ((this.availabilityType==null && other.getAvailabilityType()==null) || 
             (this.availabilityType!=null &&
              this.availabilityType.equals(other.getAvailabilityType()))) &&
            ((this.maximumConnectingFlights==null && other.getMaximumConnectingFlights()==null) || 
             (this.maximumConnectingFlights!=null &&
              this.maximumConnectingFlights.equals(other.getMaximumConnectingFlights()))) &&
            ((this.availabilityFilter==null && other.getAvailabilityFilter()==null) || 
             (this.availabilityFilter!=null &&
              this.availabilityFilter.equals(other.getAvailabilityFilter()))) &&
            ((this.SSRCollectionsMode==null && other.getSSRCollectionsMode()==null) || 
             (this.SSRCollectionsMode!=null &&
              this.SSRCollectionsMode.equals(other.getSSRCollectionsMode()))) &&
            ((this.beginTime==null && other.getBeginTime()==null) || 
             (this.beginTime!=null &&
              this.beginTime.equals(other.getBeginTime()))) &&
            ((this.endTime==null && other.getEndTime()==null) || 
             (this.endTime!=null &&
              this.endTime.equals(other.getEndTime()))) &&
            ((this.fromJourney==null && other.getFromJourney()==null) || 
             (this.fromJourney!=null &&
              this.fromJourney.equals(other.getFromJourney()))) &&
            ((this.departureStations==null && other.getDepartureStations()==null) || 
             (this.departureStations!=null &&
              java.util.Arrays.equals(this.departureStations, other.getDepartureStations()))) &&
            ((this.arrivalStations==null && other.getArrivalStations()==null) || 
             (this.arrivalStations!=null &&
              java.util.Arrays.equals(this.arrivalStations, other.getArrivalStations()))) &&
            ((this.journeySortKeys==null && other.getJourneySortKeys()==null) || 
             (this.journeySortKeys!=null &&
              java.util.Arrays.equals(this.journeySortKeys, other.getJourneySortKeys()))) &&
            ((this.movePassengerJourneyType==null && other.getMovePassengerJourneyType()==null) || 
             (this.movePassengerJourneyType!=null &&
              this.movePassengerJourneyType.equals(other.getMovePassengerJourneyType()))) &&
            ((this.passengerNumberList==null && other.getPassengerNumberList()==null) || 
             (this.passengerNumberList!=null &&
              java.util.Arrays.equals(this.passengerNumberList, other.getPassengerNumberList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getBeginDate() != null) {
            _hashCode += getBeginDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getFlightNumber() != null) {
            _hashCode += getFlightNumber().hashCode();
        }
        if (getFlightType() != null) {
            _hashCode += getFlightType().hashCode();
        }
        if (getDow() != null) {
            _hashCode += getDow().hashCode();
        }
        if (getAvailabilityType() != null) {
            _hashCode += getAvailabilityType().hashCode();
        }
        if (getMaximumConnectingFlights() != null) {
            _hashCode += getMaximumConnectingFlights().hashCode();
        }
        if (getAvailabilityFilter() != null) {
            _hashCode += getAvailabilityFilter().hashCode();
        }
        if (getSSRCollectionsMode() != null) {
            _hashCode += getSSRCollectionsMode().hashCode();
        }
        if (getBeginTime() != null) {
            _hashCode += getBeginTime().hashCode();
        }
        if (getEndTime() != null) {
            _hashCode += getEndTime().hashCode();
        }
        if (getFromJourney() != null) {
            _hashCode += getFromJourney().hashCode();
        }
        if (getDepartureStations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDepartureStations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDepartureStations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getArrivalStations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getArrivalStations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getArrivalStations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getJourneySortKeys() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJourneySortKeys());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJourneySortKeys(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMovePassengerJourneyType() != null) {
            _hashCode += getMovePassengerJourneyType().hashCode();
        }
        if (getPassengerNumberList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerNumberList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerNumberList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MoveAvailabilityRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveAvailabilityRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BeginDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FlightType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Dow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DOW"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availabilityType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AvailabilityType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maximumConnectingFlights");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MaximumConnectingFlights"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availabilityFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AvailabilityFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRCollectionsMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRCollectionsMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SSRCollectionsMode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BeginTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromJourney");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FromJourney"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeySortKeys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneySortKeys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "JourneySortKey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "JourneySortKey"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("movePassengerJourneyType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MovePassengerJourneyType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "MovePassengerJourneyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumberList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumberList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "short"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
