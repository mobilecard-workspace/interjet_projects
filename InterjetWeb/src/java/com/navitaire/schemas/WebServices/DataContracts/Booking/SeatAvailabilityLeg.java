/**
 * SeatAvailabilityLeg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SeatAvailabilityLeg  implements java.io.Serializable {
    private java.lang.String arrivalStation;

    private java.lang.String departureStation;

    private java.util.Calendar STA;

    private java.util.Calendar STD;

    private com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator;

    private java.lang.String aircraftType;

    private java.lang.String aircraftTypeSuffix;

    private java.lang.Integer equipmentIndex;

    public SeatAvailabilityLeg() {
    }

    public SeatAvailabilityLeg(
           java.lang.String arrivalStation,
           java.lang.String departureStation,
           java.util.Calendar STA,
           java.util.Calendar STD,
           com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator,
           java.lang.String aircraftType,
           java.lang.String aircraftTypeSuffix,
           java.lang.Integer equipmentIndex) {
           this.arrivalStation = arrivalStation;
           this.departureStation = departureStation;
           this.STA = STA;
           this.STD = STD;
           this.flightDesignator = flightDesignator;
           this.aircraftType = aircraftType;
           this.aircraftTypeSuffix = aircraftTypeSuffix;
           this.equipmentIndex = equipmentIndex;
    }


    /**
     * Gets the arrivalStation value for this SeatAvailabilityLeg.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this SeatAvailabilityLeg.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the departureStation value for this SeatAvailabilityLeg.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this SeatAvailabilityLeg.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the STA value for this SeatAvailabilityLeg.
     * 
     * @return STA
     */
    public java.util.Calendar getSTA() {
        return STA;
    }


    /**
     * Sets the STA value for this SeatAvailabilityLeg.
     * 
     * @param STA
     */
    public void setSTA(java.util.Calendar STA) {
        this.STA = STA;
    }


    /**
     * Gets the STD value for this SeatAvailabilityLeg.
     * 
     * @return STD
     */
    public java.util.Calendar getSTD() {
        return STD;
    }


    /**
     * Sets the STD value for this SeatAvailabilityLeg.
     * 
     * @param STD
     */
    public void setSTD(java.util.Calendar STD) {
        this.STD = STD;
    }


    /**
     * Gets the flightDesignator value for this SeatAvailabilityLeg.
     * 
     * @return flightDesignator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator getFlightDesignator() {
        return flightDesignator;
    }


    /**
     * Sets the flightDesignator value for this SeatAvailabilityLeg.
     * 
     * @param flightDesignator
     */
    public void setFlightDesignator(com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator) {
        this.flightDesignator = flightDesignator;
    }


    /**
     * Gets the aircraftType value for this SeatAvailabilityLeg.
     * 
     * @return aircraftType
     */
    public java.lang.String getAircraftType() {
        return aircraftType;
    }


    /**
     * Sets the aircraftType value for this SeatAvailabilityLeg.
     * 
     * @param aircraftType
     */
    public void setAircraftType(java.lang.String aircraftType) {
        this.aircraftType = aircraftType;
    }


    /**
     * Gets the aircraftTypeSuffix value for this SeatAvailabilityLeg.
     * 
     * @return aircraftTypeSuffix
     */
    public java.lang.String getAircraftTypeSuffix() {
        return aircraftTypeSuffix;
    }


    /**
     * Sets the aircraftTypeSuffix value for this SeatAvailabilityLeg.
     * 
     * @param aircraftTypeSuffix
     */
    public void setAircraftTypeSuffix(java.lang.String aircraftTypeSuffix) {
        this.aircraftTypeSuffix = aircraftTypeSuffix;
    }


    /**
     * Gets the equipmentIndex value for this SeatAvailabilityLeg.
     * 
     * @return equipmentIndex
     */
    public java.lang.Integer getEquipmentIndex() {
        return equipmentIndex;
    }


    /**
     * Sets the equipmentIndex value for this SeatAvailabilityLeg.
     * 
     * @param equipmentIndex
     */
    public void setEquipmentIndex(java.lang.Integer equipmentIndex) {
        this.equipmentIndex = equipmentIndex;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SeatAvailabilityLeg)) return false;
        SeatAvailabilityLeg other = (SeatAvailabilityLeg) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.STA==null && other.getSTA()==null) || 
             (this.STA!=null &&
              this.STA.equals(other.getSTA()))) &&
            ((this.STD==null && other.getSTD()==null) || 
             (this.STD!=null &&
              this.STD.equals(other.getSTD()))) &&
            ((this.flightDesignator==null && other.getFlightDesignator()==null) || 
             (this.flightDesignator!=null &&
              this.flightDesignator.equals(other.getFlightDesignator()))) &&
            ((this.aircraftType==null && other.getAircraftType()==null) || 
             (this.aircraftType!=null &&
              this.aircraftType.equals(other.getAircraftType()))) &&
            ((this.aircraftTypeSuffix==null && other.getAircraftTypeSuffix()==null) || 
             (this.aircraftTypeSuffix!=null &&
              this.aircraftTypeSuffix.equals(other.getAircraftTypeSuffix()))) &&
            ((this.equipmentIndex==null && other.getEquipmentIndex()==null) || 
             (this.equipmentIndex!=null &&
              this.equipmentIndex.equals(other.getEquipmentIndex())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getSTA() != null) {
            _hashCode += getSTA().hashCode();
        }
        if (getSTD() != null) {
            _hashCode += getSTD().hashCode();
        }
        if (getFlightDesignator() != null) {
            _hashCode += getFlightDesignator().hashCode();
        }
        if (getAircraftType() != null) {
            _hashCode += getAircraftType().hashCode();
        }
        if (getAircraftTypeSuffix() != null) {
            _hashCode += getAircraftTypeSuffix().hashCode();
        }
        if (getEquipmentIndex() != null) {
            _hashCode += getEquipmentIndex().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SeatAvailabilityLeg.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailabilityLeg"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STA");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FlightDesignator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aircraftType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AircraftType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aircraftTypeSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AircraftTypeSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentIndex");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentIndex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
