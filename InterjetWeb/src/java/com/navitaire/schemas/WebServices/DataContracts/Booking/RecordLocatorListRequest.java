/**
 * RecordLocatorListRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class RecordLocatorListRequest  implements java.io.Serializable {
    private java.util.Calendar departureDate;

    private java.lang.String carrierCode;

    private java.lang.String startFlightNumber;

    private java.lang.String endFlightNumber;

    public RecordLocatorListRequest() {
    }

    public RecordLocatorListRequest(
           java.util.Calendar departureDate,
           java.lang.String carrierCode,
           java.lang.String startFlightNumber,
           java.lang.String endFlightNumber) {
           this.departureDate = departureDate;
           this.carrierCode = carrierCode;
           this.startFlightNumber = startFlightNumber;
           this.endFlightNumber = endFlightNumber;
    }


    /**
     * Gets the departureDate value for this RecordLocatorListRequest.
     * 
     * @return departureDate
     */
    public java.util.Calendar getDepartureDate() {
        return departureDate;
    }


    /**
     * Sets the departureDate value for this RecordLocatorListRequest.
     * 
     * @param departureDate
     */
    public void setDepartureDate(java.util.Calendar departureDate) {
        this.departureDate = departureDate;
    }


    /**
     * Gets the carrierCode value for this RecordLocatorListRequest.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this RecordLocatorListRequest.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the startFlightNumber value for this RecordLocatorListRequest.
     * 
     * @return startFlightNumber
     */
    public java.lang.String getStartFlightNumber() {
        return startFlightNumber;
    }


    /**
     * Sets the startFlightNumber value for this RecordLocatorListRequest.
     * 
     * @param startFlightNumber
     */
    public void setStartFlightNumber(java.lang.String startFlightNumber) {
        this.startFlightNumber = startFlightNumber;
    }


    /**
     * Gets the endFlightNumber value for this RecordLocatorListRequest.
     * 
     * @return endFlightNumber
     */
    public java.lang.String getEndFlightNumber() {
        return endFlightNumber;
    }


    /**
     * Sets the endFlightNumber value for this RecordLocatorListRequest.
     * 
     * @param endFlightNumber
     */
    public void setEndFlightNumber(java.lang.String endFlightNumber) {
        this.endFlightNumber = endFlightNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RecordLocatorListRequest)) return false;
        RecordLocatorListRequest other = (RecordLocatorListRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departureDate==null && other.getDepartureDate()==null) || 
             (this.departureDate!=null &&
              this.departureDate.equals(other.getDepartureDate()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.startFlightNumber==null && other.getStartFlightNumber()==null) || 
             (this.startFlightNumber!=null &&
              this.startFlightNumber.equals(other.getStartFlightNumber()))) &&
            ((this.endFlightNumber==null && other.getEndFlightNumber()==null) || 
             (this.endFlightNumber!=null &&
              this.endFlightNumber.equals(other.getEndFlightNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartureDate() != null) {
            _hashCode += getDepartureDate().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getStartFlightNumber() != null) {
            _hashCode += getStartFlightNumber().hashCode();
        }
        if (getEndFlightNumber() != null) {
            _hashCode += getEndFlightNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RecordLocatorListRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocatorListRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startFlightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "StartFlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endFlightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndFlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
