/**
 * QueueEventType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class QueueEventType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected QueueEventType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Default = "Default";
    public static final java.lang.String _BookingBalanceDue = "BookingBalanceDue";
    public static final java.lang.String _BookingNegativeBalance = "BookingNegativeBalance";
    public static final java.lang.String _BookingCustomerComment = "BookingCustomerComment";
    public static final java.lang.String _DeclinedPaymentInitial = "DeclinedPaymentInitial";
    public static final java.lang.String _DeclinedPaymentChange = "DeclinedPaymentChange";
    public static final java.lang.String _FareOverride = "FareOverride";
    public static final java.lang.String _ScheduleTimeChange = "ScheduleTimeChange";
    public static final java.lang.String _ScheduleTimeChangeMisconnect = "ScheduleTimeChangeMisconnect";
    public static final java.lang.String _ScheduleCancellation = "ScheduleCancellation";
    public static final java.lang.String _FlightDesignatorChange = "FlightDesignatorChange";
    public static final java.lang.String _ReaccommodationMove = "ReaccommodationMove";
    public static final java.lang.String _GDSCancelWithPendingPayment = "GDSCancelWithPendingPayment";
    public static final java.lang.String _InvalidPriceStatusOverride = "InvalidPriceStatusOverride";
    public static final java.lang.String _FareRestrictionOverride = "FareRestrictionOverride";
    public static final java.lang.String _HeldBookings = "HeldBookings";
    public static final java.lang.String _InvalidPriceStatus = "InvalidPriceStatus";
    public static final java.lang.String _Watchlist = "Watchlist";
    public static final java.lang.String _NonFlightServiceFee = "NonFlightServiceFee";
    public static final java.lang.String _NotAllTicketNumbersReceived = "NotAllTicketNumbersReceived";
    public static final java.lang.String _BookingSegmentOversold = "BookingSegmentOversold";
    public static final java.lang.String _ReaccommodationCancel = "ReaccommodationCancel";
    public static final java.lang.String _ExternalSSRAutoConfirmed = "ExternalSSRAutoConfirmed";
    public static final java.lang.String _OpCarrierSegUpdate = "OpCarrierSegUpdate";
    public static final java.lang.String _OpCarrierSSRUpdate = "OpCarrierSSRUpdate";
    public static final java.lang.String _OpCarrierOtherUpdate = "OpCarrierOtherUpdate";
    public static final java.lang.String _NameChangeNotAllowed = "NameChangeNotAllowed";
    public static final java.lang.String _InboundASCNotProcessed = "InboundASCNotProcessed";
    public static final java.lang.String _OpCarrierInformationChange = "OpCarrierInformationChange";
    public static final java.lang.String _BookingComponentUpdate = "BookingComponentUpdate";
    public static final java.lang.String _GroupBookings = "GroupBookings";
    public static final java.lang.String _BankDirectPNROutOfBalance = "BankDirectPNROutOfBalance";
    public static final java.lang.String _NoSeatAssigned = "NoSeatAssigned";
    public static final java.lang.String _SeatNumberChange = "SeatNumberChange";
    public static final java.lang.String _SSRNotSupportedOnNewSeat = "SSRNotSupportedOnNewSeat";
    public static final java.lang.String _FewerSeatPreferencesMetOnNewSeat = "FewerSeatPreferencesMetOnNewSeat";
    public static final java.lang.String _AOSUnableToConfirmCancel = "AOSUnableToConfirmCancel";
    public static final java.lang.String _ETicketIssue = "ETicketIssue";
    public static final java.lang.String _ETicketFollowup = "ETicketFollowup";
    public static final java.lang.String _InvoluntaryFlyAhead = "InvoluntaryFlyAhead";
    public static final java.lang.String _ManualClearanceOnOutage = "ManualClearanceOnOutage";
    public static final java.lang.String _UnbalancedPoints = "UnbalancedPoints";
    public static final java.lang.String _VoluntaryFlightChange = "VoluntaryFlightChange";
    public static final java.lang.String _InvoluntaryFlightChange = "InvoluntaryFlightChange";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final QueueEventType Default = new QueueEventType(_Default);
    public static final QueueEventType BookingBalanceDue = new QueueEventType(_BookingBalanceDue);
    public static final QueueEventType BookingNegativeBalance = new QueueEventType(_BookingNegativeBalance);
    public static final QueueEventType BookingCustomerComment = new QueueEventType(_BookingCustomerComment);
    public static final QueueEventType DeclinedPaymentInitial = new QueueEventType(_DeclinedPaymentInitial);
    public static final QueueEventType DeclinedPaymentChange = new QueueEventType(_DeclinedPaymentChange);
    public static final QueueEventType FareOverride = new QueueEventType(_FareOverride);
    public static final QueueEventType ScheduleTimeChange = new QueueEventType(_ScheduleTimeChange);
    public static final QueueEventType ScheduleTimeChangeMisconnect = new QueueEventType(_ScheduleTimeChangeMisconnect);
    public static final QueueEventType ScheduleCancellation = new QueueEventType(_ScheduleCancellation);
    public static final QueueEventType FlightDesignatorChange = new QueueEventType(_FlightDesignatorChange);
    public static final QueueEventType ReaccommodationMove = new QueueEventType(_ReaccommodationMove);
    public static final QueueEventType GDSCancelWithPendingPayment = new QueueEventType(_GDSCancelWithPendingPayment);
    public static final QueueEventType InvalidPriceStatusOverride = new QueueEventType(_InvalidPriceStatusOverride);
    public static final QueueEventType FareRestrictionOverride = new QueueEventType(_FareRestrictionOverride);
    public static final QueueEventType HeldBookings = new QueueEventType(_HeldBookings);
    public static final QueueEventType InvalidPriceStatus = new QueueEventType(_InvalidPriceStatus);
    public static final QueueEventType Watchlist = new QueueEventType(_Watchlist);
    public static final QueueEventType NonFlightServiceFee = new QueueEventType(_NonFlightServiceFee);
    public static final QueueEventType NotAllTicketNumbersReceived = new QueueEventType(_NotAllTicketNumbersReceived);
    public static final QueueEventType BookingSegmentOversold = new QueueEventType(_BookingSegmentOversold);
    public static final QueueEventType ReaccommodationCancel = new QueueEventType(_ReaccommodationCancel);
    public static final QueueEventType ExternalSSRAutoConfirmed = new QueueEventType(_ExternalSSRAutoConfirmed);
    public static final QueueEventType OpCarrierSegUpdate = new QueueEventType(_OpCarrierSegUpdate);
    public static final QueueEventType OpCarrierSSRUpdate = new QueueEventType(_OpCarrierSSRUpdate);
    public static final QueueEventType OpCarrierOtherUpdate = new QueueEventType(_OpCarrierOtherUpdate);
    public static final QueueEventType NameChangeNotAllowed = new QueueEventType(_NameChangeNotAllowed);
    public static final QueueEventType InboundASCNotProcessed = new QueueEventType(_InboundASCNotProcessed);
    public static final QueueEventType OpCarrierInformationChange = new QueueEventType(_OpCarrierInformationChange);
    public static final QueueEventType BookingComponentUpdate = new QueueEventType(_BookingComponentUpdate);
    public static final QueueEventType GroupBookings = new QueueEventType(_GroupBookings);
    public static final QueueEventType BankDirectPNROutOfBalance = new QueueEventType(_BankDirectPNROutOfBalance);
    public static final QueueEventType NoSeatAssigned = new QueueEventType(_NoSeatAssigned);
    public static final QueueEventType SeatNumberChange = new QueueEventType(_SeatNumberChange);
    public static final QueueEventType SSRNotSupportedOnNewSeat = new QueueEventType(_SSRNotSupportedOnNewSeat);
    public static final QueueEventType FewerSeatPreferencesMetOnNewSeat = new QueueEventType(_FewerSeatPreferencesMetOnNewSeat);
    public static final QueueEventType AOSUnableToConfirmCancel = new QueueEventType(_AOSUnableToConfirmCancel);
    public static final QueueEventType ETicketIssue = new QueueEventType(_ETicketIssue);
    public static final QueueEventType ETicketFollowup = new QueueEventType(_ETicketFollowup);
    public static final QueueEventType InvoluntaryFlyAhead = new QueueEventType(_InvoluntaryFlyAhead);
    public static final QueueEventType ManualClearanceOnOutage = new QueueEventType(_ManualClearanceOnOutage);
    public static final QueueEventType UnbalancedPoints = new QueueEventType(_UnbalancedPoints);
    public static final QueueEventType VoluntaryFlightChange = new QueueEventType(_VoluntaryFlightChange);
    public static final QueueEventType InvoluntaryFlightChange = new QueueEventType(_InvoluntaryFlightChange);
    public static final QueueEventType Unmapped = new QueueEventType(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static QueueEventType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        QueueEventType enumeration = (QueueEventType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static QueueEventType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QueueEventType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "QueueEventType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
