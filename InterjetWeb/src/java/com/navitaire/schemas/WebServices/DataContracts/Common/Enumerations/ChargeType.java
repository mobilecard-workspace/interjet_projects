/**
 * ChargeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class ChargeType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ChargeType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _FarePrice = "FarePrice";
    public static final java.lang.String _Discount = "Discount";
    public static final java.lang.String _IncludedTravelFee = "IncludedTravelFee";
    public static final java.lang.String _IncludedTax = "IncludedTax";
    public static final java.lang.String _TravelFee = "TravelFee";
    public static final java.lang.String _Tax = "Tax";
    public static final java.lang.String _ServiceCharge = "ServiceCharge";
    public static final java.lang.String _PromotionDiscount = "PromotionDiscount";
    public static final java.lang.String _ConnectionAdjustmentAmount = "ConnectionAdjustmentAmount";
    public static final java.lang.String _AddOnServicePrice = "AddOnServicePrice";
    public static final java.lang.String _IncludedAddOnServiceFee = "IncludedAddOnServiceFee";
    public static final java.lang.String _AddOnServiceFee = "AddOnServiceFee";
    public static final java.lang.String _Calculated = "Calculated";
    public static final java.lang.String _Note = "Note";
    public static final java.lang.String _AddOnServiceMarkup = "AddOnServiceMarkup";
    public static final java.lang.String _FareSurcharge = "FareSurcharge";
    public static final java.lang.String _Loyalty = "Loyalty";
    public static final java.lang.String _FarePoints = "FarePoints";
    public static final java.lang.String _DiscountPoints = "DiscountPoints";
    public static final java.lang.String _AddOnServiceCancelFee = "AddOnServiceCancelFee";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final ChargeType FarePrice = new ChargeType(_FarePrice);
    public static final ChargeType Discount = new ChargeType(_Discount);
    public static final ChargeType IncludedTravelFee = new ChargeType(_IncludedTravelFee);
    public static final ChargeType IncludedTax = new ChargeType(_IncludedTax);
    public static final ChargeType TravelFee = new ChargeType(_TravelFee);
    public static final ChargeType Tax = new ChargeType(_Tax);
    public static final ChargeType ServiceCharge = new ChargeType(_ServiceCharge);
    public static final ChargeType PromotionDiscount = new ChargeType(_PromotionDiscount);
    public static final ChargeType ConnectionAdjustmentAmount = new ChargeType(_ConnectionAdjustmentAmount);
    public static final ChargeType AddOnServicePrice = new ChargeType(_AddOnServicePrice);
    public static final ChargeType IncludedAddOnServiceFee = new ChargeType(_IncludedAddOnServiceFee);
    public static final ChargeType AddOnServiceFee = new ChargeType(_AddOnServiceFee);
    public static final ChargeType Calculated = new ChargeType(_Calculated);
    public static final ChargeType Note = new ChargeType(_Note);
    public static final ChargeType AddOnServiceMarkup = new ChargeType(_AddOnServiceMarkup);
    public static final ChargeType FareSurcharge = new ChargeType(_FareSurcharge);
    public static final ChargeType Loyalty = new ChargeType(_Loyalty);
    public static final ChargeType FarePoints = new ChargeType(_FarePoints);
    public static final ChargeType DiscountPoints = new ChargeType(_DiscountPoints);
    public static final ChargeType AddOnServiceCancelFee = new ChargeType(_AddOnServiceCancelFee);
    public static final ChargeType Unmapped = new ChargeType(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static ChargeType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ChargeType enumeration = (ChargeType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ChargeType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChargeType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChargeType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
