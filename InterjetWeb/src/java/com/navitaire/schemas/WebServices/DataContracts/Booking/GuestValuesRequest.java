/**
 * GuestValuesRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GuestValuesRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingBy bookingBy;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByRecordLocator bookingByRecordLocator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByBookingID bookingByID;

    private short[] journeyNumberList;

    public GuestValuesRequest() {
    }

    public GuestValuesRequest(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingBy bookingBy,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByRecordLocator bookingByRecordLocator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByBookingID bookingByID,
           short[] journeyNumberList) {
           this.bookingBy = bookingBy;
           this.bookingByRecordLocator = bookingByRecordLocator;
           this.bookingByID = bookingByID;
           this.journeyNumberList = journeyNumberList;
    }


    /**
     * Gets the bookingBy value for this GuestValuesRequest.
     * 
     * @return bookingBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingBy getBookingBy() {
        return bookingBy;
    }


    /**
     * Sets the bookingBy value for this GuestValuesRequest.
     * 
     * @param bookingBy
     */
    public void setBookingBy(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingBy bookingBy) {
        this.bookingBy = bookingBy;
    }


    /**
     * Gets the bookingByRecordLocator value for this GuestValuesRequest.
     * 
     * @return bookingByRecordLocator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByRecordLocator getBookingByRecordLocator() {
        return bookingByRecordLocator;
    }


    /**
     * Sets the bookingByRecordLocator value for this GuestValuesRequest.
     * 
     * @param bookingByRecordLocator
     */
    public void setBookingByRecordLocator(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByRecordLocator bookingByRecordLocator) {
        this.bookingByRecordLocator = bookingByRecordLocator;
    }


    /**
     * Gets the bookingByID value for this GuestValuesRequest.
     * 
     * @return bookingByID
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByBookingID getBookingByID() {
        return bookingByID;
    }


    /**
     * Sets the bookingByID value for this GuestValuesRequest.
     * 
     * @param bookingByID
     */
    public void setBookingByID(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByBookingID bookingByID) {
        this.bookingByID = bookingByID;
    }


    /**
     * Gets the journeyNumberList value for this GuestValuesRequest.
     * 
     * @return journeyNumberList
     */
    public short[] getJourneyNumberList() {
        return journeyNumberList;
    }


    /**
     * Sets the journeyNumberList value for this GuestValuesRequest.
     * 
     * @param journeyNumberList
     */
    public void setJourneyNumberList(short[] journeyNumberList) {
        this.journeyNumberList = journeyNumberList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GuestValuesRequest)) return false;
        GuestValuesRequest other = (GuestValuesRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingBy==null && other.getBookingBy()==null) || 
             (this.bookingBy!=null &&
              this.bookingBy.equals(other.getBookingBy()))) &&
            ((this.bookingByRecordLocator==null && other.getBookingByRecordLocator()==null) || 
             (this.bookingByRecordLocator!=null &&
              this.bookingByRecordLocator.equals(other.getBookingByRecordLocator()))) &&
            ((this.bookingByID==null && other.getBookingByID()==null) || 
             (this.bookingByID!=null &&
              this.bookingByID.equals(other.getBookingByID()))) &&
            ((this.journeyNumberList==null && other.getJourneyNumberList()==null) || 
             (this.journeyNumberList!=null &&
              java.util.Arrays.equals(this.journeyNumberList, other.getJourneyNumberList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingBy() != null) {
            _hashCode += getBookingBy().hashCode();
        }
        if (getBookingByRecordLocator() != null) {
            _hashCode += getBookingByRecordLocator().hashCode();
        }
        if (getBookingByID() != null) {
            _hashCode += getBookingByID().hashCode();
        }
        if (getJourneyNumberList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJourneyNumberList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJourneyNumberList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GuestValuesRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GuestValuesRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingBy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingByRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingByRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingByRecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingByID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingByID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingByBookingID"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeyNumberList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneyNumberList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "short"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
