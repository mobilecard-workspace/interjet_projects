/**
 * SeatPreference.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class SeatPreference implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SeatPreference(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _None = "None";
    public static final java.lang.String _Window = "Window";
    public static final java.lang.String _Aisle = "Aisle";
    public static final java.lang.String _NoPreference = "NoPreference";
    public static final java.lang.String _Front = "Front";
    public static final java.lang.String _Rear = "Rear";
    public static final java.lang.String _WindowFront = "WindowFront";
    public static final java.lang.String _WindowRear = "WindowRear";
    public static final java.lang.String _AisleFront = "AisleFront";
    public static final java.lang.String _AisleRear = "AisleRear";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final SeatPreference None = new SeatPreference(_None);
    public static final SeatPreference Window = new SeatPreference(_Window);
    public static final SeatPreference Aisle = new SeatPreference(_Aisle);
    public static final SeatPreference NoPreference = new SeatPreference(_NoPreference);
    public static final SeatPreference Front = new SeatPreference(_Front);
    public static final SeatPreference Rear = new SeatPreference(_Rear);
    public static final SeatPreference WindowFront = new SeatPreference(_WindowFront);
    public static final SeatPreference WindowRear = new SeatPreference(_WindowRear);
    public static final SeatPreference AisleFront = new SeatPreference(_AisleFront);
    public static final SeatPreference AisleRear = new SeatPreference(_AisleRear);
    public static final SeatPreference Unmapped = new SeatPreference(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static SeatPreference fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SeatPreference enumeration = (SeatPreference)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SeatPreference fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SeatPreference.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SeatPreference"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
