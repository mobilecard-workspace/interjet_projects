/**
 * BookingComment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingComment  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CommentType commentType;

    private java.lang.String commentText;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale pointOfSale;

    private java.util.Calendar createdDate;

    public BookingComment() {
    }

    public BookingComment(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CommentType commentType,
           java.lang.String commentText,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale pointOfSale,
           java.util.Calendar createdDate) {
        super(
            state);
        this.commentType = commentType;
        this.commentText = commentText;
        this.pointOfSale = pointOfSale;
        this.createdDate = createdDate;
    }


    /**
     * Gets the commentType value for this BookingComment.
     * 
     * @return commentType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CommentType getCommentType() {
        return commentType;
    }


    /**
     * Sets the commentType value for this BookingComment.
     * 
     * @param commentType
     */
    public void setCommentType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CommentType commentType) {
        this.commentType = commentType;
    }


    /**
     * Gets the commentText value for this BookingComment.
     * 
     * @return commentText
     */
    public java.lang.String getCommentText() {
        return commentText;
    }


    /**
     * Sets the commentText value for this BookingComment.
     * 
     * @param commentText
     */
    public void setCommentText(java.lang.String commentText) {
        this.commentText = commentText;
    }


    /**
     * Gets the pointOfSale value for this BookingComment.
     * 
     * @return pointOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getPointOfSale() {
        return pointOfSale;
    }


    /**
     * Sets the pointOfSale value for this BookingComment.
     * 
     * @param pointOfSale
     */
    public void setPointOfSale(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }


    /**
     * Gets the createdDate value for this BookingComment.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this BookingComment.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingComment)) return false;
        BookingComment other = (BookingComment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.commentType==null && other.getCommentType()==null) || 
             (this.commentType!=null &&
              this.commentType.equals(other.getCommentType()))) &&
            ((this.commentText==null && other.getCommentText()==null) || 
             (this.commentText!=null &&
              this.commentText.equals(other.getCommentText()))) &&
            ((this.pointOfSale==null && other.getPointOfSale()==null) || 
             (this.pointOfSale!=null &&
              this.pointOfSale.equals(other.getPointOfSale()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCommentType() != null) {
            _hashCode += getCommentType().hashCode();
        }
        if (getCommentText() != null) {
            _hashCode += getCommentText().hashCode();
        }
        if (getPointOfSale() != null) {
            _hashCode += getPointOfSale().hashCode();
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingComment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CommentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "CommentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CommentText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PointOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
