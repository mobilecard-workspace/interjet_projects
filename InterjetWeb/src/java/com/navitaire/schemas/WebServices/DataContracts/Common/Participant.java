/**
 * Participant.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class Participant  implements java.io.Serializable {
    private java.lang.String participantId;

    private java.lang.Integer participantSequence;

    private java.lang.Boolean primaryFlag;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring docNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring docIssuedBy;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring docTypeCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring title;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring firstName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring middleName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring lastName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address1;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address2;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring city;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring stateCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring zipCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring county;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring countryCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneHome;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring busOrRes;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneFax;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneWork;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring emailAddress;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring companyName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfdateTime birthDate;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring participantTypeCode;

    private java.lang.String participantTypeDescription;

    public Participant() {
    }

    public Participant(
           java.lang.String participantId,
           java.lang.Integer participantSequence,
           java.lang.Boolean primaryFlag,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring docNumber,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring docIssuedBy,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring docTypeCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring title,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring firstName,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring middleName,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring lastName,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address1,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address2,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring city,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring stateCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring zipCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring county,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring countryCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneHome,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring busOrRes,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneFax,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneWork,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring emailAddress,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring companyName,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfdateTime birthDate,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring participantTypeCode,
           java.lang.String participantTypeDescription) {
           this.participantId = participantId;
           this.participantSequence = participantSequence;
           this.primaryFlag = primaryFlag;
           this.docNumber = docNumber;
           this.docIssuedBy = docIssuedBy;
           this.docTypeCode = docTypeCode;
           this.title = title;
           this.firstName = firstName;
           this.middleName = middleName;
           this.lastName = lastName;
           this.address1 = address1;
           this.address2 = address2;
           this.city = city;
           this.stateCode = stateCode;
           this.zipCode = zipCode;
           this.county = county;
           this.countryCode = countryCode;
           this.phoneHome = phoneHome;
           this.busOrRes = busOrRes;
           this.phoneFax = phoneFax;
           this.phoneWork = phoneWork;
           this.emailAddress = emailAddress;
           this.companyName = companyName;
           this.birthDate = birthDate;
           this.participantTypeCode = participantTypeCode;
           this.participantTypeDescription = participantTypeDescription;
    }


    /**
     * Gets the participantId value for this Participant.
     * 
     * @return participantId
     */
    public java.lang.String getParticipantId() {
        return participantId;
    }


    /**
     * Sets the participantId value for this Participant.
     * 
     * @param participantId
     */
    public void setParticipantId(java.lang.String participantId) {
        this.participantId = participantId;
    }


    /**
     * Gets the participantSequence value for this Participant.
     * 
     * @return participantSequence
     */
    public java.lang.Integer getParticipantSequence() {
        return participantSequence;
    }


    /**
     * Sets the participantSequence value for this Participant.
     * 
     * @param participantSequence
     */
    public void setParticipantSequence(java.lang.Integer participantSequence) {
        this.participantSequence = participantSequence;
    }


    /**
     * Gets the primaryFlag value for this Participant.
     * 
     * @return primaryFlag
     */
    public java.lang.Boolean getPrimaryFlag() {
        return primaryFlag;
    }


    /**
     * Sets the primaryFlag value for this Participant.
     * 
     * @param primaryFlag
     */
    public void setPrimaryFlag(java.lang.Boolean primaryFlag) {
        this.primaryFlag = primaryFlag;
    }


    /**
     * Gets the docNumber value for this Participant.
     * 
     * @return docNumber
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getDocNumber() {
        return docNumber;
    }


    /**
     * Sets the docNumber value for this Participant.
     * 
     * @param docNumber
     */
    public void setDocNumber(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring docNumber) {
        this.docNumber = docNumber;
    }


    /**
     * Gets the docIssuedBy value for this Participant.
     * 
     * @return docIssuedBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getDocIssuedBy() {
        return docIssuedBy;
    }


    /**
     * Sets the docIssuedBy value for this Participant.
     * 
     * @param docIssuedBy
     */
    public void setDocIssuedBy(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring docIssuedBy) {
        this.docIssuedBy = docIssuedBy;
    }


    /**
     * Gets the docTypeCode value for this Participant.
     * 
     * @return docTypeCode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getDocTypeCode() {
        return docTypeCode;
    }


    /**
     * Sets the docTypeCode value for this Participant.
     * 
     * @param docTypeCode
     */
    public void setDocTypeCode(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring docTypeCode) {
        this.docTypeCode = docTypeCode;
    }


    /**
     * Gets the title value for this Participant.
     * 
     * @return title
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getTitle() {
        return title;
    }


    /**
     * Sets the title value for this Participant.
     * 
     * @param title
     */
    public void setTitle(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring title) {
        this.title = title;
    }


    /**
     * Gets the firstName value for this Participant.
     * 
     * @return firstName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this Participant.
     * 
     * @param firstName
     */
    public void setFirstName(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the middleName value for this Participant.
     * 
     * @return middleName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getMiddleName() {
        return middleName;
    }


    /**
     * Sets the middleName value for this Participant.
     * 
     * @param middleName
     */
    public void setMiddleName(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring middleName) {
        this.middleName = middleName;
    }


    /**
     * Gets the lastName value for this Participant.
     * 
     * @return lastName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this Participant.
     * 
     * @param lastName
     */
    public void setLastName(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the address1 value for this Participant.
     * 
     * @return address1
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getAddress1() {
        return address1;
    }


    /**
     * Sets the address1 value for this Participant.
     * 
     * @param address1
     */
    public void setAddress1(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address1) {
        this.address1 = address1;
    }


    /**
     * Gets the address2 value for this Participant.
     * 
     * @return address2
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getAddress2() {
        return address2;
    }


    /**
     * Sets the address2 value for this Participant.
     * 
     * @param address2
     */
    public void setAddress2(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address2) {
        this.address2 = address2;
    }


    /**
     * Gets the city value for this Participant.
     * 
     * @return city
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getCity() {
        return city;
    }


    /**
     * Sets the city value for this Participant.
     * 
     * @param city
     */
    public void setCity(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring city) {
        this.city = city;
    }


    /**
     * Gets the stateCode value for this Participant.
     * 
     * @return stateCode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getStateCode() {
        return stateCode;
    }


    /**
     * Sets the stateCode value for this Participant.
     * 
     * @param stateCode
     */
    public void setStateCode(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring stateCode) {
        this.stateCode = stateCode;
    }


    /**
     * Gets the zipCode value for this Participant.
     * 
     * @return zipCode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getZipCode() {
        return zipCode;
    }


    /**
     * Sets the zipCode value for this Participant.
     * 
     * @param zipCode
     */
    public void setZipCode(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring zipCode) {
        this.zipCode = zipCode;
    }


    /**
     * Gets the county value for this Participant.
     * 
     * @return county
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getCounty() {
        return county;
    }


    /**
     * Sets the county value for this Participant.
     * 
     * @param county
     */
    public void setCounty(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring county) {
        this.county = county;
    }


    /**
     * Gets the countryCode value for this Participant.
     * 
     * @return countryCode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this Participant.
     * 
     * @param countryCode
     */
    public void setCountryCode(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * Gets the phoneHome value for this Participant.
     * 
     * @return phoneHome
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getPhoneHome() {
        return phoneHome;
    }


    /**
     * Sets the phoneHome value for this Participant.
     * 
     * @param phoneHome
     */
    public void setPhoneHome(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneHome) {
        this.phoneHome = phoneHome;
    }


    /**
     * Gets the busOrRes value for this Participant.
     * 
     * @return busOrRes
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getBusOrRes() {
        return busOrRes;
    }


    /**
     * Sets the busOrRes value for this Participant.
     * 
     * @param busOrRes
     */
    public void setBusOrRes(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring busOrRes) {
        this.busOrRes = busOrRes;
    }


    /**
     * Gets the phoneFax value for this Participant.
     * 
     * @return phoneFax
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getPhoneFax() {
        return phoneFax;
    }


    /**
     * Sets the phoneFax value for this Participant.
     * 
     * @param phoneFax
     */
    public void setPhoneFax(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneFax) {
        this.phoneFax = phoneFax;
    }


    /**
     * Gets the phoneWork value for this Participant.
     * 
     * @return phoneWork
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getPhoneWork() {
        return phoneWork;
    }


    /**
     * Sets the phoneWork value for this Participant.
     * 
     * @param phoneWork
     */
    public void setPhoneWork(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneWork) {
        this.phoneWork = phoneWork;
    }


    /**
     * Gets the emailAddress value for this Participant.
     * 
     * @return emailAddress
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getEmailAddress() {
        return emailAddress;
    }


    /**
     * Sets the emailAddress value for this Participant.
     * 
     * @param emailAddress
     */
    public void setEmailAddress(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring emailAddress) {
        this.emailAddress = emailAddress;
    }


    /**
     * Gets the companyName value for this Participant.
     * 
     * @return companyName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getCompanyName() {
        return companyName;
    }


    /**
     * Sets the companyName value for this Participant.
     * 
     * @param companyName
     */
    public void setCompanyName(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring companyName) {
        this.companyName = companyName;
    }


    /**
     * Gets the birthDate value for this Participant.
     * 
     * @return birthDate
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfdateTime getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this Participant.
     * 
     * @param birthDate
     */
    public void setBirthDate(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfdateTime birthDate) {
        this.birthDate = birthDate;
    }


    /**
     * Gets the participantTypeCode value for this Participant.
     * 
     * @return participantTypeCode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getParticipantTypeCode() {
        return participantTypeCode;
    }


    /**
     * Sets the participantTypeCode value for this Participant.
     * 
     * @param participantTypeCode
     */
    public void setParticipantTypeCode(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring participantTypeCode) {
        this.participantTypeCode = participantTypeCode;
    }


    /**
     * Gets the participantTypeDescription value for this Participant.
     * 
     * @return participantTypeDescription
     */
    public java.lang.String getParticipantTypeDescription() {
        return participantTypeDescription;
    }


    /**
     * Sets the participantTypeDescription value for this Participant.
     * 
     * @param participantTypeDescription
     */
    public void setParticipantTypeDescription(java.lang.String participantTypeDescription) {
        this.participantTypeDescription = participantTypeDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Participant)) return false;
        Participant other = (Participant) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.participantId==null && other.getParticipantId()==null) || 
             (this.participantId!=null &&
              this.participantId.equals(other.getParticipantId()))) &&
            ((this.participantSequence==null && other.getParticipantSequence()==null) || 
             (this.participantSequence!=null &&
              this.participantSequence.equals(other.getParticipantSequence()))) &&
            ((this.primaryFlag==null && other.getPrimaryFlag()==null) || 
             (this.primaryFlag!=null &&
              this.primaryFlag.equals(other.getPrimaryFlag()))) &&
            ((this.docNumber==null && other.getDocNumber()==null) || 
             (this.docNumber!=null &&
              this.docNumber.equals(other.getDocNumber()))) &&
            ((this.docIssuedBy==null && other.getDocIssuedBy()==null) || 
             (this.docIssuedBy!=null &&
              this.docIssuedBy.equals(other.getDocIssuedBy()))) &&
            ((this.docTypeCode==null && other.getDocTypeCode()==null) || 
             (this.docTypeCode!=null &&
              this.docTypeCode.equals(other.getDocTypeCode()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.middleName==null && other.getMiddleName()==null) || 
             (this.middleName!=null &&
              this.middleName.equals(other.getMiddleName()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.address1==null && other.getAddress1()==null) || 
             (this.address1!=null &&
              this.address1.equals(other.getAddress1()))) &&
            ((this.address2==null && other.getAddress2()==null) || 
             (this.address2!=null &&
              this.address2.equals(other.getAddress2()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.stateCode==null && other.getStateCode()==null) || 
             (this.stateCode!=null &&
              this.stateCode.equals(other.getStateCode()))) &&
            ((this.zipCode==null && other.getZipCode()==null) || 
             (this.zipCode!=null &&
              this.zipCode.equals(other.getZipCode()))) &&
            ((this.county==null && other.getCounty()==null) || 
             (this.county!=null &&
              this.county.equals(other.getCounty()))) &&
            ((this.countryCode==null && other.getCountryCode()==null) || 
             (this.countryCode!=null &&
              this.countryCode.equals(other.getCountryCode()))) &&
            ((this.phoneHome==null && other.getPhoneHome()==null) || 
             (this.phoneHome!=null &&
              this.phoneHome.equals(other.getPhoneHome()))) &&
            ((this.busOrRes==null && other.getBusOrRes()==null) || 
             (this.busOrRes!=null &&
              this.busOrRes.equals(other.getBusOrRes()))) &&
            ((this.phoneFax==null && other.getPhoneFax()==null) || 
             (this.phoneFax!=null &&
              this.phoneFax.equals(other.getPhoneFax()))) &&
            ((this.phoneWork==null && other.getPhoneWork()==null) || 
             (this.phoneWork!=null &&
              this.phoneWork.equals(other.getPhoneWork()))) &&
            ((this.emailAddress==null && other.getEmailAddress()==null) || 
             (this.emailAddress!=null &&
              this.emailAddress.equals(other.getEmailAddress()))) &&
            ((this.companyName==null && other.getCompanyName()==null) || 
             (this.companyName!=null &&
              this.companyName.equals(other.getCompanyName()))) &&
            ((this.birthDate==null && other.getBirthDate()==null) || 
             (this.birthDate!=null &&
              this.birthDate.equals(other.getBirthDate()))) &&
            ((this.participantTypeCode==null && other.getParticipantTypeCode()==null) || 
             (this.participantTypeCode!=null &&
              this.participantTypeCode.equals(other.getParticipantTypeCode()))) &&
            ((this.participantTypeDescription==null && other.getParticipantTypeDescription()==null) || 
             (this.participantTypeDescription!=null &&
              this.participantTypeDescription.equals(other.getParticipantTypeDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getParticipantId() != null) {
            _hashCode += getParticipantId().hashCode();
        }
        if (getParticipantSequence() != null) {
            _hashCode += getParticipantSequence().hashCode();
        }
        if (getPrimaryFlag() != null) {
            _hashCode += getPrimaryFlag().hashCode();
        }
        if (getDocNumber() != null) {
            _hashCode += getDocNumber().hashCode();
        }
        if (getDocIssuedBy() != null) {
            _hashCode += getDocIssuedBy().hashCode();
        }
        if (getDocTypeCode() != null) {
            _hashCode += getDocTypeCode().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getMiddleName() != null) {
            _hashCode += getMiddleName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getAddress1() != null) {
            _hashCode += getAddress1().hashCode();
        }
        if (getAddress2() != null) {
            _hashCode += getAddress2().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getStateCode() != null) {
            _hashCode += getStateCode().hashCode();
        }
        if (getZipCode() != null) {
            _hashCode += getZipCode().hashCode();
        }
        if (getCounty() != null) {
            _hashCode += getCounty().hashCode();
        }
        if (getCountryCode() != null) {
            _hashCode += getCountryCode().hashCode();
        }
        if (getPhoneHome() != null) {
            _hashCode += getPhoneHome().hashCode();
        }
        if (getBusOrRes() != null) {
            _hashCode += getBusOrRes().hashCode();
        }
        if (getPhoneFax() != null) {
            _hashCode += getPhoneFax().hashCode();
        }
        if (getPhoneWork() != null) {
            _hashCode += getPhoneWork().hashCode();
        }
        if (getEmailAddress() != null) {
            _hashCode += getEmailAddress().hashCode();
        }
        if (getCompanyName() != null) {
            _hashCode += getCompanyName().hashCode();
        }
        if (getBirthDate() != null) {
            _hashCode += getBirthDate().hashCode();
        }
        if (getParticipantTypeCode() != null) {
            _hashCode += getParticipantTypeCode().hashCode();
        }
        if (getParticipantTypeDescription() != null) {
            _hashCode += getParticipantTypeDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Participant.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Participant"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("participantId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ParticipantId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("participantSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ParticipantSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PrimaryFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("docNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DocNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("docIssuedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DocIssuedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("docTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DocTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("middleName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "MiddleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "LastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Address1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Address2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "StateCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("county");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "County"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneHome");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PhoneHome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("busOrRes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "BusOrRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneFax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PhoneFax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneWork");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PhoneWork"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "EmailAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CompanyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "BirthDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfdateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("participantTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ParticipantTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("participantTypeDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ParticipantTypeDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
