/**
 * Fare.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class Fare  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String classOfService;

    private java.lang.String classType;

    private java.lang.String ruleTariff;

    private java.lang.String carrierCode;

    private java.lang.String ruleNumber;

    private java.lang.String fareBasisCode;

    private java.lang.Short fareSequence;

    private java.lang.String fareClassOfService;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareStatus fareStatus;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareApplicationType fareApplicationType;

    private java.lang.String originalClassOfService;

    private java.lang.String xrefClassOfService;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxFare[] paxFares;

    private java.lang.String productClass;

    private java.lang.Boolean isAllotmentMarketFare;

    private java.lang.String travelClassCode;

    private java.lang.String fareSellKey;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound inboundOutbound;

    public Fare() {
    }

    public Fare(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String classOfService,
           java.lang.String classType,
           java.lang.String ruleTariff,
           java.lang.String carrierCode,
           java.lang.String ruleNumber,
           java.lang.String fareBasisCode,
           java.lang.Short fareSequence,
           java.lang.String fareClassOfService,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareStatus fareStatus,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareApplicationType fareApplicationType,
           java.lang.String originalClassOfService,
           java.lang.String xrefClassOfService,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxFare[] paxFares,
           java.lang.String productClass,
           java.lang.Boolean isAllotmentMarketFare,
           java.lang.String travelClassCode,
           java.lang.String fareSellKey,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound inboundOutbound) {
        super(
            state);
        this.classOfService = classOfService;
        this.classType = classType;
        this.ruleTariff = ruleTariff;
        this.carrierCode = carrierCode;
        this.ruleNumber = ruleNumber;
        this.fareBasisCode = fareBasisCode;
        this.fareSequence = fareSequence;
        this.fareClassOfService = fareClassOfService;
        this.fareStatus = fareStatus;
        this.fareApplicationType = fareApplicationType;
        this.originalClassOfService = originalClassOfService;
        this.xrefClassOfService = xrefClassOfService;
        this.paxFares = paxFares;
        this.productClass = productClass;
        this.isAllotmentMarketFare = isAllotmentMarketFare;
        this.travelClassCode = travelClassCode;
        this.fareSellKey = fareSellKey;
        this.inboundOutbound = inboundOutbound;
    }


    /**
     * Gets the classOfService value for this Fare.
     * 
     * @return classOfService
     */
    public java.lang.String getClassOfService() {
        return classOfService;
    }


    /**
     * Sets the classOfService value for this Fare.
     * 
     * @param classOfService
     */
    public void setClassOfService(java.lang.String classOfService) {
        this.classOfService = classOfService;
    }


    /**
     * Gets the classType value for this Fare.
     * 
     * @return classType
     */
    public java.lang.String getClassType() {
        return classType;
    }


    /**
     * Sets the classType value for this Fare.
     * 
     * @param classType
     */
    public void setClassType(java.lang.String classType) {
        this.classType = classType;
    }


    /**
     * Gets the ruleTariff value for this Fare.
     * 
     * @return ruleTariff
     */
    public java.lang.String getRuleTariff() {
        return ruleTariff;
    }


    /**
     * Sets the ruleTariff value for this Fare.
     * 
     * @param ruleTariff
     */
    public void setRuleTariff(java.lang.String ruleTariff) {
        this.ruleTariff = ruleTariff;
    }


    /**
     * Gets the carrierCode value for this Fare.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this Fare.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the ruleNumber value for this Fare.
     * 
     * @return ruleNumber
     */
    public java.lang.String getRuleNumber() {
        return ruleNumber;
    }


    /**
     * Sets the ruleNumber value for this Fare.
     * 
     * @param ruleNumber
     */
    public void setRuleNumber(java.lang.String ruleNumber) {
        this.ruleNumber = ruleNumber;
    }


    /**
     * Gets the fareBasisCode value for this Fare.
     * 
     * @return fareBasisCode
     */
    public java.lang.String getFareBasisCode() {
        return fareBasisCode;
    }


    /**
     * Sets the fareBasisCode value for this Fare.
     * 
     * @param fareBasisCode
     */
    public void setFareBasisCode(java.lang.String fareBasisCode) {
        this.fareBasisCode = fareBasisCode;
    }


    /**
     * Gets the fareSequence value for this Fare.
     * 
     * @return fareSequence
     */
    public java.lang.Short getFareSequence() {
        return fareSequence;
    }


    /**
     * Sets the fareSequence value for this Fare.
     * 
     * @param fareSequence
     */
    public void setFareSequence(java.lang.Short fareSequence) {
        this.fareSequence = fareSequence;
    }


    /**
     * Gets the fareClassOfService value for this Fare.
     * 
     * @return fareClassOfService
     */
    public java.lang.String getFareClassOfService() {
        return fareClassOfService;
    }


    /**
     * Sets the fareClassOfService value for this Fare.
     * 
     * @param fareClassOfService
     */
    public void setFareClassOfService(java.lang.String fareClassOfService) {
        this.fareClassOfService = fareClassOfService;
    }


    /**
     * Gets the fareStatus value for this Fare.
     * 
     * @return fareStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareStatus getFareStatus() {
        return fareStatus;
    }


    /**
     * Sets the fareStatus value for this Fare.
     * 
     * @param fareStatus
     */
    public void setFareStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareStatus fareStatus) {
        this.fareStatus = fareStatus;
    }


    /**
     * Gets the fareApplicationType value for this Fare.
     * 
     * @return fareApplicationType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareApplicationType getFareApplicationType() {
        return fareApplicationType;
    }


    /**
     * Sets the fareApplicationType value for this Fare.
     * 
     * @param fareApplicationType
     */
    public void setFareApplicationType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareApplicationType fareApplicationType) {
        this.fareApplicationType = fareApplicationType;
    }


    /**
     * Gets the originalClassOfService value for this Fare.
     * 
     * @return originalClassOfService
     */
    public java.lang.String getOriginalClassOfService() {
        return originalClassOfService;
    }


    /**
     * Sets the originalClassOfService value for this Fare.
     * 
     * @param originalClassOfService
     */
    public void setOriginalClassOfService(java.lang.String originalClassOfService) {
        this.originalClassOfService = originalClassOfService;
    }


    /**
     * Gets the xrefClassOfService value for this Fare.
     * 
     * @return xrefClassOfService
     */
    public java.lang.String getXrefClassOfService() {
        return xrefClassOfService;
    }


    /**
     * Sets the xrefClassOfService value for this Fare.
     * 
     * @param xrefClassOfService
     */
    public void setXrefClassOfService(java.lang.String xrefClassOfService) {
        this.xrefClassOfService = xrefClassOfService;
    }


    /**
     * Gets the paxFares value for this Fare.
     * 
     * @return paxFares
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxFare[] getPaxFares() {
        return paxFares;
    }


    /**
     * Sets the paxFares value for this Fare.
     * 
     * @param paxFares
     */
    public void setPaxFares(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxFare[] paxFares) {
        this.paxFares = paxFares;
    }


    /**
     * Gets the productClass value for this Fare.
     * 
     * @return productClass
     */
    public java.lang.String getProductClass() {
        return productClass;
    }


    /**
     * Sets the productClass value for this Fare.
     * 
     * @param productClass
     */
    public void setProductClass(java.lang.String productClass) {
        this.productClass = productClass;
    }


    /**
     * Gets the isAllotmentMarketFare value for this Fare.
     * 
     * @return isAllotmentMarketFare
     */
    public java.lang.Boolean getIsAllotmentMarketFare() {
        return isAllotmentMarketFare;
    }


    /**
     * Sets the isAllotmentMarketFare value for this Fare.
     * 
     * @param isAllotmentMarketFare
     */
    public void setIsAllotmentMarketFare(java.lang.Boolean isAllotmentMarketFare) {
        this.isAllotmentMarketFare = isAllotmentMarketFare;
    }


    /**
     * Gets the travelClassCode value for this Fare.
     * 
     * @return travelClassCode
     */
    public java.lang.String getTravelClassCode() {
        return travelClassCode;
    }


    /**
     * Sets the travelClassCode value for this Fare.
     * 
     * @param travelClassCode
     */
    public void setTravelClassCode(java.lang.String travelClassCode) {
        this.travelClassCode = travelClassCode;
    }


    /**
     * Gets the fareSellKey value for this Fare.
     * 
     * @return fareSellKey
     */
    public java.lang.String getFareSellKey() {
        return fareSellKey;
    }


    /**
     * Sets the fareSellKey value for this Fare.
     * 
     * @param fareSellKey
     */
    public void setFareSellKey(java.lang.String fareSellKey) {
        this.fareSellKey = fareSellKey;
    }


    /**
     * Gets the inboundOutbound value for this Fare.
     * 
     * @return inboundOutbound
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound getInboundOutbound() {
        return inboundOutbound;
    }


    /**
     * Sets the inboundOutbound value for this Fare.
     * 
     * @param inboundOutbound
     */
    public void setInboundOutbound(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound inboundOutbound) {
        this.inboundOutbound = inboundOutbound;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Fare)) return false;
        Fare other = (Fare) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.classOfService==null && other.getClassOfService()==null) || 
             (this.classOfService!=null &&
              this.classOfService.equals(other.getClassOfService()))) &&
            ((this.classType==null && other.getClassType()==null) || 
             (this.classType!=null &&
              this.classType.equals(other.getClassType()))) &&
            ((this.ruleTariff==null && other.getRuleTariff()==null) || 
             (this.ruleTariff!=null &&
              this.ruleTariff.equals(other.getRuleTariff()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.ruleNumber==null && other.getRuleNumber()==null) || 
             (this.ruleNumber!=null &&
              this.ruleNumber.equals(other.getRuleNumber()))) &&
            ((this.fareBasisCode==null && other.getFareBasisCode()==null) || 
             (this.fareBasisCode!=null &&
              this.fareBasisCode.equals(other.getFareBasisCode()))) &&
            ((this.fareSequence==null && other.getFareSequence()==null) || 
             (this.fareSequence!=null &&
              this.fareSequence.equals(other.getFareSequence()))) &&
            ((this.fareClassOfService==null && other.getFareClassOfService()==null) || 
             (this.fareClassOfService!=null &&
              this.fareClassOfService.equals(other.getFareClassOfService()))) &&
            ((this.fareStatus==null && other.getFareStatus()==null) || 
             (this.fareStatus!=null &&
              this.fareStatus.equals(other.getFareStatus()))) &&
            ((this.fareApplicationType==null && other.getFareApplicationType()==null) || 
             (this.fareApplicationType!=null &&
              this.fareApplicationType.equals(other.getFareApplicationType()))) &&
            ((this.originalClassOfService==null && other.getOriginalClassOfService()==null) || 
             (this.originalClassOfService!=null &&
              this.originalClassOfService.equals(other.getOriginalClassOfService()))) &&
            ((this.xrefClassOfService==null && other.getXrefClassOfService()==null) || 
             (this.xrefClassOfService!=null &&
              this.xrefClassOfService.equals(other.getXrefClassOfService()))) &&
            ((this.paxFares==null && other.getPaxFares()==null) || 
             (this.paxFares!=null &&
              java.util.Arrays.equals(this.paxFares, other.getPaxFares()))) &&
            ((this.productClass==null && other.getProductClass()==null) || 
             (this.productClass!=null &&
              this.productClass.equals(other.getProductClass()))) &&
            ((this.isAllotmentMarketFare==null && other.getIsAllotmentMarketFare()==null) || 
             (this.isAllotmentMarketFare!=null &&
              this.isAllotmentMarketFare.equals(other.getIsAllotmentMarketFare()))) &&
            ((this.travelClassCode==null && other.getTravelClassCode()==null) || 
             (this.travelClassCode!=null &&
              this.travelClassCode.equals(other.getTravelClassCode()))) &&
            ((this.fareSellKey==null && other.getFareSellKey()==null) || 
             (this.fareSellKey!=null &&
              this.fareSellKey.equals(other.getFareSellKey()))) &&
            ((this.inboundOutbound==null && other.getInboundOutbound()==null) || 
             (this.inboundOutbound!=null &&
              this.inboundOutbound.equals(other.getInboundOutbound())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getClassOfService() != null) {
            _hashCode += getClassOfService().hashCode();
        }
        if (getClassType() != null) {
            _hashCode += getClassType().hashCode();
        }
        if (getRuleTariff() != null) {
            _hashCode += getRuleTariff().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getRuleNumber() != null) {
            _hashCode += getRuleNumber().hashCode();
        }
        if (getFareBasisCode() != null) {
            _hashCode += getFareBasisCode().hashCode();
        }
        if (getFareSequence() != null) {
            _hashCode += getFareSequence().hashCode();
        }
        if (getFareClassOfService() != null) {
            _hashCode += getFareClassOfService().hashCode();
        }
        if (getFareStatus() != null) {
            _hashCode += getFareStatus().hashCode();
        }
        if (getFareApplicationType() != null) {
            _hashCode += getFareApplicationType().hashCode();
        }
        if (getOriginalClassOfService() != null) {
            _hashCode += getOriginalClassOfService().hashCode();
        }
        if (getXrefClassOfService() != null) {
            _hashCode += getXrefClassOfService().hashCode();
        }
        if (getPaxFares() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxFares());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxFares(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProductClass() != null) {
            _hashCode += getProductClass().hashCode();
        }
        if (getIsAllotmentMarketFare() != null) {
            _hashCode += getIsAllotmentMarketFare().hashCode();
        }
        if (getTravelClassCode() != null) {
            _hashCode += getTravelClassCode().hashCode();
        }
        if (getFareSellKey() != null) {
            _hashCode += getFareSellKey().hashCode();
        }
        if (getInboundOutbound() != null) {
            _hashCode += getInboundOutbound().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Fare.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Fare"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleTariff");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RuleTariff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RuleNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareBasisCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareBasisCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareClassOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareClassOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FareStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareApplicationType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareApplicationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FareApplicationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalClassOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OriginalClassOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xrefClassOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "XrefClassOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxFares");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxFares"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxFare"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxFare"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productClass");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProductClass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isAllotmentMarketFare");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IsAllotmentMarketFare"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("travelClassCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelClassCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareSellKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareSellKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inboundOutbound");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InboundOutbound"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "InboundOutbound"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
