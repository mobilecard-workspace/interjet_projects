/**
 * GetByID.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GetByID  implements java.io.Serializable {
    private java.lang.Long bookingID;

    private java.lang.Boolean retrieveFromArchive;

    public GetByID() {
    }

    public GetByID(
           java.lang.Long bookingID,
           java.lang.Boolean retrieveFromArchive) {
           this.bookingID = bookingID;
           this.retrieveFromArchive = retrieveFromArchive;
    }


    /**
     * Gets the bookingID value for this GetByID.
     * 
     * @return bookingID
     */
    public java.lang.Long getBookingID() {
        return bookingID;
    }


    /**
     * Sets the bookingID value for this GetByID.
     * 
     * @param bookingID
     */
    public void setBookingID(java.lang.Long bookingID) {
        this.bookingID = bookingID;
    }


    /**
     * Gets the retrieveFromArchive value for this GetByID.
     * 
     * @return retrieveFromArchive
     */
    public java.lang.Boolean getRetrieveFromArchive() {
        return retrieveFromArchive;
    }


    /**
     * Sets the retrieveFromArchive value for this GetByID.
     * 
     * @param retrieveFromArchive
     */
    public void setRetrieveFromArchive(java.lang.Boolean retrieveFromArchive) {
        this.retrieveFromArchive = retrieveFromArchive;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetByID)) return false;
        GetByID other = (GetByID) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingID==null && other.getBookingID()==null) || 
             (this.bookingID!=null &&
              this.bookingID.equals(other.getBookingID()))) &&
            ((this.retrieveFromArchive==null && other.getRetrieveFromArchive()==null) || 
             (this.retrieveFromArchive!=null &&
              this.retrieveFromArchive.equals(other.getRetrieveFromArchive())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingID() != null) {
            _hashCode += getBookingID().hashCode();
        }
        if (getRetrieveFromArchive() != null) {
            _hashCode += getRetrieveFromArchive().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetByID.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByID"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retrieveFromArchive");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RetrieveFromArchive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
