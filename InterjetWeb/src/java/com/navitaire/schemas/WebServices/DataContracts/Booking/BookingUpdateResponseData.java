/**
 * BookingUpdateResponseData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingUpdateResponseData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.Success success;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Warning warning;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Error error;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInformations;

    public BookingUpdateResponseData() {
    }

    public BookingUpdateResponseData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.Success success,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Warning warning,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Error error,
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInformations) {
           this.success = success;
           this.warning = warning;
           this.error = error;
           this.otherServiceInformations = otherServiceInformations;
    }


    /**
     * Gets the success value for this BookingUpdateResponseData.
     * 
     * @return success
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Success getSuccess() {
        return success;
    }


    /**
     * Sets the success value for this BookingUpdateResponseData.
     * 
     * @param success
     */
    public void setSuccess(com.navitaire.schemas.WebServices.DataContracts.Booking.Success success) {
        this.success = success;
    }


    /**
     * Gets the warning value for this BookingUpdateResponseData.
     * 
     * @return warning
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Warning getWarning() {
        return warning;
    }


    /**
     * Sets the warning value for this BookingUpdateResponseData.
     * 
     * @param warning
     */
    public void setWarning(com.navitaire.schemas.WebServices.DataContracts.Booking.Warning warning) {
        this.warning = warning;
    }


    /**
     * Gets the error value for this BookingUpdateResponseData.
     * 
     * @return error
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Error getError() {
        return error;
    }


    /**
     * Sets the error value for this BookingUpdateResponseData.
     * 
     * @param error
     */
    public void setError(com.navitaire.schemas.WebServices.DataContracts.Booking.Error error) {
        this.error = error;
    }


    /**
     * Gets the otherServiceInformations value for this BookingUpdateResponseData.
     * 
     * @return otherServiceInformations
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] getOtherServiceInformations() {
        return otherServiceInformations;
    }


    /**
     * Sets the otherServiceInformations value for this BookingUpdateResponseData.
     * 
     * @param otherServiceInformations
     */
    public void setOtherServiceInformations(com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInformations) {
        this.otherServiceInformations = otherServiceInformations;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingUpdateResponseData)) return false;
        BookingUpdateResponseData other = (BookingUpdateResponseData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.success==null && other.getSuccess()==null) || 
             (this.success!=null &&
              this.success.equals(other.getSuccess()))) &&
            ((this.warning==null && other.getWarning()==null) || 
             (this.warning!=null &&
              this.warning.equals(other.getWarning()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.otherServiceInformations==null && other.getOtherServiceInformations()==null) || 
             (this.otherServiceInformations!=null &&
              java.util.Arrays.equals(this.otherServiceInformations, other.getOtherServiceInformations())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSuccess() != null) {
            _hashCode += getSuccess().hashCode();
        }
        if (getWarning() != null) {
            _hashCode += getWarning().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getOtherServiceInformations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOtherServiceInformations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOtherServiceInformations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingUpdateResponseData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingUpdateResponseData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("success");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Success"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Success"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("warning");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Warning"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Warning"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Error"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherServiceInformations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OtherServiceInformations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OtherServiceInformation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OtherServiceInformation"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
