/**
 * MoveFeePriceRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class MoveFeePriceRequest  implements java.io.Serializable {
    private java.lang.String collectedCurrencyCode;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Journey fromJourney;

    public MoveFeePriceRequest() {
    }

    public MoveFeePriceRequest(
           java.lang.String collectedCurrencyCode,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Journey fromJourney) {
           this.collectedCurrencyCode = collectedCurrencyCode;
           this.fromJourney = fromJourney;
    }


    /**
     * Gets the collectedCurrencyCode value for this MoveFeePriceRequest.
     * 
     * @return collectedCurrencyCode
     */
    public java.lang.String getCollectedCurrencyCode() {
        return collectedCurrencyCode;
    }


    /**
     * Sets the collectedCurrencyCode value for this MoveFeePriceRequest.
     * 
     * @param collectedCurrencyCode
     */
    public void setCollectedCurrencyCode(java.lang.String collectedCurrencyCode) {
        this.collectedCurrencyCode = collectedCurrencyCode;
    }


    /**
     * Gets the fromJourney value for this MoveFeePriceRequest.
     * 
     * @return fromJourney
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Journey getFromJourney() {
        return fromJourney;
    }


    /**
     * Sets the fromJourney value for this MoveFeePriceRequest.
     * 
     * @param fromJourney
     */
    public void setFromJourney(com.navitaire.schemas.WebServices.DataContracts.Booking.Journey fromJourney) {
        this.fromJourney = fromJourney;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MoveFeePriceRequest)) return false;
        MoveFeePriceRequest other = (MoveFeePriceRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.collectedCurrencyCode==null && other.getCollectedCurrencyCode()==null) || 
             (this.collectedCurrencyCode!=null &&
              this.collectedCurrencyCode.equals(other.getCollectedCurrencyCode()))) &&
            ((this.fromJourney==null && other.getFromJourney()==null) || 
             (this.fromJourney!=null &&
              this.fromJourney.equals(other.getFromJourney())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCollectedCurrencyCode() != null) {
            _hashCode += getCollectedCurrencyCode().hashCode();
        }
        if (getFromJourney() != null) {
            _hashCode += getFromJourney().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MoveFeePriceRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromJourney");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FromJourney"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
