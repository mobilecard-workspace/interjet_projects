/**
 * GetBookingBaggageRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GetBookingBaggageRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator;

    private java.lang.Boolean getFromCurrentState;

    public GetBookingBaggageRequestData() {
    }

    public GetBookingBaggageRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator,
           java.lang.Boolean getFromCurrentState) {
           this.getByRecordLocator = getByRecordLocator;
           this.getFromCurrentState = getFromCurrentState;
    }


    /**
     * Gets the getByRecordLocator value for this GetBookingBaggageRequestData.
     * 
     * @return getByRecordLocator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getGetByRecordLocator() {
        return getByRecordLocator;
    }


    /**
     * Sets the getByRecordLocator value for this GetBookingBaggageRequestData.
     * 
     * @param getByRecordLocator
     */
    public void setGetByRecordLocator(com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator) {
        this.getByRecordLocator = getByRecordLocator;
    }


    /**
     * Gets the getFromCurrentState value for this GetBookingBaggageRequestData.
     * 
     * @return getFromCurrentState
     */
    public java.lang.Boolean getGetFromCurrentState() {
        return getFromCurrentState;
    }


    /**
     * Sets the getFromCurrentState value for this GetBookingBaggageRequestData.
     * 
     * @param getFromCurrentState
     */
    public void setGetFromCurrentState(java.lang.Boolean getFromCurrentState) {
        this.getFromCurrentState = getFromCurrentState;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBookingBaggageRequestData)) return false;
        GetBookingBaggageRequestData other = (GetBookingBaggageRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getByRecordLocator==null && other.getGetByRecordLocator()==null) || 
             (this.getByRecordLocator!=null &&
              this.getByRecordLocator.equals(other.getGetByRecordLocator()))) &&
            ((this.getFromCurrentState==null && other.getGetFromCurrentState()==null) || 
             (this.getFromCurrentState!=null &&
              this.getFromCurrentState.equals(other.getGetFromCurrentState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetByRecordLocator() != null) {
            _hashCode += getGetByRecordLocator().hashCode();
        }
        if (getGetFromCurrentState() != null) {
            _hashCode += getGetFromCurrentState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBookingBaggageRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingBaggageRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getByRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByRecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getFromCurrentState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetFromCurrentState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
