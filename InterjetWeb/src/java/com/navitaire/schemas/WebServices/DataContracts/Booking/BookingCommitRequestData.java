/**
 * BookingCommitRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingCommitRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state;

    private java.lang.String recordLocator;

    private java.lang.String currencyCode;

    private java.lang.Short paxCount;

    private java.lang.String systemCode;

    private java.lang.Long bookingID;

    private java.lang.Long bookingParentID;

    private java.lang.String parentRecordLocator;

    private java.lang.String bookingChangeCode;

    private java.lang.String groupName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHold bookingHold;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.ReceivedByInfo receivedBy;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] recordLocators;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment[] bookingComments;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] bookingContacts;

    private java.lang.String numericRecordLocator;

    private java.lang.Boolean restrictionOverride;

    private java.lang.Boolean changeHoldDateTime;

    private java.lang.Boolean waiveNameChangeFee;

    private java.lang.Boolean waivePenaltyFee;

    private java.lang.Boolean waiveSpoilageFee;

    private java.lang.Boolean distributeToContacts;

    public BookingCommitRequestData() {
    }

    public BookingCommitRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String recordLocator,
           java.lang.String currencyCode,
           java.lang.Short paxCount,
           java.lang.String systemCode,
           java.lang.Long bookingID,
           java.lang.Long bookingParentID,
           java.lang.String parentRecordLocator,
           java.lang.String bookingChangeCode,
           java.lang.String groupName,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHold bookingHold,
           com.navitaire.schemas.WebServices.DataContracts.Booking.ReceivedByInfo receivedBy,
           com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] recordLocators,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment[] bookingComments,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] bookingContacts,
           java.lang.String numericRecordLocator,
           java.lang.Boolean restrictionOverride,
           java.lang.Boolean changeHoldDateTime,
           java.lang.Boolean waiveNameChangeFee,
           java.lang.Boolean waivePenaltyFee,
           java.lang.Boolean waiveSpoilageFee,
           java.lang.Boolean distributeToContacts) {
           this.state = state;
           this.recordLocator = recordLocator;
           this.currencyCode = currencyCode;
           this.paxCount = paxCount;
           this.systemCode = systemCode;
           this.bookingID = bookingID;
           this.bookingParentID = bookingParentID;
           this.parentRecordLocator = parentRecordLocator;
           this.bookingChangeCode = bookingChangeCode;
           this.groupName = groupName;
           this.sourcePOS = sourcePOS;
           this.bookingHold = bookingHold;
           this.receivedBy = receivedBy;
           this.recordLocators = recordLocators;
           this.passengers = passengers;
           this.bookingComments = bookingComments;
           this.bookingContacts = bookingContacts;
           this.numericRecordLocator = numericRecordLocator;
           this.restrictionOverride = restrictionOverride;
           this.changeHoldDateTime = changeHoldDateTime;
           this.waiveNameChangeFee = waiveNameChangeFee;
           this.waivePenaltyFee = waivePenaltyFee;
           this.waiveSpoilageFee = waiveSpoilageFee;
           this.distributeToContacts = distributeToContacts;
    }


    /**
     * Gets the state value for this BookingCommitRequestData.
     * 
     * @return state
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState getState() {
        return state;
    }


    /**
     * Sets the state value for this BookingCommitRequestData.
     * 
     * @param state
     */
    public void setState(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state) {
        this.state = state;
    }


    /**
     * Gets the recordLocator value for this BookingCommitRequestData.
     * 
     * @return recordLocator
     */
    public java.lang.String getRecordLocator() {
        return recordLocator;
    }


    /**
     * Sets the recordLocator value for this BookingCommitRequestData.
     * 
     * @param recordLocator
     */
    public void setRecordLocator(java.lang.String recordLocator) {
        this.recordLocator = recordLocator;
    }


    /**
     * Gets the currencyCode value for this BookingCommitRequestData.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this BookingCommitRequestData.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the paxCount value for this BookingCommitRequestData.
     * 
     * @return paxCount
     */
    public java.lang.Short getPaxCount() {
        return paxCount;
    }


    /**
     * Sets the paxCount value for this BookingCommitRequestData.
     * 
     * @param paxCount
     */
    public void setPaxCount(java.lang.Short paxCount) {
        this.paxCount = paxCount;
    }


    /**
     * Gets the systemCode value for this BookingCommitRequestData.
     * 
     * @return systemCode
     */
    public java.lang.String getSystemCode() {
        return systemCode;
    }


    /**
     * Sets the systemCode value for this BookingCommitRequestData.
     * 
     * @param systemCode
     */
    public void setSystemCode(java.lang.String systemCode) {
        this.systemCode = systemCode;
    }


    /**
     * Gets the bookingID value for this BookingCommitRequestData.
     * 
     * @return bookingID
     */
    public java.lang.Long getBookingID() {
        return bookingID;
    }


    /**
     * Sets the bookingID value for this BookingCommitRequestData.
     * 
     * @param bookingID
     */
    public void setBookingID(java.lang.Long bookingID) {
        this.bookingID = bookingID;
    }


    /**
     * Gets the bookingParentID value for this BookingCommitRequestData.
     * 
     * @return bookingParentID
     */
    public java.lang.Long getBookingParentID() {
        return bookingParentID;
    }


    /**
     * Sets the bookingParentID value for this BookingCommitRequestData.
     * 
     * @param bookingParentID
     */
    public void setBookingParentID(java.lang.Long bookingParentID) {
        this.bookingParentID = bookingParentID;
    }


    /**
     * Gets the parentRecordLocator value for this BookingCommitRequestData.
     * 
     * @return parentRecordLocator
     */
    public java.lang.String getParentRecordLocator() {
        return parentRecordLocator;
    }


    /**
     * Sets the parentRecordLocator value for this BookingCommitRequestData.
     * 
     * @param parentRecordLocator
     */
    public void setParentRecordLocator(java.lang.String parentRecordLocator) {
        this.parentRecordLocator = parentRecordLocator;
    }


    /**
     * Gets the bookingChangeCode value for this BookingCommitRequestData.
     * 
     * @return bookingChangeCode
     */
    public java.lang.String getBookingChangeCode() {
        return bookingChangeCode;
    }


    /**
     * Sets the bookingChangeCode value for this BookingCommitRequestData.
     * 
     * @param bookingChangeCode
     */
    public void setBookingChangeCode(java.lang.String bookingChangeCode) {
        this.bookingChangeCode = bookingChangeCode;
    }


    /**
     * Gets the groupName value for this BookingCommitRequestData.
     * 
     * @return groupName
     */
    public java.lang.String getGroupName() {
        return groupName;
    }


    /**
     * Sets the groupName value for this BookingCommitRequestData.
     * 
     * @param groupName
     */
    public void setGroupName(java.lang.String groupName) {
        this.groupName = groupName;
    }


    /**
     * Gets the sourcePOS value for this BookingCommitRequestData.
     * 
     * @return sourcePOS
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePOS() {
        return sourcePOS;
    }


    /**
     * Sets the sourcePOS value for this BookingCommitRequestData.
     * 
     * @param sourcePOS
     */
    public void setSourcePOS(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS) {
        this.sourcePOS = sourcePOS;
    }


    /**
     * Gets the bookingHold value for this BookingCommitRequestData.
     * 
     * @return bookingHold
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHold getBookingHold() {
        return bookingHold;
    }


    /**
     * Sets the bookingHold value for this BookingCommitRequestData.
     * 
     * @param bookingHold
     */
    public void setBookingHold(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHold bookingHold) {
        this.bookingHold = bookingHold;
    }


    /**
     * Gets the receivedBy value for this BookingCommitRequestData.
     * 
     * @return receivedBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.ReceivedByInfo getReceivedBy() {
        return receivedBy;
    }


    /**
     * Sets the receivedBy value for this BookingCommitRequestData.
     * 
     * @param receivedBy
     */
    public void setReceivedBy(com.navitaire.schemas.WebServices.DataContracts.Booking.ReceivedByInfo receivedBy) {
        this.receivedBy = receivedBy;
    }


    /**
     * Gets the recordLocators value for this BookingCommitRequestData.
     * 
     * @return recordLocators
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] getRecordLocators() {
        return recordLocators;
    }


    /**
     * Sets the recordLocators value for this BookingCommitRequestData.
     * 
     * @param recordLocators
     */
    public void setRecordLocators(com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] recordLocators) {
        this.recordLocators = recordLocators;
    }


    /**
     * Gets the passengers value for this BookingCommitRequestData.
     * 
     * @return passengers
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] getPassengers() {
        return passengers;
    }


    /**
     * Sets the passengers value for this BookingCommitRequestData.
     * 
     * @param passengers
     */
    public void setPassengers(com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers) {
        this.passengers = passengers;
    }


    /**
     * Gets the bookingComments value for this BookingCommitRequestData.
     * 
     * @return bookingComments
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment[] getBookingComments() {
        return bookingComments;
    }


    /**
     * Sets the bookingComments value for this BookingCommitRequestData.
     * 
     * @param bookingComments
     */
    public void setBookingComments(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment[] bookingComments) {
        this.bookingComments = bookingComments;
    }


    /**
     * Gets the bookingContacts value for this BookingCommitRequestData.
     * 
     * @return bookingContacts
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] getBookingContacts() {
        return bookingContacts;
    }


    /**
     * Sets the bookingContacts value for this BookingCommitRequestData.
     * 
     * @param bookingContacts
     */
    public void setBookingContacts(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] bookingContacts) {
        this.bookingContacts = bookingContacts;
    }


    /**
     * Gets the numericRecordLocator value for this BookingCommitRequestData.
     * 
     * @return numericRecordLocator
     */
    public java.lang.String getNumericRecordLocator() {
        return numericRecordLocator;
    }


    /**
     * Sets the numericRecordLocator value for this BookingCommitRequestData.
     * 
     * @param numericRecordLocator
     */
    public void setNumericRecordLocator(java.lang.String numericRecordLocator) {
        this.numericRecordLocator = numericRecordLocator;
    }


    /**
     * Gets the restrictionOverride value for this BookingCommitRequestData.
     * 
     * @return restrictionOverride
     */
    public java.lang.Boolean getRestrictionOverride() {
        return restrictionOverride;
    }


    /**
     * Sets the restrictionOverride value for this BookingCommitRequestData.
     * 
     * @param restrictionOverride
     */
    public void setRestrictionOverride(java.lang.Boolean restrictionOverride) {
        this.restrictionOverride = restrictionOverride;
    }


    /**
     * Gets the changeHoldDateTime value for this BookingCommitRequestData.
     * 
     * @return changeHoldDateTime
     */
    public java.lang.Boolean getChangeHoldDateTime() {
        return changeHoldDateTime;
    }


    /**
     * Sets the changeHoldDateTime value for this BookingCommitRequestData.
     * 
     * @param changeHoldDateTime
     */
    public void setChangeHoldDateTime(java.lang.Boolean changeHoldDateTime) {
        this.changeHoldDateTime = changeHoldDateTime;
    }


    /**
     * Gets the waiveNameChangeFee value for this BookingCommitRequestData.
     * 
     * @return waiveNameChangeFee
     */
    public java.lang.Boolean getWaiveNameChangeFee() {
        return waiveNameChangeFee;
    }


    /**
     * Sets the waiveNameChangeFee value for this BookingCommitRequestData.
     * 
     * @param waiveNameChangeFee
     */
    public void setWaiveNameChangeFee(java.lang.Boolean waiveNameChangeFee) {
        this.waiveNameChangeFee = waiveNameChangeFee;
    }


    /**
     * Gets the waivePenaltyFee value for this BookingCommitRequestData.
     * 
     * @return waivePenaltyFee
     */
    public java.lang.Boolean getWaivePenaltyFee() {
        return waivePenaltyFee;
    }


    /**
     * Sets the waivePenaltyFee value for this BookingCommitRequestData.
     * 
     * @param waivePenaltyFee
     */
    public void setWaivePenaltyFee(java.lang.Boolean waivePenaltyFee) {
        this.waivePenaltyFee = waivePenaltyFee;
    }


    /**
     * Gets the waiveSpoilageFee value for this BookingCommitRequestData.
     * 
     * @return waiveSpoilageFee
     */
    public java.lang.Boolean getWaiveSpoilageFee() {
        return waiveSpoilageFee;
    }


    /**
     * Sets the waiveSpoilageFee value for this BookingCommitRequestData.
     * 
     * @param waiveSpoilageFee
     */
    public void setWaiveSpoilageFee(java.lang.Boolean waiveSpoilageFee) {
        this.waiveSpoilageFee = waiveSpoilageFee;
    }


    /**
     * Gets the distributeToContacts value for this BookingCommitRequestData.
     * 
     * @return distributeToContacts
     */
    public java.lang.Boolean getDistributeToContacts() {
        return distributeToContacts;
    }


    /**
     * Sets the distributeToContacts value for this BookingCommitRequestData.
     * 
     * @param distributeToContacts
     */
    public void setDistributeToContacts(java.lang.Boolean distributeToContacts) {
        this.distributeToContacts = distributeToContacts;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingCommitRequestData)) return false;
        BookingCommitRequestData other = (BookingCommitRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.recordLocator==null && other.getRecordLocator()==null) || 
             (this.recordLocator!=null &&
              this.recordLocator.equals(other.getRecordLocator()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.paxCount==null && other.getPaxCount()==null) || 
             (this.paxCount!=null &&
              this.paxCount.equals(other.getPaxCount()))) &&
            ((this.systemCode==null && other.getSystemCode()==null) || 
             (this.systemCode!=null &&
              this.systemCode.equals(other.getSystemCode()))) &&
            ((this.bookingID==null && other.getBookingID()==null) || 
             (this.bookingID!=null &&
              this.bookingID.equals(other.getBookingID()))) &&
            ((this.bookingParentID==null && other.getBookingParentID()==null) || 
             (this.bookingParentID!=null &&
              this.bookingParentID.equals(other.getBookingParentID()))) &&
            ((this.parentRecordLocator==null && other.getParentRecordLocator()==null) || 
             (this.parentRecordLocator!=null &&
              this.parentRecordLocator.equals(other.getParentRecordLocator()))) &&
            ((this.bookingChangeCode==null && other.getBookingChangeCode()==null) || 
             (this.bookingChangeCode!=null &&
              this.bookingChangeCode.equals(other.getBookingChangeCode()))) &&
            ((this.groupName==null && other.getGroupName()==null) || 
             (this.groupName!=null &&
              this.groupName.equals(other.getGroupName()))) &&
            ((this.sourcePOS==null && other.getSourcePOS()==null) || 
             (this.sourcePOS!=null &&
              this.sourcePOS.equals(other.getSourcePOS()))) &&
            ((this.bookingHold==null && other.getBookingHold()==null) || 
             (this.bookingHold!=null &&
              this.bookingHold.equals(other.getBookingHold()))) &&
            ((this.receivedBy==null && other.getReceivedBy()==null) || 
             (this.receivedBy!=null &&
              this.receivedBy.equals(other.getReceivedBy()))) &&
            ((this.recordLocators==null && other.getRecordLocators()==null) || 
             (this.recordLocators!=null &&
              java.util.Arrays.equals(this.recordLocators, other.getRecordLocators()))) &&
            ((this.passengers==null && other.getPassengers()==null) || 
             (this.passengers!=null &&
              java.util.Arrays.equals(this.passengers, other.getPassengers()))) &&
            ((this.bookingComments==null && other.getBookingComments()==null) || 
             (this.bookingComments!=null &&
              java.util.Arrays.equals(this.bookingComments, other.getBookingComments()))) &&
            ((this.bookingContacts==null && other.getBookingContacts()==null) || 
             (this.bookingContacts!=null &&
              java.util.Arrays.equals(this.bookingContacts, other.getBookingContacts()))) &&
            ((this.numericRecordLocator==null && other.getNumericRecordLocator()==null) || 
             (this.numericRecordLocator!=null &&
              this.numericRecordLocator.equals(other.getNumericRecordLocator()))) &&
            ((this.restrictionOverride==null && other.getRestrictionOverride()==null) || 
             (this.restrictionOverride!=null &&
              this.restrictionOverride.equals(other.getRestrictionOverride()))) &&
            ((this.changeHoldDateTime==null && other.getChangeHoldDateTime()==null) || 
             (this.changeHoldDateTime!=null &&
              this.changeHoldDateTime.equals(other.getChangeHoldDateTime()))) &&
            ((this.waiveNameChangeFee==null && other.getWaiveNameChangeFee()==null) || 
             (this.waiveNameChangeFee!=null &&
              this.waiveNameChangeFee.equals(other.getWaiveNameChangeFee()))) &&
            ((this.waivePenaltyFee==null && other.getWaivePenaltyFee()==null) || 
             (this.waivePenaltyFee!=null &&
              this.waivePenaltyFee.equals(other.getWaivePenaltyFee()))) &&
            ((this.waiveSpoilageFee==null && other.getWaiveSpoilageFee()==null) || 
             (this.waiveSpoilageFee!=null &&
              this.waiveSpoilageFee.equals(other.getWaiveSpoilageFee()))) &&
            ((this.distributeToContacts==null && other.getDistributeToContacts()==null) || 
             (this.distributeToContacts!=null &&
              this.distributeToContacts.equals(other.getDistributeToContacts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getRecordLocator() != null) {
            _hashCode += getRecordLocator().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getPaxCount() != null) {
            _hashCode += getPaxCount().hashCode();
        }
        if (getSystemCode() != null) {
            _hashCode += getSystemCode().hashCode();
        }
        if (getBookingID() != null) {
            _hashCode += getBookingID().hashCode();
        }
        if (getBookingParentID() != null) {
            _hashCode += getBookingParentID().hashCode();
        }
        if (getParentRecordLocator() != null) {
            _hashCode += getParentRecordLocator().hashCode();
        }
        if (getBookingChangeCode() != null) {
            _hashCode += getBookingChangeCode().hashCode();
        }
        if (getGroupName() != null) {
            _hashCode += getGroupName().hashCode();
        }
        if (getSourcePOS() != null) {
            _hashCode += getSourcePOS().hashCode();
        }
        if (getBookingHold() != null) {
            _hashCode += getBookingHold().hashCode();
        }
        if (getReceivedBy() != null) {
            _hashCode += getReceivedBy().hashCode();
        }
        if (getRecordLocators() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRecordLocators());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRecordLocators(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBookingComments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingComments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingComments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBookingContacts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingContacts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingContacts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNumericRecordLocator() != null) {
            _hashCode += getNumericRecordLocator().hashCode();
        }
        if (getRestrictionOverride() != null) {
            _hashCode += getRestrictionOverride().hashCode();
        }
        if (getChangeHoldDateTime() != null) {
            _hashCode += getChangeHoldDateTime().hashCode();
        }
        if (getWaiveNameChangeFee() != null) {
            _hashCode += getWaiveNameChangeFee().hashCode();
        }
        if (getWaivePenaltyFee() != null) {
            _hashCode += getWaivePenaltyFee().hashCode();
        }
        if (getWaiveSpoilageFee() != null) {
            _hashCode += getWaiveSpoilageFee().hashCode();
        }
        if (getDistributeToContacts() != null) {
            _hashCode += getDistributeToContacts().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingCommitRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingCommitRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "MessageState"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SystemCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingParentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingParentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ParentRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingChangeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingChangeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GroupName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePOS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingHold");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHold"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHold"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receivedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedByInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocators");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocators"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passengers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingComments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingContacts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContacts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContact"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContact"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numericRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NumericRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("restrictionOverride");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RestrictionOverride"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeHoldDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChangeHoldDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waiveNameChangeFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaiveNameChangeFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waivePenaltyFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaivePenaltyFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waiveSpoilageFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaiveSpoilageFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distributeToContacts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DistributeToContacts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
