/**
 * PassengerBag.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PassengerBag  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Long baggageID;

    private java.lang.String OSTag;

    private java.util.Calendar OSTagDate;

    private java.lang.String stationCode;

    private java.lang.Short weight;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightType weightType;

    private java.lang.String taggedToStation;

    private java.lang.String taggedToFlightNumber;

    private java.lang.Boolean LRTIndicator;

    private java.lang.String baggageType;

    private java.lang.String taggedToCarrierCode;

    public PassengerBag() {
    }

    public PassengerBag(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Long baggageID,
           java.lang.String OSTag,
           java.util.Calendar OSTagDate,
           java.lang.String stationCode,
           java.lang.Short weight,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightType weightType,
           java.lang.String taggedToStation,
           java.lang.String taggedToFlightNumber,
           java.lang.Boolean LRTIndicator,
           java.lang.String baggageType,
           java.lang.String taggedToCarrierCode) {
        super(
            state);
        this.baggageID = baggageID;
        this.OSTag = OSTag;
        this.OSTagDate = OSTagDate;
        this.stationCode = stationCode;
        this.weight = weight;
        this.weightType = weightType;
        this.taggedToStation = taggedToStation;
        this.taggedToFlightNumber = taggedToFlightNumber;
        this.LRTIndicator = LRTIndicator;
        this.baggageType = baggageType;
        this.taggedToCarrierCode = taggedToCarrierCode;
    }


    /**
     * Gets the baggageID value for this PassengerBag.
     * 
     * @return baggageID
     */
    public java.lang.Long getBaggageID() {
        return baggageID;
    }


    /**
     * Sets the baggageID value for this PassengerBag.
     * 
     * @param baggageID
     */
    public void setBaggageID(java.lang.Long baggageID) {
        this.baggageID = baggageID;
    }


    /**
     * Gets the OSTag value for this PassengerBag.
     * 
     * @return OSTag
     */
    public java.lang.String getOSTag() {
        return OSTag;
    }


    /**
     * Sets the OSTag value for this PassengerBag.
     * 
     * @param OSTag
     */
    public void setOSTag(java.lang.String OSTag) {
        this.OSTag = OSTag;
    }


    /**
     * Gets the OSTagDate value for this PassengerBag.
     * 
     * @return OSTagDate
     */
    public java.util.Calendar getOSTagDate() {
        return OSTagDate;
    }


    /**
     * Sets the OSTagDate value for this PassengerBag.
     * 
     * @param OSTagDate
     */
    public void setOSTagDate(java.util.Calendar OSTagDate) {
        this.OSTagDate = OSTagDate;
    }


    /**
     * Gets the stationCode value for this PassengerBag.
     * 
     * @return stationCode
     */
    public java.lang.String getStationCode() {
        return stationCode;
    }


    /**
     * Sets the stationCode value for this PassengerBag.
     * 
     * @param stationCode
     */
    public void setStationCode(java.lang.String stationCode) {
        this.stationCode = stationCode;
    }


    /**
     * Gets the weight value for this PassengerBag.
     * 
     * @return weight
     */
    public java.lang.Short getWeight() {
        return weight;
    }


    /**
     * Sets the weight value for this PassengerBag.
     * 
     * @param weight
     */
    public void setWeight(java.lang.Short weight) {
        this.weight = weight;
    }


    /**
     * Gets the weightType value for this PassengerBag.
     * 
     * @return weightType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightType getWeightType() {
        return weightType;
    }


    /**
     * Sets the weightType value for this PassengerBag.
     * 
     * @param weightType
     */
    public void setWeightType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightType weightType) {
        this.weightType = weightType;
    }


    /**
     * Gets the taggedToStation value for this PassengerBag.
     * 
     * @return taggedToStation
     */
    public java.lang.String getTaggedToStation() {
        return taggedToStation;
    }


    /**
     * Sets the taggedToStation value for this PassengerBag.
     * 
     * @param taggedToStation
     */
    public void setTaggedToStation(java.lang.String taggedToStation) {
        this.taggedToStation = taggedToStation;
    }


    /**
     * Gets the taggedToFlightNumber value for this PassengerBag.
     * 
     * @return taggedToFlightNumber
     */
    public java.lang.String getTaggedToFlightNumber() {
        return taggedToFlightNumber;
    }


    /**
     * Sets the taggedToFlightNumber value for this PassengerBag.
     * 
     * @param taggedToFlightNumber
     */
    public void setTaggedToFlightNumber(java.lang.String taggedToFlightNumber) {
        this.taggedToFlightNumber = taggedToFlightNumber;
    }


    /**
     * Gets the LRTIndicator value for this PassengerBag.
     * 
     * @return LRTIndicator
     */
    public java.lang.Boolean getLRTIndicator() {
        return LRTIndicator;
    }


    /**
     * Sets the LRTIndicator value for this PassengerBag.
     * 
     * @param LRTIndicator
     */
    public void setLRTIndicator(java.lang.Boolean LRTIndicator) {
        this.LRTIndicator = LRTIndicator;
    }


    /**
     * Gets the baggageType value for this PassengerBag.
     * 
     * @return baggageType
     */
    public java.lang.String getBaggageType() {
        return baggageType;
    }


    /**
     * Sets the baggageType value for this PassengerBag.
     * 
     * @param baggageType
     */
    public void setBaggageType(java.lang.String baggageType) {
        this.baggageType = baggageType;
    }


    /**
     * Gets the taggedToCarrierCode value for this PassengerBag.
     * 
     * @return taggedToCarrierCode
     */
    public java.lang.String getTaggedToCarrierCode() {
        return taggedToCarrierCode;
    }


    /**
     * Sets the taggedToCarrierCode value for this PassengerBag.
     * 
     * @param taggedToCarrierCode
     */
    public void setTaggedToCarrierCode(java.lang.String taggedToCarrierCode) {
        this.taggedToCarrierCode = taggedToCarrierCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PassengerBag)) return false;
        PassengerBag other = (PassengerBag) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.baggageID==null && other.getBaggageID()==null) || 
             (this.baggageID!=null &&
              this.baggageID.equals(other.getBaggageID()))) &&
            ((this.OSTag==null && other.getOSTag()==null) || 
             (this.OSTag!=null &&
              this.OSTag.equals(other.getOSTag()))) &&
            ((this.OSTagDate==null && other.getOSTagDate()==null) || 
             (this.OSTagDate!=null &&
              this.OSTagDate.equals(other.getOSTagDate()))) &&
            ((this.stationCode==null && other.getStationCode()==null) || 
             (this.stationCode!=null &&
              this.stationCode.equals(other.getStationCode()))) &&
            ((this.weight==null && other.getWeight()==null) || 
             (this.weight!=null &&
              this.weight.equals(other.getWeight()))) &&
            ((this.weightType==null && other.getWeightType()==null) || 
             (this.weightType!=null &&
              this.weightType.equals(other.getWeightType()))) &&
            ((this.taggedToStation==null && other.getTaggedToStation()==null) || 
             (this.taggedToStation!=null &&
              this.taggedToStation.equals(other.getTaggedToStation()))) &&
            ((this.taggedToFlightNumber==null && other.getTaggedToFlightNumber()==null) || 
             (this.taggedToFlightNumber!=null &&
              this.taggedToFlightNumber.equals(other.getTaggedToFlightNumber()))) &&
            ((this.LRTIndicator==null && other.getLRTIndicator()==null) || 
             (this.LRTIndicator!=null &&
              this.LRTIndicator.equals(other.getLRTIndicator()))) &&
            ((this.baggageType==null && other.getBaggageType()==null) || 
             (this.baggageType!=null &&
              this.baggageType.equals(other.getBaggageType()))) &&
            ((this.taggedToCarrierCode==null && other.getTaggedToCarrierCode()==null) || 
             (this.taggedToCarrierCode!=null &&
              this.taggedToCarrierCode.equals(other.getTaggedToCarrierCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBaggageID() != null) {
            _hashCode += getBaggageID().hashCode();
        }
        if (getOSTag() != null) {
            _hashCode += getOSTag().hashCode();
        }
        if (getOSTagDate() != null) {
            _hashCode += getOSTagDate().hashCode();
        }
        if (getStationCode() != null) {
            _hashCode += getStationCode().hashCode();
        }
        if (getWeight() != null) {
            _hashCode += getWeight().hashCode();
        }
        if (getWeightType() != null) {
            _hashCode += getWeightType().hashCode();
        }
        if (getTaggedToStation() != null) {
            _hashCode += getTaggedToStation().hashCode();
        }
        if (getTaggedToFlightNumber() != null) {
            _hashCode += getTaggedToFlightNumber().hashCode();
        }
        if (getLRTIndicator() != null) {
            _hashCode += getLRTIndicator().hashCode();
        }
        if (getBaggageType() != null) {
            _hashCode += getBaggageType().hashCode();
        }
        if (getTaggedToCarrierCode() != null) {
            _hashCode += getTaggedToCarrierCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PassengerBag.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerBag"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSTag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OSTag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSTagDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OSTagDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "StationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Weight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weightType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WeightType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "WeightType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taggedToStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TaggedToStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taggedToFlightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TaggedToFlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LRTIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LRTIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taggedToCarrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TaggedToCarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
