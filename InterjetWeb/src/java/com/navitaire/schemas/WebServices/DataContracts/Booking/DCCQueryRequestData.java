/**
 * DCCQueryRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DCCQueryRequestData  implements java.io.Serializable {
    private java.lang.String accountNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AuthorizationStatus authorizationStatus;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentMethodType paymentMethodType;

    private java.lang.String paymentMethodCode;

    private java.lang.String quotedCurrencyCode;

    private java.math.BigDecimal quotedAmount;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus status;

    public DCCQueryRequestData() {
    }

    public DCCQueryRequestData(
           java.lang.String accountNumber,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AuthorizationStatus authorizationStatus,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentMethodType paymentMethodType,
           java.lang.String paymentMethodCode,
           java.lang.String quotedCurrencyCode,
           java.math.BigDecimal quotedAmount,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus status) {
           this.accountNumber = accountNumber;
           this.authorizationStatus = authorizationStatus;
           this.channelType = channelType;
           this.paymentMethodType = paymentMethodType;
           this.paymentMethodCode = paymentMethodCode;
           this.quotedCurrencyCode = quotedCurrencyCode;
           this.quotedAmount = quotedAmount;
           this.status = status;
    }


    /**
     * Gets the accountNumber value for this DCCQueryRequestData.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this DCCQueryRequestData.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the authorizationStatus value for this DCCQueryRequestData.
     * 
     * @return authorizationStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AuthorizationStatus getAuthorizationStatus() {
        return authorizationStatus;
    }


    /**
     * Sets the authorizationStatus value for this DCCQueryRequestData.
     * 
     * @param authorizationStatus
     */
    public void setAuthorizationStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AuthorizationStatus authorizationStatus) {
        this.authorizationStatus = authorizationStatus;
    }


    /**
     * Gets the channelType value for this DCCQueryRequestData.
     * 
     * @return channelType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType getChannelType() {
        return channelType;
    }


    /**
     * Sets the channelType value for this DCCQueryRequestData.
     * 
     * @param channelType
     */
    public void setChannelType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType) {
        this.channelType = channelType;
    }


    /**
     * Gets the paymentMethodType value for this DCCQueryRequestData.
     * 
     * @return paymentMethodType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentMethodType getPaymentMethodType() {
        return paymentMethodType;
    }


    /**
     * Sets the paymentMethodType value for this DCCQueryRequestData.
     * 
     * @param paymentMethodType
     */
    public void setPaymentMethodType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentMethodType paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }


    /**
     * Gets the paymentMethodCode value for this DCCQueryRequestData.
     * 
     * @return paymentMethodCode
     */
    public java.lang.String getPaymentMethodCode() {
        return paymentMethodCode;
    }


    /**
     * Sets the paymentMethodCode value for this DCCQueryRequestData.
     * 
     * @param paymentMethodCode
     */
    public void setPaymentMethodCode(java.lang.String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }


    /**
     * Gets the quotedCurrencyCode value for this DCCQueryRequestData.
     * 
     * @return quotedCurrencyCode
     */
    public java.lang.String getQuotedCurrencyCode() {
        return quotedCurrencyCode;
    }


    /**
     * Sets the quotedCurrencyCode value for this DCCQueryRequestData.
     * 
     * @param quotedCurrencyCode
     */
    public void setQuotedCurrencyCode(java.lang.String quotedCurrencyCode) {
        this.quotedCurrencyCode = quotedCurrencyCode;
    }


    /**
     * Gets the quotedAmount value for this DCCQueryRequestData.
     * 
     * @return quotedAmount
     */
    public java.math.BigDecimal getQuotedAmount() {
        return quotedAmount;
    }


    /**
     * Sets the quotedAmount value for this DCCQueryRequestData.
     * 
     * @param quotedAmount
     */
    public void setQuotedAmount(java.math.BigDecimal quotedAmount) {
        this.quotedAmount = quotedAmount;
    }


    /**
     * Gets the status value for this DCCQueryRequestData.
     * 
     * @return status
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this DCCQueryRequestData.
     * 
     * @param status
     */
    public void setStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus status) {
        this.status = status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DCCQueryRequestData)) return false;
        DCCQueryRequestData other = (DCCQueryRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.authorizationStatus==null && other.getAuthorizationStatus()==null) || 
             (this.authorizationStatus!=null &&
              this.authorizationStatus.equals(other.getAuthorizationStatus()))) &&
            ((this.channelType==null && other.getChannelType()==null) || 
             (this.channelType!=null &&
              this.channelType.equals(other.getChannelType()))) &&
            ((this.paymentMethodType==null && other.getPaymentMethodType()==null) || 
             (this.paymentMethodType!=null &&
              this.paymentMethodType.equals(other.getPaymentMethodType()))) &&
            ((this.paymentMethodCode==null && other.getPaymentMethodCode()==null) || 
             (this.paymentMethodCode!=null &&
              this.paymentMethodCode.equals(other.getPaymentMethodCode()))) &&
            ((this.quotedCurrencyCode==null && other.getQuotedCurrencyCode()==null) || 
             (this.quotedCurrencyCode!=null &&
              this.quotedCurrencyCode.equals(other.getQuotedCurrencyCode()))) &&
            ((this.quotedAmount==null && other.getQuotedAmount()==null) || 
             (this.quotedAmount!=null &&
              this.quotedAmount.equals(other.getQuotedAmount()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getAuthorizationStatus() != null) {
            _hashCode += getAuthorizationStatus().hashCode();
        }
        if (getChannelType() != null) {
            _hashCode += getChannelType().hashCode();
        }
        if (getPaymentMethodType() != null) {
            _hashCode += getPaymentMethodType().hashCode();
        }
        if (getPaymentMethodCode() != null) {
            _hashCode += getPaymentMethodCode().hashCode();
        }
        if (getQuotedCurrencyCode() != null) {
            _hashCode += getQuotedCurrencyCode().hashCode();
        }
        if (getQuotedAmount() != null) {
            _hashCode += getQuotedAmount().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DCCQueryRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DCCQueryRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AuthorizationStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AuthorizationStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChannelType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChannelType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentMethodType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentMethodType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentMethodCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quotedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QuotedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quotedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QuotedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingPaymentStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
