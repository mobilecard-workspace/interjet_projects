/**
 * BookingSum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingSum  implements java.io.Serializable {
    private java.math.BigDecimal balanceDue;

    private java.math.BigDecimal authorizedBalanceDue;

    private java.lang.Short segmentCount;

    private java.lang.Short passiveSegmentCount;

    private java.math.BigDecimal totalCost;

    private java.math.BigDecimal pointsBalanceDue;

    private java.math.BigDecimal totalPointCost;

    private java.lang.String alternateCurrencyCode;

    private java.math.BigDecimal alternateCurrencyBalanceDue;

    public BookingSum() {
    }

    public BookingSum(
           java.math.BigDecimal balanceDue,
           java.math.BigDecimal authorizedBalanceDue,
           java.lang.Short segmentCount,
           java.lang.Short passiveSegmentCount,
           java.math.BigDecimal totalCost,
           java.math.BigDecimal pointsBalanceDue,
           java.math.BigDecimal totalPointCost,
           java.lang.String alternateCurrencyCode,
           java.math.BigDecimal alternateCurrencyBalanceDue) {
           this.balanceDue = balanceDue;
           this.authorizedBalanceDue = authorizedBalanceDue;
           this.segmentCount = segmentCount;
           this.passiveSegmentCount = passiveSegmentCount;
           this.totalCost = totalCost;
           this.pointsBalanceDue = pointsBalanceDue;
           this.totalPointCost = totalPointCost;
           this.alternateCurrencyCode = alternateCurrencyCode;
           this.alternateCurrencyBalanceDue = alternateCurrencyBalanceDue;
    }


    /**
     * Gets the balanceDue value for this BookingSum.
     * 
     * @return balanceDue
     */
    public java.math.BigDecimal getBalanceDue() {
        return balanceDue;
    }


    /**
     * Sets the balanceDue value for this BookingSum.
     * 
     * @param balanceDue
     */
    public void setBalanceDue(java.math.BigDecimal balanceDue) {
        this.balanceDue = balanceDue;
    }


    /**
     * Gets the authorizedBalanceDue value for this BookingSum.
     * 
     * @return authorizedBalanceDue
     */
    public java.math.BigDecimal getAuthorizedBalanceDue() {
        return authorizedBalanceDue;
    }


    /**
     * Sets the authorizedBalanceDue value for this BookingSum.
     * 
     * @param authorizedBalanceDue
     */
    public void setAuthorizedBalanceDue(java.math.BigDecimal authorizedBalanceDue) {
        this.authorizedBalanceDue = authorizedBalanceDue;
    }


    /**
     * Gets the segmentCount value for this BookingSum.
     * 
     * @return segmentCount
     */
    public java.lang.Short getSegmentCount() {
        return segmentCount;
    }


    /**
     * Sets the segmentCount value for this BookingSum.
     * 
     * @param segmentCount
     */
    public void setSegmentCount(java.lang.Short segmentCount) {
        this.segmentCount = segmentCount;
    }


    /**
     * Gets the passiveSegmentCount value for this BookingSum.
     * 
     * @return passiveSegmentCount
     */
    public java.lang.Short getPassiveSegmentCount() {
        return passiveSegmentCount;
    }


    /**
     * Sets the passiveSegmentCount value for this BookingSum.
     * 
     * @param passiveSegmentCount
     */
    public void setPassiveSegmentCount(java.lang.Short passiveSegmentCount) {
        this.passiveSegmentCount = passiveSegmentCount;
    }


    /**
     * Gets the totalCost value for this BookingSum.
     * 
     * @return totalCost
     */
    public java.math.BigDecimal getTotalCost() {
        return totalCost;
    }


    /**
     * Sets the totalCost value for this BookingSum.
     * 
     * @param totalCost
     */
    public void setTotalCost(java.math.BigDecimal totalCost) {
        this.totalCost = totalCost;
    }


    /**
     * Gets the pointsBalanceDue value for this BookingSum.
     * 
     * @return pointsBalanceDue
     */
    public java.math.BigDecimal getPointsBalanceDue() {
        return pointsBalanceDue;
    }


    /**
     * Sets the pointsBalanceDue value for this BookingSum.
     * 
     * @param pointsBalanceDue
     */
    public void setPointsBalanceDue(java.math.BigDecimal pointsBalanceDue) {
        this.pointsBalanceDue = pointsBalanceDue;
    }


    /**
     * Gets the totalPointCost value for this BookingSum.
     * 
     * @return totalPointCost
     */
    public java.math.BigDecimal getTotalPointCost() {
        return totalPointCost;
    }


    /**
     * Sets the totalPointCost value for this BookingSum.
     * 
     * @param totalPointCost
     */
    public void setTotalPointCost(java.math.BigDecimal totalPointCost) {
        this.totalPointCost = totalPointCost;
    }


    /**
     * Gets the alternateCurrencyCode value for this BookingSum.
     * 
     * @return alternateCurrencyCode
     */
    public java.lang.String getAlternateCurrencyCode() {
        return alternateCurrencyCode;
    }


    /**
     * Sets the alternateCurrencyCode value for this BookingSum.
     * 
     * @param alternateCurrencyCode
     */
    public void setAlternateCurrencyCode(java.lang.String alternateCurrencyCode) {
        this.alternateCurrencyCode = alternateCurrencyCode;
    }


    /**
     * Gets the alternateCurrencyBalanceDue value for this BookingSum.
     * 
     * @return alternateCurrencyBalanceDue
     */
    public java.math.BigDecimal getAlternateCurrencyBalanceDue() {
        return alternateCurrencyBalanceDue;
    }


    /**
     * Sets the alternateCurrencyBalanceDue value for this BookingSum.
     * 
     * @param alternateCurrencyBalanceDue
     */
    public void setAlternateCurrencyBalanceDue(java.math.BigDecimal alternateCurrencyBalanceDue) {
        this.alternateCurrencyBalanceDue = alternateCurrencyBalanceDue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingSum)) return false;
        BookingSum other = (BookingSum) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.balanceDue==null && other.getBalanceDue()==null) || 
             (this.balanceDue!=null &&
              this.balanceDue.equals(other.getBalanceDue()))) &&
            ((this.authorizedBalanceDue==null && other.getAuthorizedBalanceDue()==null) || 
             (this.authorizedBalanceDue!=null &&
              this.authorizedBalanceDue.equals(other.getAuthorizedBalanceDue()))) &&
            ((this.segmentCount==null && other.getSegmentCount()==null) || 
             (this.segmentCount!=null &&
              this.segmentCount.equals(other.getSegmentCount()))) &&
            ((this.passiveSegmentCount==null && other.getPassiveSegmentCount()==null) || 
             (this.passiveSegmentCount!=null &&
              this.passiveSegmentCount.equals(other.getPassiveSegmentCount()))) &&
            ((this.totalCost==null && other.getTotalCost()==null) || 
             (this.totalCost!=null &&
              this.totalCost.equals(other.getTotalCost()))) &&
            ((this.pointsBalanceDue==null && other.getPointsBalanceDue()==null) || 
             (this.pointsBalanceDue!=null &&
              this.pointsBalanceDue.equals(other.getPointsBalanceDue()))) &&
            ((this.totalPointCost==null && other.getTotalPointCost()==null) || 
             (this.totalPointCost!=null &&
              this.totalPointCost.equals(other.getTotalPointCost()))) &&
            ((this.alternateCurrencyCode==null && other.getAlternateCurrencyCode()==null) || 
             (this.alternateCurrencyCode!=null &&
              this.alternateCurrencyCode.equals(other.getAlternateCurrencyCode()))) &&
            ((this.alternateCurrencyBalanceDue==null && other.getAlternateCurrencyBalanceDue()==null) || 
             (this.alternateCurrencyBalanceDue!=null &&
              this.alternateCurrencyBalanceDue.equals(other.getAlternateCurrencyBalanceDue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBalanceDue() != null) {
            _hashCode += getBalanceDue().hashCode();
        }
        if (getAuthorizedBalanceDue() != null) {
            _hashCode += getAuthorizedBalanceDue().hashCode();
        }
        if (getSegmentCount() != null) {
            _hashCode += getSegmentCount().hashCode();
        }
        if (getPassiveSegmentCount() != null) {
            _hashCode += getPassiveSegmentCount().hashCode();
        }
        if (getTotalCost() != null) {
            _hashCode += getTotalCost().hashCode();
        }
        if (getPointsBalanceDue() != null) {
            _hashCode += getPointsBalanceDue().hashCode();
        }
        if (getTotalPointCost() != null) {
            _hashCode += getTotalPointCost().hashCode();
        }
        if (getAlternateCurrencyCode() != null) {
            _hashCode += getAlternateCurrencyCode().hashCode();
        }
        if (getAlternateCurrencyBalanceDue() != null) {
            _hashCode += getAlternateCurrencyBalanceDue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingSum.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingSum"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceDue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BalanceDue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizedBalanceDue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AuthorizedBalanceDue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passiveSegmentCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassiveSegmentCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalCost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TotalCost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointsBalanceDue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PointsBalanceDue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPointCost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TotalPointCost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternateCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternateCurrencyBalanceDue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateCurrencyBalanceDue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
