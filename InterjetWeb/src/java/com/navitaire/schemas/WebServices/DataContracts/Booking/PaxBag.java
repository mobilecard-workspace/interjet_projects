/**
 * PaxBag.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxBag  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Short passengerNumber;

    private java.lang.String arrivalStation;

    private java.lang.String departureStation;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BaggageStatus baggageStatus;

    private java.lang.Short compartmentID;

    private java.lang.String OSTag;

    private java.util.Calendar OSTagDate;

    public PaxBag() {
    }

    public PaxBag(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Short passengerNumber,
           java.lang.String arrivalStation,
           java.lang.String departureStation,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BaggageStatus baggageStatus,
           java.lang.Short compartmentID,
           java.lang.String OSTag,
           java.util.Calendar OSTagDate) {
        super(
            state);
        this.passengerNumber = passengerNumber;
        this.arrivalStation = arrivalStation;
        this.departureStation = departureStation;
        this.baggageStatus = baggageStatus;
        this.compartmentID = compartmentID;
        this.OSTag = OSTag;
        this.OSTagDate = OSTagDate;
    }


    /**
     * Gets the passengerNumber value for this PaxBag.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this PaxBag.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the arrivalStation value for this PaxBag.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this PaxBag.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the departureStation value for this PaxBag.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this PaxBag.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the baggageStatus value for this PaxBag.
     * 
     * @return baggageStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BaggageStatus getBaggageStatus() {
        return baggageStatus;
    }


    /**
     * Sets the baggageStatus value for this PaxBag.
     * 
     * @param baggageStatus
     */
    public void setBaggageStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BaggageStatus baggageStatus) {
        this.baggageStatus = baggageStatus;
    }


    /**
     * Gets the compartmentID value for this PaxBag.
     * 
     * @return compartmentID
     */
    public java.lang.Short getCompartmentID() {
        return compartmentID;
    }


    /**
     * Sets the compartmentID value for this PaxBag.
     * 
     * @param compartmentID
     */
    public void setCompartmentID(java.lang.Short compartmentID) {
        this.compartmentID = compartmentID;
    }


    /**
     * Gets the OSTag value for this PaxBag.
     * 
     * @return OSTag
     */
    public java.lang.String getOSTag() {
        return OSTag;
    }


    /**
     * Sets the OSTag value for this PaxBag.
     * 
     * @param OSTag
     */
    public void setOSTag(java.lang.String OSTag) {
        this.OSTag = OSTag;
    }


    /**
     * Gets the OSTagDate value for this PaxBag.
     * 
     * @return OSTagDate
     */
    public java.util.Calendar getOSTagDate() {
        return OSTagDate;
    }


    /**
     * Sets the OSTagDate value for this PaxBag.
     * 
     * @param OSTagDate
     */
    public void setOSTagDate(java.util.Calendar OSTagDate) {
        this.OSTagDate = OSTagDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxBag)) return false;
        PaxBag other = (PaxBag) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.baggageStatus==null && other.getBaggageStatus()==null) || 
             (this.baggageStatus!=null &&
              this.baggageStatus.equals(other.getBaggageStatus()))) &&
            ((this.compartmentID==null && other.getCompartmentID()==null) || 
             (this.compartmentID!=null &&
              this.compartmentID.equals(other.getCompartmentID()))) &&
            ((this.OSTag==null && other.getOSTag()==null) || 
             (this.OSTag!=null &&
              this.OSTag.equals(other.getOSTag()))) &&
            ((this.OSTagDate==null && other.getOSTagDate()==null) || 
             (this.OSTagDate!=null &&
              this.OSTagDate.equals(other.getOSTagDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getBaggageStatus() != null) {
            _hashCode += getBaggageStatus().hashCode();
        }
        if (getCompartmentID() != null) {
            _hashCode += getCompartmentID().hashCode();
        }
        if (getOSTag() != null) {
            _hashCode += getOSTag().hashCode();
        }
        if (getOSTagDate() != null) {
            _hashCode += getOSTagDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxBag.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxBag"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BaggageStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("compartmentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSTag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OSTag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSTagDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OSTagDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
