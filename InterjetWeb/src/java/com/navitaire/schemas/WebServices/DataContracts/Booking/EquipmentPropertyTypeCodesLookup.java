/**
 * EquipmentPropertyTypeCodesLookup.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class EquipmentPropertyTypeCodesLookup  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] booleanPropertyTypes;

    private java.lang.String[] numericPropertyTypeCodes;

    private java.lang.String[] SSRCodes;

    private java.util.Calendar timestamp;

    public EquipmentPropertyTypeCodesLookup() {
    }

    public EquipmentPropertyTypeCodesLookup(
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] booleanPropertyTypes,
           java.lang.String[] numericPropertyTypeCodes,
           java.lang.String[] SSRCodes,
           java.util.Calendar timestamp) {
           this.booleanPropertyTypes = booleanPropertyTypes;
           this.numericPropertyTypeCodes = numericPropertyTypeCodes;
           this.SSRCodes = SSRCodes;
           this.timestamp = timestamp;
    }


    /**
     * Gets the booleanPropertyTypes value for this EquipmentPropertyTypeCodesLookup.
     * 
     * @return booleanPropertyTypes
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] getBooleanPropertyTypes() {
        return booleanPropertyTypes;
    }


    /**
     * Sets the booleanPropertyTypes value for this EquipmentPropertyTypeCodesLookup.
     * 
     * @param booleanPropertyTypes
     */
    public void setBooleanPropertyTypes(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] booleanPropertyTypes) {
        this.booleanPropertyTypes = booleanPropertyTypes;
    }


    /**
     * Gets the numericPropertyTypeCodes value for this EquipmentPropertyTypeCodesLookup.
     * 
     * @return numericPropertyTypeCodes
     */
    public java.lang.String[] getNumericPropertyTypeCodes() {
        return numericPropertyTypeCodes;
    }


    /**
     * Sets the numericPropertyTypeCodes value for this EquipmentPropertyTypeCodesLookup.
     * 
     * @param numericPropertyTypeCodes
     */
    public void setNumericPropertyTypeCodes(java.lang.String[] numericPropertyTypeCodes) {
        this.numericPropertyTypeCodes = numericPropertyTypeCodes;
    }


    /**
     * Gets the SSRCodes value for this EquipmentPropertyTypeCodesLookup.
     * 
     * @return SSRCodes
     */
    public java.lang.String[] getSSRCodes() {
        return SSRCodes;
    }


    /**
     * Sets the SSRCodes value for this EquipmentPropertyTypeCodesLookup.
     * 
     * @param SSRCodes
     */
    public void setSSRCodes(java.lang.String[] SSRCodes) {
        this.SSRCodes = SSRCodes;
    }


    /**
     * Gets the timestamp value for this EquipmentPropertyTypeCodesLookup.
     * 
     * @return timestamp
     */
    public java.util.Calendar getTimestamp() {
        return timestamp;
    }


    /**
     * Sets the timestamp value for this EquipmentPropertyTypeCodesLookup.
     * 
     * @param timestamp
     */
    public void setTimestamp(java.util.Calendar timestamp) {
        this.timestamp = timestamp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EquipmentPropertyTypeCodesLookup)) return false;
        EquipmentPropertyTypeCodesLookup other = (EquipmentPropertyTypeCodesLookup) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.booleanPropertyTypes==null && other.getBooleanPropertyTypes()==null) || 
             (this.booleanPropertyTypes!=null &&
              java.util.Arrays.equals(this.booleanPropertyTypes, other.getBooleanPropertyTypes()))) &&
            ((this.numericPropertyTypeCodes==null && other.getNumericPropertyTypeCodes()==null) || 
             (this.numericPropertyTypeCodes!=null &&
              java.util.Arrays.equals(this.numericPropertyTypeCodes, other.getNumericPropertyTypeCodes()))) &&
            ((this.SSRCodes==null && other.getSSRCodes()==null) || 
             (this.SSRCodes!=null &&
              java.util.Arrays.equals(this.SSRCodes, other.getSSRCodes()))) &&
            ((this.timestamp==null && other.getTimestamp()==null) || 
             (this.timestamp!=null &&
              this.timestamp.equals(other.getTimestamp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBooleanPropertyTypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBooleanPropertyTypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBooleanPropertyTypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNumericPropertyTypeCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNumericPropertyTypeCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNumericPropertyTypeCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSSRCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSSRCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSSRCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTimestamp() != null) {
            _hashCode += getTimestamp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EquipmentPropertyTypeCodesLookup.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentPropertyTypeCodesLookup"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("booleanPropertyTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BooleanPropertyTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numericPropertyTypeCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NumericPropertyTypeCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Timestamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
