/**
 * EquipmentListRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class EquipmentListRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentRequest[] equipmentRequests;

    private java.lang.Boolean includePropertyLookup;

    private java.lang.Boolean compressProperties;

    public EquipmentListRequest() {
    }

    public EquipmentListRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentRequest[] equipmentRequests,
           java.lang.Boolean includePropertyLookup,
           java.lang.Boolean compressProperties) {
           this.equipmentRequests = equipmentRequests;
           this.includePropertyLookup = includePropertyLookup;
           this.compressProperties = compressProperties;
    }


    /**
     * Gets the equipmentRequests value for this EquipmentListRequest.
     * 
     * @return equipmentRequests
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentRequest[] getEquipmentRequests() {
        return equipmentRequests;
    }


    /**
     * Sets the equipmentRequests value for this EquipmentListRequest.
     * 
     * @param equipmentRequests
     */
    public void setEquipmentRequests(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentRequest[] equipmentRequests) {
        this.equipmentRequests = equipmentRequests;
    }


    /**
     * Gets the includePropertyLookup value for this EquipmentListRequest.
     * 
     * @return includePropertyLookup
     */
    public java.lang.Boolean getIncludePropertyLookup() {
        return includePropertyLookup;
    }


    /**
     * Sets the includePropertyLookup value for this EquipmentListRequest.
     * 
     * @param includePropertyLookup
     */
    public void setIncludePropertyLookup(java.lang.Boolean includePropertyLookup) {
        this.includePropertyLookup = includePropertyLookup;
    }


    /**
     * Gets the compressProperties value for this EquipmentListRequest.
     * 
     * @return compressProperties
     */
    public java.lang.Boolean getCompressProperties() {
        return compressProperties;
    }


    /**
     * Sets the compressProperties value for this EquipmentListRequest.
     * 
     * @param compressProperties
     */
    public void setCompressProperties(java.lang.Boolean compressProperties) {
        this.compressProperties = compressProperties;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EquipmentListRequest)) return false;
        EquipmentListRequest other = (EquipmentListRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.equipmentRequests==null && other.getEquipmentRequests()==null) || 
             (this.equipmentRequests!=null &&
              java.util.Arrays.equals(this.equipmentRequests, other.getEquipmentRequests()))) &&
            ((this.includePropertyLookup==null && other.getIncludePropertyLookup()==null) || 
             (this.includePropertyLookup!=null &&
              this.includePropertyLookup.equals(other.getIncludePropertyLookup()))) &&
            ((this.compressProperties==null && other.getCompressProperties()==null) || 
             (this.compressProperties!=null &&
              this.compressProperties.equals(other.getCompressProperties())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEquipmentRequests() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEquipmentRequests());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEquipmentRequests(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIncludePropertyLookup() != null) {
            _hashCode += getIncludePropertyLookup().hashCode();
        }
        if (getCompressProperties() != null) {
            _hashCode += getCompressProperties().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EquipmentListRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentListRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentRequests");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentRequests"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentRequest"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePropertyLookup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludePropertyLookup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("compressProperties");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompressProperties"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
