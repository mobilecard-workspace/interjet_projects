/**
 * TravelCommerceSellRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class TravelCommerceSellRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem[] orderItemList;

    private java.lang.String cultureCode;

    public TravelCommerceSellRequestData() {
    }

    public TravelCommerceSellRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem[] orderItemList,
           java.lang.String cultureCode) {
           this.orderItemList = orderItemList;
           this.cultureCode = cultureCode;
    }


    /**
     * Gets the orderItemList value for this TravelCommerceSellRequestData.
     * 
     * @return orderItemList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem[] getOrderItemList() {
        return orderItemList;
    }


    /**
     * Sets the orderItemList value for this TravelCommerceSellRequestData.
     * 
     * @param orderItemList
     */
    public void setOrderItemList(com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem[] orderItemList) {
        this.orderItemList = orderItemList;
    }


    /**
     * Gets the cultureCode value for this TravelCommerceSellRequestData.
     * 
     * @return cultureCode
     */
    public java.lang.String getCultureCode() {
        return cultureCode;
    }


    /**
     * Sets the cultureCode value for this TravelCommerceSellRequestData.
     * 
     * @param cultureCode
     */
    public void setCultureCode(java.lang.String cultureCode) {
        this.cultureCode = cultureCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TravelCommerceSellRequestData)) return false;
        TravelCommerceSellRequestData other = (TravelCommerceSellRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.orderItemList==null && other.getOrderItemList()==null) || 
             (this.orderItemList!=null &&
              java.util.Arrays.equals(this.orderItemList, other.getOrderItemList()))) &&
            ((this.cultureCode==null && other.getCultureCode()==null) || 
             (this.cultureCode!=null &&
              this.cultureCode.equals(other.getCultureCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOrderItemList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCultureCode() != null) {
            _hashCode += getCultureCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TravelCommerceSellRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelCommerceSellRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OrderItemList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCOrderItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCOrderItem"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cultureCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CultureCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
