/**
 * CaptureBaggageEventRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class CaptureBaggageEventRequestData  implements java.io.Serializable {
    private java.lang.String baggageEventCode;

    private java.lang.String OSTag;

    private java.lang.Long baggageID;

    private java.lang.String eventText;

    private java.lang.String locationCode;

    private java.lang.String toLocationCode;

    public CaptureBaggageEventRequestData() {
    }

    public CaptureBaggageEventRequestData(
           java.lang.String baggageEventCode,
           java.lang.String OSTag,
           java.lang.Long baggageID,
           java.lang.String eventText,
           java.lang.String locationCode,
           java.lang.String toLocationCode) {
           this.baggageEventCode = baggageEventCode;
           this.OSTag = OSTag;
           this.baggageID = baggageID;
           this.eventText = eventText;
           this.locationCode = locationCode;
           this.toLocationCode = toLocationCode;
    }


    /**
     * Gets the baggageEventCode value for this CaptureBaggageEventRequestData.
     * 
     * @return baggageEventCode
     */
    public java.lang.String getBaggageEventCode() {
        return baggageEventCode;
    }


    /**
     * Sets the baggageEventCode value for this CaptureBaggageEventRequestData.
     * 
     * @param baggageEventCode
     */
    public void setBaggageEventCode(java.lang.String baggageEventCode) {
        this.baggageEventCode = baggageEventCode;
    }


    /**
     * Gets the OSTag value for this CaptureBaggageEventRequestData.
     * 
     * @return OSTag
     */
    public java.lang.String getOSTag() {
        return OSTag;
    }


    /**
     * Sets the OSTag value for this CaptureBaggageEventRequestData.
     * 
     * @param OSTag
     */
    public void setOSTag(java.lang.String OSTag) {
        this.OSTag = OSTag;
    }


    /**
     * Gets the baggageID value for this CaptureBaggageEventRequestData.
     * 
     * @return baggageID
     */
    public java.lang.Long getBaggageID() {
        return baggageID;
    }


    /**
     * Sets the baggageID value for this CaptureBaggageEventRequestData.
     * 
     * @param baggageID
     */
    public void setBaggageID(java.lang.Long baggageID) {
        this.baggageID = baggageID;
    }


    /**
     * Gets the eventText value for this CaptureBaggageEventRequestData.
     * 
     * @return eventText
     */
    public java.lang.String getEventText() {
        return eventText;
    }


    /**
     * Sets the eventText value for this CaptureBaggageEventRequestData.
     * 
     * @param eventText
     */
    public void setEventText(java.lang.String eventText) {
        this.eventText = eventText;
    }


    /**
     * Gets the locationCode value for this CaptureBaggageEventRequestData.
     * 
     * @return locationCode
     */
    public java.lang.String getLocationCode() {
        return locationCode;
    }


    /**
     * Sets the locationCode value for this CaptureBaggageEventRequestData.
     * 
     * @param locationCode
     */
    public void setLocationCode(java.lang.String locationCode) {
        this.locationCode = locationCode;
    }


    /**
     * Gets the toLocationCode value for this CaptureBaggageEventRequestData.
     * 
     * @return toLocationCode
     */
    public java.lang.String getToLocationCode() {
        return toLocationCode;
    }


    /**
     * Sets the toLocationCode value for this CaptureBaggageEventRequestData.
     * 
     * @param toLocationCode
     */
    public void setToLocationCode(java.lang.String toLocationCode) {
        this.toLocationCode = toLocationCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CaptureBaggageEventRequestData)) return false;
        CaptureBaggageEventRequestData other = (CaptureBaggageEventRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.baggageEventCode==null && other.getBaggageEventCode()==null) || 
             (this.baggageEventCode!=null &&
              this.baggageEventCode.equals(other.getBaggageEventCode()))) &&
            ((this.OSTag==null && other.getOSTag()==null) || 
             (this.OSTag!=null &&
              this.OSTag.equals(other.getOSTag()))) &&
            ((this.baggageID==null && other.getBaggageID()==null) || 
             (this.baggageID!=null &&
              this.baggageID.equals(other.getBaggageID()))) &&
            ((this.eventText==null && other.getEventText()==null) || 
             (this.eventText!=null &&
              this.eventText.equals(other.getEventText()))) &&
            ((this.locationCode==null && other.getLocationCode()==null) || 
             (this.locationCode!=null &&
              this.locationCode.equals(other.getLocationCode()))) &&
            ((this.toLocationCode==null && other.getToLocationCode()==null) || 
             (this.toLocationCode!=null &&
              this.toLocationCode.equals(other.getToLocationCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBaggageEventCode() != null) {
            _hashCode += getBaggageEventCode().hashCode();
        }
        if (getOSTag() != null) {
            _hashCode += getOSTag().hashCode();
        }
        if (getBaggageID() != null) {
            _hashCode += getBaggageID().hashCode();
        }
        if (getEventText() != null) {
            _hashCode += getEventText().hashCode();
        }
        if (getLocationCode() != null) {
            _hashCode += getLocationCode().hashCode();
        }
        if (getToLocationCode() != null) {
            _hashCode += getToLocationCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CaptureBaggageEventRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CaptureBaggageEventRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageEventCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageEventCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSTag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OSTag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EventText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LocationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toLocationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ToLocationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
