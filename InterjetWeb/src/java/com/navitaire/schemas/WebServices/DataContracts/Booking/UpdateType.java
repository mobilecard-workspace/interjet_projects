/**
 * UpdateType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class UpdateType  implements java.io.Serializable {
    private java.lang.Boolean none;

    private java.lang.Boolean payments;

    private java.lang.Boolean participants;

    public UpdateType() {
    }

    public UpdateType(
           java.lang.Boolean none,
           java.lang.Boolean payments,
           java.lang.Boolean participants) {
           this.none = none;
           this.payments = payments;
           this.participants = participants;
    }


    /**
     * Gets the none value for this UpdateType.
     * 
     * @return none
     */
    public java.lang.Boolean getNone() {
        return none;
    }


    /**
     * Sets the none value for this UpdateType.
     * 
     * @param none
     */
    public void setNone(java.lang.Boolean none) {
        this.none = none;
    }


    /**
     * Gets the payments value for this UpdateType.
     * 
     * @return payments
     */
    public java.lang.Boolean getPayments() {
        return payments;
    }


    /**
     * Sets the payments value for this UpdateType.
     * 
     * @param payments
     */
    public void setPayments(java.lang.Boolean payments) {
        this.payments = payments;
    }


    /**
     * Gets the participants value for this UpdateType.
     * 
     * @return participants
     */
    public java.lang.Boolean getParticipants() {
        return participants;
    }


    /**
     * Sets the participants value for this UpdateType.
     * 
     * @param participants
     */
    public void setParticipants(java.lang.Boolean participants) {
        this.participants = participants;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateType)) return false;
        UpdateType other = (UpdateType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.none==null && other.getNone()==null) || 
             (this.none!=null &&
              this.none.equals(other.getNone()))) &&
            ((this.payments==null && other.getPayments()==null) || 
             (this.payments!=null &&
              this.payments.equals(other.getPayments()))) &&
            ((this.participants==null && other.getParticipants()==null) || 
             (this.participants!=null &&
              this.participants.equals(other.getParticipants())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNone() != null) {
            _hashCode += getNone().hashCode();
        }
        if (getPayments() != null) {
            _hashCode += getPayments().hashCode();
        }
        if (getParticipants() != null) {
            _hashCode += getParticipants().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("none");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "None"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("participants");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Participants"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
