/**
 * CancelJourneyRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class CancelJourneyRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] journeys;

    private java.lang.Boolean waivePenaltyFee;

    private java.lang.Boolean waiveSpoilageFee;

    private java.lang.Boolean preventReprice;

    public CancelJourneyRequest() {
    }

    public CancelJourneyRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] journeys,
           java.lang.Boolean waivePenaltyFee,
           java.lang.Boolean waiveSpoilageFee,
           java.lang.Boolean preventReprice) {
           this.journeys = journeys;
           this.waivePenaltyFee = waivePenaltyFee;
           this.waiveSpoilageFee = waiveSpoilageFee;
           this.preventReprice = preventReprice;
    }


    /**
     * Gets the journeys value for this CancelJourneyRequest.
     * 
     * @return journeys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] getJourneys() {
        return journeys;
    }


    /**
     * Sets the journeys value for this CancelJourneyRequest.
     * 
     * @param journeys
     */
    public void setJourneys(com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[] journeys) {
        this.journeys = journeys;
    }


    /**
     * Gets the waivePenaltyFee value for this CancelJourneyRequest.
     * 
     * @return waivePenaltyFee
     */
    public java.lang.Boolean getWaivePenaltyFee() {
        return waivePenaltyFee;
    }


    /**
     * Sets the waivePenaltyFee value for this CancelJourneyRequest.
     * 
     * @param waivePenaltyFee
     */
    public void setWaivePenaltyFee(java.lang.Boolean waivePenaltyFee) {
        this.waivePenaltyFee = waivePenaltyFee;
    }


    /**
     * Gets the waiveSpoilageFee value for this CancelJourneyRequest.
     * 
     * @return waiveSpoilageFee
     */
    public java.lang.Boolean getWaiveSpoilageFee() {
        return waiveSpoilageFee;
    }


    /**
     * Sets the waiveSpoilageFee value for this CancelJourneyRequest.
     * 
     * @param waiveSpoilageFee
     */
    public void setWaiveSpoilageFee(java.lang.Boolean waiveSpoilageFee) {
        this.waiveSpoilageFee = waiveSpoilageFee;
    }


    /**
     * Gets the preventReprice value for this CancelJourneyRequest.
     * 
     * @return preventReprice
     */
    public java.lang.Boolean getPreventReprice() {
        return preventReprice;
    }


    /**
     * Sets the preventReprice value for this CancelJourneyRequest.
     * 
     * @param preventReprice
     */
    public void setPreventReprice(java.lang.Boolean preventReprice) {
        this.preventReprice = preventReprice;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CancelJourneyRequest)) return false;
        CancelJourneyRequest other = (CancelJourneyRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.journeys==null && other.getJourneys()==null) || 
             (this.journeys!=null &&
              java.util.Arrays.equals(this.journeys, other.getJourneys()))) &&
            ((this.waivePenaltyFee==null && other.getWaivePenaltyFee()==null) || 
             (this.waivePenaltyFee!=null &&
              this.waivePenaltyFee.equals(other.getWaivePenaltyFee()))) &&
            ((this.waiveSpoilageFee==null && other.getWaiveSpoilageFee()==null) || 
             (this.waiveSpoilageFee!=null &&
              this.waiveSpoilageFee.equals(other.getWaiveSpoilageFee()))) &&
            ((this.preventReprice==null && other.getPreventReprice()==null) || 
             (this.preventReprice!=null &&
              this.preventReprice.equals(other.getPreventReprice())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getJourneys() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJourneys());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJourneys(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getWaivePenaltyFee() != null) {
            _hashCode += getWaivePenaltyFee().hashCode();
        }
        if (getWaiveSpoilageFee() != null) {
            _hashCode += getWaiveSpoilageFee().hashCode();
        }
        if (getPreventReprice() != null) {
            _hashCode += getPreventReprice().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CancelJourneyRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelJourneyRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journeys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waivePenaltyFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaivePenaltyFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waiveSpoilageFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaiveSpoilageFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preventReprice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PreventReprice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
