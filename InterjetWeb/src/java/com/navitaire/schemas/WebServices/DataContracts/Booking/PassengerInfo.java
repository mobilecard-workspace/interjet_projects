/**
 * PassengerInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PassengerInfo  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.math.BigDecimal balanceDue;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.Gender gender;

    private java.lang.String nationality;

    private java.lang.String residentCountry;

    private java.math.BigDecimal totalCost;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightCategory weightCategory;

    public PassengerInfo() {
    }

    public PassengerInfo(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.math.BigDecimal balanceDue,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.Gender gender,
           java.lang.String nationality,
           java.lang.String residentCountry,
           java.math.BigDecimal totalCost,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightCategory weightCategory) {
        super(
            state);
        this.balanceDue = balanceDue;
        this.gender = gender;
        this.nationality = nationality;
        this.residentCountry = residentCountry;
        this.totalCost = totalCost;
        this.weightCategory = weightCategory;
    }


    /**
     * Gets the balanceDue value for this PassengerInfo.
     * 
     * @return balanceDue
     */
    public java.math.BigDecimal getBalanceDue() {
        return balanceDue;
    }


    /**
     * Sets the balanceDue value for this PassengerInfo.
     * 
     * @param balanceDue
     */
    public void setBalanceDue(java.math.BigDecimal balanceDue) {
        this.balanceDue = balanceDue;
    }


    /**
     * Gets the gender value for this PassengerInfo.
     * 
     * @return gender
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.Gender getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this PassengerInfo.
     * 
     * @param gender
     */
    public void setGender(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.Gender gender) {
        this.gender = gender;
    }


    /**
     * Gets the nationality value for this PassengerInfo.
     * 
     * @return nationality
     */
    public java.lang.String getNationality() {
        return nationality;
    }


    /**
     * Sets the nationality value for this PassengerInfo.
     * 
     * @param nationality
     */
    public void setNationality(java.lang.String nationality) {
        this.nationality = nationality;
    }


    /**
     * Gets the residentCountry value for this PassengerInfo.
     * 
     * @return residentCountry
     */
    public java.lang.String getResidentCountry() {
        return residentCountry;
    }


    /**
     * Sets the residentCountry value for this PassengerInfo.
     * 
     * @param residentCountry
     */
    public void setResidentCountry(java.lang.String residentCountry) {
        this.residentCountry = residentCountry;
    }


    /**
     * Gets the totalCost value for this PassengerInfo.
     * 
     * @return totalCost
     */
    public java.math.BigDecimal getTotalCost() {
        return totalCost;
    }


    /**
     * Sets the totalCost value for this PassengerInfo.
     * 
     * @param totalCost
     */
    public void setTotalCost(java.math.BigDecimal totalCost) {
        this.totalCost = totalCost;
    }


    /**
     * Gets the weightCategory value for this PassengerInfo.
     * 
     * @return weightCategory
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightCategory getWeightCategory() {
        return weightCategory;
    }


    /**
     * Sets the weightCategory value for this PassengerInfo.
     * 
     * @param weightCategory
     */
    public void setWeightCategory(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightCategory weightCategory) {
        this.weightCategory = weightCategory;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PassengerInfo)) return false;
        PassengerInfo other = (PassengerInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.balanceDue==null && other.getBalanceDue()==null) || 
             (this.balanceDue!=null &&
              this.balanceDue.equals(other.getBalanceDue()))) &&
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              this.gender.equals(other.getGender()))) &&
            ((this.nationality==null && other.getNationality()==null) || 
             (this.nationality!=null &&
              this.nationality.equals(other.getNationality()))) &&
            ((this.residentCountry==null && other.getResidentCountry()==null) || 
             (this.residentCountry!=null &&
              this.residentCountry.equals(other.getResidentCountry()))) &&
            ((this.totalCost==null && other.getTotalCost()==null) || 
             (this.totalCost!=null &&
              this.totalCost.equals(other.getTotalCost()))) &&
            ((this.weightCategory==null && other.getWeightCategory()==null) || 
             (this.weightCategory!=null &&
              this.weightCategory.equals(other.getWeightCategory())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBalanceDue() != null) {
            _hashCode += getBalanceDue().hashCode();
        }
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getNationality() != null) {
            _hashCode += getNationality().hashCode();
        }
        if (getResidentCountry() != null) {
            _hashCode += getResidentCountry().hashCode();
        }
        if (getTotalCost() != null) {
            _hashCode += getTotalCost().hashCode();
        }
        if (getWeightCategory() != null) {
            _hashCode += getWeightCategory().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PassengerInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceDue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BalanceDue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Gender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "Gender"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nationality");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Nationality"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("residentCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ResidentCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalCost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TotalCost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weightCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WeightCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "WeightCategory"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
