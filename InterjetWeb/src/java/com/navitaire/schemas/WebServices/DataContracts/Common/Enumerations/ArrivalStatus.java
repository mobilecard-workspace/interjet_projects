/**
 * ArrivalStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class ArrivalStatus implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ArrivalStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Default = "Default";
    public static final java.lang.String _Cancelled = "Cancelled";
    public static final java.lang.String _Arrived = "Arrived";
    public static final java.lang.String _SeeAgent = "SeeAgent";
    public static final java.lang.String _Delayed = "Delayed";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final ArrivalStatus Default = new ArrivalStatus(_Default);
    public static final ArrivalStatus Cancelled = new ArrivalStatus(_Cancelled);
    public static final ArrivalStatus Arrived = new ArrivalStatus(_Arrived);
    public static final ArrivalStatus SeeAgent = new ArrivalStatus(_SeeAgent);
    public static final ArrivalStatus Delayed = new ArrivalStatus(_Delayed);
    public static final ArrivalStatus Unmapped = new ArrivalStatus(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static ArrivalStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ArrivalStatus enumeration = (ArrivalStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ArrivalStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArrivalStatus.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ArrivalStatus"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
