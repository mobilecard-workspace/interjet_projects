/**
 * AvailablePaxSSR.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class AvailablePaxSSR  implements java.io.Serializable {
    private java.lang.String SSRCode;

    private java.lang.Boolean inventoryControlled;

    private java.lang.Boolean nonInventoryControlled;

    private java.lang.Boolean seatDependent;

    private java.lang.Boolean nonSeatDependent;

    private java.lang.Short available;

    private short[] passengerNumberList;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSRPrice[] paxSSRPriceList;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SSRLeg[] SSRLegList;

    public AvailablePaxSSR() {
    }

    public AvailablePaxSSR(
           java.lang.String SSRCode,
           java.lang.Boolean inventoryControlled,
           java.lang.Boolean nonInventoryControlled,
           java.lang.Boolean seatDependent,
           java.lang.Boolean nonSeatDependent,
           java.lang.Short available,
           short[] passengerNumberList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSRPrice[] paxSSRPriceList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SSRLeg[] SSRLegList) {
           this.SSRCode = SSRCode;
           this.inventoryControlled = inventoryControlled;
           this.nonInventoryControlled = nonInventoryControlled;
           this.seatDependent = seatDependent;
           this.nonSeatDependent = nonSeatDependent;
           this.available = available;
           this.passengerNumberList = passengerNumberList;
           this.paxSSRPriceList = paxSSRPriceList;
           this.SSRLegList = SSRLegList;
    }


    /**
     * Gets the SSRCode value for this AvailablePaxSSR.
     * 
     * @return SSRCode
     */
    public java.lang.String getSSRCode() {
        return SSRCode;
    }


    /**
     * Sets the SSRCode value for this AvailablePaxSSR.
     * 
     * @param SSRCode
     */
    public void setSSRCode(java.lang.String SSRCode) {
        this.SSRCode = SSRCode;
    }


    /**
     * Gets the inventoryControlled value for this AvailablePaxSSR.
     * 
     * @return inventoryControlled
     */
    public java.lang.Boolean getInventoryControlled() {
        return inventoryControlled;
    }


    /**
     * Sets the inventoryControlled value for this AvailablePaxSSR.
     * 
     * @param inventoryControlled
     */
    public void setInventoryControlled(java.lang.Boolean inventoryControlled) {
        this.inventoryControlled = inventoryControlled;
    }


    /**
     * Gets the nonInventoryControlled value for this AvailablePaxSSR.
     * 
     * @return nonInventoryControlled
     */
    public java.lang.Boolean getNonInventoryControlled() {
        return nonInventoryControlled;
    }


    /**
     * Sets the nonInventoryControlled value for this AvailablePaxSSR.
     * 
     * @param nonInventoryControlled
     */
    public void setNonInventoryControlled(java.lang.Boolean nonInventoryControlled) {
        this.nonInventoryControlled = nonInventoryControlled;
    }


    /**
     * Gets the seatDependent value for this AvailablePaxSSR.
     * 
     * @return seatDependent
     */
    public java.lang.Boolean getSeatDependent() {
        return seatDependent;
    }


    /**
     * Sets the seatDependent value for this AvailablePaxSSR.
     * 
     * @param seatDependent
     */
    public void setSeatDependent(java.lang.Boolean seatDependent) {
        this.seatDependent = seatDependent;
    }


    /**
     * Gets the nonSeatDependent value for this AvailablePaxSSR.
     * 
     * @return nonSeatDependent
     */
    public java.lang.Boolean getNonSeatDependent() {
        return nonSeatDependent;
    }


    /**
     * Sets the nonSeatDependent value for this AvailablePaxSSR.
     * 
     * @param nonSeatDependent
     */
    public void setNonSeatDependent(java.lang.Boolean nonSeatDependent) {
        this.nonSeatDependent = nonSeatDependent;
    }


    /**
     * Gets the available value for this AvailablePaxSSR.
     * 
     * @return available
     */
    public java.lang.Short getAvailable() {
        return available;
    }


    /**
     * Sets the available value for this AvailablePaxSSR.
     * 
     * @param available
     */
    public void setAvailable(java.lang.Short available) {
        this.available = available;
    }


    /**
     * Gets the passengerNumberList value for this AvailablePaxSSR.
     * 
     * @return passengerNumberList
     */
    public short[] getPassengerNumberList() {
        return passengerNumberList;
    }


    /**
     * Sets the passengerNumberList value for this AvailablePaxSSR.
     * 
     * @param passengerNumberList
     */
    public void setPassengerNumberList(short[] passengerNumberList) {
        this.passengerNumberList = passengerNumberList;
    }


    /**
     * Gets the paxSSRPriceList value for this AvailablePaxSSR.
     * 
     * @return paxSSRPriceList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSRPrice[] getPaxSSRPriceList() {
        return paxSSRPriceList;
    }


    /**
     * Sets the paxSSRPriceList value for this AvailablePaxSSR.
     * 
     * @param paxSSRPriceList
     */
    public void setPaxSSRPriceList(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSRPrice[] paxSSRPriceList) {
        this.paxSSRPriceList = paxSSRPriceList;
    }


    /**
     * Gets the SSRLegList value for this AvailablePaxSSR.
     * 
     * @return SSRLegList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SSRLeg[] getSSRLegList() {
        return SSRLegList;
    }


    /**
     * Sets the SSRLegList value for this AvailablePaxSSR.
     * 
     * @param SSRLegList
     */
    public void setSSRLegList(com.navitaire.schemas.WebServices.DataContracts.Booking.SSRLeg[] SSRLegList) {
        this.SSRLegList = SSRLegList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AvailablePaxSSR)) return false;
        AvailablePaxSSR other = (AvailablePaxSSR) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.SSRCode==null && other.getSSRCode()==null) || 
             (this.SSRCode!=null &&
              this.SSRCode.equals(other.getSSRCode()))) &&
            ((this.inventoryControlled==null && other.getInventoryControlled()==null) || 
             (this.inventoryControlled!=null &&
              this.inventoryControlled.equals(other.getInventoryControlled()))) &&
            ((this.nonInventoryControlled==null && other.getNonInventoryControlled()==null) || 
             (this.nonInventoryControlled!=null &&
              this.nonInventoryControlled.equals(other.getNonInventoryControlled()))) &&
            ((this.seatDependent==null && other.getSeatDependent()==null) || 
             (this.seatDependent!=null &&
              this.seatDependent.equals(other.getSeatDependent()))) &&
            ((this.nonSeatDependent==null && other.getNonSeatDependent()==null) || 
             (this.nonSeatDependent!=null &&
              this.nonSeatDependent.equals(other.getNonSeatDependent()))) &&
            ((this.available==null && other.getAvailable()==null) || 
             (this.available!=null &&
              this.available.equals(other.getAvailable()))) &&
            ((this.passengerNumberList==null && other.getPassengerNumberList()==null) || 
             (this.passengerNumberList!=null &&
              java.util.Arrays.equals(this.passengerNumberList, other.getPassengerNumberList()))) &&
            ((this.paxSSRPriceList==null && other.getPaxSSRPriceList()==null) || 
             (this.paxSSRPriceList!=null &&
              java.util.Arrays.equals(this.paxSSRPriceList, other.getPaxSSRPriceList()))) &&
            ((this.SSRLegList==null && other.getSSRLegList()==null) || 
             (this.SSRLegList!=null &&
              java.util.Arrays.equals(this.SSRLegList, other.getSSRLegList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSSRCode() != null) {
            _hashCode += getSSRCode().hashCode();
        }
        if (getInventoryControlled() != null) {
            _hashCode += getInventoryControlled().hashCode();
        }
        if (getNonInventoryControlled() != null) {
            _hashCode += getNonInventoryControlled().hashCode();
        }
        if (getSeatDependent() != null) {
            _hashCode += getSeatDependent().hashCode();
        }
        if (getNonSeatDependent() != null) {
            _hashCode += getNonSeatDependent().hashCode();
        }
        if (getAvailable() != null) {
            _hashCode += getAvailable().hashCode();
        }
        if (getPassengerNumberList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerNumberList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerNumberList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxSSRPriceList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxSSRPriceList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxSSRPriceList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSSRLegList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSSRLegList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSSRLegList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AvailablePaxSSR.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailablePaxSSR"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inventoryControlled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InventoryControlled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nonInventoryControlled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NonInventoryControlled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatDependent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatDependent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nonSeatDependent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NonSeatDependent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("available");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Available"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumberList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumberList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "short"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxSSRPriceList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSRPriceList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSRPrice"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSRPrice"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRLegList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRLegList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRLeg"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRLeg"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
