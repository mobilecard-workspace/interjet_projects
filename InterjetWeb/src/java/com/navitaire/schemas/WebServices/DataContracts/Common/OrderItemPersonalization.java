/**
 * OrderItemPersonalization.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class OrderItemPersonalization  implements java.io.Serializable {
    private java.lang.Boolean priceIsValid;

    private java.lang.Boolean required;

    private java.lang.String personalizationCode;

    private java.lang.String personalizationDescription;

    private com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem personalizationPrice;

    private java.lang.Integer parentSequence;

    private java.math.BigDecimal personalizationPriceTotal;

    private java.lang.String value;

    private java.lang.Integer quantity;

    public OrderItemPersonalization() {
    }

    public OrderItemPersonalization(
           java.lang.Boolean priceIsValid,
           java.lang.Boolean required,
           java.lang.String personalizationCode,
           java.lang.String personalizationDescription,
           com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem personalizationPrice,
           java.lang.Integer parentSequence,
           java.math.BigDecimal personalizationPriceTotal,
           java.lang.String value,
           java.lang.Integer quantity) {
           this.priceIsValid = priceIsValid;
           this.required = required;
           this.personalizationCode = personalizationCode;
           this.personalizationDescription = personalizationDescription;
           this.personalizationPrice = personalizationPrice;
           this.parentSequence = parentSequence;
           this.personalizationPriceTotal = personalizationPriceTotal;
           this.value = value;
           this.quantity = quantity;
    }


    /**
     * Gets the priceIsValid value for this OrderItemPersonalization.
     * 
     * @return priceIsValid
     */
    public java.lang.Boolean getPriceIsValid() {
        return priceIsValid;
    }


    /**
     * Sets the priceIsValid value for this OrderItemPersonalization.
     * 
     * @param priceIsValid
     */
    public void setPriceIsValid(java.lang.Boolean priceIsValid) {
        this.priceIsValid = priceIsValid;
    }


    /**
     * Gets the required value for this OrderItemPersonalization.
     * 
     * @return required
     */
    public java.lang.Boolean getRequired() {
        return required;
    }


    /**
     * Sets the required value for this OrderItemPersonalization.
     * 
     * @param required
     */
    public void setRequired(java.lang.Boolean required) {
        this.required = required;
    }


    /**
     * Gets the personalizationCode value for this OrderItemPersonalization.
     * 
     * @return personalizationCode
     */
    public java.lang.String getPersonalizationCode() {
        return personalizationCode;
    }


    /**
     * Sets the personalizationCode value for this OrderItemPersonalization.
     * 
     * @param personalizationCode
     */
    public void setPersonalizationCode(java.lang.String personalizationCode) {
        this.personalizationCode = personalizationCode;
    }


    /**
     * Gets the personalizationDescription value for this OrderItemPersonalization.
     * 
     * @return personalizationDescription
     */
    public java.lang.String getPersonalizationDescription() {
        return personalizationDescription;
    }


    /**
     * Sets the personalizationDescription value for this OrderItemPersonalization.
     * 
     * @param personalizationDescription
     */
    public void setPersonalizationDescription(java.lang.String personalizationDescription) {
        this.personalizationDescription = personalizationDescription;
    }


    /**
     * Gets the personalizationPrice value for this OrderItemPersonalization.
     * 
     * @return personalizationPrice
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem getPersonalizationPrice() {
        return personalizationPrice;
    }


    /**
     * Sets the personalizationPrice value for this OrderItemPersonalization.
     * 
     * @param personalizationPrice
     */
    public void setPersonalizationPrice(com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem personalizationPrice) {
        this.personalizationPrice = personalizationPrice;
    }


    /**
     * Gets the parentSequence value for this OrderItemPersonalization.
     * 
     * @return parentSequence
     */
    public java.lang.Integer getParentSequence() {
        return parentSequence;
    }


    /**
     * Sets the parentSequence value for this OrderItemPersonalization.
     * 
     * @param parentSequence
     */
    public void setParentSequence(java.lang.Integer parentSequence) {
        this.parentSequence = parentSequence;
    }


    /**
     * Gets the personalizationPriceTotal value for this OrderItemPersonalization.
     * 
     * @return personalizationPriceTotal
     */
    public java.math.BigDecimal getPersonalizationPriceTotal() {
        return personalizationPriceTotal;
    }


    /**
     * Sets the personalizationPriceTotal value for this OrderItemPersonalization.
     * 
     * @param personalizationPriceTotal
     */
    public void setPersonalizationPriceTotal(java.math.BigDecimal personalizationPriceTotal) {
        this.personalizationPriceTotal = personalizationPriceTotal;
    }


    /**
     * Gets the value value for this OrderItemPersonalization.
     * 
     * @return value
     */
    public java.lang.String getValue() {
        return value;
    }


    /**
     * Sets the value value for this OrderItemPersonalization.
     * 
     * @param value
     */
    public void setValue(java.lang.String value) {
        this.value = value;
    }


    /**
     * Gets the quantity value for this OrderItemPersonalization.
     * 
     * @return quantity
     */
    public java.lang.Integer getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this OrderItemPersonalization.
     * 
     * @param quantity
     */
    public void setQuantity(java.lang.Integer quantity) {
        this.quantity = quantity;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderItemPersonalization)) return false;
        OrderItemPersonalization other = (OrderItemPersonalization) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.priceIsValid==null && other.getPriceIsValid()==null) || 
             (this.priceIsValid!=null &&
              this.priceIsValid.equals(other.getPriceIsValid()))) &&
            ((this.required==null && other.getRequired()==null) || 
             (this.required!=null &&
              this.required.equals(other.getRequired()))) &&
            ((this.personalizationCode==null && other.getPersonalizationCode()==null) || 
             (this.personalizationCode!=null &&
              this.personalizationCode.equals(other.getPersonalizationCode()))) &&
            ((this.personalizationDescription==null && other.getPersonalizationDescription()==null) || 
             (this.personalizationDescription!=null &&
              this.personalizationDescription.equals(other.getPersonalizationDescription()))) &&
            ((this.personalizationPrice==null && other.getPersonalizationPrice()==null) || 
             (this.personalizationPrice!=null &&
              this.personalizationPrice.equals(other.getPersonalizationPrice()))) &&
            ((this.parentSequence==null && other.getParentSequence()==null) || 
             (this.parentSequence!=null &&
              this.parentSequence.equals(other.getParentSequence()))) &&
            ((this.personalizationPriceTotal==null && other.getPersonalizationPriceTotal()==null) || 
             (this.personalizationPriceTotal!=null &&
              this.personalizationPriceTotal.equals(other.getPersonalizationPriceTotal()))) &&
            ((this.value==null && other.getValue()==null) || 
             (this.value!=null &&
              this.value.equals(other.getValue()))) &&
            ((this.quantity==null && other.getQuantity()==null) || 
             (this.quantity!=null &&
              this.quantity.equals(other.getQuantity())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPriceIsValid() != null) {
            _hashCode += getPriceIsValid().hashCode();
        }
        if (getRequired() != null) {
            _hashCode += getRequired().hashCode();
        }
        if (getPersonalizationCode() != null) {
            _hashCode += getPersonalizationCode().hashCode();
        }
        if (getPersonalizationDescription() != null) {
            _hashCode += getPersonalizationDescription().hashCode();
        }
        if (getPersonalizationPrice() != null) {
            _hashCode += getPersonalizationPrice().hashCode();
        }
        if (getParentSequence() != null) {
            _hashCode += getParentSequence().hashCode();
        }
        if (getPersonalizationPriceTotal() != null) {
            _hashCode += getPersonalizationPriceTotal().hashCode();
        }
        if (getValue() != null) {
            _hashCode += getValue().hashCode();
        }
        if (getQuantity() != null) {
            _hashCode += getQuantity().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderItemPersonalization.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemPersonalization"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priceIsValid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PriceIsValid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("required");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Required"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personalizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PersonalizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personalizationDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PersonalizationDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personalizationPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PersonalizationPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ChargeableItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ParentSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personalizationPriceTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PersonalizationPriceTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("value");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Value"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
