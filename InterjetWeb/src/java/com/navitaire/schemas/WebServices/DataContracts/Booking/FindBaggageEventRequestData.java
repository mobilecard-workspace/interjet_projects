/**
 * FindBaggageEventRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class FindBaggageEventRequestData  implements java.io.Serializable {
    private java.lang.String OSTag;

    private java.util.Calendar OSTagStartDate;

    private java.util.Calendar OSTagEndDate;

    public FindBaggageEventRequestData() {
    }

    public FindBaggageEventRequestData(
           java.lang.String OSTag,
           java.util.Calendar OSTagStartDate,
           java.util.Calendar OSTagEndDate) {
           this.OSTag = OSTag;
           this.OSTagStartDate = OSTagStartDate;
           this.OSTagEndDate = OSTagEndDate;
    }


    /**
     * Gets the OSTag value for this FindBaggageEventRequestData.
     * 
     * @return OSTag
     */
    public java.lang.String getOSTag() {
        return OSTag;
    }


    /**
     * Sets the OSTag value for this FindBaggageEventRequestData.
     * 
     * @param OSTag
     */
    public void setOSTag(java.lang.String OSTag) {
        this.OSTag = OSTag;
    }


    /**
     * Gets the OSTagStartDate value for this FindBaggageEventRequestData.
     * 
     * @return OSTagStartDate
     */
    public java.util.Calendar getOSTagStartDate() {
        return OSTagStartDate;
    }


    /**
     * Sets the OSTagStartDate value for this FindBaggageEventRequestData.
     * 
     * @param OSTagStartDate
     */
    public void setOSTagStartDate(java.util.Calendar OSTagStartDate) {
        this.OSTagStartDate = OSTagStartDate;
    }


    /**
     * Gets the OSTagEndDate value for this FindBaggageEventRequestData.
     * 
     * @return OSTagEndDate
     */
    public java.util.Calendar getOSTagEndDate() {
        return OSTagEndDate;
    }


    /**
     * Sets the OSTagEndDate value for this FindBaggageEventRequestData.
     * 
     * @param OSTagEndDate
     */
    public void setOSTagEndDate(java.util.Calendar OSTagEndDate) {
        this.OSTagEndDate = OSTagEndDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindBaggageEventRequestData)) return false;
        FindBaggageEventRequestData other = (FindBaggageEventRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OSTag==null && other.getOSTag()==null) || 
             (this.OSTag!=null &&
              this.OSTag.equals(other.getOSTag()))) &&
            ((this.OSTagStartDate==null && other.getOSTagStartDate()==null) || 
             (this.OSTagStartDate!=null &&
              this.OSTagStartDate.equals(other.getOSTagStartDate()))) &&
            ((this.OSTagEndDate==null && other.getOSTagEndDate()==null) || 
             (this.OSTagEndDate!=null &&
              this.OSTagEndDate.equals(other.getOSTagEndDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOSTag() != null) {
            _hashCode += getOSTag().hashCode();
        }
        if (getOSTagStartDate() != null) {
            _hashCode += getOSTagStartDate().hashCode();
        }
        if (getOSTagEndDate() != null) {
            _hashCode += getOSTagEndDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindBaggageEventRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBaggageEventRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSTag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OSTag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSTagStartDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OSTagStartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSTagEndDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OSTagEndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
