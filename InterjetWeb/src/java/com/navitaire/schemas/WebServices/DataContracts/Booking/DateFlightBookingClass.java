/**
 * DateFlightBookingClass.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DateFlightBookingClass  implements java.io.Serializable {
    private java.lang.String bookingClass;

    private java.lang.Short availableCount;

    public DateFlightBookingClass() {
    }

    public DateFlightBookingClass(
           java.lang.String bookingClass,
           java.lang.Short availableCount) {
           this.bookingClass = bookingClass;
           this.availableCount = availableCount;
    }


    /**
     * Gets the bookingClass value for this DateFlightBookingClass.
     * 
     * @return bookingClass
     */
    public java.lang.String getBookingClass() {
        return bookingClass;
    }


    /**
     * Sets the bookingClass value for this DateFlightBookingClass.
     * 
     * @param bookingClass
     */
    public void setBookingClass(java.lang.String bookingClass) {
        this.bookingClass = bookingClass;
    }


    /**
     * Gets the availableCount value for this DateFlightBookingClass.
     * 
     * @return availableCount
     */
    public java.lang.Short getAvailableCount() {
        return availableCount;
    }


    /**
     * Sets the availableCount value for this DateFlightBookingClass.
     * 
     * @param availableCount
     */
    public void setAvailableCount(java.lang.Short availableCount) {
        this.availableCount = availableCount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DateFlightBookingClass)) return false;
        DateFlightBookingClass other = (DateFlightBookingClass) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingClass==null && other.getBookingClass()==null) || 
             (this.bookingClass!=null &&
              this.bookingClass.equals(other.getBookingClass()))) &&
            ((this.availableCount==null && other.getAvailableCount()==null) || 
             (this.availableCount!=null &&
              this.availableCount.equals(other.getAvailableCount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingClass() != null) {
            _hashCode += getBookingClass().hashCode();
        }
        if (getAvailableCount() != null) {
            _hashCode += getAvailableCount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DateFlightBookingClass.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightBookingClass"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingClass");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingClass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailableCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
