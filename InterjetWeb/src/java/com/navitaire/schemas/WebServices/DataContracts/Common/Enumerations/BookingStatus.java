/**
 * BookingStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class BookingStatus implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected BookingStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Default = "Default";
    public static final java.lang.String _Hold = "Hold";
    public static final java.lang.String _Confirmed = "Confirmed";
    public static final java.lang.String _Closed = "Closed";
    public static final java.lang.String _HoldCanceled = "HoldCanceled";
    public static final java.lang.String _PendingArchive = "PendingArchive";
    public static final java.lang.String _Archived = "Archived";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final BookingStatus Default = new BookingStatus(_Default);
    public static final BookingStatus Hold = new BookingStatus(_Hold);
    public static final BookingStatus Confirmed = new BookingStatus(_Confirmed);
    public static final BookingStatus Closed = new BookingStatus(_Closed);
    public static final BookingStatus HoldCanceled = new BookingStatus(_HoldCanceled);
    public static final BookingStatus PendingArchive = new BookingStatus(_PendingArchive);
    public static final BookingStatus Archived = new BookingStatus(_Archived);
    public static final BookingStatus Unmapped = new BookingStatus(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static BookingStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        BookingStatus enumeration = (BookingStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static BookingStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingStatus.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingStatus"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
