/**
 * BookingPaymentTransfer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingPaymentTransfer  implements java.io.Serializable {
    private java.lang.Long bookingPaymentID;

    private java.math.BigDecimal transferAmount;

    public BookingPaymentTransfer() {
    }

    public BookingPaymentTransfer(
           java.lang.Long bookingPaymentID,
           java.math.BigDecimal transferAmount) {
           this.bookingPaymentID = bookingPaymentID;
           this.transferAmount = transferAmount;
    }


    /**
     * Gets the bookingPaymentID value for this BookingPaymentTransfer.
     * 
     * @return bookingPaymentID
     */
    public java.lang.Long getBookingPaymentID() {
        return bookingPaymentID;
    }


    /**
     * Sets the bookingPaymentID value for this BookingPaymentTransfer.
     * 
     * @param bookingPaymentID
     */
    public void setBookingPaymentID(java.lang.Long bookingPaymentID) {
        this.bookingPaymentID = bookingPaymentID;
    }


    /**
     * Gets the transferAmount value for this BookingPaymentTransfer.
     * 
     * @return transferAmount
     */
    public java.math.BigDecimal getTransferAmount() {
        return transferAmount;
    }


    /**
     * Sets the transferAmount value for this BookingPaymentTransfer.
     * 
     * @param transferAmount
     */
    public void setTransferAmount(java.math.BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingPaymentTransfer)) return false;
        BookingPaymentTransfer other = (BookingPaymentTransfer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingPaymentID==null && other.getBookingPaymentID()==null) || 
             (this.bookingPaymentID!=null &&
              this.bookingPaymentID.equals(other.getBookingPaymentID()))) &&
            ((this.transferAmount==null && other.getTransferAmount()==null) || 
             (this.transferAmount!=null &&
              this.transferAmount.equals(other.getTransferAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingPaymentID() != null) {
            _hashCode += getBookingPaymentID().hashCode();
        }
        if (getTransferAmount() != null) {
            _hashCode += getTransferAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingPaymentTransfer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingPaymentTransfer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingPaymentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingPaymentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transferAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TransferAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
