/**
 * QueueAction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class QueueAction implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected QueueAction(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Default = "Default";
    public static final java.lang.String _Warning = "Warning";
    public static final java.lang.String _Lock = "Lock";
    public static final java.lang.String _DefaultAndNotify = "DefaultAndNotify";
    public static final java.lang.String _WarningAndNotify = "WarningAndNotify";
    public static final java.lang.String _LockAndNotify = "LockAndNotify";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final QueueAction Default = new QueueAction(_Default);
    public static final QueueAction Warning = new QueueAction(_Warning);
    public static final QueueAction Lock = new QueueAction(_Lock);
    public static final QueueAction DefaultAndNotify = new QueueAction(_DefaultAndNotify);
    public static final QueueAction WarningAndNotify = new QueueAction(_WarningAndNotify);
    public static final QueueAction LockAndNotify = new QueueAction(_LockAndNotify);
    public static final QueueAction Unmapped = new QueueAction(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static QueueAction fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        QueueAction enumeration = (QueueAction)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static QueueAction fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QueueAction.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "QueueAction"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
