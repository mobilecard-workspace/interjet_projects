/**
 * Passenger.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class Passenger  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram[] passengerPrograms;

    private java.lang.String customerNumber;

    private java.lang.Short passengerNumber;

    private java.lang.Short familyNumber;

    private java.lang.String paxDiscountCode;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName[] names;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant infant;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo passengerInfo;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram passengerProgram;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee[] passengerFees;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerAddress[] passengerAddresses;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTravelDocument[] passengerTravelDocuments;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerBag[] passengerBags;

    private java.lang.Long passengerID;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo[] passengerTypeInfos;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo[] passengerInfos;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant[] passengerInfants;

    private java.lang.Boolean pseudoPassenger;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo passengerTypeInfo;

    public Passenger() {
    }

    public Passenger(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram[] passengerPrograms,
           java.lang.String customerNumber,
           java.lang.Short passengerNumber,
           java.lang.Short familyNumber,
           java.lang.String paxDiscountCode,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName[] names,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant infant,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo passengerInfo,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram passengerProgram,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee[] passengerFees,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerAddress[] passengerAddresses,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTravelDocument[] passengerTravelDocuments,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerBag[] passengerBags,
           java.lang.Long passengerID,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo[] passengerTypeInfos,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo[] passengerInfos,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant[] passengerInfants,
           java.lang.Boolean pseudoPassenger,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo passengerTypeInfo) {
        super(
            state);
        this.passengerPrograms = passengerPrograms;
        this.customerNumber = customerNumber;
        this.passengerNumber = passengerNumber;
        this.familyNumber = familyNumber;
        this.paxDiscountCode = paxDiscountCode;
        this.names = names;
        this.infant = infant;
        this.passengerInfo = passengerInfo;
        this.passengerProgram = passengerProgram;
        this.passengerFees = passengerFees;
        this.passengerAddresses = passengerAddresses;
        this.passengerTravelDocuments = passengerTravelDocuments;
        this.passengerBags = passengerBags;
        this.passengerID = passengerID;
        this.passengerTypeInfos = passengerTypeInfos;
        this.passengerInfos = passengerInfos;
        this.passengerInfants = passengerInfants;
        this.pseudoPassenger = pseudoPassenger;
        this.passengerTypeInfo = passengerTypeInfo;
    }


    /**
     * Gets the passengerPrograms value for this Passenger.
     * 
     * @return passengerPrograms
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram[] getPassengerPrograms() {
        return passengerPrograms;
    }


    /**
     * Sets the passengerPrograms value for this Passenger.
     * 
     * @param passengerPrograms
     */
    public void setPassengerPrograms(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram[] passengerPrograms) {
        this.passengerPrograms = passengerPrograms;
    }


    /**
     * Gets the customerNumber value for this Passenger.
     * 
     * @return customerNumber
     */
    public java.lang.String getCustomerNumber() {
        return customerNumber;
    }


    /**
     * Sets the customerNumber value for this Passenger.
     * 
     * @param customerNumber
     */
    public void setCustomerNumber(java.lang.String customerNumber) {
        this.customerNumber = customerNumber;
    }


    /**
     * Gets the passengerNumber value for this Passenger.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this Passenger.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the familyNumber value for this Passenger.
     * 
     * @return familyNumber
     */
    public java.lang.Short getFamilyNumber() {
        return familyNumber;
    }


    /**
     * Sets the familyNumber value for this Passenger.
     * 
     * @param familyNumber
     */
    public void setFamilyNumber(java.lang.Short familyNumber) {
        this.familyNumber = familyNumber;
    }


    /**
     * Gets the paxDiscountCode value for this Passenger.
     * 
     * @return paxDiscountCode
     */
    public java.lang.String getPaxDiscountCode() {
        return paxDiscountCode;
    }


    /**
     * Sets the paxDiscountCode value for this Passenger.
     * 
     * @param paxDiscountCode
     */
    public void setPaxDiscountCode(java.lang.String paxDiscountCode) {
        this.paxDiscountCode = paxDiscountCode;
    }


    /**
     * Gets the names value for this Passenger.
     * 
     * @return names
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName[] getNames() {
        return names;
    }


    /**
     * Sets the names value for this Passenger.
     * 
     * @param names
     */
    public void setNames(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName[] names) {
        this.names = names;
    }


    /**
     * Gets the infant value for this Passenger.
     * 
     * @return infant
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant getInfant() {
        return infant;
    }


    /**
     * Sets the infant value for this Passenger.
     * 
     * @param infant
     */
    public void setInfant(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant infant) {
        this.infant = infant;
    }


    /**
     * Gets the passengerInfo value for this Passenger.
     * 
     * @return passengerInfo
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo getPassengerInfo() {
        return passengerInfo;
    }


    /**
     * Sets the passengerInfo value for this Passenger.
     * 
     * @param passengerInfo
     */
    public void setPassengerInfo(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo passengerInfo) {
        this.passengerInfo = passengerInfo;
    }


    /**
     * Gets the passengerProgram value for this Passenger.
     * 
     * @return passengerProgram
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram getPassengerProgram() {
        return passengerProgram;
    }


    /**
     * Sets the passengerProgram value for this Passenger.
     * 
     * @param passengerProgram
     */
    public void setPassengerProgram(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram passengerProgram) {
        this.passengerProgram = passengerProgram;
    }


    /**
     * Gets the passengerFees value for this Passenger.
     * 
     * @return passengerFees
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee[] getPassengerFees() {
        return passengerFees;
    }


    /**
     * Sets the passengerFees value for this Passenger.
     * 
     * @param passengerFees
     */
    public void setPassengerFees(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee[] passengerFees) {
        this.passengerFees = passengerFees;
    }


    /**
     * Gets the passengerAddresses value for this Passenger.
     * 
     * @return passengerAddresses
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerAddress[] getPassengerAddresses() {
        return passengerAddresses;
    }


    /**
     * Sets the passengerAddresses value for this Passenger.
     * 
     * @param passengerAddresses
     */
    public void setPassengerAddresses(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerAddress[] passengerAddresses) {
        this.passengerAddresses = passengerAddresses;
    }


    /**
     * Gets the passengerTravelDocuments value for this Passenger.
     * 
     * @return passengerTravelDocuments
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTravelDocument[] getPassengerTravelDocuments() {
        return passengerTravelDocuments;
    }


    /**
     * Sets the passengerTravelDocuments value for this Passenger.
     * 
     * @param passengerTravelDocuments
     */
    public void setPassengerTravelDocuments(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTravelDocument[] passengerTravelDocuments) {
        this.passengerTravelDocuments = passengerTravelDocuments;
    }


    /**
     * Gets the passengerBags value for this Passenger.
     * 
     * @return passengerBags
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerBag[] getPassengerBags() {
        return passengerBags;
    }


    /**
     * Sets the passengerBags value for this Passenger.
     * 
     * @param passengerBags
     */
    public void setPassengerBags(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerBag[] passengerBags) {
        this.passengerBags = passengerBags;
    }


    /**
     * Gets the passengerID value for this Passenger.
     * 
     * @return passengerID
     */
    public java.lang.Long getPassengerID() {
        return passengerID;
    }


    /**
     * Sets the passengerID value for this Passenger.
     * 
     * @param passengerID
     */
    public void setPassengerID(java.lang.Long passengerID) {
        this.passengerID = passengerID;
    }


    /**
     * Gets the passengerTypeInfos value for this Passenger.
     * 
     * @return passengerTypeInfos
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo[] getPassengerTypeInfos() {
        return passengerTypeInfos;
    }


    /**
     * Sets the passengerTypeInfos value for this Passenger.
     * 
     * @param passengerTypeInfos
     */
    public void setPassengerTypeInfos(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo[] passengerTypeInfos) {
        this.passengerTypeInfos = passengerTypeInfos;
    }


    /**
     * Gets the passengerInfos value for this Passenger.
     * 
     * @return passengerInfos
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo[] getPassengerInfos() {
        return passengerInfos;
    }


    /**
     * Sets the passengerInfos value for this Passenger.
     * 
     * @param passengerInfos
     */
    public void setPassengerInfos(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo[] passengerInfos) {
        this.passengerInfos = passengerInfos;
    }


    /**
     * Gets the passengerInfants value for this Passenger.
     * 
     * @return passengerInfants
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant[] getPassengerInfants() {
        return passengerInfants;
    }


    /**
     * Sets the passengerInfants value for this Passenger.
     * 
     * @param passengerInfants
     */
    public void setPassengerInfants(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant[] passengerInfants) {
        this.passengerInfants = passengerInfants;
    }


    /**
     * Gets the pseudoPassenger value for this Passenger.
     * 
     * @return pseudoPassenger
     */
    public java.lang.Boolean getPseudoPassenger() {
        return pseudoPassenger;
    }


    /**
     * Sets the pseudoPassenger value for this Passenger.
     * 
     * @param pseudoPassenger
     */
    public void setPseudoPassenger(java.lang.Boolean pseudoPassenger) {
        this.pseudoPassenger = pseudoPassenger;
    }


    /**
     * Gets the passengerTypeInfo value for this Passenger.
     * 
     * @return passengerTypeInfo
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo getPassengerTypeInfo() {
        return passengerTypeInfo;
    }


    /**
     * Sets the passengerTypeInfo value for this Passenger.
     * 
     * @param passengerTypeInfo
     */
    public void setPassengerTypeInfo(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo passengerTypeInfo) {
        this.passengerTypeInfo = passengerTypeInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Passenger)) return false;
        Passenger other = (Passenger) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.passengerPrograms==null && other.getPassengerPrograms()==null) || 
             (this.passengerPrograms!=null &&
              java.util.Arrays.equals(this.passengerPrograms, other.getPassengerPrograms()))) &&
            ((this.customerNumber==null && other.getCustomerNumber()==null) || 
             (this.customerNumber!=null &&
              this.customerNumber.equals(other.getCustomerNumber()))) &&
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.familyNumber==null && other.getFamilyNumber()==null) || 
             (this.familyNumber!=null &&
              this.familyNumber.equals(other.getFamilyNumber()))) &&
            ((this.paxDiscountCode==null && other.getPaxDiscountCode()==null) || 
             (this.paxDiscountCode!=null &&
              this.paxDiscountCode.equals(other.getPaxDiscountCode()))) &&
            ((this.names==null && other.getNames()==null) || 
             (this.names!=null &&
              java.util.Arrays.equals(this.names, other.getNames()))) &&
            ((this.infant==null && other.getInfant()==null) || 
             (this.infant!=null &&
              this.infant.equals(other.getInfant()))) &&
            ((this.passengerInfo==null && other.getPassengerInfo()==null) || 
             (this.passengerInfo!=null &&
              this.passengerInfo.equals(other.getPassengerInfo()))) &&
            ((this.passengerProgram==null && other.getPassengerProgram()==null) || 
             (this.passengerProgram!=null &&
              this.passengerProgram.equals(other.getPassengerProgram()))) &&
            ((this.passengerFees==null && other.getPassengerFees()==null) || 
             (this.passengerFees!=null &&
              java.util.Arrays.equals(this.passengerFees, other.getPassengerFees()))) &&
            ((this.passengerAddresses==null && other.getPassengerAddresses()==null) || 
             (this.passengerAddresses!=null &&
              java.util.Arrays.equals(this.passengerAddresses, other.getPassengerAddresses()))) &&
            ((this.passengerTravelDocuments==null && other.getPassengerTravelDocuments()==null) || 
             (this.passengerTravelDocuments!=null &&
              java.util.Arrays.equals(this.passengerTravelDocuments, other.getPassengerTravelDocuments()))) &&
            ((this.passengerBags==null && other.getPassengerBags()==null) || 
             (this.passengerBags!=null &&
              java.util.Arrays.equals(this.passengerBags, other.getPassengerBags()))) &&
            ((this.passengerID==null && other.getPassengerID()==null) || 
             (this.passengerID!=null &&
              this.passengerID.equals(other.getPassengerID()))) &&
            ((this.passengerTypeInfos==null && other.getPassengerTypeInfos()==null) || 
             (this.passengerTypeInfos!=null &&
              java.util.Arrays.equals(this.passengerTypeInfos, other.getPassengerTypeInfos()))) &&
            ((this.passengerInfos==null && other.getPassengerInfos()==null) || 
             (this.passengerInfos!=null &&
              java.util.Arrays.equals(this.passengerInfos, other.getPassengerInfos()))) &&
            ((this.passengerInfants==null && other.getPassengerInfants()==null) || 
             (this.passengerInfants!=null &&
              java.util.Arrays.equals(this.passengerInfants, other.getPassengerInfants()))) &&
            ((this.pseudoPassenger==null && other.getPseudoPassenger()==null) || 
             (this.pseudoPassenger!=null &&
              this.pseudoPassenger.equals(other.getPseudoPassenger()))) &&
            ((this.passengerTypeInfo==null && other.getPassengerTypeInfo()==null) || 
             (this.passengerTypeInfo!=null &&
              this.passengerTypeInfo.equals(other.getPassengerTypeInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPassengerPrograms() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerPrograms());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerPrograms(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomerNumber() != null) {
            _hashCode += getCustomerNumber().hashCode();
        }
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getFamilyNumber() != null) {
            _hashCode += getFamilyNumber().hashCode();
        }
        if (getPaxDiscountCode() != null) {
            _hashCode += getPaxDiscountCode().hashCode();
        }
        if (getNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getInfant() != null) {
            _hashCode += getInfant().hashCode();
        }
        if (getPassengerInfo() != null) {
            _hashCode += getPassengerInfo().hashCode();
        }
        if (getPassengerProgram() != null) {
            _hashCode += getPassengerProgram().hashCode();
        }
        if (getPassengerFees() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerFees());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerFees(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerAddresses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerAddresses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerAddresses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerTravelDocuments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerTravelDocuments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerTravelDocuments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerBags() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerBags());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerBags(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerID() != null) {
            _hashCode += getPassengerID().hashCode();
        }
        if (getPassengerTypeInfos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerTypeInfos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerTypeInfos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerInfos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerInfos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerInfos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerInfants() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerInfants());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerInfants(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPseudoPassenger() != null) {
            _hashCode += getPseudoPassenger().hashCode();
        }
        if (getPassengerTypeInfo() != null) {
            _hashCode += getPassengerTypeInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Passenger.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerPrograms");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerPrograms"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerProgram"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerProgram"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CustomerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("familyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FamilyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxDiscountCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxDiscountCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("names");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Names"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingName"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infant");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Infant"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfant"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerProgram");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerProgram"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerProgram"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerFees");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerAddresses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerAddresses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerAddress"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerAddress"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerTravelDocuments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTravelDocuments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTravelDocument"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTravelDocument"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerBags");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerBags"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerBag"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerBag"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerTypeInfos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTypeInfos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTypeInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTypeInfo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerInfos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerInfants");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfants"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfant"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfant"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pseudoPassenger");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PseudoPassenger"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerTypeInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTypeInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTypeInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
