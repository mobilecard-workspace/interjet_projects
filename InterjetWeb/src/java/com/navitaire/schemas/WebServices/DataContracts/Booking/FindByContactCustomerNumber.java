/**
 * FindByContactCustomerNumber.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class FindByContactCustomerNumber  implements java.io.Serializable {
    private java.lang.String contactCustomerNumber;

    private java.lang.Long agentID;

    private java.lang.String organizationCode;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Filter filter;

    public FindByContactCustomerNumber() {
    }

    public FindByContactCustomerNumber(
           java.lang.String contactCustomerNumber,
           java.lang.Long agentID,
           java.lang.String organizationCode,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Filter filter) {
           this.contactCustomerNumber = contactCustomerNumber;
           this.agentID = agentID;
           this.organizationCode = organizationCode;
           this.filter = filter;
    }


    /**
     * Gets the contactCustomerNumber value for this FindByContactCustomerNumber.
     * 
     * @return contactCustomerNumber
     */
    public java.lang.String getContactCustomerNumber() {
        return contactCustomerNumber;
    }


    /**
     * Sets the contactCustomerNumber value for this FindByContactCustomerNumber.
     * 
     * @param contactCustomerNumber
     */
    public void setContactCustomerNumber(java.lang.String contactCustomerNumber) {
        this.contactCustomerNumber = contactCustomerNumber;
    }


    /**
     * Gets the agentID value for this FindByContactCustomerNumber.
     * 
     * @return agentID
     */
    public java.lang.Long getAgentID() {
        return agentID;
    }


    /**
     * Sets the agentID value for this FindByContactCustomerNumber.
     * 
     * @param agentID
     */
    public void setAgentID(java.lang.Long agentID) {
        this.agentID = agentID;
    }


    /**
     * Gets the organizationCode value for this FindByContactCustomerNumber.
     * 
     * @return organizationCode
     */
    public java.lang.String getOrganizationCode() {
        return organizationCode;
    }


    /**
     * Sets the organizationCode value for this FindByContactCustomerNumber.
     * 
     * @param organizationCode
     */
    public void setOrganizationCode(java.lang.String organizationCode) {
        this.organizationCode = organizationCode;
    }


    /**
     * Gets the filter value for this FindByContactCustomerNumber.
     * 
     * @return filter
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Filter getFilter() {
        return filter;
    }


    /**
     * Sets the filter value for this FindByContactCustomerNumber.
     * 
     * @param filter
     */
    public void setFilter(com.navitaire.schemas.WebServices.DataContracts.Booking.Filter filter) {
        this.filter = filter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindByContactCustomerNumber)) return false;
        FindByContactCustomerNumber other = (FindByContactCustomerNumber) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.contactCustomerNumber==null && other.getContactCustomerNumber()==null) || 
             (this.contactCustomerNumber!=null &&
              this.contactCustomerNumber.equals(other.getContactCustomerNumber()))) &&
            ((this.agentID==null && other.getAgentID()==null) || 
             (this.agentID!=null &&
              this.agentID.equals(other.getAgentID()))) &&
            ((this.organizationCode==null && other.getOrganizationCode()==null) || 
             (this.organizationCode!=null &&
              this.organizationCode.equals(other.getOrganizationCode()))) &&
            ((this.filter==null && other.getFilter()==null) || 
             (this.filter!=null &&
              this.filter.equals(other.getFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getContactCustomerNumber() != null) {
            _hashCode += getContactCustomerNumber().hashCode();
        }
        if (getAgentID() != null) {
            _hashCode += getAgentID().hashCode();
        }
        if (getOrganizationCode() != null) {
            _hashCode += getOrganizationCode().hashCode();
        }
        if (getFilter() != null) {
            _hashCode += getFilter().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindByContactCustomerNumber.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByContactCustomerNumber"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactCustomerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ContactCustomerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AgentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OrganizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Filter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Filter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
