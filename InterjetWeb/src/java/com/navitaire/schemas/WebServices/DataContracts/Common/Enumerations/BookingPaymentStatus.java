/**
 * BookingPaymentStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class BookingPaymentStatus implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected BookingPaymentStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _New = "New";
    public static final java.lang.String _Received = "Received";
    public static final java.lang.String _Pending = "Pending";
    public static final java.lang.String _Approved = "Approved";
    public static final java.lang.String _Declined = "Declined";
    public static final java.lang.String _Unknown = "Unknown";
    public static final java.lang.String _PendingCustomerAction = "PendingCustomerAction";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final BookingPaymentStatus New = new BookingPaymentStatus(_New);
    public static final BookingPaymentStatus Received = new BookingPaymentStatus(_Received);
    public static final BookingPaymentStatus Pending = new BookingPaymentStatus(_Pending);
    public static final BookingPaymentStatus Approved = new BookingPaymentStatus(_Approved);
    public static final BookingPaymentStatus Declined = new BookingPaymentStatus(_Declined);
    public static final BookingPaymentStatus Unknown = new BookingPaymentStatus(_Unknown);
    public static final BookingPaymentStatus PendingCustomerAction = new BookingPaymentStatus(_PendingCustomerAction);
    public static final BookingPaymentStatus Unmapped = new BookingPaymentStatus(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static BookingPaymentStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        BookingPaymentStatus enumeration = (BookingPaymentStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static BookingPaymentStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingPaymentStatus.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingPaymentStatus"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
