/**
 * Fee.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class Fee  implements java.io.Serializable {
    private java.lang.Integer parentSequence;

    private java.lang.Integer sequence;

    private java.math.BigDecimal amountTotal;

    private java.math.BigDecimal foreignAmountTotal;

    private java.lang.String description;

    private java.lang.Integer ruleSetID;

    private java.lang.Boolean isWaivable;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AOSFeeType feeType;

    private java.lang.Boolean fromSku;

    private java.math.BigDecimal amount;

    private java.lang.String currencyCode;

    private java.math.BigDecimal foreignAmount;

    private java.lang.String foreignCurrencyCode;

    private java.lang.Boolean isChargeable;

    private java.lang.String code;

    private java.lang.String typeCode;

    public Fee() {
    }

    public Fee(
           java.lang.Integer parentSequence,
           java.lang.Integer sequence,
           java.math.BigDecimal amountTotal,
           java.math.BigDecimal foreignAmountTotal,
           java.lang.String description,
           java.lang.Integer ruleSetID,
           java.lang.Boolean isWaivable,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AOSFeeType feeType,
           java.lang.Boolean fromSku,
           java.math.BigDecimal amount,
           java.lang.String currencyCode,
           java.math.BigDecimal foreignAmount,
           java.lang.String foreignCurrencyCode,
           java.lang.Boolean isChargeable,
           java.lang.String code,
           java.lang.String typeCode) {
           this.parentSequence = parentSequence;
           this.sequence = sequence;
           this.amountTotal = amountTotal;
           this.foreignAmountTotal = foreignAmountTotal;
           this.description = description;
           this.ruleSetID = ruleSetID;
           this.isWaivable = isWaivable;
           this.feeType = feeType;
           this.fromSku = fromSku;
           this.amount = amount;
           this.currencyCode = currencyCode;
           this.foreignAmount = foreignAmount;
           this.foreignCurrencyCode = foreignCurrencyCode;
           this.isChargeable = isChargeable;
           this.code = code;
           this.typeCode = typeCode;
    }


    /**
     * Gets the parentSequence value for this Fee.
     * 
     * @return parentSequence
     */
    public java.lang.Integer getParentSequence() {
        return parentSequence;
    }


    /**
     * Sets the parentSequence value for this Fee.
     * 
     * @param parentSequence
     */
    public void setParentSequence(java.lang.Integer parentSequence) {
        this.parentSequence = parentSequence;
    }


    /**
     * Gets the sequence value for this Fee.
     * 
     * @return sequence
     */
    public java.lang.Integer getSequence() {
        return sequence;
    }


    /**
     * Sets the sequence value for this Fee.
     * 
     * @param sequence
     */
    public void setSequence(java.lang.Integer sequence) {
        this.sequence = sequence;
    }


    /**
     * Gets the amountTotal value for this Fee.
     * 
     * @return amountTotal
     */
    public java.math.BigDecimal getAmountTotal() {
        return amountTotal;
    }


    /**
     * Sets the amountTotal value for this Fee.
     * 
     * @param amountTotal
     */
    public void setAmountTotal(java.math.BigDecimal amountTotal) {
        this.amountTotal = amountTotal;
    }


    /**
     * Gets the foreignAmountTotal value for this Fee.
     * 
     * @return foreignAmountTotal
     */
    public java.math.BigDecimal getForeignAmountTotal() {
        return foreignAmountTotal;
    }


    /**
     * Sets the foreignAmountTotal value for this Fee.
     * 
     * @param foreignAmountTotal
     */
    public void setForeignAmountTotal(java.math.BigDecimal foreignAmountTotal) {
        this.foreignAmountTotal = foreignAmountTotal;
    }


    /**
     * Gets the description value for this Fee.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Fee.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the ruleSetID value for this Fee.
     * 
     * @return ruleSetID
     */
    public java.lang.Integer getRuleSetID() {
        return ruleSetID;
    }


    /**
     * Sets the ruleSetID value for this Fee.
     * 
     * @param ruleSetID
     */
    public void setRuleSetID(java.lang.Integer ruleSetID) {
        this.ruleSetID = ruleSetID;
    }


    /**
     * Gets the isWaivable value for this Fee.
     * 
     * @return isWaivable
     */
    public java.lang.Boolean getIsWaivable() {
        return isWaivable;
    }


    /**
     * Sets the isWaivable value for this Fee.
     * 
     * @param isWaivable
     */
    public void setIsWaivable(java.lang.Boolean isWaivable) {
        this.isWaivable = isWaivable;
    }


    /**
     * Gets the feeType value for this Fee.
     * 
     * @return feeType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AOSFeeType getFeeType() {
        return feeType;
    }


    /**
     * Sets the feeType value for this Fee.
     * 
     * @param feeType
     */
    public void setFeeType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AOSFeeType feeType) {
        this.feeType = feeType;
    }


    /**
     * Gets the fromSku value for this Fee.
     * 
     * @return fromSku
     */
    public java.lang.Boolean getFromSku() {
        return fromSku;
    }


    /**
     * Sets the fromSku value for this Fee.
     * 
     * @param fromSku
     */
    public void setFromSku(java.lang.Boolean fromSku) {
        this.fromSku = fromSku;
    }


    /**
     * Gets the amount value for this Fee.
     * 
     * @return amount
     */
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this Fee.
     * 
     * @param amount
     */
    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }


    /**
     * Gets the currencyCode value for this Fee.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this Fee.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the foreignAmount value for this Fee.
     * 
     * @return foreignAmount
     */
    public java.math.BigDecimal getForeignAmount() {
        return foreignAmount;
    }


    /**
     * Sets the foreignAmount value for this Fee.
     * 
     * @param foreignAmount
     */
    public void setForeignAmount(java.math.BigDecimal foreignAmount) {
        this.foreignAmount = foreignAmount;
    }


    /**
     * Gets the foreignCurrencyCode value for this Fee.
     * 
     * @return foreignCurrencyCode
     */
    public java.lang.String getForeignCurrencyCode() {
        return foreignCurrencyCode;
    }


    /**
     * Sets the foreignCurrencyCode value for this Fee.
     * 
     * @param foreignCurrencyCode
     */
    public void setForeignCurrencyCode(java.lang.String foreignCurrencyCode) {
        this.foreignCurrencyCode = foreignCurrencyCode;
    }


    /**
     * Gets the isChargeable value for this Fee.
     * 
     * @return isChargeable
     */
    public java.lang.Boolean getIsChargeable() {
        return isChargeable;
    }


    /**
     * Sets the isChargeable value for this Fee.
     * 
     * @param isChargeable
     */
    public void setIsChargeable(java.lang.Boolean isChargeable) {
        this.isChargeable = isChargeable;
    }


    /**
     * Gets the code value for this Fee.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this Fee.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the typeCode value for this Fee.
     * 
     * @return typeCode
     */
    public java.lang.String getTypeCode() {
        return typeCode;
    }


    /**
     * Sets the typeCode value for this Fee.
     * 
     * @param typeCode
     */
    public void setTypeCode(java.lang.String typeCode) {
        this.typeCode = typeCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Fee)) return false;
        Fee other = (Fee) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.parentSequence==null && other.getParentSequence()==null) || 
             (this.parentSequence!=null &&
              this.parentSequence.equals(other.getParentSequence()))) &&
            ((this.sequence==null && other.getSequence()==null) || 
             (this.sequence!=null &&
              this.sequence.equals(other.getSequence()))) &&
            ((this.amountTotal==null && other.getAmountTotal()==null) || 
             (this.amountTotal!=null &&
              this.amountTotal.equals(other.getAmountTotal()))) &&
            ((this.foreignAmountTotal==null && other.getForeignAmountTotal()==null) || 
             (this.foreignAmountTotal!=null &&
              this.foreignAmountTotal.equals(other.getForeignAmountTotal()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.ruleSetID==null && other.getRuleSetID()==null) || 
             (this.ruleSetID!=null &&
              this.ruleSetID.equals(other.getRuleSetID()))) &&
            ((this.isWaivable==null && other.getIsWaivable()==null) || 
             (this.isWaivable!=null &&
              this.isWaivable.equals(other.getIsWaivable()))) &&
            ((this.feeType==null && other.getFeeType()==null) || 
             (this.feeType!=null &&
              this.feeType.equals(other.getFeeType()))) &&
            ((this.fromSku==null && other.getFromSku()==null) || 
             (this.fromSku!=null &&
              this.fromSku.equals(other.getFromSku()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.foreignAmount==null && other.getForeignAmount()==null) || 
             (this.foreignAmount!=null &&
              this.foreignAmount.equals(other.getForeignAmount()))) &&
            ((this.foreignCurrencyCode==null && other.getForeignCurrencyCode()==null) || 
             (this.foreignCurrencyCode!=null &&
              this.foreignCurrencyCode.equals(other.getForeignCurrencyCode()))) &&
            ((this.isChargeable==null && other.getIsChargeable()==null) || 
             (this.isChargeable!=null &&
              this.isChargeable.equals(other.getIsChargeable()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.typeCode==null && other.getTypeCode()==null) || 
             (this.typeCode!=null &&
              this.typeCode.equals(other.getTypeCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getParentSequence() != null) {
            _hashCode += getParentSequence().hashCode();
        }
        if (getSequence() != null) {
            _hashCode += getSequence().hashCode();
        }
        if (getAmountTotal() != null) {
            _hashCode += getAmountTotal().hashCode();
        }
        if (getForeignAmountTotal() != null) {
            _hashCode += getForeignAmountTotal().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getRuleSetID() != null) {
            _hashCode += getRuleSetID().hashCode();
        }
        if (getIsWaivable() != null) {
            _hashCode += getIsWaivable().hashCode();
        }
        if (getFeeType() != null) {
            _hashCode += getFeeType().hashCode();
        }
        if (getFromSku() != null) {
            _hashCode += getFromSku().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getForeignAmount() != null) {
            _hashCode += getForeignAmount().hashCode();
        }
        if (getForeignCurrencyCode() != null) {
            _hashCode += getForeignCurrencyCode().hashCode();
        }
        if (getIsChargeable() != null) {
            _hashCode += getIsChargeable().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getTypeCode() != null) {
            _hashCode += getTypeCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Fee.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Fee"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ParentSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Sequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amountTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "AmountTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignAmountTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ForeignAmountTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleSetID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RuleSetID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isWaivable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "IsWaivable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FeeType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AOSFeeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromSku");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FromSku"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ForeignAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ForeignCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isChargeable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "IsChargeable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
