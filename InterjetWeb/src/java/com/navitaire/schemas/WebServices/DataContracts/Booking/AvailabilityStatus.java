/**
 * AvailabilityStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class AvailabilityStatus  implements java.io.Serializable {
    private java.lang.Long sequenceNumber;

    private java.lang.String carrierCode;

    private java.lang.String AVSType;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketAVS[] dateMarketAVSList;

    public AvailabilityStatus() {
    }

    public AvailabilityStatus(
           java.lang.Long sequenceNumber,
           java.lang.String carrierCode,
           java.lang.String AVSType,
           com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketAVS[] dateMarketAVSList) {
           this.sequenceNumber = sequenceNumber;
           this.carrierCode = carrierCode;
           this.AVSType = AVSType;
           this.dateMarketAVSList = dateMarketAVSList;
    }


    /**
     * Gets the sequenceNumber value for this AvailabilityStatus.
     * 
     * @return sequenceNumber
     */
    public java.lang.Long getSequenceNumber() {
        return sequenceNumber;
    }


    /**
     * Sets the sequenceNumber value for this AvailabilityStatus.
     * 
     * @param sequenceNumber
     */
    public void setSequenceNumber(java.lang.Long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }


    /**
     * Gets the carrierCode value for this AvailabilityStatus.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this AvailabilityStatus.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the AVSType value for this AvailabilityStatus.
     * 
     * @return AVSType
     */
    public java.lang.String getAVSType() {
        return AVSType;
    }


    /**
     * Sets the AVSType value for this AvailabilityStatus.
     * 
     * @param AVSType
     */
    public void setAVSType(java.lang.String AVSType) {
        this.AVSType = AVSType;
    }


    /**
     * Gets the dateMarketAVSList value for this AvailabilityStatus.
     * 
     * @return dateMarketAVSList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketAVS[] getDateMarketAVSList() {
        return dateMarketAVSList;
    }


    /**
     * Sets the dateMarketAVSList value for this AvailabilityStatus.
     * 
     * @param dateMarketAVSList
     */
    public void setDateMarketAVSList(com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketAVS[] dateMarketAVSList) {
        this.dateMarketAVSList = dateMarketAVSList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AvailabilityStatus)) return false;
        AvailabilityStatus other = (AvailabilityStatus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sequenceNumber==null && other.getSequenceNumber()==null) || 
             (this.sequenceNumber!=null &&
              this.sequenceNumber.equals(other.getSequenceNumber()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.AVSType==null && other.getAVSType()==null) || 
             (this.AVSType!=null &&
              this.AVSType.equals(other.getAVSType()))) &&
            ((this.dateMarketAVSList==null && other.getDateMarketAVSList()==null) || 
             (this.dateMarketAVSList!=null &&
              java.util.Arrays.equals(this.dateMarketAVSList, other.getDateMarketAVSList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSequenceNumber() != null) {
            _hashCode += getSequenceNumber().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getAVSType() != null) {
            _hashCode += getAVSType().hashCode();
        }
        if (getDateMarketAVSList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDateMarketAVSList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDateMarketAVSList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AvailabilityStatus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityStatus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sequenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SequenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AVSType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateMarketAVSList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketAVSList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketAVS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketAVS"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
