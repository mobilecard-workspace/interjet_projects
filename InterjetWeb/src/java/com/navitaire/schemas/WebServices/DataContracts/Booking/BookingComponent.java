/**
 * BookingComponent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingComponent  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Long bookingID;

    private java.lang.Long bookingComponentID;

    private java.lang.String orderID;

    private java.lang.Short itemSequence;

    private java.lang.String supplierCode;

    private java.lang.String supplierRecordLocator;

    private java.lang.String systemCode;

    private java.lang.String systemRecordLocator;

    private java.lang.String recordReference;

    private java.lang.String status;

    private java.lang.String serviceTypeCode;

    private java.lang.String itemID;

    private java.lang.String itemTypeCode;

    private java.lang.String itemDescription;

    private java.util.Calendar beginDate;

    private java.util.Calendar endDate;

    private java.lang.String beginLocationCode;

    private java.lang.String endLocationCode;

    private java.lang.Long passengerID;

    private java.lang.String createdAgentCode;

    private java.lang.String createdOrganizationCode;

    private java.lang.String createdDomainCode;

    private java.lang.String createdLocationCode;

    private java.lang.String sourceAgentCode;

    private java.lang.String sourceOrganizationCode;

    private java.lang.String sourceDomainCode;

    private java.lang.String sourceLocationCode;

    private java.lang.Long createdAgentID;

    private java.util.Calendar createdDate;

    private java.lang.Long modifiedAgentID;

    private java.util.Calendar modifiedDate;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponentCharge[] bookingComponentCharges;

    private java.lang.Boolean historical;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.OrderItem orderItem;

    private java.lang.String declinedText;

    private java.lang.String cultureCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation otherServiceInfo;

    private com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem travelCommerceOrderItem;

    public BookingComponent() {
    }

    public BookingComponent(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Long bookingID,
           java.lang.Long bookingComponentID,
           java.lang.String orderID,
           java.lang.Short itemSequence,
           java.lang.String supplierCode,
           java.lang.String supplierRecordLocator,
           java.lang.String systemCode,
           java.lang.String systemRecordLocator,
           java.lang.String recordReference,
           java.lang.String status,
           java.lang.String serviceTypeCode,
           java.lang.String itemID,
           java.lang.String itemTypeCode,
           java.lang.String itemDescription,
           java.util.Calendar beginDate,
           java.util.Calendar endDate,
           java.lang.String beginLocationCode,
           java.lang.String endLocationCode,
           java.lang.Long passengerID,
           java.lang.String createdAgentCode,
           java.lang.String createdOrganizationCode,
           java.lang.String createdDomainCode,
           java.lang.String createdLocationCode,
           java.lang.String sourceAgentCode,
           java.lang.String sourceOrganizationCode,
           java.lang.String sourceDomainCode,
           java.lang.String sourceLocationCode,
           java.lang.Long createdAgentID,
           java.util.Calendar createdDate,
           java.lang.Long modifiedAgentID,
           java.util.Calendar modifiedDate,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponentCharge[] bookingComponentCharges,
           java.lang.Boolean historical,
           com.navitaire.schemas.WebServices.DataContracts.Booking.OrderItem orderItem,
           java.lang.String declinedText,
           java.lang.String cultureCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation otherServiceInfo,
           com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem travelCommerceOrderItem) {
        super(
            state);
        this.bookingID = bookingID;
        this.bookingComponentID = bookingComponentID;
        this.orderID = orderID;
        this.itemSequence = itemSequence;
        this.supplierCode = supplierCode;
        this.supplierRecordLocator = supplierRecordLocator;
        this.systemCode = systemCode;
        this.systemRecordLocator = systemRecordLocator;
        this.recordReference = recordReference;
        this.status = status;
        this.serviceTypeCode = serviceTypeCode;
        this.itemID = itemID;
        this.itemTypeCode = itemTypeCode;
        this.itemDescription = itemDescription;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.beginLocationCode = beginLocationCode;
        this.endLocationCode = endLocationCode;
        this.passengerID = passengerID;
        this.createdAgentCode = createdAgentCode;
        this.createdOrganizationCode = createdOrganizationCode;
        this.createdDomainCode = createdDomainCode;
        this.createdLocationCode = createdLocationCode;
        this.sourceAgentCode = sourceAgentCode;
        this.sourceOrganizationCode = sourceOrganizationCode;
        this.sourceDomainCode = sourceDomainCode;
        this.sourceLocationCode = sourceLocationCode;
        this.createdAgentID = createdAgentID;
        this.createdDate = createdDate;
        this.modifiedAgentID = modifiedAgentID;
        this.modifiedDate = modifiedDate;
        this.bookingComponentCharges = bookingComponentCharges;
        this.historical = historical;
        this.orderItem = orderItem;
        this.declinedText = declinedText;
        this.cultureCode = cultureCode;
        this.otherServiceInfo = otherServiceInfo;
        this.travelCommerceOrderItem = travelCommerceOrderItem;
    }


    /**
     * Gets the bookingID value for this BookingComponent.
     * 
     * @return bookingID
     */
    public java.lang.Long getBookingID() {
        return bookingID;
    }


    /**
     * Sets the bookingID value for this BookingComponent.
     * 
     * @param bookingID
     */
    public void setBookingID(java.lang.Long bookingID) {
        this.bookingID = bookingID;
    }


    /**
     * Gets the bookingComponentID value for this BookingComponent.
     * 
     * @return bookingComponentID
     */
    public java.lang.Long getBookingComponentID() {
        return bookingComponentID;
    }


    /**
     * Sets the bookingComponentID value for this BookingComponent.
     * 
     * @param bookingComponentID
     */
    public void setBookingComponentID(java.lang.Long bookingComponentID) {
        this.bookingComponentID = bookingComponentID;
    }


    /**
     * Gets the orderID value for this BookingComponent.
     * 
     * @return orderID
     */
    public java.lang.String getOrderID() {
        return orderID;
    }


    /**
     * Sets the orderID value for this BookingComponent.
     * 
     * @param orderID
     */
    public void setOrderID(java.lang.String orderID) {
        this.orderID = orderID;
    }


    /**
     * Gets the itemSequence value for this BookingComponent.
     * 
     * @return itemSequence
     */
    public java.lang.Short getItemSequence() {
        return itemSequence;
    }


    /**
     * Sets the itemSequence value for this BookingComponent.
     * 
     * @param itemSequence
     */
    public void setItemSequence(java.lang.Short itemSequence) {
        this.itemSequence = itemSequence;
    }


    /**
     * Gets the supplierCode value for this BookingComponent.
     * 
     * @return supplierCode
     */
    public java.lang.String getSupplierCode() {
        return supplierCode;
    }


    /**
     * Sets the supplierCode value for this BookingComponent.
     * 
     * @param supplierCode
     */
    public void setSupplierCode(java.lang.String supplierCode) {
        this.supplierCode = supplierCode;
    }


    /**
     * Gets the supplierRecordLocator value for this BookingComponent.
     * 
     * @return supplierRecordLocator
     */
    public java.lang.String getSupplierRecordLocator() {
        return supplierRecordLocator;
    }


    /**
     * Sets the supplierRecordLocator value for this BookingComponent.
     * 
     * @param supplierRecordLocator
     */
    public void setSupplierRecordLocator(java.lang.String supplierRecordLocator) {
        this.supplierRecordLocator = supplierRecordLocator;
    }


    /**
     * Gets the systemCode value for this BookingComponent.
     * 
     * @return systemCode
     */
    public java.lang.String getSystemCode() {
        return systemCode;
    }


    /**
     * Sets the systemCode value for this BookingComponent.
     * 
     * @param systemCode
     */
    public void setSystemCode(java.lang.String systemCode) {
        this.systemCode = systemCode;
    }


    /**
     * Gets the systemRecordLocator value for this BookingComponent.
     * 
     * @return systemRecordLocator
     */
    public java.lang.String getSystemRecordLocator() {
        return systemRecordLocator;
    }


    /**
     * Sets the systemRecordLocator value for this BookingComponent.
     * 
     * @param systemRecordLocator
     */
    public void setSystemRecordLocator(java.lang.String systemRecordLocator) {
        this.systemRecordLocator = systemRecordLocator;
    }


    /**
     * Gets the recordReference value for this BookingComponent.
     * 
     * @return recordReference
     */
    public java.lang.String getRecordReference() {
        return recordReference;
    }


    /**
     * Sets the recordReference value for this BookingComponent.
     * 
     * @param recordReference
     */
    public void setRecordReference(java.lang.String recordReference) {
        this.recordReference = recordReference;
    }


    /**
     * Gets the status value for this BookingComponent.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this BookingComponent.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the serviceTypeCode value for this BookingComponent.
     * 
     * @return serviceTypeCode
     */
    public java.lang.String getServiceTypeCode() {
        return serviceTypeCode;
    }


    /**
     * Sets the serviceTypeCode value for this BookingComponent.
     * 
     * @param serviceTypeCode
     */
    public void setServiceTypeCode(java.lang.String serviceTypeCode) {
        this.serviceTypeCode = serviceTypeCode;
    }


    /**
     * Gets the itemID value for this BookingComponent.
     * 
     * @return itemID
     */
    public java.lang.String getItemID() {
        return itemID;
    }


    /**
     * Sets the itemID value for this BookingComponent.
     * 
     * @param itemID
     */
    public void setItemID(java.lang.String itemID) {
        this.itemID = itemID;
    }


    /**
     * Gets the itemTypeCode value for this BookingComponent.
     * 
     * @return itemTypeCode
     */
    public java.lang.String getItemTypeCode() {
        return itemTypeCode;
    }


    /**
     * Sets the itemTypeCode value for this BookingComponent.
     * 
     * @param itemTypeCode
     */
    public void setItemTypeCode(java.lang.String itemTypeCode) {
        this.itemTypeCode = itemTypeCode;
    }


    /**
     * Gets the itemDescription value for this BookingComponent.
     * 
     * @return itemDescription
     */
    public java.lang.String getItemDescription() {
        return itemDescription;
    }


    /**
     * Sets the itemDescription value for this BookingComponent.
     * 
     * @param itemDescription
     */
    public void setItemDescription(java.lang.String itemDescription) {
        this.itemDescription = itemDescription;
    }


    /**
     * Gets the beginDate value for this BookingComponent.
     * 
     * @return beginDate
     */
    public java.util.Calendar getBeginDate() {
        return beginDate;
    }


    /**
     * Sets the beginDate value for this BookingComponent.
     * 
     * @param beginDate
     */
    public void setBeginDate(java.util.Calendar beginDate) {
        this.beginDate = beginDate;
    }


    /**
     * Gets the endDate value for this BookingComponent.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this BookingComponent.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the beginLocationCode value for this BookingComponent.
     * 
     * @return beginLocationCode
     */
    public java.lang.String getBeginLocationCode() {
        return beginLocationCode;
    }


    /**
     * Sets the beginLocationCode value for this BookingComponent.
     * 
     * @param beginLocationCode
     */
    public void setBeginLocationCode(java.lang.String beginLocationCode) {
        this.beginLocationCode = beginLocationCode;
    }


    /**
     * Gets the endLocationCode value for this BookingComponent.
     * 
     * @return endLocationCode
     */
    public java.lang.String getEndLocationCode() {
        return endLocationCode;
    }


    /**
     * Sets the endLocationCode value for this BookingComponent.
     * 
     * @param endLocationCode
     */
    public void setEndLocationCode(java.lang.String endLocationCode) {
        this.endLocationCode = endLocationCode;
    }


    /**
     * Gets the passengerID value for this BookingComponent.
     * 
     * @return passengerID
     */
    public java.lang.Long getPassengerID() {
        return passengerID;
    }


    /**
     * Sets the passengerID value for this BookingComponent.
     * 
     * @param passengerID
     */
    public void setPassengerID(java.lang.Long passengerID) {
        this.passengerID = passengerID;
    }


    /**
     * Gets the createdAgentCode value for this BookingComponent.
     * 
     * @return createdAgentCode
     */
    public java.lang.String getCreatedAgentCode() {
        return createdAgentCode;
    }


    /**
     * Sets the createdAgentCode value for this BookingComponent.
     * 
     * @param createdAgentCode
     */
    public void setCreatedAgentCode(java.lang.String createdAgentCode) {
        this.createdAgentCode = createdAgentCode;
    }


    /**
     * Gets the createdOrganizationCode value for this BookingComponent.
     * 
     * @return createdOrganizationCode
     */
    public java.lang.String getCreatedOrganizationCode() {
        return createdOrganizationCode;
    }


    /**
     * Sets the createdOrganizationCode value for this BookingComponent.
     * 
     * @param createdOrganizationCode
     */
    public void setCreatedOrganizationCode(java.lang.String createdOrganizationCode) {
        this.createdOrganizationCode = createdOrganizationCode;
    }


    /**
     * Gets the createdDomainCode value for this BookingComponent.
     * 
     * @return createdDomainCode
     */
    public java.lang.String getCreatedDomainCode() {
        return createdDomainCode;
    }


    /**
     * Sets the createdDomainCode value for this BookingComponent.
     * 
     * @param createdDomainCode
     */
    public void setCreatedDomainCode(java.lang.String createdDomainCode) {
        this.createdDomainCode = createdDomainCode;
    }


    /**
     * Gets the createdLocationCode value for this BookingComponent.
     * 
     * @return createdLocationCode
     */
    public java.lang.String getCreatedLocationCode() {
        return createdLocationCode;
    }


    /**
     * Sets the createdLocationCode value for this BookingComponent.
     * 
     * @param createdLocationCode
     */
    public void setCreatedLocationCode(java.lang.String createdLocationCode) {
        this.createdLocationCode = createdLocationCode;
    }


    /**
     * Gets the sourceAgentCode value for this BookingComponent.
     * 
     * @return sourceAgentCode
     */
    public java.lang.String getSourceAgentCode() {
        return sourceAgentCode;
    }


    /**
     * Sets the sourceAgentCode value for this BookingComponent.
     * 
     * @param sourceAgentCode
     */
    public void setSourceAgentCode(java.lang.String sourceAgentCode) {
        this.sourceAgentCode = sourceAgentCode;
    }


    /**
     * Gets the sourceOrganizationCode value for this BookingComponent.
     * 
     * @return sourceOrganizationCode
     */
    public java.lang.String getSourceOrganizationCode() {
        return sourceOrganizationCode;
    }


    /**
     * Sets the sourceOrganizationCode value for this BookingComponent.
     * 
     * @param sourceOrganizationCode
     */
    public void setSourceOrganizationCode(java.lang.String sourceOrganizationCode) {
        this.sourceOrganizationCode = sourceOrganizationCode;
    }


    /**
     * Gets the sourceDomainCode value for this BookingComponent.
     * 
     * @return sourceDomainCode
     */
    public java.lang.String getSourceDomainCode() {
        return sourceDomainCode;
    }


    /**
     * Sets the sourceDomainCode value for this BookingComponent.
     * 
     * @param sourceDomainCode
     */
    public void setSourceDomainCode(java.lang.String sourceDomainCode) {
        this.sourceDomainCode = sourceDomainCode;
    }


    /**
     * Gets the sourceLocationCode value for this BookingComponent.
     * 
     * @return sourceLocationCode
     */
    public java.lang.String getSourceLocationCode() {
        return sourceLocationCode;
    }


    /**
     * Sets the sourceLocationCode value for this BookingComponent.
     * 
     * @param sourceLocationCode
     */
    public void setSourceLocationCode(java.lang.String sourceLocationCode) {
        this.sourceLocationCode = sourceLocationCode;
    }


    /**
     * Gets the createdAgentID value for this BookingComponent.
     * 
     * @return createdAgentID
     */
    public java.lang.Long getCreatedAgentID() {
        return createdAgentID;
    }


    /**
     * Sets the createdAgentID value for this BookingComponent.
     * 
     * @param createdAgentID
     */
    public void setCreatedAgentID(java.lang.Long createdAgentID) {
        this.createdAgentID = createdAgentID;
    }


    /**
     * Gets the createdDate value for this BookingComponent.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this BookingComponent.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }


    /**
     * Gets the modifiedAgentID value for this BookingComponent.
     * 
     * @return modifiedAgentID
     */
    public java.lang.Long getModifiedAgentID() {
        return modifiedAgentID;
    }


    /**
     * Sets the modifiedAgentID value for this BookingComponent.
     * 
     * @param modifiedAgentID
     */
    public void setModifiedAgentID(java.lang.Long modifiedAgentID) {
        this.modifiedAgentID = modifiedAgentID;
    }


    /**
     * Gets the modifiedDate value for this BookingComponent.
     * 
     * @return modifiedDate
     */
    public java.util.Calendar getModifiedDate() {
        return modifiedDate;
    }


    /**
     * Sets the modifiedDate value for this BookingComponent.
     * 
     * @param modifiedDate
     */
    public void setModifiedDate(java.util.Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    /**
     * Gets the bookingComponentCharges value for this BookingComponent.
     * 
     * @return bookingComponentCharges
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponentCharge[] getBookingComponentCharges() {
        return bookingComponentCharges;
    }


    /**
     * Sets the bookingComponentCharges value for this BookingComponent.
     * 
     * @param bookingComponentCharges
     */
    public void setBookingComponentCharges(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponentCharge[] bookingComponentCharges) {
        this.bookingComponentCharges = bookingComponentCharges;
    }


    /**
     * Gets the historical value for this BookingComponent.
     * 
     * @return historical
     */
    public java.lang.Boolean getHistorical() {
        return historical;
    }


    /**
     * Sets the historical value for this BookingComponent.
     * 
     * @param historical
     */
    public void setHistorical(java.lang.Boolean historical) {
        this.historical = historical;
    }


    /**
     * Gets the orderItem value for this BookingComponent.
     * 
     * @return orderItem
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.OrderItem getOrderItem() {
        return orderItem;
    }


    /**
     * Sets the orderItem value for this BookingComponent.
     * 
     * @param orderItem
     */
    public void setOrderItem(com.navitaire.schemas.WebServices.DataContracts.Booking.OrderItem orderItem) {
        this.orderItem = orderItem;
    }


    /**
     * Gets the declinedText value for this BookingComponent.
     * 
     * @return declinedText
     */
    public java.lang.String getDeclinedText() {
        return declinedText;
    }


    /**
     * Sets the declinedText value for this BookingComponent.
     * 
     * @param declinedText
     */
    public void setDeclinedText(java.lang.String declinedText) {
        this.declinedText = declinedText;
    }


    /**
     * Gets the cultureCode value for this BookingComponent.
     * 
     * @return cultureCode
     */
    public java.lang.String getCultureCode() {
        return cultureCode;
    }


    /**
     * Sets the cultureCode value for this BookingComponent.
     * 
     * @param cultureCode
     */
    public void setCultureCode(java.lang.String cultureCode) {
        this.cultureCode = cultureCode;
    }


    /**
     * Gets the otherServiceInfo value for this BookingComponent.
     * 
     * @return otherServiceInfo
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation getOtherServiceInfo() {
        return otherServiceInfo;
    }


    /**
     * Sets the otherServiceInfo value for this BookingComponent.
     * 
     * @param otherServiceInfo
     */
    public void setOtherServiceInfo(com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation otherServiceInfo) {
        this.otherServiceInfo = otherServiceInfo;
    }


    /**
     * Gets the travelCommerceOrderItem value for this BookingComponent.
     * 
     * @return travelCommerceOrderItem
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem getTravelCommerceOrderItem() {
        return travelCommerceOrderItem;
    }


    /**
     * Sets the travelCommerceOrderItem value for this BookingComponent.
     * 
     * @param travelCommerceOrderItem
     */
    public void setTravelCommerceOrderItem(com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem travelCommerceOrderItem) {
        this.travelCommerceOrderItem = travelCommerceOrderItem;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingComponent)) return false;
        BookingComponent other = (BookingComponent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.bookingID==null && other.getBookingID()==null) || 
             (this.bookingID!=null &&
              this.bookingID.equals(other.getBookingID()))) &&
            ((this.bookingComponentID==null && other.getBookingComponentID()==null) || 
             (this.bookingComponentID!=null &&
              this.bookingComponentID.equals(other.getBookingComponentID()))) &&
            ((this.orderID==null && other.getOrderID()==null) || 
             (this.orderID!=null &&
              this.orderID.equals(other.getOrderID()))) &&
            ((this.itemSequence==null && other.getItemSequence()==null) || 
             (this.itemSequence!=null &&
              this.itemSequence.equals(other.getItemSequence()))) &&
            ((this.supplierCode==null && other.getSupplierCode()==null) || 
             (this.supplierCode!=null &&
              this.supplierCode.equals(other.getSupplierCode()))) &&
            ((this.supplierRecordLocator==null && other.getSupplierRecordLocator()==null) || 
             (this.supplierRecordLocator!=null &&
              this.supplierRecordLocator.equals(other.getSupplierRecordLocator()))) &&
            ((this.systemCode==null && other.getSystemCode()==null) || 
             (this.systemCode!=null &&
              this.systemCode.equals(other.getSystemCode()))) &&
            ((this.systemRecordLocator==null && other.getSystemRecordLocator()==null) || 
             (this.systemRecordLocator!=null &&
              this.systemRecordLocator.equals(other.getSystemRecordLocator()))) &&
            ((this.recordReference==null && other.getRecordReference()==null) || 
             (this.recordReference!=null &&
              this.recordReference.equals(other.getRecordReference()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.serviceTypeCode==null && other.getServiceTypeCode()==null) || 
             (this.serviceTypeCode!=null &&
              this.serviceTypeCode.equals(other.getServiceTypeCode()))) &&
            ((this.itemID==null && other.getItemID()==null) || 
             (this.itemID!=null &&
              this.itemID.equals(other.getItemID()))) &&
            ((this.itemTypeCode==null && other.getItemTypeCode()==null) || 
             (this.itemTypeCode!=null &&
              this.itemTypeCode.equals(other.getItemTypeCode()))) &&
            ((this.itemDescription==null && other.getItemDescription()==null) || 
             (this.itemDescription!=null &&
              this.itemDescription.equals(other.getItemDescription()))) &&
            ((this.beginDate==null && other.getBeginDate()==null) || 
             (this.beginDate!=null &&
              this.beginDate.equals(other.getBeginDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.beginLocationCode==null && other.getBeginLocationCode()==null) || 
             (this.beginLocationCode!=null &&
              this.beginLocationCode.equals(other.getBeginLocationCode()))) &&
            ((this.endLocationCode==null && other.getEndLocationCode()==null) || 
             (this.endLocationCode!=null &&
              this.endLocationCode.equals(other.getEndLocationCode()))) &&
            ((this.passengerID==null && other.getPassengerID()==null) || 
             (this.passengerID!=null &&
              this.passengerID.equals(other.getPassengerID()))) &&
            ((this.createdAgentCode==null && other.getCreatedAgentCode()==null) || 
             (this.createdAgentCode!=null &&
              this.createdAgentCode.equals(other.getCreatedAgentCode()))) &&
            ((this.createdOrganizationCode==null && other.getCreatedOrganizationCode()==null) || 
             (this.createdOrganizationCode!=null &&
              this.createdOrganizationCode.equals(other.getCreatedOrganizationCode()))) &&
            ((this.createdDomainCode==null && other.getCreatedDomainCode()==null) || 
             (this.createdDomainCode!=null &&
              this.createdDomainCode.equals(other.getCreatedDomainCode()))) &&
            ((this.createdLocationCode==null && other.getCreatedLocationCode()==null) || 
             (this.createdLocationCode!=null &&
              this.createdLocationCode.equals(other.getCreatedLocationCode()))) &&
            ((this.sourceAgentCode==null && other.getSourceAgentCode()==null) || 
             (this.sourceAgentCode!=null &&
              this.sourceAgentCode.equals(other.getSourceAgentCode()))) &&
            ((this.sourceOrganizationCode==null && other.getSourceOrganizationCode()==null) || 
             (this.sourceOrganizationCode!=null &&
              this.sourceOrganizationCode.equals(other.getSourceOrganizationCode()))) &&
            ((this.sourceDomainCode==null && other.getSourceDomainCode()==null) || 
             (this.sourceDomainCode!=null &&
              this.sourceDomainCode.equals(other.getSourceDomainCode()))) &&
            ((this.sourceLocationCode==null && other.getSourceLocationCode()==null) || 
             (this.sourceLocationCode!=null &&
              this.sourceLocationCode.equals(other.getSourceLocationCode()))) &&
            ((this.createdAgentID==null && other.getCreatedAgentID()==null) || 
             (this.createdAgentID!=null &&
              this.createdAgentID.equals(other.getCreatedAgentID()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate()))) &&
            ((this.modifiedAgentID==null && other.getModifiedAgentID()==null) || 
             (this.modifiedAgentID!=null &&
              this.modifiedAgentID.equals(other.getModifiedAgentID()))) &&
            ((this.modifiedDate==null && other.getModifiedDate()==null) || 
             (this.modifiedDate!=null &&
              this.modifiedDate.equals(other.getModifiedDate()))) &&
            ((this.bookingComponentCharges==null && other.getBookingComponentCharges()==null) || 
             (this.bookingComponentCharges!=null &&
              java.util.Arrays.equals(this.bookingComponentCharges, other.getBookingComponentCharges()))) &&
            ((this.historical==null && other.getHistorical()==null) || 
             (this.historical!=null &&
              this.historical.equals(other.getHistorical()))) &&
            ((this.orderItem==null && other.getOrderItem()==null) || 
             (this.orderItem!=null &&
              this.orderItem.equals(other.getOrderItem()))) &&
            ((this.declinedText==null && other.getDeclinedText()==null) || 
             (this.declinedText!=null &&
              this.declinedText.equals(other.getDeclinedText()))) &&
            ((this.cultureCode==null && other.getCultureCode()==null) || 
             (this.cultureCode!=null &&
              this.cultureCode.equals(other.getCultureCode()))) &&
            ((this.otherServiceInfo==null && other.getOtherServiceInfo()==null) || 
             (this.otherServiceInfo!=null &&
              this.otherServiceInfo.equals(other.getOtherServiceInfo()))) &&
            ((this.travelCommerceOrderItem==null && other.getTravelCommerceOrderItem()==null) || 
             (this.travelCommerceOrderItem!=null &&
              this.travelCommerceOrderItem.equals(other.getTravelCommerceOrderItem())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBookingID() != null) {
            _hashCode += getBookingID().hashCode();
        }
        if (getBookingComponentID() != null) {
            _hashCode += getBookingComponentID().hashCode();
        }
        if (getOrderID() != null) {
            _hashCode += getOrderID().hashCode();
        }
        if (getItemSequence() != null) {
            _hashCode += getItemSequence().hashCode();
        }
        if (getSupplierCode() != null) {
            _hashCode += getSupplierCode().hashCode();
        }
        if (getSupplierRecordLocator() != null) {
            _hashCode += getSupplierRecordLocator().hashCode();
        }
        if (getSystemCode() != null) {
            _hashCode += getSystemCode().hashCode();
        }
        if (getSystemRecordLocator() != null) {
            _hashCode += getSystemRecordLocator().hashCode();
        }
        if (getRecordReference() != null) {
            _hashCode += getRecordReference().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getServiceTypeCode() != null) {
            _hashCode += getServiceTypeCode().hashCode();
        }
        if (getItemID() != null) {
            _hashCode += getItemID().hashCode();
        }
        if (getItemTypeCode() != null) {
            _hashCode += getItemTypeCode().hashCode();
        }
        if (getItemDescription() != null) {
            _hashCode += getItemDescription().hashCode();
        }
        if (getBeginDate() != null) {
            _hashCode += getBeginDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getBeginLocationCode() != null) {
            _hashCode += getBeginLocationCode().hashCode();
        }
        if (getEndLocationCode() != null) {
            _hashCode += getEndLocationCode().hashCode();
        }
        if (getPassengerID() != null) {
            _hashCode += getPassengerID().hashCode();
        }
        if (getCreatedAgentCode() != null) {
            _hashCode += getCreatedAgentCode().hashCode();
        }
        if (getCreatedOrganizationCode() != null) {
            _hashCode += getCreatedOrganizationCode().hashCode();
        }
        if (getCreatedDomainCode() != null) {
            _hashCode += getCreatedDomainCode().hashCode();
        }
        if (getCreatedLocationCode() != null) {
            _hashCode += getCreatedLocationCode().hashCode();
        }
        if (getSourceAgentCode() != null) {
            _hashCode += getSourceAgentCode().hashCode();
        }
        if (getSourceOrganizationCode() != null) {
            _hashCode += getSourceOrganizationCode().hashCode();
        }
        if (getSourceDomainCode() != null) {
            _hashCode += getSourceDomainCode().hashCode();
        }
        if (getSourceLocationCode() != null) {
            _hashCode += getSourceLocationCode().hashCode();
        }
        if (getCreatedAgentID() != null) {
            _hashCode += getCreatedAgentID().hashCode();
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        if (getModifiedAgentID() != null) {
            _hashCode += getModifiedAgentID().hashCode();
        }
        if (getModifiedDate() != null) {
            _hashCode += getModifiedDate().hashCode();
        }
        if (getBookingComponentCharges() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingComponentCharges());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingComponentCharges(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getHistorical() != null) {
            _hashCode += getHistorical().hashCode();
        }
        if (getOrderItem() != null) {
            _hashCode += getOrderItem().hashCode();
        }
        if (getDeclinedText() != null) {
            _hashCode += getDeclinedText().hashCode();
        }
        if (getCultureCode() != null) {
            _hashCode += getCultureCode().hashCode();
        }
        if (getOtherServiceInfo() != null) {
            _hashCode += getOtherServiceInfo().hashCode();
        }
        if (getTravelCommerceOrderItem() != null) {
            _hashCode += getTravelCommerceOrderItem().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingComponent.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponent"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingComponentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OrderID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ItemSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("supplierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SupplierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("supplierRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SupplierRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SystemCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SystemRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ServiceTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ItemID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ItemTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ItemDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BeginDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginLocationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BeginLocationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endLocationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndLocationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdAgentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedAgentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdOrganizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedOrganizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDomainCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDomainCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdLocationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedLocationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceAgentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceAgentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceOrganizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceOrganizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceDomainCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceDomainCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceLocationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceLocationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdAgentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedAgentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedAgentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ModifiedAgentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ModifiedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingComponentCharges");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentCharges"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentCharge"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentCharge"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("historical");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Historical"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OrderItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OrderItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("declinedText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DeclinedText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cultureCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CultureCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherServiceInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OtherServiceInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OtherServiceInformation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("travelCommerceOrderItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelCommerceOrderItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCOrderItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
