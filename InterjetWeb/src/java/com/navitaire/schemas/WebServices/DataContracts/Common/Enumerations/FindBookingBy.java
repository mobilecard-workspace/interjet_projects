/**
 * FindBookingBy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class FindBookingBy implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected FindBookingBy(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _RecordLocator = "RecordLocator";
    public static final java.lang.String _Contact = "Contact";
    public static final java.lang.String _ThirdPartyRecordLocator = "ThirdPartyRecordLocator";
    public static final java.lang.String _Name = "Name";
    public static final java.lang.String _AgentID = "AgentID";
    public static final java.lang.String _AgentName = "AgentName";
    public static final java.lang.String _AgencyNumber = "AgencyNumber";
    public static final java.lang.String _EmailAddress = "EmailAddress";
    public static final java.lang.String _PhoneNumber = "PhoneNumber";
    public static final java.lang.String _CreditCardNumber = "CreditCardNumber";
    public static final java.lang.String _CustomerNumber = "CustomerNumber";
    public static final java.lang.String _ContactCustomerNumber = "ContactCustomerNumber";
    public static final java.lang.String _Customer = "Customer";
    public static final java.lang.String _OSTag = "OSTag";
    public static final java.lang.String _TravelDocument = "TravelDocument";
    public static final java.lang.String _BookingDate = "BookingDate";
    public static final FindBookingBy RecordLocator = new FindBookingBy(_RecordLocator);
    public static final FindBookingBy Contact = new FindBookingBy(_Contact);
    public static final FindBookingBy ThirdPartyRecordLocator = new FindBookingBy(_ThirdPartyRecordLocator);
    public static final FindBookingBy Name = new FindBookingBy(_Name);
    public static final FindBookingBy AgentID = new FindBookingBy(_AgentID);
    public static final FindBookingBy AgentName = new FindBookingBy(_AgentName);
    public static final FindBookingBy AgencyNumber = new FindBookingBy(_AgencyNumber);
    public static final FindBookingBy EmailAddress = new FindBookingBy(_EmailAddress);
    public static final FindBookingBy PhoneNumber = new FindBookingBy(_PhoneNumber);
    public static final FindBookingBy CreditCardNumber = new FindBookingBy(_CreditCardNumber);
    public static final FindBookingBy CustomerNumber = new FindBookingBy(_CustomerNumber);
    public static final FindBookingBy ContactCustomerNumber = new FindBookingBy(_ContactCustomerNumber);
    public static final FindBookingBy Customer = new FindBookingBy(_Customer);
    public static final FindBookingBy OSTag = new FindBookingBy(_OSTag);
    public static final FindBookingBy TravelDocument = new FindBookingBy(_TravelDocument);
    public static final FindBookingBy BookingDate = new FindBookingBy(_BookingDate);
    public java.lang.String getValue() { return _value_;}
    public static FindBookingBy fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        FindBookingBy enumeration = (FindBookingBy)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static FindBookingBy fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindBookingBy.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FindBookingBy"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
