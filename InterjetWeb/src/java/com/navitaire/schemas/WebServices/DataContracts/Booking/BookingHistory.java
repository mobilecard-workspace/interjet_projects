/**
 * BookingHistory.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingHistory  implements java.io.Serializable {
    private java.lang.String historyCode;

    private java.lang.String historyDetail;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale pointOfSale;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePointOfSale;

    private java.lang.String receivedBy;

    private java.lang.String receivedByReference;

    private java.util.Calendar createdDate;

    public BookingHistory() {
    }

    public BookingHistory(
           java.lang.String historyCode,
           java.lang.String historyDetail,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale pointOfSale,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePointOfSale,
           java.lang.String receivedBy,
           java.lang.String receivedByReference,
           java.util.Calendar createdDate) {
           this.historyCode = historyCode;
           this.historyDetail = historyDetail;
           this.pointOfSale = pointOfSale;
           this.sourcePointOfSale = sourcePointOfSale;
           this.receivedBy = receivedBy;
           this.receivedByReference = receivedByReference;
           this.createdDate = createdDate;
    }


    /**
     * Gets the historyCode value for this BookingHistory.
     * 
     * @return historyCode
     */
    public java.lang.String getHistoryCode() {
        return historyCode;
    }


    /**
     * Sets the historyCode value for this BookingHistory.
     * 
     * @param historyCode
     */
    public void setHistoryCode(java.lang.String historyCode) {
        this.historyCode = historyCode;
    }


    /**
     * Gets the historyDetail value for this BookingHistory.
     * 
     * @return historyDetail
     */
    public java.lang.String getHistoryDetail() {
        return historyDetail;
    }


    /**
     * Sets the historyDetail value for this BookingHistory.
     * 
     * @param historyDetail
     */
    public void setHistoryDetail(java.lang.String historyDetail) {
        this.historyDetail = historyDetail;
    }


    /**
     * Gets the pointOfSale value for this BookingHistory.
     * 
     * @return pointOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getPointOfSale() {
        return pointOfSale;
    }


    /**
     * Sets the pointOfSale value for this BookingHistory.
     * 
     * @param pointOfSale
     */
    public void setPointOfSale(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }


    /**
     * Gets the sourcePointOfSale value for this BookingHistory.
     * 
     * @return sourcePointOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePointOfSale() {
        return sourcePointOfSale;
    }


    /**
     * Sets the sourcePointOfSale value for this BookingHistory.
     * 
     * @param sourcePointOfSale
     */
    public void setSourcePointOfSale(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePointOfSale) {
        this.sourcePointOfSale = sourcePointOfSale;
    }


    /**
     * Gets the receivedBy value for this BookingHistory.
     * 
     * @return receivedBy
     */
    public java.lang.String getReceivedBy() {
        return receivedBy;
    }


    /**
     * Sets the receivedBy value for this BookingHistory.
     * 
     * @param receivedBy
     */
    public void setReceivedBy(java.lang.String receivedBy) {
        this.receivedBy = receivedBy;
    }


    /**
     * Gets the receivedByReference value for this BookingHistory.
     * 
     * @return receivedByReference
     */
    public java.lang.String getReceivedByReference() {
        return receivedByReference;
    }


    /**
     * Sets the receivedByReference value for this BookingHistory.
     * 
     * @param receivedByReference
     */
    public void setReceivedByReference(java.lang.String receivedByReference) {
        this.receivedByReference = receivedByReference;
    }


    /**
     * Gets the createdDate value for this BookingHistory.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this BookingHistory.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingHistory)) return false;
        BookingHistory other = (BookingHistory) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.historyCode==null && other.getHistoryCode()==null) || 
             (this.historyCode!=null &&
              this.historyCode.equals(other.getHistoryCode()))) &&
            ((this.historyDetail==null && other.getHistoryDetail()==null) || 
             (this.historyDetail!=null &&
              this.historyDetail.equals(other.getHistoryDetail()))) &&
            ((this.pointOfSale==null && other.getPointOfSale()==null) || 
             (this.pointOfSale!=null &&
              this.pointOfSale.equals(other.getPointOfSale()))) &&
            ((this.sourcePointOfSale==null && other.getSourcePointOfSale()==null) || 
             (this.sourcePointOfSale!=null &&
              this.sourcePointOfSale.equals(other.getSourcePointOfSale()))) &&
            ((this.receivedBy==null && other.getReceivedBy()==null) || 
             (this.receivedBy!=null &&
              this.receivedBy.equals(other.getReceivedBy()))) &&
            ((this.receivedByReference==null && other.getReceivedByReference()==null) || 
             (this.receivedByReference!=null &&
              this.receivedByReference.equals(other.getReceivedByReference()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHistoryCode() != null) {
            _hashCode += getHistoryCode().hashCode();
        }
        if (getHistoryDetail() != null) {
            _hashCode += getHistoryDetail().hashCode();
        }
        if (getPointOfSale() != null) {
            _hashCode += getPointOfSale().hashCode();
        }
        if (getSourcePointOfSale() != null) {
            _hashCode += getSourcePointOfSale().hashCode();
        }
        if (getReceivedBy() != null) {
            _hashCode += getReceivedBy().hashCode();
        }
        if (getReceivedByReference() != null) {
            _hashCode += getReceivedByReference().hashCode();
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingHistory.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHistory"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("historyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "HistoryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("historyDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "HistoryDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PointOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePointOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePointOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receivedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receivedByReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedByReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
