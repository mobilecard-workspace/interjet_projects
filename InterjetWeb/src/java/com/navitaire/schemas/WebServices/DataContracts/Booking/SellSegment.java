/**
 * SellSegment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SellSegment  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String actionStatusCode;

    private java.lang.String arrivalStation;

    private java.lang.String cabinOfService;

    private java.lang.String changeReasonCode;

    private java.lang.String departureStation;

    private java.lang.String priorityCode;

    private java.lang.String segmentType;

    private java.util.Calendar STA;

    private java.util.Calendar STD;

    private com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator;

    private com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator xrefFlightDesignator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellFare fare;

    public SellSegment() {
    }

    public SellSegment(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String actionStatusCode,
           java.lang.String arrivalStation,
           java.lang.String cabinOfService,
           java.lang.String changeReasonCode,
           java.lang.String departureStation,
           java.lang.String priorityCode,
           java.lang.String segmentType,
           java.util.Calendar STA,
           java.util.Calendar STD,
           com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator,
           com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator xrefFlightDesignator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellFare fare) {
        super(
            state);
        this.actionStatusCode = actionStatusCode;
        this.arrivalStation = arrivalStation;
        this.cabinOfService = cabinOfService;
        this.changeReasonCode = changeReasonCode;
        this.departureStation = departureStation;
        this.priorityCode = priorityCode;
        this.segmentType = segmentType;
        this.STA = STA;
        this.STD = STD;
        this.flightDesignator = flightDesignator;
        this.xrefFlightDesignator = xrefFlightDesignator;
        this.fare = fare;
    }


    /**
     * Gets the actionStatusCode value for this SellSegment.
     * 
     * @return actionStatusCode
     */
    public java.lang.String getActionStatusCode() {
        return actionStatusCode;
    }


    /**
     * Sets the actionStatusCode value for this SellSegment.
     * 
     * @param actionStatusCode
     */
    public void setActionStatusCode(java.lang.String actionStatusCode) {
        this.actionStatusCode = actionStatusCode;
    }


    /**
     * Gets the arrivalStation value for this SellSegment.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this SellSegment.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the cabinOfService value for this SellSegment.
     * 
     * @return cabinOfService
     */
    public java.lang.String getCabinOfService() {
        return cabinOfService;
    }


    /**
     * Sets the cabinOfService value for this SellSegment.
     * 
     * @param cabinOfService
     */
    public void setCabinOfService(java.lang.String cabinOfService) {
        this.cabinOfService = cabinOfService;
    }


    /**
     * Gets the changeReasonCode value for this SellSegment.
     * 
     * @return changeReasonCode
     */
    public java.lang.String getChangeReasonCode() {
        return changeReasonCode;
    }


    /**
     * Sets the changeReasonCode value for this SellSegment.
     * 
     * @param changeReasonCode
     */
    public void setChangeReasonCode(java.lang.String changeReasonCode) {
        this.changeReasonCode = changeReasonCode;
    }


    /**
     * Gets the departureStation value for this SellSegment.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this SellSegment.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the priorityCode value for this SellSegment.
     * 
     * @return priorityCode
     */
    public java.lang.String getPriorityCode() {
        return priorityCode;
    }


    /**
     * Sets the priorityCode value for this SellSegment.
     * 
     * @param priorityCode
     */
    public void setPriorityCode(java.lang.String priorityCode) {
        this.priorityCode = priorityCode;
    }


    /**
     * Gets the segmentType value for this SellSegment.
     * 
     * @return segmentType
     */
    public java.lang.String getSegmentType() {
        return segmentType;
    }


    /**
     * Sets the segmentType value for this SellSegment.
     * 
     * @param segmentType
     */
    public void setSegmentType(java.lang.String segmentType) {
        this.segmentType = segmentType;
    }


    /**
     * Gets the STA value for this SellSegment.
     * 
     * @return STA
     */
    public java.util.Calendar getSTA() {
        return STA;
    }


    /**
     * Sets the STA value for this SellSegment.
     * 
     * @param STA
     */
    public void setSTA(java.util.Calendar STA) {
        this.STA = STA;
    }


    /**
     * Gets the STD value for this SellSegment.
     * 
     * @return STD
     */
    public java.util.Calendar getSTD() {
        return STD;
    }


    /**
     * Sets the STD value for this SellSegment.
     * 
     * @param STD
     */
    public void setSTD(java.util.Calendar STD) {
        this.STD = STD;
    }


    /**
     * Gets the flightDesignator value for this SellSegment.
     * 
     * @return flightDesignator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator getFlightDesignator() {
        return flightDesignator;
    }


    /**
     * Sets the flightDesignator value for this SellSegment.
     * 
     * @param flightDesignator
     */
    public void setFlightDesignator(com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator) {
        this.flightDesignator = flightDesignator;
    }


    /**
     * Gets the xrefFlightDesignator value for this SellSegment.
     * 
     * @return xrefFlightDesignator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator getXrefFlightDesignator() {
        return xrefFlightDesignator;
    }


    /**
     * Sets the xrefFlightDesignator value for this SellSegment.
     * 
     * @param xrefFlightDesignator
     */
    public void setXrefFlightDesignator(com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator xrefFlightDesignator) {
        this.xrefFlightDesignator = xrefFlightDesignator;
    }


    /**
     * Gets the fare value for this SellSegment.
     * 
     * @return fare
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellFare getFare() {
        return fare;
    }


    /**
     * Sets the fare value for this SellSegment.
     * 
     * @param fare
     */
    public void setFare(com.navitaire.schemas.WebServices.DataContracts.Booking.SellFare fare) {
        this.fare = fare;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SellSegment)) return false;
        SellSegment other = (SellSegment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.actionStatusCode==null && other.getActionStatusCode()==null) || 
             (this.actionStatusCode!=null &&
              this.actionStatusCode.equals(other.getActionStatusCode()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.cabinOfService==null && other.getCabinOfService()==null) || 
             (this.cabinOfService!=null &&
              this.cabinOfService.equals(other.getCabinOfService()))) &&
            ((this.changeReasonCode==null && other.getChangeReasonCode()==null) || 
             (this.changeReasonCode!=null &&
              this.changeReasonCode.equals(other.getChangeReasonCode()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.priorityCode==null && other.getPriorityCode()==null) || 
             (this.priorityCode!=null &&
              this.priorityCode.equals(other.getPriorityCode()))) &&
            ((this.segmentType==null && other.getSegmentType()==null) || 
             (this.segmentType!=null &&
              this.segmentType.equals(other.getSegmentType()))) &&
            ((this.STA==null && other.getSTA()==null) || 
             (this.STA!=null &&
              this.STA.equals(other.getSTA()))) &&
            ((this.STD==null && other.getSTD()==null) || 
             (this.STD!=null &&
              this.STD.equals(other.getSTD()))) &&
            ((this.flightDesignator==null && other.getFlightDesignator()==null) || 
             (this.flightDesignator!=null &&
              this.flightDesignator.equals(other.getFlightDesignator()))) &&
            ((this.xrefFlightDesignator==null && other.getXrefFlightDesignator()==null) || 
             (this.xrefFlightDesignator!=null &&
              this.xrefFlightDesignator.equals(other.getXrefFlightDesignator()))) &&
            ((this.fare==null && other.getFare()==null) || 
             (this.fare!=null &&
              this.fare.equals(other.getFare())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getActionStatusCode() != null) {
            _hashCode += getActionStatusCode().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getCabinOfService() != null) {
            _hashCode += getCabinOfService().hashCode();
        }
        if (getChangeReasonCode() != null) {
            _hashCode += getChangeReasonCode().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getPriorityCode() != null) {
            _hashCode += getPriorityCode().hashCode();
        }
        if (getSegmentType() != null) {
            _hashCode += getSegmentType().hashCode();
        }
        if (getSTA() != null) {
            _hashCode += getSTA().hashCode();
        }
        if (getSTD() != null) {
            _hashCode += getSTD().hashCode();
        }
        if (getFlightDesignator() != null) {
            _hashCode += getFlightDesignator().hashCode();
        }
        if (getXrefFlightDesignator() != null) {
            _hashCode += getXrefFlightDesignator().hashCode();
        }
        if (getFare() != null) {
            _hashCode += getFare().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SellSegment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellSegment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cabinOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CabinOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeReasonCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChangeReasonCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priorityCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriorityCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STA");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FlightDesignator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xrefFlightDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "XrefFlightDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FlightDesignator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fare");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Fare"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellFare"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
