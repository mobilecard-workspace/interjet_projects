/**
 * MoveJourneyByKeyRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class MoveJourneyByKeyRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList fromJourneySellKeys;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList toJourneySellKeys;

    private java.lang.String toJourneyActionStatusCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType moveType;

    private java.lang.String changeReasonCode;

    private java.lang.Boolean keepWaitListStatus;

    private java.lang.Boolean ignoreClosedFlightStatus;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.IgnoreLiftStatus ignoreLiftStatus;

    private java.lang.Boolean changeStatus;

    private java.lang.Boolean oversell;

    private java.lang.Short boardingSequenceOffset;

    private java.lang.Boolean commit;

    private java.lang.String collectedCurrencyCode;

    public MoveJourneyByKeyRequestData() {
    }

    public MoveJourneyByKeyRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList fromJourneySellKeys,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList toJourneySellKeys,
           java.lang.String toJourneyActionStatusCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType moveType,
           java.lang.String changeReasonCode,
           java.lang.Boolean keepWaitListStatus,
           java.lang.Boolean ignoreClosedFlightStatus,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.IgnoreLiftStatus ignoreLiftStatus,
           java.lang.Boolean changeStatus,
           java.lang.Boolean oversell,
           java.lang.Short boardingSequenceOffset,
           java.lang.Boolean commit,
           java.lang.String collectedCurrencyCode) {
           this.fromJourneySellKeys = fromJourneySellKeys;
           this.toJourneySellKeys = toJourneySellKeys;
           this.toJourneyActionStatusCode = toJourneyActionStatusCode;
           this.moveType = moveType;
           this.changeReasonCode = changeReasonCode;
           this.keepWaitListStatus = keepWaitListStatus;
           this.ignoreClosedFlightStatus = ignoreClosedFlightStatus;
           this.ignoreLiftStatus = ignoreLiftStatus;
           this.changeStatus = changeStatus;
           this.oversell = oversell;
           this.boardingSequenceOffset = boardingSequenceOffset;
           this.commit = commit;
           this.collectedCurrencyCode = collectedCurrencyCode;
    }


    /**
     * Gets the fromJourneySellKeys value for this MoveJourneyByKeyRequestData.
     * 
     * @return fromJourneySellKeys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList getFromJourneySellKeys() {
        return fromJourneySellKeys;
    }


    /**
     * Sets the fromJourneySellKeys value for this MoveJourneyByKeyRequestData.
     * 
     * @param fromJourneySellKeys
     */
    public void setFromJourneySellKeys(com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList fromJourneySellKeys) {
        this.fromJourneySellKeys = fromJourneySellKeys;
    }


    /**
     * Gets the toJourneySellKeys value for this MoveJourneyByKeyRequestData.
     * 
     * @return toJourneySellKeys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList getToJourneySellKeys() {
        return toJourneySellKeys;
    }


    /**
     * Sets the toJourneySellKeys value for this MoveJourneyByKeyRequestData.
     * 
     * @param toJourneySellKeys
     */
    public void setToJourneySellKeys(com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList toJourneySellKeys) {
        this.toJourneySellKeys = toJourneySellKeys;
    }


    /**
     * Gets the toJourneyActionStatusCode value for this MoveJourneyByKeyRequestData.
     * 
     * @return toJourneyActionStatusCode
     */
    public java.lang.String getToJourneyActionStatusCode() {
        return toJourneyActionStatusCode;
    }


    /**
     * Sets the toJourneyActionStatusCode value for this MoveJourneyByKeyRequestData.
     * 
     * @param toJourneyActionStatusCode
     */
    public void setToJourneyActionStatusCode(java.lang.String toJourneyActionStatusCode) {
        this.toJourneyActionStatusCode = toJourneyActionStatusCode;
    }


    /**
     * Gets the moveType value for this MoveJourneyByKeyRequestData.
     * 
     * @return moveType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType getMoveType() {
        return moveType;
    }


    /**
     * Sets the moveType value for this MoveJourneyByKeyRequestData.
     * 
     * @param moveType
     */
    public void setMoveType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType moveType) {
        this.moveType = moveType;
    }


    /**
     * Gets the changeReasonCode value for this MoveJourneyByKeyRequestData.
     * 
     * @return changeReasonCode
     */
    public java.lang.String getChangeReasonCode() {
        return changeReasonCode;
    }


    /**
     * Sets the changeReasonCode value for this MoveJourneyByKeyRequestData.
     * 
     * @param changeReasonCode
     */
    public void setChangeReasonCode(java.lang.String changeReasonCode) {
        this.changeReasonCode = changeReasonCode;
    }


    /**
     * Gets the keepWaitListStatus value for this MoveJourneyByKeyRequestData.
     * 
     * @return keepWaitListStatus
     */
    public java.lang.Boolean getKeepWaitListStatus() {
        return keepWaitListStatus;
    }


    /**
     * Sets the keepWaitListStatus value for this MoveJourneyByKeyRequestData.
     * 
     * @param keepWaitListStatus
     */
    public void setKeepWaitListStatus(java.lang.Boolean keepWaitListStatus) {
        this.keepWaitListStatus = keepWaitListStatus;
    }


    /**
     * Gets the ignoreClosedFlightStatus value for this MoveJourneyByKeyRequestData.
     * 
     * @return ignoreClosedFlightStatus
     */
    public java.lang.Boolean getIgnoreClosedFlightStatus() {
        return ignoreClosedFlightStatus;
    }


    /**
     * Sets the ignoreClosedFlightStatus value for this MoveJourneyByKeyRequestData.
     * 
     * @param ignoreClosedFlightStatus
     */
    public void setIgnoreClosedFlightStatus(java.lang.Boolean ignoreClosedFlightStatus) {
        this.ignoreClosedFlightStatus = ignoreClosedFlightStatus;
    }


    /**
     * Gets the ignoreLiftStatus value for this MoveJourneyByKeyRequestData.
     * 
     * @return ignoreLiftStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.IgnoreLiftStatus getIgnoreLiftStatus() {
        return ignoreLiftStatus;
    }


    /**
     * Sets the ignoreLiftStatus value for this MoveJourneyByKeyRequestData.
     * 
     * @param ignoreLiftStatus
     */
    public void setIgnoreLiftStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.IgnoreLiftStatus ignoreLiftStatus) {
        this.ignoreLiftStatus = ignoreLiftStatus;
    }


    /**
     * Gets the changeStatus value for this MoveJourneyByKeyRequestData.
     * 
     * @return changeStatus
     */
    public java.lang.Boolean getChangeStatus() {
        return changeStatus;
    }


    /**
     * Sets the changeStatus value for this MoveJourneyByKeyRequestData.
     * 
     * @param changeStatus
     */
    public void setChangeStatus(java.lang.Boolean changeStatus) {
        this.changeStatus = changeStatus;
    }


    /**
     * Gets the oversell value for this MoveJourneyByKeyRequestData.
     * 
     * @return oversell
     */
    public java.lang.Boolean getOversell() {
        return oversell;
    }


    /**
     * Sets the oversell value for this MoveJourneyByKeyRequestData.
     * 
     * @param oversell
     */
    public void setOversell(java.lang.Boolean oversell) {
        this.oversell = oversell;
    }


    /**
     * Gets the boardingSequenceOffset value for this MoveJourneyByKeyRequestData.
     * 
     * @return boardingSequenceOffset
     */
    public java.lang.Short getBoardingSequenceOffset() {
        return boardingSequenceOffset;
    }


    /**
     * Sets the boardingSequenceOffset value for this MoveJourneyByKeyRequestData.
     * 
     * @param boardingSequenceOffset
     */
    public void setBoardingSequenceOffset(java.lang.Short boardingSequenceOffset) {
        this.boardingSequenceOffset = boardingSequenceOffset;
    }


    /**
     * Gets the commit value for this MoveJourneyByKeyRequestData.
     * 
     * @return commit
     */
    public java.lang.Boolean getCommit() {
        return commit;
    }


    /**
     * Sets the commit value for this MoveJourneyByKeyRequestData.
     * 
     * @param commit
     */
    public void setCommit(java.lang.Boolean commit) {
        this.commit = commit;
    }


    /**
     * Gets the collectedCurrencyCode value for this MoveJourneyByKeyRequestData.
     * 
     * @return collectedCurrencyCode
     */
    public java.lang.String getCollectedCurrencyCode() {
        return collectedCurrencyCode;
    }


    /**
     * Sets the collectedCurrencyCode value for this MoveJourneyByKeyRequestData.
     * 
     * @param collectedCurrencyCode
     */
    public void setCollectedCurrencyCode(java.lang.String collectedCurrencyCode) {
        this.collectedCurrencyCode = collectedCurrencyCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MoveJourneyByKeyRequestData)) return false;
        MoveJourneyByKeyRequestData other = (MoveJourneyByKeyRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fromJourneySellKeys==null && other.getFromJourneySellKeys()==null) || 
             (this.fromJourneySellKeys!=null &&
              this.fromJourneySellKeys.equals(other.getFromJourneySellKeys()))) &&
            ((this.toJourneySellKeys==null && other.getToJourneySellKeys()==null) || 
             (this.toJourneySellKeys!=null &&
              this.toJourneySellKeys.equals(other.getToJourneySellKeys()))) &&
            ((this.toJourneyActionStatusCode==null && other.getToJourneyActionStatusCode()==null) || 
             (this.toJourneyActionStatusCode!=null &&
              this.toJourneyActionStatusCode.equals(other.getToJourneyActionStatusCode()))) &&
            ((this.moveType==null && other.getMoveType()==null) || 
             (this.moveType!=null &&
              this.moveType.equals(other.getMoveType()))) &&
            ((this.changeReasonCode==null && other.getChangeReasonCode()==null) || 
             (this.changeReasonCode!=null &&
              this.changeReasonCode.equals(other.getChangeReasonCode()))) &&
            ((this.keepWaitListStatus==null && other.getKeepWaitListStatus()==null) || 
             (this.keepWaitListStatus!=null &&
              this.keepWaitListStatus.equals(other.getKeepWaitListStatus()))) &&
            ((this.ignoreClosedFlightStatus==null && other.getIgnoreClosedFlightStatus()==null) || 
             (this.ignoreClosedFlightStatus!=null &&
              this.ignoreClosedFlightStatus.equals(other.getIgnoreClosedFlightStatus()))) &&
            ((this.ignoreLiftStatus==null && other.getIgnoreLiftStatus()==null) || 
             (this.ignoreLiftStatus!=null &&
              this.ignoreLiftStatus.equals(other.getIgnoreLiftStatus()))) &&
            ((this.changeStatus==null && other.getChangeStatus()==null) || 
             (this.changeStatus!=null &&
              this.changeStatus.equals(other.getChangeStatus()))) &&
            ((this.oversell==null && other.getOversell()==null) || 
             (this.oversell!=null &&
              this.oversell.equals(other.getOversell()))) &&
            ((this.boardingSequenceOffset==null && other.getBoardingSequenceOffset()==null) || 
             (this.boardingSequenceOffset!=null &&
              this.boardingSequenceOffset.equals(other.getBoardingSequenceOffset()))) &&
            ((this.commit==null && other.getCommit()==null) || 
             (this.commit!=null &&
              this.commit.equals(other.getCommit()))) &&
            ((this.collectedCurrencyCode==null && other.getCollectedCurrencyCode()==null) || 
             (this.collectedCurrencyCode!=null &&
              this.collectedCurrencyCode.equals(other.getCollectedCurrencyCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFromJourneySellKeys() != null) {
            _hashCode += getFromJourneySellKeys().hashCode();
        }
        if (getToJourneySellKeys() != null) {
            _hashCode += getToJourneySellKeys().hashCode();
        }
        if (getToJourneyActionStatusCode() != null) {
            _hashCode += getToJourneyActionStatusCode().hashCode();
        }
        if (getMoveType() != null) {
            _hashCode += getMoveType().hashCode();
        }
        if (getChangeReasonCode() != null) {
            _hashCode += getChangeReasonCode().hashCode();
        }
        if (getKeepWaitListStatus() != null) {
            _hashCode += getKeepWaitListStatus().hashCode();
        }
        if (getIgnoreClosedFlightStatus() != null) {
            _hashCode += getIgnoreClosedFlightStatus().hashCode();
        }
        if (getIgnoreLiftStatus() != null) {
            _hashCode += getIgnoreLiftStatus().hashCode();
        }
        if (getChangeStatus() != null) {
            _hashCode += getChangeStatus().hashCode();
        }
        if (getOversell() != null) {
            _hashCode += getOversell().hashCode();
        }
        if (getBoardingSequenceOffset() != null) {
            _hashCode += getBoardingSequenceOffset().hashCode();
        }
        if (getCommit() != null) {
            _hashCode += getCommit().hashCode();
        }
        if (getCollectedCurrencyCode() != null) {
            _hashCode += getCollectedCurrencyCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MoveJourneyByKeyRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyByKeyRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromJourneySellKeys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FromJourneySellKeys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellKeyList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toJourneySellKeys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ToJourneySellKeys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellKeyList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toJourneyActionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ToJourneyActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moveType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "MovePassengerJourneyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeReasonCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChangeReasonCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keepWaitListStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "KeepWaitListStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ignoreClosedFlightStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IgnoreClosedFlightStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ignoreLiftStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IgnoreLiftStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "IgnoreLiftStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChangeStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oversell");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Oversell"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("boardingSequenceOffset");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BoardingSequenceOffset"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Commit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
