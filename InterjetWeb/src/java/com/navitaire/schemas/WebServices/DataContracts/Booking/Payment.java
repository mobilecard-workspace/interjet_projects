/**
 * Payment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class Payment  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentReferenceType referenceType;

    private java.lang.Long referenceID;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentMethodType paymentMethodType;

    private java.lang.String paymentMethodCode;

    private java.lang.String currencyCode;

    private java.math.BigDecimal paymentAmount;

    private java.lang.String collectedCurrencyCode;

    private java.math.BigDecimal collectedAmount;

    private java.lang.String quotedCurrencyCode;

    private java.math.BigDecimal quotedAmount;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus status;

    private java.lang.String accountNumber;

    private java.lang.Long accountNumberID;

    private java.util.Calendar expiration;

    private java.lang.String authorizationCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AuthorizationStatus authorizationStatus;

    private java.lang.Long parentPaymentID;

    private java.lang.Boolean transferred;

    private java.lang.Long reconcilliationID;

    private java.util.Calendar fundedDate;

    private java.lang.Short installments;

    private java.lang.String paymentText;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType;

    private java.lang.Short paymentNumber;

    private java.lang.String accountName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePointOfSale;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale pointOfSale;

    private java.lang.Long paymentID;

    private java.lang.Boolean deposit;

    private java.lang.Long accountID;

    private java.lang.String password;

    private java.lang.String accountTransactionCode;

    private java.lang.Long voucherID;

    private java.lang.Long voucherTransactionID;

    private java.lang.Boolean overrideVoucherRestrictions;

    private java.lang.Boolean overrideAmount;

    private java.lang.String recordLocator;

    private java.lang.Boolean paymentAddedToState;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.DCC DCC;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecure threeDSecure;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField[] paymentFields;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress[] paymentAddresses;

    private java.util.Calendar createdDate;

    private java.lang.Long createdAgentID;

    private java.util.Calendar modifiedDate;

    private java.lang.Long modifiedAgentID;

    private java.lang.Integer binRange;

    private java.util.Calendar approvalDate;

    public Payment() {
    }

    public Payment(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentReferenceType referenceType,
           java.lang.Long referenceID,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentMethodType paymentMethodType,
           java.lang.String paymentMethodCode,
           java.lang.String currencyCode,
           java.math.BigDecimal paymentAmount,
           java.lang.String collectedCurrencyCode,
           java.math.BigDecimal collectedAmount,
           java.lang.String quotedCurrencyCode,
           java.math.BigDecimal quotedAmount,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus status,
           java.lang.String accountNumber,
           java.lang.Long accountNumberID,
           java.util.Calendar expiration,
           java.lang.String authorizationCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AuthorizationStatus authorizationStatus,
           java.lang.Long parentPaymentID,
           java.lang.Boolean transferred,
           java.lang.Long reconcilliationID,
           java.util.Calendar fundedDate,
           java.lang.Short installments,
           java.lang.String paymentText,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType,
           java.lang.Short paymentNumber,
           java.lang.String accountName,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePointOfSale,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale pointOfSale,
           java.lang.Long paymentID,
           java.lang.Boolean deposit,
           java.lang.Long accountID,
           java.lang.String password,
           java.lang.String accountTransactionCode,
           java.lang.Long voucherID,
           java.lang.Long voucherTransactionID,
           java.lang.Boolean overrideVoucherRestrictions,
           java.lang.Boolean overrideAmount,
           java.lang.String recordLocator,
           java.lang.Boolean paymentAddedToState,
           com.navitaire.schemas.WebServices.DataContracts.Booking.DCC DCC,
           com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecure threeDSecure,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField[] paymentFields,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress[] paymentAddresses,
           java.util.Calendar createdDate,
           java.lang.Long createdAgentID,
           java.util.Calendar modifiedDate,
           java.lang.Long modifiedAgentID,
           java.lang.Integer binRange,
           java.util.Calendar approvalDate) {
        super(
            state);
        this.referenceType = referenceType;
        this.referenceID = referenceID;
        this.paymentMethodType = paymentMethodType;
        this.paymentMethodCode = paymentMethodCode;
        this.currencyCode = currencyCode;
        this.paymentAmount = paymentAmount;
        this.collectedCurrencyCode = collectedCurrencyCode;
        this.collectedAmount = collectedAmount;
        this.quotedCurrencyCode = quotedCurrencyCode;
        this.quotedAmount = quotedAmount;
        this.status = status;
        this.accountNumber = accountNumber;
        this.accountNumberID = accountNumberID;
        this.expiration = expiration;
        this.authorizationCode = authorizationCode;
        this.authorizationStatus = authorizationStatus;
        this.parentPaymentID = parentPaymentID;
        this.transferred = transferred;
        this.reconcilliationID = reconcilliationID;
        this.fundedDate = fundedDate;
        this.installments = installments;
        this.paymentText = paymentText;
        this.channelType = channelType;
        this.paymentNumber = paymentNumber;
        this.accountName = accountName;
        this.sourcePointOfSale = sourcePointOfSale;
        this.pointOfSale = pointOfSale;
        this.paymentID = paymentID;
        this.deposit = deposit;
        this.accountID = accountID;
        this.password = password;
        this.accountTransactionCode = accountTransactionCode;
        this.voucherID = voucherID;
        this.voucherTransactionID = voucherTransactionID;
        this.overrideVoucherRestrictions = overrideVoucherRestrictions;
        this.overrideAmount = overrideAmount;
        this.recordLocator = recordLocator;
        this.paymentAddedToState = paymentAddedToState;
        this.DCC = DCC;
        this.threeDSecure = threeDSecure;
        this.paymentFields = paymentFields;
        this.paymentAddresses = paymentAddresses;
        this.createdDate = createdDate;
        this.createdAgentID = createdAgentID;
        this.modifiedDate = modifiedDate;
        this.modifiedAgentID = modifiedAgentID;
        this.binRange = binRange;
        this.approvalDate = approvalDate;
    }


    /**
     * Gets the referenceType value for this Payment.
     * 
     * @return referenceType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentReferenceType getReferenceType() {
        return referenceType;
    }


    /**
     * Sets the referenceType value for this Payment.
     * 
     * @param referenceType
     */
    public void setReferenceType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentReferenceType referenceType) {
        this.referenceType = referenceType;
    }


    /**
     * Gets the referenceID value for this Payment.
     * 
     * @return referenceID
     */
    public java.lang.Long getReferenceID() {
        return referenceID;
    }


    /**
     * Sets the referenceID value for this Payment.
     * 
     * @param referenceID
     */
    public void setReferenceID(java.lang.Long referenceID) {
        this.referenceID = referenceID;
    }


    /**
     * Gets the paymentMethodType value for this Payment.
     * 
     * @return paymentMethodType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentMethodType getPaymentMethodType() {
        return paymentMethodType;
    }


    /**
     * Sets the paymentMethodType value for this Payment.
     * 
     * @param paymentMethodType
     */
    public void setPaymentMethodType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentMethodType paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }


    /**
     * Gets the paymentMethodCode value for this Payment.
     * 
     * @return paymentMethodCode
     */
    public java.lang.String getPaymentMethodCode() {
        return paymentMethodCode;
    }


    /**
     * Sets the paymentMethodCode value for this Payment.
     * 
     * @param paymentMethodCode
     */
    public void setPaymentMethodCode(java.lang.String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }


    /**
     * Gets the currencyCode value for this Payment.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this Payment.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the paymentAmount value for this Payment.
     * 
     * @return paymentAmount
     */
    public java.math.BigDecimal getPaymentAmount() {
        return paymentAmount;
    }


    /**
     * Sets the paymentAmount value for this Payment.
     * 
     * @param paymentAmount
     */
    public void setPaymentAmount(java.math.BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }


    /**
     * Gets the collectedCurrencyCode value for this Payment.
     * 
     * @return collectedCurrencyCode
     */
    public java.lang.String getCollectedCurrencyCode() {
        return collectedCurrencyCode;
    }


    /**
     * Sets the collectedCurrencyCode value for this Payment.
     * 
     * @param collectedCurrencyCode
     */
    public void setCollectedCurrencyCode(java.lang.String collectedCurrencyCode) {
        this.collectedCurrencyCode = collectedCurrencyCode;
    }


    /**
     * Gets the collectedAmount value for this Payment.
     * 
     * @return collectedAmount
     */
    public java.math.BigDecimal getCollectedAmount() {
        return collectedAmount;
    }


    /**
     * Sets the collectedAmount value for this Payment.
     * 
     * @param collectedAmount
     */
    public void setCollectedAmount(java.math.BigDecimal collectedAmount) {
        this.collectedAmount = collectedAmount;
    }


    /**
     * Gets the quotedCurrencyCode value for this Payment.
     * 
     * @return quotedCurrencyCode
     */
    public java.lang.String getQuotedCurrencyCode() {
        return quotedCurrencyCode;
    }


    /**
     * Sets the quotedCurrencyCode value for this Payment.
     * 
     * @param quotedCurrencyCode
     */
    public void setQuotedCurrencyCode(java.lang.String quotedCurrencyCode) {
        this.quotedCurrencyCode = quotedCurrencyCode;
    }


    /**
     * Gets the quotedAmount value for this Payment.
     * 
     * @return quotedAmount
     */
    public java.math.BigDecimal getQuotedAmount() {
        return quotedAmount;
    }


    /**
     * Sets the quotedAmount value for this Payment.
     * 
     * @param quotedAmount
     */
    public void setQuotedAmount(java.math.BigDecimal quotedAmount) {
        this.quotedAmount = quotedAmount;
    }


    /**
     * Gets the status value for this Payment.
     * 
     * @return status
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Payment.
     * 
     * @param status
     */
    public void setStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus status) {
        this.status = status;
    }


    /**
     * Gets the accountNumber value for this Payment.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this Payment.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the accountNumberID value for this Payment.
     * 
     * @return accountNumberID
     */
    public java.lang.Long getAccountNumberID() {
        return accountNumberID;
    }


    /**
     * Sets the accountNumberID value for this Payment.
     * 
     * @param accountNumberID
     */
    public void setAccountNumberID(java.lang.Long accountNumberID) {
        this.accountNumberID = accountNumberID;
    }


    /**
     * Gets the expiration value for this Payment.
     * 
     * @return expiration
     */
    public java.util.Calendar getExpiration() {
        return expiration;
    }


    /**
     * Sets the expiration value for this Payment.
     * 
     * @param expiration
     */
    public void setExpiration(java.util.Calendar expiration) {
        this.expiration = expiration;
    }


    /**
     * Gets the authorizationCode value for this Payment.
     * 
     * @return authorizationCode
     */
    public java.lang.String getAuthorizationCode() {
        return authorizationCode;
    }


    /**
     * Sets the authorizationCode value for this Payment.
     * 
     * @param authorizationCode
     */
    public void setAuthorizationCode(java.lang.String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }


    /**
     * Gets the authorizationStatus value for this Payment.
     * 
     * @return authorizationStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AuthorizationStatus getAuthorizationStatus() {
        return authorizationStatus;
    }


    /**
     * Sets the authorizationStatus value for this Payment.
     * 
     * @param authorizationStatus
     */
    public void setAuthorizationStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AuthorizationStatus authorizationStatus) {
        this.authorizationStatus = authorizationStatus;
    }


    /**
     * Gets the parentPaymentID value for this Payment.
     * 
     * @return parentPaymentID
     */
    public java.lang.Long getParentPaymentID() {
        return parentPaymentID;
    }


    /**
     * Sets the parentPaymentID value for this Payment.
     * 
     * @param parentPaymentID
     */
    public void setParentPaymentID(java.lang.Long parentPaymentID) {
        this.parentPaymentID = parentPaymentID;
    }


    /**
     * Gets the transferred value for this Payment.
     * 
     * @return transferred
     */
    public java.lang.Boolean getTransferred() {
        return transferred;
    }


    /**
     * Sets the transferred value for this Payment.
     * 
     * @param transferred
     */
    public void setTransferred(java.lang.Boolean transferred) {
        this.transferred = transferred;
    }


    /**
     * Gets the reconcilliationID value for this Payment.
     * 
     * @return reconcilliationID
     */
    public java.lang.Long getReconcilliationID() {
        return reconcilliationID;
    }


    /**
     * Sets the reconcilliationID value for this Payment.
     * 
     * @param reconcilliationID
     */
    public void setReconcilliationID(java.lang.Long reconcilliationID) {
        this.reconcilliationID = reconcilliationID;
    }


    /**
     * Gets the fundedDate value for this Payment.
     * 
     * @return fundedDate
     */
    public java.util.Calendar getFundedDate() {
        return fundedDate;
    }


    /**
     * Sets the fundedDate value for this Payment.
     * 
     * @param fundedDate
     */
    public void setFundedDate(java.util.Calendar fundedDate) {
        this.fundedDate = fundedDate;
    }


    /**
     * Gets the installments value for this Payment.
     * 
     * @return installments
     */
    public java.lang.Short getInstallments() {
        return installments;
    }


    /**
     * Sets the installments value for this Payment.
     * 
     * @param installments
     */
    public void setInstallments(java.lang.Short installments) {
        this.installments = installments;
    }


    /**
     * Gets the paymentText value for this Payment.
     * 
     * @return paymentText
     */
    public java.lang.String getPaymentText() {
        return paymentText;
    }


    /**
     * Sets the paymentText value for this Payment.
     * 
     * @param paymentText
     */
    public void setPaymentText(java.lang.String paymentText) {
        this.paymentText = paymentText;
    }


    /**
     * Gets the channelType value for this Payment.
     * 
     * @return channelType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType getChannelType() {
        return channelType;
    }


    /**
     * Sets the channelType value for this Payment.
     * 
     * @param channelType
     */
    public void setChannelType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType) {
        this.channelType = channelType;
    }


    /**
     * Gets the paymentNumber value for this Payment.
     * 
     * @return paymentNumber
     */
    public java.lang.Short getPaymentNumber() {
        return paymentNumber;
    }


    /**
     * Sets the paymentNumber value for this Payment.
     * 
     * @param paymentNumber
     */
    public void setPaymentNumber(java.lang.Short paymentNumber) {
        this.paymentNumber = paymentNumber;
    }


    /**
     * Gets the accountName value for this Payment.
     * 
     * @return accountName
     */
    public java.lang.String getAccountName() {
        return accountName;
    }


    /**
     * Sets the accountName value for this Payment.
     * 
     * @param accountName
     */
    public void setAccountName(java.lang.String accountName) {
        this.accountName = accountName;
    }


    /**
     * Gets the sourcePointOfSale value for this Payment.
     * 
     * @return sourcePointOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePointOfSale() {
        return sourcePointOfSale;
    }


    /**
     * Sets the sourcePointOfSale value for this Payment.
     * 
     * @param sourcePointOfSale
     */
    public void setSourcePointOfSale(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePointOfSale) {
        this.sourcePointOfSale = sourcePointOfSale;
    }


    /**
     * Gets the pointOfSale value for this Payment.
     * 
     * @return pointOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getPointOfSale() {
        return pointOfSale;
    }


    /**
     * Sets the pointOfSale value for this Payment.
     * 
     * @param pointOfSale
     */
    public void setPointOfSale(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }


    /**
     * Gets the paymentID value for this Payment.
     * 
     * @return paymentID
     */
    public java.lang.Long getPaymentID() {
        return paymentID;
    }


    /**
     * Sets the paymentID value for this Payment.
     * 
     * @param paymentID
     */
    public void setPaymentID(java.lang.Long paymentID) {
        this.paymentID = paymentID;
    }


    /**
     * Gets the deposit value for this Payment.
     * 
     * @return deposit
     */
    public java.lang.Boolean getDeposit() {
        return deposit;
    }


    /**
     * Sets the deposit value for this Payment.
     * 
     * @param deposit
     */
    public void setDeposit(java.lang.Boolean deposit) {
        this.deposit = deposit;
    }


    /**
     * Gets the accountID value for this Payment.
     * 
     * @return accountID
     */
    public java.lang.Long getAccountID() {
        return accountID;
    }


    /**
     * Sets the accountID value for this Payment.
     * 
     * @param accountID
     */
    public void setAccountID(java.lang.Long accountID) {
        this.accountID = accountID;
    }


    /**
     * Gets the password value for this Payment.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this Payment.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the accountTransactionCode value for this Payment.
     * 
     * @return accountTransactionCode
     */
    public java.lang.String getAccountTransactionCode() {
        return accountTransactionCode;
    }


    /**
     * Sets the accountTransactionCode value for this Payment.
     * 
     * @param accountTransactionCode
     */
    public void setAccountTransactionCode(java.lang.String accountTransactionCode) {
        this.accountTransactionCode = accountTransactionCode;
    }


    /**
     * Gets the voucherID value for this Payment.
     * 
     * @return voucherID
     */
    public java.lang.Long getVoucherID() {
        return voucherID;
    }


    /**
     * Sets the voucherID value for this Payment.
     * 
     * @param voucherID
     */
    public void setVoucherID(java.lang.Long voucherID) {
        this.voucherID = voucherID;
    }


    /**
     * Gets the voucherTransactionID value for this Payment.
     * 
     * @return voucherTransactionID
     */
    public java.lang.Long getVoucherTransactionID() {
        return voucherTransactionID;
    }


    /**
     * Sets the voucherTransactionID value for this Payment.
     * 
     * @param voucherTransactionID
     */
    public void setVoucherTransactionID(java.lang.Long voucherTransactionID) {
        this.voucherTransactionID = voucherTransactionID;
    }


    /**
     * Gets the overrideVoucherRestrictions value for this Payment.
     * 
     * @return overrideVoucherRestrictions
     */
    public java.lang.Boolean getOverrideVoucherRestrictions() {
        return overrideVoucherRestrictions;
    }


    /**
     * Sets the overrideVoucherRestrictions value for this Payment.
     * 
     * @param overrideVoucherRestrictions
     */
    public void setOverrideVoucherRestrictions(java.lang.Boolean overrideVoucherRestrictions) {
        this.overrideVoucherRestrictions = overrideVoucherRestrictions;
    }


    /**
     * Gets the overrideAmount value for this Payment.
     * 
     * @return overrideAmount
     */
    public java.lang.Boolean getOverrideAmount() {
        return overrideAmount;
    }


    /**
     * Sets the overrideAmount value for this Payment.
     * 
     * @param overrideAmount
     */
    public void setOverrideAmount(java.lang.Boolean overrideAmount) {
        this.overrideAmount = overrideAmount;
    }


    /**
     * Gets the recordLocator value for this Payment.
     * 
     * @return recordLocator
     */
    public java.lang.String getRecordLocator() {
        return recordLocator;
    }


    /**
     * Sets the recordLocator value for this Payment.
     * 
     * @param recordLocator
     */
    public void setRecordLocator(java.lang.String recordLocator) {
        this.recordLocator = recordLocator;
    }


    /**
     * Gets the paymentAddedToState value for this Payment.
     * 
     * @return paymentAddedToState
     */
    public java.lang.Boolean getPaymentAddedToState() {
        return paymentAddedToState;
    }


    /**
     * Sets the paymentAddedToState value for this Payment.
     * 
     * @param paymentAddedToState
     */
    public void setPaymentAddedToState(java.lang.Boolean paymentAddedToState) {
        this.paymentAddedToState = paymentAddedToState;
    }


    /**
     * Gets the DCC value for this Payment.
     * 
     * @return DCC
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.DCC getDCC() {
        return DCC;
    }


    /**
     * Sets the DCC value for this Payment.
     * 
     * @param DCC
     */
    public void setDCC(com.navitaire.schemas.WebServices.DataContracts.Booking.DCC DCC) {
        this.DCC = DCC;
    }


    /**
     * Gets the threeDSecure value for this Payment.
     * 
     * @return threeDSecure
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecure getThreeDSecure() {
        return threeDSecure;
    }


    /**
     * Sets the threeDSecure value for this Payment.
     * 
     * @param threeDSecure
     */
    public void setThreeDSecure(com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecure threeDSecure) {
        this.threeDSecure = threeDSecure;
    }


    /**
     * Gets the paymentFields value for this Payment.
     * 
     * @return paymentFields
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField[] getPaymentFields() {
        return paymentFields;
    }


    /**
     * Sets the paymentFields value for this Payment.
     * 
     * @param paymentFields
     */
    public void setPaymentFields(com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField[] paymentFields) {
        this.paymentFields = paymentFields;
    }


    /**
     * Gets the paymentAddresses value for this Payment.
     * 
     * @return paymentAddresses
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress[] getPaymentAddresses() {
        return paymentAddresses;
    }


    /**
     * Sets the paymentAddresses value for this Payment.
     * 
     * @param paymentAddresses
     */
    public void setPaymentAddresses(com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress[] paymentAddresses) {
        this.paymentAddresses = paymentAddresses;
    }


    /**
     * Gets the createdDate value for this Payment.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this Payment.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }


    /**
     * Gets the createdAgentID value for this Payment.
     * 
     * @return createdAgentID
     */
    public java.lang.Long getCreatedAgentID() {
        return createdAgentID;
    }


    /**
     * Sets the createdAgentID value for this Payment.
     * 
     * @param createdAgentID
     */
    public void setCreatedAgentID(java.lang.Long createdAgentID) {
        this.createdAgentID = createdAgentID;
    }


    /**
     * Gets the modifiedDate value for this Payment.
     * 
     * @return modifiedDate
     */
    public java.util.Calendar getModifiedDate() {
        return modifiedDate;
    }


    /**
     * Sets the modifiedDate value for this Payment.
     * 
     * @param modifiedDate
     */
    public void setModifiedDate(java.util.Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    /**
     * Gets the modifiedAgentID value for this Payment.
     * 
     * @return modifiedAgentID
     */
    public java.lang.Long getModifiedAgentID() {
        return modifiedAgentID;
    }


    /**
     * Sets the modifiedAgentID value for this Payment.
     * 
     * @param modifiedAgentID
     */
    public void setModifiedAgentID(java.lang.Long modifiedAgentID) {
        this.modifiedAgentID = modifiedAgentID;
    }


    /**
     * Gets the binRange value for this Payment.
     * 
     * @return binRange
     */
    public java.lang.Integer getBinRange() {
        return binRange;
    }


    /**
     * Sets the binRange value for this Payment.
     * 
     * @param binRange
     */
    public void setBinRange(java.lang.Integer binRange) {
        this.binRange = binRange;
    }


    /**
     * Gets the approvalDate value for this Payment.
     * 
     * @return approvalDate
     */
    public java.util.Calendar getApprovalDate() {
        return approvalDate;
    }


    /**
     * Sets the approvalDate value for this Payment.
     * 
     * @param approvalDate
     */
    public void setApprovalDate(java.util.Calendar approvalDate) {
        this.approvalDate = approvalDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Payment)) return false;
        Payment other = (Payment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.referenceType==null && other.getReferenceType()==null) || 
             (this.referenceType!=null &&
              this.referenceType.equals(other.getReferenceType()))) &&
            ((this.referenceID==null && other.getReferenceID()==null) || 
             (this.referenceID!=null &&
              this.referenceID.equals(other.getReferenceID()))) &&
            ((this.paymentMethodType==null && other.getPaymentMethodType()==null) || 
             (this.paymentMethodType!=null &&
              this.paymentMethodType.equals(other.getPaymentMethodType()))) &&
            ((this.paymentMethodCode==null && other.getPaymentMethodCode()==null) || 
             (this.paymentMethodCode!=null &&
              this.paymentMethodCode.equals(other.getPaymentMethodCode()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.paymentAmount==null && other.getPaymentAmount()==null) || 
             (this.paymentAmount!=null &&
              this.paymentAmount.equals(other.getPaymentAmount()))) &&
            ((this.collectedCurrencyCode==null && other.getCollectedCurrencyCode()==null) || 
             (this.collectedCurrencyCode!=null &&
              this.collectedCurrencyCode.equals(other.getCollectedCurrencyCode()))) &&
            ((this.collectedAmount==null && other.getCollectedAmount()==null) || 
             (this.collectedAmount!=null &&
              this.collectedAmount.equals(other.getCollectedAmount()))) &&
            ((this.quotedCurrencyCode==null && other.getQuotedCurrencyCode()==null) || 
             (this.quotedCurrencyCode!=null &&
              this.quotedCurrencyCode.equals(other.getQuotedCurrencyCode()))) &&
            ((this.quotedAmount==null && other.getQuotedAmount()==null) || 
             (this.quotedAmount!=null &&
              this.quotedAmount.equals(other.getQuotedAmount()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.accountNumberID==null && other.getAccountNumberID()==null) || 
             (this.accountNumberID!=null &&
              this.accountNumberID.equals(other.getAccountNumberID()))) &&
            ((this.expiration==null && other.getExpiration()==null) || 
             (this.expiration!=null &&
              this.expiration.equals(other.getExpiration()))) &&
            ((this.authorizationCode==null && other.getAuthorizationCode()==null) || 
             (this.authorizationCode!=null &&
              this.authorizationCode.equals(other.getAuthorizationCode()))) &&
            ((this.authorizationStatus==null && other.getAuthorizationStatus()==null) || 
             (this.authorizationStatus!=null &&
              this.authorizationStatus.equals(other.getAuthorizationStatus()))) &&
            ((this.parentPaymentID==null && other.getParentPaymentID()==null) || 
             (this.parentPaymentID!=null &&
              this.parentPaymentID.equals(other.getParentPaymentID()))) &&
            ((this.transferred==null && other.getTransferred()==null) || 
             (this.transferred!=null &&
              this.transferred.equals(other.getTransferred()))) &&
            ((this.reconcilliationID==null && other.getReconcilliationID()==null) || 
             (this.reconcilliationID!=null &&
              this.reconcilliationID.equals(other.getReconcilliationID()))) &&
            ((this.fundedDate==null && other.getFundedDate()==null) || 
             (this.fundedDate!=null &&
              this.fundedDate.equals(other.getFundedDate()))) &&
            ((this.installments==null && other.getInstallments()==null) || 
             (this.installments!=null &&
              this.installments.equals(other.getInstallments()))) &&
            ((this.paymentText==null && other.getPaymentText()==null) || 
             (this.paymentText!=null &&
              this.paymentText.equals(other.getPaymentText()))) &&
            ((this.channelType==null && other.getChannelType()==null) || 
             (this.channelType!=null &&
              this.channelType.equals(other.getChannelType()))) &&
            ((this.paymentNumber==null && other.getPaymentNumber()==null) || 
             (this.paymentNumber!=null &&
              this.paymentNumber.equals(other.getPaymentNumber()))) &&
            ((this.accountName==null && other.getAccountName()==null) || 
             (this.accountName!=null &&
              this.accountName.equals(other.getAccountName()))) &&
            ((this.sourcePointOfSale==null && other.getSourcePointOfSale()==null) || 
             (this.sourcePointOfSale!=null &&
              this.sourcePointOfSale.equals(other.getSourcePointOfSale()))) &&
            ((this.pointOfSale==null && other.getPointOfSale()==null) || 
             (this.pointOfSale!=null &&
              this.pointOfSale.equals(other.getPointOfSale()))) &&
            ((this.paymentID==null && other.getPaymentID()==null) || 
             (this.paymentID!=null &&
              this.paymentID.equals(other.getPaymentID()))) &&
            ((this.deposit==null && other.getDeposit()==null) || 
             (this.deposit!=null &&
              this.deposit.equals(other.getDeposit()))) &&
            ((this.accountID==null && other.getAccountID()==null) || 
             (this.accountID!=null &&
              this.accountID.equals(other.getAccountID()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.accountTransactionCode==null && other.getAccountTransactionCode()==null) || 
             (this.accountTransactionCode!=null &&
              this.accountTransactionCode.equals(other.getAccountTransactionCode()))) &&
            ((this.voucherID==null && other.getVoucherID()==null) || 
             (this.voucherID!=null &&
              this.voucherID.equals(other.getVoucherID()))) &&
            ((this.voucherTransactionID==null && other.getVoucherTransactionID()==null) || 
             (this.voucherTransactionID!=null &&
              this.voucherTransactionID.equals(other.getVoucherTransactionID()))) &&
            ((this.overrideVoucherRestrictions==null && other.getOverrideVoucherRestrictions()==null) || 
             (this.overrideVoucherRestrictions!=null &&
              this.overrideVoucherRestrictions.equals(other.getOverrideVoucherRestrictions()))) &&
            ((this.overrideAmount==null && other.getOverrideAmount()==null) || 
             (this.overrideAmount!=null &&
              this.overrideAmount.equals(other.getOverrideAmount()))) &&
            ((this.recordLocator==null && other.getRecordLocator()==null) || 
             (this.recordLocator!=null &&
              this.recordLocator.equals(other.getRecordLocator()))) &&
            ((this.paymentAddedToState==null && other.getPaymentAddedToState()==null) || 
             (this.paymentAddedToState!=null &&
              this.paymentAddedToState.equals(other.getPaymentAddedToState()))) &&
            ((this.DCC==null && other.getDCC()==null) || 
             (this.DCC!=null &&
              this.DCC.equals(other.getDCC()))) &&
            ((this.threeDSecure==null && other.getThreeDSecure()==null) || 
             (this.threeDSecure!=null &&
              this.threeDSecure.equals(other.getThreeDSecure()))) &&
            ((this.paymentFields==null && other.getPaymentFields()==null) || 
             (this.paymentFields!=null &&
              java.util.Arrays.equals(this.paymentFields, other.getPaymentFields()))) &&
            ((this.paymentAddresses==null && other.getPaymentAddresses()==null) || 
             (this.paymentAddresses!=null &&
              java.util.Arrays.equals(this.paymentAddresses, other.getPaymentAddresses()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate()))) &&
            ((this.createdAgentID==null && other.getCreatedAgentID()==null) || 
             (this.createdAgentID!=null &&
              this.createdAgentID.equals(other.getCreatedAgentID()))) &&
            ((this.modifiedDate==null && other.getModifiedDate()==null) || 
             (this.modifiedDate!=null &&
              this.modifiedDate.equals(other.getModifiedDate()))) &&
            ((this.modifiedAgentID==null && other.getModifiedAgentID()==null) || 
             (this.modifiedAgentID!=null &&
              this.modifiedAgentID.equals(other.getModifiedAgentID()))) &&
            ((this.binRange==null && other.getBinRange()==null) || 
             (this.binRange!=null &&
              this.binRange.equals(other.getBinRange()))) &&
            ((this.approvalDate==null && other.getApprovalDate()==null) || 
             (this.approvalDate!=null &&
              this.approvalDate.equals(other.getApprovalDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getReferenceType() != null) {
            _hashCode += getReferenceType().hashCode();
        }
        if (getReferenceID() != null) {
            _hashCode += getReferenceID().hashCode();
        }
        if (getPaymentMethodType() != null) {
            _hashCode += getPaymentMethodType().hashCode();
        }
        if (getPaymentMethodCode() != null) {
            _hashCode += getPaymentMethodCode().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getPaymentAmount() != null) {
            _hashCode += getPaymentAmount().hashCode();
        }
        if (getCollectedCurrencyCode() != null) {
            _hashCode += getCollectedCurrencyCode().hashCode();
        }
        if (getCollectedAmount() != null) {
            _hashCode += getCollectedAmount().hashCode();
        }
        if (getQuotedCurrencyCode() != null) {
            _hashCode += getQuotedCurrencyCode().hashCode();
        }
        if (getQuotedAmount() != null) {
            _hashCode += getQuotedAmount().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getAccountNumberID() != null) {
            _hashCode += getAccountNumberID().hashCode();
        }
        if (getExpiration() != null) {
            _hashCode += getExpiration().hashCode();
        }
        if (getAuthorizationCode() != null) {
            _hashCode += getAuthorizationCode().hashCode();
        }
        if (getAuthorizationStatus() != null) {
            _hashCode += getAuthorizationStatus().hashCode();
        }
        if (getParentPaymentID() != null) {
            _hashCode += getParentPaymentID().hashCode();
        }
        if (getTransferred() != null) {
            _hashCode += getTransferred().hashCode();
        }
        if (getReconcilliationID() != null) {
            _hashCode += getReconcilliationID().hashCode();
        }
        if (getFundedDate() != null) {
            _hashCode += getFundedDate().hashCode();
        }
        if (getInstallments() != null) {
            _hashCode += getInstallments().hashCode();
        }
        if (getPaymentText() != null) {
            _hashCode += getPaymentText().hashCode();
        }
        if (getChannelType() != null) {
            _hashCode += getChannelType().hashCode();
        }
        if (getPaymentNumber() != null) {
            _hashCode += getPaymentNumber().hashCode();
        }
        if (getAccountName() != null) {
            _hashCode += getAccountName().hashCode();
        }
        if (getSourcePointOfSale() != null) {
            _hashCode += getSourcePointOfSale().hashCode();
        }
        if (getPointOfSale() != null) {
            _hashCode += getPointOfSale().hashCode();
        }
        if (getPaymentID() != null) {
            _hashCode += getPaymentID().hashCode();
        }
        if (getDeposit() != null) {
            _hashCode += getDeposit().hashCode();
        }
        if (getAccountID() != null) {
            _hashCode += getAccountID().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getAccountTransactionCode() != null) {
            _hashCode += getAccountTransactionCode().hashCode();
        }
        if (getVoucherID() != null) {
            _hashCode += getVoucherID().hashCode();
        }
        if (getVoucherTransactionID() != null) {
            _hashCode += getVoucherTransactionID().hashCode();
        }
        if (getOverrideVoucherRestrictions() != null) {
            _hashCode += getOverrideVoucherRestrictions().hashCode();
        }
        if (getOverrideAmount() != null) {
            _hashCode += getOverrideAmount().hashCode();
        }
        if (getRecordLocator() != null) {
            _hashCode += getRecordLocator().hashCode();
        }
        if (getPaymentAddedToState() != null) {
            _hashCode += getPaymentAddedToState().hashCode();
        }
        if (getDCC() != null) {
            _hashCode += getDCC().hashCode();
        }
        if (getThreeDSecure() != null) {
            _hashCode += getThreeDSecure().hashCode();
        }
        if (getPaymentFields() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaymentFields());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaymentFields(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaymentAddresses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaymentAddresses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaymentAddresses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        if (getCreatedAgentID() != null) {
            _hashCode += getCreatedAgentID().hashCode();
        }
        if (getModifiedDate() != null) {
            _hashCode += getModifiedDate().hashCode();
        }
        if (getModifiedAgentID() != null) {
            _hashCode += getModifiedAgentID().hashCode();
        }
        if (getBinRange() != null) {
            _hashCode += getBinRange().hashCode();
        }
        if (getApprovalDate() != null) {
            _hashCode += getApprovalDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Payment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReferenceType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentReferenceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReferenceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentMethodType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentMethodType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentMethodCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quotedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QuotedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quotedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QuotedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingPaymentStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumberID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AccountNumberID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expiration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Expiration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AuthorizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AuthorizationStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AuthorizationStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentPaymentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ParentPaymentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transferred");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Transferred"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reconcilliationID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReconcilliationID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FundedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("installments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Installments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChannelType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChannelType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AccountName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePointOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePointOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PointOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deposit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Deposit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AccountID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountTransactionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AccountTransactionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voucherID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "VoucherID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voucherTransactionID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "VoucherTransactionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideVoucherRestrictions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideVoucherRestrictions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentAddedToState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddedToState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DCC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DCC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DCC"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("threeDSecure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ThreeDSecure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ThreeDSecure"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentField"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentField"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentAddresses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddresses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddress"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddress"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdAgentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedAgentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ModifiedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifiedAgentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ModifiedAgentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("binRange");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BinRange"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvalDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ApprovalDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
