/**
 * TokenRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Session;

public class TokenRequest  implements java.io.Serializable {
    private java.lang.String token;

    private java.lang.String terminalInfo;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SystemType systemType;

    public TokenRequest() {
    }

    public TokenRequest(
           java.lang.String token,
           java.lang.String terminalInfo,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SystemType systemType) {
           this.token = token;
           this.terminalInfo = terminalInfo;
           this.channelType = channelType;
           this.systemType = systemType;
    }


    /**
     * Gets the token value for this TokenRequest.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this TokenRequest.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the terminalInfo value for this TokenRequest.
     * 
     * @return terminalInfo
     */
    public java.lang.String getTerminalInfo() {
        return terminalInfo;
    }


    /**
     * Sets the terminalInfo value for this TokenRequest.
     * 
     * @param terminalInfo
     */
    public void setTerminalInfo(java.lang.String terminalInfo) {
        this.terminalInfo = terminalInfo;
    }


    /**
     * Gets the channelType value for this TokenRequest.
     * 
     * @return channelType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType getChannelType() {
        return channelType;
    }


    /**
     * Sets the channelType value for this TokenRequest.
     * 
     * @param channelType
     */
    public void setChannelType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType) {
        this.channelType = channelType;
    }


    /**
     * Gets the systemType value for this TokenRequest.
     * 
     * @return systemType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SystemType getSystemType() {
        return systemType;
    }


    /**
     * Sets the systemType value for this TokenRequest.
     * 
     * @param systemType
     */
    public void setSystemType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SystemType systemType) {
        this.systemType = systemType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TokenRequest)) return false;
        TokenRequest other = (TokenRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.terminalInfo==null && other.getTerminalInfo()==null) || 
             (this.terminalInfo!=null &&
              this.terminalInfo.equals(other.getTerminalInfo()))) &&
            ((this.channelType==null && other.getChannelType()==null) || 
             (this.channelType!=null &&
              this.channelType.equals(other.getChannelType()))) &&
            ((this.systemType==null && other.getSystemType()==null) || 
             (this.systemType!=null &&
              this.systemType.equals(other.getSystemType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getTerminalInfo() != null) {
            _hashCode += getTerminalInfo().hashCode();
        }
        if (getChannelType() != null) {
            _hashCode += getChannelType().hashCode();
        }
        if (getSystemType() != null) {
            _hashCode += getSystemType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TokenRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "TokenRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "Token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminalInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "TerminalInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "ChannelType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChannelType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "SystemType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SystemType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
