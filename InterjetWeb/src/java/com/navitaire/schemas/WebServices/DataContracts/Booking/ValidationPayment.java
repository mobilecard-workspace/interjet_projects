/**
 * ValidationPayment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class ValidationPayment  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.Payment payment;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentValidationError[] paymentValidationErrors;

    public ValidationPayment() {
    }

    public ValidationPayment(
           com.navitaire.schemas.WebServices.DataContracts.Booking.Payment payment,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentValidationError[] paymentValidationErrors) {
           this.payment = payment;
           this.paymentValidationErrors = paymentValidationErrors;
    }


    /**
     * Gets the payment value for this ValidationPayment.
     * 
     * @return payment
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Payment getPayment() {
        return payment;
    }


    /**
     * Sets the payment value for this ValidationPayment.
     * 
     * @param payment
     */
    public void setPayment(com.navitaire.schemas.WebServices.DataContracts.Booking.Payment payment) {
        this.payment = payment;
    }


    /**
     * Gets the paymentValidationErrors value for this ValidationPayment.
     * 
     * @return paymentValidationErrors
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentValidationError[] getPaymentValidationErrors() {
        return paymentValidationErrors;
    }


    /**
     * Sets the paymentValidationErrors value for this ValidationPayment.
     * 
     * @param paymentValidationErrors
     */
    public void setPaymentValidationErrors(com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentValidationError[] paymentValidationErrors) {
        this.paymentValidationErrors = paymentValidationErrors;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValidationPayment)) return false;
        ValidationPayment other = (ValidationPayment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.payment==null && other.getPayment()==null) || 
             (this.payment!=null &&
              this.payment.equals(other.getPayment()))) &&
            ((this.paymentValidationErrors==null && other.getPaymentValidationErrors()==null) || 
             (this.paymentValidationErrors!=null &&
              java.util.Arrays.equals(this.paymentValidationErrors, other.getPaymentValidationErrors())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPayment() != null) {
            _hashCode += getPayment().hashCode();
        }
        if (getPaymentValidationErrors() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaymentValidationErrors());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaymentValidationErrors(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValidationPayment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationPayment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentValidationErrors");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentValidationErrors"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentValidationError"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentValidationError"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
