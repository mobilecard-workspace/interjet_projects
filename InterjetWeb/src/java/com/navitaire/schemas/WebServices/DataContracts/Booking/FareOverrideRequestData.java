/**
 * FareOverrideRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class FareOverrideRequestData  implements java.io.Serializable {
    private java.lang.String currencyCode;

    private java.math.BigDecimal amount;

    private java.lang.String ruleTariff;

    private java.lang.String carrierCode;

    private java.lang.String ruleNumber;

    private java.lang.String fareBasisCode;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Journey journey;

    public FareOverrideRequestData() {
    }

    public FareOverrideRequestData(
           java.lang.String currencyCode,
           java.math.BigDecimal amount,
           java.lang.String ruleTariff,
           java.lang.String carrierCode,
           java.lang.String ruleNumber,
           java.lang.String fareBasisCode,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Journey journey) {
           this.currencyCode = currencyCode;
           this.amount = amount;
           this.ruleTariff = ruleTariff;
           this.carrierCode = carrierCode;
           this.ruleNumber = ruleNumber;
           this.fareBasisCode = fareBasisCode;
           this.journey = journey;
    }


    /**
     * Gets the currencyCode value for this FareOverrideRequestData.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this FareOverrideRequestData.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the amount value for this FareOverrideRequestData.
     * 
     * @return amount
     */
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this FareOverrideRequestData.
     * 
     * @param amount
     */
    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }


    /**
     * Gets the ruleTariff value for this FareOverrideRequestData.
     * 
     * @return ruleTariff
     */
    public java.lang.String getRuleTariff() {
        return ruleTariff;
    }


    /**
     * Sets the ruleTariff value for this FareOverrideRequestData.
     * 
     * @param ruleTariff
     */
    public void setRuleTariff(java.lang.String ruleTariff) {
        this.ruleTariff = ruleTariff;
    }


    /**
     * Gets the carrierCode value for this FareOverrideRequestData.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this FareOverrideRequestData.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the ruleNumber value for this FareOverrideRequestData.
     * 
     * @return ruleNumber
     */
    public java.lang.String getRuleNumber() {
        return ruleNumber;
    }


    /**
     * Sets the ruleNumber value for this FareOverrideRequestData.
     * 
     * @param ruleNumber
     */
    public void setRuleNumber(java.lang.String ruleNumber) {
        this.ruleNumber = ruleNumber;
    }


    /**
     * Gets the fareBasisCode value for this FareOverrideRequestData.
     * 
     * @return fareBasisCode
     */
    public java.lang.String getFareBasisCode() {
        return fareBasisCode;
    }


    /**
     * Sets the fareBasisCode value for this FareOverrideRequestData.
     * 
     * @param fareBasisCode
     */
    public void setFareBasisCode(java.lang.String fareBasisCode) {
        this.fareBasisCode = fareBasisCode;
    }


    /**
     * Gets the journey value for this FareOverrideRequestData.
     * 
     * @return journey
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Journey getJourney() {
        return journey;
    }


    /**
     * Sets the journey value for this FareOverrideRequestData.
     * 
     * @param journey
     */
    public void setJourney(com.navitaire.schemas.WebServices.DataContracts.Booking.Journey journey) {
        this.journey = journey;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FareOverrideRequestData)) return false;
        FareOverrideRequestData other = (FareOverrideRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.ruleTariff==null && other.getRuleTariff()==null) || 
             (this.ruleTariff!=null &&
              this.ruleTariff.equals(other.getRuleTariff()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.ruleNumber==null && other.getRuleNumber()==null) || 
             (this.ruleNumber!=null &&
              this.ruleNumber.equals(other.getRuleNumber()))) &&
            ((this.fareBasisCode==null && other.getFareBasisCode()==null) || 
             (this.fareBasisCode!=null &&
              this.fareBasisCode.equals(other.getFareBasisCode()))) &&
            ((this.journey==null && other.getJourney()==null) || 
             (this.journey!=null &&
              this.journey.equals(other.getJourney())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getRuleTariff() != null) {
            _hashCode += getRuleTariff().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getRuleNumber() != null) {
            _hashCode += getRuleNumber().hashCode();
        }
        if (getFareBasisCode() != null) {
            _hashCode += getFareBasisCode().hashCode();
        }
        if (getJourney() != null) {
            _hashCode += getJourney().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FareOverrideRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareOverrideRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleTariff");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RuleTariff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RuleNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareBasisCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareBasisCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
