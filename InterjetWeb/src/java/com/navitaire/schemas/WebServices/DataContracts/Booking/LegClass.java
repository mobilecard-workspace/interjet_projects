/**
 * LegClass.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class LegClass  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Short classNest;

    private java.lang.String classOfService;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ClassStatus status;

    private java.lang.String classType;

    private java.lang.Short classAllotted;

    private java.lang.Short classRank;

    private java.lang.Short classAU;

    private java.lang.Short classSold;

    private java.lang.Short nonStopSold;

    private java.lang.Short thruSold;

    private java.lang.Short cnxSold;

    private java.lang.Short latestAdvRes;

    public LegClass() {
    }

    public LegClass(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Short classNest,
           java.lang.String classOfService,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ClassStatus status,
           java.lang.String classType,
           java.lang.Short classAllotted,
           java.lang.Short classRank,
           java.lang.Short classAU,
           java.lang.Short classSold,
           java.lang.Short nonStopSold,
           java.lang.Short thruSold,
           java.lang.Short cnxSold,
           java.lang.Short latestAdvRes) {
        super(
            state);
        this.classNest = classNest;
        this.classOfService = classOfService;
        this.status = status;
        this.classType = classType;
        this.classAllotted = classAllotted;
        this.classRank = classRank;
        this.classAU = classAU;
        this.classSold = classSold;
        this.nonStopSold = nonStopSold;
        this.thruSold = thruSold;
        this.cnxSold = cnxSold;
        this.latestAdvRes = latestAdvRes;
    }


    /**
     * Gets the classNest value for this LegClass.
     * 
     * @return classNest
     */
    public java.lang.Short getClassNest() {
        return classNest;
    }


    /**
     * Sets the classNest value for this LegClass.
     * 
     * @param classNest
     */
    public void setClassNest(java.lang.Short classNest) {
        this.classNest = classNest;
    }


    /**
     * Gets the classOfService value for this LegClass.
     * 
     * @return classOfService
     */
    public java.lang.String getClassOfService() {
        return classOfService;
    }


    /**
     * Sets the classOfService value for this LegClass.
     * 
     * @param classOfService
     */
    public void setClassOfService(java.lang.String classOfService) {
        this.classOfService = classOfService;
    }


    /**
     * Gets the status value for this LegClass.
     * 
     * @return status
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ClassStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this LegClass.
     * 
     * @param status
     */
    public void setStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ClassStatus status) {
        this.status = status;
    }


    /**
     * Gets the classType value for this LegClass.
     * 
     * @return classType
     */
    public java.lang.String getClassType() {
        return classType;
    }


    /**
     * Sets the classType value for this LegClass.
     * 
     * @param classType
     */
    public void setClassType(java.lang.String classType) {
        this.classType = classType;
    }


    /**
     * Gets the classAllotted value for this LegClass.
     * 
     * @return classAllotted
     */
    public java.lang.Short getClassAllotted() {
        return classAllotted;
    }


    /**
     * Sets the classAllotted value for this LegClass.
     * 
     * @param classAllotted
     */
    public void setClassAllotted(java.lang.Short classAllotted) {
        this.classAllotted = classAllotted;
    }


    /**
     * Gets the classRank value for this LegClass.
     * 
     * @return classRank
     */
    public java.lang.Short getClassRank() {
        return classRank;
    }


    /**
     * Sets the classRank value for this LegClass.
     * 
     * @param classRank
     */
    public void setClassRank(java.lang.Short classRank) {
        this.classRank = classRank;
    }


    /**
     * Gets the classAU value for this LegClass.
     * 
     * @return classAU
     */
    public java.lang.Short getClassAU() {
        return classAU;
    }


    /**
     * Sets the classAU value for this LegClass.
     * 
     * @param classAU
     */
    public void setClassAU(java.lang.Short classAU) {
        this.classAU = classAU;
    }


    /**
     * Gets the classSold value for this LegClass.
     * 
     * @return classSold
     */
    public java.lang.Short getClassSold() {
        return classSold;
    }


    /**
     * Sets the classSold value for this LegClass.
     * 
     * @param classSold
     */
    public void setClassSold(java.lang.Short classSold) {
        this.classSold = classSold;
    }


    /**
     * Gets the nonStopSold value for this LegClass.
     * 
     * @return nonStopSold
     */
    public java.lang.Short getNonStopSold() {
        return nonStopSold;
    }


    /**
     * Sets the nonStopSold value for this LegClass.
     * 
     * @param nonStopSold
     */
    public void setNonStopSold(java.lang.Short nonStopSold) {
        this.nonStopSold = nonStopSold;
    }


    /**
     * Gets the thruSold value for this LegClass.
     * 
     * @return thruSold
     */
    public java.lang.Short getThruSold() {
        return thruSold;
    }


    /**
     * Sets the thruSold value for this LegClass.
     * 
     * @param thruSold
     */
    public void setThruSold(java.lang.Short thruSold) {
        this.thruSold = thruSold;
    }


    /**
     * Gets the cnxSold value for this LegClass.
     * 
     * @return cnxSold
     */
    public java.lang.Short getCnxSold() {
        return cnxSold;
    }


    /**
     * Sets the cnxSold value for this LegClass.
     * 
     * @param cnxSold
     */
    public void setCnxSold(java.lang.Short cnxSold) {
        this.cnxSold = cnxSold;
    }


    /**
     * Gets the latestAdvRes value for this LegClass.
     * 
     * @return latestAdvRes
     */
    public java.lang.Short getLatestAdvRes() {
        return latestAdvRes;
    }


    /**
     * Sets the latestAdvRes value for this LegClass.
     * 
     * @param latestAdvRes
     */
    public void setLatestAdvRes(java.lang.Short latestAdvRes) {
        this.latestAdvRes = latestAdvRes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LegClass)) return false;
        LegClass other = (LegClass) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.classNest==null && other.getClassNest()==null) || 
             (this.classNest!=null &&
              this.classNest.equals(other.getClassNest()))) &&
            ((this.classOfService==null && other.getClassOfService()==null) || 
             (this.classOfService!=null &&
              this.classOfService.equals(other.getClassOfService()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.classType==null && other.getClassType()==null) || 
             (this.classType!=null &&
              this.classType.equals(other.getClassType()))) &&
            ((this.classAllotted==null && other.getClassAllotted()==null) || 
             (this.classAllotted!=null &&
              this.classAllotted.equals(other.getClassAllotted()))) &&
            ((this.classRank==null && other.getClassRank()==null) || 
             (this.classRank!=null &&
              this.classRank.equals(other.getClassRank()))) &&
            ((this.classAU==null && other.getClassAU()==null) || 
             (this.classAU!=null &&
              this.classAU.equals(other.getClassAU()))) &&
            ((this.classSold==null && other.getClassSold()==null) || 
             (this.classSold!=null &&
              this.classSold.equals(other.getClassSold()))) &&
            ((this.nonStopSold==null && other.getNonStopSold()==null) || 
             (this.nonStopSold!=null &&
              this.nonStopSold.equals(other.getNonStopSold()))) &&
            ((this.thruSold==null && other.getThruSold()==null) || 
             (this.thruSold!=null &&
              this.thruSold.equals(other.getThruSold()))) &&
            ((this.cnxSold==null && other.getCnxSold()==null) || 
             (this.cnxSold!=null &&
              this.cnxSold.equals(other.getCnxSold()))) &&
            ((this.latestAdvRes==null && other.getLatestAdvRes()==null) || 
             (this.latestAdvRes!=null &&
              this.latestAdvRes.equals(other.getLatestAdvRes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getClassNest() != null) {
            _hashCode += getClassNest().hashCode();
        }
        if (getClassOfService() != null) {
            _hashCode += getClassOfService().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getClassType() != null) {
            _hashCode += getClassType().hashCode();
        }
        if (getClassAllotted() != null) {
            _hashCode += getClassAllotted().hashCode();
        }
        if (getClassRank() != null) {
            _hashCode += getClassRank().hashCode();
        }
        if (getClassAU() != null) {
            _hashCode += getClassAU().hashCode();
        }
        if (getClassSold() != null) {
            _hashCode += getClassSold().hashCode();
        }
        if (getNonStopSold() != null) {
            _hashCode += getNonStopSold().hashCode();
        }
        if (getThruSold() != null) {
            _hashCode += getThruSold().hashCode();
        }
        if (getCnxSold() != null) {
            _hashCode += getCnxSold().hashCode();
        }
        if (getLatestAdvRes() != null) {
            _hashCode += getLatestAdvRes().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LegClass.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegClass"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classNest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassNest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ClassStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classAllotted");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassAllotted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classRank");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassRank"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classAU");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassAU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classSold");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassSold"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nonStopSold");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NonStopSold"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("thruSold");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ThruSold"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnxSold");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CnxSold"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latestAdvRes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LatestAdvRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
