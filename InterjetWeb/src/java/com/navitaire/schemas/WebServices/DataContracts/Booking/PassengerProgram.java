/**
 * PassengerProgram.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PassengerProgram  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String programCode;

    private java.lang.String programLevelCode;

    private java.lang.String programNumber;

    public PassengerProgram() {
    }

    public PassengerProgram(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String programCode,
           java.lang.String programLevelCode,
           java.lang.String programNumber) {
        super(
            state);
        this.programCode = programCode;
        this.programLevelCode = programLevelCode;
        this.programNumber = programNumber;
    }


    /**
     * Gets the programCode value for this PassengerProgram.
     * 
     * @return programCode
     */
    public java.lang.String getProgramCode() {
        return programCode;
    }


    /**
     * Sets the programCode value for this PassengerProgram.
     * 
     * @param programCode
     */
    public void setProgramCode(java.lang.String programCode) {
        this.programCode = programCode;
    }


    /**
     * Gets the programLevelCode value for this PassengerProgram.
     * 
     * @return programLevelCode
     */
    public java.lang.String getProgramLevelCode() {
        return programLevelCode;
    }


    /**
     * Sets the programLevelCode value for this PassengerProgram.
     * 
     * @param programLevelCode
     */
    public void setProgramLevelCode(java.lang.String programLevelCode) {
        this.programLevelCode = programLevelCode;
    }


    /**
     * Gets the programNumber value for this PassengerProgram.
     * 
     * @return programNumber
     */
    public java.lang.String getProgramNumber() {
        return programNumber;
    }


    /**
     * Sets the programNumber value for this PassengerProgram.
     * 
     * @param programNumber
     */
    public void setProgramNumber(java.lang.String programNumber) {
        this.programNumber = programNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PassengerProgram)) return false;
        PassengerProgram other = (PassengerProgram) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.programCode==null && other.getProgramCode()==null) || 
             (this.programCode!=null &&
              this.programCode.equals(other.getProgramCode()))) &&
            ((this.programLevelCode==null && other.getProgramLevelCode()==null) || 
             (this.programLevelCode!=null &&
              this.programLevelCode.equals(other.getProgramLevelCode()))) &&
            ((this.programNumber==null && other.getProgramNumber()==null) || 
             (this.programNumber!=null &&
              this.programNumber.equals(other.getProgramNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getProgramCode() != null) {
            _hashCode += getProgramCode().hashCode();
        }
        if (getProgramLevelCode() != null) {
            _hashCode += getProgramLevelCode().hashCode();
        }
        if (getProgramNumber() != null) {
            _hashCode += getProgramNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PassengerProgram.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerProgram"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("programCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProgramCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("programLevelCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProgramLevelCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("programNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProgramNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
