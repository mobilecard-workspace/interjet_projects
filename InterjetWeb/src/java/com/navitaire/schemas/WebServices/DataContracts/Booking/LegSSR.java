/**
 * LegSSR.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class LegSSR  implements java.io.Serializable {
    private java.lang.String SSRNestCode;

    private java.lang.Short SSRLid;

    private java.lang.Short SSRSold;

    private java.lang.Short SSRValueSold;

    public LegSSR() {
    }

    public LegSSR(
           java.lang.String SSRNestCode,
           java.lang.Short SSRLid,
           java.lang.Short SSRSold,
           java.lang.Short SSRValueSold) {
           this.SSRNestCode = SSRNestCode;
           this.SSRLid = SSRLid;
           this.SSRSold = SSRSold;
           this.SSRValueSold = SSRValueSold;
    }


    /**
     * Gets the SSRNestCode value for this LegSSR.
     * 
     * @return SSRNestCode
     */
    public java.lang.String getSSRNestCode() {
        return SSRNestCode;
    }


    /**
     * Sets the SSRNestCode value for this LegSSR.
     * 
     * @param SSRNestCode
     */
    public void setSSRNestCode(java.lang.String SSRNestCode) {
        this.SSRNestCode = SSRNestCode;
    }


    /**
     * Gets the SSRLid value for this LegSSR.
     * 
     * @return SSRLid
     */
    public java.lang.Short getSSRLid() {
        return SSRLid;
    }


    /**
     * Sets the SSRLid value for this LegSSR.
     * 
     * @param SSRLid
     */
    public void setSSRLid(java.lang.Short SSRLid) {
        this.SSRLid = SSRLid;
    }


    /**
     * Gets the SSRSold value for this LegSSR.
     * 
     * @return SSRSold
     */
    public java.lang.Short getSSRSold() {
        return SSRSold;
    }


    /**
     * Sets the SSRSold value for this LegSSR.
     * 
     * @param SSRSold
     */
    public void setSSRSold(java.lang.Short SSRSold) {
        this.SSRSold = SSRSold;
    }


    /**
     * Gets the SSRValueSold value for this LegSSR.
     * 
     * @return SSRValueSold
     */
    public java.lang.Short getSSRValueSold() {
        return SSRValueSold;
    }


    /**
     * Sets the SSRValueSold value for this LegSSR.
     * 
     * @param SSRValueSold
     */
    public void setSSRValueSold(java.lang.Short SSRValueSold) {
        this.SSRValueSold = SSRValueSold;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LegSSR)) return false;
        LegSSR other = (LegSSR) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.SSRNestCode==null && other.getSSRNestCode()==null) || 
             (this.SSRNestCode!=null &&
              this.SSRNestCode.equals(other.getSSRNestCode()))) &&
            ((this.SSRLid==null && other.getSSRLid()==null) || 
             (this.SSRLid!=null &&
              this.SSRLid.equals(other.getSSRLid()))) &&
            ((this.SSRSold==null && other.getSSRSold()==null) || 
             (this.SSRSold!=null &&
              this.SSRSold.equals(other.getSSRSold()))) &&
            ((this.SSRValueSold==null && other.getSSRValueSold()==null) || 
             (this.SSRValueSold!=null &&
              this.SSRValueSold.equals(other.getSSRValueSold())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSSRNestCode() != null) {
            _hashCode += getSSRNestCode().hashCode();
        }
        if (getSSRLid() != null) {
            _hashCode += getSSRLid().hashCode();
        }
        if (getSSRSold() != null) {
            _hashCode += getSSRSold().hashCode();
        }
        if (getSSRValueSold() != null) {
            _hashCode += getSSRValueSold().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LegSSR.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegSSR"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRNestCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRNestCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRLid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRLid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRSold");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRSold"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRValueSold");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRValueSold"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
