/**
 * ThreeDSecureRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class ThreeDSecureRequest  implements java.io.Serializable {
    private java.lang.String browserUserAgent;

    private java.lang.String browserAccept;

    private java.lang.String remoteIpAddress;

    private java.lang.String termUrl;

    private java.lang.String proxyVia;

    public ThreeDSecureRequest() {
    }

    public ThreeDSecureRequest(
           java.lang.String browserUserAgent,
           java.lang.String browserAccept,
           java.lang.String remoteIpAddress,
           java.lang.String termUrl,
           java.lang.String proxyVia) {
           this.browserUserAgent = browserUserAgent;
           this.browserAccept = browserAccept;
           this.remoteIpAddress = remoteIpAddress;
           this.termUrl = termUrl;
           this.proxyVia = proxyVia;
    }


    /**
     * Gets the browserUserAgent value for this ThreeDSecureRequest.
     * 
     * @return browserUserAgent
     */
    public java.lang.String getBrowserUserAgent() {
        return browserUserAgent;
    }


    /**
     * Sets the browserUserAgent value for this ThreeDSecureRequest.
     * 
     * @param browserUserAgent
     */
    public void setBrowserUserAgent(java.lang.String browserUserAgent) {
        this.browserUserAgent = browserUserAgent;
    }


    /**
     * Gets the browserAccept value for this ThreeDSecureRequest.
     * 
     * @return browserAccept
     */
    public java.lang.String getBrowserAccept() {
        return browserAccept;
    }


    /**
     * Sets the browserAccept value for this ThreeDSecureRequest.
     * 
     * @param browserAccept
     */
    public void setBrowserAccept(java.lang.String browserAccept) {
        this.browserAccept = browserAccept;
    }


    /**
     * Gets the remoteIpAddress value for this ThreeDSecureRequest.
     * 
     * @return remoteIpAddress
     */
    public java.lang.String getRemoteIpAddress() {
        return remoteIpAddress;
    }


    /**
     * Sets the remoteIpAddress value for this ThreeDSecureRequest.
     * 
     * @param remoteIpAddress
     */
    public void setRemoteIpAddress(java.lang.String remoteIpAddress) {
        this.remoteIpAddress = remoteIpAddress;
    }


    /**
     * Gets the termUrl value for this ThreeDSecureRequest.
     * 
     * @return termUrl
     */
    public java.lang.String getTermUrl() {
        return termUrl;
    }


    /**
     * Sets the termUrl value for this ThreeDSecureRequest.
     * 
     * @param termUrl
     */
    public void setTermUrl(java.lang.String termUrl) {
        this.termUrl = termUrl;
    }


    /**
     * Gets the proxyVia value for this ThreeDSecureRequest.
     * 
     * @return proxyVia
     */
    public java.lang.String getProxyVia() {
        return proxyVia;
    }


    /**
     * Sets the proxyVia value for this ThreeDSecureRequest.
     * 
     * @param proxyVia
     */
    public void setProxyVia(java.lang.String proxyVia) {
        this.proxyVia = proxyVia;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ThreeDSecureRequest)) return false;
        ThreeDSecureRequest other = (ThreeDSecureRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browserUserAgent==null && other.getBrowserUserAgent()==null) || 
             (this.browserUserAgent!=null &&
              this.browserUserAgent.equals(other.getBrowserUserAgent()))) &&
            ((this.browserAccept==null && other.getBrowserAccept()==null) || 
             (this.browserAccept!=null &&
              this.browserAccept.equals(other.getBrowserAccept()))) &&
            ((this.remoteIpAddress==null && other.getRemoteIpAddress()==null) || 
             (this.remoteIpAddress!=null &&
              this.remoteIpAddress.equals(other.getRemoteIpAddress()))) &&
            ((this.termUrl==null && other.getTermUrl()==null) || 
             (this.termUrl!=null &&
              this.termUrl.equals(other.getTermUrl()))) &&
            ((this.proxyVia==null && other.getProxyVia()==null) || 
             (this.proxyVia!=null &&
              this.proxyVia.equals(other.getProxyVia())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowserUserAgent() != null) {
            _hashCode += getBrowserUserAgent().hashCode();
        }
        if (getBrowserAccept() != null) {
            _hashCode += getBrowserAccept().hashCode();
        }
        if (getRemoteIpAddress() != null) {
            _hashCode += getRemoteIpAddress().hashCode();
        }
        if (getTermUrl() != null) {
            _hashCode += getTermUrl().hashCode();
        }
        if (getProxyVia() != null) {
            _hashCode += getProxyVia().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ThreeDSecureRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ThreeDSecureRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browserUserAgent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BrowserUserAgent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browserAccept");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BrowserAccept"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remoteIpAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RemoteIpAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("termUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TermUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proxyVia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProxyVia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
