/**
 * SSRSegment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SSRSegment  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey legKey;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.AvailablePaxSSR[] availablePaxSSRList;

    public SSRSegment() {
    }

    public SSRSegment(
           com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey legKey,
           com.navitaire.schemas.WebServices.DataContracts.Booking.AvailablePaxSSR[] availablePaxSSRList) {
           this.legKey = legKey;
           this.availablePaxSSRList = availablePaxSSRList;
    }


    /**
     * Gets the legKey value for this SSRSegment.
     * 
     * @return legKey
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey getLegKey() {
        return legKey;
    }


    /**
     * Sets the legKey value for this SSRSegment.
     * 
     * @param legKey
     */
    public void setLegKey(com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey legKey) {
        this.legKey = legKey;
    }


    /**
     * Gets the availablePaxSSRList value for this SSRSegment.
     * 
     * @return availablePaxSSRList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.AvailablePaxSSR[] getAvailablePaxSSRList() {
        return availablePaxSSRList;
    }


    /**
     * Sets the availablePaxSSRList value for this SSRSegment.
     * 
     * @param availablePaxSSRList
     */
    public void setAvailablePaxSSRList(com.navitaire.schemas.WebServices.DataContracts.Booking.AvailablePaxSSR[] availablePaxSSRList) {
        this.availablePaxSSRList = availablePaxSSRList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SSRSegment)) return false;
        SSRSegment other = (SSRSegment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.legKey==null && other.getLegKey()==null) || 
             (this.legKey!=null &&
              this.legKey.equals(other.getLegKey()))) &&
            ((this.availablePaxSSRList==null && other.getAvailablePaxSSRList()==null) || 
             (this.availablePaxSSRList!=null &&
              java.util.Arrays.equals(this.availablePaxSSRList, other.getAvailablePaxSSRList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLegKey() != null) {
            _hashCode += getLegKey().hashCode();
        }
        if (getAvailablePaxSSRList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAvailablePaxSSRList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAvailablePaxSSRList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SSRSegment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRSegment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("legKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegKey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availablePaxSSRList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailablePaxSSRList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailablePaxSSR"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailablePaxSSR"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
