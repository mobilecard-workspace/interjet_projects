/**
 * Segment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class Segment  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String actionStatusCode;

    private java.lang.String arrivalStation;

    private java.lang.String cabinOfService;

    private java.lang.String changeReasonCode;

    private java.lang.String departureStation;

    private java.lang.String priorityCode;

    private java.lang.String segmentType;

    private java.util.Calendar STA;

    private java.util.Calendar STD;

    private java.lang.Boolean international;

    private com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator;

    private com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator xrefFlightDesignator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Fare[] fares;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[] legs;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxBag[] paxBags;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeat[] paxSeats;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSR[] paxSSRs;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSegment[] paxSegments;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicket[] paxTickets;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] paxSeatPreferences;

    private java.util.Calendar salesDate;

    private java.lang.String segmentSellKey;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxScore[] paxScores;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType;

    public Segment() {
    }

    public Segment(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String actionStatusCode,
           java.lang.String arrivalStation,
           java.lang.String cabinOfService,
           java.lang.String changeReasonCode,
           java.lang.String departureStation,
           java.lang.String priorityCode,
           java.lang.String segmentType,
           java.util.Calendar STA,
           java.util.Calendar STD,
           java.lang.Boolean international,
           com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator,
           com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator xrefFlightDesignator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Fare[] fares,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[] legs,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxBag[] paxBags,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeat[] paxSeats,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSR[] paxSSRs,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSegment[] paxSegments,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicket[] paxTickets,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] paxSeatPreferences,
           java.util.Calendar salesDate,
           java.lang.String segmentSellKey,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxScore[] paxScores,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType) {
        super(
            state);
        this.actionStatusCode = actionStatusCode;
        this.arrivalStation = arrivalStation;
        this.cabinOfService = cabinOfService;
        this.changeReasonCode = changeReasonCode;
        this.departureStation = departureStation;
        this.priorityCode = priorityCode;
        this.segmentType = segmentType;
        this.STA = STA;
        this.STD = STD;
        this.international = international;
        this.flightDesignator = flightDesignator;
        this.xrefFlightDesignator = xrefFlightDesignator;
        this.fares = fares;
        this.legs = legs;
        this.paxBags = paxBags;
        this.paxSeats = paxSeats;
        this.paxSSRs = paxSSRs;
        this.paxSegments = paxSegments;
        this.paxTickets = paxTickets;
        this.paxSeatPreferences = paxSeatPreferences;
        this.salesDate = salesDate;
        this.segmentSellKey = segmentSellKey;
        this.paxScores = paxScores;
        this.channelType = channelType;
    }


    /**
     * Gets the actionStatusCode value for this Segment.
     * 
     * @return actionStatusCode
     */
    public java.lang.String getActionStatusCode() {
        return actionStatusCode;
    }


    /**
     * Sets the actionStatusCode value for this Segment.
     * 
     * @param actionStatusCode
     */
    public void setActionStatusCode(java.lang.String actionStatusCode) {
        this.actionStatusCode = actionStatusCode;
    }


    /**
     * Gets the arrivalStation value for this Segment.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this Segment.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the cabinOfService value for this Segment.
     * 
     * @return cabinOfService
     */
    public java.lang.String getCabinOfService() {
        return cabinOfService;
    }


    /**
     * Sets the cabinOfService value for this Segment.
     * 
     * @param cabinOfService
     */
    public void setCabinOfService(java.lang.String cabinOfService) {
        this.cabinOfService = cabinOfService;
    }


    /**
     * Gets the changeReasonCode value for this Segment.
     * 
     * @return changeReasonCode
     */
    public java.lang.String getChangeReasonCode() {
        return changeReasonCode;
    }


    /**
     * Sets the changeReasonCode value for this Segment.
     * 
     * @param changeReasonCode
     */
    public void setChangeReasonCode(java.lang.String changeReasonCode) {
        this.changeReasonCode = changeReasonCode;
    }


    /**
     * Gets the departureStation value for this Segment.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this Segment.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the priorityCode value for this Segment.
     * 
     * @return priorityCode
     */
    public java.lang.String getPriorityCode() {
        return priorityCode;
    }


    /**
     * Sets the priorityCode value for this Segment.
     * 
     * @param priorityCode
     */
    public void setPriorityCode(java.lang.String priorityCode) {
        this.priorityCode = priorityCode;
    }


    /**
     * Gets the segmentType value for this Segment.
     * 
     * @return segmentType
     */
    public java.lang.String getSegmentType() {
        return segmentType;
    }


    /**
     * Sets the segmentType value for this Segment.
     * 
     * @param segmentType
     */
    public void setSegmentType(java.lang.String segmentType) {
        this.segmentType = segmentType;
    }


    /**
     * Gets the STA value for this Segment.
     * 
     * @return STA
     */
    public java.util.Calendar getSTA() {
        return STA;
    }


    /**
     * Sets the STA value for this Segment.
     * 
     * @param STA
     */
    public void setSTA(java.util.Calendar STA) {
        this.STA = STA;
    }


    /**
     * Gets the STD value for this Segment.
     * 
     * @return STD
     */
    public java.util.Calendar getSTD() {
        return STD;
    }


    /**
     * Sets the STD value for this Segment.
     * 
     * @param STD
     */
    public void setSTD(java.util.Calendar STD) {
        this.STD = STD;
    }


    /**
     * Gets the international value for this Segment.
     * 
     * @return international
     */
    public java.lang.Boolean getInternational() {
        return international;
    }


    /**
     * Sets the international value for this Segment.
     * 
     * @param international
     */
    public void setInternational(java.lang.Boolean international) {
        this.international = international;
    }


    /**
     * Gets the flightDesignator value for this Segment.
     * 
     * @return flightDesignator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator getFlightDesignator() {
        return flightDesignator;
    }


    /**
     * Sets the flightDesignator value for this Segment.
     * 
     * @param flightDesignator
     */
    public void setFlightDesignator(com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator) {
        this.flightDesignator = flightDesignator;
    }


    /**
     * Gets the xrefFlightDesignator value for this Segment.
     * 
     * @return xrefFlightDesignator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator getXrefFlightDesignator() {
        return xrefFlightDesignator;
    }


    /**
     * Sets the xrefFlightDesignator value for this Segment.
     * 
     * @param xrefFlightDesignator
     */
    public void setXrefFlightDesignator(com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator xrefFlightDesignator) {
        this.xrefFlightDesignator = xrefFlightDesignator;
    }


    /**
     * Gets the fares value for this Segment.
     * 
     * @return fares
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Fare[] getFares() {
        return fares;
    }


    /**
     * Sets the fares value for this Segment.
     * 
     * @param fares
     */
    public void setFares(com.navitaire.schemas.WebServices.DataContracts.Booking.Fare[] fares) {
        this.fares = fares;
    }


    /**
     * Gets the legs value for this Segment.
     * 
     * @return legs
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[] getLegs() {
        return legs;
    }


    /**
     * Sets the legs value for this Segment.
     * 
     * @param legs
     */
    public void setLegs(com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[] legs) {
        this.legs = legs;
    }


    /**
     * Gets the paxBags value for this Segment.
     * 
     * @return paxBags
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxBag[] getPaxBags() {
        return paxBags;
    }


    /**
     * Sets the paxBags value for this Segment.
     * 
     * @param paxBags
     */
    public void setPaxBags(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxBag[] paxBags) {
        this.paxBags = paxBags;
    }


    /**
     * Gets the paxSeats value for this Segment.
     * 
     * @return paxSeats
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeat[] getPaxSeats() {
        return paxSeats;
    }


    /**
     * Sets the paxSeats value for this Segment.
     * 
     * @param paxSeats
     */
    public void setPaxSeats(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeat[] paxSeats) {
        this.paxSeats = paxSeats;
    }


    /**
     * Gets the paxSSRs value for this Segment.
     * 
     * @return paxSSRs
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSR[] getPaxSSRs() {
        return paxSSRs;
    }


    /**
     * Sets the paxSSRs value for this Segment.
     * 
     * @param paxSSRs
     */
    public void setPaxSSRs(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSR[] paxSSRs) {
        this.paxSSRs = paxSSRs;
    }


    /**
     * Gets the paxSegments value for this Segment.
     * 
     * @return paxSegments
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSegment[] getPaxSegments() {
        return paxSegments;
    }


    /**
     * Sets the paxSegments value for this Segment.
     * 
     * @param paxSegments
     */
    public void setPaxSegments(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSegment[] paxSegments) {
        this.paxSegments = paxSegments;
    }


    /**
     * Gets the paxTickets value for this Segment.
     * 
     * @return paxTickets
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicket[] getPaxTickets() {
        return paxTickets;
    }


    /**
     * Sets the paxTickets value for this Segment.
     * 
     * @param paxTickets
     */
    public void setPaxTickets(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicket[] paxTickets) {
        this.paxTickets = paxTickets;
    }


    /**
     * Gets the paxSeatPreferences value for this Segment.
     * 
     * @return paxSeatPreferences
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] getPaxSeatPreferences() {
        return paxSeatPreferences;
    }


    /**
     * Sets the paxSeatPreferences value for this Segment.
     * 
     * @param paxSeatPreferences
     */
    public void setPaxSeatPreferences(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] paxSeatPreferences) {
        this.paxSeatPreferences = paxSeatPreferences;
    }


    /**
     * Gets the salesDate value for this Segment.
     * 
     * @return salesDate
     */
    public java.util.Calendar getSalesDate() {
        return salesDate;
    }


    /**
     * Sets the salesDate value for this Segment.
     * 
     * @param salesDate
     */
    public void setSalesDate(java.util.Calendar salesDate) {
        this.salesDate = salesDate;
    }


    /**
     * Gets the segmentSellKey value for this Segment.
     * 
     * @return segmentSellKey
     */
    public java.lang.String getSegmentSellKey() {
        return segmentSellKey;
    }


    /**
     * Sets the segmentSellKey value for this Segment.
     * 
     * @param segmentSellKey
     */
    public void setSegmentSellKey(java.lang.String segmentSellKey) {
        this.segmentSellKey = segmentSellKey;
    }


    /**
     * Gets the paxScores value for this Segment.
     * 
     * @return paxScores
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxScore[] getPaxScores() {
        return paxScores;
    }


    /**
     * Sets the paxScores value for this Segment.
     * 
     * @param paxScores
     */
    public void setPaxScores(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxScore[] paxScores) {
        this.paxScores = paxScores;
    }


    /**
     * Gets the channelType value for this Segment.
     * 
     * @return channelType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType getChannelType() {
        return channelType;
    }


    /**
     * Sets the channelType value for this Segment.
     * 
     * @param channelType
     */
    public void setChannelType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType) {
        this.channelType = channelType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Segment)) return false;
        Segment other = (Segment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.actionStatusCode==null && other.getActionStatusCode()==null) || 
             (this.actionStatusCode!=null &&
              this.actionStatusCode.equals(other.getActionStatusCode()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.cabinOfService==null && other.getCabinOfService()==null) || 
             (this.cabinOfService!=null &&
              this.cabinOfService.equals(other.getCabinOfService()))) &&
            ((this.changeReasonCode==null && other.getChangeReasonCode()==null) || 
             (this.changeReasonCode!=null &&
              this.changeReasonCode.equals(other.getChangeReasonCode()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.priorityCode==null && other.getPriorityCode()==null) || 
             (this.priorityCode!=null &&
              this.priorityCode.equals(other.getPriorityCode()))) &&
            ((this.segmentType==null && other.getSegmentType()==null) || 
             (this.segmentType!=null &&
              this.segmentType.equals(other.getSegmentType()))) &&
            ((this.STA==null && other.getSTA()==null) || 
             (this.STA!=null &&
              this.STA.equals(other.getSTA()))) &&
            ((this.STD==null && other.getSTD()==null) || 
             (this.STD!=null &&
              this.STD.equals(other.getSTD()))) &&
            ((this.international==null && other.getInternational()==null) || 
             (this.international!=null &&
              this.international.equals(other.getInternational()))) &&
            ((this.flightDesignator==null && other.getFlightDesignator()==null) || 
             (this.flightDesignator!=null &&
              this.flightDesignator.equals(other.getFlightDesignator()))) &&
            ((this.xrefFlightDesignator==null && other.getXrefFlightDesignator()==null) || 
             (this.xrefFlightDesignator!=null &&
              this.xrefFlightDesignator.equals(other.getXrefFlightDesignator()))) &&
            ((this.fares==null && other.getFares()==null) || 
             (this.fares!=null &&
              java.util.Arrays.equals(this.fares, other.getFares()))) &&
            ((this.legs==null && other.getLegs()==null) || 
             (this.legs!=null &&
              java.util.Arrays.equals(this.legs, other.getLegs()))) &&
            ((this.paxBags==null && other.getPaxBags()==null) || 
             (this.paxBags!=null &&
              java.util.Arrays.equals(this.paxBags, other.getPaxBags()))) &&
            ((this.paxSeats==null && other.getPaxSeats()==null) || 
             (this.paxSeats!=null &&
              java.util.Arrays.equals(this.paxSeats, other.getPaxSeats()))) &&
            ((this.paxSSRs==null && other.getPaxSSRs()==null) || 
             (this.paxSSRs!=null &&
              java.util.Arrays.equals(this.paxSSRs, other.getPaxSSRs()))) &&
            ((this.paxSegments==null && other.getPaxSegments()==null) || 
             (this.paxSegments!=null &&
              java.util.Arrays.equals(this.paxSegments, other.getPaxSegments()))) &&
            ((this.paxTickets==null && other.getPaxTickets()==null) || 
             (this.paxTickets!=null &&
              java.util.Arrays.equals(this.paxTickets, other.getPaxTickets()))) &&
            ((this.paxSeatPreferences==null && other.getPaxSeatPreferences()==null) || 
             (this.paxSeatPreferences!=null &&
              java.util.Arrays.equals(this.paxSeatPreferences, other.getPaxSeatPreferences()))) &&
            ((this.salesDate==null && other.getSalesDate()==null) || 
             (this.salesDate!=null &&
              this.salesDate.equals(other.getSalesDate()))) &&
            ((this.segmentSellKey==null && other.getSegmentSellKey()==null) || 
             (this.segmentSellKey!=null &&
              this.segmentSellKey.equals(other.getSegmentSellKey()))) &&
            ((this.paxScores==null && other.getPaxScores()==null) || 
             (this.paxScores!=null &&
              java.util.Arrays.equals(this.paxScores, other.getPaxScores()))) &&
            ((this.channelType==null && other.getChannelType()==null) || 
             (this.channelType!=null &&
              this.channelType.equals(other.getChannelType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getActionStatusCode() != null) {
            _hashCode += getActionStatusCode().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getCabinOfService() != null) {
            _hashCode += getCabinOfService().hashCode();
        }
        if (getChangeReasonCode() != null) {
            _hashCode += getChangeReasonCode().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getPriorityCode() != null) {
            _hashCode += getPriorityCode().hashCode();
        }
        if (getSegmentType() != null) {
            _hashCode += getSegmentType().hashCode();
        }
        if (getSTA() != null) {
            _hashCode += getSTA().hashCode();
        }
        if (getSTD() != null) {
            _hashCode += getSTD().hashCode();
        }
        if (getInternational() != null) {
            _hashCode += getInternational().hashCode();
        }
        if (getFlightDesignator() != null) {
            _hashCode += getFlightDesignator().hashCode();
        }
        if (getXrefFlightDesignator() != null) {
            _hashCode += getXrefFlightDesignator().hashCode();
        }
        if (getFares() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFares());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFares(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLegs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLegs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLegs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxBags() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxBags());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxBags(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxSeats() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxSeats());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxSeats(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxSSRs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxSSRs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxSSRs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxSegments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxSegments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxSegments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxTickets() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxTickets());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxTickets(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxSeatPreferences() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxSeatPreferences());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxSeatPreferences(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSalesDate() != null) {
            _hashCode += getSalesDate().hashCode();
        }
        if (getSegmentSellKey() != null) {
            _hashCode += getSegmentSellKey().hashCode();
        }
        if (getPaxScores() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxScores());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxScores(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getChannelType() != null) {
            _hashCode += getChannelType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Segment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cabinOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CabinOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeReasonCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChangeReasonCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priorityCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriorityCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STA");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("international");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "International"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FlightDesignator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xrefFlightDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "XrefFlightDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FlightDesignator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fares");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Fares"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Fare"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Fare"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("legs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Legs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Leg"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Leg"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxBags");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxBags"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxBag"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxBag"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxSeats");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeats"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeat"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeat"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxSSRs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSRs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSR"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSR"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxSegments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSegments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSegment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSegment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxTickets");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTickets"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicket"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicket"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxSeatPreferences");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreferences"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salesDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SalesDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentSellKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSellKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxScores");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxScores"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxScore"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxScore"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChannelType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChannelType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
