/**
 * EquipmentDeviation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class EquipmentDeviation  implements java.io.Serializable {
    private java.lang.String equipmentType;

    private java.lang.String equipmentTypeSuffix;

    private java.lang.String marketingCode;

    public EquipmentDeviation() {
    }

    public EquipmentDeviation(
           java.lang.String equipmentType,
           java.lang.String equipmentTypeSuffix,
           java.lang.String marketingCode) {
           this.equipmentType = equipmentType;
           this.equipmentTypeSuffix = equipmentTypeSuffix;
           this.marketingCode = marketingCode;
    }


    /**
     * Gets the equipmentType value for this EquipmentDeviation.
     * 
     * @return equipmentType
     */
    public java.lang.String getEquipmentType() {
        return equipmentType;
    }


    /**
     * Sets the equipmentType value for this EquipmentDeviation.
     * 
     * @param equipmentType
     */
    public void setEquipmentType(java.lang.String equipmentType) {
        this.equipmentType = equipmentType;
    }


    /**
     * Gets the equipmentTypeSuffix value for this EquipmentDeviation.
     * 
     * @return equipmentTypeSuffix
     */
    public java.lang.String getEquipmentTypeSuffix() {
        return equipmentTypeSuffix;
    }


    /**
     * Sets the equipmentTypeSuffix value for this EquipmentDeviation.
     * 
     * @param equipmentTypeSuffix
     */
    public void setEquipmentTypeSuffix(java.lang.String equipmentTypeSuffix) {
        this.equipmentTypeSuffix = equipmentTypeSuffix;
    }


    /**
     * Gets the marketingCode value for this EquipmentDeviation.
     * 
     * @return marketingCode
     */
    public java.lang.String getMarketingCode() {
        return marketingCode;
    }


    /**
     * Sets the marketingCode value for this EquipmentDeviation.
     * 
     * @param marketingCode
     */
    public void setMarketingCode(java.lang.String marketingCode) {
        this.marketingCode = marketingCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EquipmentDeviation)) return false;
        EquipmentDeviation other = (EquipmentDeviation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.equipmentType==null && other.getEquipmentType()==null) || 
             (this.equipmentType!=null &&
              this.equipmentType.equals(other.getEquipmentType()))) &&
            ((this.equipmentTypeSuffix==null && other.getEquipmentTypeSuffix()==null) || 
             (this.equipmentTypeSuffix!=null &&
              this.equipmentTypeSuffix.equals(other.getEquipmentTypeSuffix()))) &&
            ((this.marketingCode==null && other.getMarketingCode()==null) || 
             (this.marketingCode!=null &&
              this.marketingCode.equals(other.getMarketingCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEquipmentType() != null) {
            _hashCode += getEquipmentType().hashCode();
        }
        if (getEquipmentTypeSuffix() != null) {
            _hashCode += getEquipmentTypeSuffix().hashCode();
        }
        if (getMarketingCode() != null) {
            _hashCode += getMarketingCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EquipmentDeviation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentTypeSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentTypeSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketingCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MarketingCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
