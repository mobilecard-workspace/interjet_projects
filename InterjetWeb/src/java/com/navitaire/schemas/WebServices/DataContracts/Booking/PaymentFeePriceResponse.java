/**
 * PaymentFeePriceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaymentFeePriceResponse  implements java.io.Serializable {
    private java.lang.Boolean isFixedAmount;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee[] passengerFees;

    public PaymentFeePriceResponse() {
    }

    public PaymentFeePriceResponse(
           java.lang.Boolean isFixedAmount,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee[] passengerFees) {
           this.isFixedAmount = isFixedAmount;
           this.passengerFees = passengerFees;
    }


    /**
     * Gets the isFixedAmount value for this PaymentFeePriceResponse.
     * 
     * @return isFixedAmount
     */
    public java.lang.Boolean getIsFixedAmount() {
        return isFixedAmount;
    }


    /**
     * Sets the isFixedAmount value for this PaymentFeePriceResponse.
     * 
     * @param isFixedAmount
     */
    public void setIsFixedAmount(java.lang.Boolean isFixedAmount) {
        this.isFixedAmount = isFixedAmount;
    }


    /**
     * Gets the passengerFees value for this PaymentFeePriceResponse.
     * 
     * @return passengerFees
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee[] getPassengerFees() {
        return passengerFees;
    }


    /**
     * Sets the passengerFees value for this PaymentFeePriceResponse.
     * 
     * @param passengerFees
     */
    public void setPassengerFees(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee[] passengerFees) {
        this.passengerFees = passengerFees;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentFeePriceResponse)) return false;
        PaymentFeePriceResponse other = (PaymentFeePriceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.isFixedAmount==null && other.getIsFixedAmount()==null) || 
             (this.isFixedAmount!=null &&
              this.isFixedAmount.equals(other.getIsFixedAmount()))) &&
            ((this.passengerFees==null && other.getPassengerFees()==null) || 
             (this.passengerFees!=null &&
              java.util.Arrays.equals(this.passengerFees, other.getPassengerFees())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIsFixedAmount() != null) {
            _hashCode += getIsFixedAmount().hashCode();
        }
        if (getPassengerFees() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerFees());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerFees(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentFeePriceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentFeePriceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isFixedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IsFixedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerFees");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
