/**
 * MoveAvailabilityResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class MoveAvailabilityResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlyAheadOfferStatus flyAheadOfferStatus;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.JourneyDateMarket[] schedule;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Journey scheduledFromJourney;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInformationList;

    public MoveAvailabilityResponse() {
    }

    public MoveAvailabilityResponse(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlyAheadOfferStatus flyAheadOfferStatus,
           com.navitaire.schemas.WebServices.DataContracts.Booking.JourneyDateMarket[] schedule,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Journey scheduledFromJourney,
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInformationList) {
           this.flyAheadOfferStatus = flyAheadOfferStatus;
           this.schedule = schedule;
           this.scheduledFromJourney = scheduledFromJourney;
           this.otherServiceInformationList = otherServiceInformationList;
    }


    /**
     * Gets the flyAheadOfferStatus value for this MoveAvailabilityResponse.
     * 
     * @return flyAheadOfferStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlyAheadOfferStatus getFlyAheadOfferStatus() {
        return flyAheadOfferStatus;
    }


    /**
     * Sets the flyAheadOfferStatus value for this MoveAvailabilityResponse.
     * 
     * @param flyAheadOfferStatus
     */
    public void setFlyAheadOfferStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlyAheadOfferStatus flyAheadOfferStatus) {
        this.flyAheadOfferStatus = flyAheadOfferStatus;
    }


    /**
     * Gets the schedule value for this MoveAvailabilityResponse.
     * 
     * @return schedule
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.JourneyDateMarket[] getSchedule() {
        return schedule;
    }


    /**
     * Sets the schedule value for this MoveAvailabilityResponse.
     * 
     * @param schedule
     */
    public void setSchedule(com.navitaire.schemas.WebServices.DataContracts.Booking.JourneyDateMarket[] schedule) {
        this.schedule = schedule;
    }


    /**
     * Gets the scheduledFromJourney value for this MoveAvailabilityResponse.
     * 
     * @return scheduledFromJourney
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Journey getScheduledFromJourney() {
        return scheduledFromJourney;
    }


    /**
     * Sets the scheduledFromJourney value for this MoveAvailabilityResponse.
     * 
     * @param scheduledFromJourney
     */
    public void setScheduledFromJourney(com.navitaire.schemas.WebServices.DataContracts.Booking.Journey scheduledFromJourney) {
        this.scheduledFromJourney = scheduledFromJourney;
    }


    /**
     * Gets the otherServiceInformationList value for this MoveAvailabilityResponse.
     * 
     * @return otherServiceInformationList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] getOtherServiceInformationList() {
        return otherServiceInformationList;
    }


    /**
     * Sets the otherServiceInformationList value for this MoveAvailabilityResponse.
     * 
     * @param otherServiceInformationList
     */
    public void setOtherServiceInformationList(com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInformationList) {
        this.otherServiceInformationList = otherServiceInformationList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MoveAvailabilityResponse)) return false;
        MoveAvailabilityResponse other = (MoveAvailabilityResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flyAheadOfferStatus==null && other.getFlyAheadOfferStatus()==null) || 
             (this.flyAheadOfferStatus!=null &&
              this.flyAheadOfferStatus.equals(other.getFlyAheadOfferStatus()))) &&
            ((this.schedule==null && other.getSchedule()==null) || 
             (this.schedule!=null &&
              java.util.Arrays.equals(this.schedule, other.getSchedule()))) &&
            ((this.scheduledFromJourney==null && other.getScheduledFromJourney()==null) || 
             (this.scheduledFromJourney!=null &&
              this.scheduledFromJourney.equals(other.getScheduledFromJourney()))) &&
            ((this.otherServiceInformationList==null && other.getOtherServiceInformationList()==null) || 
             (this.otherServiceInformationList!=null &&
              java.util.Arrays.equals(this.otherServiceInformationList, other.getOtherServiceInformationList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlyAheadOfferStatus() != null) {
            _hashCode += getFlyAheadOfferStatus().hashCode();
        }
        if (getSchedule() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSchedule());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSchedule(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getScheduledFromJourney() != null) {
            _hashCode += getScheduledFromJourney().hashCode();
        }
        if (getOtherServiceInformationList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOtherServiceInformationList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOtherServiceInformationList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MoveAvailabilityResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveAvailabilityResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flyAheadOfferStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlyAheadOfferStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FlyAheadOfferStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("schedule");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Schedule"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneyDateMarket"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneyDateMarket"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduledFromJourney");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ScheduledFromJourney"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherServiceInformationList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OtherServiceInformationList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OtherServiceInformation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OtherServiceInformation"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
