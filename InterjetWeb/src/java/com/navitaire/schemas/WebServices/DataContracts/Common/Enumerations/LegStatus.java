/**
 * LegStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class LegStatus implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected LegStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Normal = "Normal";
    public static final java.lang.String _Closed = "Closed";
    public static final java.lang.String _Canceled = "Canceled";
    public static final java.lang.String _Suspended = "Suspended";
    public static final java.lang.String _ClosedPending = "ClosedPending";
    public static final java.lang.String _BlockAllActivities = "BlockAllActivities";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final LegStatus Normal = new LegStatus(_Normal);
    public static final LegStatus Closed = new LegStatus(_Closed);
    public static final LegStatus Canceled = new LegStatus(_Canceled);
    public static final LegStatus Suspended = new LegStatus(_Suspended);
    public static final LegStatus ClosedPending = new LegStatus(_ClosedPending);
    public static final LegStatus BlockAllActivities = new LegStatus(_BlockAllActivities);
    public static final LegStatus Unmapped = new LegStatus(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static LegStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        LegStatus enumeration = (LegStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static LegStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LegStatus.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "LegStatus"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
