/**
 * AvailabilityRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class AvailabilityRequest  implements java.io.Serializable {
    private java.lang.String departureStation;

    private java.lang.String arrivalStation;

    private java.util.Calendar beginDate;

    private java.util.Calendar endDate;

    private java.lang.String carrierCode;

    private java.lang.String flightNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlightType flightType;

    private java.lang.Short paxCount;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DOW dow;

    private java.lang.String currencyCode;

    private java.lang.String displayCurrencyCode;

    private java.lang.String discountCode;

    private java.lang.String promotionCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityType availabilityType;

    private java.lang.String sourceOrganization;

    private java.lang.Short maximumConnectingFlights;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityFilter availabilityFilter;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareClassControl fareClassControl;

    private java.math.BigDecimal minimumFarePrice;

    private java.math.BigDecimal maximumFarePrice;

    private java.lang.String productClassCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SSRCollectionsMode SSRCollectionsMode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound inboundOutbound;

    private java.lang.Integer nightsStay;

    private java.lang.Boolean includeAllotments;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Time beginTime;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Time endTime;

    private java.lang.String[] departureStations;

    private java.lang.String[] arrivalStations;

    private java.lang.String[] fareTypes;

    private java.lang.String[] productClasses;

    private java.lang.String[] fareClasses;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType[] paxPriceTypes;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey[] journeySortKeys;

    private int[] travelClassCodes;

    private java.lang.Boolean includeTaxesAndFees;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareRuleFilter fareRuleFilter;

    private org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter;

    private java.lang.String paxResidentCountry;

    private java.lang.String[] travelClassCodeList;

    private java.lang.String systemCode;

    public AvailabilityRequest() {
    }

    public AvailabilityRequest(
           java.lang.String departureStation,
           java.lang.String arrivalStation,
           java.util.Calendar beginDate,
           java.util.Calendar endDate,
           java.lang.String carrierCode,
           java.lang.String flightNumber,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlightType flightType,
           java.lang.Short paxCount,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DOW dow,
           java.lang.String currencyCode,
           java.lang.String displayCurrencyCode,
           java.lang.String discountCode,
           java.lang.String promotionCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityType availabilityType,
           java.lang.String sourceOrganization,
           java.lang.Short maximumConnectingFlights,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityFilter availabilityFilter,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareClassControl fareClassControl,
           java.math.BigDecimal minimumFarePrice,
           java.math.BigDecimal maximumFarePrice,
           java.lang.String productClassCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SSRCollectionsMode SSRCollectionsMode,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound inboundOutbound,
           java.lang.Integer nightsStay,
           java.lang.Boolean includeAllotments,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Time beginTime,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Time endTime,
           java.lang.String[] departureStations,
           java.lang.String[] arrivalStations,
           java.lang.String[] fareTypes,
           java.lang.String[] productClasses,
           java.lang.String[] fareClasses,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType[] paxPriceTypes,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey[] journeySortKeys,
           int[] travelClassCodes,
           java.lang.Boolean includeTaxesAndFees,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareRuleFilter fareRuleFilter,
           org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter,
           java.lang.String paxResidentCountry,
           java.lang.String[] travelClassCodeList,
           java.lang.String systemCode) {
           this.departureStation = departureStation;
           this.arrivalStation = arrivalStation;
           this.beginDate = beginDate;
           this.endDate = endDate;
           this.carrierCode = carrierCode;
           this.flightNumber = flightNumber;
           this.flightType = flightType;
           this.paxCount = paxCount;
           this.dow = dow;
           this.currencyCode = currencyCode;
           this.displayCurrencyCode = displayCurrencyCode;
           this.discountCode = discountCode;
           this.promotionCode = promotionCode;
           this.availabilityType = availabilityType;
           this.sourceOrganization = sourceOrganization;
           this.maximumConnectingFlights = maximumConnectingFlights;
           this.availabilityFilter = availabilityFilter;
           this.fareClassControl = fareClassControl;
           this.minimumFarePrice = minimumFarePrice;
           this.maximumFarePrice = maximumFarePrice;
           this.productClassCode = productClassCode;
           this.SSRCollectionsMode = SSRCollectionsMode;
           this.inboundOutbound = inboundOutbound;
           this.nightsStay = nightsStay;
           this.includeAllotments = includeAllotments;
           this.beginTime = beginTime;
           this.endTime = endTime;
           this.departureStations = departureStations;
           this.arrivalStations = arrivalStations;
           this.fareTypes = fareTypes;
           this.productClasses = productClasses;
           this.fareClasses = fareClasses;
           this.paxPriceTypes = paxPriceTypes;
           this.journeySortKeys = journeySortKeys;
           this.travelClassCodes = travelClassCodes;
           this.includeTaxesAndFees = includeTaxesAndFees;
           this.fareRuleFilter = fareRuleFilter;
           this.loyaltyFilter = loyaltyFilter;
           this.paxResidentCountry = paxResidentCountry;
           this.travelClassCodeList = travelClassCodeList;
           this.systemCode = systemCode;
    }


    /**
     * Gets the departureStation value for this AvailabilityRequest.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this AvailabilityRequest.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the arrivalStation value for this AvailabilityRequest.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this AvailabilityRequest.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the beginDate value for this AvailabilityRequest.
     * 
     * @return beginDate
     */
    public java.util.Calendar getBeginDate() {
        return beginDate;
    }


    /**
     * Sets the beginDate value for this AvailabilityRequest.
     * 
     * @param beginDate
     */
    public void setBeginDate(java.util.Calendar beginDate) {
        this.beginDate = beginDate;
    }


    /**
     * Gets the endDate value for this AvailabilityRequest.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this AvailabilityRequest.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the carrierCode value for this AvailabilityRequest.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this AvailabilityRequest.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the flightNumber value for this AvailabilityRequest.
     * 
     * @return flightNumber
     */
    public java.lang.String getFlightNumber() {
        return flightNumber;
    }


    /**
     * Sets the flightNumber value for this AvailabilityRequest.
     * 
     * @param flightNumber
     */
    public void setFlightNumber(java.lang.String flightNumber) {
        this.flightNumber = flightNumber;
    }


    /**
     * Gets the flightType value for this AvailabilityRequest.
     * 
     * @return flightType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlightType getFlightType() {
        return flightType;
    }


    /**
     * Sets the flightType value for this AvailabilityRequest.
     * 
     * @param flightType
     */
    public void setFlightType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlightType flightType) {
        this.flightType = flightType;
    }


    /**
     * Gets the paxCount value for this AvailabilityRequest.
     * 
     * @return paxCount
     */
    public java.lang.Short getPaxCount() {
        return paxCount;
    }


    /**
     * Sets the paxCount value for this AvailabilityRequest.
     * 
     * @param paxCount
     */
    public void setPaxCount(java.lang.Short paxCount) {
        this.paxCount = paxCount;
    }


    /**
     * Gets the dow value for this AvailabilityRequest.
     * 
     * @return dow
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DOW getDow() {
        return dow;
    }


    /**
     * Sets the dow value for this AvailabilityRequest.
     * 
     * @param dow
     */
    public void setDow(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DOW dow) {
        this.dow = dow;
    }


    /**
     * Gets the currencyCode value for this AvailabilityRequest.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this AvailabilityRequest.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the displayCurrencyCode value for this AvailabilityRequest.
     * 
     * @return displayCurrencyCode
     */
    public java.lang.String getDisplayCurrencyCode() {
        return displayCurrencyCode;
    }


    /**
     * Sets the displayCurrencyCode value for this AvailabilityRequest.
     * 
     * @param displayCurrencyCode
     */
    public void setDisplayCurrencyCode(java.lang.String displayCurrencyCode) {
        this.displayCurrencyCode = displayCurrencyCode;
    }


    /**
     * Gets the discountCode value for this AvailabilityRequest.
     * 
     * @return discountCode
     */
    public java.lang.String getDiscountCode() {
        return discountCode;
    }


    /**
     * Sets the discountCode value for this AvailabilityRequest.
     * 
     * @param discountCode
     */
    public void setDiscountCode(java.lang.String discountCode) {
        this.discountCode = discountCode;
    }


    /**
     * Gets the promotionCode value for this AvailabilityRequest.
     * 
     * @return promotionCode
     */
    public java.lang.String getPromotionCode() {
        return promotionCode;
    }


    /**
     * Sets the promotionCode value for this AvailabilityRequest.
     * 
     * @param promotionCode
     */
    public void setPromotionCode(java.lang.String promotionCode) {
        this.promotionCode = promotionCode;
    }


    /**
     * Gets the availabilityType value for this AvailabilityRequest.
     * 
     * @return availabilityType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityType getAvailabilityType() {
        return availabilityType;
    }


    /**
     * Sets the availabilityType value for this AvailabilityRequest.
     * 
     * @param availabilityType
     */
    public void setAvailabilityType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityType availabilityType) {
        this.availabilityType = availabilityType;
    }


    /**
     * Gets the sourceOrganization value for this AvailabilityRequest.
     * 
     * @return sourceOrganization
     */
    public java.lang.String getSourceOrganization() {
        return sourceOrganization;
    }


    /**
     * Sets the sourceOrganization value for this AvailabilityRequest.
     * 
     * @param sourceOrganization
     */
    public void setSourceOrganization(java.lang.String sourceOrganization) {
        this.sourceOrganization = sourceOrganization;
    }


    /**
     * Gets the maximumConnectingFlights value for this AvailabilityRequest.
     * 
     * @return maximumConnectingFlights
     */
    public java.lang.Short getMaximumConnectingFlights() {
        return maximumConnectingFlights;
    }


    /**
     * Sets the maximumConnectingFlights value for this AvailabilityRequest.
     * 
     * @param maximumConnectingFlights
     */
    public void setMaximumConnectingFlights(java.lang.Short maximumConnectingFlights) {
        this.maximumConnectingFlights = maximumConnectingFlights;
    }


    /**
     * Gets the availabilityFilter value for this AvailabilityRequest.
     * 
     * @return availabilityFilter
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityFilter getAvailabilityFilter() {
        return availabilityFilter;
    }


    /**
     * Sets the availabilityFilter value for this AvailabilityRequest.
     * 
     * @param availabilityFilter
     */
    public void setAvailabilityFilter(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityFilter availabilityFilter) {
        this.availabilityFilter = availabilityFilter;
    }


    /**
     * Gets the fareClassControl value for this AvailabilityRequest.
     * 
     * @return fareClassControl
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareClassControl getFareClassControl() {
        return fareClassControl;
    }


    /**
     * Sets the fareClassControl value for this AvailabilityRequest.
     * 
     * @param fareClassControl
     */
    public void setFareClassControl(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareClassControl fareClassControl) {
        this.fareClassControl = fareClassControl;
    }


    /**
     * Gets the minimumFarePrice value for this AvailabilityRequest.
     * 
     * @return minimumFarePrice
     */
    public java.math.BigDecimal getMinimumFarePrice() {
        return minimumFarePrice;
    }


    /**
     * Sets the minimumFarePrice value for this AvailabilityRequest.
     * 
     * @param minimumFarePrice
     */
    public void setMinimumFarePrice(java.math.BigDecimal minimumFarePrice) {
        this.minimumFarePrice = minimumFarePrice;
    }


    /**
     * Gets the maximumFarePrice value for this AvailabilityRequest.
     * 
     * @return maximumFarePrice
     */
    public java.math.BigDecimal getMaximumFarePrice() {
        return maximumFarePrice;
    }


    /**
     * Sets the maximumFarePrice value for this AvailabilityRequest.
     * 
     * @param maximumFarePrice
     */
    public void setMaximumFarePrice(java.math.BigDecimal maximumFarePrice) {
        this.maximumFarePrice = maximumFarePrice;
    }


    /**
     * Gets the productClassCode value for this AvailabilityRequest.
     * 
     * @return productClassCode
     */
    public java.lang.String getProductClassCode() {
        return productClassCode;
    }


    /**
     * Sets the productClassCode value for this AvailabilityRequest.
     * 
     * @param productClassCode
     */
    public void setProductClassCode(java.lang.String productClassCode) {
        this.productClassCode = productClassCode;
    }


    /**
     * Gets the SSRCollectionsMode value for this AvailabilityRequest.
     * 
     * @return SSRCollectionsMode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SSRCollectionsMode getSSRCollectionsMode() {
        return SSRCollectionsMode;
    }


    /**
     * Sets the SSRCollectionsMode value for this AvailabilityRequest.
     * 
     * @param SSRCollectionsMode
     */
    public void setSSRCollectionsMode(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SSRCollectionsMode SSRCollectionsMode) {
        this.SSRCollectionsMode = SSRCollectionsMode;
    }


    /**
     * Gets the inboundOutbound value for this AvailabilityRequest.
     * 
     * @return inboundOutbound
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound getInboundOutbound() {
        return inboundOutbound;
    }


    /**
     * Sets the inboundOutbound value for this AvailabilityRequest.
     * 
     * @param inboundOutbound
     */
    public void setInboundOutbound(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound inboundOutbound) {
        this.inboundOutbound = inboundOutbound;
    }


    /**
     * Gets the nightsStay value for this AvailabilityRequest.
     * 
     * @return nightsStay
     */
    public java.lang.Integer getNightsStay() {
        return nightsStay;
    }


    /**
     * Sets the nightsStay value for this AvailabilityRequest.
     * 
     * @param nightsStay
     */
    public void setNightsStay(java.lang.Integer nightsStay) {
        this.nightsStay = nightsStay;
    }


    /**
     * Gets the includeAllotments value for this AvailabilityRequest.
     * 
     * @return includeAllotments
     */
    public java.lang.Boolean getIncludeAllotments() {
        return includeAllotments;
    }


    /**
     * Sets the includeAllotments value for this AvailabilityRequest.
     * 
     * @param includeAllotments
     */
    public void setIncludeAllotments(java.lang.Boolean includeAllotments) {
        this.includeAllotments = includeAllotments;
    }


    /**
     * Gets the beginTime value for this AvailabilityRequest.
     * 
     * @return beginTime
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Time getBeginTime() {
        return beginTime;
    }


    /**
     * Sets the beginTime value for this AvailabilityRequest.
     * 
     * @param beginTime
     */
    public void setBeginTime(com.navitaire.schemas.WebServices.DataContracts.Booking.Time beginTime) {
        this.beginTime = beginTime;
    }


    /**
     * Gets the endTime value for this AvailabilityRequest.
     * 
     * @return endTime
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Time getEndTime() {
        return endTime;
    }


    /**
     * Sets the endTime value for this AvailabilityRequest.
     * 
     * @param endTime
     */
    public void setEndTime(com.navitaire.schemas.WebServices.DataContracts.Booking.Time endTime) {
        this.endTime = endTime;
    }


    /**
     * Gets the departureStations value for this AvailabilityRequest.
     * 
     * @return departureStations
     */
    public java.lang.String[] getDepartureStations() {
        return departureStations;
    }


    /**
     * Sets the departureStations value for this AvailabilityRequest.
     * 
     * @param departureStations
     */
    public void setDepartureStations(java.lang.String[] departureStations) {
        this.departureStations = departureStations;
    }


    /**
     * Gets the arrivalStations value for this AvailabilityRequest.
     * 
     * @return arrivalStations
     */
    public java.lang.String[] getArrivalStations() {
        return arrivalStations;
    }


    /**
     * Sets the arrivalStations value for this AvailabilityRequest.
     * 
     * @param arrivalStations
     */
    public void setArrivalStations(java.lang.String[] arrivalStations) {
        this.arrivalStations = arrivalStations;
    }


    /**
     * Gets the fareTypes value for this AvailabilityRequest.
     * 
     * @return fareTypes
     */
    public java.lang.String[] getFareTypes() {
        return fareTypes;
    }


    /**
     * Sets the fareTypes value for this AvailabilityRequest.
     * 
     * @param fareTypes
     */
    public void setFareTypes(java.lang.String[] fareTypes) {
        this.fareTypes = fareTypes;
    }


    /**
     * Gets the productClasses value for this AvailabilityRequest.
     * 
     * @return productClasses
     */
    public java.lang.String[] getProductClasses() {
        return productClasses;
    }


    /**
     * Sets the productClasses value for this AvailabilityRequest.
     * 
     * @param productClasses
     */
    public void setProductClasses(java.lang.String[] productClasses) {
        this.productClasses = productClasses;
    }


    /**
     * Gets the fareClasses value for this AvailabilityRequest.
     * 
     * @return fareClasses
     */
    public java.lang.String[] getFareClasses() {
        return fareClasses;
    }


    /**
     * Sets the fareClasses value for this AvailabilityRequest.
     * 
     * @param fareClasses
     */
    public void setFareClasses(java.lang.String[] fareClasses) {
        this.fareClasses = fareClasses;
    }


    /**
     * Gets the paxPriceTypes value for this AvailabilityRequest.
     * 
     * @return paxPriceTypes
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType[] getPaxPriceTypes() {
        return paxPriceTypes;
    }


    /**
     * Sets the paxPriceTypes value for this AvailabilityRequest.
     * 
     * @param paxPriceTypes
     */
    public void setPaxPriceTypes(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType[] paxPriceTypes) {
        this.paxPriceTypes = paxPriceTypes;
    }


    /**
     * Gets the journeySortKeys value for this AvailabilityRequest.
     * 
     * @return journeySortKeys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey[] getJourneySortKeys() {
        return journeySortKeys;
    }


    /**
     * Sets the journeySortKeys value for this AvailabilityRequest.
     * 
     * @param journeySortKeys
     */
    public void setJourneySortKeys(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey[] journeySortKeys) {
        this.journeySortKeys = journeySortKeys;
    }


    /**
     * Gets the travelClassCodes value for this AvailabilityRequest.
     * 
     * @return travelClassCodes
     */
    public int[] getTravelClassCodes() {
        return travelClassCodes;
    }


    /**
     * Sets the travelClassCodes value for this AvailabilityRequest.
     * 
     * @param travelClassCodes
     */
    public void setTravelClassCodes(int[] travelClassCodes) {
        this.travelClassCodes = travelClassCodes;
    }


    /**
     * Gets the includeTaxesAndFees value for this AvailabilityRequest.
     * 
     * @return includeTaxesAndFees
     */
    public java.lang.Boolean getIncludeTaxesAndFees() {
        return includeTaxesAndFees;
    }


    /**
     * Sets the includeTaxesAndFees value for this AvailabilityRequest.
     * 
     * @param includeTaxesAndFees
     */
    public void setIncludeTaxesAndFees(java.lang.Boolean includeTaxesAndFees) {
        this.includeTaxesAndFees = includeTaxesAndFees;
    }


    /**
     * Gets the fareRuleFilter value for this AvailabilityRequest.
     * 
     * @return fareRuleFilter
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareRuleFilter getFareRuleFilter() {
        return fareRuleFilter;
    }


    /**
     * Sets the fareRuleFilter value for this AvailabilityRequest.
     * 
     * @param fareRuleFilter
     */
    public void setFareRuleFilter(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareRuleFilter fareRuleFilter) {
        this.fareRuleFilter = fareRuleFilter;
    }


    /**
     * Gets the loyaltyFilter value for this AvailabilityRequest.
     * 
     * @return loyaltyFilter
     */
    public org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter getLoyaltyFilter() {
        return loyaltyFilter;
    }


    /**
     * Sets the loyaltyFilter value for this AvailabilityRequest.
     * 
     * @param loyaltyFilter
     */
    public void setLoyaltyFilter(org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
        this.loyaltyFilter = loyaltyFilter;
    }


    /**
     * Gets the paxResidentCountry value for this AvailabilityRequest.
     * 
     * @return paxResidentCountry
     */
    public java.lang.String getPaxResidentCountry() {
        return paxResidentCountry;
    }


    /**
     * Sets the paxResidentCountry value for this AvailabilityRequest.
     * 
     * @param paxResidentCountry
     */
    public void setPaxResidentCountry(java.lang.String paxResidentCountry) {
        this.paxResidentCountry = paxResidentCountry;
    }


    /**
     * Gets the travelClassCodeList value for this AvailabilityRequest.
     * 
     * @return travelClassCodeList
     */
    public java.lang.String[] getTravelClassCodeList() {
        return travelClassCodeList;
    }


    /**
     * Sets the travelClassCodeList value for this AvailabilityRequest.
     * 
     * @param travelClassCodeList
     */
    public void setTravelClassCodeList(java.lang.String[] travelClassCodeList) {
        this.travelClassCodeList = travelClassCodeList;
    }


    /**
     * Gets the systemCode value for this AvailabilityRequest.
     * 
     * @return systemCode
     */
    public java.lang.String getSystemCode() {
        return systemCode;
    }


    /**
     * Sets the systemCode value for this AvailabilityRequest.
     * 
     * @param systemCode
     */
    public void setSystemCode(java.lang.String systemCode) {
        this.systemCode = systemCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AvailabilityRequest)) return false;
        AvailabilityRequest other = (AvailabilityRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.beginDate==null && other.getBeginDate()==null) || 
             (this.beginDate!=null &&
              this.beginDate.equals(other.getBeginDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.flightNumber==null && other.getFlightNumber()==null) || 
             (this.flightNumber!=null &&
              this.flightNumber.equals(other.getFlightNumber()))) &&
            ((this.flightType==null && other.getFlightType()==null) || 
             (this.flightType!=null &&
              this.flightType.equals(other.getFlightType()))) &&
            ((this.paxCount==null && other.getPaxCount()==null) || 
             (this.paxCount!=null &&
              this.paxCount.equals(other.getPaxCount()))) &&
            ((this.dow==null && other.getDow()==null) || 
             (this.dow!=null &&
              this.dow.equals(other.getDow()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.displayCurrencyCode==null && other.getDisplayCurrencyCode()==null) || 
             (this.displayCurrencyCode!=null &&
              this.displayCurrencyCode.equals(other.getDisplayCurrencyCode()))) &&
            ((this.discountCode==null && other.getDiscountCode()==null) || 
             (this.discountCode!=null &&
              this.discountCode.equals(other.getDiscountCode()))) &&
            ((this.promotionCode==null && other.getPromotionCode()==null) || 
             (this.promotionCode!=null &&
              this.promotionCode.equals(other.getPromotionCode()))) &&
            ((this.availabilityType==null && other.getAvailabilityType()==null) || 
             (this.availabilityType!=null &&
              this.availabilityType.equals(other.getAvailabilityType()))) &&
            ((this.sourceOrganization==null && other.getSourceOrganization()==null) || 
             (this.sourceOrganization!=null &&
              this.sourceOrganization.equals(other.getSourceOrganization()))) &&
            ((this.maximumConnectingFlights==null && other.getMaximumConnectingFlights()==null) || 
             (this.maximumConnectingFlights!=null &&
              this.maximumConnectingFlights.equals(other.getMaximumConnectingFlights()))) &&
            ((this.availabilityFilter==null && other.getAvailabilityFilter()==null) || 
             (this.availabilityFilter!=null &&
              this.availabilityFilter.equals(other.getAvailabilityFilter()))) &&
            ((this.fareClassControl==null && other.getFareClassControl()==null) || 
             (this.fareClassControl!=null &&
              this.fareClassControl.equals(other.getFareClassControl()))) &&
            ((this.minimumFarePrice==null && other.getMinimumFarePrice()==null) || 
             (this.minimumFarePrice!=null &&
              this.minimumFarePrice.equals(other.getMinimumFarePrice()))) &&
            ((this.maximumFarePrice==null && other.getMaximumFarePrice()==null) || 
             (this.maximumFarePrice!=null &&
              this.maximumFarePrice.equals(other.getMaximumFarePrice()))) &&
            ((this.productClassCode==null && other.getProductClassCode()==null) || 
             (this.productClassCode!=null &&
              this.productClassCode.equals(other.getProductClassCode()))) &&
            ((this.SSRCollectionsMode==null && other.getSSRCollectionsMode()==null) || 
             (this.SSRCollectionsMode!=null &&
              this.SSRCollectionsMode.equals(other.getSSRCollectionsMode()))) &&
            ((this.inboundOutbound==null && other.getInboundOutbound()==null) || 
             (this.inboundOutbound!=null &&
              this.inboundOutbound.equals(other.getInboundOutbound()))) &&
            ((this.nightsStay==null && other.getNightsStay()==null) || 
             (this.nightsStay!=null &&
              this.nightsStay.equals(other.getNightsStay()))) &&
            ((this.includeAllotments==null && other.getIncludeAllotments()==null) || 
             (this.includeAllotments!=null &&
              this.includeAllotments.equals(other.getIncludeAllotments()))) &&
            ((this.beginTime==null && other.getBeginTime()==null) || 
             (this.beginTime!=null &&
              this.beginTime.equals(other.getBeginTime()))) &&
            ((this.endTime==null && other.getEndTime()==null) || 
             (this.endTime!=null &&
              this.endTime.equals(other.getEndTime()))) &&
            ((this.departureStations==null && other.getDepartureStations()==null) || 
             (this.departureStations!=null &&
              java.util.Arrays.equals(this.departureStations, other.getDepartureStations()))) &&
            ((this.arrivalStations==null && other.getArrivalStations()==null) || 
             (this.arrivalStations!=null &&
              java.util.Arrays.equals(this.arrivalStations, other.getArrivalStations()))) &&
            ((this.fareTypes==null && other.getFareTypes()==null) || 
             (this.fareTypes!=null &&
              java.util.Arrays.equals(this.fareTypes, other.getFareTypes()))) &&
            ((this.productClasses==null && other.getProductClasses()==null) || 
             (this.productClasses!=null &&
              java.util.Arrays.equals(this.productClasses, other.getProductClasses()))) &&
            ((this.fareClasses==null && other.getFareClasses()==null) || 
             (this.fareClasses!=null &&
              java.util.Arrays.equals(this.fareClasses, other.getFareClasses()))) &&
            ((this.paxPriceTypes==null && other.getPaxPriceTypes()==null) || 
             (this.paxPriceTypes!=null &&
              java.util.Arrays.equals(this.paxPriceTypes, other.getPaxPriceTypes()))) &&
            ((this.journeySortKeys==null && other.getJourneySortKeys()==null) || 
             (this.journeySortKeys!=null &&
              java.util.Arrays.equals(this.journeySortKeys, other.getJourneySortKeys()))) &&
            ((this.travelClassCodes==null && other.getTravelClassCodes()==null) || 
             (this.travelClassCodes!=null &&
              java.util.Arrays.equals(this.travelClassCodes, other.getTravelClassCodes()))) &&
            ((this.includeTaxesAndFees==null && other.getIncludeTaxesAndFees()==null) || 
             (this.includeTaxesAndFees!=null &&
              this.includeTaxesAndFees.equals(other.getIncludeTaxesAndFees()))) &&
            ((this.fareRuleFilter==null && other.getFareRuleFilter()==null) || 
             (this.fareRuleFilter!=null &&
              this.fareRuleFilter.equals(other.getFareRuleFilter()))) &&
            ((this.loyaltyFilter==null && other.getLoyaltyFilter()==null) || 
             (this.loyaltyFilter!=null &&
              this.loyaltyFilter.equals(other.getLoyaltyFilter()))) &&
            ((this.paxResidentCountry==null && other.getPaxResidentCountry()==null) || 
             (this.paxResidentCountry!=null &&
              this.paxResidentCountry.equals(other.getPaxResidentCountry()))) &&
            ((this.travelClassCodeList==null && other.getTravelClassCodeList()==null) || 
             (this.travelClassCodeList!=null &&
              java.util.Arrays.equals(this.travelClassCodeList, other.getTravelClassCodeList()))) &&
            ((this.systemCode==null && other.getSystemCode()==null) || 
             (this.systemCode!=null &&
              this.systemCode.equals(other.getSystemCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getBeginDate() != null) {
            _hashCode += getBeginDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getFlightNumber() != null) {
            _hashCode += getFlightNumber().hashCode();
        }
        if (getFlightType() != null) {
            _hashCode += getFlightType().hashCode();
        }
        if (getPaxCount() != null) {
            _hashCode += getPaxCount().hashCode();
        }
        if (getDow() != null) {
            _hashCode += getDow().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getDisplayCurrencyCode() != null) {
            _hashCode += getDisplayCurrencyCode().hashCode();
        }
        if (getDiscountCode() != null) {
            _hashCode += getDiscountCode().hashCode();
        }
        if (getPromotionCode() != null) {
            _hashCode += getPromotionCode().hashCode();
        }
        if (getAvailabilityType() != null) {
            _hashCode += getAvailabilityType().hashCode();
        }
        if (getSourceOrganization() != null) {
            _hashCode += getSourceOrganization().hashCode();
        }
        if (getMaximumConnectingFlights() != null) {
            _hashCode += getMaximumConnectingFlights().hashCode();
        }
        if (getAvailabilityFilter() != null) {
            _hashCode += getAvailabilityFilter().hashCode();
        }
        if (getFareClassControl() != null) {
            _hashCode += getFareClassControl().hashCode();
        }
        if (getMinimumFarePrice() != null) {
            _hashCode += getMinimumFarePrice().hashCode();
        }
        if (getMaximumFarePrice() != null) {
            _hashCode += getMaximumFarePrice().hashCode();
        }
        if (getProductClassCode() != null) {
            _hashCode += getProductClassCode().hashCode();
        }
        if (getSSRCollectionsMode() != null) {
            _hashCode += getSSRCollectionsMode().hashCode();
        }
        if (getInboundOutbound() != null) {
            _hashCode += getInboundOutbound().hashCode();
        }
        if (getNightsStay() != null) {
            _hashCode += getNightsStay().hashCode();
        }
        if (getIncludeAllotments() != null) {
            _hashCode += getIncludeAllotments().hashCode();
        }
        if (getBeginTime() != null) {
            _hashCode += getBeginTime().hashCode();
        }
        if (getEndTime() != null) {
            _hashCode += getEndTime().hashCode();
        }
        if (getDepartureStations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDepartureStations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDepartureStations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getArrivalStations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getArrivalStations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getArrivalStations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFareTypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFareTypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFareTypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProductClasses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProductClasses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProductClasses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFareClasses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFareClasses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFareClasses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxPriceTypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxPriceTypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxPriceTypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getJourneySortKeys() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJourneySortKeys());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJourneySortKeys(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTravelClassCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTravelClassCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTravelClassCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIncludeTaxesAndFees() != null) {
            _hashCode += getIncludeTaxesAndFees().hashCode();
        }
        if (getFareRuleFilter() != null) {
            _hashCode += getFareRuleFilter().hashCode();
        }
        if (getLoyaltyFilter() != null) {
            _hashCode += getLoyaltyFilter().hashCode();
        }
        if (getPaxResidentCountry() != null) {
            _hashCode += getPaxResidentCountry().hashCode();
        }
        if (getTravelClassCodeList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTravelClassCodeList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTravelClassCodeList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSystemCode() != null) {
            _hashCode += getSystemCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AvailabilityRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BeginDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FlightType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Dow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DOW"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DisplayCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DiscountCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promotionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PromotionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availabilityType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AvailabilityType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceOrganization");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceOrganization"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maximumConnectingFlights");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MaximumConnectingFlights"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availabilityFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AvailabilityFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareClassControl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareClassControl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FareClassControl"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("minimumFarePrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MinimumFarePrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maximumFarePrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MaximumFarePrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productClassCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProductClassCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRCollectionsMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRCollectionsMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SSRCollectionsMode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inboundOutbound");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InboundOutbound"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "InboundOutbound"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nightsStay");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NightsStay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeAllotments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeAllotments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BeginTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Time"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productClasses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProductClasses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareClasses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareClasses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxPriceTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeySortKeys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneySortKeys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "JourneySortKey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "JourneySortKey"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("travelClassCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelClassCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/", "char"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "char"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeTaxesAndFees");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludeTaxesAndFees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareRuleFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareRuleFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FareRuleFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loyaltyFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LoyaltyFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LoyaltyFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxResidentCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxResidentCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("travelClassCodeList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelClassCodeList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SystemCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
