/**
 * OrderItemSkuDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class OrderItemSkuDetail  implements java.io.Serializable {
    private java.lang.String skuStyleCode;

    private java.lang.String skuDetailCode;

    private java.lang.String description;

    public OrderItemSkuDetail() {
    }

    public OrderItemSkuDetail(
           java.lang.String skuStyleCode,
           java.lang.String skuDetailCode,
           java.lang.String description) {
           this.skuStyleCode = skuStyleCode;
           this.skuDetailCode = skuDetailCode;
           this.description = description;
    }


    /**
     * Gets the skuStyleCode value for this OrderItemSkuDetail.
     * 
     * @return skuStyleCode
     */
    public java.lang.String getSkuStyleCode() {
        return skuStyleCode;
    }


    /**
     * Sets the skuStyleCode value for this OrderItemSkuDetail.
     * 
     * @param skuStyleCode
     */
    public void setSkuStyleCode(java.lang.String skuStyleCode) {
        this.skuStyleCode = skuStyleCode;
    }


    /**
     * Gets the skuDetailCode value for this OrderItemSkuDetail.
     * 
     * @return skuDetailCode
     */
    public java.lang.String getSkuDetailCode() {
        return skuDetailCode;
    }


    /**
     * Sets the skuDetailCode value for this OrderItemSkuDetail.
     * 
     * @param skuDetailCode
     */
    public void setSkuDetailCode(java.lang.String skuDetailCode) {
        this.skuDetailCode = skuDetailCode;
    }


    /**
     * Gets the description value for this OrderItemSkuDetail.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this OrderItemSkuDetail.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderItemSkuDetail)) return false;
        OrderItemSkuDetail other = (OrderItemSkuDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.skuStyleCode==null && other.getSkuStyleCode()==null) || 
             (this.skuStyleCode!=null &&
              this.skuStyleCode.equals(other.getSkuStyleCode()))) &&
            ((this.skuDetailCode==null && other.getSkuDetailCode()==null) || 
             (this.skuDetailCode!=null &&
              this.skuDetailCode.equals(other.getSkuDetailCode()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSkuStyleCode() != null) {
            _hashCode += getSkuStyleCode().hashCode();
        }
        if (getSkuDetailCode() != null) {
            _hashCode += getSkuDetailCode().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderItemSkuDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemSkuDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skuStyleCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SkuStyleCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skuDetailCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SkuDetailCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
