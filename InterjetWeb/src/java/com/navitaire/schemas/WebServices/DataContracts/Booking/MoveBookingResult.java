/**
 * MoveBookingResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class MoveBookingResult  implements java.io.Serializable {
    private java.lang.String classOfService;

    private java.lang.String classType;

    private java.lang.String moveStatusCode;

    private java.lang.Short paxCount;

    private java.lang.String recordLocator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Journey toJourney;

    public MoveBookingResult() {
    }

    public MoveBookingResult(
           java.lang.String classOfService,
           java.lang.String classType,
           java.lang.String moveStatusCode,
           java.lang.Short paxCount,
           java.lang.String recordLocator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Journey toJourney) {
           this.classOfService = classOfService;
           this.classType = classType;
           this.moveStatusCode = moveStatusCode;
           this.paxCount = paxCount;
           this.recordLocator = recordLocator;
           this.toJourney = toJourney;
    }


    /**
     * Gets the classOfService value for this MoveBookingResult.
     * 
     * @return classOfService
     */
    public java.lang.String getClassOfService() {
        return classOfService;
    }


    /**
     * Sets the classOfService value for this MoveBookingResult.
     * 
     * @param classOfService
     */
    public void setClassOfService(java.lang.String classOfService) {
        this.classOfService = classOfService;
    }


    /**
     * Gets the classType value for this MoveBookingResult.
     * 
     * @return classType
     */
    public java.lang.String getClassType() {
        return classType;
    }


    /**
     * Sets the classType value for this MoveBookingResult.
     * 
     * @param classType
     */
    public void setClassType(java.lang.String classType) {
        this.classType = classType;
    }


    /**
     * Gets the moveStatusCode value for this MoveBookingResult.
     * 
     * @return moveStatusCode
     */
    public java.lang.String getMoveStatusCode() {
        return moveStatusCode;
    }


    /**
     * Sets the moveStatusCode value for this MoveBookingResult.
     * 
     * @param moveStatusCode
     */
    public void setMoveStatusCode(java.lang.String moveStatusCode) {
        this.moveStatusCode = moveStatusCode;
    }


    /**
     * Gets the paxCount value for this MoveBookingResult.
     * 
     * @return paxCount
     */
    public java.lang.Short getPaxCount() {
        return paxCount;
    }


    /**
     * Sets the paxCount value for this MoveBookingResult.
     * 
     * @param paxCount
     */
    public void setPaxCount(java.lang.Short paxCount) {
        this.paxCount = paxCount;
    }


    /**
     * Gets the recordLocator value for this MoveBookingResult.
     * 
     * @return recordLocator
     */
    public java.lang.String getRecordLocator() {
        return recordLocator;
    }


    /**
     * Sets the recordLocator value for this MoveBookingResult.
     * 
     * @param recordLocator
     */
    public void setRecordLocator(java.lang.String recordLocator) {
        this.recordLocator = recordLocator;
    }


    /**
     * Gets the toJourney value for this MoveBookingResult.
     * 
     * @return toJourney
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Journey getToJourney() {
        return toJourney;
    }


    /**
     * Sets the toJourney value for this MoveBookingResult.
     * 
     * @param toJourney
     */
    public void setToJourney(com.navitaire.schemas.WebServices.DataContracts.Booking.Journey toJourney) {
        this.toJourney = toJourney;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MoveBookingResult)) return false;
        MoveBookingResult other = (MoveBookingResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.classOfService==null && other.getClassOfService()==null) || 
             (this.classOfService!=null &&
              this.classOfService.equals(other.getClassOfService()))) &&
            ((this.classType==null && other.getClassType()==null) || 
             (this.classType!=null &&
              this.classType.equals(other.getClassType()))) &&
            ((this.moveStatusCode==null && other.getMoveStatusCode()==null) || 
             (this.moveStatusCode!=null &&
              this.moveStatusCode.equals(other.getMoveStatusCode()))) &&
            ((this.paxCount==null && other.getPaxCount()==null) || 
             (this.paxCount!=null &&
              this.paxCount.equals(other.getPaxCount()))) &&
            ((this.recordLocator==null && other.getRecordLocator()==null) || 
             (this.recordLocator!=null &&
              this.recordLocator.equals(other.getRecordLocator()))) &&
            ((this.toJourney==null && other.getToJourney()==null) || 
             (this.toJourney!=null &&
              this.toJourney.equals(other.getToJourney())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClassOfService() != null) {
            _hashCode += getClassOfService().hashCode();
        }
        if (getClassType() != null) {
            _hashCode += getClassType().hashCode();
        }
        if (getMoveStatusCode() != null) {
            _hashCode += getMoveStatusCode().hashCode();
        }
        if (getPaxCount() != null) {
            _hashCode += getPaxCount().hashCode();
        }
        if (getRecordLocator() != null) {
            _hashCode += getRecordLocator().hashCode();
        }
        if (getToJourney() != null) {
            _hashCode += getToJourney().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MoveBookingResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveBookingResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moveStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toJourney");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ToJourney"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
