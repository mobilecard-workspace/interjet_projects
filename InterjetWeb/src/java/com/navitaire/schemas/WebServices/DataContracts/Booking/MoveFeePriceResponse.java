/**
 * MoveFeePriceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class MoveFeePriceResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceItem[] moveFees;

    public MoveFeePriceResponse() {
    }

    public MoveFeePriceResponse(
           com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceItem[] moveFees) {
           this.moveFees = moveFees;
    }


    /**
     * Gets the moveFees value for this MoveFeePriceResponse.
     * 
     * @return moveFees
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceItem[] getMoveFees() {
        return moveFees;
    }


    /**
     * Sets the moveFees value for this MoveFeePriceResponse.
     * 
     * @param moveFees
     */
    public void setMoveFees(com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceItem[] moveFees) {
        this.moveFees = moveFees;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MoveFeePriceResponse)) return false;
        MoveFeePriceResponse other = (MoveFeePriceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.moveFees==null && other.getMoveFees()==null) || 
             (this.moveFees!=null &&
              java.util.Arrays.equals(this.moveFees, other.getMoveFees())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMoveFees() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMoveFees());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMoveFees(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MoveFeePriceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moveFees");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceItem"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
