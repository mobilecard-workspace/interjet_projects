/**
 * UpdateBookingComponentDataRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class UpdateBookingComponentDataRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent[] bookingComponentList;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateType updateType;

    public UpdateBookingComponentDataRequestData() {
    }

    public UpdateBookingComponentDataRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent[] bookingComponentList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateType updateType) {
           this.bookingComponentList = bookingComponentList;
           this.updateType = updateType;
    }


    /**
     * Gets the bookingComponentList value for this UpdateBookingComponentDataRequestData.
     * 
     * @return bookingComponentList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent[] getBookingComponentList() {
        return bookingComponentList;
    }


    /**
     * Sets the bookingComponentList value for this UpdateBookingComponentDataRequestData.
     * 
     * @param bookingComponentList
     */
    public void setBookingComponentList(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent[] bookingComponentList) {
        this.bookingComponentList = bookingComponentList;
    }


    /**
     * Gets the updateType value for this UpdateBookingComponentDataRequestData.
     * 
     * @return updateType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateType getUpdateType() {
        return updateType;
    }


    /**
     * Sets the updateType value for this UpdateBookingComponentDataRequestData.
     * 
     * @param updateType
     */
    public void setUpdateType(com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateType updateType) {
        this.updateType = updateType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateBookingComponentDataRequestData)) return false;
        UpdateBookingComponentDataRequestData other = (UpdateBookingComponentDataRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingComponentList==null && other.getBookingComponentList()==null) || 
             (this.bookingComponentList!=null &&
              java.util.Arrays.equals(this.bookingComponentList, other.getBookingComponentList()))) &&
            ((this.updateType==null && other.getUpdateType()==null) || 
             (this.updateType!=null &&
              this.updateType.equals(other.getUpdateType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingComponentList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingComponentList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingComponentList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUpdateType() != null) {
            _hashCode += getUpdateType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateBookingComponentDataRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateBookingComponentDataRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingComponentList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponent"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponent"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
