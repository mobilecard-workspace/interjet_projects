/**
 * UpdateContactsRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class UpdateContactsRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] bookingContactList;

    public UpdateContactsRequestData() {
    }

    public UpdateContactsRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] bookingContactList) {
           this.bookingContactList = bookingContactList;
    }


    /**
     * Gets the bookingContactList value for this UpdateContactsRequestData.
     * 
     * @return bookingContactList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] getBookingContactList() {
        return bookingContactList;
    }


    /**
     * Sets the bookingContactList value for this UpdateContactsRequestData.
     * 
     * @param bookingContactList
     */
    public void setBookingContactList(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[] bookingContactList) {
        this.bookingContactList = bookingContactList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateContactsRequestData)) return false;
        UpdateContactsRequestData other = (UpdateContactsRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingContactList==null && other.getBookingContactList()==null) || 
             (this.bookingContactList!=null &&
              java.util.Arrays.equals(this.bookingContactList, other.getBookingContactList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingContactList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingContactList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingContactList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateContactsRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateContactsRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingContactList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContactList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContact"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContact"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
