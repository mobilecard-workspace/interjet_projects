/**
 * EquipmentListResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class EquipmentListResponse  extends com.navitaire.schemas.WebServices.DataContracts.Booking.ServiceMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentResponse[] equipmentResponses;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentPropertyTypeCodesLookup propertyTypeCodesLookup;

    public EquipmentListResponse() {
    }

    public EquipmentListResponse(
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInfoList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentResponse[] equipmentResponses,
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentPropertyTypeCodesLookup propertyTypeCodesLookup) {
        super(
            otherServiceInfoList);
        this.equipmentResponses = equipmentResponses;
        this.propertyTypeCodesLookup = propertyTypeCodesLookup;
    }


    /**
     * Gets the equipmentResponses value for this EquipmentListResponse.
     * 
     * @return equipmentResponses
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentResponse[] getEquipmentResponses() {
        return equipmentResponses;
    }


    /**
     * Sets the equipmentResponses value for this EquipmentListResponse.
     * 
     * @param equipmentResponses
     */
    public void setEquipmentResponses(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentResponse[] equipmentResponses) {
        this.equipmentResponses = equipmentResponses;
    }


    /**
     * Gets the propertyTypeCodesLookup value for this EquipmentListResponse.
     * 
     * @return propertyTypeCodesLookup
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentPropertyTypeCodesLookup getPropertyTypeCodesLookup() {
        return propertyTypeCodesLookup;
    }


    /**
     * Sets the propertyTypeCodesLookup value for this EquipmentListResponse.
     * 
     * @param propertyTypeCodesLookup
     */
    public void setPropertyTypeCodesLookup(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentPropertyTypeCodesLookup propertyTypeCodesLookup) {
        this.propertyTypeCodesLookup = propertyTypeCodesLookup;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EquipmentListResponse)) return false;
        EquipmentListResponse other = (EquipmentListResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.equipmentResponses==null && other.getEquipmentResponses()==null) || 
             (this.equipmentResponses!=null &&
              java.util.Arrays.equals(this.equipmentResponses, other.getEquipmentResponses()))) &&
            ((this.propertyTypeCodesLookup==null && other.getPropertyTypeCodesLookup()==null) || 
             (this.propertyTypeCodesLookup!=null &&
              this.propertyTypeCodesLookup.equals(other.getPropertyTypeCodesLookup())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEquipmentResponses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEquipmentResponses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEquipmentResponses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyTypeCodesLookup() != null) {
            _hashCode += getPropertyTypeCodesLookup().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EquipmentListResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentListResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentResponses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentResponses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentResponse"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyTypeCodesLookup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyTypeCodesLookup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentPropertyTypeCodesLookup"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
