/**
 * PassengerScoresResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PassengerScoresResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScore[] paxScoreList;

    private java.lang.String ruleSetName;

    public PassengerScoresResponse() {
    }

    public PassengerScoresResponse(
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScore[] paxScoreList,
           java.lang.String ruleSetName) {
           this.paxScoreList = paxScoreList;
           this.ruleSetName = ruleSetName;
    }


    /**
     * Gets the paxScoreList value for this PassengerScoresResponse.
     * 
     * @return paxScoreList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScore[] getPaxScoreList() {
        return paxScoreList;
    }


    /**
     * Sets the paxScoreList value for this PassengerScoresResponse.
     * 
     * @param paxScoreList
     */
    public void setPaxScoreList(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScore[] paxScoreList) {
        this.paxScoreList = paxScoreList;
    }


    /**
     * Gets the ruleSetName value for this PassengerScoresResponse.
     * 
     * @return ruleSetName
     */
    public java.lang.String getRuleSetName() {
        return ruleSetName;
    }


    /**
     * Sets the ruleSetName value for this PassengerScoresResponse.
     * 
     * @param ruleSetName
     */
    public void setRuleSetName(java.lang.String ruleSetName) {
        this.ruleSetName = ruleSetName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PassengerScoresResponse)) return false;
        PassengerScoresResponse other = (PassengerScoresResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paxScoreList==null && other.getPaxScoreList()==null) || 
             (this.paxScoreList!=null &&
              java.util.Arrays.equals(this.paxScoreList, other.getPaxScoreList()))) &&
            ((this.ruleSetName==null && other.getRuleSetName()==null) || 
             (this.ruleSetName!=null &&
              this.ruleSetName.equals(other.getRuleSetName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaxScoreList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxScoreList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxScoreList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRuleSetName() != null) {
            _hashCode += getRuleSetName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PassengerScoresResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScoresResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxScoreList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxScoreList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScore"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScore"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleSetName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RuleSetName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
