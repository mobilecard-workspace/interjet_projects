/**
 * PassengerScore.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PassengerScore  implements java.io.Serializable {
    private java.lang.Short journeyNumber;

    private java.lang.Short passengerNumber;

    private java.lang.Integer score;

    public PassengerScore() {
    }

    public PassengerScore(
           java.lang.Short journeyNumber,
           java.lang.Short passengerNumber,
           java.lang.Integer score) {
           this.journeyNumber = journeyNumber;
           this.passengerNumber = passengerNumber;
           this.score = score;
    }


    /**
     * Gets the journeyNumber value for this PassengerScore.
     * 
     * @return journeyNumber
     */
    public java.lang.Short getJourneyNumber() {
        return journeyNumber;
    }


    /**
     * Sets the journeyNumber value for this PassengerScore.
     * 
     * @param journeyNumber
     */
    public void setJourneyNumber(java.lang.Short journeyNumber) {
        this.journeyNumber = journeyNumber;
    }


    /**
     * Gets the passengerNumber value for this PassengerScore.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this PassengerScore.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the score value for this PassengerScore.
     * 
     * @return score
     */
    public java.lang.Integer getScore() {
        return score;
    }


    /**
     * Sets the score value for this PassengerScore.
     * 
     * @param score
     */
    public void setScore(java.lang.Integer score) {
        this.score = score;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PassengerScore)) return false;
        PassengerScore other = (PassengerScore) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.journeyNumber==null && other.getJourneyNumber()==null) || 
             (this.journeyNumber!=null &&
              this.journeyNumber.equals(other.getJourneyNumber()))) &&
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.score==null && other.getScore()==null) || 
             (this.score!=null &&
              this.score.equals(other.getScore())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getJourneyNumber() != null) {
            _hashCode += getJourneyNumber().hashCode();
        }
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getScore() != null) {
            _hashCode += getScore().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PassengerScore.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScore"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("score");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Score"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
