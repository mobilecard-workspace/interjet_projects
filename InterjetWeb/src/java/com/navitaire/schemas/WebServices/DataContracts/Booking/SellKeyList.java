/**
 * SellKeyList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SellKeyList  implements java.io.Serializable {
    private java.lang.String journeySellKey;

    private java.lang.String fareSellKey;

    private java.lang.String standbyPriorityCode;

    public SellKeyList() {
    }

    public SellKeyList(
           java.lang.String journeySellKey,
           java.lang.String fareSellKey,
           java.lang.String standbyPriorityCode) {
           this.journeySellKey = journeySellKey;
           this.fareSellKey = fareSellKey;
           this.standbyPriorityCode = standbyPriorityCode;
    }


    /**
     * Gets the journeySellKey value for this SellKeyList.
     * 
     * @return journeySellKey
     */
    public java.lang.String getJourneySellKey() {
        return journeySellKey;
    }


    /**
     * Sets the journeySellKey value for this SellKeyList.
     * 
     * @param journeySellKey
     */
    public void setJourneySellKey(java.lang.String journeySellKey) {
        this.journeySellKey = journeySellKey;
    }


    /**
     * Gets the fareSellKey value for this SellKeyList.
     * 
     * @return fareSellKey
     */
    public java.lang.String getFareSellKey() {
        return fareSellKey;
    }


    /**
     * Sets the fareSellKey value for this SellKeyList.
     * 
     * @param fareSellKey
     */
    public void setFareSellKey(java.lang.String fareSellKey) {
        this.fareSellKey = fareSellKey;
    }


    /**
     * Gets the standbyPriorityCode value for this SellKeyList.
     * 
     * @return standbyPriorityCode
     */
    public java.lang.String getStandbyPriorityCode() {
        return standbyPriorityCode;
    }


    /**
     * Sets the standbyPriorityCode value for this SellKeyList.
     * 
     * @param standbyPriorityCode
     */
    public void setStandbyPriorityCode(java.lang.String standbyPriorityCode) {
        this.standbyPriorityCode = standbyPriorityCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SellKeyList)) return false;
        SellKeyList other = (SellKeyList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.journeySellKey==null && other.getJourneySellKey()==null) || 
             (this.journeySellKey!=null &&
              this.journeySellKey.equals(other.getJourneySellKey()))) &&
            ((this.fareSellKey==null && other.getFareSellKey()==null) || 
             (this.fareSellKey!=null &&
              this.fareSellKey.equals(other.getFareSellKey()))) &&
            ((this.standbyPriorityCode==null && other.getStandbyPriorityCode()==null) || 
             (this.standbyPriorityCode!=null &&
              this.standbyPriorityCode.equals(other.getStandbyPriorityCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getJourneySellKey() != null) {
            _hashCode += getJourneySellKey().hashCode();
        }
        if (getFareSellKey() != null) {
            _hashCode += getFareSellKey().hashCode();
        }
        if (getStandbyPriorityCode() != null) {
            _hashCode += getStandbyPriorityCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SellKeyList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellKeyList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeySellKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneySellKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareSellKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareSellKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("standbyPriorityCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "StandbyPriorityCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
