/**
 * LegInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class LegInfo  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Short adjustedCapacity;

    private java.lang.String equipmentType;

    private java.lang.String equipmentTypeSuffix;

    private java.lang.String arrivalTerminal;

    private java.lang.Short arrvLTV;

    private java.lang.Short capacity;

    private java.lang.String codeShareIndicator;

    private java.lang.String departureTerminal;

    private java.lang.Short deptLTV;

    private java.lang.Boolean ETicket;

    private java.lang.Boolean flifoUpdated;

    private java.lang.Boolean IROP;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LegStatus status;

    private java.lang.Short lid;

    private java.lang.String onTime;

    private java.util.Calendar paxSTA;

    private java.util.Calendar paxSTD;

    private java.lang.String PRBCCode;

    private java.lang.String scheduleServiceType;

    private java.lang.Short sold;

    private java.lang.Short outMoveDays;

    private java.lang.Short backMoveDays;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.LegNest[] legNests;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.LegSSR[] legSSRs;

    private java.lang.String operatingFlightNumber;

    private java.lang.String operatedByText;

    private java.lang.String operatingCarrier;

    private java.lang.String operatingOpSuffix;

    private java.lang.Boolean subjectToGovtApproval;

    private java.lang.String marketingCode;

    private java.lang.Boolean changeOfDirection;

    private java.lang.Boolean marketingOverride;

    public LegInfo() {
    }

    public LegInfo(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Short adjustedCapacity,
           java.lang.String equipmentType,
           java.lang.String equipmentTypeSuffix,
           java.lang.String arrivalTerminal,
           java.lang.Short arrvLTV,
           java.lang.Short capacity,
           java.lang.String codeShareIndicator,
           java.lang.String departureTerminal,
           java.lang.Short deptLTV,
           java.lang.Boolean ETicket,
           java.lang.Boolean flifoUpdated,
           java.lang.Boolean IROP,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LegStatus status,
           java.lang.Short lid,
           java.lang.String onTime,
           java.util.Calendar paxSTA,
           java.util.Calendar paxSTD,
           java.lang.String PRBCCode,
           java.lang.String scheduleServiceType,
           java.lang.Short sold,
           java.lang.Short outMoveDays,
           java.lang.Short backMoveDays,
           com.navitaire.schemas.WebServices.DataContracts.Booking.LegNest[] legNests,
           com.navitaire.schemas.WebServices.DataContracts.Booking.LegSSR[] legSSRs,
           java.lang.String operatingFlightNumber,
           java.lang.String operatedByText,
           java.lang.String operatingCarrier,
           java.lang.String operatingOpSuffix,
           java.lang.Boolean subjectToGovtApproval,
           java.lang.String marketingCode,
           java.lang.Boolean changeOfDirection,
           java.lang.Boolean marketingOverride) {
        super(
            state);
        this.adjustedCapacity = adjustedCapacity;
        this.equipmentType = equipmentType;
        this.equipmentTypeSuffix = equipmentTypeSuffix;
        this.arrivalTerminal = arrivalTerminal;
        this.arrvLTV = arrvLTV;
        this.capacity = capacity;
        this.codeShareIndicator = codeShareIndicator;
        this.departureTerminal = departureTerminal;
        this.deptLTV = deptLTV;
        this.ETicket = ETicket;
        this.flifoUpdated = flifoUpdated;
        this.IROP = IROP;
        this.status = status;
        this.lid = lid;
        this.onTime = onTime;
        this.paxSTA = paxSTA;
        this.paxSTD = paxSTD;
        this.PRBCCode = PRBCCode;
        this.scheduleServiceType = scheduleServiceType;
        this.sold = sold;
        this.outMoveDays = outMoveDays;
        this.backMoveDays = backMoveDays;
        this.legNests = legNests;
        this.legSSRs = legSSRs;
        this.operatingFlightNumber = operatingFlightNumber;
        this.operatedByText = operatedByText;
        this.operatingCarrier = operatingCarrier;
        this.operatingOpSuffix = operatingOpSuffix;
        this.subjectToGovtApproval = subjectToGovtApproval;
        this.marketingCode = marketingCode;
        this.changeOfDirection = changeOfDirection;
        this.marketingOverride = marketingOverride;
    }


    /**
     * Gets the adjustedCapacity value for this LegInfo.
     * 
     * @return adjustedCapacity
     */
    public java.lang.Short getAdjustedCapacity() {
        return adjustedCapacity;
    }


    /**
     * Sets the adjustedCapacity value for this LegInfo.
     * 
     * @param adjustedCapacity
     */
    public void setAdjustedCapacity(java.lang.Short adjustedCapacity) {
        this.adjustedCapacity = adjustedCapacity;
    }


    /**
     * Gets the equipmentType value for this LegInfo.
     * 
     * @return equipmentType
     */
    public java.lang.String getEquipmentType() {
        return equipmentType;
    }


    /**
     * Sets the equipmentType value for this LegInfo.
     * 
     * @param equipmentType
     */
    public void setEquipmentType(java.lang.String equipmentType) {
        this.equipmentType = equipmentType;
    }


    /**
     * Gets the equipmentTypeSuffix value for this LegInfo.
     * 
     * @return equipmentTypeSuffix
     */
    public java.lang.String getEquipmentTypeSuffix() {
        return equipmentTypeSuffix;
    }


    /**
     * Sets the equipmentTypeSuffix value for this LegInfo.
     * 
     * @param equipmentTypeSuffix
     */
    public void setEquipmentTypeSuffix(java.lang.String equipmentTypeSuffix) {
        this.equipmentTypeSuffix = equipmentTypeSuffix;
    }


    /**
     * Gets the arrivalTerminal value for this LegInfo.
     * 
     * @return arrivalTerminal
     */
    public java.lang.String getArrivalTerminal() {
        return arrivalTerminal;
    }


    /**
     * Sets the arrivalTerminal value for this LegInfo.
     * 
     * @param arrivalTerminal
     */
    public void setArrivalTerminal(java.lang.String arrivalTerminal) {
        this.arrivalTerminal = arrivalTerminal;
    }


    /**
     * Gets the arrvLTV value for this LegInfo.
     * 
     * @return arrvLTV
     */
    public java.lang.Short getArrvLTV() {
        return arrvLTV;
    }


    /**
     * Sets the arrvLTV value for this LegInfo.
     * 
     * @param arrvLTV
     */
    public void setArrvLTV(java.lang.Short arrvLTV) {
        this.arrvLTV = arrvLTV;
    }


    /**
     * Gets the capacity value for this LegInfo.
     * 
     * @return capacity
     */
    public java.lang.Short getCapacity() {
        return capacity;
    }


    /**
     * Sets the capacity value for this LegInfo.
     * 
     * @param capacity
     */
    public void setCapacity(java.lang.Short capacity) {
        this.capacity = capacity;
    }


    /**
     * Gets the codeShareIndicator value for this LegInfo.
     * 
     * @return codeShareIndicator
     */
    public java.lang.String getCodeShareIndicator() {
        return codeShareIndicator;
    }


    /**
     * Sets the codeShareIndicator value for this LegInfo.
     * 
     * @param codeShareIndicator
     */
    public void setCodeShareIndicator(java.lang.String codeShareIndicator) {
        this.codeShareIndicator = codeShareIndicator;
    }


    /**
     * Gets the departureTerminal value for this LegInfo.
     * 
     * @return departureTerminal
     */
    public java.lang.String getDepartureTerminal() {
        return departureTerminal;
    }


    /**
     * Sets the departureTerminal value for this LegInfo.
     * 
     * @param departureTerminal
     */
    public void setDepartureTerminal(java.lang.String departureTerminal) {
        this.departureTerminal = departureTerminal;
    }


    /**
     * Gets the deptLTV value for this LegInfo.
     * 
     * @return deptLTV
     */
    public java.lang.Short getDeptLTV() {
        return deptLTV;
    }


    /**
     * Sets the deptLTV value for this LegInfo.
     * 
     * @param deptLTV
     */
    public void setDeptLTV(java.lang.Short deptLTV) {
        this.deptLTV = deptLTV;
    }


    /**
     * Gets the ETicket value for this LegInfo.
     * 
     * @return ETicket
     */
    public java.lang.Boolean getETicket() {
        return ETicket;
    }


    /**
     * Sets the ETicket value for this LegInfo.
     * 
     * @param ETicket
     */
    public void setETicket(java.lang.Boolean ETicket) {
        this.ETicket = ETicket;
    }


    /**
     * Gets the flifoUpdated value for this LegInfo.
     * 
     * @return flifoUpdated
     */
    public java.lang.Boolean getFlifoUpdated() {
        return flifoUpdated;
    }


    /**
     * Sets the flifoUpdated value for this LegInfo.
     * 
     * @param flifoUpdated
     */
    public void setFlifoUpdated(java.lang.Boolean flifoUpdated) {
        this.flifoUpdated = flifoUpdated;
    }


    /**
     * Gets the IROP value for this LegInfo.
     * 
     * @return IROP
     */
    public java.lang.Boolean getIROP() {
        return IROP;
    }


    /**
     * Sets the IROP value for this LegInfo.
     * 
     * @param IROP
     */
    public void setIROP(java.lang.Boolean IROP) {
        this.IROP = IROP;
    }


    /**
     * Gets the status value for this LegInfo.
     * 
     * @return status
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LegStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this LegInfo.
     * 
     * @param status
     */
    public void setStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LegStatus status) {
        this.status = status;
    }


    /**
     * Gets the lid value for this LegInfo.
     * 
     * @return lid
     */
    public java.lang.Short getLid() {
        return lid;
    }


    /**
     * Sets the lid value for this LegInfo.
     * 
     * @param lid
     */
    public void setLid(java.lang.Short lid) {
        this.lid = lid;
    }


    /**
     * Gets the onTime value for this LegInfo.
     * 
     * @return onTime
     */
    public java.lang.String getOnTime() {
        return onTime;
    }


    /**
     * Sets the onTime value for this LegInfo.
     * 
     * @param onTime
     */
    public void setOnTime(java.lang.String onTime) {
        this.onTime = onTime;
    }


    /**
     * Gets the paxSTA value for this LegInfo.
     * 
     * @return paxSTA
     */
    public java.util.Calendar getPaxSTA() {
        return paxSTA;
    }


    /**
     * Sets the paxSTA value for this LegInfo.
     * 
     * @param paxSTA
     */
    public void setPaxSTA(java.util.Calendar paxSTA) {
        this.paxSTA = paxSTA;
    }


    /**
     * Gets the paxSTD value for this LegInfo.
     * 
     * @return paxSTD
     */
    public java.util.Calendar getPaxSTD() {
        return paxSTD;
    }


    /**
     * Sets the paxSTD value for this LegInfo.
     * 
     * @param paxSTD
     */
    public void setPaxSTD(java.util.Calendar paxSTD) {
        this.paxSTD = paxSTD;
    }


    /**
     * Gets the PRBCCode value for this LegInfo.
     * 
     * @return PRBCCode
     */
    public java.lang.String getPRBCCode() {
        return PRBCCode;
    }


    /**
     * Sets the PRBCCode value for this LegInfo.
     * 
     * @param PRBCCode
     */
    public void setPRBCCode(java.lang.String PRBCCode) {
        this.PRBCCode = PRBCCode;
    }


    /**
     * Gets the scheduleServiceType value for this LegInfo.
     * 
     * @return scheduleServiceType
     */
    public java.lang.String getScheduleServiceType() {
        return scheduleServiceType;
    }


    /**
     * Sets the scheduleServiceType value for this LegInfo.
     * 
     * @param scheduleServiceType
     */
    public void setScheduleServiceType(java.lang.String scheduleServiceType) {
        this.scheduleServiceType = scheduleServiceType;
    }


    /**
     * Gets the sold value for this LegInfo.
     * 
     * @return sold
     */
    public java.lang.Short getSold() {
        return sold;
    }


    /**
     * Sets the sold value for this LegInfo.
     * 
     * @param sold
     */
    public void setSold(java.lang.Short sold) {
        this.sold = sold;
    }


    /**
     * Gets the outMoveDays value for this LegInfo.
     * 
     * @return outMoveDays
     */
    public java.lang.Short getOutMoveDays() {
        return outMoveDays;
    }


    /**
     * Sets the outMoveDays value for this LegInfo.
     * 
     * @param outMoveDays
     */
    public void setOutMoveDays(java.lang.Short outMoveDays) {
        this.outMoveDays = outMoveDays;
    }


    /**
     * Gets the backMoveDays value for this LegInfo.
     * 
     * @return backMoveDays
     */
    public java.lang.Short getBackMoveDays() {
        return backMoveDays;
    }


    /**
     * Sets the backMoveDays value for this LegInfo.
     * 
     * @param backMoveDays
     */
    public void setBackMoveDays(java.lang.Short backMoveDays) {
        this.backMoveDays = backMoveDays;
    }


    /**
     * Gets the legNests value for this LegInfo.
     * 
     * @return legNests
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.LegNest[] getLegNests() {
        return legNests;
    }


    /**
     * Sets the legNests value for this LegInfo.
     * 
     * @param legNests
     */
    public void setLegNests(com.navitaire.schemas.WebServices.DataContracts.Booking.LegNest[] legNests) {
        this.legNests = legNests;
    }


    /**
     * Gets the legSSRs value for this LegInfo.
     * 
     * @return legSSRs
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.LegSSR[] getLegSSRs() {
        return legSSRs;
    }


    /**
     * Sets the legSSRs value for this LegInfo.
     * 
     * @param legSSRs
     */
    public void setLegSSRs(com.navitaire.schemas.WebServices.DataContracts.Booking.LegSSR[] legSSRs) {
        this.legSSRs = legSSRs;
    }


    /**
     * Gets the operatingFlightNumber value for this LegInfo.
     * 
     * @return operatingFlightNumber
     */
    public java.lang.String getOperatingFlightNumber() {
        return operatingFlightNumber;
    }


    /**
     * Sets the operatingFlightNumber value for this LegInfo.
     * 
     * @param operatingFlightNumber
     */
    public void setOperatingFlightNumber(java.lang.String operatingFlightNumber) {
        this.operatingFlightNumber = operatingFlightNumber;
    }


    /**
     * Gets the operatedByText value for this LegInfo.
     * 
     * @return operatedByText
     */
    public java.lang.String getOperatedByText() {
        return operatedByText;
    }


    /**
     * Sets the operatedByText value for this LegInfo.
     * 
     * @param operatedByText
     */
    public void setOperatedByText(java.lang.String operatedByText) {
        this.operatedByText = operatedByText;
    }


    /**
     * Gets the operatingCarrier value for this LegInfo.
     * 
     * @return operatingCarrier
     */
    public java.lang.String getOperatingCarrier() {
        return operatingCarrier;
    }


    /**
     * Sets the operatingCarrier value for this LegInfo.
     * 
     * @param operatingCarrier
     */
    public void setOperatingCarrier(java.lang.String operatingCarrier) {
        this.operatingCarrier = operatingCarrier;
    }


    /**
     * Gets the operatingOpSuffix value for this LegInfo.
     * 
     * @return operatingOpSuffix
     */
    public java.lang.String getOperatingOpSuffix() {
        return operatingOpSuffix;
    }


    /**
     * Sets the operatingOpSuffix value for this LegInfo.
     * 
     * @param operatingOpSuffix
     */
    public void setOperatingOpSuffix(java.lang.String operatingOpSuffix) {
        this.operatingOpSuffix = operatingOpSuffix;
    }


    /**
     * Gets the subjectToGovtApproval value for this LegInfo.
     * 
     * @return subjectToGovtApproval
     */
    public java.lang.Boolean getSubjectToGovtApproval() {
        return subjectToGovtApproval;
    }


    /**
     * Sets the subjectToGovtApproval value for this LegInfo.
     * 
     * @param subjectToGovtApproval
     */
    public void setSubjectToGovtApproval(java.lang.Boolean subjectToGovtApproval) {
        this.subjectToGovtApproval = subjectToGovtApproval;
    }


    /**
     * Gets the marketingCode value for this LegInfo.
     * 
     * @return marketingCode
     */
    public java.lang.String getMarketingCode() {
        return marketingCode;
    }


    /**
     * Sets the marketingCode value for this LegInfo.
     * 
     * @param marketingCode
     */
    public void setMarketingCode(java.lang.String marketingCode) {
        this.marketingCode = marketingCode;
    }


    /**
     * Gets the changeOfDirection value for this LegInfo.
     * 
     * @return changeOfDirection
     */
    public java.lang.Boolean getChangeOfDirection() {
        return changeOfDirection;
    }


    /**
     * Sets the changeOfDirection value for this LegInfo.
     * 
     * @param changeOfDirection
     */
    public void setChangeOfDirection(java.lang.Boolean changeOfDirection) {
        this.changeOfDirection = changeOfDirection;
    }


    /**
     * Gets the marketingOverride value for this LegInfo.
     * 
     * @return marketingOverride
     */
    public java.lang.Boolean getMarketingOverride() {
        return marketingOverride;
    }


    /**
     * Sets the marketingOverride value for this LegInfo.
     * 
     * @param marketingOverride
     */
    public void setMarketingOverride(java.lang.Boolean marketingOverride) {
        this.marketingOverride = marketingOverride;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LegInfo)) return false;
        LegInfo other = (LegInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.adjustedCapacity==null && other.getAdjustedCapacity()==null) || 
             (this.adjustedCapacity!=null &&
              this.adjustedCapacity.equals(other.getAdjustedCapacity()))) &&
            ((this.equipmentType==null && other.getEquipmentType()==null) || 
             (this.equipmentType!=null &&
              this.equipmentType.equals(other.getEquipmentType()))) &&
            ((this.equipmentTypeSuffix==null && other.getEquipmentTypeSuffix()==null) || 
             (this.equipmentTypeSuffix!=null &&
              this.equipmentTypeSuffix.equals(other.getEquipmentTypeSuffix()))) &&
            ((this.arrivalTerminal==null && other.getArrivalTerminal()==null) || 
             (this.arrivalTerminal!=null &&
              this.arrivalTerminal.equals(other.getArrivalTerminal()))) &&
            ((this.arrvLTV==null && other.getArrvLTV()==null) || 
             (this.arrvLTV!=null &&
              this.arrvLTV.equals(other.getArrvLTV()))) &&
            ((this.capacity==null && other.getCapacity()==null) || 
             (this.capacity!=null &&
              this.capacity.equals(other.getCapacity()))) &&
            ((this.codeShareIndicator==null && other.getCodeShareIndicator()==null) || 
             (this.codeShareIndicator!=null &&
              this.codeShareIndicator.equals(other.getCodeShareIndicator()))) &&
            ((this.departureTerminal==null && other.getDepartureTerminal()==null) || 
             (this.departureTerminal!=null &&
              this.departureTerminal.equals(other.getDepartureTerminal()))) &&
            ((this.deptLTV==null && other.getDeptLTV()==null) || 
             (this.deptLTV!=null &&
              this.deptLTV.equals(other.getDeptLTV()))) &&
            ((this.ETicket==null && other.getETicket()==null) || 
             (this.ETicket!=null &&
              this.ETicket.equals(other.getETicket()))) &&
            ((this.flifoUpdated==null && other.getFlifoUpdated()==null) || 
             (this.flifoUpdated!=null &&
              this.flifoUpdated.equals(other.getFlifoUpdated()))) &&
            ((this.IROP==null && other.getIROP()==null) || 
             (this.IROP!=null &&
              this.IROP.equals(other.getIROP()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.lid==null && other.getLid()==null) || 
             (this.lid!=null &&
              this.lid.equals(other.getLid()))) &&
            ((this.onTime==null && other.getOnTime()==null) || 
             (this.onTime!=null &&
              this.onTime.equals(other.getOnTime()))) &&
            ((this.paxSTA==null && other.getPaxSTA()==null) || 
             (this.paxSTA!=null &&
              this.paxSTA.equals(other.getPaxSTA()))) &&
            ((this.paxSTD==null && other.getPaxSTD()==null) || 
             (this.paxSTD!=null &&
              this.paxSTD.equals(other.getPaxSTD()))) &&
            ((this.PRBCCode==null && other.getPRBCCode()==null) || 
             (this.PRBCCode!=null &&
              this.PRBCCode.equals(other.getPRBCCode()))) &&
            ((this.scheduleServiceType==null && other.getScheduleServiceType()==null) || 
             (this.scheduleServiceType!=null &&
              this.scheduleServiceType.equals(other.getScheduleServiceType()))) &&
            ((this.sold==null && other.getSold()==null) || 
             (this.sold!=null &&
              this.sold.equals(other.getSold()))) &&
            ((this.outMoveDays==null && other.getOutMoveDays()==null) || 
             (this.outMoveDays!=null &&
              this.outMoveDays.equals(other.getOutMoveDays()))) &&
            ((this.backMoveDays==null && other.getBackMoveDays()==null) || 
             (this.backMoveDays!=null &&
              this.backMoveDays.equals(other.getBackMoveDays()))) &&
            ((this.legNests==null && other.getLegNests()==null) || 
             (this.legNests!=null &&
              java.util.Arrays.equals(this.legNests, other.getLegNests()))) &&
            ((this.legSSRs==null && other.getLegSSRs()==null) || 
             (this.legSSRs!=null &&
              java.util.Arrays.equals(this.legSSRs, other.getLegSSRs()))) &&
            ((this.operatingFlightNumber==null && other.getOperatingFlightNumber()==null) || 
             (this.operatingFlightNumber!=null &&
              this.operatingFlightNumber.equals(other.getOperatingFlightNumber()))) &&
            ((this.operatedByText==null && other.getOperatedByText()==null) || 
             (this.operatedByText!=null &&
              this.operatedByText.equals(other.getOperatedByText()))) &&
            ((this.operatingCarrier==null && other.getOperatingCarrier()==null) || 
             (this.operatingCarrier!=null &&
              this.operatingCarrier.equals(other.getOperatingCarrier()))) &&
            ((this.operatingOpSuffix==null && other.getOperatingOpSuffix()==null) || 
             (this.operatingOpSuffix!=null &&
              this.operatingOpSuffix.equals(other.getOperatingOpSuffix()))) &&
            ((this.subjectToGovtApproval==null && other.getSubjectToGovtApproval()==null) || 
             (this.subjectToGovtApproval!=null &&
              this.subjectToGovtApproval.equals(other.getSubjectToGovtApproval()))) &&
            ((this.marketingCode==null && other.getMarketingCode()==null) || 
             (this.marketingCode!=null &&
              this.marketingCode.equals(other.getMarketingCode()))) &&
            ((this.changeOfDirection==null && other.getChangeOfDirection()==null) || 
             (this.changeOfDirection!=null &&
              this.changeOfDirection.equals(other.getChangeOfDirection()))) &&
            ((this.marketingOverride==null && other.getMarketingOverride()==null) || 
             (this.marketingOverride!=null &&
              this.marketingOverride.equals(other.getMarketingOverride())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAdjustedCapacity() != null) {
            _hashCode += getAdjustedCapacity().hashCode();
        }
        if (getEquipmentType() != null) {
            _hashCode += getEquipmentType().hashCode();
        }
        if (getEquipmentTypeSuffix() != null) {
            _hashCode += getEquipmentTypeSuffix().hashCode();
        }
        if (getArrivalTerminal() != null) {
            _hashCode += getArrivalTerminal().hashCode();
        }
        if (getArrvLTV() != null) {
            _hashCode += getArrvLTV().hashCode();
        }
        if (getCapacity() != null) {
            _hashCode += getCapacity().hashCode();
        }
        if (getCodeShareIndicator() != null) {
            _hashCode += getCodeShareIndicator().hashCode();
        }
        if (getDepartureTerminal() != null) {
            _hashCode += getDepartureTerminal().hashCode();
        }
        if (getDeptLTV() != null) {
            _hashCode += getDeptLTV().hashCode();
        }
        if (getETicket() != null) {
            _hashCode += getETicket().hashCode();
        }
        if (getFlifoUpdated() != null) {
            _hashCode += getFlifoUpdated().hashCode();
        }
        if (getIROP() != null) {
            _hashCode += getIROP().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getLid() != null) {
            _hashCode += getLid().hashCode();
        }
        if (getOnTime() != null) {
            _hashCode += getOnTime().hashCode();
        }
        if (getPaxSTA() != null) {
            _hashCode += getPaxSTA().hashCode();
        }
        if (getPaxSTD() != null) {
            _hashCode += getPaxSTD().hashCode();
        }
        if (getPRBCCode() != null) {
            _hashCode += getPRBCCode().hashCode();
        }
        if (getScheduleServiceType() != null) {
            _hashCode += getScheduleServiceType().hashCode();
        }
        if (getSold() != null) {
            _hashCode += getSold().hashCode();
        }
        if (getOutMoveDays() != null) {
            _hashCode += getOutMoveDays().hashCode();
        }
        if (getBackMoveDays() != null) {
            _hashCode += getBackMoveDays().hashCode();
        }
        if (getLegNests() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLegNests());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLegNests(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLegSSRs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLegSSRs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLegSSRs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOperatingFlightNumber() != null) {
            _hashCode += getOperatingFlightNumber().hashCode();
        }
        if (getOperatedByText() != null) {
            _hashCode += getOperatedByText().hashCode();
        }
        if (getOperatingCarrier() != null) {
            _hashCode += getOperatingCarrier().hashCode();
        }
        if (getOperatingOpSuffix() != null) {
            _hashCode += getOperatingOpSuffix().hashCode();
        }
        if (getSubjectToGovtApproval() != null) {
            _hashCode += getSubjectToGovtApproval().hashCode();
        }
        if (getMarketingCode() != null) {
            _hashCode += getMarketingCode().hashCode();
        }
        if (getChangeOfDirection() != null) {
            _hashCode += getChangeOfDirection().hashCode();
        }
        if (getMarketingOverride() != null) {
            _hashCode += getMarketingOverride().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LegInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adjustedCapacity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AdjustedCapacity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentTypeSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentTypeSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalTerminal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalTerminal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrvLTV");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrvLTV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("capacity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Capacity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeShareIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CodeShareIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureTerminal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureTerminal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deptLTV");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DeptLTV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ETicket");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ETicket"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flifoUpdated");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlifoUpdated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IROP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IROP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "LegStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Lid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("onTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OnTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxSTA");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxSTD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSTD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRBCCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PRBCCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduleServiceType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ScheduleServiceType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sold");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Sold"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outMoveDays");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OutMoveDays"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("backMoveDays");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BackMoveDays"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("legNests");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegNests"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegNest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegNest"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("legSSRs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegSSRs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegSSR"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegSSR"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatingFlightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OperatingFlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatedByText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OperatedByText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatingCarrier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OperatingCarrier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatingOpSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OperatingOpSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subjectToGovtApproval");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SubjectToGovtApproval"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketingCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MarketingCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeOfDirection");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChangeOfDirection"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketingOverride");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MarketingOverride"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
