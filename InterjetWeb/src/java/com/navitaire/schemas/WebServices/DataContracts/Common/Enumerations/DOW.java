/**
 * DOW.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class DOW implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected DOW(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _None = "None";
    public static final java.lang.String _Monday = "Monday";
    public static final java.lang.String _Tuesday = "Tuesday";
    public static final java.lang.String _Wednesday = "Wednesday";
    public static final java.lang.String _Thursday = "Thursday";
    public static final java.lang.String _Friday = "Friday";
    public static final java.lang.String _WeekDay = "WeekDay";
    public static final java.lang.String _Saturday = "Saturday";
    public static final java.lang.String _Sunday = "Sunday";
    public static final java.lang.String _WeekEnd = "WeekEnd";
    public static final java.lang.String _Daily = "Daily";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final DOW None = new DOW(_None);
    public static final DOW Monday = new DOW(_Monday);
    public static final DOW Tuesday = new DOW(_Tuesday);
    public static final DOW Wednesday = new DOW(_Wednesday);
    public static final DOW Thursday = new DOW(_Thursday);
    public static final DOW Friday = new DOW(_Friday);
    public static final DOW WeekDay = new DOW(_WeekDay);
    public static final DOW Saturday = new DOW(_Saturday);
    public static final DOW Sunday = new DOW(_Sunday);
    public static final DOW WeekEnd = new DOW(_WeekEnd);
    public static final DOW Daily = new DOW(_Daily);
    public static final DOW Unmapped = new DOW(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static DOW fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        DOW enumeration = (DOW)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static DOW fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DOW.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DOW"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
