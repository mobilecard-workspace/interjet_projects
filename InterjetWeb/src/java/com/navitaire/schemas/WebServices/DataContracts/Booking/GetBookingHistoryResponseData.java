/**
 * GetBookingHistoryResponseData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GetBookingHistoryResponseData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHistory[] bookingHistories;

    private java.lang.Long lastID;

    private java.lang.Short pageSize;

    private java.lang.Integer totalCount;

    public GetBookingHistoryResponseData() {
    }

    public GetBookingHistoryResponseData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHistory[] bookingHistories,
           java.lang.Long lastID,
           java.lang.Short pageSize,
           java.lang.Integer totalCount) {
           this.bookingHistories = bookingHistories;
           this.lastID = lastID;
           this.pageSize = pageSize;
           this.totalCount = totalCount;
    }


    /**
     * Gets the bookingHistories value for this GetBookingHistoryResponseData.
     * 
     * @return bookingHistories
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHistory[] getBookingHistories() {
        return bookingHistories;
    }


    /**
     * Sets the bookingHistories value for this GetBookingHistoryResponseData.
     * 
     * @param bookingHistories
     */
    public void setBookingHistories(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHistory[] bookingHistories) {
        this.bookingHistories = bookingHistories;
    }


    /**
     * Gets the lastID value for this GetBookingHistoryResponseData.
     * 
     * @return lastID
     */
    public java.lang.Long getLastID() {
        return lastID;
    }


    /**
     * Sets the lastID value for this GetBookingHistoryResponseData.
     * 
     * @param lastID
     */
    public void setLastID(java.lang.Long lastID) {
        this.lastID = lastID;
    }


    /**
     * Gets the pageSize value for this GetBookingHistoryResponseData.
     * 
     * @return pageSize
     */
    public java.lang.Short getPageSize() {
        return pageSize;
    }


    /**
     * Sets the pageSize value for this GetBookingHistoryResponseData.
     * 
     * @param pageSize
     */
    public void setPageSize(java.lang.Short pageSize) {
        this.pageSize = pageSize;
    }


    /**
     * Gets the totalCount value for this GetBookingHistoryResponseData.
     * 
     * @return totalCount
     */
    public java.lang.Integer getTotalCount() {
        return totalCount;
    }


    /**
     * Sets the totalCount value for this GetBookingHistoryResponseData.
     * 
     * @param totalCount
     */
    public void setTotalCount(java.lang.Integer totalCount) {
        this.totalCount = totalCount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBookingHistoryResponseData)) return false;
        GetBookingHistoryResponseData other = (GetBookingHistoryResponseData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bookingHistories==null && other.getBookingHistories()==null) || 
             (this.bookingHistories!=null &&
              java.util.Arrays.equals(this.bookingHistories, other.getBookingHistories()))) &&
            ((this.lastID==null && other.getLastID()==null) || 
             (this.lastID!=null &&
              this.lastID.equals(other.getLastID()))) &&
            ((this.pageSize==null && other.getPageSize()==null) || 
             (this.pageSize!=null &&
              this.pageSize.equals(other.getPageSize()))) &&
            ((this.totalCount==null && other.getTotalCount()==null) || 
             (this.totalCount!=null &&
              this.totalCount.equals(other.getTotalCount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBookingHistories() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingHistories());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingHistories(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLastID() != null) {
            _hashCode += getLastID().hashCode();
        }
        if (getPageSize() != null) {
            _hashCode += getPageSize().hashCode();
        }
        if (getTotalCount() != null) {
            _hashCode += getTotalCount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBookingHistoryResponseData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingHistoryResponseData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingHistories");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHistories"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHistory"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHistory"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LastID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageSize");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PageSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TotalCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
