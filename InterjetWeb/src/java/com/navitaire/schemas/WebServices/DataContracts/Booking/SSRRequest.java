/**
 * SSRRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SSRRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSSRRequest[] segmentSSRRequests;

    private java.lang.String currencyCode;

    private java.lang.Boolean cancelFirstSSR;

    private java.lang.Boolean SSRFeeForceWaiveOnSell;

    public SSRRequest() {
    }

    public SSRRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSSRRequest[] segmentSSRRequests,
           java.lang.String currencyCode,
           java.lang.Boolean cancelFirstSSR,
           java.lang.Boolean SSRFeeForceWaiveOnSell) {
           this.segmentSSRRequests = segmentSSRRequests;
           this.currencyCode = currencyCode;
           this.cancelFirstSSR = cancelFirstSSR;
           this.SSRFeeForceWaiveOnSell = SSRFeeForceWaiveOnSell;
    }


    /**
     * Gets the segmentSSRRequests value for this SSRRequest.
     * 
     * @return segmentSSRRequests
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSSRRequest[] getSegmentSSRRequests() {
        return segmentSSRRequests;
    }


    /**
     * Sets the segmentSSRRequests value for this SSRRequest.
     * 
     * @param segmentSSRRequests
     */
    public void setSegmentSSRRequests(com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSSRRequest[] segmentSSRRequests) {
        this.segmentSSRRequests = segmentSSRRequests;
    }


    /**
     * Gets the currencyCode value for this SSRRequest.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this SSRRequest.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the cancelFirstSSR value for this SSRRequest.
     * 
     * @return cancelFirstSSR
     */
    public java.lang.Boolean getCancelFirstSSR() {
        return cancelFirstSSR;
    }


    /**
     * Sets the cancelFirstSSR value for this SSRRequest.
     * 
     * @param cancelFirstSSR
     */
    public void setCancelFirstSSR(java.lang.Boolean cancelFirstSSR) {
        this.cancelFirstSSR = cancelFirstSSR;
    }


    /**
     * Gets the SSRFeeForceWaiveOnSell value for this SSRRequest.
     * 
     * @return SSRFeeForceWaiveOnSell
     */
    public java.lang.Boolean getSSRFeeForceWaiveOnSell() {
        return SSRFeeForceWaiveOnSell;
    }


    /**
     * Sets the SSRFeeForceWaiveOnSell value for this SSRRequest.
     * 
     * @param SSRFeeForceWaiveOnSell
     */
    public void setSSRFeeForceWaiveOnSell(java.lang.Boolean SSRFeeForceWaiveOnSell) {
        this.SSRFeeForceWaiveOnSell = SSRFeeForceWaiveOnSell;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SSRRequest)) return false;
        SSRRequest other = (SSRRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.segmentSSRRequests==null && other.getSegmentSSRRequests()==null) || 
             (this.segmentSSRRequests!=null &&
              java.util.Arrays.equals(this.segmentSSRRequests, other.getSegmentSSRRequests()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.cancelFirstSSR==null && other.getCancelFirstSSR()==null) || 
             (this.cancelFirstSSR!=null &&
              this.cancelFirstSSR.equals(other.getCancelFirstSSR()))) &&
            ((this.SSRFeeForceWaiveOnSell==null && other.getSSRFeeForceWaiveOnSell()==null) || 
             (this.SSRFeeForceWaiveOnSell!=null &&
              this.SSRFeeForceWaiveOnSell.equals(other.getSSRFeeForceWaiveOnSell())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSegmentSSRRequests() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegmentSSRRequests());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegmentSSRRequests(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getCancelFirstSSR() != null) {
            _hashCode += getCancelFirstSSR().hashCode();
        }
        if (getSSRFeeForceWaiveOnSell() != null) {
            _hashCode += getSSRFeeForceWaiveOnSell().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SSRRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentSSRRequests");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSSRRequests"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSSRRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSSRRequest"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelFirstSSR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelFirstSSR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRFeeForceWaiveOnSell");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRFeeForceWaiveOnSell"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
