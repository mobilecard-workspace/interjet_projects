/**
 * DateMarketLowFare.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DateMarketLowFare  implements java.io.Serializable {
    private java.lang.String departureCity;

    private java.lang.String arrivalCity;

    private java.math.BigDecimal fareAmount;

    private java.math.BigDecimal taxesAndFeesAmount;

    private java.util.Calendar departureDate;

    private java.util.Calendar expireUTC;

    private java.lang.Boolean includesTaxesAndFees;

    private java.lang.String carrierCode;

    private java.lang.String statusCode;

    private java.math.BigDecimal farePointAmount;

    private java.lang.Short availableCount;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLowFare[] dateFlightLowFareList;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketSegment[] dateMarketSegmentList;

    private java.lang.String currencyCode;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare[] alternateLowFareList;

    public DateMarketLowFare() {
    }

    public DateMarketLowFare(
           java.lang.String departureCity,
           java.lang.String arrivalCity,
           java.math.BigDecimal fareAmount,
           java.math.BigDecimal taxesAndFeesAmount,
           java.util.Calendar departureDate,
           java.util.Calendar expireUTC,
           java.lang.Boolean includesTaxesAndFees,
           java.lang.String carrierCode,
           java.lang.String statusCode,
           java.math.BigDecimal farePointAmount,
           java.lang.Short availableCount,
           com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLowFare[] dateFlightLowFareList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketSegment[] dateMarketSegmentList,
           java.lang.String currencyCode,
           com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare[] alternateLowFareList) {
           this.departureCity = departureCity;
           this.arrivalCity = arrivalCity;
           this.fareAmount = fareAmount;
           this.taxesAndFeesAmount = taxesAndFeesAmount;
           this.departureDate = departureDate;
           this.expireUTC = expireUTC;
           this.includesTaxesAndFees = includesTaxesAndFees;
           this.carrierCode = carrierCode;
           this.statusCode = statusCode;
           this.farePointAmount = farePointAmount;
           this.availableCount = availableCount;
           this.dateFlightLowFareList = dateFlightLowFareList;
           this.dateMarketSegmentList = dateMarketSegmentList;
           this.currencyCode = currencyCode;
           this.alternateLowFareList = alternateLowFareList;
    }


    /**
     * Gets the departureCity value for this DateMarketLowFare.
     * 
     * @return departureCity
     */
    public java.lang.String getDepartureCity() {
        return departureCity;
    }


    /**
     * Sets the departureCity value for this DateMarketLowFare.
     * 
     * @param departureCity
     */
    public void setDepartureCity(java.lang.String departureCity) {
        this.departureCity = departureCity;
    }


    /**
     * Gets the arrivalCity value for this DateMarketLowFare.
     * 
     * @return arrivalCity
     */
    public java.lang.String getArrivalCity() {
        return arrivalCity;
    }


    /**
     * Sets the arrivalCity value for this DateMarketLowFare.
     * 
     * @param arrivalCity
     */
    public void setArrivalCity(java.lang.String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }


    /**
     * Gets the fareAmount value for this DateMarketLowFare.
     * 
     * @return fareAmount
     */
    public java.math.BigDecimal getFareAmount() {
        return fareAmount;
    }


    /**
     * Sets the fareAmount value for this DateMarketLowFare.
     * 
     * @param fareAmount
     */
    public void setFareAmount(java.math.BigDecimal fareAmount) {
        this.fareAmount = fareAmount;
    }


    /**
     * Gets the taxesAndFeesAmount value for this DateMarketLowFare.
     * 
     * @return taxesAndFeesAmount
     */
    public java.math.BigDecimal getTaxesAndFeesAmount() {
        return taxesAndFeesAmount;
    }


    /**
     * Sets the taxesAndFeesAmount value for this DateMarketLowFare.
     * 
     * @param taxesAndFeesAmount
     */
    public void setTaxesAndFeesAmount(java.math.BigDecimal taxesAndFeesAmount) {
        this.taxesAndFeesAmount = taxesAndFeesAmount;
    }


    /**
     * Gets the departureDate value for this DateMarketLowFare.
     * 
     * @return departureDate
     */
    public java.util.Calendar getDepartureDate() {
        return departureDate;
    }


    /**
     * Sets the departureDate value for this DateMarketLowFare.
     * 
     * @param departureDate
     */
    public void setDepartureDate(java.util.Calendar departureDate) {
        this.departureDate = departureDate;
    }


    /**
     * Gets the expireUTC value for this DateMarketLowFare.
     * 
     * @return expireUTC
     */
    public java.util.Calendar getExpireUTC() {
        return expireUTC;
    }


    /**
     * Sets the expireUTC value for this DateMarketLowFare.
     * 
     * @param expireUTC
     */
    public void setExpireUTC(java.util.Calendar expireUTC) {
        this.expireUTC = expireUTC;
    }


    /**
     * Gets the includesTaxesAndFees value for this DateMarketLowFare.
     * 
     * @return includesTaxesAndFees
     */
    public java.lang.Boolean getIncludesTaxesAndFees() {
        return includesTaxesAndFees;
    }


    /**
     * Sets the includesTaxesAndFees value for this DateMarketLowFare.
     * 
     * @param includesTaxesAndFees
     */
    public void setIncludesTaxesAndFees(java.lang.Boolean includesTaxesAndFees) {
        this.includesTaxesAndFees = includesTaxesAndFees;
    }


    /**
     * Gets the carrierCode value for this DateMarketLowFare.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this DateMarketLowFare.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the statusCode value for this DateMarketLowFare.
     * 
     * @return statusCode
     */
    public java.lang.String getStatusCode() {
        return statusCode;
    }


    /**
     * Sets the statusCode value for this DateMarketLowFare.
     * 
     * @param statusCode
     */
    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }


    /**
     * Gets the farePointAmount value for this DateMarketLowFare.
     * 
     * @return farePointAmount
     */
    public java.math.BigDecimal getFarePointAmount() {
        return farePointAmount;
    }


    /**
     * Sets the farePointAmount value for this DateMarketLowFare.
     * 
     * @param farePointAmount
     */
    public void setFarePointAmount(java.math.BigDecimal farePointAmount) {
        this.farePointAmount = farePointAmount;
    }


    /**
     * Gets the availableCount value for this DateMarketLowFare.
     * 
     * @return availableCount
     */
    public java.lang.Short getAvailableCount() {
        return availableCount;
    }


    /**
     * Sets the availableCount value for this DateMarketLowFare.
     * 
     * @param availableCount
     */
    public void setAvailableCount(java.lang.Short availableCount) {
        this.availableCount = availableCount;
    }


    /**
     * Gets the dateFlightLowFareList value for this DateMarketLowFare.
     * 
     * @return dateFlightLowFareList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLowFare[] getDateFlightLowFareList() {
        return dateFlightLowFareList;
    }


    /**
     * Sets the dateFlightLowFareList value for this DateMarketLowFare.
     * 
     * @param dateFlightLowFareList
     */
    public void setDateFlightLowFareList(com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLowFare[] dateFlightLowFareList) {
        this.dateFlightLowFareList = dateFlightLowFareList;
    }


    /**
     * Gets the dateMarketSegmentList value for this DateMarketLowFare.
     * 
     * @return dateMarketSegmentList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketSegment[] getDateMarketSegmentList() {
        return dateMarketSegmentList;
    }


    /**
     * Sets the dateMarketSegmentList value for this DateMarketLowFare.
     * 
     * @param dateMarketSegmentList
     */
    public void setDateMarketSegmentList(com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketSegment[] dateMarketSegmentList) {
        this.dateMarketSegmentList = dateMarketSegmentList;
    }


    /**
     * Gets the currencyCode value for this DateMarketLowFare.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this DateMarketLowFare.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the alternateLowFareList value for this DateMarketLowFare.
     * 
     * @return alternateLowFareList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare[] getAlternateLowFareList() {
        return alternateLowFareList;
    }


    /**
     * Sets the alternateLowFareList value for this DateMarketLowFare.
     * 
     * @param alternateLowFareList
     */
    public void setAlternateLowFareList(com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare[] alternateLowFareList) {
        this.alternateLowFareList = alternateLowFareList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DateMarketLowFare)) return false;
        DateMarketLowFare other = (DateMarketLowFare) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departureCity==null && other.getDepartureCity()==null) || 
             (this.departureCity!=null &&
              this.departureCity.equals(other.getDepartureCity()))) &&
            ((this.arrivalCity==null && other.getArrivalCity()==null) || 
             (this.arrivalCity!=null &&
              this.arrivalCity.equals(other.getArrivalCity()))) &&
            ((this.fareAmount==null && other.getFareAmount()==null) || 
             (this.fareAmount!=null &&
              this.fareAmount.equals(other.getFareAmount()))) &&
            ((this.taxesAndFeesAmount==null && other.getTaxesAndFeesAmount()==null) || 
             (this.taxesAndFeesAmount!=null &&
              this.taxesAndFeesAmount.equals(other.getTaxesAndFeesAmount()))) &&
            ((this.departureDate==null && other.getDepartureDate()==null) || 
             (this.departureDate!=null &&
              this.departureDate.equals(other.getDepartureDate()))) &&
            ((this.expireUTC==null && other.getExpireUTC()==null) || 
             (this.expireUTC!=null &&
              this.expireUTC.equals(other.getExpireUTC()))) &&
            ((this.includesTaxesAndFees==null && other.getIncludesTaxesAndFees()==null) || 
             (this.includesTaxesAndFees!=null &&
              this.includesTaxesAndFees.equals(other.getIncludesTaxesAndFees()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.farePointAmount==null && other.getFarePointAmount()==null) || 
             (this.farePointAmount!=null &&
              this.farePointAmount.equals(other.getFarePointAmount()))) &&
            ((this.availableCount==null && other.getAvailableCount()==null) || 
             (this.availableCount!=null &&
              this.availableCount.equals(other.getAvailableCount()))) &&
            ((this.dateFlightLowFareList==null && other.getDateFlightLowFareList()==null) || 
             (this.dateFlightLowFareList!=null &&
              java.util.Arrays.equals(this.dateFlightLowFareList, other.getDateFlightLowFareList()))) &&
            ((this.dateMarketSegmentList==null && other.getDateMarketSegmentList()==null) || 
             (this.dateMarketSegmentList!=null &&
              java.util.Arrays.equals(this.dateMarketSegmentList, other.getDateMarketSegmentList()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.alternateLowFareList==null && other.getAlternateLowFareList()==null) || 
             (this.alternateLowFareList!=null &&
              java.util.Arrays.equals(this.alternateLowFareList, other.getAlternateLowFareList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartureCity() != null) {
            _hashCode += getDepartureCity().hashCode();
        }
        if (getArrivalCity() != null) {
            _hashCode += getArrivalCity().hashCode();
        }
        if (getFareAmount() != null) {
            _hashCode += getFareAmount().hashCode();
        }
        if (getTaxesAndFeesAmount() != null) {
            _hashCode += getTaxesAndFeesAmount().hashCode();
        }
        if (getDepartureDate() != null) {
            _hashCode += getDepartureDate().hashCode();
        }
        if (getExpireUTC() != null) {
            _hashCode += getExpireUTC().hashCode();
        }
        if (getIncludesTaxesAndFees() != null) {
            _hashCode += getIncludesTaxesAndFees().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        if (getFarePointAmount() != null) {
            _hashCode += getFarePointAmount().hashCode();
        }
        if (getAvailableCount() != null) {
            _hashCode += getAvailableCount().hashCode();
        }
        if (getDateFlightLowFareList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDateFlightLowFareList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDateFlightLowFareList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDateMarketSegmentList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDateMarketSegmentList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDateMarketSegmentList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getAlternateLowFareList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAlternateLowFareList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAlternateLowFareList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DateMarketLowFare.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketLowFare"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxesAndFeesAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TaxesAndFeesAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expireUTC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ExpireUTC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includesTaxesAndFees");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IncludesTaxesAndFees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "StatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("farePointAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FarePointAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailableCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateFlightLowFareList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLowFareList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLowFare"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLowFare"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateMarketSegmentList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketSegmentList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketSegment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketSegment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternateLowFareList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFareList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFare"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFare"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
