/**
 * MovePassengerJourneyType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class MovePassengerJourneyType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected MovePassengerJourneyType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _None = "None";
    public static final java.lang.String _IROP = "IROP";
    public static final java.lang.String _Diversion = "Diversion";
    public static final java.lang.String _FlightClose = "FlightClose";
    public static final java.lang.String _FlyAhead = "FlyAhead";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final MovePassengerJourneyType None = new MovePassengerJourneyType(_None);
    public static final MovePassengerJourneyType IROP = new MovePassengerJourneyType(_IROP);
    public static final MovePassengerJourneyType Diversion = new MovePassengerJourneyType(_Diversion);
    public static final MovePassengerJourneyType FlightClose = new MovePassengerJourneyType(_FlightClose);
    public static final MovePassengerJourneyType FlyAhead = new MovePassengerJourneyType(_FlyAhead);
    public static final MovePassengerJourneyType Unmapped = new MovePassengerJourneyType(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static MovePassengerJourneyType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        MovePassengerJourneyType enumeration = (MovePassengerJourneyType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static MovePassengerJourneyType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MovePassengerJourneyType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "MovePassengerJourneyType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
