/**
 * DateFlightLowFare.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DateFlightLowFare  implements java.io.Serializable {
    private java.math.BigDecimal fareAmount;

    private java.math.BigDecimal taxesAndFeesAmount;

    private java.util.Calendar STA;

    private java.util.Calendar STD;

    private java.math.BigDecimal farePointAmount;

    private java.lang.Short availableCount;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLeg[] dateFlightLegList;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare[] alternateLowFareList;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFares[] dateFlightFaresList;

    public DateFlightLowFare() {
    }

    public DateFlightLowFare(
           java.math.BigDecimal fareAmount,
           java.math.BigDecimal taxesAndFeesAmount,
           java.util.Calendar STA,
           java.util.Calendar STD,
           java.math.BigDecimal farePointAmount,
           java.lang.Short availableCount,
           com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLeg[] dateFlightLegList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare[] alternateLowFareList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFares[] dateFlightFaresList) {
           this.fareAmount = fareAmount;
           this.taxesAndFeesAmount = taxesAndFeesAmount;
           this.STA = STA;
           this.STD = STD;
           this.farePointAmount = farePointAmount;
           this.availableCount = availableCount;
           this.dateFlightLegList = dateFlightLegList;
           this.alternateLowFareList = alternateLowFareList;
           this.dateFlightFaresList = dateFlightFaresList;
    }


    /**
     * Gets the fareAmount value for this DateFlightLowFare.
     * 
     * @return fareAmount
     */
    public java.math.BigDecimal getFareAmount() {
        return fareAmount;
    }


    /**
     * Sets the fareAmount value for this DateFlightLowFare.
     * 
     * @param fareAmount
     */
    public void setFareAmount(java.math.BigDecimal fareAmount) {
        this.fareAmount = fareAmount;
    }


    /**
     * Gets the taxesAndFeesAmount value for this DateFlightLowFare.
     * 
     * @return taxesAndFeesAmount
     */
    public java.math.BigDecimal getTaxesAndFeesAmount() {
        return taxesAndFeesAmount;
    }


    /**
     * Sets the taxesAndFeesAmount value for this DateFlightLowFare.
     * 
     * @param taxesAndFeesAmount
     */
    public void setTaxesAndFeesAmount(java.math.BigDecimal taxesAndFeesAmount) {
        this.taxesAndFeesAmount = taxesAndFeesAmount;
    }


    /**
     * Gets the STA value for this DateFlightLowFare.
     * 
     * @return STA
     */
    public java.util.Calendar getSTA() {
        return STA;
    }


    /**
     * Sets the STA value for this DateFlightLowFare.
     * 
     * @param STA
     */
    public void setSTA(java.util.Calendar STA) {
        this.STA = STA;
    }


    /**
     * Gets the STD value for this DateFlightLowFare.
     * 
     * @return STD
     */
    public java.util.Calendar getSTD() {
        return STD;
    }


    /**
     * Sets the STD value for this DateFlightLowFare.
     * 
     * @param STD
     */
    public void setSTD(java.util.Calendar STD) {
        this.STD = STD;
    }


    /**
     * Gets the farePointAmount value for this DateFlightLowFare.
     * 
     * @return farePointAmount
     */
    public java.math.BigDecimal getFarePointAmount() {
        return farePointAmount;
    }


    /**
     * Sets the farePointAmount value for this DateFlightLowFare.
     * 
     * @param farePointAmount
     */
    public void setFarePointAmount(java.math.BigDecimal farePointAmount) {
        this.farePointAmount = farePointAmount;
    }


    /**
     * Gets the availableCount value for this DateFlightLowFare.
     * 
     * @return availableCount
     */
    public java.lang.Short getAvailableCount() {
        return availableCount;
    }


    /**
     * Sets the availableCount value for this DateFlightLowFare.
     * 
     * @param availableCount
     */
    public void setAvailableCount(java.lang.Short availableCount) {
        this.availableCount = availableCount;
    }


    /**
     * Gets the dateFlightLegList value for this DateFlightLowFare.
     * 
     * @return dateFlightLegList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLeg[] getDateFlightLegList() {
        return dateFlightLegList;
    }


    /**
     * Sets the dateFlightLegList value for this DateFlightLowFare.
     * 
     * @param dateFlightLegList
     */
    public void setDateFlightLegList(com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLeg[] dateFlightLegList) {
        this.dateFlightLegList = dateFlightLegList;
    }


    /**
     * Gets the alternateLowFareList value for this DateFlightLowFare.
     * 
     * @return alternateLowFareList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare[] getAlternateLowFareList() {
        return alternateLowFareList;
    }


    /**
     * Sets the alternateLowFareList value for this DateFlightLowFare.
     * 
     * @param alternateLowFareList
     */
    public void setAlternateLowFareList(com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare[] alternateLowFareList) {
        this.alternateLowFareList = alternateLowFareList;
    }


    /**
     * Gets the dateFlightFaresList value for this DateFlightLowFare.
     * 
     * @return dateFlightFaresList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFares[] getDateFlightFaresList() {
        return dateFlightFaresList;
    }


    /**
     * Sets the dateFlightFaresList value for this DateFlightLowFare.
     * 
     * @param dateFlightFaresList
     */
    public void setDateFlightFaresList(com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFares[] dateFlightFaresList) {
        this.dateFlightFaresList = dateFlightFaresList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DateFlightLowFare)) return false;
        DateFlightLowFare other = (DateFlightLowFare) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fareAmount==null && other.getFareAmount()==null) || 
             (this.fareAmount!=null &&
              this.fareAmount.equals(other.getFareAmount()))) &&
            ((this.taxesAndFeesAmount==null && other.getTaxesAndFeesAmount()==null) || 
             (this.taxesAndFeesAmount!=null &&
              this.taxesAndFeesAmount.equals(other.getTaxesAndFeesAmount()))) &&
            ((this.STA==null && other.getSTA()==null) || 
             (this.STA!=null &&
              this.STA.equals(other.getSTA()))) &&
            ((this.STD==null && other.getSTD()==null) || 
             (this.STD!=null &&
              this.STD.equals(other.getSTD()))) &&
            ((this.farePointAmount==null && other.getFarePointAmount()==null) || 
             (this.farePointAmount!=null &&
              this.farePointAmount.equals(other.getFarePointAmount()))) &&
            ((this.availableCount==null && other.getAvailableCount()==null) || 
             (this.availableCount!=null &&
              this.availableCount.equals(other.getAvailableCount()))) &&
            ((this.dateFlightLegList==null && other.getDateFlightLegList()==null) || 
             (this.dateFlightLegList!=null &&
              java.util.Arrays.equals(this.dateFlightLegList, other.getDateFlightLegList()))) &&
            ((this.alternateLowFareList==null && other.getAlternateLowFareList()==null) || 
             (this.alternateLowFareList!=null &&
              java.util.Arrays.equals(this.alternateLowFareList, other.getAlternateLowFareList()))) &&
            ((this.dateFlightFaresList==null && other.getDateFlightFaresList()==null) || 
             (this.dateFlightFaresList!=null &&
              java.util.Arrays.equals(this.dateFlightFaresList, other.getDateFlightFaresList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFareAmount() != null) {
            _hashCode += getFareAmount().hashCode();
        }
        if (getTaxesAndFeesAmount() != null) {
            _hashCode += getTaxesAndFeesAmount().hashCode();
        }
        if (getSTA() != null) {
            _hashCode += getSTA().hashCode();
        }
        if (getSTD() != null) {
            _hashCode += getSTD().hashCode();
        }
        if (getFarePointAmount() != null) {
            _hashCode += getFarePointAmount().hashCode();
        }
        if (getAvailableCount() != null) {
            _hashCode += getAvailableCount().hashCode();
        }
        if (getDateFlightLegList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDateFlightLegList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDateFlightLegList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAlternateLowFareList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAlternateLowFareList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAlternateLowFareList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDateFlightFaresList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDateFlightFaresList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDateFlightFaresList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DateFlightLowFare.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLowFare"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxesAndFeesAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TaxesAndFeesAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STA");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("farePointAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FarePointAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailableCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateFlightLegList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLegList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLeg"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLeg"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternateLowFareList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFareList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFare"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFare"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateFlightFaresList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFaresList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFares"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFares"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
