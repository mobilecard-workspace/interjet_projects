/**
 * SellFeeRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SellFeeRequestData  implements java.io.Serializable {
    private java.lang.String feeCode;

    private java.lang.String originatingStationCode;

    private java.lang.String collectedCurrencyCode;

    private java.lang.Long passengerNumber;

    private java.lang.String note;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellFeeType sellFeeType;

    public SellFeeRequestData() {
    }

    public SellFeeRequestData(
           java.lang.String feeCode,
           java.lang.String originatingStationCode,
           java.lang.String collectedCurrencyCode,
           java.lang.Long passengerNumber,
           java.lang.String note,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellFeeType sellFeeType) {
           this.feeCode = feeCode;
           this.originatingStationCode = originatingStationCode;
           this.collectedCurrencyCode = collectedCurrencyCode;
           this.passengerNumber = passengerNumber;
           this.note = note;
           this.sellFeeType = sellFeeType;
    }


    /**
     * Gets the feeCode value for this SellFeeRequestData.
     * 
     * @return feeCode
     */
    public java.lang.String getFeeCode() {
        return feeCode;
    }


    /**
     * Sets the feeCode value for this SellFeeRequestData.
     * 
     * @param feeCode
     */
    public void setFeeCode(java.lang.String feeCode) {
        this.feeCode = feeCode;
    }


    /**
     * Gets the originatingStationCode value for this SellFeeRequestData.
     * 
     * @return originatingStationCode
     */
    public java.lang.String getOriginatingStationCode() {
        return originatingStationCode;
    }


    /**
     * Sets the originatingStationCode value for this SellFeeRequestData.
     * 
     * @param originatingStationCode
     */
    public void setOriginatingStationCode(java.lang.String originatingStationCode) {
        this.originatingStationCode = originatingStationCode;
    }


    /**
     * Gets the collectedCurrencyCode value for this SellFeeRequestData.
     * 
     * @return collectedCurrencyCode
     */
    public java.lang.String getCollectedCurrencyCode() {
        return collectedCurrencyCode;
    }


    /**
     * Sets the collectedCurrencyCode value for this SellFeeRequestData.
     * 
     * @param collectedCurrencyCode
     */
    public void setCollectedCurrencyCode(java.lang.String collectedCurrencyCode) {
        this.collectedCurrencyCode = collectedCurrencyCode;
    }


    /**
     * Gets the passengerNumber value for this SellFeeRequestData.
     * 
     * @return passengerNumber
     */
    public java.lang.Long getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this SellFeeRequestData.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Long passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the note value for this SellFeeRequestData.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this SellFeeRequestData.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }


    /**
     * Gets the sellFeeType value for this SellFeeRequestData.
     * 
     * @return sellFeeType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellFeeType getSellFeeType() {
        return sellFeeType;
    }


    /**
     * Sets the sellFeeType value for this SellFeeRequestData.
     * 
     * @param sellFeeType
     */
    public void setSellFeeType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellFeeType sellFeeType) {
        this.sellFeeType = sellFeeType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SellFeeRequestData)) return false;
        SellFeeRequestData other = (SellFeeRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.feeCode==null && other.getFeeCode()==null) || 
             (this.feeCode!=null &&
              this.feeCode.equals(other.getFeeCode()))) &&
            ((this.originatingStationCode==null && other.getOriginatingStationCode()==null) || 
             (this.originatingStationCode!=null &&
              this.originatingStationCode.equals(other.getOriginatingStationCode()))) &&
            ((this.collectedCurrencyCode==null && other.getCollectedCurrencyCode()==null) || 
             (this.collectedCurrencyCode!=null &&
              this.collectedCurrencyCode.equals(other.getCollectedCurrencyCode()))) &&
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote()))) &&
            ((this.sellFeeType==null && other.getSellFeeType()==null) || 
             (this.sellFeeType!=null &&
              this.sellFeeType.equals(other.getSellFeeType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFeeCode() != null) {
            _hashCode += getFeeCode().hashCode();
        }
        if (getOriginatingStationCode() != null) {
            _hashCode += getOriginatingStationCode().hashCode();
        }
        if (getCollectedCurrencyCode() != null) {
            _hashCode += getCollectedCurrencyCode().hashCode();
        }
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        if (getSellFeeType() != null) {
            _hashCode += getSellFeeType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SellFeeRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellFeeRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originatingStationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OriginatingStationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellFeeType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellFeeType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SellFeeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
