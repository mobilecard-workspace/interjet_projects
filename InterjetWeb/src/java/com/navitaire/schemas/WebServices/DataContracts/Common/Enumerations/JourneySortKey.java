/**
 * JourneySortKey.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class JourneySortKey implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected JourneySortKey(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _ServiceType = "ServiceType";
    public static final java.lang.String _ShortestTravelTime = "ShortestTravelTime";
    public static final java.lang.String _LowestFare = "LowestFare";
    public static final java.lang.String _HighestFare = "HighestFare";
    public static final java.lang.String _EarliestDeparture = "EarliestDeparture";
    public static final java.lang.String _LatestDeparture = "LatestDeparture";
    public static final java.lang.String _EarliestArrival = "EarliestArrival";
    public static final java.lang.String _LatestArrival = "LatestArrival";
    public static final java.lang.String _NoSort = "NoSort";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final JourneySortKey ServiceType = new JourneySortKey(_ServiceType);
    public static final JourneySortKey ShortestTravelTime = new JourneySortKey(_ShortestTravelTime);
    public static final JourneySortKey LowestFare = new JourneySortKey(_LowestFare);
    public static final JourneySortKey HighestFare = new JourneySortKey(_HighestFare);
    public static final JourneySortKey EarliestDeparture = new JourneySortKey(_EarliestDeparture);
    public static final JourneySortKey LatestDeparture = new JourneySortKey(_LatestDeparture);
    public static final JourneySortKey EarliestArrival = new JourneySortKey(_EarliestArrival);
    public static final JourneySortKey LatestArrival = new JourneySortKey(_LatestArrival);
    public static final JourneySortKey NoSort = new JourneySortKey(_NoSort);
    public static final JourneySortKey Unmapped = new JourneySortKey(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static JourneySortKey fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        JourneySortKey enumeration = (JourneySortKey)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static JourneySortKey fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(JourneySortKey.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "JourneySortKey"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
