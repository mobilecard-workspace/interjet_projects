/**
 * DivideRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DivideRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DivideAction divideAction;

    private java.lang.String sourceRecordLocator;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] crsRecordLocators;

    private short[] passengerNumbers;

    private java.lang.Boolean autoDividePayments;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingPaymentTransfer[] bookingPaymentTransfers;

    private java.lang.Boolean queueNegativeBalancePNRs;

    private java.lang.Boolean addComments;

    private java.lang.String receivedBy;

    private java.lang.Boolean overrideRestrictions;

    private java.lang.Boolean cancelChildPNRJourneys;

    private java.lang.String parentEMail;

    private java.lang.String childEMail;

    public DivideRequestData() {
    }

    public DivideRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DivideAction divideAction,
           java.lang.String sourceRecordLocator,
           com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] crsRecordLocators,
           short[] passengerNumbers,
           java.lang.Boolean autoDividePayments,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingPaymentTransfer[] bookingPaymentTransfers,
           java.lang.Boolean queueNegativeBalancePNRs,
           java.lang.Boolean addComments,
           java.lang.String receivedBy,
           java.lang.Boolean overrideRestrictions,
           java.lang.Boolean cancelChildPNRJourneys,
           java.lang.String parentEMail,
           java.lang.String childEMail) {
           this.divideAction = divideAction;
           this.sourceRecordLocator = sourceRecordLocator;
           this.crsRecordLocators = crsRecordLocators;
           this.passengerNumbers = passengerNumbers;
           this.autoDividePayments = autoDividePayments;
           this.bookingPaymentTransfers = bookingPaymentTransfers;
           this.queueNegativeBalancePNRs = queueNegativeBalancePNRs;
           this.addComments = addComments;
           this.receivedBy = receivedBy;
           this.overrideRestrictions = overrideRestrictions;
           this.cancelChildPNRJourneys = cancelChildPNRJourneys;
           this.parentEMail = parentEMail;
           this.childEMail = childEMail;
    }


    /**
     * Gets the divideAction value for this DivideRequestData.
     * 
     * @return divideAction
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DivideAction getDivideAction() {
        return divideAction;
    }


    /**
     * Sets the divideAction value for this DivideRequestData.
     * 
     * @param divideAction
     */
    public void setDivideAction(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DivideAction divideAction) {
        this.divideAction = divideAction;
    }


    /**
     * Gets the sourceRecordLocator value for this DivideRequestData.
     * 
     * @return sourceRecordLocator
     */
    public java.lang.String getSourceRecordLocator() {
        return sourceRecordLocator;
    }


    /**
     * Sets the sourceRecordLocator value for this DivideRequestData.
     * 
     * @param sourceRecordLocator
     */
    public void setSourceRecordLocator(java.lang.String sourceRecordLocator) {
        this.sourceRecordLocator = sourceRecordLocator;
    }


    /**
     * Gets the crsRecordLocators value for this DivideRequestData.
     * 
     * @return crsRecordLocators
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] getCrsRecordLocators() {
        return crsRecordLocators;
    }


    /**
     * Sets the crsRecordLocators value for this DivideRequestData.
     * 
     * @param crsRecordLocators
     */
    public void setCrsRecordLocators(com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[] crsRecordLocators) {
        this.crsRecordLocators = crsRecordLocators;
    }


    /**
     * Gets the passengerNumbers value for this DivideRequestData.
     * 
     * @return passengerNumbers
     */
    public short[] getPassengerNumbers() {
        return passengerNumbers;
    }


    /**
     * Sets the passengerNumbers value for this DivideRequestData.
     * 
     * @param passengerNumbers
     */
    public void setPassengerNumbers(short[] passengerNumbers) {
        this.passengerNumbers = passengerNumbers;
    }


    /**
     * Gets the autoDividePayments value for this DivideRequestData.
     * 
     * @return autoDividePayments
     */
    public java.lang.Boolean getAutoDividePayments() {
        return autoDividePayments;
    }


    /**
     * Sets the autoDividePayments value for this DivideRequestData.
     * 
     * @param autoDividePayments
     */
    public void setAutoDividePayments(java.lang.Boolean autoDividePayments) {
        this.autoDividePayments = autoDividePayments;
    }


    /**
     * Gets the bookingPaymentTransfers value for this DivideRequestData.
     * 
     * @return bookingPaymentTransfers
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingPaymentTransfer[] getBookingPaymentTransfers() {
        return bookingPaymentTransfers;
    }


    /**
     * Sets the bookingPaymentTransfers value for this DivideRequestData.
     * 
     * @param bookingPaymentTransfers
     */
    public void setBookingPaymentTransfers(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingPaymentTransfer[] bookingPaymentTransfers) {
        this.bookingPaymentTransfers = bookingPaymentTransfers;
    }


    /**
     * Gets the queueNegativeBalancePNRs value for this DivideRequestData.
     * 
     * @return queueNegativeBalancePNRs
     */
    public java.lang.Boolean getQueueNegativeBalancePNRs() {
        return queueNegativeBalancePNRs;
    }


    /**
     * Sets the queueNegativeBalancePNRs value for this DivideRequestData.
     * 
     * @param queueNegativeBalancePNRs
     */
    public void setQueueNegativeBalancePNRs(java.lang.Boolean queueNegativeBalancePNRs) {
        this.queueNegativeBalancePNRs = queueNegativeBalancePNRs;
    }


    /**
     * Gets the addComments value for this DivideRequestData.
     * 
     * @return addComments
     */
    public java.lang.Boolean getAddComments() {
        return addComments;
    }


    /**
     * Sets the addComments value for this DivideRequestData.
     * 
     * @param addComments
     */
    public void setAddComments(java.lang.Boolean addComments) {
        this.addComments = addComments;
    }


    /**
     * Gets the receivedBy value for this DivideRequestData.
     * 
     * @return receivedBy
     */
    public java.lang.String getReceivedBy() {
        return receivedBy;
    }


    /**
     * Sets the receivedBy value for this DivideRequestData.
     * 
     * @param receivedBy
     */
    public void setReceivedBy(java.lang.String receivedBy) {
        this.receivedBy = receivedBy;
    }


    /**
     * Gets the overrideRestrictions value for this DivideRequestData.
     * 
     * @return overrideRestrictions
     */
    public java.lang.Boolean getOverrideRestrictions() {
        return overrideRestrictions;
    }


    /**
     * Sets the overrideRestrictions value for this DivideRequestData.
     * 
     * @param overrideRestrictions
     */
    public void setOverrideRestrictions(java.lang.Boolean overrideRestrictions) {
        this.overrideRestrictions = overrideRestrictions;
    }


    /**
     * Gets the cancelChildPNRJourneys value for this DivideRequestData.
     * 
     * @return cancelChildPNRJourneys
     */
    public java.lang.Boolean getCancelChildPNRJourneys() {
        return cancelChildPNRJourneys;
    }


    /**
     * Sets the cancelChildPNRJourneys value for this DivideRequestData.
     * 
     * @param cancelChildPNRJourneys
     */
    public void setCancelChildPNRJourneys(java.lang.Boolean cancelChildPNRJourneys) {
        this.cancelChildPNRJourneys = cancelChildPNRJourneys;
    }


    /**
     * Gets the parentEMail value for this DivideRequestData.
     * 
     * @return parentEMail
     */
    public java.lang.String getParentEMail() {
        return parentEMail;
    }


    /**
     * Sets the parentEMail value for this DivideRequestData.
     * 
     * @param parentEMail
     */
    public void setParentEMail(java.lang.String parentEMail) {
        this.parentEMail = parentEMail;
    }


    /**
     * Gets the childEMail value for this DivideRequestData.
     * 
     * @return childEMail
     */
    public java.lang.String getChildEMail() {
        return childEMail;
    }


    /**
     * Sets the childEMail value for this DivideRequestData.
     * 
     * @param childEMail
     */
    public void setChildEMail(java.lang.String childEMail) {
        this.childEMail = childEMail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DivideRequestData)) return false;
        DivideRequestData other = (DivideRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.divideAction==null && other.getDivideAction()==null) || 
             (this.divideAction!=null &&
              this.divideAction.equals(other.getDivideAction()))) &&
            ((this.sourceRecordLocator==null && other.getSourceRecordLocator()==null) || 
             (this.sourceRecordLocator!=null &&
              this.sourceRecordLocator.equals(other.getSourceRecordLocator()))) &&
            ((this.crsRecordLocators==null && other.getCrsRecordLocators()==null) || 
             (this.crsRecordLocators!=null &&
              java.util.Arrays.equals(this.crsRecordLocators, other.getCrsRecordLocators()))) &&
            ((this.passengerNumbers==null && other.getPassengerNumbers()==null) || 
             (this.passengerNumbers!=null &&
              java.util.Arrays.equals(this.passengerNumbers, other.getPassengerNumbers()))) &&
            ((this.autoDividePayments==null && other.getAutoDividePayments()==null) || 
             (this.autoDividePayments!=null &&
              this.autoDividePayments.equals(other.getAutoDividePayments()))) &&
            ((this.bookingPaymentTransfers==null && other.getBookingPaymentTransfers()==null) || 
             (this.bookingPaymentTransfers!=null &&
              java.util.Arrays.equals(this.bookingPaymentTransfers, other.getBookingPaymentTransfers()))) &&
            ((this.queueNegativeBalancePNRs==null && other.getQueueNegativeBalancePNRs()==null) || 
             (this.queueNegativeBalancePNRs!=null &&
              this.queueNegativeBalancePNRs.equals(other.getQueueNegativeBalancePNRs()))) &&
            ((this.addComments==null && other.getAddComments()==null) || 
             (this.addComments!=null &&
              this.addComments.equals(other.getAddComments()))) &&
            ((this.receivedBy==null && other.getReceivedBy()==null) || 
             (this.receivedBy!=null &&
              this.receivedBy.equals(other.getReceivedBy()))) &&
            ((this.overrideRestrictions==null && other.getOverrideRestrictions()==null) || 
             (this.overrideRestrictions!=null &&
              this.overrideRestrictions.equals(other.getOverrideRestrictions()))) &&
            ((this.cancelChildPNRJourneys==null && other.getCancelChildPNRJourneys()==null) || 
             (this.cancelChildPNRJourneys!=null &&
              this.cancelChildPNRJourneys.equals(other.getCancelChildPNRJourneys()))) &&
            ((this.parentEMail==null && other.getParentEMail()==null) || 
             (this.parentEMail!=null &&
              this.parentEMail.equals(other.getParentEMail()))) &&
            ((this.childEMail==null && other.getChildEMail()==null) || 
             (this.childEMail!=null &&
              this.childEMail.equals(other.getChildEMail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDivideAction() != null) {
            _hashCode += getDivideAction().hashCode();
        }
        if (getSourceRecordLocator() != null) {
            _hashCode += getSourceRecordLocator().hashCode();
        }
        if (getCrsRecordLocators() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCrsRecordLocators());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCrsRecordLocators(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerNumbers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerNumbers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerNumbers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAutoDividePayments() != null) {
            _hashCode += getAutoDividePayments().hashCode();
        }
        if (getBookingPaymentTransfers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingPaymentTransfers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingPaymentTransfers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getQueueNegativeBalancePNRs() != null) {
            _hashCode += getQueueNegativeBalancePNRs().hashCode();
        }
        if (getAddComments() != null) {
            _hashCode += getAddComments().hashCode();
        }
        if (getReceivedBy() != null) {
            _hashCode += getReceivedBy().hashCode();
        }
        if (getOverrideRestrictions() != null) {
            _hashCode += getOverrideRestrictions().hashCode();
        }
        if (getCancelChildPNRJourneys() != null) {
            _hashCode += getCancelChildPNRJourneys().hashCode();
        }
        if (getParentEMail() != null) {
            _hashCode += getParentEMail().hashCode();
        }
        if (getChildEMail() != null) {
            _hashCode += getChildEMail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DivideRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DivideRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("divideAction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DivideAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DivideAction"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("crsRecordLocators");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CrsRecordLocators"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumbers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumbers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "short"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("autoDividePayments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AutoDividePayments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingPaymentTransfers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingPaymentTransfers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingPaymentTransfer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingPaymentTransfer"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queueNegativeBalancePNRs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QueueNegativeBalancePNRs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addComments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AddComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receivedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overrideRestrictions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OverrideRestrictions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelChildPNRJourneys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelChildPNRJourneys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentEMail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ParentEMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("childEMail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChildEMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
