/**
 * LegNest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class LegNest  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Short classNest;

    private java.lang.Short lid;

    private java.lang.Short adjustedCapacity;

    private java.lang.String travelClassCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.NestType nestType;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.LegClass[] legClasses;

    public LegNest() {
    }

    public LegNest(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Short classNest,
           java.lang.Short lid,
           java.lang.Short adjustedCapacity,
           java.lang.String travelClassCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.NestType nestType,
           com.navitaire.schemas.WebServices.DataContracts.Booking.LegClass[] legClasses) {
        super(
            state);
        this.classNest = classNest;
        this.lid = lid;
        this.adjustedCapacity = adjustedCapacity;
        this.travelClassCode = travelClassCode;
        this.nestType = nestType;
        this.legClasses = legClasses;
    }


    /**
     * Gets the classNest value for this LegNest.
     * 
     * @return classNest
     */
    public java.lang.Short getClassNest() {
        return classNest;
    }


    /**
     * Sets the classNest value for this LegNest.
     * 
     * @param classNest
     */
    public void setClassNest(java.lang.Short classNest) {
        this.classNest = classNest;
    }


    /**
     * Gets the lid value for this LegNest.
     * 
     * @return lid
     */
    public java.lang.Short getLid() {
        return lid;
    }


    /**
     * Sets the lid value for this LegNest.
     * 
     * @param lid
     */
    public void setLid(java.lang.Short lid) {
        this.lid = lid;
    }


    /**
     * Gets the adjustedCapacity value for this LegNest.
     * 
     * @return adjustedCapacity
     */
    public java.lang.Short getAdjustedCapacity() {
        return adjustedCapacity;
    }


    /**
     * Sets the adjustedCapacity value for this LegNest.
     * 
     * @param adjustedCapacity
     */
    public void setAdjustedCapacity(java.lang.Short adjustedCapacity) {
        this.adjustedCapacity = adjustedCapacity;
    }


    /**
     * Gets the travelClassCode value for this LegNest.
     * 
     * @return travelClassCode
     */
    public java.lang.String getTravelClassCode() {
        return travelClassCode;
    }


    /**
     * Sets the travelClassCode value for this LegNest.
     * 
     * @param travelClassCode
     */
    public void setTravelClassCode(java.lang.String travelClassCode) {
        this.travelClassCode = travelClassCode;
    }


    /**
     * Gets the nestType value for this LegNest.
     * 
     * @return nestType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.NestType getNestType() {
        return nestType;
    }


    /**
     * Sets the nestType value for this LegNest.
     * 
     * @param nestType
     */
    public void setNestType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.NestType nestType) {
        this.nestType = nestType;
    }


    /**
     * Gets the legClasses value for this LegNest.
     * 
     * @return legClasses
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.LegClass[] getLegClasses() {
        return legClasses;
    }


    /**
     * Sets the legClasses value for this LegNest.
     * 
     * @param legClasses
     */
    public void setLegClasses(com.navitaire.schemas.WebServices.DataContracts.Booking.LegClass[] legClasses) {
        this.legClasses = legClasses;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LegNest)) return false;
        LegNest other = (LegNest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.classNest==null && other.getClassNest()==null) || 
             (this.classNest!=null &&
              this.classNest.equals(other.getClassNest()))) &&
            ((this.lid==null && other.getLid()==null) || 
             (this.lid!=null &&
              this.lid.equals(other.getLid()))) &&
            ((this.adjustedCapacity==null && other.getAdjustedCapacity()==null) || 
             (this.adjustedCapacity!=null &&
              this.adjustedCapacity.equals(other.getAdjustedCapacity()))) &&
            ((this.travelClassCode==null && other.getTravelClassCode()==null) || 
             (this.travelClassCode!=null &&
              this.travelClassCode.equals(other.getTravelClassCode()))) &&
            ((this.nestType==null && other.getNestType()==null) || 
             (this.nestType!=null &&
              this.nestType.equals(other.getNestType()))) &&
            ((this.legClasses==null && other.getLegClasses()==null) || 
             (this.legClasses!=null &&
              java.util.Arrays.equals(this.legClasses, other.getLegClasses())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getClassNest() != null) {
            _hashCode += getClassNest().hashCode();
        }
        if (getLid() != null) {
            _hashCode += getLid().hashCode();
        }
        if (getAdjustedCapacity() != null) {
            _hashCode += getAdjustedCapacity().hashCode();
        }
        if (getTravelClassCode() != null) {
            _hashCode += getTravelClassCode().hashCode();
        }
        if (getNestType() != null) {
            _hashCode += getNestType().hashCode();
        }
        if (getLegClasses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLegClasses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLegClasses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LegNest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegNest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classNest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ClassNest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Lid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adjustedCapacity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AdjustedCapacity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("travelClassCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelClassCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nestType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NestType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "NestType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("legClasses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegClasses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegClass"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegClass"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
