/**
 * SeatSellRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SeatSellRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.UnitHoldType blockType;

    private java.lang.Boolean sameSeatRequiredOnThruLegs;

    private java.lang.Boolean assignNoSeatIfAlreadyTaken;

    private java.lang.Boolean allowSeatSwappingInPNR;

    private java.lang.Boolean waiveFee;

    private java.lang.Boolean replaceSpecificSeatRequest;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAssignmentMode seatAssignmentMode;

    private java.lang.Boolean ignoreSeatSSRs;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSeatRequest[] segmentSeatRequests;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation[] equipmentDeviations;

    private java.lang.String collectedCurrencyCode;

    public SeatSellRequest() {
    }

    public SeatSellRequest(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.UnitHoldType blockType,
           java.lang.Boolean sameSeatRequiredOnThruLegs,
           java.lang.Boolean assignNoSeatIfAlreadyTaken,
           java.lang.Boolean allowSeatSwappingInPNR,
           java.lang.Boolean waiveFee,
           java.lang.Boolean replaceSpecificSeatRequest,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAssignmentMode seatAssignmentMode,
           java.lang.Boolean ignoreSeatSSRs,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSeatRequest[] segmentSeatRequests,
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation[] equipmentDeviations,
           java.lang.String collectedCurrencyCode) {
           this.blockType = blockType;
           this.sameSeatRequiredOnThruLegs = sameSeatRequiredOnThruLegs;
           this.assignNoSeatIfAlreadyTaken = assignNoSeatIfAlreadyTaken;
           this.allowSeatSwappingInPNR = allowSeatSwappingInPNR;
           this.waiveFee = waiveFee;
           this.replaceSpecificSeatRequest = replaceSpecificSeatRequest;
           this.seatAssignmentMode = seatAssignmentMode;
           this.ignoreSeatSSRs = ignoreSeatSSRs;
           this.segmentSeatRequests = segmentSeatRequests;
           this.equipmentDeviations = equipmentDeviations;
           this.collectedCurrencyCode = collectedCurrencyCode;
    }


    /**
     * Gets the blockType value for this SeatSellRequest.
     * 
     * @return blockType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.UnitHoldType getBlockType() {
        return blockType;
    }


    /**
     * Sets the blockType value for this SeatSellRequest.
     * 
     * @param blockType
     */
    public void setBlockType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.UnitHoldType blockType) {
        this.blockType = blockType;
    }


    /**
     * Gets the sameSeatRequiredOnThruLegs value for this SeatSellRequest.
     * 
     * @return sameSeatRequiredOnThruLegs
     */
    public java.lang.Boolean getSameSeatRequiredOnThruLegs() {
        return sameSeatRequiredOnThruLegs;
    }


    /**
     * Sets the sameSeatRequiredOnThruLegs value for this SeatSellRequest.
     * 
     * @param sameSeatRequiredOnThruLegs
     */
    public void setSameSeatRequiredOnThruLegs(java.lang.Boolean sameSeatRequiredOnThruLegs) {
        this.sameSeatRequiredOnThruLegs = sameSeatRequiredOnThruLegs;
    }


    /**
     * Gets the assignNoSeatIfAlreadyTaken value for this SeatSellRequest.
     * 
     * @return assignNoSeatIfAlreadyTaken
     */
    public java.lang.Boolean getAssignNoSeatIfAlreadyTaken() {
        return assignNoSeatIfAlreadyTaken;
    }


    /**
     * Sets the assignNoSeatIfAlreadyTaken value for this SeatSellRequest.
     * 
     * @param assignNoSeatIfAlreadyTaken
     */
    public void setAssignNoSeatIfAlreadyTaken(java.lang.Boolean assignNoSeatIfAlreadyTaken) {
        this.assignNoSeatIfAlreadyTaken = assignNoSeatIfAlreadyTaken;
    }


    /**
     * Gets the allowSeatSwappingInPNR value for this SeatSellRequest.
     * 
     * @return allowSeatSwappingInPNR
     */
    public java.lang.Boolean getAllowSeatSwappingInPNR() {
        return allowSeatSwappingInPNR;
    }


    /**
     * Sets the allowSeatSwappingInPNR value for this SeatSellRequest.
     * 
     * @param allowSeatSwappingInPNR
     */
    public void setAllowSeatSwappingInPNR(java.lang.Boolean allowSeatSwappingInPNR) {
        this.allowSeatSwappingInPNR = allowSeatSwappingInPNR;
    }


    /**
     * Gets the waiveFee value for this SeatSellRequest.
     * 
     * @return waiveFee
     */
    public java.lang.Boolean getWaiveFee() {
        return waiveFee;
    }


    /**
     * Sets the waiveFee value for this SeatSellRequest.
     * 
     * @param waiveFee
     */
    public void setWaiveFee(java.lang.Boolean waiveFee) {
        this.waiveFee = waiveFee;
    }


    /**
     * Gets the replaceSpecificSeatRequest value for this SeatSellRequest.
     * 
     * @return replaceSpecificSeatRequest
     */
    public java.lang.Boolean getReplaceSpecificSeatRequest() {
        return replaceSpecificSeatRequest;
    }


    /**
     * Sets the replaceSpecificSeatRequest value for this SeatSellRequest.
     * 
     * @param replaceSpecificSeatRequest
     */
    public void setReplaceSpecificSeatRequest(java.lang.Boolean replaceSpecificSeatRequest) {
        this.replaceSpecificSeatRequest = replaceSpecificSeatRequest;
    }


    /**
     * Gets the seatAssignmentMode value for this SeatSellRequest.
     * 
     * @return seatAssignmentMode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAssignmentMode getSeatAssignmentMode() {
        return seatAssignmentMode;
    }


    /**
     * Sets the seatAssignmentMode value for this SeatSellRequest.
     * 
     * @param seatAssignmentMode
     */
    public void setSeatAssignmentMode(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAssignmentMode seatAssignmentMode) {
        this.seatAssignmentMode = seatAssignmentMode;
    }


    /**
     * Gets the ignoreSeatSSRs value for this SeatSellRequest.
     * 
     * @return ignoreSeatSSRs
     */
    public java.lang.Boolean getIgnoreSeatSSRs() {
        return ignoreSeatSSRs;
    }


    /**
     * Sets the ignoreSeatSSRs value for this SeatSellRequest.
     * 
     * @param ignoreSeatSSRs
     */
    public void setIgnoreSeatSSRs(java.lang.Boolean ignoreSeatSSRs) {
        this.ignoreSeatSSRs = ignoreSeatSSRs;
    }


    /**
     * Gets the segmentSeatRequests value for this SeatSellRequest.
     * 
     * @return segmentSeatRequests
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSeatRequest[] getSegmentSeatRequests() {
        return segmentSeatRequests;
    }


    /**
     * Sets the segmentSeatRequests value for this SeatSellRequest.
     * 
     * @param segmentSeatRequests
     */
    public void setSegmentSeatRequests(com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSeatRequest[] segmentSeatRequests) {
        this.segmentSeatRequests = segmentSeatRequests;
    }


    /**
     * Gets the equipmentDeviations value for this SeatSellRequest.
     * 
     * @return equipmentDeviations
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation[] getEquipmentDeviations() {
        return equipmentDeviations;
    }


    /**
     * Sets the equipmentDeviations value for this SeatSellRequest.
     * 
     * @param equipmentDeviations
     */
    public void setEquipmentDeviations(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation[] equipmentDeviations) {
        this.equipmentDeviations = equipmentDeviations;
    }


    /**
     * Gets the collectedCurrencyCode value for this SeatSellRequest.
     * 
     * @return collectedCurrencyCode
     */
    public java.lang.String getCollectedCurrencyCode() {
        return collectedCurrencyCode;
    }


    /**
     * Sets the collectedCurrencyCode value for this SeatSellRequest.
     * 
     * @param collectedCurrencyCode
     */
    public void setCollectedCurrencyCode(java.lang.String collectedCurrencyCode) {
        this.collectedCurrencyCode = collectedCurrencyCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SeatSellRequest)) return false;
        SeatSellRequest other = (SeatSellRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.blockType==null && other.getBlockType()==null) || 
             (this.blockType!=null &&
              this.blockType.equals(other.getBlockType()))) &&
            ((this.sameSeatRequiredOnThruLegs==null && other.getSameSeatRequiredOnThruLegs()==null) || 
             (this.sameSeatRequiredOnThruLegs!=null &&
              this.sameSeatRequiredOnThruLegs.equals(other.getSameSeatRequiredOnThruLegs()))) &&
            ((this.assignNoSeatIfAlreadyTaken==null && other.getAssignNoSeatIfAlreadyTaken()==null) || 
             (this.assignNoSeatIfAlreadyTaken!=null &&
              this.assignNoSeatIfAlreadyTaken.equals(other.getAssignNoSeatIfAlreadyTaken()))) &&
            ((this.allowSeatSwappingInPNR==null && other.getAllowSeatSwappingInPNR()==null) || 
             (this.allowSeatSwappingInPNR!=null &&
              this.allowSeatSwappingInPNR.equals(other.getAllowSeatSwappingInPNR()))) &&
            ((this.waiveFee==null && other.getWaiveFee()==null) || 
             (this.waiveFee!=null &&
              this.waiveFee.equals(other.getWaiveFee()))) &&
            ((this.replaceSpecificSeatRequest==null && other.getReplaceSpecificSeatRequest()==null) || 
             (this.replaceSpecificSeatRequest!=null &&
              this.replaceSpecificSeatRequest.equals(other.getReplaceSpecificSeatRequest()))) &&
            ((this.seatAssignmentMode==null && other.getSeatAssignmentMode()==null) || 
             (this.seatAssignmentMode!=null &&
              this.seatAssignmentMode.equals(other.getSeatAssignmentMode()))) &&
            ((this.ignoreSeatSSRs==null && other.getIgnoreSeatSSRs()==null) || 
             (this.ignoreSeatSSRs!=null &&
              this.ignoreSeatSSRs.equals(other.getIgnoreSeatSSRs()))) &&
            ((this.segmentSeatRequests==null && other.getSegmentSeatRequests()==null) || 
             (this.segmentSeatRequests!=null &&
              java.util.Arrays.equals(this.segmentSeatRequests, other.getSegmentSeatRequests()))) &&
            ((this.equipmentDeviations==null && other.getEquipmentDeviations()==null) || 
             (this.equipmentDeviations!=null &&
              java.util.Arrays.equals(this.equipmentDeviations, other.getEquipmentDeviations()))) &&
            ((this.collectedCurrencyCode==null && other.getCollectedCurrencyCode()==null) || 
             (this.collectedCurrencyCode!=null &&
              this.collectedCurrencyCode.equals(other.getCollectedCurrencyCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBlockType() != null) {
            _hashCode += getBlockType().hashCode();
        }
        if (getSameSeatRequiredOnThruLegs() != null) {
            _hashCode += getSameSeatRequiredOnThruLegs().hashCode();
        }
        if (getAssignNoSeatIfAlreadyTaken() != null) {
            _hashCode += getAssignNoSeatIfAlreadyTaken().hashCode();
        }
        if (getAllowSeatSwappingInPNR() != null) {
            _hashCode += getAllowSeatSwappingInPNR().hashCode();
        }
        if (getWaiveFee() != null) {
            _hashCode += getWaiveFee().hashCode();
        }
        if (getReplaceSpecificSeatRequest() != null) {
            _hashCode += getReplaceSpecificSeatRequest().hashCode();
        }
        if (getSeatAssignmentMode() != null) {
            _hashCode += getSeatAssignmentMode().hashCode();
        }
        if (getIgnoreSeatSSRs() != null) {
            _hashCode += getIgnoreSeatSSRs().hashCode();
        }
        if (getSegmentSeatRequests() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegmentSeatRequests());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegmentSeatRequests(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEquipmentDeviations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEquipmentDeviations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEquipmentDeviations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCollectedCurrencyCode() != null) {
            _hashCode += getCollectedCurrencyCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SeatSellRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatSellRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("blockType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BlockType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "UnitHoldType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sameSeatRequiredOnThruLegs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SameSeatRequiredOnThruLegs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assignNoSeatIfAlreadyTaken");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AssignNoSeatIfAlreadyTaken"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowSeatSwappingInPNR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AllowSeatSwappingInPNR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waiveFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaiveFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("replaceSpecificSeatRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReplaceSpecificSeatRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatAssignmentMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAssignmentMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SeatAssignmentMode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ignoreSeatSSRs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IgnoreSeatSSRs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentSeatRequests");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSeatRequests"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSeatRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSeatRequest"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentDeviations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviation"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
