/**
 * FindBaggageEventResponseData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class FindBaggageEventResponseData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.BaggageEvent[] findBaggageEvents;

    public FindBaggageEventResponseData() {
    }

    public FindBaggageEventResponseData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.BaggageEvent[] findBaggageEvents) {
           this.findBaggageEvents = findBaggageEvents;
    }


    /**
     * Gets the findBaggageEvents value for this FindBaggageEventResponseData.
     * 
     * @return findBaggageEvents
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BaggageEvent[] getFindBaggageEvents() {
        return findBaggageEvents;
    }


    /**
     * Sets the findBaggageEvents value for this FindBaggageEventResponseData.
     * 
     * @param findBaggageEvents
     */
    public void setFindBaggageEvents(com.navitaire.schemas.WebServices.DataContracts.Booking.BaggageEvent[] findBaggageEvents) {
        this.findBaggageEvents = findBaggageEvents;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindBaggageEventResponseData)) return false;
        FindBaggageEventResponseData other = (FindBaggageEventResponseData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.findBaggageEvents==null && other.getFindBaggageEvents()==null) || 
             (this.findBaggageEvents!=null &&
              java.util.Arrays.equals(this.findBaggageEvents, other.getFindBaggageEvents())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFindBaggageEvents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFindBaggageEvents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFindBaggageEvents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindBaggageEventResponseData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBaggageEventResponseData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findBaggageEvents");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBaggageEvents"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageEvent"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageEvent"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
