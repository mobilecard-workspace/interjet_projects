/**
 * OrderPayment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class OrderPayment  implements java.io.Serializable {
    private java.lang.String currencyCode;

    private java.lang.String foreignCurrencyCode;

    private java.lang.String cultureCode;

    private java.lang.Integer paymentSequence;

    private java.lang.String paymentTypeCode;

    private java.lang.String description;

    private java.lang.String CCName;

    private java.lang.String CCNumber;

    private java.lang.Long accountNumberID;

    private java.lang.String CCExpiration;

    private java.lang.String CCCvv;

    private java.math.BigDecimal amount;

    private java.math.BigDecimal foreignAmount;

    private java.lang.String issueNumber;

    public OrderPayment() {
    }

    public OrderPayment(
           java.lang.String currencyCode,
           java.lang.String foreignCurrencyCode,
           java.lang.String cultureCode,
           java.lang.Integer paymentSequence,
           java.lang.String paymentTypeCode,
           java.lang.String description,
           java.lang.String CCName,
           java.lang.String CCNumber,
           java.lang.Long accountNumberID,
           java.lang.String CCExpiration,
           java.lang.String CCCvv,
           java.math.BigDecimal amount,
           java.math.BigDecimal foreignAmount,
           java.lang.String issueNumber) {
           this.currencyCode = currencyCode;
           this.foreignCurrencyCode = foreignCurrencyCode;
           this.cultureCode = cultureCode;
           this.paymentSequence = paymentSequence;
           this.paymentTypeCode = paymentTypeCode;
           this.description = description;
           this.CCName = CCName;
           this.CCNumber = CCNumber;
           this.accountNumberID = accountNumberID;
           this.CCExpiration = CCExpiration;
           this.CCCvv = CCCvv;
           this.amount = amount;
           this.foreignAmount = foreignAmount;
           this.issueNumber = issueNumber;
    }


    /**
     * Gets the currencyCode value for this OrderPayment.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this OrderPayment.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the foreignCurrencyCode value for this OrderPayment.
     * 
     * @return foreignCurrencyCode
     */
    public java.lang.String getForeignCurrencyCode() {
        return foreignCurrencyCode;
    }


    /**
     * Sets the foreignCurrencyCode value for this OrderPayment.
     * 
     * @param foreignCurrencyCode
     */
    public void setForeignCurrencyCode(java.lang.String foreignCurrencyCode) {
        this.foreignCurrencyCode = foreignCurrencyCode;
    }


    /**
     * Gets the cultureCode value for this OrderPayment.
     * 
     * @return cultureCode
     */
    public java.lang.String getCultureCode() {
        return cultureCode;
    }


    /**
     * Sets the cultureCode value for this OrderPayment.
     * 
     * @param cultureCode
     */
    public void setCultureCode(java.lang.String cultureCode) {
        this.cultureCode = cultureCode;
    }


    /**
     * Gets the paymentSequence value for this OrderPayment.
     * 
     * @return paymentSequence
     */
    public java.lang.Integer getPaymentSequence() {
        return paymentSequence;
    }


    /**
     * Sets the paymentSequence value for this OrderPayment.
     * 
     * @param paymentSequence
     */
    public void setPaymentSequence(java.lang.Integer paymentSequence) {
        this.paymentSequence = paymentSequence;
    }


    /**
     * Gets the paymentTypeCode value for this OrderPayment.
     * 
     * @return paymentTypeCode
     */
    public java.lang.String getPaymentTypeCode() {
        return paymentTypeCode;
    }


    /**
     * Sets the paymentTypeCode value for this OrderPayment.
     * 
     * @param paymentTypeCode
     */
    public void setPaymentTypeCode(java.lang.String paymentTypeCode) {
        this.paymentTypeCode = paymentTypeCode;
    }


    /**
     * Gets the description value for this OrderPayment.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this OrderPayment.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the CCName value for this OrderPayment.
     * 
     * @return CCName
     */
    public java.lang.String getCCName() {
        return CCName;
    }


    /**
     * Sets the CCName value for this OrderPayment.
     * 
     * @param CCName
     */
    public void setCCName(java.lang.String CCName) {
        this.CCName = CCName;
    }


    /**
     * Gets the CCNumber value for this OrderPayment.
     * 
     * @return CCNumber
     */
    public java.lang.String getCCNumber() {
        return CCNumber;
    }


    /**
     * Sets the CCNumber value for this OrderPayment.
     * 
     * @param CCNumber
     */
    public void setCCNumber(java.lang.String CCNumber) {
        this.CCNumber = CCNumber;
    }


    /**
     * Gets the accountNumberID value for this OrderPayment.
     * 
     * @return accountNumberID
     */
    public java.lang.Long getAccountNumberID() {
        return accountNumberID;
    }


    /**
     * Sets the accountNumberID value for this OrderPayment.
     * 
     * @param accountNumberID
     */
    public void setAccountNumberID(java.lang.Long accountNumberID) {
        this.accountNumberID = accountNumberID;
    }


    /**
     * Gets the CCExpiration value for this OrderPayment.
     * 
     * @return CCExpiration
     */
    public java.lang.String getCCExpiration() {
        return CCExpiration;
    }


    /**
     * Sets the CCExpiration value for this OrderPayment.
     * 
     * @param CCExpiration
     */
    public void setCCExpiration(java.lang.String CCExpiration) {
        this.CCExpiration = CCExpiration;
    }


    /**
     * Gets the CCCvv value for this OrderPayment.
     * 
     * @return CCCvv
     */
    public java.lang.String getCCCvv() {
        return CCCvv;
    }


    /**
     * Sets the CCCvv value for this OrderPayment.
     * 
     * @param CCCvv
     */
    public void setCCCvv(java.lang.String CCCvv) {
        this.CCCvv = CCCvv;
    }


    /**
     * Gets the amount value for this OrderPayment.
     * 
     * @return amount
     */
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this OrderPayment.
     * 
     * @param amount
     */
    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }


    /**
     * Gets the foreignAmount value for this OrderPayment.
     * 
     * @return foreignAmount
     */
    public java.math.BigDecimal getForeignAmount() {
        return foreignAmount;
    }


    /**
     * Sets the foreignAmount value for this OrderPayment.
     * 
     * @param foreignAmount
     */
    public void setForeignAmount(java.math.BigDecimal foreignAmount) {
        this.foreignAmount = foreignAmount;
    }


    /**
     * Gets the issueNumber value for this OrderPayment.
     * 
     * @return issueNumber
     */
    public java.lang.String getIssueNumber() {
        return issueNumber;
    }


    /**
     * Sets the issueNumber value for this OrderPayment.
     * 
     * @param issueNumber
     */
    public void setIssueNumber(java.lang.String issueNumber) {
        this.issueNumber = issueNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderPayment)) return false;
        OrderPayment other = (OrderPayment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.foreignCurrencyCode==null && other.getForeignCurrencyCode()==null) || 
             (this.foreignCurrencyCode!=null &&
              this.foreignCurrencyCode.equals(other.getForeignCurrencyCode()))) &&
            ((this.cultureCode==null && other.getCultureCode()==null) || 
             (this.cultureCode!=null &&
              this.cultureCode.equals(other.getCultureCode()))) &&
            ((this.paymentSequence==null && other.getPaymentSequence()==null) || 
             (this.paymentSequence!=null &&
              this.paymentSequence.equals(other.getPaymentSequence()))) &&
            ((this.paymentTypeCode==null && other.getPaymentTypeCode()==null) || 
             (this.paymentTypeCode!=null &&
              this.paymentTypeCode.equals(other.getPaymentTypeCode()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.CCName==null && other.getCCName()==null) || 
             (this.CCName!=null &&
              this.CCName.equals(other.getCCName()))) &&
            ((this.CCNumber==null && other.getCCNumber()==null) || 
             (this.CCNumber!=null &&
              this.CCNumber.equals(other.getCCNumber()))) &&
            ((this.accountNumberID==null && other.getAccountNumberID()==null) || 
             (this.accountNumberID!=null &&
              this.accountNumberID.equals(other.getAccountNumberID()))) &&
            ((this.CCExpiration==null && other.getCCExpiration()==null) || 
             (this.CCExpiration!=null &&
              this.CCExpiration.equals(other.getCCExpiration()))) &&
            ((this.CCCvv==null && other.getCCCvv()==null) || 
             (this.CCCvv!=null &&
              this.CCCvv.equals(other.getCCCvv()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.foreignAmount==null && other.getForeignAmount()==null) || 
             (this.foreignAmount!=null &&
              this.foreignAmount.equals(other.getForeignAmount()))) &&
            ((this.issueNumber==null && other.getIssueNumber()==null) || 
             (this.issueNumber!=null &&
              this.issueNumber.equals(other.getIssueNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getForeignCurrencyCode() != null) {
            _hashCode += getForeignCurrencyCode().hashCode();
        }
        if (getCultureCode() != null) {
            _hashCode += getCultureCode().hashCode();
        }
        if (getPaymentSequence() != null) {
            _hashCode += getPaymentSequence().hashCode();
        }
        if (getPaymentTypeCode() != null) {
            _hashCode += getPaymentTypeCode().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getCCName() != null) {
            _hashCode += getCCName().hashCode();
        }
        if (getCCNumber() != null) {
            _hashCode += getCCNumber().hashCode();
        }
        if (getAccountNumberID() != null) {
            _hashCode += getAccountNumberID().hashCode();
        }
        if (getCCExpiration() != null) {
            _hashCode += getCCExpiration().hashCode();
        }
        if (getCCCvv() != null) {
            _hashCode += getCCCvv().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getForeignAmount() != null) {
            _hashCode += getForeignAmount().hashCode();
        }
        if (getIssueNumber() != null) {
            _hashCode += getIssueNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderPayment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderPayment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ForeignCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cultureCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CultureCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PaymentSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PaymentTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CCName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CCName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CCNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CCNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumberID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "AccountNumberID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CCExpiration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CCExpiration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CCCvv");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CCCvv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ForeignAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issueNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "IssueNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
