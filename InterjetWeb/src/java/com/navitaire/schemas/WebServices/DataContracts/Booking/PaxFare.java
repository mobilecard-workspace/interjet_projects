/**
 * PaxFare.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxFare  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String paxType;

    private java.lang.String paxDiscountCode;

    private java.lang.String fareDiscountCode;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge[] serviceCharges;

    public PaxFare() {
    }

    public PaxFare(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String paxType,
           java.lang.String paxDiscountCode,
           java.lang.String fareDiscountCode,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge[] serviceCharges) {
        super(
            state);
        this.paxType = paxType;
        this.paxDiscountCode = paxDiscountCode;
        this.fareDiscountCode = fareDiscountCode;
        this.serviceCharges = serviceCharges;
    }


    /**
     * Gets the paxType value for this PaxFare.
     * 
     * @return paxType
     */
    public java.lang.String getPaxType() {
        return paxType;
    }


    /**
     * Sets the paxType value for this PaxFare.
     * 
     * @param paxType
     */
    public void setPaxType(java.lang.String paxType) {
        this.paxType = paxType;
    }


    /**
     * Gets the paxDiscountCode value for this PaxFare.
     * 
     * @return paxDiscountCode
     */
    public java.lang.String getPaxDiscountCode() {
        return paxDiscountCode;
    }


    /**
     * Sets the paxDiscountCode value for this PaxFare.
     * 
     * @param paxDiscountCode
     */
    public void setPaxDiscountCode(java.lang.String paxDiscountCode) {
        this.paxDiscountCode = paxDiscountCode;
    }


    /**
     * Gets the fareDiscountCode value for this PaxFare.
     * 
     * @return fareDiscountCode
     */
    public java.lang.String getFareDiscountCode() {
        return fareDiscountCode;
    }


    /**
     * Sets the fareDiscountCode value for this PaxFare.
     * 
     * @param fareDiscountCode
     */
    public void setFareDiscountCode(java.lang.String fareDiscountCode) {
        this.fareDiscountCode = fareDiscountCode;
    }


    /**
     * Gets the serviceCharges value for this PaxFare.
     * 
     * @return serviceCharges
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge[] getServiceCharges() {
        return serviceCharges;
    }


    /**
     * Sets the serviceCharges value for this PaxFare.
     * 
     * @param serviceCharges
     */
    public void setServiceCharges(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge[] serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxFare)) return false;
        PaxFare other = (PaxFare) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.paxType==null && other.getPaxType()==null) || 
             (this.paxType!=null &&
              this.paxType.equals(other.getPaxType()))) &&
            ((this.paxDiscountCode==null && other.getPaxDiscountCode()==null) || 
             (this.paxDiscountCode!=null &&
              this.paxDiscountCode.equals(other.getPaxDiscountCode()))) &&
            ((this.fareDiscountCode==null && other.getFareDiscountCode()==null) || 
             (this.fareDiscountCode!=null &&
              this.fareDiscountCode.equals(other.getFareDiscountCode()))) &&
            ((this.serviceCharges==null && other.getServiceCharges()==null) || 
             (this.serviceCharges!=null &&
              java.util.Arrays.equals(this.serviceCharges, other.getServiceCharges())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPaxType() != null) {
            _hashCode += getPaxType().hashCode();
        }
        if (getPaxDiscountCode() != null) {
            _hashCode += getPaxDiscountCode().hashCode();
        }
        if (getFareDiscountCode() != null) {
            _hashCode += getFareDiscountCode().hashCode();
        }
        if (getServiceCharges() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getServiceCharges());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getServiceCharges(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxFare.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxFare"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxDiscountCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxDiscountCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareDiscountCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareDiscountCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceCharges");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ServiceCharges"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingServiceCharge"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingServiceCharge"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
