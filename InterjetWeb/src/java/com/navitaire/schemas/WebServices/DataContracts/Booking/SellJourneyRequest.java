/**
 * SellJourneyRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SellJourneyRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequestData sellJourneyRequestData;

    public SellJourneyRequest() {
    }

    public SellJourneyRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequestData sellJourneyRequestData) {
           this.sellJourneyRequestData = sellJourneyRequestData;
    }


    /**
     * Gets the sellJourneyRequestData value for this SellJourneyRequest.
     * 
     * @return sellJourneyRequestData
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequestData getSellJourneyRequestData() {
        return sellJourneyRequestData;
    }


    /**
     * Sets the sellJourneyRequestData value for this SellJourneyRequest.
     * 
     * @param sellJourneyRequestData
     */
    public void setSellJourneyRequestData(com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequestData sellJourneyRequestData) {
        this.sellJourneyRequestData = sellJourneyRequestData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SellJourneyRequest)) return false;
        SellJourneyRequest other = (SellJourneyRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sellJourneyRequestData==null && other.getSellJourneyRequestData()==null) || 
             (this.sellJourneyRequestData!=null &&
              this.sellJourneyRequestData.equals(other.getSellJourneyRequestData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSellJourneyRequestData() != null) {
            _hashCode += getSellJourneyRequestData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SellJourneyRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellJourneyRequestData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyRequestData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
