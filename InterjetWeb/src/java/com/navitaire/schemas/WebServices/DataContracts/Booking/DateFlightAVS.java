/**
 * DateFlightAVS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DateFlightAVS  implements java.io.Serializable {
    private java.lang.String departureStation;

    private java.lang.String arrivalStation;

    private java.util.Calendar departureDate;

    private java.lang.String carrierCode;

    private java.lang.String flightNumber;

    private java.lang.String opSuffix;

    private java.lang.String flightAVSType;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingClassAVS[] bookingClassAVSList;

    public DateFlightAVS() {
    }

    public DateFlightAVS(
           java.lang.String departureStation,
           java.lang.String arrivalStation,
           java.util.Calendar departureDate,
           java.lang.String carrierCode,
           java.lang.String flightNumber,
           java.lang.String opSuffix,
           java.lang.String flightAVSType,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingClassAVS[] bookingClassAVSList) {
           this.departureStation = departureStation;
           this.arrivalStation = arrivalStation;
           this.departureDate = departureDate;
           this.carrierCode = carrierCode;
           this.flightNumber = flightNumber;
           this.opSuffix = opSuffix;
           this.flightAVSType = flightAVSType;
           this.bookingClassAVSList = bookingClassAVSList;
    }


    /**
     * Gets the departureStation value for this DateFlightAVS.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this DateFlightAVS.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the arrivalStation value for this DateFlightAVS.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this DateFlightAVS.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the departureDate value for this DateFlightAVS.
     * 
     * @return departureDate
     */
    public java.util.Calendar getDepartureDate() {
        return departureDate;
    }


    /**
     * Sets the departureDate value for this DateFlightAVS.
     * 
     * @param departureDate
     */
    public void setDepartureDate(java.util.Calendar departureDate) {
        this.departureDate = departureDate;
    }


    /**
     * Gets the carrierCode value for this DateFlightAVS.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this DateFlightAVS.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the flightNumber value for this DateFlightAVS.
     * 
     * @return flightNumber
     */
    public java.lang.String getFlightNumber() {
        return flightNumber;
    }


    /**
     * Sets the flightNumber value for this DateFlightAVS.
     * 
     * @param flightNumber
     */
    public void setFlightNumber(java.lang.String flightNumber) {
        this.flightNumber = flightNumber;
    }


    /**
     * Gets the opSuffix value for this DateFlightAVS.
     * 
     * @return opSuffix
     */
    public java.lang.String getOpSuffix() {
        return opSuffix;
    }


    /**
     * Sets the opSuffix value for this DateFlightAVS.
     * 
     * @param opSuffix
     */
    public void setOpSuffix(java.lang.String opSuffix) {
        this.opSuffix = opSuffix;
    }


    /**
     * Gets the flightAVSType value for this DateFlightAVS.
     * 
     * @return flightAVSType
     */
    public java.lang.String getFlightAVSType() {
        return flightAVSType;
    }


    /**
     * Sets the flightAVSType value for this DateFlightAVS.
     * 
     * @param flightAVSType
     */
    public void setFlightAVSType(java.lang.String flightAVSType) {
        this.flightAVSType = flightAVSType;
    }


    /**
     * Gets the bookingClassAVSList value for this DateFlightAVS.
     * 
     * @return bookingClassAVSList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingClassAVS[] getBookingClassAVSList() {
        return bookingClassAVSList;
    }


    /**
     * Sets the bookingClassAVSList value for this DateFlightAVS.
     * 
     * @param bookingClassAVSList
     */
    public void setBookingClassAVSList(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingClassAVS[] bookingClassAVSList) {
        this.bookingClassAVSList = bookingClassAVSList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DateFlightAVS)) return false;
        DateFlightAVS other = (DateFlightAVS) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.departureDate==null && other.getDepartureDate()==null) || 
             (this.departureDate!=null &&
              this.departureDate.equals(other.getDepartureDate()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.flightNumber==null && other.getFlightNumber()==null) || 
             (this.flightNumber!=null &&
              this.flightNumber.equals(other.getFlightNumber()))) &&
            ((this.opSuffix==null && other.getOpSuffix()==null) || 
             (this.opSuffix!=null &&
              this.opSuffix.equals(other.getOpSuffix()))) &&
            ((this.flightAVSType==null && other.getFlightAVSType()==null) || 
             (this.flightAVSType!=null &&
              this.flightAVSType.equals(other.getFlightAVSType()))) &&
            ((this.bookingClassAVSList==null && other.getBookingClassAVSList()==null) || 
             (this.bookingClassAVSList!=null &&
              java.util.Arrays.equals(this.bookingClassAVSList, other.getBookingClassAVSList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getDepartureDate() != null) {
            _hashCode += getDepartureDate().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getFlightNumber() != null) {
            _hashCode += getFlightNumber().hashCode();
        }
        if (getOpSuffix() != null) {
            _hashCode += getOpSuffix().hashCode();
        }
        if (getFlightAVSType() != null) {
            _hashCode += getFlightAVSType().hashCode();
        }
        if (getBookingClassAVSList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingClassAVSList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingClassAVSList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DateFlightAVS.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightAVS"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OpSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightAVSType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightAVSType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingClassAVSList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingClassAVSList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingClassAVS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingClassAVS"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
