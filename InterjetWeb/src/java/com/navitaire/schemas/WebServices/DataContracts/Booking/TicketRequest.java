/**
 * TicketRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class TicketRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentTicketRequest[] segmentTicketRequests;

    public TicketRequest() {
    }

    public TicketRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentTicketRequest[] segmentTicketRequests) {
           this.segmentTicketRequests = segmentTicketRequests;
    }


    /**
     * Gets the segmentTicketRequests value for this TicketRequest.
     * 
     * @return segmentTicketRequests
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentTicketRequest[] getSegmentTicketRequests() {
        return segmentTicketRequests;
    }


    /**
     * Sets the segmentTicketRequests value for this TicketRequest.
     * 
     * @param segmentTicketRequests
     */
    public void setSegmentTicketRequests(com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentTicketRequest[] segmentTicketRequests) {
        this.segmentTicketRequests = segmentTicketRequests;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TicketRequest)) return false;
        TicketRequest other = (TicketRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.segmentTicketRequests==null && other.getSegmentTicketRequests()==null) || 
             (this.segmentTicketRequests!=null &&
              java.util.Arrays.equals(this.segmentTicketRequests, other.getSegmentTicketRequests())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSegmentTicketRequests() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegmentTicketRequests());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegmentTicketRequests(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TicketRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TicketRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentTicketRequests");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentTicketRequests"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentTicketRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentTicketRequest"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
