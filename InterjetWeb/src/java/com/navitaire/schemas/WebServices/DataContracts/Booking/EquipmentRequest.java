/**
 * EquipmentRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class EquipmentRequest  implements java.io.Serializable {
    private java.lang.String equipmentType;

    private java.lang.String equipmentTypeSuffix;

    private java.util.Calendar departureDate;

    private java.lang.String departureStation;

    private java.lang.String arrivalStation;

    private java.lang.String carrierCode;

    private java.lang.String flightNumber;

    private java.lang.String opSuffix;

    private java.lang.String marketingCode;

    private java.lang.Boolean marketingOverride;

    private java.lang.String productClassCode;

    public EquipmentRequest() {
    }

    public EquipmentRequest(
           java.lang.String equipmentType,
           java.lang.String equipmentTypeSuffix,
           java.util.Calendar departureDate,
           java.lang.String departureStation,
           java.lang.String arrivalStation,
           java.lang.String carrierCode,
           java.lang.String flightNumber,
           java.lang.String opSuffix,
           java.lang.String marketingCode,
           java.lang.Boolean marketingOverride,
           java.lang.String productClassCode) {
           this.equipmentType = equipmentType;
           this.equipmentTypeSuffix = equipmentTypeSuffix;
           this.departureDate = departureDate;
           this.departureStation = departureStation;
           this.arrivalStation = arrivalStation;
           this.carrierCode = carrierCode;
           this.flightNumber = flightNumber;
           this.opSuffix = opSuffix;
           this.marketingCode = marketingCode;
           this.marketingOverride = marketingOverride;
           this.productClassCode = productClassCode;
    }


    /**
     * Gets the equipmentType value for this EquipmentRequest.
     * 
     * @return equipmentType
     */
    public java.lang.String getEquipmentType() {
        return equipmentType;
    }


    /**
     * Sets the equipmentType value for this EquipmentRequest.
     * 
     * @param equipmentType
     */
    public void setEquipmentType(java.lang.String equipmentType) {
        this.equipmentType = equipmentType;
    }


    /**
     * Gets the equipmentTypeSuffix value for this EquipmentRequest.
     * 
     * @return equipmentTypeSuffix
     */
    public java.lang.String getEquipmentTypeSuffix() {
        return equipmentTypeSuffix;
    }


    /**
     * Sets the equipmentTypeSuffix value for this EquipmentRequest.
     * 
     * @param equipmentTypeSuffix
     */
    public void setEquipmentTypeSuffix(java.lang.String equipmentTypeSuffix) {
        this.equipmentTypeSuffix = equipmentTypeSuffix;
    }


    /**
     * Gets the departureDate value for this EquipmentRequest.
     * 
     * @return departureDate
     */
    public java.util.Calendar getDepartureDate() {
        return departureDate;
    }


    /**
     * Sets the departureDate value for this EquipmentRequest.
     * 
     * @param departureDate
     */
    public void setDepartureDate(java.util.Calendar departureDate) {
        this.departureDate = departureDate;
    }


    /**
     * Gets the departureStation value for this EquipmentRequest.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this EquipmentRequest.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the arrivalStation value for this EquipmentRequest.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this EquipmentRequest.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the carrierCode value for this EquipmentRequest.
     * 
     * @return carrierCode
     */
    public java.lang.String getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this EquipmentRequest.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(java.lang.String carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the flightNumber value for this EquipmentRequest.
     * 
     * @return flightNumber
     */
    public java.lang.String getFlightNumber() {
        return flightNumber;
    }


    /**
     * Sets the flightNumber value for this EquipmentRequest.
     * 
     * @param flightNumber
     */
    public void setFlightNumber(java.lang.String flightNumber) {
        this.flightNumber = flightNumber;
    }


    /**
     * Gets the opSuffix value for this EquipmentRequest.
     * 
     * @return opSuffix
     */
    public java.lang.String getOpSuffix() {
        return opSuffix;
    }


    /**
     * Sets the opSuffix value for this EquipmentRequest.
     * 
     * @param opSuffix
     */
    public void setOpSuffix(java.lang.String opSuffix) {
        this.opSuffix = opSuffix;
    }


    /**
     * Gets the marketingCode value for this EquipmentRequest.
     * 
     * @return marketingCode
     */
    public java.lang.String getMarketingCode() {
        return marketingCode;
    }


    /**
     * Sets the marketingCode value for this EquipmentRequest.
     * 
     * @param marketingCode
     */
    public void setMarketingCode(java.lang.String marketingCode) {
        this.marketingCode = marketingCode;
    }


    /**
     * Gets the marketingOverride value for this EquipmentRequest.
     * 
     * @return marketingOverride
     */
    public java.lang.Boolean getMarketingOverride() {
        return marketingOverride;
    }


    /**
     * Sets the marketingOverride value for this EquipmentRequest.
     * 
     * @param marketingOverride
     */
    public void setMarketingOverride(java.lang.Boolean marketingOverride) {
        this.marketingOverride = marketingOverride;
    }


    /**
     * Gets the productClassCode value for this EquipmentRequest.
     * 
     * @return productClassCode
     */
    public java.lang.String getProductClassCode() {
        return productClassCode;
    }


    /**
     * Sets the productClassCode value for this EquipmentRequest.
     * 
     * @param productClassCode
     */
    public void setProductClassCode(java.lang.String productClassCode) {
        this.productClassCode = productClassCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EquipmentRequest)) return false;
        EquipmentRequest other = (EquipmentRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.equipmentType==null && other.getEquipmentType()==null) || 
             (this.equipmentType!=null &&
              this.equipmentType.equals(other.getEquipmentType()))) &&
            ((this.equipmentTypeSuffix==null && other.getEquipmentTypeSuffix()==null) || 
             (this.equipmentTypeSuffix!=null &&
              this.equipmentTypeSuffix.equals(other.getEquipmentTypeSuffix()))) &&
            ((this.departureDate==null && other.getDepartureDate()==null) || 
             (this.departureDate!=null &&
              this.departureDate.equals(other.getDepartureDate()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.flightNumber==null && other.getFlightNumber()==null) || 
             (this.flightNumber!=null &&
              this.flightNumber.equals(other.getFlightNumber()))) &&
            ((this.opSuffix==null && other.getOpSuffix()==null) || 
             (this.opSuffix!=null &&
              this.opSuffix.equals(other.getOpSuffix()))) &&
            ((this.marketingCode==null && other.getMarketingCode()==null) || 
             (this.marketingCode!=null &&
              this.marketingCode.equals(other.getMarketingCode()))) &&
            ((this.marketingOverride==null && other.getMarketingOverride()==null) || 
             (this.marketingOverride!=null &&
              this.marketingOverride.equals(other.getMarketingOverride()))) &&
            ((this.productClassCode==null && other.getProductClassCode()==null) || 
             (this.productClassCode!=null &&
              this.productClassCode.equals(other.getProductClassCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEquipmentType() != null) {
            _hashCode += getEquipmentType().hashCode();
        }
        if (getEquipmentTypeSuffix() != null) {
            _hashCode += getEquipmentTypeSuffix().hashCode();
        }
        if (getDepartureDate() != null) {
            _hashCode += getDepartureDate().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getFlightNumber() != null) {
            _hashCode += getFlightNumber().hashCode();
        }
        if (getOpSuffix() != null) {
            _hashCode += getOpSuffix().hashCode();
        }
        if (getMarketingCode() != null) {
            _hashCode += getMarketingCode().hashCode();
        }
        if (getMarketingOverride() != null) {
            _hashCode += getMarketingOverride().hashCode();
        }
        if (getProductClassCode() != null) {
            _hashCode += getProductClassCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EquipmentRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentTypeSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentTypeSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OpSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketingCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MarketingCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketingOverride");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MarketingOverride"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productClassCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ProductClassCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
