/**
 * SellJourney.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SellJourney  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Boolean notForGeneralUse;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellSegment[] segments;

    public SellJourney() {
    }

    public SellJourney(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Boolean notForGeneralUse,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellSegment[] segments) {
        super(
            state);
        this.notForGeneralUse = notForGeneralUse;
        this.segments = segments;
    }


    /**
     * Gets the notForGeneralUse value for this SellJourney.
     * 
     * @return notForGeneralUse
     */
    public java.lang.Boolean getNotForGeneralUse() {
        return notForGeneralUse;
    }


    /**
     * Sets the notForGeneralUse value for this SellJourney.
     * 
     * @param notForGeneralUse
     */
    public void setNotForGeneralUse(java.lang.Boolean notForGeneralUse) {
        this.notForGeneralUse = notForGeneralUse;
    }


    /**
     * Gets the segments value for this SellJourney.
     * 
     * @return segments
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellSegment[] getSegments() {
        return segments;
    }


    /**
     * Sets the segments value for this SellJourney.
     * 
     * @param segments
     */
    public void setSegments(com.navitaire.schemas.WebServices.DataContracts.Booking.SellSegment[] segments) {
        this.segments = segments;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SellJourney)) return false;
        SellJourney other = (SellJourney) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.notForGeneralUse==null && other.getNotForGeneralUse()==null) || 
             (this.notForGeneralUse!=null &&
              this.notForGeneralUse.equals(other.getNotForGeneralUse()))) &&
            ((this.segments==null && other.getSegments()==null) || 
             (this.segments!=null &&
              java.util.Arrays.equals(this.segments, other.getSegments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getNotForGeneralUse() != null) {
            _hashCode += getNotForGeneralUse().hashCode();
        }
        if (getSegments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SellJourney.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourney"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notForGeneralUse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NotForGeneralUse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellSegment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellSegment"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
