/**
 * UpdateFeeStatusRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class UpdateFeeStatusRequestData  implements java.io.Serializable {
    private java.lang.String recordLocator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FeeStatusRequest[] feeStatusRequests;

    public UpdateFeeStatusRequestData() {
    }

    public UpdateFeeStatusRequestData(
           java.lang.String recordLocator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FeeStatusRequest[] feeStatusRequests) {
           this.recordLocator = recordLocator;
           this.feeStatusRequests = feeStatusRequests;
    }


    /**
     * Gets the recordLocator value for this UpdateFeeStatusRequestData.
     * 
     * @return recordLocator
     */
    public java.lang.String getRecordLocator() {
        return recordLocator;
    }


    /**
     * Sets the recordLocator value for this UpdateFeeStatusRequestData.
     * 
     * @param recordLocator
     */
    public void setRecordLocator(java.lang.String recordLocator) {
        this.recordLocator = recordLocator;
    }


    /**
     * Gets the feeStatusRequests value for this UpdateFeeStatusRequestData.
     * 
     * @return feeStatusRequests
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FeeStatusRequest[] getFeeStatusRequests() {
        return feeStatusRequests;
    }


    /**
     * Sets the feeStatusRequests value for this UpdateFeeStatusRequestData.
     * 
     * @param feeStatusRequests
     */
    public void setFeeStatusRequests(com.navitaire.schemas.WebServices.DataContracts.Booking.FeeStatusRequest[] feeStatusRequests) {
        this.feeStatusRequests = feeStatusRequests;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateFeeStatusRequestData)) return false;
        UpdateFeeStatusRequestData other = (UpdateFeeStatusRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.recordLocator==null && other.getRecordLocator()==null) || 
             (this.recordLocator!=null &&
              this.recordLocator.equals(other.getRecordLocator()))) &&
            ((this.feeStatusRequests==null && other.getFeeStatusRequests()==null) || 
             (this.feeStatusRequests!=null &&
              java.util.Arrays.equals(this.feeStatusRequests, other.getFeeStatusRequests())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRecordLocator() != null) {
            _hashCode += getRecordLocator().hashCode();
        }
        if (getFeeStatusRequests() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFeeStatusRequests());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFeeStatusRequests(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateFeeStatusRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateFeeStatusRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeStatusRequests");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeStatusRequests"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeStatusRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeStatusRequest"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
