/**
 * SegmentSeatRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SegmentSeatRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator;

    private java.util.Calendar STD;

    private java.lang.String departureStation;

    private java.lang.String arrivalStation;

    private short[] passengerNumbers;

    private java.lang.String unitDesignator;

    private java.lang.String compartmentDesignator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] passengerSeatPreferences;

    private long[] passengerIDs;

    private java.lang.String[] requestedSSRs;

    public SegmentSeatRequest() {
    }

    public SegmentSeatRequest(
           com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator,
           java.util.Calendar STD,
           java.lang.String departureStation,
           java.lang.String arrivalStation,
           short[] passengerNumbers,
           java.lang.String unitDesignator,
           java.lang.String compartmentDesignator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] passengerSeatPreferences,
           long[] passengerIDs,
           java.lang.String[] requestedSSRs) {
           this.flightDesignator = flightDesignator;
           this.STD = STD;
           this.departureStation = departureStation;
           this.arrivalStation = arrivalStation;
           this.passengerNumbers = passengerNumbers;
           this.unitDesignator = unitDesignator;
           this.compartmentDesignator = compartmentDesignator;
           this.passengerSeatPreferences = passengerSeatPreferences;
           this.passengerIDs = passengerIDs;
           this.requestedSSRs = requestedSSRs;
    }


    /**
     * Gets the flightDesignator value for this SegmentSeatRequest.
     * 
     * @return flightDesignator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator getFlightDesignator() {
        return flightDesignator;
    }


    /**
     * Sets the flightDesignator value for this SegmentSeatRequest.
     * 
     * @param flightDesignator
     */
    public void setFlightDesignator(com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator flightDesignator) {
        this.flightDesignator = flightDesignator;
    }


    /**
     * Gets the STD value for this SegmentSeatRequest.
     * 
     * @return STD
     */
    public java.util.Calendar getSTD() {
        return STD;
    }


    /**
     * Sets the STD value for this SegmentSeatRequest.
     * 
     * @param STD
     */
    public void setSTD(java.util.Calendar STD) {
        this.STD = STD;
    }


    /**
     * Gets the departureStation value for this SegmentSeatRequest.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this SegmentSeatRequest.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the arrivalStation value for this SegmentSeatRequest.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this SegmentSeatRequest.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the passengerNumbers value for this SegmentSeatRequest.
     * 
     * @return passengerNumbers
     */
    public short[] getPassengerNumbers() {
        return passengerNumbers;
    }


    /**
     * Sets the passengerNumbers value for this SegmentSeatRequest.
     * 
     * @param passengerNumbers
     */
    public void setPassengerNumbers(short[] passengerNumbers) {
        this.passengerNumbers = passengerNumbers;
    }


    /**
     * Gets the unitDesignator value for this SegmentSeatRequest.
     * 
     * @return unitDesignator
     */
    public java.lang.String getUnitDesignator() {
        return unitDesignator;
    }


    /**
     * Sets the unitDesignator value for this SegmentSeatRequest.
     * 
     * @param unitDesignator
     */
    public void setUnitDesignator(java.lang.String unitDesignator) {
        this.unitDesignator = unitDesignator;
    }


    /**
     * Gets the compartmentDesignator value for this SegmentSeatRequest.
     * 
     * @return compartmentDesignator
     */
    public java.lang.String getCompartmentDesignator() {
        return compartmentDesignator;
    }


    /**
     * Sets the compartmentDesignator value for this SegmentSeatRequest.
     * 
     * @param compartmentDesignator
     */
    public void setCompartmentDesignator(java.lang.String compartmentDesignator) {
        this.compartmentDesignator = compartmentDesignator;
    }


    /**
     * Gets the passengerSeatPreferences value for this SegmentSeatRequest.
     * 
     * @return passengerSeatPreferences
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] getPassengerSeatPreferences() {
        return passengerSeatPreferences;
    }


    /**
     * Sets the passengerSeatPreferences value for this SegmentSeatRequest.
     * 
     * @param passengerSeatPreferences
     */
    public void setPassengerSeatPreferences(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[] passengerSeatPreferences) {
        this.passengerSeatPreferences = passengerSeatPreferences;
    }


    /**
     * Gets the passengerIDs value for this SegmentSeatRequest.
     * 
     * @return passengerIDs
     */
    public long[] getPassengerIDs() {
        return passengerIDs;
    }


    /**
     * Sets the passengerIDs value for this SegmentSeatRequest.
     * 
     * @param passengerIDs
     */
    public void setPassengerIDs(long[] passengerIDs) {
        this.passengerIDs = passengerIDs;
    }


    /**
     * Gets the requestedSSRs value for this SegmentSeatRequest.
     * 
     * @return requestedSSRs
     */
    public java.lang.String[] getRequestedSSRs() {
        return requestedSSRs;
    }


    /**
     * Sets the requestedSSRs value for this SegmentSeatRequest.
     * 
     * @param requestedSSRs
     */
    public void setRequestedSSRs(java.lang.String[] requestedSSRs) {
        this.requestedSSRs = requestedSSRs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SegmentSeatRequest)) return false;
        SegmentSeatRequest other = (SegmentSeatRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flightDesignator==null && other.getFlightDesignator()==null) || 
             (this.flightDesignator!=null &&
              this.flightDesignator.equals(other.getFlightDesignator()))) &&
            ((this.STD==null && other.getSTD()==null) || 
             (this.STD!=null &&
              this.STD.equals(other.getSTD()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.passengerNumbers==null && other.getPassengerNumbers()==null) || 
             (this.passengerNumbers!=null &&
              java.util.Arrays.equals(this.passengerNumbers, other.getPassengerNumbers()))) &&
            ((this.unitDesignator==null && other.getUnitDesignator()==null) || 
             (this.unitDesignator!=null &&
              this.unitDesignator.equals(other.getUnitDesignator()))) &&
            ((this.compartmentDesignator==null && other.getCompartmentDesignator()==null) || 
             (this.compartmentDesignator!=null &&
              this.compartmentDesignator.equals(other.getCompartmentDesignator()))) &&
            ((this.passengerSeatPreferences==null && other.getPassengerSeatPreferences()==null) || 
             (this.passengerSeatPreferences!=null &&
              java.util.Arrays.equals(this.passengerSeatPreferences, other.getPassengerSeatPreferences()))) &&
            ((this.passengerIDs==null && other.getPassengerIDs()==null) || 
             (this.passengerIDs!=null &&
              java.util.Arrays.equals(this.passengerIDs, other.getPassengerIDs()))) &&
            ((this.requestedSSRs==null && other.getRequestedSSRs()==null) || 
             (this.requestedSSRs!=null &&
              java.util.Arrays.equals(this.requestedSSRs, other.getRequestedSSRs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlightDesignator() != null) {
            _hashCode += getFlightDesignator().hashCode();
        }
        if (getSTD() != null) {
            _hashCode += getSTD().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getPassengerNumbers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerNumbers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerNumbers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUnitDesignator() != null) {
            _hashCode += getUnitDesignator().hashCode();
        }
        if (getCompartmentDesignator() != null) {
            _hashCode += getCompartmentDesignator().hashCode();
        }
        if (getPassengerSeatPreferences() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerSeatPreferences());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerSeatPreferences(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerIDs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerIDs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerIDs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRequestedSSRs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRequestedSSRs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRequestedSSRs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SegmentSeatRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSeatRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FlightDesignator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumbers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumbers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "short"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UnitDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("compartmentDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerSeatPreferences");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerSeatPreferences"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerIDs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerIDs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "long"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestedSSRs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RequestedSSRs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
