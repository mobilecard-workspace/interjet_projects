/**
 * TripAvailabilityRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class TripAvailabilityRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityRequest[] availabilityRequests;

    private org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter;

    public TripAvailabilityRequest() {
    }

    public TripAvailabilityRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityRequest[] availabilityRequests,
           org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
           this.availabilityRequests = availabilityRequests;
           this.loyaltyFilter = loyaltyFilter;
    }


    /**
     * Gets the availabilityRequests value for this TripAvailabilityRequest.
     * 
     * @return availabilityRequests
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityRequest[] getAvailabilityRequests() {
        return availabilityRequests;
    }


    /**
     * Sets the availabilityRequests value for this TripAvailabilityRequest.
     * 
     * @param availabilityRequests
     */
    public void setAvailabilityRequests(com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityRequest[] availabilityRequests) {
        this.availabilityRequests = availabilityRequests;
    }


    /**
     * Gets the loyaltyFilter value for this TripAvailabilityRequest.
     * 
     * @return loyaltyFilter
     */
    public org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter getLoyaltyFilter() {
        return loyaltyFilter;
    }


    /**
     * Sets the loyaltyFilter value for this TripAvailabilityRequest.
     * 
     * @param loyaltyFilter
     */
    public void setLoyaltyFilter(org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
        this.loyaltyFilter = loyaltyFilter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TripAvailabilityRequest)) return false;
        TripAvailabilityRequest other = (TripAvailabilityRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.availabilityRequests==null && other.getAvailabilityRequests()==null) || 
             (this.availabilityRequests!=null &&
              java.util.Arrays.equals(this.availabilityRequests, other.getAvailabilityRequests()))) &&
            ((this.loyaltyFilter==null && other.getLoyaltyFilter()==null) || 
             (this.loyaltyFilter!=null &&
              this.loyaltyFilter.equals(other.getLoyaltyFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAvailabilityRequests() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAvailabilityRequests());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAvailabilityRequests(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLoyaltyFilter() != null) {
            _hashCode += getLoyaltyFilter().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TripAvailabilityRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TripAvailabilityRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availabilityRequests");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityRequests"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityRequest"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loyaltyFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LoyaltyFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LoyaltyFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
