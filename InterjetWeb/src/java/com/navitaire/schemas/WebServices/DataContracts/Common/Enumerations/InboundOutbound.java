/**
 * InboundOutbound.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class InboundOutbound implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected InboundOutbound(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _None = "None";
    public static final java.lang.String _Inbound = "Inbound";
    public static final java.lang.String _Outbound = "Outbound";
    public static final java.lang.String _Both = "Both";
    public static final java.lang.String _RoundFrom = "RoundFrom";
    public static final java.lang.String _RoundTo = "RoundTo";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final InboundOutbound None = new InboundOutbound(_None);
    public static final InboundOutbound Inbound = new InboundOutbound(_Inbound);
    public static final InboundOutbound Outbound = new InboundOutbound(_Outbound);
    public static final InboundOutbound Both = new InboundOutbound(_Both);
    public static final InboundOutbound RoundFrom = new InboundOutbound(_RoundFrom);
    public static final InboundOutbound RoundTo = new InboundOutbound(_RoundTo);
    public static final InboundOutbound Unmapped = new InboundOutbound(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static InboundOutbound fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        InboundOutbound enumeration = (InboundOutbound)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static InboundOutbound fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InboundOutbound.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "InboundOutbound"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
