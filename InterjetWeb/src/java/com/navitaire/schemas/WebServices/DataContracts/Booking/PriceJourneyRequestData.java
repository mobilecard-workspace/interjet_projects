/**
 * PriceJourneyRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PriceJourneyRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourney[] priceJourneys;

    private java.lang.Short paxCount;

    private java.lang.String currencyCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers;

    private org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter;

    public PriceJourneyRequestData() {
    }

    public PriceJourneyRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourney[] priceJourneys,
           java.lang.Short paxCount,
           java.lang.String currencyCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS,
           com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers,
           org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
           this.priceJourneys = priceJourneys;
           this.paxCount = paxCount;
           this.currencyCode = currencyCode;
           this.sourcePOS = sourcePOS;
           this.typeOfSale = typeOfSale;
           this.passengers = passengers;
           this.loyaltyFilter = loyaltyFilter;
    }


    /**
     * Gets the priceJourneys value for this PriceJourneyRequestData.
     * 
     * @return priceJourneys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourney[] getPriceJourneys() {
        return priceJourneys;
    }


    /**
     * Sets the priceJourneys value for this PriceJourneyRequestData.
     * 
     * @param priceJourneys
     */
    public void setPriceJourneys(com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourney[] priceJourneys) {
        this.priceJourneys = priceJourneys;
    }


    /**
     * Gets the paxCount value for this PriceJourneyRequestData.
     * 
     * @return paxCount
     */
    public java.lang.Short getPaxCount() {
        return paxCount;
    }


    /**
     * Sets the paxCount value for this PriceJourneyRequestData.
     * 
     * @param paxCount
     */
    public void setPaxCount(java.lang.Short paxCount) {
        this.paxCount = paxCount;
    }


    /**
     * Gets the currencyCode value for this PriceJourneyRequestData.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this PriceJourneyRequestData.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the sourcePOS value for this PriceJourneyRequestData.
     * 
     * @return sourcePOS
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePOS() {
        return sourcePOS;
    }


    /**
     * Sets the sourcePOS value for this PriceJourneyRequestData.
     * 
     * @param sourcePOS
     */
    public void setSourcePOS(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS) {
        this.sourcePOS = sourcePOS;
    }


    /**
     * Gets the typeOfSale value for this PriceJourneyRequestData.
     * 
     * @return typeOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale getTypeOfSale() {
        return typeOfSale;
    }


    /**
     * Sets the typeOfSale value for this PriceJourneyRequestData.
     * 
     * @param typeOfSale
     */
    public void setTypeOfSale(com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale) {
        this.typeOfSale = typeOfSale;
    }


    /**
     * Gets the passengers value for this PriceJourneyRequestData.
     * 
     * @return passengers
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] getPassengers() {
        return passengers;
    }


    /**
     * Sets the passengers value for this PriceJourneyRequestData.
     * 
     * @param passengers
     */
    public void setPassengers(com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[] passengers) {
        this.passengers = passengers;
    }


    /**
     * Gets the loyaltyFilter value for this PriceJourneyRequestData.
     * 
     * @return loyaltyFilter
     */
    public org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter getLoyaltyFilter() {
        return loyaltyFilter;
    }


    /**
     * Sets the loyaltyFilter value for this PriceJourneyRequestData.
     * 
     * @param loyaltyFilter
     */
    public void setLoyaltyFilter(org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
        this.loyaltyFilter = loyaltyFilter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PriceJourneyRequestData)) return false;
        PriceJourneyRequestData other = (PriceJourneyRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.priceJourneys==null && other.getPriceJourneys()==null) || 
             (this.priceJourneys!=null &&
              java.util.Arrays.equals(this.priceJourneys, other.getPriceJourneys()))) &&
            ((this.paxCount==null && other.getPaxCount()==null) || 
             (this.paxCount!=null &&
              this.paxCount.equals(other.getPaxCount()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.sourcePOS==null && other.getSourcePOS()==null) || 
             (this.sourcePOS!=null &&
              this.sourcePOS.equals(other.getSourcePOS()))) &&
            ((this.typeOfSale==null && other.getTypeOfSale()==null) || 
             (this.typeOfSale!=null &&
              this.typeOfSale.equals(other.getTypeOfSale()))) &&
            ((this.passengers==null && other.getPassengers()==null) || 
             (this.passengers!=null &&
              java.util.Arrays.equals(this.passengers, other.getPassengers()))) &&
            ((this.loyaltyFilter==null && other.getLoyaltyFilter()==null) || 
             (this.loyaltyFilter!=null &&
              this.loyaltyFilter.equals(other.getLoyaltyFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPriceJourneys() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPriceJourneys());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPriceJourneys(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxCount() != null) {
            _hashCode += getPaxCount().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getSourcePOS() != null) {
            _hashCode += getSourcePOS().hashCode();
        }
        if (getTypeOfSale() != null) {
            _hashCode += getTypeOfSale().hashCode();
        }
        if (getPassengers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLoyaltyFilter() != null) {
            _hashCode += getLoyaltyFilter().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PriceJourneyRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourneyRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priceJourneys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourneys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourney"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourney"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePOS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passengers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loyaltyFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LoyaltyFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LoyaltyFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
