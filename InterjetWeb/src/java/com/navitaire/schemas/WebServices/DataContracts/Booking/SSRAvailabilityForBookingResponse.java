/**
 * SSRAvailabilityForBookingResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SSRAvailabilityForBookingResponse  extends com.navitaire.schemas.WebServices.DataContracts.Booking.ServiceMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.SSRSegment[] SSRSegmentList;

    public SSRAvailabilityForBookingResponse() {
    }

    public SSRAvailabilityForBookingResponse(
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInfoList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SSRSegment[] SSRSegmentList) {
        super(
            otherServiceInfoList);
        this.SSRSegmentList = SSRSegmentList;
    }


    /**
     * Gets the SSRSegmentList value for this SSRAvailabilityForBookingResponse.
     * 
     * @return SSRSegmentList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SSRSegment[] getSSRSegmentList() {
        return SSRSegmentList;
    }


    /**
     * Sets the SSRSegmentList value for this SSRAvailabilityForBookingResponse.
     * 
     * @param SSRSegmentList
     */
    public void setSSRSegmentList(com.navitaire.schemas.WebServices.DataContracts.Booking.SSRSegment[] SSRSegmentList) {
        this.SSRSegmentList = SSRSegmentList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SSRAvailabilityForBookingResponse)) return false;
        SSRAvailabilityForBookingResponse other = (SSRAvailabilityForBookingResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.SSRSegmentList==null && other.getSSRSegmentList()==null) || 
             (this.SSRSegmentList!=null &&
              java.util.Arrays.equals(this.SSRSegmentList, other.getSSRSegmentList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSSRSegmentList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSSRSegmentList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSSRSegmentList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SSRAvailabilityForBookingResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRAvailabilityForBookingResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRSegmentList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRSegmentList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRSegment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRSegment"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
