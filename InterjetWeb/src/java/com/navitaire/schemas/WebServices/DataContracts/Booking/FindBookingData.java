/**
 * FindBookingData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class FindBookingData  implements java.io.Serializable {
    private java.util.Calendar flightDate;

    private java.lang.String fromCity;

    private java.lang.String toCity;

    private java.lang.String recordLocator;

    private java.lang.Long bookingID;

    private java.lang.Long passengerID;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingStatus bookingStatus;

    private java.lang.String flightNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType;

    private java.lang.String sourceOrganizationCode;

    private java.lang.String sourceDomainCode;

    private java.lang.String sourceAgentCode;

    private java.lang.Boolean editable;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName name;

    private java.util.Calendar expiredDate;

    private java.lang.Boolean allowedToModifyGDSBooking;

    private java.lang.String systemCode;

    public FindBookingData() {
    }

    public FindBookingData(
           java.util.Calendar flightDate,
           java.lang.String fromCity,
           java.lang.String toCity,
           java.lang.String recordLocator,
           java.lang.Long bookingID,
           java.lang.Long passengerID,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingStatus bookingStatus,
           java.lang.String flightNumber,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType,
           java.lang.String sourceOrganizationCode,
           java.lang.String sourceDomainCode,
           java.lang.String sourceAgentCode,
           java.lang.Boolean editable,
           com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName name,
           java.util.Calendar expiredDate,
           java.lang.Boolean allowedToModifyGDSBooking,
           java.lang.String systemCode) {
           this.flightDate = flightDate;
           this.fromCity = fromCity;
           this.toCity = toCity;
           this.recordLocator = recordLocator;
           this.bookingID = bookingID;
           this.passengerID = passengerID;
           this.bookingStatus = bookingStatus;
           this.flightNumber = flightNumber;
           this.channelType = channelType;
           this.sourceOrganizationCode = sourceOrganizationCode;
           this.sourceDomainCode = sourceDomainCode;
           this.sourceAgentCode = sourceAgentCode;
           this.editable = editable;
           this.name = name;
           this.expiredDate = expiredDate;
           this.allowedToModifyGDSBooking = allowedToModifyGDSBooking;
           this.systemCode = systemCode;
    }


    /**
     * Gets the flightDate value for this FindBookingData.
     * 
     * @return flightDate
     */
    public java.util.Calendar getFlightDate() {
        return flightDate;
    }


    /**
     * Sets the flightDate value for this FindBookingData.
     * 
     * @param flightDate
     */
    public void setFlightDate(java.util.Calendar flightDate) {
        this.flightDate = flightDate;
    }


    /**
     * Gets the fromCity value for this FindBookingData.
     * 
     * @return fromCity
     */
    public java.lang.String getFromCity() {
        return fromCity;
    }


    /**
     * Sets the fromCity value for this FindBookingData.
     * 
     * @param fromCity
     */
    public void setFromCity(java.lang.String fromCity) {
        this.fromCity = fromCity;
    }


    /**
     * Gets the toCity value for this FindBookingData.
     * 
     * @return toCity
     */
    public java.lang.String getToCity() {
        return toCity;
    }


    /**
     * Sets the toCity value for this FindBookingData.
     * 
     * @param toCity
     */
    public void setToCity(java.lang.String toCity) {
        this.toCity = toCity;
    }


    /**
     * Gets the recordLocator value for this FindBookingData.
     * 
     * @return recordLocator
     */
    public java.lang.String getRecordLocator() {
        return recordLocator;
    }


    /**
     * Sets the recordLocator value for this FindBookingData.
     * 
     * @param recordLocator
     */
    public void setRecordLocator(java.lang.String recordLocator) {
        this.recordLocator = recordLocator;
    }


    /**
     * Gets the bookingID value for this FindBookingData.
     * 
     * @return bookingID
     */
    public java.lang.Long getBookingID() {
        return bookingID;
    }


    /**
     * Sets the bookingID value for this FindBookingData.
     * 
     * @param bookingID
     */
    public void setBookingID(java.lang.Long bookingID) {
        this.bookingID = bookingID;
    }


    /**
     * Gets the passengerID value for this FindBookingData.
     * 
     * @return passengerID
     */
    public java.lang.Long getPassengerID() {
        return passengerID;
    }


    /**
     * Sets the passengerID value for this FindBookingData.
     * 
     * @param passengerID
     */
    public void setPassengerID(java.lang.Long passengerID) {
        this.passengerID = passengerID;
    }


    /**
     * Gets the bookingStatus value for this FindBookingData.
     * 
     * @return bookingStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingStatus getBookingStatus() {
        return bookingStatus;
    }


    /**
     * Sets the bookingStatus value for this FindBookingData.
     * 
     * @param bookingStatus
     */
    public void setBookingStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }


    /**
     * Gets the flightNumber value for this FindBookingData.
     * 
     * @return flightNumber
     */
    public java.lang.String getFlightNumber() {
        return flightNumber;
    }


    /**
     * Sets the flightNumber value for this FindBookingData.
     * 
     * @param flightNumber
     */
    public void setFlightNumber(java.lang.String flightNumber) {
        this.flightNumber = flightNumber;
    }


    /**
     * Gets the channelType value for this FindBookingData.
     * 
     * @return channelType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType getChannelType() {
        return channelType;
    }


    /**
     * Sets the channelType value for this FindBookingData.
     * 
     * @param channelType
     */
    public void setChannelType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType channelType) {
        this.channelType = channelType;
    }


    /**
     * Gets the sourceOrganizationCode value for this FindBookingData.
     * 
     * @return sourceOrganizationCode
     */
    public java.lang.String getSourceOrganizationCode() {
        return sourceOrganizationCode;
    }


    /**
     * Sets the sourceOrganizationCode value for this FindBookingData.
     * 
     * @param sourceOrganizationCode
     */
    public void setSourceOrganizationCode(java.lang.String sourceOrganizationCode) {
        this.sourceOrganizationCode = sourceOrganizationCode;
    }


    /**
     * Gets the sourceDomainCode value for this FindBookingData.
     * 
     * @return sourceDomainCode
     */
    public java.lang.String getSourceDomainCode() {
        return sourceDomainCode;
    }


    /**
     * Sets the sourceDomainCode value for this FindBookingData.
     * 
     * @param sourceDomainCode
     */
    public void setSourceDomainCode(java.lang.String sourceDomainCode) {
        this.sourceDomainCode = sourceDomainCode;
    }


    /**
     * Gets the sourceAgentCode value for this FindBookingData.
     * 
     * @return sourceAgentCode
     */
    public java.lang.String getSourceAgentCode() {
        return sourceAgentCode;
    }


    /**
     * Sets the sourceAgentCode value for this FindBookingData.
     * 
     * @param sourceAgentCode
     */
    public void setSourceAgentCode(java.lang.String sourceAgentCode) {
        this.sourceAgentCode = sourceAgentCode;
    }


    /**
     * Gets the editable value for this FindBookingData.
     * 
     * @return editable
     */
    public java.lang.Boolean getEditable() {
        return editable;
    }


    /**
     * Sets the editable value for this FindBookingData.
     * 
     * @param editable
     */
    public void setEditable(java.lang.Boolean editable) {
        this.editable = editable;
    }


    /**
     * Gets the name value for this FindBookingData.
     * 
     * @return name
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName getName() {
        return name;
    }


    /**
     * Sets the name value for this FindBookingData.
     * 
     * @param name
     */
    public void setName(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName name) {
        this.name = name;
    }


    /**
     * Gets the expiredDate value for this FindBookingData.
     * 
     * @return expiredDate
     */
    public java.util.Calendar getExpiredDate() {
        return expiredDate;
    }


    /**
     * Sets the expiredDate value for this FindBookingData.
     * 
     * @param expiredDate
     */
    public void setExpiredDate(java.util.Calendar expiredDate) {
        this.expiredDate = expiredDate;
    }


    /**
     * Gets the allowedToModifyGDSBooking value for this FindBookingData.
     * 
     * @return allowedToModifyGDSBooking
     */
    public java.lang.Boolean getAllowedToModifyGDSBooking() {
        return allowedToModifyGDSBooking;
    }


    /**
     * Sets the allowedToModifyGDSBooking value for this FindBookingData.
     * 
     * @param allowedToModifyGDSBooking
     */
    public void setAllowedToModifyGDSBooking(java.lang.Boolean allowedToModifyGDSBooking) {
        this.allowedToModifyGDSBooking = allowedToModifyGDSBooking;
    }


    /**
     * Gets the systemCode value for this FindBookingData.
     * 
     * @return systemCode
     */
    public java.lang.String getSystemCode() {
        return systemCode;
    }


    /**
     * Sets the systemCode value for this FindBookingData.
     * 
     * @param systemCode
     */
    public void setSystemCode(java.lang.String systemCode) {
        this.systemCode = systemCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindBookingData)) return false;
        FindBookingData other = (FindBookingData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flightDate==null && other.getFlightDate()==null) || 
             (this.flightDate!=null &&
              this.flightDate.equals(other.getFlightDate()))) &&
            ((this.fromCity==null && other.getFromCity()==null) || 
             (this.fromCity!=null &&
              this.fromCity.equals(other.getFromCity()))) &&
            ((this.toCity==null && other.getToCity()==null) || 
             (this.toCity!=null &&
              this.toCity.equals(other.getToCity()))) &&
            ((this.recordLocator==null && other.getRecordLocator()==null) || 
             (this.recordLocator!=null &&
              this.recordLocator.equals(other.getRecordLocator()))) &&
            ((this.bookingID==null && other.getBookingID()==null) || 
             (this.bookingID!=null &&
              this.bookingID.equals(other.getBookingID()))) &&
            ((this.passengerID==null && other.getPassengerID()==null) || 
             (this.passengerID!=null &&
              this.passengerID.equals(other.getPassengerID()))) &&
            ((this.bookingStatus==null && other.getBookingStatus()==null) || 
             (this.bookingStatus!=null &&
              this.bookingStatus.equals(other.getBookingStatus()))) &&
            ((this.flightNumber==null && other.getFlightNumber()==null) || 
             (this.flightNumber!=null &&
              this.flightNumber.equals(other.getFlightNumber()))) &&
            ((this.channelType==null && other.getChannelType()==null) || 
             (this.channelType!=null &&
              this.channelType.equals(other.getChannelType()))) &&
            ((this.sourceOrganizationCode==null && other.getSourceOrganizationCode()==null) || 
             (this.sourceOrganizationCode!=null &&
              this.sourceOrganizationCode.equals(other.getSourceOrganizationCode()))) &&
            ((this.sourceDomainCode==null && other.getSourceDomainCode()==null) || 
             (this.sourceDomainCode!=null &&
              this.sourceDomainCode.equals(other.getSourceDomainCode()))) &&
            ((this.sourceAgentCode==null && other.getSourceAgentCode()==null) || 
             (this.sourceAgentCode!=null &&
              this.sourceAgentCode.equals(other.getSourceAgentCode()))) &&
            ((this.editable==null && other.getEditable()==null) || 
             (this.editable!=null &&
              this.editable.equals(other.getEditable()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.expiredDate==null && other.getExpiredDate()==null) || 
             (this.expiredDate!=null &&
              this.expiredDate.equals(other.getExpiredDate()))) &&
            ((this.allowedToModifyGDSBooking==null && other.getAllowedToModifyGDSBooking()==null) || 
             (this.allowedToModifyGDSBooking!=null &&
              this.allowedToModifyGDSBooking.equals(other.getAllowedToModifyGDSBooking()))) &&
            ((this.systemCode==null && other.getSystemCode()==null) || 
             (this.systemCode!=null &&
              this.systemCode.equals(other.getSystemCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlightDate() != null) {
            _hashCode += getFlightDate().hashCode();
        }
        if (getFromCity() != null) {
            _hashCode += getFromCity().hashCode();
        }
        if (getToCity() != null) {
            _hashCode += getToCity().hashCode();
        }
        if (getRecordLocator() != null) {
            _hashCode += getRecordLocator().hashCode();
        }
        if (getBookingID() != null) {
            _hashCode += getBookingID().hashCode();
        }
        if (getPassengerID() != null) {
            _hashCode += getPassengerID().hashCode();
        }
        if (getBookingStatus() != null) {
            _hashCode += getBookingStatus().hashCode();
        }
        if (getFlightNumber() != null) {
            _hashCode += getFlightNumber().hashCode();
        }
        if (getChannelType() != null) {
            _hashCode += getChannelType().hashCode();
        }
        if (getSourceOrganizationCode() != null) {
            _hashCode += getSourceOrganizationCode().hashCode();
        }
        if (getSourceDomainCode() != null) {
            _hashCode += getSourceDomainCode().hashCode();
        }
        if (getSourceAgentCode() != null) {
            _hashCode += getSourceAgentCode().hashCode();
        }
        if (getEditable() != null) {
            _hashCode += getEditable().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getExpiredDate() != null) {
            _hashCode += getExpiredDate().hashCode();
        }
        if (getAllowedToModifyGDSBooking() != null) {
            _hashCode += getAllowedToModifyGDSBooking().hashCode();
        }
        if (getSystemCode() != null) {
            _hashCode += getSystemCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindBookingData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FromCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ToCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flightNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FlightNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChannelType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChannelType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceOrganizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceOrganizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceDomainCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceDomainCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceAgentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourceAgentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("editable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Editable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expiredDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ExpiredDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowedToModifyGDSBooking");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AllowedToModifyGDSBooking"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SystemCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
