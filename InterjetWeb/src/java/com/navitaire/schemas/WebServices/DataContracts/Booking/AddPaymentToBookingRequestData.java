/**
 * AddPaymentToBookingRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class AddPaymentToBookingRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState messageState;

    private java.lang.Boolean waiveFee;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentReferenceType referenceType;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.RequestPaymentMethodType paymentMethodType;

    private java.lang.String paymentMethodCode;

    private java.lang.String quotedCurrencyCode;

    private java.math.BigDecimal quotedAmount;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus status;

    private java.lang.Long accountNumberID;

    private java.lang.String accountNumber;

    private java.util.Calendar expiration;

    private java.lang.Long parentPaymentID;

    private java.lang.Short installments;

    private java.lang.String paymentText;

    private java.lang.Boolean deposit;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField[] paymentFields;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress[] paymentAddresses;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.AgencyAccount agencyAccount;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.CreditShell creditShell;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.CreditFile creditFile;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentVoucher paymentVoucher;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecureRequest threeDSecureRequest;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.MCCRequest MCCRequest;

    private java.lang.String authorizationCode;

    public AddPaymentToBookingRequestData() {
    }

    public AddPaymentToBookingRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState messageState,
           java.lang.Boolean waiveFee,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentReferenceType referenceType,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.RequestPaymentMethodType paymentMethodType,
           java.lang.String paymentMethodCode,
           java.lang.String quotedCurrencyCode,
           java.math.BigDecimal quotedAmount,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus status,
           java.lang.Long accountNumberID,
           java.lang.String accountNumber,
           java.util.Calendar expiration,
           java.lang.Long parentPaymentID,
           java.lang.Short installments,
           java.lang.String paymentText,
           java.lang.Boolean deposit,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField[] paymentFields,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress[] paymentAddresses,
           com.navitaire.schemas.WebServices.DataContracts.Booking.AgencyAccount agencyAccount,
           com.navitaire.schemas.WebServices.DataContracts.Booking.CreditShell creditShell,
           com.navitaire.schemas.WebServices.DataContracts.Booking.CreditFile creditFile,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentVoucher paymentVoucher,
           com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecureRequest threeDSecureRequest,
           com.navitaire.schemas.WebServices.DataContracts.Booking.MCCRequest MCCRequest,
           java.lang.String authorizationCode) {
           this.messageState = messageState;
           this.waiveFee = waiveFee;
           this.referenceType = referenceType;
           this.paymentMethodType = paymentMethodType;
           this.paymentMethodCode = paymentMethodCode;
           this.quotedCurrencyCode = quotedCurrencyCode;
           this.quotedAmount = quotedAmount;
           this.status = status;
           this.accountNumberID = accountNumberID;
           this.accountNumber = accountNumber;
           this.expiration = expiration;
           this.parentPaymentID = parentPaymentID;
           this.installments = installments;
           this.paymentText = paymentText;
           this.deposit = deposit;
           this.paymentFields = paymentFields;
           this.paymentAddresses = paymentAddresses;
           this.agencyAccount = agencyAccount;
           this.creditShell = creditShell;
           this.creditFile = creditFile;
           this.paymentVoucher = paymentVoucher;
           this.threeDSecureRequest = threeDSecureRequest;
           this.MCCRequest = MCCRequest;
           this.authorizationCode = authorizationCode;
    }


    /**
     * Gets the messageState value for this AddPaymentToBookingRequestData.
     * 
     * @return messageState
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState getMessageState() {
        return messageState;
    }


    /**
     * Sets the messageState value for this AddPaymentToBookingRequestData.
     * 
     * @param messageState
     */
    public void setMessageState(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState messageState) {
        this.messageState = messageState;
    }


    /**
     * Gets the waiveFee value for this AddPaymentToBookingRequestData.
     * 
     * @return waiveFee
     */
    public java.lang.Boolean getWaiveFee() {
        return waiveFee;
    }


    /**
     * Sets the waiveFee value for this AddPaymentToBookingRequestData.
     * 
     * @param waiveFee
     */
    public void setWaiveFee(java.lang.Boolean waiveFee) {
        this.waiveFee = waiveFee;
    }


    /**
     * Gets the referenceType value for this AddPaymentToBookingRequestData.
     * 
     * @return referenceType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentReferenceType getReferenceType() {
        return referenceType;
    }


    /**
     * Sets the referenceType value for this AddPaymentToBookingRequestData.
     * 
     * @param referenceType
     */
    public void setReferenceType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentReferenceType referenceType) {
        this.referenceType = referenceType;
    }


    /**
     * Gets the paymentMethodType value for this AddPaymentToBookingRequestData.
     * 
     * @return paymentMethodType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.RequestPaymentMethodType getPaymentMethodType() {
        return paymentMethodType;
    }


    /**
     * Sets the paymentMethodType value for this AddPaymentToBookingRequestData.
     * 
     * @param paymentMethodType
     */
    public void setPaymentMethodType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.RequestPaymentMethodType paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }


    /**
     * Gets the paymentMethodCode value for this AddPaymentToBookingRequestData.
     * 
     * @return paymentMethodCode
     */
    public java.lang.String getPaymentMethodCode() {
        return paymentMethodCode;
    }


    /**
     * Sets the paymentMethodCode value for this AddPaymentToBookingRequestData.
     * 
     * @param paymentMethodCode
     */
    public void setPaymentMethodCode(java.lang.String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }


    /**
     * Gets the quotedCurrencyCode value for this AddPaymentToBookingRequestData.
     * 
     * @return quotedCurrencyCode
     */
    public java.lang.String getQuotedCurrencyCode() {
        return quotedCurrencyCode;
    }


    /**
     * Sets the quotedCurrencyCode value for this AddPaymentToBookingRequestData.
     * 
     * @param quotedCurrencyCode
     */
    public void setQuotedCurrencyCode(java.lang.String quotedCurrencyCode) {
        this.quotedCurrencyCode = quotedCurrencyCode;
    }


    /**
     * Gets the quotedAmount value for this AddPaymentToBookingRequestData.
     * 
     * @return quotedAmount
     */
    public java.math.BigDecimal getQuotedAmount() {
        return quotedAmount;
    }


    /**
     * Sets the quotedAmount value for this AddPaymentToBookingRequestData.
     * 
     * @param quotedAmount
     */
    public void setQuotedAmount(java.math.BigDecimal quotedAmount) {
        this.quotedAmount = quotedAmount;
    }


    /**
     * Gets the status value for this AddPaymentToBookingRequestData.
     * 
     * @return status
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this AddPaymentToBookingRequestData.
     * 
     * @param status
     */
    public void setStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus status) {
        this.status = status;
    }


    /**
     * Gets the accountNumberID value for this AddPaymentToBookingRequestData.
     * 
     * @return accountNumberID
     */
    public java.lang.Long getAccountNumberID() {
        return accountNumberID;
    }


    /**
     * Sets the accountNumberID value for this AddPaymentToBookingRequestData.
     * 
     * @param accountNumberID
     */
    public void setAccountNumberID(java.lang.Long accountNumberID) {
        this.accountNumberID = accountNumberID;
    }


    /**
     * Gets the accountNumber value for this AddPaymentToBookingRequestData.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this AddPaymentToBookingRequestData.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the expiration value for this AddPaymentToBookingRequestData.
     * 
     * @return expiration
     */
    public java.util.Calendar getExpiration() {
        return expiration;
    }


    /**
     * Sets the expiration value for this AddPaymentToBookingRequestData.
     * 
     * @param expiration
     */
    public void setExpiration(java.util.Calendar expiration) {
        this.expiration = expiration;
    }


    /**
     * Gets the parentPaymentID value for this AddPaymentToBookingRequestData.
     * 
     * @return parentPaymentID
     */
    public java.lang.Long getParentPaymentID() {
        return parentPaymentID;
    }


    /**
     * Sets the parentPaymentID value for this AddPaymentToBookingRequestData.
     * 
     * @param parentPaymentID
     */
    public void setParentPaymentID(java.lang.Long parentPaymentID) {
        this.parentPaymentID = parentPaymentID;
    }


    /**
     * Gets the installments value for this AddPaymentToBookingRequestData.
     * 
     * @return installments
     */
    public java.lang.Short getInstallments() {
        return installments;
    }


    /**
     * Sets the installments value for this AddPaymentToBookingRequestData.
     * 
     * @param installments
     */
    public void setInstallments(java.lang.Short installments) {
        this.installments = installments;
    }


    /**
     * Gets the paymentText value for this AddPaymentToBookingRequestData.
     * 
     * @return paymentText
     */
    public java.lang.String getPaymentText() {
        return paymentText;
    }


    /**
     * Sets the paymentText value for this AddPaymentToBookingRequestData.
     * 
     * @param paymentText
     */
    public void setPaymentText(java.lang.String paymentText) {
        this.paymentText = paymentText;
    }


    /**
     * Gets the deposit value for this AddPaymentToBookingRequestData.
     * 
     * @return deposit
     */
    public java.lang.Boolean getDeposit() {
        return deposit;
    }


    /**
     * Sets the deposit value for this AddPaymentToBookingRequestData.
     * 
     * @param deposit
     */
    public void setDeposit(java.lang.Boolean deposit) {
        this.deposit = deposit;
    }


    /**
     * Gets the paymentFields value for this AddPaymentToBookingRequestData.
     * 
     * @return paymentFields
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField[] getPaymentFields() {
        return paymentFields;
    }


    /**
     * Sets the paymentFields value for this AddPaymentToBookingRequestData.
     * 
     * @param paymentFields
     */
    public void setPaymentFields(com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField[] paymentFields) {
        this.paymentFields = paymentFields;
    }


    /**
     * Gets the paymentAddresses value for this AddPaymentToBookingRequestData.
     * 
     * @return paymentAddresses
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress[] getPaymentAddresses() {
        return paymentAddresses;
    }


    /**
     * Sets the paymentAddresses value for this AddPaymentToBookingRequestData.
     * 
     * @param paymentAddresses
     */
    public void setPaymentAddresses(com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress[] paymentAddresses) {
        this.paymentAddresses = paymentAddresses;
    }


    /**
     * Gets the agencyAccount value for this AddPaymentToBookingRequestData.
     * 
     * @return agencyAccount
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.AgencyAccount getAgencyAccount() {
        return agencyAccount;
    }


    /**
     * Sets the agencyAccount value for this AddPaymentToBookingRequestData.
     * 
     * @param agencyAccount
     */
    public void setAgencyAccount(com.navitaire.schemas.WebServices.DataContracts.Booking.AgencyAccount agencyAccount) {
        this.agencyAccount = agencyAccount;
    }


    /**
     * Gets the creditShell value for this AddPaymentToBookingRequestData.
     * 
     * @return creditShell
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.CreditShell getCreditShell() {
        return creditShell;
    }


    /**
     * Sets the creditShell value for this AddPaymentToBookingRequestData.
     * 
     * @param creditShell
     */
    public void setCreditShell(com.navitaire.schemas.WebServices.DataContracts.Booking.CreditShell creditShell) {
        this.creditShell = creditShell;
    }


    /**
     * Gets the creditFile value for this AddPaymentToBookingRequestData.
     * 
     * @return creditFile
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.CreditFile getCreditFile() {
        return creditFile;
    }


    /**
     * Sets the creditFile value for this AddPaymentToBookingRequestData.
     * 
     * @param creditFile
     */
    public void setCreditFile(com.navitaire.schemas.WebServices.DataContracts.Booking.CreditFile creditFile) {
        this.creditFile = creditFile;
    }


    /**
     * Gets the paymentVoucher value for this AddPaymentToBookingRequestData.
     * 
     * @return paymentVoucher
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentVoucher getPaymentVoucher() {
        return paymentVoucher;
    }


    /**
     * Sets the paymentVoucher value for this AddPaymentToBookingRequestData.
     * 
     * @param paymentVoucher
     */
    public void setPaymentVoucher(com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentVoucher paymentVoucher) {
        this.paymentVoucher = paymentVoucher;
    }


    /**
     * Gets the threeDSecureRequest value for this AddPaymentToBookingRequestData.
     * 
     * @return threeDSecureRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecureRequest getThreeDSecureRequest() {
        return threeDSecureRequest;
    }


    /**
     * Sets the threeDSecureRequest value for this AddPaymentToBookingRequestData.
     * 
     * @param threeDSecureRequest
     */
    public void setThreeDSecureRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecureRequest threeDSecureRequest) {
        this.threeDSecureRequest = threeDSecureRequest;
    }


    /**
     * Gets the MCCRequest value for this AddPaymentToBookingRequestData.
     * 
     * @return MCCRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.MCCRequest getMCCRequest() {
        return MCCRequest;
    }


    /**
     * Sets the MCCRequest value for this AddPaymentToBookingRequestData.
     * 
     * @param MCCRequest
     */
    public void setMCCRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.MCCRequest MCCRequest) {
        this.MCCRequest = MCCRequest;
    }


    /**
     * Gets the authorizationCode value for this AddPaymentToBookingRequestData.
     * 
     * @return authorizationCode
     */
    public java.lang.String getAuthorizationCode() {
        return authorizationCode;
    }


    /**
     * Sets the authorizationCode value for this AddPaymentToBookingRequestData.
     * 
     * @param authorizationCode
     */
    public void setAuthorizationCode(java.lang.String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddPaymentToBookingRequestData)) return false;
        AddPaymentToBookingRequestData other = (AddPaymentToBookingRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.messageState==null && other.getMessageState()==null) || 
             (this.messageState!=null &&
              this.messageState.equals(other.getMessageState()))) &&
            ((this.waiveFee==null && other.getWaiveFee()==null) || 
             (this.waiveFee!=null &&
              this.waiveFee.equals(other.getWaiveFee()))) &&
            ((this.referenceType==null && other.getReferenceType()==null) || 
             (this.referenceType!=null &&
              this.referenceType.equals(other.getReferenceType()))) &&
            ((this.paymentMethodType==null && other.getPaymentMethodType()==null) || 
             (this.paymentMethodType!=null &&
              this.paymentMethodType.equals(other.getPaymentMethodType()))) &&
            ((this.paymentMethodCode==null && other.getPaymentMethodCode()==null) || 
             (this.paymentMethodCode!=null &&
              this.paymentMethodCode.equals(other.getPaymentMethodCode()))) &&
            ((this.quotedCurrencyCode==null && other.getQuotedCurrencyCode()==null) || 
             (this.quotedCurrencyCode!=null &&
              this.quotedCurrencyCode.equals(other.getQuotedCurrencyCode()))) &&
            ((this.quotedAmount==null && other.getQuotedAmount()==null) || 
             (this.quotedAmount!=null &&
              this.quotedAmount.equals(other.getQuotedAmount()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.accountNumberID==null && other.getAccountNumberID()==null) || 
             (this.accountNumberID!=null &&
              this.accountNumberID.equals(other.getAccountNumberID()))) &&
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.expiration==null && other.getExpiration()==null) || 
             (this.expiration!=null &&
              this.expiration.equals(other.getExpiration()))) &&
            ((this.parentPaymentID==null && other.getParentPaymentID()==null) || 
             (this.parentPaymentID!=null &&
              this.parentPaymentID.equals(other.getParentPaymentID()))) &&
            ((this.installments==null && other.getInstallments()==null) || 
             (this.installments!=null &&
              this.installments.equals(other.getInstallments()))) &&
            ((this.paymentText==null && other.getPaymentText()==null) || 
             (this.paymentText!=null &&
              this.paymentText.equals(other.getPaymentText()))) &&
            ((this.deposit==null && other.getDeposit()==null) || 
             (this.deposit!=null &&
              this.deposit.equals(other.getDeposit()))) &&
            ((this.paymentFields==null && other.getPaymentFields()==null) || 
             (this.paymentFields!=null &&
              java.util.Arrays.equals(this.paymentFields, other.getPaymentFields()))) &&
            ((this.paymentAddresses==null && other.getPaymentAddresses()==null) || 
             (this.paymentAddresses!=null &&
              java.util.Arrays.equals(this.paymentAddresses, other.getPaymentAddresses()))) &&
            ((this.agencyAccount==null && other.getAgencyAccount()==null) || 
             (this.agencyAccount!=null &&
              this.agencyAccount.equals(other.getAgencyAccount()))) &&
            ((this.creditShell==null && other.getCreditShell()==null) || 
             (this.creditShell!=null &&
              this.creditShell.equals(other.getCreditShell()))) &&
            ((this.creditFile==null && other.getCreditFile()==null) || 
             (this.creditFile!=null &&
              this.creditFile.equals(other.getCreditFile()))) &&
            ((this.paymentVoucher==null && other.getPaymentVoucher()==null) || 
             (this.paymentVoucher!=null &&
              this.paymentVoucher.equals(other.getPaymentVoucher()))) &&
            ((this.threeDSecureRequest==null && other.getThreeDSecureRequest()==null) || 
             (this.threeDSecureRequest!=null &&
              this.threeDSecureRequest.equals(other.getThreeDSecureRequest()))) &&
            ((this.MCCRequest==null && other.getMCCRequest()==null) || 
             (this.MCCRequest!=null &&
              this.MCCRequest.equals(other.getMCCRequest()))) &&
            ((this.authorizationCode==null && other.getAuthorizationCode()==null) || 
             (this.authorizationCode!=null &&
              this.authorizationCode.equals(other.getAuthorizationCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMessageState() != null) {
            _hashCode += getMessageState().hashCode();
        }
        if (getWaiveFee() != null) {
            _hashCode += getWaiveFee().hashCode();
        }
        if (getReferenceType() != null) {
            _hashCode += getReferenceType().hashCode();
        }
        if (getPaymentMethodType() != null) {
            _hashCode += getPaymentMethodType().hashCode();
        }
        if (getPaymentMethodCode() != null) {
            _hashCode += getPaymentMethodCode().hashCode();
        }
        if (getQuotedCurrencyCode() != null) {
            _hashCode += getQuotedCurrencyCode().hashCode();
        }
        if (getQuotedAmount() != null) {
            _hashCode += getQuotedAmount().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getAccountNumberID() != null) {
            _hashCode += getAccountNumberID().hashCode();
        }
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getExpiration() != null) {
            _hashCode += getExpiration().hashCode();
        }
        if (getParentPaymentID() != null) {
            _hashCode += getParentPaymentID().hashCode();
        }
        if (getInstallments() != null) {
            _hashCode += getInstallments().hashCode();
        }
        if (getPaymentText() != null) {
            _hashCode += getPaymentText().hashCode();
        }
        if (getDeposit() != null) {
            _hashCode += getDeposit().hashCode();
        }
        if (getPaymentFields() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaymentFields());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaymentFields(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaymentAddresses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaymentAddresses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaymentAddresses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAgencyAccount() != null) {
            _hashCode += getAgencyAccount().hashCode();
        }
        if (getCreditShell() != null) {
            _hashCode += getCreditShell().hashCode();
        }
        if (getCreditFile() != null) {
            _hashCode += getCreditFile().hashCode();
        }
        if (getPaymentVoucher() != null) {
            _hashCode += getPaymentVoucher().hashCode();
        }
        if (getThreeDSecureRequest() != null) {
            _hashCode += getThreeDSecureRequest().hashCode();
        }
        if (getMCCRequest() != null) {
            _hashCode += getMCCRequest().hashCode();
        }
        if (getAuthorizationCode() != null) {
            _hashCode += getAuthorizationCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddPaymentToBookingRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AddPaymentToBookingRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MessageState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "MessageState"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waiveFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaiveFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReferenceType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentReferenceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentMethodType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "RequestPaymentMethodType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentMethodCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quotedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QuotedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quotedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QuotedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingPaymentStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumberID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AccountNumberID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expiration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Expiration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentPaymentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ParentPaymentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("installments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Installments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deposit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Deposit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentField"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentField"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentAddresses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddresses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddress"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddress"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agencyAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AgencyAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AgencyAccount"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditShell");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreditShell"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreditShell"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditFile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreditFile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreditFile"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentVoucher");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentVoucher"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentVoucher"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("threeDSecureRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ThreeDSecureRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ThreeDSecureRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MCCRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MCCRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MCCRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AuthorizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
