/**
 * SellJourneyByKeyRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SellJourneyByKeyRequestData  implements java.io.Serializable {
    private java.lang.String actionStatusCode;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList[] journeySellKeys;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType[] paxPriceType;

    private java.lang.String currencyCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS;

    private java.lang.Short paxCount;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale;

    private org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter;

    private java.lang.Boolean isAllotmentMarketFare;

    public SellJourneyByKeyRequestData() {
    }

    public SellJourneyByKeyRequestData(
           java.lang.String actionStatusCode,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList[] journeySellKeys,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType[] paxPriceType,
           java.lang.String currencyCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS,
           java.lang.Short paxCount,
           com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale,
           org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter,
           java.lang.Boolean isAllotmentMarketFare) {
           this.actionStatusCode = actionStatusCode;
           this.journeySellKeys = journeySellKeys;
           this.paxPriceType = paxPriceType;
           this.currencyCode = currencyCode;
           this.sourcePOS = sourcePOS;
           this.paxCount = paxCount;
           this.typeOfSale = typeOfSale;
           this.loyaltyFilter = loyaltyFilter;
           this.isAllotmentMarketFare = isAllotmentMarketFare;
    }


    /**
     * Gets the actionStatusCode value for this SellJourneyByKeyRequestData.
     * 
     * @return actionStatusCode
     */
    public java.lang.String getActionStatusCode() {
        return actionStatusCode;
    }


    /**
     * Sets the actionStatusCode value for this SellJourneyByKeyRequestData.
     * 
     * @param actionStatusCode
     */
    public void setActionStatusCode(java.lang.String actionStatusCode) {
        this.actionStatusCode = actionStatusCode;
    }


    /**
     * Gets the journeySellKeys value for this SellJourneyByKeyRequestData.
     * 
     * @return journeySellKeys
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList[] getJourneySellKeys() {
        return journeySellKeys;
    }


    /**
     * Sets the journeySellKeys value for this SellJourneyByKeyRequestData.
     * 
     * @param journeySellKeys
     */
    public void setJourneySellKeys(com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList[] journeySellKeys) {
        this.journeySellKeys = journeySellKeys;
    }


    /**
     * Gets the paxPriceType value for this SellJourneyByKeyRequestData.
     * 
     * @return paxPriceType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType[] getPaxPriceType() {
        return paxPriceType;
    }


    /**
     * Sets the paxPriceType value for this SellJourneyByKeyRequestData.
     * 
     * @param paxPriceType
     */
    public void setPaxPriceType(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType[] paxPriceType) {
        this.paxPriceType = paxPriceType;
    }


    /**
     * Gets the currencyCode value for this SellJourneyByKeyRequestData.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this SellJourneyByKeyRequestData.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the sourcePOS value for this SellJourneyByKeyRequestData.
     * 
     * @return sourcePOS
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePOS() {
        return sourcePOS;
    }


    /**
     * Sets the sourcePOS value for this SellJourneyByKeyRequestData.
     * 
     * @param sourcePOS
     */
    public void setSourcePOS(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePOS) {
        this.sourcePOS = sourcePOS;
    }


    /**
     * Gets the paxCount value for this SellJourneyByKeyRequestData.
     * 
     * @return paxCount
     */
    public java.lang.Short getPaxCount() {
        return paxCount;
    }


    /**
     * Sets the paxCount value for this SellJourneyByKeyRequestData.
     * 
     * @param paxCount
     */
    public void setPaxCount(java.lang.Short paxCount) {
        this.paxCount = paxCount;
    }


    /**
     * Gets the typeOfSale value for this SellJourneyByKeyRequestData.
     * 
     * @return typeOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale getTypeOfSale() {
        return typeOfSale;
    }


    /**
     * Sets the typeOfSale value for this SellJourneyByKeyRequestData.
     * 
     * @param typeOfSale
     */
    public void setTypeOfSale(com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale) {
        this.typeOfSale = typeOfSale;
    }


    /**
     * Gets the loyaltyFilter value for this SellJourneyByKeyRequestData.
     * 
     * @return loyaltyFilter
     */
    public org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter getLoyaltyFilter() {
        return loyaltyFilter;
    }


    /**
     * Sets the loyaltyFilter value for this SellJourneyByKeyRequestData.
     * 
     * @param loyaltyFilter
     */
    public void setLoyaltyFilter(org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter loyaltyFilter) {
        this.loyaltyFilter = loyaltyFilter;
    }


    /**
     * Gets the isAllotmentMarketFare value for this SellJourneyByKeyRequestData.
     * 
     * @return isAllotmentMarketFare
     */
    public java.lang.Boolean getIsAllotmentMarketFare() {
        return isAllotmentMarketFare;
    }


    /**
     * Sets the isAllotmentMarketFare value for this SellJourneyByKeyRequestData.
     * 
     * @param isAllotmentMarketFare
     */
    public void setIsAllotmentMarketFare(java.lang.Boolean isAllotmentMarketFare) {
        this.isAllotmentMarketFare = isAllotmentMarketFare;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SellJourneyByKeyRequestData)) return false;
        SellJourneyByKeyRequestData other = (SellJourneyByKeyRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actionStatusCode==null && other.getActionStatusCode()==null) || 
             (this.actionStatusCode!=null &&
              this.actionStatusCode.equals(other.getActionStatusCode()))) &&
            ((this.journeySellKeys==null && other.getJourneySellKeys()==null) || 
             (this.journeySellKeys!=null &&
              java.util.Arrays.equals(this.journeySellKeys, other.getJourneySellKeys()))) &&
            ((this.paxPriceType==null && other.getPaxPriceType()==null) || 
             (this.paxPriceType!=null &&
              java.util.Arrays.equals(this.paxPriceType, other.getPaxPriceType()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.sourcePOS==null && other.getSourcePOS()==null) || 
             (this.sourcePOS!=null &&
              this.sourcePOS.equals(other.getSourcePOS()))) &&
            ((this.paxCount==null && other.getPaxCount()==null) || 
             (this.paxCount!=null &&
              this.paxCount.equals(other.getPaxCount()))) &&
            ((this.typeOfSale==null && other.getTypeOfSale()==null) || 
             (this.typeOfSale!=null &&
              this.typeOfSale.equals(other.getTypeOfSale()))) &&
            ((this.loyaltyFilter==null && other.getLoyaltyFilter()==null) || 
             (this.loyaltyFilter!=null &&
              this.loyaltyFilter.equals(other.getLoyaltyFilter()))) &&
            ((this.isAllotmentMarketFare==null && other.getIsAllotmentMarketFare()==null) || 
             (this.isAllotmentMarketFare!=null &&
              this.isAllotmentMarketFare.equals(other.getIsAllotmentMarketFare())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActionStatusCode() != null) {
            _hashCode += getActionStatusCode().hashCode();
        }
        if (getJourneySellKeys() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJourneySellKeys());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJourneySellKeys(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaxPriceType() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaxPriceType());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaxPriceType(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getSourcePOS() != null) {
            _hashCode += getSourcePOS().hashCode();
        }
        if (getPaxCount() != null) {
            _hashCode += getPaxCount().hashCode();
        }
        if (getTypeOfSale() != null) {
            _hashCode += getTypeOfSale().hashCode();
        }
        if (getLoyaltyFilter() != null) {
            _hashCode += getLoyaltyFilter().hashCode();
        }
        if (getIsAllotmentMarketFare() != null) {
            _hashCode += getIsAllotmentMarketFare().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SellJourneyByKeyRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyByKeyRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeySellKeys");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneySellKeys"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellKeyList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellKeyList"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxPriceType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePOS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePOS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loyaltyFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LoyaltyFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LoyaltyFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isAllotmentMarketFare");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "IsAllotmentMarketFare"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
