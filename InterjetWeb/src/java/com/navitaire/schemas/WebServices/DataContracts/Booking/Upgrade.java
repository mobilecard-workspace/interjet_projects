/**
 * Upgrade.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class Upgrade  implements java.io.Serializable {
    private java.lang.String originalClassOfService;

    private java.lang.String upgradeClassOfService;

    private java.lang.Short available;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee passengerFee;

    public Upgrade() {
    }

    public Upgrade(
           java.lang.String originalClassOfService,
           java.lang.String upgradeClassOfService,
           java.lang.Short available,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee passengerFee) {
           this.originalClassOfService = originalClassOfService;
           this.upgradeClassOfService = upgradeClassOfService;
           this.available = available;
           this.passengerFee = passengerFee;
    }


    /**
     * Gets the originalClassOfService value for this Upgrade.
     * 
     * @return originalClassOfService
     */
    public java.lang.String getOriginalClassOfService() {
        return originalClassOfService;
    }


    /**
     * Sets the originalClassOfService value for this Upgrade.
     * 
     * @param originalClassOfService
     */
    public void setOriginalClassOfService(java.lang.String originalClassOfService) {
        this.originalClassOfService = originalClassOfService;
    }


    /**
     * Gets the upgradeClassOfService value for this Upgrade.
     * 
     * @return upgradeClassOfService
     */
    public java.lang.String getUpgradeClassOfService() {
        return upgradeClassOfService;
    }


    /**
     * Sets the upgradeClassOfService value for this Upgrade.
     * 
     * @param upgradeClassOfService
     */
    public void setUpgradeClassOfService(java.lang.String upgradeClassOfService) {
        this.upgradeClassOfService = upgradeClassOfService;
    }


    /**
     * Gets the available value for this Upgrade.
     * 
     * @return available
     */
    public java.lang.Short getAvailable() {
        return available;
    }


    /**
     * Sets the available value for this Upgrade.
     * 
     * @param available
     */
    public void setAvailable(java.lang.Short available) {
        this.available = available;
    }


    /**
     * Gets the passengerFee value for this Upgrade.
     * 
     * @return passengerFee
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee getPassengerFee() {
        return passengerFee;
    }


    /**
     * Sets the passengerFee value for this Upgrade.
     * 
     * @param passengerFee
     */
    public void setPassengerFee(com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee passengerFee) {
        this.passengerFee = passengerFee;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Upgrade)) return false;
        Upgrade other = (Upgrade) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.originalClassOfService==null && other.getOriginalClassOfService()==null) || 
             (this.originalClassOfService!=null &&
              this.originalClassOfService.equals(other.getOriginalClassOfService()))) &&
            ((this.upgradeClassOfService==null && other.getUpgradeClassOfService()==null) || 
             (this.upgradeClassOfService!=null &&
              this.upgradeClassOfService.equals(other.getUpgradeClassOfService()))) &&
            ((this.available==null && other.getAvailable()==null) || 
             (this.available!=null &&
              this.available.equals(other.getAvailable()))) &&
            ((this.passengerFee==null && other.getPassengerFee()==null) || 
             (this.passengerFee!=null &&
              this.passengerFee.equals(other.getPassengerFee())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOriginalClassOfService() != null) {
            _hashCode += getOriginalClassOfService().hashCode();
        }
        if (getUpgradeClassOfService() != null) {
            _hashCode += getUpgradeClassOfService().hashCode();
        }
        if (getAvailable() != null) {
            _hashCode += getAvailable().hashCode();
        }
        if (getPassengerFee() != null) {
            _hashCode += getPassengerFee().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Upgrade.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Upgrade"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalClassOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OriginalClassOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("upgradeClassOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeClassOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("available");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Available"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
