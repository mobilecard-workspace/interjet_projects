/**
 * FeeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class FeeType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected FeeType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _All = "All";
    public static final java.lang.String _Tax = "Tax";
    public static final java.lang.String _TravelFee = "TravelFee";
    public static final java.lang.String _ServiceFee = "ServiceFee";
    public static final java.lang.String _PaymentFee = "PaymentFee";
    public static final java.lang.String _PenaltyFee = "PenaltyFee";
    public static final java.lang.String _SSRFee = "SSRFee";
    public static final java.lang.String _NonFlightServiceFee = "NonFlightServiceFee";
    public static final java.lang.String _UpgradeFee = "UpgradeFee";
    public static final java.lang.String _SeatFee = "SeatFee";
    public static final java.lang.String _BaseFare = "BaseFare";
    public static final java.lang.String _SpoilageFee = "SpoilageFee";
    public static final java.lang.String _NameChangeFee = "NameChangeFee";
    public static final java.lang.String _ConvenienceFee = "ConvenienceFee";
    public static final java.lang.String _BaggageFee = "BaggageFee";
    public static final java.lang.String _FareSurcharge = "FareSurcharge";
    public static final java.lang.String _PromotionDiscount = "PromotionDiscount";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final FeeType All = new FeeType(_All);
    public static final FeeType Tax = new FeeType(_Tax);
    public static final FeeType TravelFee = new FeeType(_TravelFee);
    public static final FeeType ServiceFee = new FeeType(_ServiceFee);
    public static final FeeType PaymentFee = new FeeType(_PaymentFee);
    public static final FeeType PenaltyFee = new FeeType(_PenaltyFee);
    public static final FeeType SSRFee = new FeeType(_SSRFee);
    public static final FeeType NonFlightServiceFee = new FeeType(_NonFlightServiceFee);
    public static final FeeType UpgradeFee = new FeeType(_UpgradeFee);
    public static final FeeType SeatFee = new FeeType(_SeatFee);
    public static final FeeType BaseFare = new FeeType(_BaseFare);
    public static final FeeType SpoilageFee = new FeeType(_SpoilageFee);
    public static final FeeType NameChangeFee = new FeeType(_NameChangeFee);
    public static final FeeType ConvenienceFee = new FeeType(_ConvenienceFee);
    public static final FeeType BaggageFee = new FeeType(_BaggageFee);
    public static final FeeType FareSurcharge = new FeeType(_FareSurcharge);
    public static final FeeType PromotionDiscount = new FeeType(_PromotionDiscount);
    public static final FeeType Unmapped = new FeeType(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static FeeType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        FeeType enumeration = (FeeType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static FeeType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FeeType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FeeType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
