/**
 * AlternateLowFareRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class AlternateLowFareRequest  implements java.io.Serializable {
    private java.lang.Short nightsStay;

    private java.lang.Short paxCount;

    public AlternateLowFareRequest() {
    }

    public AlternateLowFareRequest(
           java.lang.Short nightsStay,
           java.lang.Short paxCount) {
           this.nightsStay = nightsStay;
           this.paxCount = paxCount;
    }


    /**
     * Gets the nightsStay value for this AlternateLowFareRequest.
     * 
     * @return nightsStay
     */
    public java.lang.Short getNightsStay() {
        return nightsStay;
    }


    /**
     * Sets the nightsStay value for this AlternateLowFareRequest.
     * 
     * @param nightsStay
     */
    public void setNightsStay(java.lang.Short nightsStay) {
        this.nightsStay = nightsStay;
    }


    /**
     * Gets the paxCount value for this AlternateLowFareRequest.
     * 
     * @return paxCount
     */
    public java.lang.Short getPaxCount() {
        return paxCount;
    }


    /**
     * Sets the paxCount value for this AlternateLowFareRequest.
     * 
     * @param paxCount
     */
    public void setPaxCount(java.lang.Short paxCount) {
        this.paxCount = paxCount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AlternateLowFareRequest)) return false;
        AlternateLowFareRequest other = (AlternateLowFareRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nightsStay==null && other.getNightsStay()==null) || 
             (this.nightsStay!=null &&
              this.nightsStay.equals(other.getNightsStay()))) &&
            ((this.paxCount==null && other.getPaxCount()==null) || 
             (this.paxCount!=null &&
              this.paxCount.equals(other.getPaxCount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNightsStay() != null) {
            _hashCode += getNightsStay().hashCode();
        }
        if (getPaxCount() != null) {
            _hashCode += getPaxCount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AlternateLowFareRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFareRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nightsStay");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NightsStay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
