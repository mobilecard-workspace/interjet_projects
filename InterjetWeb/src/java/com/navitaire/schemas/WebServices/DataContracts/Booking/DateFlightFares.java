/**
 * DateFlightFares.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DateFlightFares  implements java.io.Serializable {
    private java.lang.Short nightsStay;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFare[] dateFlightFareList;

    public DateFlightFares() {
    }

    public DateFlightFares(
           java.lang.Short nightsStay,
           com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFare[] dateFlightFareList) {
           this.nightsStay = nightsStay;
           this.dateFlightFareList = dateFlightFareList;
    }


    /**
     * Gets the nightsStay value for this DateFlightFares.
     * 
     * @return nightsStay
     */
    public java.lang.Short getNightsStay() {
        return nightsStay;
    }


    /**
     * Sets the nightsStay value for this DateFlightFares.
     * 
     * @param nightsStay
     */
    public void setNightsStay(java.lang.Short nightsStay) {
        this.nightsStay = nightsStay;
    }


    /**
     * Gets the dateFlightFareList value for this DateFlightFares.
     * 
     * @return dateFlightFareList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFare[] getDateFlightFareList() {
        return dateFlightFareList;
    }


    /**
     * Sets the dateFlightFareList value for this DateFlightFares.
     * 
     * @param dateFlightFareList
     */
    public void setDateFlightFareList(com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFare[] dateFlightFareList) {
        this.dateFlightFareList = dateFlightFareList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DateFlightFares)) return false;
        DateFlightFares other = (DateFlightFares) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nightsStay==null && other.getNightsStay()==null) || 
             (this.nightsStay!=null &&
              this.nightsStay.equals(other.getNightsStay()))) &&
            ((this.dateFlightFareList==null && other.getDateFlightFareList()==null) || 
             (this.dateFlightFareList!=null &&
              java.util.Arrays.equals(this.dateFlightFareList, other.getDateFlightFareList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNightsStay() != null) {
            _hashCode += getNightsStay().hashCode();
        }
        if (getDateFlightFareList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDateFlightFareList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDateFlightFareList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DateFlightFares.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFares"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nightsStay");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NightsStay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateFlightFareList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFareList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFare"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFare"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
