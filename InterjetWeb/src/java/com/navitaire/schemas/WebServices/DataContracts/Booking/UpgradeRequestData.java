/**
 * UpgradeRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class UpgradeRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeSegmentRequest[] upgradeSegmentRequests;

    private java.lang.String collectedCurrencyCode;

    public UpgradeRequestData() {
    }

    public UpgradeRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeSegmentRequest[] upgradeSegmentRequests,
           java.lang.String collectedCurrencyCode) {
           this.upgradeSegmentRequests = upgradeSegmentRequests;
           this.collectedCurrencyCode = collectedCurrencyCode;
    }


    /**
     * Gets the upgradeSegmentRequests value for this UpgradeRequestData.
     * 
     * @return upgradeSegmentRequests
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeSegmentRequest[] getUpgradeSegmentRequests() {
        return upgradeSegmentRequests;
    }


    /**
     * Sets the upgradeSegmentRequests value for this UpgradeRequestData.
     * 
     * @param upgradeSegmentRequests
     */
    public void setUpgradeSegmentRequests(com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeSegmentRequest[] upgradeSegmentRequests) {
        this.upgradeSegmentRequests = upgradeSegmentRequests;
    }


    /**
     * Gets the collectedCurrencyCode value for this UpgradeRequestData.
     * 
     * @return collectedCurrencyCode
     */
    public java.lang.String getCollectedCurrencyCode() {
        return collectedCurrencyCode;
    }


    /**
     * Sets the collectedCurrencyCode value for this UpgradeRequestData.
     * 
     * @param collectedCurrencyCode
     */
    public void setCollectedCurrencyCode(java.lang.String collectedCurrencyCode) {
        this.collectedCurrencyCode = collectedCurrencyCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpgradeRequestData)) return false;
        UpgradeRequestData other = (UpgradeRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.upgradeSegmentRequests==null && other.getUpgradeSegmentRequests()==null) || 
             (this.upgradeSegmentRequests!=null &&
              java.util.Arrays.equals(this.upgradeSegmentRequests, other.getUpgradeSegmentRequests()))) &&
            ((this.collectedCurrencyCode==null && other.getCollectedCurrencyCode()==null) || 
             (this.collectedCurrencyCode!=null &&
              this.collectedCurrencyCode.equals(other.getCollectedCurrencyCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUpgradeSegmentRequests() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUpgradeSegmentRequests());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUpgradeSegmentRequests(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCollectedCurrencyCode() != null) {
            _hashCode += getCollectedCurrencyCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpgradeRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("upgradeSegmentRequests");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegmentRequests"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegmentRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegmentRequest"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectedCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectedCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
