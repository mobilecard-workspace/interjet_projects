/**
 * SSRAvailabilityForBookingRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SSRAvailabilityForBookingRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey[] segmentKeyList;

    private short[] passengerNumberList;

    private java.lang.Boolean inventoryControlled;

    private java.lang.Boolean nonInventoryControlled;

    private java.lang.Boolean seatDependent;

    private java.lang.Boolean nonSeatDependent;

    private java.lang.String currencyCode;

    public SSRAvailabilityForBookingRequest() {
    }

    public SSRAvailabilityForBookingRequest(
           com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey[] segmentKeyList,
           short[] passengerNumberList,
           java.lang.Boolean inventoryControlled,
           java.lang.Boolean nonInventoryControlled,
           java.lang.Boolean seatDependent,
           java.lang.Boolean nonSeatDependent,
           java.lang.String currencyCode) {
           this.segmentKeyList = segmentKeyList;
           this.passengerNumberList = passengerNumberList;
           this.inventoryControlled = inventoryControlled;
           this.nonInventoryControlled = nonInventoryControlled;
           this.seatDependent = seatDependent;
           this.nonSeatDependent = nonSeatDependent;
           this.currencyCode = currencyCode;
    }


    /**
     * Gets the segmentKeyList value for this SSRAvailabilityForBookingRequest.
     * 
     * @return segmentKeyList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey[] getSegmentKeyList() {
        return segmentKeyList;
    }


    /**
     * Sets the segmentKeyList value for this SSRAvailabilityForBookingRequest.
     * 
     * @param segmentKeyList
     */
    public void setSegmentKeyList(com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey[] segmentKeyList) {
        this.segmentKeyList = segmentKeyList;
    }


    /**
     * Gets the passengerNumberList value for this SSRAvailabilityForBookingRequest.
     * 
     * @return passengerNumberList
     */
    public short[] getPassengerNumberList() {
        return passengerNumberList;
    }


    /**
     * Sets the passengerNumberList value for this SSRAvailabilityForBookingRequest.
     * 
     * @param passengerNumberList
     */
    public void setPassengerNumberList(short[] passengerNumberList) {
        this.passengerNumberList = passengerNumberList;
    }


    /**
     * Gets the inventoryControlled value for this SSRAvailabilityForBookingRequest.
     * 
     * @return inventoryControlled
     */
    public java.lang.Boolean getInventoryControlled() {
        return inventoryControlled;
    }


    /**
     * Sets the inventoryControlled value for this SSRAvailabilityForBookingRequest.
     * 
     * @param inventoryControlled
     */
    public void setInventoryControlled(java.lang.Boolean inventoryControlled) {
        this.inventoryControlled = inventoryControlled;
    }


    /**
     * Gets the nonInventoryControlled value for this SSRAvailabilityForBookingRequest.
     * 
     * @return nonInventoryControlled
     */
    public java.lang.Boolean getNonInventoryControlled() {
        return nonInventoryControlled;
    }


    /**
     * Sets the nonInventoryControlled value for this SSRAvailabilityForBookingRequest.
     * 
     * @param nonInventoryControlled
     */
    public void setNonInventoryControlled(java.lang.Boolean nonInventoryControlled) {
        this.nonInventoryControlled = nonInventoryControlled;
    }


    /**
     * Gets the seatDependent value for this SSRAvailabilityForBookingRequest.
     * 
     * @return seatDependent
     */
    public java.lang.Boolean getSeatDependent() {
        return seatDependent;
    }


    /**
     * Sets the seatDependent value for this SSRAvailabilityForBookingRequest.
     * 
     * @param seatDependent
     */
    public void setSeatDependent(java.lang.Boolean seatDependent) {
        this.seatDependent = seatDependent;
    }


    /**
     * Gets the nonSeatDependent value for this SSRAvailabilityForBookingRequest.
     * 
     * @return nonSeatDependent
     */
    public java.lang.Boolean getNonSeatDependent() {
        return nonSeatDependent;
    }


    /**
     * Sets the nonSeatDependent value for this SSRAvailabilityForBookingRequest.
     * 
     * @param nonSeatDependent
     */
    public void setNonSeatDependent(java.lang.Boolean nonSeatDependent) {
        this.nonSeatDependent = nonSeatDependent;
    }


    /**
     * Gets the currencyCode value for this SSRAvailabilityForBookingRequest.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this SSRAvailabilityForBookingRequest.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SSRAvailabilityForBookingRequest)) return false;
        SSRAvailabilityForBookingRequest other = (SSRAvailabilityForBookingRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.segmentKeyList==null && other.getSegmentKeyList()==null) || 
             (this.segmentKeyList!=null &&
              java.util.Arrays.equals(this.segmentKeyList, other.getSegmentKeyList()))) &&
            ((this.passengerNumberList==null && other.getPassengerNumberList()==null) || 
             (this.passengerNumberList!=null &&
              java.util.Arrays.equals(this.passengerNumberList, other.getPassengerNumberList()))) &&
            ((this.inventoryControlled==null && other.getInventoryControlled()==null) || 
             (this.inventoryControlled!=null &&
              this.inventoryControlled.equals(other.getInventoryControlled()))) &&
            ((this.nonInventoryControlled==null && other.getNonInventoryControlled()==null) || 
             (this.nonInventoryControlled!=null &&
              this.nonInventoryControlled.equals(other.getNonInventoryControlled()))) &&
            ((this.seatDependent==null && other.getSeatDependent()==null) || 
             (this.seatDependent!=null &&
              this.seatDependent.equals(other.getSeatDependent()))) &&
            ((this.nonSeatDependent==null && other.getNonSeatDependent()==null) || 
             (this.nonSeatDependent!=null &&
              this.nonSeatDependent.equals(other.getNonSeatDependent()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSegmentKeyList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegmentKeyList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegmentKeyList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengerNumberList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengerNumberList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengerNumberList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getInventoryControlled() != null) {
            _hashCode += getInventoryControlled().hashCode();
        }
        if (getNonInventoryControlled() != null) {
            _hashCode += getNonInventoryControlled().hashCode();
        }
        if (getSeatDependent() != null) {
            _hashCode += getSeatDependent().hashCode();
        }
        if (getNonSeatDependent() != null) {
            _hashCode += getNonSeatDependent().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SSRAvailabilityForBookingRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRAvailabilityForBookingRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentKeyList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentKeyList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegKey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegKey"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumberList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumberList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "short"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inventoryControlled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InventoryControlled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nonInventoryControlled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NonInventoryControlled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatDependent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatDependent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nonSeatDependent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "NonSeatDependent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
