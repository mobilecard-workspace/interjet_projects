/**
 * BookingServiceCharge.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingServiceCharge  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChargeType chargeType;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CollectType collectType;

    private java.lang.String chargeCode;

    private java.lang.String ticketCode;

    private java.lang.String currencyCode;

    private java.math.BigDecimal amount;

    private java.lang.String chargeDetail;

    private java.lang.String foreignCurrencyCode;

    private java.math.BigDecimal foreignAmount;

    public BookingServiceCharge() {
    }

    public BookingServiceCharge(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChargeType chargeType,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CollectType collectType,
           java.lang.String chargeCode,
           java.lang.String ticketCode,
           java.lang.String currencyCode,
           java.math.BigDecimal amount,
           java.lang.String chargeDetail,
           java.lang.String foreignCurrencyCode,
           java.math.BigDecimal foreignAmount) {
        super(
            state);
        this.chargeType = chargeType;
        this.collectType = collectType;
        this.chargeCode = chargeCode;
        this.ticketCode = ticketCode;
        this.currencyCode = currencyCode;
        this.amount = amount;
        this.chargeDetail = chargeDetail;
        this.foreignCurrencyCode = foreignCurrencyCode;
        this.foreignAmount = foreignAmount;
    }


    /**
     * Gets the chargeType value for this BookingServiceCharge.
     * 
     * @return chargeType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChargeType getChargeType() {
        return chargeType;
    }


    /**
     * Sets the chargeType value for this BookingServiceCharge.
     * 
     * @param chargeType
     */
    public void setChargeType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChargeType chargeType) {
        this.chargeType = chargeType;
    }


    /**
     * Gets the collectType value for this BookingServiceCharge.
     * 
     * @return collectType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CollectType getCollectType() {
        return collectType;
    }


    /**
     * Sets the collectType value for this BookingServiceCharge.
     * 
     * @param collectType
     */
    public void setCollectType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CollectType collectType) {
        this.collectType = collectType;
    }


    /**
     * Gets the chargeCode value for this BookingServiceCharge.
     * 
     * @return chargeCode
     */
    public java.lang.String getChargeCode() {
        return chargeCode;
    }


    /**
     * Sets the chargeCode value for this BookingServiceCharge.
     * 
     * @param chargeCode
     */
    public void setChargeCode(java.lang.String chargeCode) {
        this.chargeCode = chargeCode;
    }


    /**
     * Gets the ticketCode value for this BookingServiceCharge.
     * 
     * @return ticketCode
     */
    public java.lang.String getTicketCode() {
        return ticketCode;
    }


    /**
     * Sets the ticketCode value for this BookingServiceCharge.
     * 
     * @param ticketCode
     */
    public void setTicketCode(java.lang.String ticketCode) {
        this.ticketCode = ticketCode;
    }


    /**
     * Gets the currencyCode value for this BookingServiceCharge.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this BookingServiceCharge.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the amount value for this BookingServiceCharge.
     * 
     * @return amount
     */
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this BookingServiceCharge.
     * 
     * @param amount
     */
    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }


    /**
     * Gets the chargeDetail value for this BookingServiceCharge.
     * 
     * @return chargeDetail
     */
    public java.lang.String getChargeDetail() {
        return chargeDetail;
    }


    /**
     * Sets the chargeDetail value for this BookingServiceCharge.
     * 
     * @param chargeDetail
     */
    public void setChargeDetail(java.lang.String chargeDetail) {
        this.chargeDetail = chargeDetail;
    }


    /**
     * Gets the foreignCurrencyCode value for this BookingServiceCharge.
     * 
     * @return foreignCurrencyCode
     */
    public java.lang.String getForeignCurrencyCode() {
        return foreignCurrencyCode;
    }


    /**
     * Sets the foreignCurrencyCode value for this BookingServiceCharge.
     * 
     * @param foreignCurrencyCode
     */
    public void setForeignCurrencyCode(java.lang.String foreignCurrencyCode) {
        this.foreignCurrencyCode = foreignCurrencyCode;
    }


    /**
     * Gets the foreignAmount value for this BookingServiceCharge.
     * 
     * @return foreignAmount
     */
    public java.math.BigDecimal getForeignAmount() {
        return foreignAmount;
    }


    /**
     * Sets the foreignAmount value for this BookingServiceCharge.
     * 
     * @param foreignAmount
     */
    public void setForeignAmount(java.math.BigDecimal foreignAmount) {
        this.foreignAmount = foreignAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingServiceCharge)) return false;
        BookingServiceCharge other = (BookingServiceCharge) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.chargeType==null && other.getChargeType()==null) || 
             (this.chargeType!=null &&
              this.chargeType.equals(other.getChargeType()))) &&
            ((this.collectType==null && other.getCollectType()==null) || 
             (this.collectType!=null &&
              this.collectType.equals(other.getCollectType()))) &&
            ((this.chargeCode==null && other.getChargeCode()==null) || 
             (this.chargeCode!=null &&
              this.chargeCode.equals(other.getChargeCode()))) &&
            ((this.ticketCode==null && other.getTicketCode()==null) || 
             (this.ticketCode!=null &&
              this.ticketCode.equals(other.getTicketCode()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.chargeDetail==null && other.getChargeDetail()==null) || 
             (this.chargeDetail!=null &&
              this.chargeDetail.equals(other.getChargeDetail()))) &&
            ((this.foreignCurrencyCode==null && other.getForeignCurrencyCode()==null) || 
             (this.foreignCurrencyCode!=null &&
              this.foreignCurrencyCode.equals(other.getForeignCurrencyCode()))) &&
            ((this.foreignAmount==null && other.getForeignAmount()==null) || 
             (this.foreignAmount!=null &&
              this.foreignAmount.equals(other.getForeignAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getChargeType() != null) {
            _hashCode += getChargeType().hashCode();
        }
        if (getCollectType() != null) {
            _hashCode += getCollectType().hashCode();
        }
        if (getChargeCode() != null) {
            _hashCode += getChargeCode().hashCode();
        }
        if (getTicketCode() != null) {
            _hashCode += getTicketCode().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getChargeDetail() != null) {
            _hashCode += getChargeDetail().hashCode();
        }
        if (getForeignCurrencyCode() != null) {
            _hashCode += getForeignCurrencyCode().hashCode();
        }
        if (getForeignAmount() != null) {
            _hashCode += getForeignAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingServiceCharge.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingServiceCharge"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChargeType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "CollectType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChargeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TicketCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChargeDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ForeignCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ForeignAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
