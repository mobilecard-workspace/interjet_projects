/**
 * DateFlightFare.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class DateFlightFare  implements java.io.Serializable {
    private java.math.BigDecimal fareAmount;

    private java.math.BigDecimal taxesAndFeesAmount;

    private java.math.BigDecimal farePointAmount;

    private java.lang.Short availableCount;

    private java.lang.String[] bookingClassList;

    public DateFlightFare() {
    }

    public DateFlightFare(
           java.math.BigDecimal fareAmount,
           java.math.BigDecimal taxesAndFeesAmount,
           java.math.BigDecimal farePointAmount,
           java.lang.Short availableCount,
           java.lang.String[] bookingClassList) {
           this.fareAmount = fareAmount;
           this.taxesAndFeesAmount = taxesAndFeesAmount;
           this.farePointAmount = farePointAmount;
           this.availableCount = availableCount;
           this.bookingClassList = bookingClassList;
    }


    /**
     * Gets the fareAmount value for this DateFlightFare.
     * 
     * @return fareAmount
     */
    public java.math.BigDecimal getFareAmount() {
        return fareAmount;
    }


    /**
     * Sets the fareAmount value for this DateFlightFare.
     * 
     * @param fareAmount
     */
    public void setFareAmount(java.math.BigDecimal fareAmount) {
        this.fareAmount = fareAmount;
    }


    /**
     * Gets the taxesAndFeesAmount value for this DateFlightFare.
     * 
     * @return taxesAndFeesAmount
     */
    public java.math.BigDecimal getTaxesAndFeesAmount() {
        return taxesAndFeesAmount;
    }


    /**
     * Sets the taxesAndFeesAmount value for this DateFlightFare.
     * 
     * @param taxesAndFeesAmount
     */
    public void setTaxesAndFeesAmount(java.math.BigDecimal taxesAndFeesAmount) {
        this.taxesAndFeesAmount = taxesAndFeesAmount;
    }


    /**
     * Gets the farePointAmount value for this DateFlightFare.
     * 
     * @return farePointAmount
     */
    public java.math.BigDecimal getFarePointAmount() {
        return farePointAmount;
    }


    /**
     * Sets the farePointAmount value for this DateFlightFare.
     * 
     * @param farePointAmount
     */
    public void setFarePointAmount(java.math.BigDecimal farePointAmount) {
        this.farePointAmount = farePointAmount;
    }


    /**
     * Gets the availableCount value for this DateFlightFare.
     * 
     * @return availableCount
     */
    public java.lang.Short getAvailableCount() {
        return availableCount;
    }


    /**
     * Sets the availableCount value for this DateFlightFare.
     * 
     * @param availableCount
     */
    public void setAvailableCount(java.lang.Short availableCount) {
        this.availableCount = availableCount;
    }


    /**
     * Gets the bookingClassList value for this DateFlightFare.
     * 
     * @return bookingClassList
     */
    public java.lang.String[] getBookingClassList() {
        return bookingClassList;
    }


    /**
     * Sets the bookingClassList value for this DateFlightFare.
     * 
     * @param bookingClassList
     */
    public void setBookingClassList(java.lang.String[] bookingClassList) {
        this.bookingClassList = bookingClassList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DateFlightFare)) return false;
        DateFlightFare other = (DateFlightFare) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fareAmount==null && other.getFareAmount()==null) || 
             (this.fareAmount!=null &&
              this.fareAmount.equals(other.getFareAmount()))) &&
            ((this.taxesAndFeesAmount==null && other.getTaxesAndFeesAmount()==null) || 
             (this.taxesAndFeesAmount!=null &&
              this.taxesAndFeesAmount.equals(other.getTaxesAndFeesAmount()))) &&
            ((this.farePointAmount==null && other.getFarePointAmount()==null) || 
             (this.farePointAmount!=null &&
              this.farePointAmount.equals(other.getFarePointAmount()))) &&
            ((this.availableCount==null && other.getAvailableCount()==null) || 
             (this.availableCount!=null &&
              this.availableCount.equals(other.getAvailableCount()))) &&
            ((this.bookingClassList==null && other.getBookingClassList()==null) || 
             (this.bookingClassList!=null &&
              java.util.Arrays.equals(this.bookingClassList, other.getBookingClassList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFareAmount() != null) {
            _hashCode += getFareAmount().hashCode();
        }
        if (getTaxesAndFeesAmount() != null) {
            _hashCode += getTaxesAndFeesAmount().hashCode();
        }
        if (getFarePointAmount() != null) {
            _hashCode += getFarePointAmount().hashCode();
        }
        if (getAvailableCount() != null) {
            _hashCode += getAvailableCount().hashCode();
        }
        if (getBookingClassList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBookingClassList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBookingClassList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DateFlightFare.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFare"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxesAndFeesAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TaxesAndFeesAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("farePointAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FarePointAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailableCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingClassList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingClassList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
