/**
 * AlternateLowFare.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class AlternateLowFare  implements java.io.Serializable {
    private java.lang.String statusCode;

    private java.math.BigDecimal fareAmount;

    private java.math.BigDecimal taxesAndFeesAmount;

    private java.math.BigDecimal farePointAmount;

    public AlternateLowFare() {
    }

    public AlternateLowFare(
           java.lang.String statusCode,
           java.math.BigDecimal fareAmount,
           java.math.BigDecimal taxesAndFeesAmount,
           java.math.BigDecimal farePointAmount) {
           this.statusCode = statusCode;
           this.fareAmount = fareAmount;
           this.taxesAndFeesAmount = taxesAndFeesAmount;
           this.farePointAmount = farePointAmount;
    }


    /**
     * Gets the statusCode value for this AlternateLowFare.
     * 
     * @return statusCode
     */
    public java.lang.String getStatusCode() {
        return statusCode;
    }


    /**
     * Sets the statusCode value for this AlternateLowFare.
     * 
     * @param statusCode
     */
    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }


    /**
     * Gets the fareAmount value for this AlternateLowFare.
     * 
     * @return fareAmount
     */
    public java.math.BigDecimal getFareAmount() {
        return fareAmount;
    }


    /**
     * Sets the fareAmount value for this AlternateLowFare.
     * 
     * @param fareAmount
     */
    public void setFareAmount(java.math.BigDecimal fareAmount) {
        this.fareAmount = fareAmount;
    }


    /**
     * Gets the taxesAndFeesAmount value for this AlternateLowFare.
     * 
     * @return taxesAndFeesAmount
     */
    public java.math.BigDecimal getTaxesAndFeesAmount() {
        return taxesAndFeesAmount;
    }


    /**
     * Sets the taxesAndFeesAmount value for this AlternateLowFare.
     * 
     * @param taxesAndFeesAmount
     */
    public void setTaxesAndFeesAmount(java.math.BigDecimal taxesAndFeesAmount) {
        this.taxesAndFeesAmount = taxesAndFeesAmount;
    }


    /**
     * Gets the farePointAmount value for this AlternateLowFare.
     * 
     * @return farePointAmount
     */
    public java.math.BigDecimal getFarePointAmount() {
        return farePointAmount;
    }


    /**
     * Sets the farePointAmount value for this AlternateLowFare.
     * 
     * @param farePointAmount
     */
    public void setFarePointAmount(java.math.BigDecimal farePointAmount) {
        this.farePointAmount = farePointAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AlternateLowFare)) return false;
        AlternateLowFare other = (AlternateLowFare) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.fareAmount==null && other.getFareAmount()==null) || 
             (this.fareAmount!=null &&
              this.fareAmount.equals(other.getFareAmount()))) &&
            ((this.taxesAndFeesAmount==null && other.getTaxesAndFeesAmount()==null) || 
             (this.taxesAndFeesAmount!=null &&
              this.taxesAndFeesAmount.equals(other.getTaxesAndFeesAmount()))) &&
            ((this.farePointAmount==null && other.getFarePointAmount()==null) || 
             (this.farePointAmount!=null &&
              this.farePointAmount.equals(other.getFarePointAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        if (getFareAmount() != null) {
            _hashCode += getFareAmount().hashCode();
        }
        if (getTaxesAndFeesAmount() != null) {
            _hashCode += getTaxesAndFeesAmount().hashCode();
        }
        if (getFarePointAmount() != null) {
            _hashCode += getFarePointAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AlternateLowFare.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFare"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "StatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxesAndFeesAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TaxesAndFeesAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("farePointAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FarePointAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
