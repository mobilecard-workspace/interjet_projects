/**
 * FindBookingRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class FindBookingRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FindBookingBy findBookingBy;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContact findByContact;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByRecordLocator findByRecordLocator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByThirdPartyRecordLocator findByThirdPartyRecordLocator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByName findByName;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentID findByAgentID;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentName findByAgentName;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgencyNumber findByAgencyNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByEmailAddress findByEmailAddress;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByPhoneNumber findByPhoneNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCreditCardNumber findByCreditCardNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomerNumber findByCustomerNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContactCustomerNumber findByContactCustomerNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomer findByCustomer;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBagTag findByBagTag;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByTravelDocument findByTravelDocument;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBookingDate findByBookingDate;

    private java.lang.Integer lastID;

    private java.lang.Short pageSize;

    private java.lang.Boolean searchArchive;

    public FindBookingRequestData() {
    }

    public FindBookingRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FindBookingBy findBookingBy,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContact findByContact,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByRecordLocator findByRecordLocator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByThirdPartyRecordLocator findByThirdPartyRecordLocator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByName findByName,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentID findByAgentID,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentName findByAgentName,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgencyNumber findByAgencyNumber,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByEmailAddress findByEmailAddress,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByPhoneNumber findByPhoneNumber,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCreditCardNumber findByCreditCardNumber,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomerNumber findByCustomerNumber,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContactCustomerNumber findByContactCustomerNumber,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomer findByCustomer,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBagTag findByBagTag,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByTravelDocument findByTravelDocument,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBookingDate findByBookingDate,
           java.lang.Integer lastID,
           java.lang.Short pageSize,
           java.lang.Boolean searchArchive) {
           this.findBookingBy = findBookingBy;
           this.findByContact = findByContact;
           this.findByRecordLocator = findByRecordLocator;
           this.findByThirdPartyRecordLocator = findByThirdPartyRecordLocator;
           this.findByName = findByName;
           this.findByAgentID = findByAgentID;
           this.findByAgentName = findByAgentName;
           this.findByAgencyNumber = findByAgencyNumber;
           this.findByEmailAddress = findByEmailAddress;
           this.findByPhoneNumber = findByPhoneNumber;
           this.findByCreditCardNumber = findByCreditCardNumber;
           this.findByCustomerNumber = findByCustomerNumber;
           this.findByContactCustomerNumber = findByContactCustomerNumber;
           this.findByCustomer = findByCustomer;
           this.findByBagTag = findByBagTag;
           this.findByTravelDocument = findByTravelDocument;
           this.findByBookingDate = findByBookingDate;
           this.lastID = lastID;
           this.pageSize = pageSize;
           this.searchArchive = searchArchive;
    }


    /**
     * Gets the findBookingBy value for this FindBookingRequestData.
     * 
     * @return findBookingBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FindBookingBy getFindBookingBy() {
        return findBookingBy;
    }


    /**
     * Sets the findBookingBy value for this FindBookingRequestData.
     * 
     * @param findBookingBy
     */
    public void setFindBookingBy(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FindBookingBy findBookingBy) {
        this.findBookingBy = findBookingBy;
    }


    /**
     * Gets the findByContact value for this FindBookingRequestData.
     * 
     * @return findByContact
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContact getFindByContact() {
        return findByContact;
    }


    /**
     * Sets the findByContact value for this FindBookingRequestData.
     * 
     * @param findByContact
     */
    public void setFindByContact(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContact findByContact) {
        this.findByContact = findByContact;
    }


    /**
     * Gets the findByRecordLocator value for this FindBookingRequestData.
     * 
     * @return findByRecordLocator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByRecordLocator getFindByRecordLocator() {
        return findByRecordLocator;
    }


    /**
     * Sets the findByRecordLocator value for this FindBookingRequestData.
     * 
     * @param findByRecordLocator
     */
    public void setFindByRecordLocator(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByRecordLocator findByRecordLocator) {
        this.findByRecordLocator = findByRecordLocator;
    }


    /**
     * Gets the findByThirdPartyRecordLocator value for this FindBookingRequestData.
     * 
     * @return findByThirdPartyRecordLocator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByThirdPartyRecordLocator getFindByThirdPartyRecordLocator() {
        return findByThirdPartyRecordLocator;
    }


    /**
     * Sets the findByThirdPartyRecordLocator value for this FindBookingRequestData.
     * 
     * @param findByThirdPartyRecordLocator
     */
    public void setFindByThirdPartyRecordLocator(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByThirdPartyRecordLocator findByThirdPartyRecordLocator) {
        this.findByThirdPartyRecordLocator = findByThirdPartyRecordLocator;
    }


    /**
     * Gets the findByName value for this FindBookingRequestData.
     * 
     * @return findByName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByName getFindByName() {
        return findByName;
    }


    /**
     * Sets the findByName value for this FindBookingRequestData.
     * 
     * @param findByName
     */
    public void setFindByName(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByName findByName) {
        this.findByName = findByName;
    }


    /**
     * Gets the findByAgentID value for this FindBookingRequestData.
     * 
     * @return findByAgentID
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentID getFindByAgentID() {
        return findByAgentID;
    }


    /**
     * Sets the findByAgentID value for this FindBookingRequestData.
     * 
     * @param findByAgentID
     */
    public void setFindByAgentID(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentID findByAgentID) {
        this.findByAgentID = findByAgentID;
    }


    /**
     * Gets the findByAgentName value for this FindBookingRequestData.
     * 
     * @return findByAgentName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentName getFindByAgentName() {
        return findByAgentName;
    }


    /**
     * Sets the findByAgentName value for this FindBookingRequestData.
     * 
     * @param findByAgentName
     */
    public void setFindByAgentName(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentName findByAgentName) {
        this.findByAgentName = findByAgentName;
    }


    /**
     * Gets the findByAgencyNumber value for this FindBookingRequestData.
     * 
     * @return findByAgencyNumber
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgencyNumber getFindByAgencyNumber() {
        return findByAgencyNumber;
    }


    /**
     * Sets the findByAgencyNumber value for this FindBookingRequestData.
     * 
     * @param findByAgencyNumber
     */
    public void setFindByAgencyNumber(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgencyNumber findByAgencyNumber) {
        this.findByAgencyNumber = findByAgencyNumber;
    }


    /**
     * Gets the findByEmailAddress value for this FindBookingRequestData.
     * 
     * @return findByEmailAddress
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByEmailAddress getFindByEmailAddress() {
        return findByEmailAddress;
    }


    /**
     * Sets the findByEmailAddress value for this FindBookingRequestData.
     * 
     * @param findByEmailAddress
     */
    public void setFindByEmailAddress(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByEmailAddress findByEmailAddress) {
        this.findByEmailAddress = findByEmailAddress;
    }


    /**
     * Gets the findByPhoneNumber value for this FindBookingRequestData.
     * 
     * @return findByPhoneNumber
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByPhoneNumber getFindByPhoneNumber() {
        return findByPhoneNumber;
    }


    /**
     * Sets the findByPhoneNumber value for this FindBookingRequestData.
     * 
     * @param findByPhoneNumber
     */
    public void setFindByPhoneNumber(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByPhoneNumber findByPhoneNumber) {
        this.findByPhoneNumber = findByPhoneNumber;
    }


    /**
     * Gets the findByCreditCardNumber value for this FindBookingRequestData.
     * 
     * @return findByCreditCardNumber
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCreditCardNumber getFindByCreditCardNumber() {
        return findByCreditCardNumber;
    }


    /**
     * Sets the findByCreditCardNumber value for this FindBookingRequestData.
     * 
     * @param findByCreditCardNumber
     */
    public void setFindByCreditCardNumber(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCreditCardNumber findByCreditCardNumber) {
        this.findByCreditCardNumber = findByCreditCardNumber;
    }


    /**
     * Gets the findByCustomerNumber value for this FindBookingRequestData.
     * 
     * @return findByCustomerNumber
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomerNumber getFindByCustomerNumber() {
        return findByCustomerNumber;
    }


    /**
     * Sets the findByCustomerNumber value for this FindBookingRequestData.
     * 
     * @param findByCustomerNumber
     */
    public void setFindByCustomerNumber(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomerNumber findByCustomerNumber) {
        this.findByCustomerNumber = findByCustomerNumber;
    }


    /**
     * Gets the findByContactCustomerNumber value for this FindBookingRequestData.
     * 
     * @return findByContactCustomerNumber
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContactCustomerNumber getFindByContactCustomerNumber() {
        return findByContactCustomerNumber;
    }


    /**
     * Sets the findByContactCustomerNumber value for this FindBookingRequestData.
     * 
     * @param findByContactCustomerNumber
     */
    public void setFindByContactCustomerNumber(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContactCustomerNumber findByContactCustomerNumber) {
        this.findByContactCustomerNumber = findByContactCustomerNumber;
    }


    /**
     * Gets the findByCustomer value for this FindBookingRequestData.
     * 
     * @return findByCustomer
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomer getFindByCustomer() {
        return findByCustomer;
    }


    /**
     * Sets the findByCustomer value for this FindBookingRequestData.
     * 
     * @param findByCustomer
     */
    public void setFindByCustomer(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomer findByCustomer) {
        this.findByCustomer = findByCustomer;
    }


    /**
     * Gets the findByBagTag value for this FindBookingRequestData.
     * 
     * @return findByBagTag
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBagTag getFindByBagTag() {
        return findByBagTag;
    }


    /**
     * Sets the findByBagTag value for this FindBookingRequestData.
     * 
     * @param findByBagTag
     */
    public void setFindByBagTag(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBagTag findByBagTag) {
        this.findByBagTag = findByBagTag;
    }


    /**
     * Gets the findByTravelDocument value for this FindBookingRequestData.
     * 
     * @return findByTravelDocument
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByTravelDocument getFindByTravelDocument() {
        return findByTravelDocument;
    }


    /**
     * Sets the findByTravelDocument value for this FindBookingRequestData.
     * 
     * @param findByTravelDocument
     */
    public void setFindByTravelDocument(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByTravelDocument findByTravelDocument) {
        this.findByTravelDocument = findByTravelDocument;
    }


    /**
     * Gets the findByBookingDate value for this FindBookingRequestData.
     * 
     * @return findByBookingDate
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBookingDate getFindByBookingDate() {
        return findByBookingDate;
    }


    /**
     * Sets the findByBookingDate value for this FindBookingRequestData.
     * 
     * @param findByBookingDate
     */
    public void setFindByBookingDate(com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBookingDate findByBookingDate) {
        this.findByBookingDate = findByBookingDate;
    }


    /**
     * Gets the lastID value for this FindBookingRequestData.
     * 
     * @return lastID
     */
    public java.lang.Integer getLastID() {
        return lastID;
    }


    /**
     * Sets the lastID value for this FindBookingRequestData.
     * 
     * @param lastID
     */
    public void setLastID(java.lang.Integer lastID) {
        this.lastID = lastID;
    }


    /**
     * Gets the pageSize value for this FindBookingRequestData.
     * 
     * @return pageSize
     */
    public java.lang.Short getPageSize() {
        return pageSize;
    }


    /**
     * Sets the pageSize value for this FindBookingRequestData.
     * 
     * @param pageSize
     */
    public void setPageSize(java.lang.Short pageSize) {
        this.pageSize = pageSize;
    }


    /**
     * Gets the searchArchive value for this FindBookingRequestData.
     * 
     * @return searchArchive
     */
    public java.lang.Boolean getSearchArchive() {
        return searchArchive;
    }


    /**
     * Sets the searchArchive value for this FindBookingRequestData.
     * 
     * @param searchArchive
     */
    public void setSearchArchive(java.lang.Boolean searchArchive) {
        this.searchArchive = searchArchive;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindBookingRequestData)) return false;
        FindBookingRequestData other = (FindBookingRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.findBookingBy==null && other.getFindBookingBy()==null) || 
             (this.findBookingBy!=null &&
              this.findBookingBy.equals(other.getFindBookingBy()))) &&
            ((this.findByContact==null && other.getFindByContact()==null) || 
             (this.findByContact!=null &&
              this.findByContact.equals(other.getFindByContact()))) &&
            ((this.findByRecordLocator==null && other.getFindByRecordLocator()==null) || 
             (this.findByRecordLocator!=null &&
              this.findByRecordLocator.equals(other.getFindByRecordLocator()))) &&
            ((this.findByThirdPartyRecordLocator==null && other.getFindByThirdPartyRecordLocator()==null) || 
             (this.findByThirdPartyRecordLocator!=null &&
              this.findByThirdPartyRecordLocator.equals(other.getFindByThirdPartyRecordLocator()))) &&
            ((this.findByName==null && other.getFindByName()==null) || 
             (this.findByName!=null &&
              this.findByName.equals(other.getFindByName()))) &&
            ((this.findByAgentID==null && other.getFindByAgentID()==null) || 
             (this.findByAgentID!=null &&
              this.findByAgentID.equals(other.getFindByAgentID()))) &&
            ((this.findByAgentName==null && other.getFindByAgentName()==null) || 
             (this.findByAgentName!=null &&
              this.findByAgentName.equals(other.getFindByAgentName()))) &&
            ((this.findByAgencyNumber==null && other.getFindByAgencyNumber()==null) || 
             (this.findByAgencyNumber!=null &&
              this.findByAgencyNumber.equals(other.getFindByAgencyNumber()))) &&
            ((this.findByEmailAddress==null && other.getFindByEmailAddress()==null) || 
             (this.findByEmailAddress!=null &&
              this.findByEmailAddress.equals(other.getFindByEmailAddress()))) &&
            ((this.findByPhoneNumber==null && other.getFindByPhoneNumber()==null) || 
             (this.findByPhoneNumber!=null &&
              this.findByPhoneNumber.equals(other.getFindByPhoneNumber()))) &&
            ((this.findByCreditCardNumber==null && other.getFindByCreditCardNumber()==null) || 
             (this.findByCreditCardNumber!=null &&
              this.findByCreditCardNumber.equals(other.getFindByCreditCardNumber()))) &&
            ((this.findByCustomerNumber==null && other.getFindByCustomerNumber()==null) || 
             (this.findByCustomerNumber!=null &&
              this.findByCustomerNumber.equals(other.getFindByCustomerNumber()))) &&
            ((this.findByContactCustomerNumber==null && other.getFindByContactCustomerNumber()==null) || 
             (this.findByContactCustomerNumber!=null &&
              this.findByContactCustomerNumber.equals(other.getFindByContactCustomerNumber()))) &&
            ((this.findByCustomer==null && other.getFindByCustomer()==null) || 
             (this.findByCustomer!=null &&
              this.findByCustomer.equals(other.getFindByCustomer()))) &&
            ((this.findByBagTag==null && other.getFindByBagTag()==null) || 
             (this.findByBagTag!=null &&
              this.findByBagTag.equals(other.getFindByBagTag()))) &&
            ((this.findByTravelDocument==null && other.getFindByTravelDocument()==null) || 
             (this.findByTravelDocument!=null &&
              this.findByTravelDocument.equals(other.getFindByTravelDocument()))) &&
            ((this.findByBookingDate==null && other.getFindByBookingDate()==null) || 
             (this.findByBookingDate!=null &&
              this.findByBookingDate.equals(other.getFindByBookingDate()))) &&
            ((this.lastID==null && other.getLastID()==null) || 
             (this.lastID!=null &&
              this.lastID.equals(other.getLastID()))) &&
            ((this.pageSize==null && other.getPageSize()==null) || 
             (this.pageSize!=null &&
              this.pageSize.equals(other.getPageSize()))) &&
            ((this.searchArchive==null && other.getSearchArchive()==null) || 
             (this.searchArchive!=null &&
              this.searchArchive.equals(other.getSearchArchive())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFindBookingBy() != null) {
            _hashCode += getFindBookingBy().hashCode();
        }
        if (getFindByContact() != null) {
            _hashCode += getFindByContact().hashCode();
        }
        if (getFindByRecordLocator() != null) {
            _hashCode += getFindByRecordLocator().hashCode();
        }
        if (getFindByThirdPartyRecordLocator() != null) {
            _hashCode += getFindByThirdPartyRecordLocator().hashCode();
        }
        if (getFindByName() != null) {
            _hashCode += getFindByName().hashCode();
        }
        if (getFindByAgentID() != null) {
            _hashCode += getFindByAgentID().hashCode();
        }
        if (getFindByAgentName() != null) {
            _hashCode += getFindByAgentName().hashCode();
        }
        if (getFindByAgencyNumber() != null) {
            _hashCode += getFindByAgencyNumber().hashCode();
        }
        if (getFindByEmailAddress() != null) {
            _hashCode += getFindByEmailAddress().hashCode();
        }
        if (getFindByPhoneNumber() != null) {
            _hashCode += getFindByPhoneNumber().hashCode();
        }
        if (getFindByCreditCardNumber() != null) {
            _hashCode += getFindByCreditCardNumber().hashCode();
        }
        if (getFindByCustomerNumber() != null) {
            _hashCode += getFindByCustomerNumber().hashCode();
        }
        if (getFindByContactCustomerNumber() != null) {
            _hashCode += getFindByContactCustomerNumber().hashCode();
        }
        if (getFindByCustomer() != null) {
            _hashCode += getFindByCustomer().hashCode();
        }
        if (getFindByBagTag() != null) {
            _hashCode += getFindByBagTag().hashCode();
        }
        if (getFindByTravelDocument() != null) {
            _hashCode += getFindByTravelDocument().hashCode();
        }
        if (getFindByBookingDate() != null) {
            _hashCode += getFindByBookingDate().hashCode();
        }
        if (getLastID() != null) {
            _hashCode += getLastID().hashCode();
        }
        if (getPageSize() != null) {
            _hashCode += getPageSize().hashCode();
        }
        if (getSearchArchive() != null) {
            _hashCode += getSearchArchive().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindBookingRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findBookingBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FindBookingBy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByContact");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByContact"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByContact"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByRecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByThirdPartyRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByThirdPartyRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByThirdPartyRecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByAgentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByAgentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByAgentID"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByAgentName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByAgentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByAgentName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByAgencyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByAgencyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByAgencyNumber"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByEmailAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByEmailAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByEmailAddress"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByPhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByPhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByPhoneNumber"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByCreditCardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByCreditCardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByCreditCardNumber"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByCustomerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByCustomerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByCustomerNumber"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByContactCustomerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByContactCustomerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByContactCustomerNumber"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByCustomer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByCustomer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByCustomer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByBagTag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByBagTag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByBagTag"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByTravelDocument");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByTravelDocument"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByTravelDocument"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findByBookingDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByBookingDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByBookingDate"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LastID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageSize");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PageSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchArchive");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SearchArchive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
