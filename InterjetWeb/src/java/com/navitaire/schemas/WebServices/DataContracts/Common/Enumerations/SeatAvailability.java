/**
 * SeatAvailability.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class SeatAvailability implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SeatAvailability(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Unknown = "Unknown";
    public static final java.lang.String _Reserved = "Reserved";
    public static final java.lang.String _Blocked = "Blocked";
    public static final java.lang.String _HeldForAnotherSession = "HeldForAnotherSession";
    public static final java.lang.String _HeldForThisSession = "HeldForThisSession";
    public static final java.lang.String _Open = "Open";
    public static final java.lang.String _Missing = "Missing";
    public static final java.lang.String _NotVisible = "NotVisible";
    public static final java.lang.String _CheckedIn = "CheckedIn";
    public static final java.lang.String _FleetBlocked = "FleetBlocked";
    public static final java.lang.String _Restricted = "Restricted";
    public static final java.lang.String _Broken = "Broken";
    public static final java.lang.String _ReservedForPNR = "ReservedForPNR";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final SeatAvailability Unknown = new SeatAvailability(_Unknown);
    public static final SeatAvailability Reserved = new SeatAvailability(_Reserved);
    public static final SeatAvailability Blocked = new SeatAvailability(_Blocked);
    public static final SeatAvailability HeldForAnotherSession = new SeatAvailability(_HeldForAnotherSession);
    public static final SeatAvailability HeldForThisSession = new SeatAvailability(_HeldForThisSession);
    public static final SeatAvailability Open = new SeatAvailability(_Open);
    public static final SeatAvailability Missing = new SeatAvailability(_Missing);
    public static final SeatAvailability NotVisible = new SeatAvailability(_NotVisible);
    public static final SeatAvailability CheckedIn = new SeatAvailability(_CheckedIn);
    public static final SeatAvailability FleetBlocked = new SeatAvailability(_FleetBlocked);
    public static final SeatAvailability Restricted = new SeatAvailability(_Restricted);
    public static final SeatAvailability Broken = new SeatAvailability(_Broken);
    public static final SeatAvailability ReservedForPNR = new SeatAvailability(_ReservedForPNR);
    public static final SeatAvailability Unmapped = new SeatAvailability(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static SeatAvailability fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SeatAvailability enumeration = (SeatAvailability)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SeatAvailability fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SeatAvailability.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SeatAvailability"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
