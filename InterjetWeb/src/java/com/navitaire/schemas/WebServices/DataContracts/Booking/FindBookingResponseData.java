/**
 * FindBookingResponseData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class FindBookingResponseData  extends com.navitaire.schemas.WebServices.DataContracts.Booking.ServiceMessage  implements java.io.Serializable {
    private java.lang.Integer records;

    private java.lang.Long endingID;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingData[] findBookingDataList;

    public FindBookingResponseData() {
    }

    public FindBookingResponseData(
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInfoList,
           java.lang.Integer records,
           java.lang.Long endingID,
           com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingData[] findBookingDataList) {
        super(
            otherServiceInfoList);
        this.records = records;
        this.endingID = endingID;
        this.findBookingDataList = findBookingDataList;
    }


    /**
     * Gets the records value for this FindBookingResponseData.
     * 
     * @return records
     */
    public java.lang.Integer getRecords() {
        return records;
    }


    /**
     * Sets the records value for this FindBookingResponseData.
     * 
     * @param records
     */
    public void setRecords(java.lang.Integer records) {
        this.records = records;
    }


    /**
     * Gets the endingID value for this FindBookingResponseData.
     * 
     * @return endingID
     */
    public java.lang.Long getEndingID() {
        return endingID;
    }


    /**
     * Sets the endingID value for this FindBookingResponseData.
     * 
     * @param endingID
     */
    public void setEndingID(java.lang.Long endingID) {
        this.endingID = endingID;
    }


    /**
     * Gets the findBookingDataList value for this FindBookingResponseData.
     * 
     * @return findBookingDataList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingData[] getFindBookingDataList() {
        return findBookingDataList;
    }


    /**
     * Sets the findBookingDataList value for this FindBookingResponseData.
     * 
     * @param findBookingDataList
     */
    public void setFindBookingDataList(com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingData[] findBookingDataList) {
        this.findBookingDataList = findBookingDataList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindBookingResponseData)) return false;
        FindBookingResponseData other = (FindBookingResponseData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.records==null && other.getRecords()==null) || 
             (this.records!=null &&
              this.records.equals(other.getRecords()))) &&
            ((this.endingID==null && other.getEndingID()==null) || 
             (this.endingID!=null &&
              this.endingID.equals(other.getEndingID()))) &&
            ((this.findBookingDataList==null && other.getFindBookingDataList()==null) || 
             (this.findBookingDataList!=null &&
              java.util.Arrays.equals(this.findBookingDataList, other.getFindBookingDataList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRecords() != null) {
            _hashCode += getRecords().hashCode();
        }
        if (getEndingID() != null) {
            _hashCode += getEndingID().hashCode();
        }
        if (getFindBookingDataList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFindBookingDataList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFindBookingDataList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindBookingResponseData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingResponseData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("records");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Records"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endingID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EndingID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findBookingDataList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingDataList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingData"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
