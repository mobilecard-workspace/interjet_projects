/**
 * BookingQueueInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingQueueInfo  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Long passengerID;

    private java.lang.Long watchListID;

    private java.lang.String queueCode;

    private java.lang.String notes;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueEventType queueEventType;

    private java.lang.String queueName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueAction queueAction;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueMode queueMode;

    private java.lang.Long bookingQueueID;

    private java.lang.Long bookingID;

    private java.lang.String segmentKey;

    private java.lang.String subQueueCode;

    public BookingQueueInfo() {
    }

    public BookingQueueInfo(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Long passengerID,
           java.lang.Long watchListID,
           java.lang.String queueCode,
           java.lang.String notes,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueEventType queueEventType,
           java.lang.String queueName,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueAction queueAction,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueMode queueMode,
           java.lang.Long bookingQueueID,
           java.lang.Long bookingID,
           java.lang.String segmentKey,
           java.lang.String subQueueCode) {
        super(
            state);
        this.passengerID = passengerID;
        this.watchListID = watchListID;
        this.queueCode = queueCode;
        this.notes = notes;
        this.queueEventType = queueEventType;
        this.queueName = queueName;
        this.queueAction = queueAction;
        this.queueMode = queueMode;
        this.bookingQueueID = bookingQueueID;
        this.bookingID = bookingID;
        this.segmentKey = segmentKey;
        this.subQueueCode = subQueueCode;
    }


    /**
     * Gets the passengerID value for this BookingQueueInfo.
     * 
     * @return passengerID
     */
    public java.lang.Long getPassengerID() {
        return passengerID;
    }


    /**
     * Sets the passengerID value for this BookingQueueInfo.
     * 
     * @param passengerID
     */
    public void setPassengerID(java.lang.Long passengerID) {
        this.passengerID = passengerID;
    }


    /**
     * Gets the watchListID value for this BookingQueueInfo.
     * 
     * @return watchListID
     */
    public java.lang.Long getWatchListID() {
        return watchListID;
    }


    /**
     * Sets the watchListID value for this BookingQueueInfo.
     * 
     * @param watchListID
     */
    public void setWatchListID(java.lang.Long watchListID) {
        this.watchListID = watchListID;
    }


    /**
     * Gets the queueCode value for this BookingQueueInfo.
     * 
     * @return queueCode
     */
    public java.lang.String getQueueCode() {
        return queueCode;
    }


    /**
     * Sets the queueCode value for this BookingQueueInfo.
     * 
     * @param queueCode
     */
    public void setQueueCode(java.lang.String queueCode) {
        this.queueCode = queueCode;
    }


    /**
     * Gets the notes value for this BookingQueueInfo.
     * 
     * @return notes
     */
    public java.lang.String getNotes() {
        return notes;
    }


    /**
     * Sets the notes value for this BookingQueueInfo.
     * 
     * @param notes
     */
    public void setNotes(java.lang.String notes) {
        this.notes = notes;
    }


    /**
     * Gets the queueEventType value for this BookingQueueInfo.
     * 
     * @return queueEventType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueEventType getQueueEventType() {
        return queueEventType;
    }


    /**
     * Sets the queueEventType value for this BookingQueueInfo.
     * 
     * @param queueEventType
     */
    public void setQueueEventType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueEventType queueEventType) {
        this.queueEventType = queueEventType;
    }


    /**
     * Gets the queueName value for this BookingQueueInfo.
     * 
     * @return queueName
     */
    public java.lang.String getQueueName() {
        return queueName;
    }


    /**
     * Sets the queueName value for this BookingQueueInfo.
     * 
     * @param queueName
     */
    public void setQueueName(java.lang.String queueName) {
        this.queueName = queueName;
    }


    /**
     * Gets the queueAction value for this BookingQueueInfo.
     * 
     * @return queueAction
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueAction getQueueAction() {
        return queueAction;
    }


    /**
     * Sets the queueAction value for this BookingQueueInfo.
     * 
     * @param queueAction
     */
    public void setQueueAction(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueAction queueAction) {
        this.queueAction = queueAction;
    }


    /**
     * Gets the queueMode value for this BookingQueueInfo.
     * 
     * @return queueMode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueMode getQueueMode() {
        return queueMode;
    }


    /**
     * Sets the queueMode value for this BookingQueueInfo.
     * 
     * @param queueMode
     */
    public void setQueueMode(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueMode queueMode) {
        this.queueMode = queueMode;
    }


    /**
     * Gets the bookingQueueID value for this BookingQueueInfo.
     * 
     * @return bookingQueueID
     */
    public java.lang.Long getBookingQueueID() {
        return bookingQueueID;
    }


    /**
     * Sets the bookingQueueID value for this BookingQueueInfo.
     * 
     * @param bookingQueueID
     */
    public void setBookingQueueID(java.lang.Long bookingQueueID) {
        this.bookingQueueID = bookingQueueID;
    }


    /**
     * Gets the bookingID value for this BookingQueueInfo.
     * 
     * @return bookingID
     */
    public java.lang.Long getBookingID() {
        return bookingID;
    }


    /**
     * Sets the bookingID value for this BookingQueueInfo.
     * 
     * @param bookingID
     */
    public void setBookingID(java.lang.Long bookingID) {
        this.bookingID = bookingID;
    }


    /**
     * Gets the segmentKey value for this BookingQueueInfo.
     * 
     * @return segmentKey
     */
    public java.lang.String getSegmentKey() {
        return segmentKey;
    }


    /**
     * Sets the segmentKey value for this BookingQueueInfo.
     * 
     * @param segmentKey
     */
    public void setSegmentKey(java.lang.String segmentKey) {
        this.segmentKey = segmentKey;
    }


    /**
     * Gets the subQueueCode value for this BookingQueueInfo.
     * 
     * @return subQueueCode
     */
    public java.lang.String getSubQueueCode() {
        return subQueueCode;
    }


    /**
     * Sets the subQueueCode value for this BookingQueueInfo.
     * 
     * @param subQueueCode
     */
    public void setSubQueueCode(java.lang.String subQueueCode) {
        this.subQueueCode = subQueueCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingQueueInfo)) return false;
        BookingQueueInfo other = (BookingQueueInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.passengerID==null && other.getPassengerID()==null) || 
             (this.passengerID!=null &&
              this.passengerID.equals(other.getPassengerID()))) &&
            ((this.watchListID==null && other.getWatchListID()==null) || 
             (this.watchListID!=null &&
              this.watchListID.equals(other.getWatchListID()))) &&
            ((this.queueCode==null && other.getQueueCode()==null) || 
             (this.queueCode!=null &&
              this.queueCode.equals(other.getQueueCode()))) &&
            ((this.notes==null && other.getNotes()==null) || 
             (this.notes!=null &&
              this.notes.equals(other.getNotes()))) &&
            ((this.queueEventType==null && other.getQueueEventType()==null) || 
             (this.queueEventType!=null &&
              this.queueEventType.equals(other.getQueueEventType()))) &&
            ((this.queueName==null && other.getQueueName()==null) || 
             (this.queueName!=null &&
              this.queueName.equals(other.getQueueName()))) &&
            ((this.queueAction==null && other.getQueueAction()==null) || 
             (this.queueAction!=null &&
              this.queueAction.equals(other.getQueueAction()))) &&
            ((this.queueMode==null && other.getQueueMode()==null) || 
             (this.queueMode!=null &&
              this.queueMode.equals(other.getQueueMode()))) &&
            ((this.bookingQueueID==null && other.getBookingQueueID()==null) || 
             (this.bookingQueueID!=null &&
              this.bookingQueueID.equals(other.getBookingQueueID()))) &&
            ((this.bookingID==null && other.getBookingID()==null) || 
             (this.bookingID!=null &&
              this.bookingID.equals(other.getBookingID()))) &&
            ((this.segmentKey==null && other.getSegmentKey()==null) || 
             (this.segmentKey!=null &&
              this.segmentKey.equals(other.getSegmentKey()))) &&
            ((this.subQueueCode==null && other.getSubQueueCode()==null) || 
             (this.subQueueCode!=null &&
              this.subQueueCode.equals(other.getSubQueueCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPassengerID() != null) {
            _hashCode += getPassengerID().hashCode();
        }
        if (getWatchListID() != null) {
            _hashCode += getWatchListID().hashCode();
        }
        if (getQueueCode() != null) {
            _hashCode += getQueueCode().hashCode();
        }
        if (getNotes() != null) {
            _hashCode += getNotes().hashCode();
        }
        if (getQueueEventType() != null) {
            _hashCode += getQueueEventType().hashCode();
        }
        if (getQueueName() != null) {
            _hashCode += getQueueName().hashCode();
        }
        if (getQueueAction() != null) {
            _hashCode += getQueueAction().hashCode();
        }
        if (getQueueMode() != null) {
            _hashCode += getQueueMode().hashCode();
        }
        if (getBookingQueueID() != null) {
            _hashCode += getBookingQueueID().hashCode();
        }
        if (getBookingID() != null) {
            _hashCode += getBookingID().hashCode();
        }
        if (getSegmentKey() != null) {
            _hashCode += getSegmentKey().hashCode();
        }
        if (getSubQueueCode() != null) {
            _hashCode += getSubQueueCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingQueueInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingQueueInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("watchListID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WatchListID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queueCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QueueCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Notes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queueEventType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QueueEventType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "QueueEventType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queueName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QueueName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queueAction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QueueAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "QueueAction"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queueMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "QueueMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "QueueMode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingQueueID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingQueueID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segmentKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subQueueCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SubQueueCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
