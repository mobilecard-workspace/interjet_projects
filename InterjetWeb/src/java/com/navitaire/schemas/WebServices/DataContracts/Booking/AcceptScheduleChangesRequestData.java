/**
 * AcceptScheduleChangesRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class AcceptScheduleChangesRequestData  implements java.io.Serializable {
    private java.lang.String recordLocator;

    private java.lang.Boolean acceptAllChanges;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.Segment[] segments;

    private java.lang.String currentQueueCode;

    private java.lang.String errorQueueCode;

    public AcceptScheduleChangesRequestData() {
    }

    public AcceptScheduleChangesRequestData(
           java.lang.String recordLocator,
           java.lang.Boolean acceptAllChanges,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Segment[] segments,
           java.lang.String currentQueueCode,
           java.lang.String errorQueueCode) {
           this.recordLocator = recordLocator;
           this.acceptAllChanges = acceptAllChanges;
           this.segments = segments;
           this.currentQueueCode = currentQueueCode;
           this.errorQueueCode = errorQueueCode;
    }


    /**
     * Gets the recordLocator value for this AcceptScheduleChangesRequestData.
     * 
     * @return recordLocator
     */
    public java.lang.String getRecordLocator() {
        return recordLocator;
    }


    /**
     * Sets the recordLocator value for this AcceptScheduleChangesRequestData.
     * 
     * @param recordLocator
     */
    public void setRecordLocator(java.lang.String recordLocator) {
        this.recordLocator = recordLocator;
    }


    /**
     * Gets the acceptAllChanges value for this AcceptScheduleChangesRequestData.
     * 
     * @return acceptAllChanges
     */
    public java.lang.Boolean getAcceptAllChanges() {
        return acceptAllChanges;
    }


    /**
     * Sets the acceptAllChanges value for this AcceptScheduleChangesRequestData.
     * 
     * @param acceptAllChanges
     */
    public void setAcceptAllChanges(java.lang.Boolean acceptAllChanges) {
        this.acceptAllChanges = acceptAllChanges;
    }


    /**
     * Gets the segments value for this AcceptScheduleChangesRequestData.
     * 
     * @return segments
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Segment[] getSegments() {
        return segments;
    }


    /**
     * Sets the segments value for this AcceptScheduleChangesRequestData.
     * 
     * @param segments
     */
    public void setSegments(com.navitaire.schemas.WebServices.DataContracts.Booking.Segment[] segments) {
        this.segments = segments;
    }


    /**
     * Gets the currentQueueCode value for this AcceptScheduleChangesRequestData.
     * 
     * @return currentQueueCode
     */
    public java.lang.String getCurrentQueueCode() {
        return currentQueueCode;
    }


    /**
     * Sets the currentQueueCode value for this AcceptScheduleChangesRequestData.
     * 
     * @param currentQueueCode
     */
    public void setCurrentQueueCode(java.lang.String currentQueueCode) {
        this.currentQueueCode = currentQueueCode;
    }


    /**
     * Gets the errorQueueCode value for this AcceptScheduleChangesRequestData.
     * 
     * @return errorQueueCode
     */
    public java.lang.String getErrorQueueCode() {
        return errorQueueCode;
    }


    /**
     * Sets the errorQueueCode value for this AcceptScheduleChangesRequestData.
     * 
     * @param errorQueueCode
     */
    public void setErrorQueueCode(java.lang.String errorQueueCode) {
        this.errorQueueCode = errorQueueCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AcceptScheduleChangesRequestData)) return false;
        AcceptScheduleChangesRequestData other = (AcceptScheduleChangesRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.recordLocator==null && other.getRecordLocator()==null) || 
             (this.recordLocator!=null &&
              this.recordLocator.equals(other.getRecordLocator()))) &&
            ((this.acceptAllChanges==null && other.getAcceptAllChanges()==null) || 
             (this.acceptAllChanges!=null &&
              this.acceptAllChanges.equals(other.getAcceptAllChanges()))) &&
            ((this.segments==null && other.getSegments()==null) || 
             (this.segments!=null &&
              java.util.Arrays.equals(this.segments, other.getSegments()))) &&
            ((this.currentQueueCode==null && other.getCurrentQueueCode()==null) || 
             (this.currentQueueCode!=null &&
              this.currentQueueCode.equals(other.getCurrentQueueCode()))) &&
            ((this.errorQueueCode==null && other.getErrorQueueCode()==null) || 
             (this.errorQueueCode!=null &&
              this.errorQueueCode.equals(other.getErrorQueueCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRecordLocator() != null) {
            _hashCode += getRecordLocator().hashCode();
        }
        if (getAcceptAllChanges() != null) {
            _hashCode += getAcceptAllChanges().hashCode();
        }
        if (getSegments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCurrentQueueCode() != null) {
            _hashCode += getCurrentQueueCode().hashCode();
        }
        if (getErrorQueueCode() != null) {
            _hashCode += getErrorQueueCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AcceptScheduleChangesRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AcceptScheduleChangesRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acceptAllChanges");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AcceptAllChanges"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentQueueCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrentQueueCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorQueueCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ErrorQueueCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
