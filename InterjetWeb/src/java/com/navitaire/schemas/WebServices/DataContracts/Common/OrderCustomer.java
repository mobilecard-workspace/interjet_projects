/**
 * OrderCustomer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class OrderCustomer  implements java.io.Serializable {
    private java.lang.String orderId;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring customerId;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring title;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring firstName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring lastName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring middleName;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address1;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address2;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring city;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring stateCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring county;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring countryCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring zipCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneHome;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneFax;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneWork;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring emailAddress;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring busOrRes;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring companyName;

    private java.lang.String field1;

    private java.lang.String field2;

    private java.lang.String field3;

    private java.lang.String field4;

    private java.lang.String field5;

    private com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfdateTime birthDate;

    public OrderCustomer() {
    }

    public OrderCustomer(
           java.lang.String orderId,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring customerId,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring title,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring firstName,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring lastName,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring middleName,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address1,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address2,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring city,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring stateCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring county,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring countryCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring zipCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneHome,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneFax,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneWork,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring emailAddress,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring busOrRes,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring companyName,
           java.lang.String field1,
           java.lang.String field2,
           java.lang.String field3,
           java.lang.String field4,
           java.lang.String field5,
           com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfdateTime birthDate) {
           this.orderId = orderId;
           this.customerId = customerId;
           this.title = title;
           this.firstName = firstName;
           this.lastName = lastName;
           this.middleName = middleName;
           this.address1 = address1;
           this.address2 = address2;
           this.city = city;
           this.stateCode = stateCode;
           this.county = county;
           this.countryCode = countryCode;
           this.zipCode = zipCode;
           this.phoneHome = phoneHome;
           this.phoneFax = phoneFax;
           this.phoneWork = phoneWork;
           this.emailAddress = emailAddress;
           this.busOrRes = busOrRes;
           this.companyName = companyName;
           this.field1 = field1;
           this.field2 = field2;
           this.field3 = field3;
           this.field4 = field4;
           this.field5 = field5;
           this.birthDate = birthDate;
    }


    /**
     * Gets the orderId value for this OrderCustomer.
     * 
     * @return orderId
     */
    public java.lang.String getOrderId() {
        return orderId;
    }


    /**
     * Sets the orderId value for this OrderCustomer.
     * 
     * @param orderId
     */
    public void setOrderId(java.lang.String orderId) {
        this.orderId = orderId;
    }


    /**
     * Gets the customerId value for this OrderCustomer.
     * 
     * @return customerId
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getCustomerId() {
        return customerId;
    }


    /**
     * Sets the customerId value for this OrderCustomer.
     * 
     * @param customerId
     */
    public void setCustomerId(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring customerId) {
        this.customerId = customerId;
    }


    /**
     * Gets the title value for this OrderCustomer.
     * 
     * @return title
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getTitle() {
        return title;
    }


    /**
     * Sets the title value for this OrderCustomer.
     * 
     * @param title
     */
    public void setTitle(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring title) {
        this.title = title;
    }


    /**
     * Gets the firstName value for this OrderCustomer.
     * 
     * @return firstName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this OrderCustomer.
     * 
     * @param firstName
     */
    public void setFirstName(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the lastName value for this OrderCustomer.
     * 
     * @return lastName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this OrderCustomer.
     * 
     * @param lastName
     */
    public void setLastName(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the middleName value for this OrderCustomer.
     * 
     * @return middleName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getMiddleName() {
        return middleName;
    }


    /**
     * Sets the middleName value for this OrderCustomer.
     * 
     * @param middleName
     */
    public void setMiddleName(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring middleName) {
        this.middleName = middleName;
    }


    /**
     * Gets the address1 value for this OrderCustomer.
     * 
     * @return address1
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getAddress1() {
        return address1;
    }


    /**
     * Sets the address1 value for this OrderCustomer.
     * 
     * @param address1
     */
    public void setAddress1(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address1) {
        this.address1 = address1;
    }


    /**
     * Gets the address2 value for this OrderCustomer.
     * 
     * @return address2
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getAddress2() {
        return address2;
    }


    /**
     * Sets the address2 value for this OrderCustomer.
     * 
     * @param address2
     */
    public void setAddress2(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring address2) {
        this.address2 = address2;
    }


    /**
     * Gets the city value for this OrderCustomer.
     * 
     * @return city
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getCity() {
        return city;
    }


    /**
     * Sets the city value for this OrderCustomer.
     * 
     * @param city
     */
    public void setCity(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring city) {
        this.city = city;
    }


    /**
     * Gets the stateCode value for this OrderCustomer.
     * 
     * @return stateCode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getStateCode() {
        return stateCode;
    }


    /**
     * Sets the stateCode value for this OrderCustomer.
     * 
     * @param stateCode
     */
    public void setStateCode(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring stateCode) {
        this.stateCode = stateCode;
    }


    /**
     * Gets the county value for this OrderCustomer.
     * 
     * @return county
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getCounty() {
        return county;
    }


    /**
     * Sets the county value for this OrderCustomer.
     * 
     * @param county
     */
    public void setCounty(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring county) {
        this.county = county;
    }


    /**
     * Gets the countryCode value for this OrderCustomer.
     * 
     * @return countryCode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this OrderCustomer.
     * 
     * @param countryCode
     */
    public void setCountryCode(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * Gets the zipCode value for this OrderCustomer.
     * 
     * @return zipCode
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getZipCode() {
        return zipCode;
    }


    /**
     * Sets the zipCode value for this OrderCustomer.
     * 
     * @param zipCode
     */
    public void setZipCode(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring zipCode) {
        this.zipCode = zipCode;
    }


    /**
     * Gets the phoneHome value for this OrderCustomer.
     * 
     * @return phoneHome
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getPhoneHome() {
        return phoneHome;
    }


    /**
     * Sets the phoneHome value for this OrderCustomer.
     * 
     * @param phoneHome
     */
    public void setPhoneHome(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneHome) {
        this.phoneHome = phoneHome;
    }


    /**
     * Gets the phoneFax value for this OrderCustomer.
     * 
     * @return phoneFax
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getPhoneFax() {
        return phoneFax;
    }


    /**
     * Sets the phoneFax value for this OrderCustomer.
     * 
     * @param phoneFax
     */
    public void setPhoneFax(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneFax) {
        this.phoneFax = phoneFax;
    }


    /**
     * Gets the phoneWork value for this OrderCustomer.
     * 
     * @return phoneWork
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getPhoneWork() {
        return phoneWork;
    }


    /**
     * Sets the phoneWork value for this OrderCustomer.
     * 
     * @param phoneWork
     */
    public void setPhoneWork(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring phoneWork) {
        this.phoneWork = phoneWork;
    }


    /**
     * Gets the emailAddress value for this OrderCustomer.
     * 
     * @return emailAddress
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getEmailAddress() {
        return emailAddress;
    }


    /**
     * Sets the emailAddress value for this OrderCustomer.
     * 
     * @param emailAddress
     */
    public void setEmailAddress(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring emailAddress) {
        this.emailAddress = emailAddress;
    }


    /**
     * Gets the busOrRes value for this OrderCustomer.
     * 
     * @return busOrRes
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getBusOrRes() {
        return busOrRes;
    }


    /**
     * Sets the busOrRes value for this OrderCustomer.
     * 
     * @param busOrRes
     */
    public void setBusOrRes(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring busOrRes) {
        this.busOrRes = busOrRes;
    }


    /**
     * Gets the companyName value for this OrderCustomer.
     * 
     * @return companyName
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring getCompanyName() {
        return companyName;
    }


    /**
     * Sets the companyName value for this OrderCustomer.
     * 
     * @param companyName
     */
    public void setCompanyName(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring companyName) {
        this.companyName = companyName;
    }


    /**
     * Gets the field1 value for this OrderCustomer.
     * 
     * @return field1
     */
    public java.lang.String getField1() {
        return field1;
    }


    /**
     * Sets the field1 value for this OrderCustomer.
     * 
     * @param field1
     */
    public void setField1(java.lang.String field1) {
        this.field1 = field1;
    }


    /**
     * Gets the field2 value for this OrderCustomer.
     * 
     * @return field2
     */
    public java.lang.String getField2() {
        return field2;
    }


    /**
     * Sets the field2 value for this OrderCustomer.
     * 
     * @param field2
     */
    public void setField2(java.lang.String field2) {
        this.field2 = field2;
    }


    /**
     * Gets the field3 value for this OrderCustomer.
     * 
     * @return field3
     */
    public java.lang.String getField3() {
        return field3;
    }


    /**
     * Sets the field3 value for this OrderCustomer.
     * 
     * @param field3
     */
    public void setField3(java.lang.String field3) {
        this.field3 = field3;
    }


    /**
     * Gets the field4 value for this OrderCustomer.
     * 
     * @return field4
     */
    public java.lang.String getField4() {
        return field4;
    }


    /**
     * Sets the field4 value for this OrderCustomer.
     * 
     * @param field4
     */
    public void setField4(java.lang.String field4) {
        this.field4 = field4;
    }


    /**
     * Gets the field5 value for this OrderCustomer.
     * 
     * @return field5
     */
    public java.lang.String getField5() {
        return field5;
    }


    /**
     * Sets the field5 value for this OrderCustomer.
     * 
     * @param field5
     */
    public void setField5(java.lang.String field5) {
        this.field5 = field5;
    }


    /**
     * Gets the birthDate value for this OrderCustomer.
     * 
     * @return birthDate
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfdateTime getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this OrderCustomer.
     * 
     * @param birthDate
     */
    public void setBirthDate(com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfdateTime birthDate) {
        this.birthDate = birthDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderCustomer)) return false;
        OrderCustomer other = (OrderCustomer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.orderId==null && other.getOrderId()==null) || 
             (this.orderId!=null &&
              this.orderId.equals(other.getOrderId()))) &&
            ((this.customerId==null && other.getCustomerId()==null) || 
             (this.customerId!=null &&
              this.customerId.equals(other.getCustomerId()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.middleName==null && other.getMiddleName()==null) || 
             (this.middleName!=null &&
              this.middleName.equals(other.getMiddleName()))) &&
            ((this.address1==null && other.getAddress1()==null) || 
             (this.address1!=null &&
              this.address1.equals(other.getAddress1()))) &&
            ((this.address2==null && other.getAddress2()==null) || 
             (this.address2!=null &&
              this.address2.equals(other.getAddress2()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.stateCode==null && other.getStateCode()==null) || 
             (this.stateCode!=null &&
              this.stateCode.equals(other.getStateCode()))) &&
            ((this.county==null && other.getCounty()==null) || 
             (this.county!=null &&
              this.county.equals(other.getCounty()))) &&
            ((this.countryCode==null && other.getCountryCode()==null) || 
             (this.countryCode!=null &&
              this.countryCode.equals(other.getCountryCode()))) &&
            ((this.zipCode==null && other.getZipCode()==null) || 
             (this.zipCode!=null &&
              this.zipCode.equals(other.getZipCode()))) &&
            ((this.phoneHome==null && other.getPhoneHome()==null) || 
             (this.phoneHome!=null &&
              this.phoneHome.equals(other.getPhoneHome()))) &&
            ((this.phoneFax==null && other.getPhoneFax()==null) || 
             (this.phoneFax!=null &&
              this.phoneFax.equals(other.getPhoneFax()))) &&
            ((this.phoneWork==null && other.getPhoneWork()==null) || 
             (this.phoneWork!=null &&
              this.phoneWork.equals(other.getPhoneWork()))) &&
            ((this.emailAddress==null && other.getEmailAddress()==null) || 
             (this.emailAddress!=null &&
              this.emailAddress.equals(other.getEmailAddress()))) &&
            ((this.busOrRes==null && other.getBusOrRes()==null) || 
             (this.busOrRes!=null &&
              this.busOrRes.equals(other.getBusOrRes()))) &&
            ((this.companyName==null && other.getCompanyName()==null) || 
             (this.companyName!=null &&
              this.companyName.equals(other.getCompanyName()))) &&
            ((this.field1==null && other.getField1()==null) || 
             (this.field1!=null &&
              this.field1.equals(other.getField1()))) &&
            ((this.field2==null && other.getField2()==null) || 
             (this.field2!=null &&
              this.field2.equals(other.getField2()))) &&
            ((this.field3==null && other.getField3()==null) || 
             (this.field3!=null &&
              this.field3.equals(other.getField3()))) &&
            ((this.field4==null && other.getField4()==null) || 
             (this.field4!=null &&
              this.field4.equals(other.getField4()))) &&
            ((this.field5==null && other.getField5()==null) || 
             (this.field5!=null &&
              this.field5.equals(other.getField5()))) &&
            ((this.birthDate==null && other.getBirthDate()==null) || 
             (this.birthDate!=null &&
              this.birthDate.equals(other.getBirthDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOrderId() != null) {
            _hashCode += getOrderId().hashCode();
        }
        if (getCustomerId() != null) {
            _hashCode += getCustomerId().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getMiddleName() != null) {
            _hashCode += getMiddleName().hashCode();
        }
        if (getAddress1() != null) {
            _hashCode += getAddress1().hashCode();
        }
        if (getAddress2() != null) {
            _hashCode += getAddress2().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getStateCode() != null) {
            _hashCode += getStateCode().hashCode();
        }
        if (getCounty() != null) {
            _hashCode += getCounty().hashCode();
        }
        if (getCountryCode() != null) {
            _hashCode += getCountryCode().hashCode();
        }
        if (getZipCode() != null) {
            _hashCode += getZipCode().hashCode();
        }
        if (getPhoneHome() != null) {
            _hashCode += getPhoneHome().hashCode();
        }
        if (getPhoneFax() != null) {
            _hashCode += getPhoneFax().hashCode();
        }
        if (getPhoneWork() != null) {
            _hashCode += getPhoneWork().hashCode();
        }
        if (getEmailAddress() != null) {
            _hashCode += getEmailAddress().hashCode();
        }
        if (getBusOrRes() != null) {
            _hashCode += getBusOrRes().hashCode();
        }
        if (getCompanyName() != null) {
            _hashCode += getCompanyName().hashCode();
        }
        if (getField1() != null) {
            _hashCode += getField1().hashCode();
        }
        if (getField2() != null) {
            _hashCode += getField2().hashCode();
        }
        if (getField3() != null) {
            _hashCode += getField3().hashCode();
        }
        if (getField4() != null) {
            _hashCode += getField4().hashCode();
        }
        if (getField5() != null) {
            _hashCode += getField5().hashCode();
        }
        if (getBirthDate() != null) {
            _hashCode += getBirthDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderCustomer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderCustomer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CustomerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "LastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("middleName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "MiddleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Address1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Address2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "StateCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("county");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "County"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneHome");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PhoneHome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneFax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PhoneFax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneWork");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PhoneWork"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "EmailAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("busOrRes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "BusOrRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CompanyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field5");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "BirthDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfdateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
