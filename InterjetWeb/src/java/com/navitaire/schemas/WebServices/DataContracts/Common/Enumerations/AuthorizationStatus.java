/**
 * AuthorizationStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class AuthorizationStatus implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected AuthorizationStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Unknown = "Unknown";
    public static final java.lang.String _Acknowledged = "Acknowledged";
    public static final java.lang.String _Pending = "Pending";
    public static final java.lang.String _InProcess = "InProcess";
    public static final java.lang.String _Approved = "Approved";
    public static final java.lang.String _Declined = "Declined";
    public static final java.lang.String _Referral = "Referral";
    public static final java.lang.String _PickUpCard = "PickUpCard";
    public static final java.lang.String _HotCard = "HotCard";
    public static final java.lang.String _Voided = "Voided";
    public static final java.lang.String _Retrieval = "Retrieval";
    public static final java.lang.String _ChargedBack = "ChargedBack";
    public static final java.lang.String _Error = "Error";
    public static final java.lang.String _ValidationFailed = "ValidationFailed";
    public static final java.lang.String _Address = "Address";
    public static final java.lang.String _VerificationCode = "VerificationCode";
    public static final java.lang.String _FraudPrevention = "FraudPrevention";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final AuthorizationStatus Unknown = new AuthorizationStatus(_Unknown);
    public static final AuthorizationStatus Acknowledged = new AuthorizationStatus(_Acknowledged);
    public static final AuthorizationStatus Pending = new AuthorizationStatus(_Pending);
    public static final AuthorizationStatus InProcess = new AuthorizationStatus(_InProcess);
    public static final AuthorizationStatus Approved = new AuthorizationStatus(_Approved);
    public static final AuthorizationStatus Declined = new AuthorizationStatus(_Declined);
    public static final AuthorizationStatus Referral = new AuthorizationStatus(_Referral);
    public static final AuthorizationStatus PickUpCard = new AuthorizationStatus(_PickUpCard);
    public static final AuthorizationStatus HotCard = new AuthorizationStatus(_HotCard);
    public static final AuthorizationStatus Voided = new AuthorizationStatus(_Voided);
    public static final AuthorizationStatus Retrieval = new AuthorizationStatus(_Retrieval);
    public static final AuthorizationStatus ChargedBack = new AuthorizationStatus(_ChargedBack);
    public static final AuthorizationStatus Error = new AuthorizationStatus(_Error);
    public static final AuthorizationStatus ValidationFailed = new AuthorizationStatus(_ValidationFailed);
    public static final AuthorizationStatus Address = new AuthorizationStatus(_Address);
    public static final AuthorizationStatus VerificationCode = new AuthorizationStatus(_VerificationCode);
    public static final AuthorizationStatus FraudPrevention = new AuthorizationStatus(_FraudPrevention);
    public static final AuthorizationStatus Unmapped = new AuthorizationStatus(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static AuthorizationStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        AuthorizationStatus enumeration = (AuthorizationStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static AuthorizationStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthorizationStatus.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AuthorizationStatus"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
