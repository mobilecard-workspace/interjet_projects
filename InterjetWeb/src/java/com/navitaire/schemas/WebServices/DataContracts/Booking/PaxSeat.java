/**
 * PaxSeat.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxSeat  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Short passengerNumber;

    private java.lang.String arrivalStation;

    private java.lang.String departureStation;

    private java.lang.String unitDesignator;

    private java.lang.String compartmentDesignator;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatPreference seatPreference;

    private java.lang.Integer penalty;

    private java.lang.Boolean seatTogetherPreference;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatInfo paxSeatInfo;

    public PaxSeat() {
    }

    public PaxSeat(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Short passengerNumber,
           java.lang.String arrivalStation,
           java.lang.String departureStation,
           java.lang.String unitDesignator,
           java.lang.String compartmentDesignator,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatPreference seatPreference,
           java.lang.Integer penalty,
           java.lang.Boolean seatTogetherPreference,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatInfo paxSeatInfo) {
        super(
            state);
        this.passengerNumber = passengerNumber;
        this.arrivalStation = arrivalStation;
        this.departureStation = departureStation;
        this.unitDesignator = unitDesignator;
        this.compartmentDesignator = compartmentDesignator;
        this.seatPreference = seatPreference;
        this.penalty = penalty;
        this.seatTogetherPreference = seatTogetherPreference;
        this.paxSeatInfo = paxSeatInfo;
    }


    /**
     * Gets the passengerNumber value for this PaxSeat.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this PaxSeat.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the arrivalStation value for this PaxSeat.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this PaxSeat.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the departureStation value for this PaxSeat.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this PaxSeat.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the unitDesignator value for this PaxSeat.
     * 
     * @return unitDesignator
     */
    public java.lang.String getUnitDesignator() {
        return unitDesignator;
    }


    /**
     * Sets the unitDesignator value for this PaxSeat.
     * 
     * @param unitDesignator
     */
    public void setUnitDesignator(java.lang.String unitDesignator) {
        this.unitDesignator = unitDesignator;
    }


    /**
     * Gets the compartmentDesignator value for this PaxSeat.
     * 
     * @return compartmentDesignator
     */
    public java.lang.String getCompartmentDesignator() {
        return compartmentDesignator;
    }


    /**
     * Sets the compartmentDesignator value for this PaxSeat.
     * 
     * @param compartmentDesignator
     */
    public void setCompartmentDesignator(java.lang.String compartmentDesignator) {
        this.compartmentDesignator = compartmentDesignator;
    }


    /**
     * Gets the seatPreference value for this PaxSeat.
     * 
     * @return seatPreference
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatPreference getSeatPreference() {
        return seatPreference;
    }


    /**
     * Sets the seatPreference value for this PaxSeat.
     * 
     * @param seatPreference
     */
    public void setSeatPreference(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatPreference seatPreference) {
        this.seatPreference = seatPreference;
    }


    /**
     * Gets the penalty value for this PaxSeat.
     * 
     * @return penalty
     */
    public java.lang.Integer getPenalty() {
        return penalty;
    }


    /**
     * Sets the penalty value for this PaxSeat.
     * 
     * @param penalty
     */
    public void setPenalty(java.lang.Integer penalty) {
        this.penalty = penalty;
    }


    /**
     * Gets the seatTogetherPreference value for this PaxSeat.
     * 
     * @return seatTogetherPreference
     */
    public java.lang.Boolean getSeatTogetherPreference() {
        return seatTogetherPreference;
    }


    /**
     * Sets the seatTogetherPreference value for this PaxSeat.
     * 
     * @param seatTogetherPreference
     */
    public void setSeatTogetherPreference(java.lang.Boolean seatTogetherPreference) {
        this.seatTogetherPreference = seatTogetherPreference;
    }


    /**
     * Gets the paxSeatInfo value for this PaxSeat.
     * 
     * @return paxSeatInfo
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatInfo getPaxSeatInfo() {
        return paxSeatInfo;
    }


    /**
     * Sets the paxSeatInfo value for this PaxSeat.
     * 
     * @param paxSeatInfo
     */
    public void setPaxSeatInfo(com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatInfo paxSeatInfo) {
        this.paxSeatInfo = paxSeatInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxSeat)) return false;
        PaxSeat other = (PaxSeat) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.unitDesignator==null && other.getUnitDesignator()==null) || 
             (this.unitDesignator!=null &&
              this.unitDesignator.equals(other.getUnitDesignator()))) &&
            ((this.compartmentDesignator==null && other.getCompartmentDesignator()==null) || 
             (this.compartmentDesignator!=null &&
              this.compartmentDesignator.equals(other.getCompartmentDesignator()))) &&
            ((this.seatPreference==null && other.getSeatPreference()==null) || 
             (this.seatPreference!=null &&
              this.seatPreference.equals(other.getSeatPreference()))) &&
            ((this.penalty==null && other.getPenalty()==null) || 
             (this.penalty!=null &&
              this.penalty.equals(other.getPenalty()))) &&
            ((this.seatTogetherPreference==null && other.getSeatTogetherPreference()==null) || 
             (this.seatTogetherPreference!=null &&
              this.seatTogetherPreference.equals(other.getSeatTogetherPreference()))) &&
            ((this.paxSeatInfo==null && other.getPaxSeatInfo()==null) || 
             (this.paxSeatInfo!=null &&
              this.paxSeatInfo.equals(other.getPaxSeatInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getUnitDesignator() != null) {
            _hashCode += getUnitDesignator().hashCode();
        }
        if (getCompartmentDesignator() != null) {
            _hashCode += getCompartmentDesignator().hashCode();
        }
        if (getSeatPreference() != null) {
            _hashCode += getSeatPreference().hashCode();
        }
        if (getPenalty() != null) {
            _hashCode += getPenalty().hashCode();
        }
        if (getSeatTogetherPreference() != null) {
            _hashCode += getSeatTogetherPreference().hashCode();
        }
        if (getPaxSeatInfo() != null) {
            _hashCode += getPaxSeatInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxSeat.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeat"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UnitDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("compartmentDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatPreference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatPreference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SeatPreference"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("penalty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Penalty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatTogetherPreference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatTogetherPreference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxSeatInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
