/**
 * AvailableFare.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class AvailableFare  extends com.navitaire.schemas.WebServices.DataContracts.Booking.Fare  implements java.io.Serializable {
    private java.lang.Short availableCount;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ClassStatus status;

    public AvailableFare() {
    }

    public AvailableFare(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String classOfService,
           java.lang.String classType,
           java.lang.String ruleTariff,
           java.lang.String carrierCode,
           java.lang.String ruleNumber,
           java.lang.String fareBasisCode,
           java.lang.Short fareSequence,
           java.lang.String fareClassOfService,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareStatus fareStatus,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareApplicationType fareApplicationType,
           java.lang.String originalClassOfService,
           java.lang.String xrefClassOfService,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PaxFare[] paxFares,
           java.lang.String productClass,
           java.lang.Boolean isAllotmentMarketFare,
           java.lang.String travelClassCode,
           java.lang.String fareSellKey,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound inboundOutbound,
           java.lang.Short availableCount,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ClassStatus status) {
        super(
            state,
            classOfService,
            classType,
            ruleTariff,
            carrierCode,
            ruleNumber,
            fareBasisCode,
            fareSequence,
            fareClassOfService,
            fareStatus,
            fareApplicationType,
            originalClassOfService,
            xrefClassOfService,
            paxFares,
            productClass,
            isAllotmentMarketFare,
            travelClassCode,
            fareSellKey,
            inboundOutbound);
        this.availableCount = availableCount;
        this.status = status;
    }


    /**
     * Gets the availableCount value for this AvailableFare.
     * 
     * @return availableCount
     */
    public java.lang.Short getAvailableCount() {
        return availableCount;
    }


    /**
     * Sets the availableCount value for this AvailableFare.
     * 
     * @param availableCount
     */
    public void setAvailableCount(java.lang.Short availableCount) {
        this.availableCount = availableCount;
    }


    /**
     * Gets the status value for this AvailableFare.
     * 
     * @return status
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ClassStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this AvailableFare.
     * 
     * @param status
     */
    public void setStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ClassStatus status) {
        this.status = status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AvailableFare)) return false;
        AvailableFare other = (AvailableFare) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.availableCount==null && other.getAvailableCount()==null) || 
             (this.availableCount!=null &&
              this.availableCount.equals(other.getAvailableCount()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAvailableCount() != null) {
            _hashCode += getAvailableCount().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AvailableFare.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailableFare"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailableCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ClassStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
