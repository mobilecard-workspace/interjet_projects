/**
 * UpdateSourcePointOfSaleRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class UpdateSourcePointOfSaleRequest  implements java.io.Serializable {
    private java.lang.Boolean forceChangeInState;

    private com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePointOfSale;

    public UpdateSourcePointOfSaleRequest() {
    }

    public UpdateSourcePointOfSaleRequest(
           java.lang.Boolean forceChangeInState,
           com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePointOfSale) {
           this.forceChangeInState = forceChangeInState;
           this.sourcePointOfSale = sourcePointOfSale;
    }


    /**
     * Gets the forceChangeInState value for this UpdateSourcePointOfSaleRequest.
     * 
     * @return forceChangeInState
     */
    public java.lang.Boolean getForceChangeInState() {
        return forceChangeInState;
    }


    /**
     * Sets the forceChangeInState value for this UpdateSourcePointOfSaleRequest.
     * 
     * @param forceChangeInState
     */
    public void setForceChangeInState(java.lang.Boolean forceChangeInState) {
        this.forceChangeInState = forceChangeInState;
    }


    /**
     * Gets the sourcePointOfSale value for this UpdateSourcePointOfSaleRequest.
     * 
     * @return sourcePointOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale getSourcePointOfSale() {
        return sourcePointOfSale;
    }


    /**
     * Sets the sourcePointOfSale value for this UpdateSourcePointOfSaleRequest.
     * 
     * @param sourcePointOfSale
     */
    public void setSourcePointOfSale(com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale sourcePointOfSale) {
        this.sourcePointOfSale = sourcePointOfSale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateSourcePointOfSaleRequest)) return false;
        UpdateSourcePointOfSaleRequest other = (UpdateSourcePointOfSaleRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.forceChangeInState==null && other.getForceChangeInState()==null) || 
             (this.forceChangeInState!=null &&
              this.forceChangeInState.equals(other.getForceChangeInState()))) &&
            ((this.sourcePointOfSale==null && other.getSourcePointOfSale()==null) || 
             (this.sourcePointOfSale!=null &&
              this.sourcePointOfSale.equals(other.getSourcePointOfSale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getForceChangeInState() != null) {
            _hashCode += getForceChangeInState().hashCode();
        }
        if (getSourcePointOfSale() != null) {
            _hashCode += getSourcePointOfSale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateSourcePointOfSaleRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateSourcePointOfSaleRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forceChangeInState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ForceChangeInState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePointOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SourcePointOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
