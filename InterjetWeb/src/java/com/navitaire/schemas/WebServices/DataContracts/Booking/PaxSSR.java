/**
 * PaxSSR.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxSSR  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String actionStatusCode;

    private java.lang.String arrivalStation;

    private java.lang.String departureStation;

    private java.lang.Short passengerNumber;

    private java.lang.String SSRCode;

    private java.lang.Short SSRNumber;

    private java.lang.String SSRDetail;

    private java.lang.String feeCode;

    private java.lang.String note;

    private java.lang.Short SSRValue;

    public PaxSSR() {
    }

    public PaxSSR(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String actionStatusCode,
           java.lang.String arrivalStation,
           java.lang.String departureStation,
           java.lang.Short passengerNumber,
           java.lang.String SSRCode,
           java.lang.Short SSRNumber,
           java.lang.String SSRDetail,
           java.lang.String feeCode,
           java.lang.String note,
           java.lang.Short SSRValue) {
        super(
            state);
        this.actionStatusCode = actionStatusCode;
        this.arrivalStation = arrivalStation;
        this.departureStation = departureStation;
        this.passengerNumber = passengerNumber;
        this.SSRCode = SSRCode;
        this.SSRNumber = SSRNumber;
        this.SSRDetail = SSRDetail;
        this.feeCode = feeCode;
        this.note = note;
        this.SSRValue = SSRValue;
    }


    /**
     * Gets the actionStatusCode value for this PaxSSR.
     * 
     * @return actionStatusCode
     */
    public java.lang.String getActionStatusCode() {
        return actionStatusCode;
    }


    /**
     * Sets the actionStatusCode value for this PaxSSR.
     * 
     * @param actionStatusCode
     */
    public void setActionStatusCode(java.lang.String actionStatusCode) {
        this.actionStatusCode = actionStatusCode;
    }


    /**
     * Gets the arrivalStation value for this PaxSSR.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this PaxSSR.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the departureStation value for this PaxSSR.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this PaxSSR.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the passengerNumber value for this PaxSSR.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this PaxSSR.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the SSRCode value for this PaxSSR.
     * 
     * @return SSRCode
     */
    public java.lang.String getSSRCode() {
        return SSRCode;
    }


    /**
     * Sets the SSRCode value for this PaxSSR.
     * 
     * @param SSRCode
     */
    public void setSSRCode(java.lang.String SSRCode) {
        this.SSRCode = SSRCode;
    }


    /**
     * Gets the SSRNumber value for this PaxSSR.
     * 
     * @return SSRNumber
     */
    public java.lang.Short getSSRNumber() {
        return SSRNumber;
    }


    /**
     * Sets the SSRNumber value for this PaxSSR.
     * 
     * @param SSRNumber
     */
    public void setSSRNumber(java.lang.Short SSRNumber) {
        this.SSRNumber = SSRNumber;
    }


    /**
     * Gets the SSRDetail value for this PaxSSR.
     * 
     * @return SSRDetail
     */
    public java.lang.String getSSRDetail() {
        return SSRDetail;
    }


    /**
     * Sets the SSRDetail value for this PaxSSR.
     * 
     * @param SSRDetail
     */
    public void setSSRDetail(java.lang.String SSRDetail) {
        this.SSRDetail = SSRDetail;
    }


    /**
     * Gets the feeCode value for this PaxSSR.
     * 
     * @return feeCode
     */
    public java.lang.String getFeeCode() {
        return feeCode;
    }


    /**
     * Sets the feeCode value for this PaxSSR.
     * 
     * @param feeCode
     */
    public void setFeeCode(java.lang.String feeCode) {
        this.feeCode = feeCode;
    }


    /**
     * Gets the note value for this PaxSSR.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this PaxSSR.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }


    /**
     * Gets the SSRValue value for this PaxSSR.
     * 
     * @return SSRValue
     */
    public java.lang.Short getSSRValue() {
        return SSRValue;
    }


    /**
     * Sets the SSRValue value for this PaxSSR.
     * 
     * @param SSRValue
     */
    public void setSSRValue(java.lang.Short SSRValue) {
        this.SSRValue = SSRValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxSSR)) return false;
        PaxSSR other = (PaxSSR) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.actionStatusCode==null && other.getActionStatusCode()==null) || 
             (this.actionStatusCode!=null &&
              this.actionStatusCode.equals(other.getActionStatusCode()))) &&
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.SSRCode==null && other.getSSRCode()==null) || 
             (this.SSRCode!=null &&
              this.SSRCode.equals(other.getSSRCode()))) &&
            ((this.SSRNumber==null && other.getSSRNumber()==null) || 
             (this.SSRNumber!=null &&
              this.SSRNumber.equals(other.getSSRNumber()))) &&
            ((this.SSRDetail==null && other.getSSRDetail()==null) || 
             (this.SSRDetail!=null &&
              this.SSRDetail.equals(other.getSSRDetail()))) &&
            ((this.feeCode==null && other.getFeeCode()==null) || 
             (this.feeCode!=null &&
              this.feeCode.equals(other.getFeeCode()))) &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote()))) &&
            ((this.SSRValue==null && other.getSSRValue()==null) || 
             (this.SSRValue!=null &&
              this.SSRValue.equals(other.getSSRValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getActionStatusCode() != null) {
            _hashCode += getActionStatusCode().hashCode();
        }
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getSSRCode() != null) {
            _hashCode += getSSRCode().hashCode();
        }
        if (getSSRNumber() != null) {
            _hashCode += getSSRNumber().hashCode();
        }
        if (getSSRDetail() != null) {
            _hashCode += getSSRDetail().hashCode();
        }
        if (getFeeCode() != null) {
            _hashCode += getFeeCode().hashCode();
        }
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        if (getSSRValue() != null) {
            _hashCode += getSSRValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxSSR.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSR"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
