/**
 * CollectType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class CollectType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected CollectType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _SellerChargeable = "SellerChargeable";
    public static final java.lang.String _ExternalChargeable = "ExternalChargeable";
    public static final java.lang.String _SellerNonChargeable = "SellerNonChargeable";
    public static final java.lang.String _ExternalNonChargeable = "ExternalNonChargeable";
    public static final java.lang.String _ExternalChargeableImmediate = "ExternalChargeableImmediate";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final CollectType SellerChargeable = new CollectType(_SellerChargeable);
    public static final CollectType ExternalChargeable = new CollectType(_ExternalChargeable);
    public static final CollectType SellerNonChargeable = new CollectType(_SellerNonChargeable);
    public static final CollectType ExternalNonChargeable = new CollectType(_ExternalNonChargeable);
    public static final CollectType ExternalChargeableImmediate = new CollectType(_ExternalChargeableImmediate);
    public static final CollectType Unmapped = new CollectType(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static CollectType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        CollectType enumeration = (CollectType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static CollectType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CollectType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "CollectType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
