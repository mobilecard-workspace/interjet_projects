/**
 * CommitRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class CommitRequestData  extends com.navitaire.schemas.WebServices.DataContracts.Booking.ServiceMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.Booking booking;

    private java.lang.Boolean restrictionOverride;

    private java.lang.Boolean changeHoldDateTime;

    private java.lang.Boolean waiveNameChangeFee;

    private java.lang.Boolean waivePenaltyFee;

    private java.lang.Boolean waiveSpoilageFee;

    private java.lang.Boolean distributeToContacts;

    public CommitRequestData() {
    }

    public CommitRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInfoList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.Booking booking,
           java.lang.Boolean restrictionOverride,
           java.lang.Boolean changeHoldDateTime,
           java.lang.Boolean waiveNameChangeFee,
           java.lang.Boolean waivePenaltyFee,
           java.lang.Boolean waiveSpoilageFee,
           java.lang.Boolean distributeToContacts) {
        super(
            otherServiceInfoList);
        this.booking = booking;
        this.restrictionOverride = restrictionOverride;
        this.changeHoldDateTime = changeHoldDateTime;
        this.waiveNameChangeFee = waiveNameChangeFee;
        this.waivePenaltyFee = waivePenaltyFee;
        this.waiveSpoilageFee = waiveSpoilageFee;
        this.distributeToContacts = distributeToContacts;
    }


    /**
     * Gets the booking value for this CommitRequestData.
     * 
     * @return booking
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.Booking getBooking() {
        return booking;
    }


    /**
     * Sets the booking value for this CommitRequestData.
     * 
     * @param booking
     */
    public void setBooking(com.navitaire.schemas.WebServices.DataContracts.Booking.Booking booking) {
        this.booking = booking;
    }


    /**
     * Gets the restrictionOverride value for this CommitRequestData.
     * 
     * @return restrictionOverride
     */
    public java.lang.Boolean getRestrictionOverride() {
        return restrictionOverride;
    }


    /**
     * Sets the restrictionOverride value for this CommitRequestData.
     * 
     * @param restrictionOverride
     */
    public void setRestrictionOverride(java.lang.Boolean restrictionOverride) {
        this.restrictionOverride = restrictionOverride;
    }


    /**
     * Gets the changeHoldDateTime value for this CommitRequestData.
     * 
     * @return changeHoldDateTime
     */
    public java.lang.Boolean getChangeHoldDateTime() {
        return changeHoldDateTime;
    }


    /**
     * Sets the changeHoldDateTime value for this CommitRequestData.
     * 
     * @param changeHoldDateTime
     */
    public void setChangeHoldDateTime(java.lang.Boolean changeHoldDateTime) {
        this.changeHoldDateTime = changeHoldDateTime;
    }


    /**
     * Gets the waiveNameChangeFee value for this CommitRequestData.
     * 
     * @return waiveNameChangeFee
     */
    public java.lang.Boolean getWaiveNameChangeFee() {
        return waiveNameChangeFee;
    }


    /**
     * Sets the waiveNameChangeFee value for this CommitRequestData.
     * 
     * @param waiveNameChangeFee
     */
    public void setWaiveNameChangeFee(java.lang.Boolean waiveNameChangeFee) {
        this.waiveNameChangeFee = waiveNameChangeFee;
    }


    /**
     * Gets the waivePenaltyFee value for this CommitRequestData.
     * 
     * @return waivePenaltyFee
     */
    public java.lang.Boolean getWaivePenaltyFee() {
        return waivePenaltyFee;
    }


    /**
     * Sets the waivePenaltyFee value for this CommitRequestData.
     * 
     * @param waivePenaltyFee
     */
    public void setWaivePenaltyFee(java.lang.Boolean waivePenaltyFee) {
        this.waivePenaltyFee = waivePenaltyFee;
    }


    /**
     * Gets the waiveSpoilageFee value for this CommitRequestData.
     * 
     * @return waiveSpoilageFee
     */
    public java.lang.Boolean getWaiveSpoilageFee() {
        return waiveSpoilageFee;
    }


    /**
     * Sets the waiveSpoilageFee value for this CommitRequestData.
     * 
     * @param waiveSpoilageFee
     */
    public void setWaiveSpoilageFee(java.lang.Boolean waiveSpoilageFee) {
        this.waiveSpoilageFee = waiveSpoilageFee;
    }


    /**
     * Gets the distributeToContacts value for this CommitRequestData.
     * 
     * @return distributeToContacts
     */
    public java.lang.Boolean getDistributeToContacts() {
        return distributeToContacts;
    }


    /**
     * Sets the distributeToContacts value for this CommitRequestData.
     * 
     * @param distributeToContacts
     */
    public void setDistributeToContacts(java.lang.Boolean distributeToContacts) {
        this.distributeToContacts = distributeToContacts;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CommitRequestData)) return false;
        CommitRequestData other = (CommitRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.booking==null && other.getBooking()==null) || 
             (this.booking!=null &&
              this.booking.equals(other.getBooking()))) &&
            ((this.restrictionOverride==null && other.getRestrictionOverride()==null) || 
             (this.restrictionOverride!=null &&
              this.restrictionOverride.equals(other.getRestrictionOverride()))) &&
            ((this.changeHoldDateTime==null && other.getChangeHoldDateTime()==null) || 
             (this.changeHoldDateTime!=null &&
              this.changeHoldDateTime.equals(other.getChangeHoldDateTime()))) &&
            ((this.waiveNameChangeFee==null && other.getWaiveNameChangeFee()==null) || 
             (this.waiveNameChangeFee!=null &&
              this.waiveNameChangeFee.equals(other.getWaiveNameChangeFee()))) &&
            ((this.waivePenaltyFee==null && other.getWaivePenaltyFee()==null) || 
             (this.waivePenaltyFee!=null &&
              this.waivePenaltyFee.equals(other.getWaivePenaltyFee()))) &&
            ((this.waiveSpoilageFee==null && other.getWaiveSpoilageFee()==null) || 
             (this.waiveSpoilageFee!=null &&
              this.waiveSpoilageFee.equals(other.getWaiveSpoilageFee()))) &&
            ((this.distributeToContacts==null && other.getDistributeToContacts()==null) || 
             (this.distributeToContacts!=null &&
              this.distributeToContacts.equals(other.getDistributeToContacts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBooking() != null) {
            _hashCode += getBooking().hashCode();
        }
        if (getRestrictionOverride() != null) {
            _hashCode += getRestrictionOverride().hashCode();
        }
        if (getChangeHoldDateTime() != null) {
            _hashCode += getChangeHoldDateTime().hashCode();
        }
        if (getWaiveNameChangeFee() != null) {
            _hashCode += getWaiveNameChangeFee().hashCode();
        }
        if (getWaivePenaltyFee() != null) {
            _hashCode += getWaivePenaltyFee().hashCode();
        }
        if (getWaiveSpoilageFee() != null) {
            _hashCode += getWaiveSpoilageFee().hashCode();
        }
        if (getDistributeToContacts() != null) {
            _hashCode += getDistributeToContacts().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CommitRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CommitRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("booking");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Booking"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Booking"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("restrictionOverride");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RestrictionOverride"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeHoldDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChangeHoldDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waiveNameChangeFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaiveNameChangeFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waivePenaltyFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaivePenaltyFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waiveSpoilageFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaiveSpoilageFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distributeToContacts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DistributeToContacts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
