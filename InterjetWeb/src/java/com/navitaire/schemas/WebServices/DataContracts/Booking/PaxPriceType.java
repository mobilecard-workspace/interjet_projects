/**
 * PaxPriceType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxPriceType  implements java.io.Serializable {
    private java.lang.String paxType;

    private java.lang.String paxDiscountCode;

    public PaxPriceType() {
    }

    public PaxPriceType(
           java.lang.String paxType,
           java.lang.String paxDiscountCode) {
           this.paxType = paxType;
           this.paxDiscountCode = paxDiscountCode;
    }


    /**
     * Gets the paxType value for this PaxPriceType.
     * 
     * @return paxType
     */
    public java.lang.String getPaxType() {
        return paxType;
    }


    /**
     * Sets the paxType value for this PaxPriceType.
     * 
     * @param paxType
     */
    public void setPaxType(java.lang.String paxType) {
        this.paxType = paxType;
    }


    /**
     * Gets the paxDiscountCode value for this PaxPriceType.
     * 
     * @return paxDiscountCode
     */
    public java.lang.String getPaxDiscountCode() {
        return paxDiscountCode;
    }


    /**
     * Sets the paxDiscountCode value for this PaxPriceType.
     * 
     * @param paxDiscountCode
     */
    public void setPaxDiscountCode(java.lang.String paxDiscountCode) {
        this.paxDiscountCode = paxDiscountCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxPriceType)) return false;
        PaxPriceType other = (PaxPriceType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paxType==null && other.getPaxType()==null) || 
             (this.paxType!=null &&
              this.paxType.equals(other.getPaxType()))) &&
            ((this.paxDiscountCode==null && other.getPaxDiscountCode()==null) || 
             (this.paxDiscountCode!=null &&
              this.paxDiscountCode.equals(other.getPaxDiscountCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaxType() != null) {
            _hashCode += getPaxType().hashCode();
        }
        if (getPaxDiscountCode() != null) {
            _hashCode += getPaxDiscountCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxPriceType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxDiscountCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxDiscountCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
