/**
 * BaggageEvent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BaggageEvent  implements java.io.Serializable {
    private java.lang.String actionStatusCode;

    private java.lang.Long baggageEventID;

    private java.lang.String baggageEventCode;

    private java.lang.String OSTag;

    private java.lang.Long baggageID;

    private java.lang.String eventText;

    private java.lang.String locationCode;

    private java.lang.String toLocationCode;

    private java.lang.String agentCode;

    private java.util.Calendar createdDate;

    public BaggageEvent() {
    }

    public BaggageEvent(
           java.lang.String actionStatusCode,
           java.lang.Long baggageEventID,
           java.lang.String baggageEventCode,
           java.lang.String OSTag,
           java.lang.Long baggageID,
           java.lang.String eventText,
           java.lang.String locationCode,
           java.lang.String toLocationCode,
           java.lang.String agentCode,
           java.util.Calendar createdDate) {
           this.actionStatusCode = actionStatusCode;
           this.baggageEventID = baggageEventID;
           this.baggageEventCode = baggageEventCode;
           this.OSTag = OSTag;
           this.baggageID = baggageID;
           this.eventText = eventText;
           this.locationCode = locationCode;
           this.toLocationCode = toLocationCode;
           this.agentCode = agentCode;
           this.createdDate = createdDate;
    }


    /**
     * Gets the actionStatusCode value for this BaggageEvent.
     * 
     * @return actionStatusCode
     */
    public java.lang.String getActionStatusCode() {
        return actionStatusCode;
    }


    /**
     * Sets the actionStatusCode value for this BaggageEvent.
     * 
     * @param actionStatusCode
     */
    public void setActionStatusCode(java.lang.String actionStatusCode) {
        this.actionStatusCode = actionStatusCode;
    }


    /**
     * Gets the baggageEventID value for this BaggageEvent.
     * 
     * @return baggageEventID
     */
    public java.lang.Long getBaggageEventID() {
        return baggageEventID;
    }


    /**
     * Sets the baggageEventID value for this BaggageEvent.
     * 
     * @param baggageEventID
     */
    public void setBaggageEventID(java.lang.Long baggageEventID) {
        this.baggageEventID = baggageEventID;
    }


    /**
     * Gets the baggageEventCode value for this BaggageEvent.
     * 
     * @return baggageEventCode
     */
    public java.lang.String getBaggageEventCode() {
        return baggageEventCode;
    }


    /**
     * Sets the baggageEventCode value for this BaggageEvent.
     * 
     * @param baggageEventCode
     */
    public void setBaggageEventCode(java.lang.String baggageEventCode) {
        this.baggageEventCode = baggageEventCode;
    }


    /**
     * Gets the OSTag value for this BaggageEvent.
     * 
     * @return OSTag
     */
    public java.lang.String getOSTag() {
        return OSTag;
    }


    /**
     * Sets the OSTag value for this BaggageEvent.
     * 
     * @param OSTag
     */
    public void setOSTag(java.lang.String OSTag) {
        this.OSTag = OSTag;
    }


    /**
     * Gets the baggageID value for this BaggageEvent.
     * 
     * @return baggageID
     */
    public java.lang.Long getBaggageID() {
        return baggageID;
    }


    /**
     * Sets the baggageID value for this BaggageEvent.
     * 
     * @param baggageID
     */
    public void setBaggageID(java.lang.Long baggageID) {
        this.baggageID = baggageID;
    }


    /**
     * Gets the eventText value for this BaggageEvent.
     * 
     * @return eventText
     */
    public java.lang.String getEventText() {
        return eventText;
    }


    /**
     * Sets the eventText value for this BaggageEvent.
     * 
     * @param eventText
     */
    public void setEventText(java.lang.String eventText) {
        this.eventText = eventText;
    }


    /**
     * Gets the locationCode value for this BaggageEvent.
     * 
     * @return locationCode
     */
    public java.lang.String getLocationCode() {
        return locationCode;
    }


    /**
     * Sets the locationCode value for this BaggageEvent.
     * 
     * @param locationCode
     */
    public void setLocationCode(java.lang.String locationCode) {
        this.locationCode = locationCode;
    }


    /**
     * Gets the toLocationCode value for this BaggageEvent.
     * 
     * @return toLocationCode
     */
    public java.lang.String getToLocationCode() {
        return toLocationCode;
    }


    /**
     * Sets the toLocationCode value for this BaggageEvent.
     * 
     * @param toLocationCode
     */
    public void setToLocationCode(java.lang.String toLocationCode) {
        this.toLocationCode = toLocationCode;
    }


    /**
     * Gets the agentCode value for this BaggageEvent.
     * 
     * @return agentCode
     */
    public java.lang.String getAgentCode() {
        return agentCode;
    }


    /**
     * Sets the agentCode value for this BaggageEvent.
     * 
     * @param agentCode
     */
    public void setAgentCode(java.lang.String agentCode) {
        this.agentCode = agentCode;
    }


    /**
     * Gets the createdDate value for this BaggageEvent.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this BaggageEvent.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BaggageEvent)) return false;
        BaggageEvent other = (BaggageEvent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actionStatusCode==null && other.getActionStatusCode()==null) || 
             (this.actionStatusCode!=null &&
              this.actionStatusCode.equals(other.getActionStatusCode()))) &&
            ((this.baggageEventID==null && other.getBaggageEventID()==null) || 
             (this.baggageEventID!=null &&
              this.baggageEventID.equals(other.getBaggageEventID()))) &&
            ((this.baggageEventCode==null && other.getBaggageEventCode()==null) || 
             (this.baggageEventCode!=null &&
              this.baggageEventCode.equals(other.getBaggageEventCode()))) &&
            ((this.OSTag==null && other.getOSTag()==null) || 
             (this.OSTag!=null &&
              this.OSTag.equals(other.getOSTag()))) &&
            ((this.baggageID==null && other.getBaggageID()==null) || 
             (this.baggageID!=null &&
              this.baggageID.equals(other.getBaggageID()))) &&
            ((this.eventText==null && other.getEventText()==null) || 
             (this.eventText!=null &&
              this.eventText.equals(other.getEventText()))) &&
            ((this.locationCode==null && other.getLocationCode()==null) || 
             (this.locationCode!=null &&
              this.locationCode.equals(other.getLocationCode()))) &&
            ((this.toLocationCode==null && other.getToLocationCode()==null) || 
             (this.toLocationCode!=null &&
              this.toLocationCode.equals(other.getToLocationCode()))) &&
            ((this.agentCode==null && other.getAgentCode()==null) || 
             (this.agentCode!=null &&
              this.agentCode.equals(other.getAgentCode()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActionStatusCode() != null) {
            _hashCode += getActionStatusCode().hashCode();
        }
        if (getBaggageEventID() != null) {
            _hashCode += getBaggageEventID().hashCode();
        }
        if (getBaggageEventCode() != null) {
            _hashCode += getBaggageEventCode().hashCode();
        }
        if (getOSTag() != null) {
            _hashCode += getOSTag().hashCode();
        }
        if (getBaggageID() != null) {
            _hashCode += getBaggageID().hashCode();
        }
        if (getEventText() != null) {
            _hashCode += getEventText().hashCode();
        }
        if (getLocationCode() != null) {
            _hashCode += getLocationCode().hashCode();
        }
        if (getToLocationCode() != null) {
            _hashCode += getToLocationCode().hashCode();
        }
        if (getAgentCode() != null) {
            _hashCode += getAgentCode().hashCode();
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BaggageEvent.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageEvent"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageEventID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageEventID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageEventCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageEventCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OSTag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OSTag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EventText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LocationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toLocationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ToLocationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AgentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
