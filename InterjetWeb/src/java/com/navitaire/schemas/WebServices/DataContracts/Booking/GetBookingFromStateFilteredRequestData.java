/**
 * GetBookingFromStateFilteredRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GetBookingFromStateFilteredRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFilter getBookingFilter;

    public GetBookingFromStateFilteredRequestData() {
    }

    public GetBookingFromStateFilteredRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFilter getBookingFilter) {
           this.getBookingFilter = getBookingFilter;
    }


    /**
     * Gets the getBookingFilter value for this GetBookingFromStateFilteredRequestData.
     * 
     * @return getBookingFilter
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFilter getGetBookingFilter() {
        return getBookingFilter;
    }


    /**
     * Sets the getBookingFilter value for this GetBookingFromStateFilteredRequestData.
     * 
     * @param getBookingFilter
     */
    public void setGetBookingFilter(com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFilter getBookingFilter) {
        this.getBookingFilter = getBookingFilter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBookingFromStateFilteredRequestData)) return false;
        GetBookingFromStateFilteredRequestData other = (GetBookingFromStateFilteredRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getBookingFilter==null && other.getGetBookingFilter()==null) || 
             (this.getBookingFilter!=null &&
              this.getBookingFilter.equals(other.getGetBookingFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetBookingFilter() != null) {
            _hashCode += getGetBookingFilter().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBookingFromStateFilteredRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFromStateFilteredRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getBookingFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
