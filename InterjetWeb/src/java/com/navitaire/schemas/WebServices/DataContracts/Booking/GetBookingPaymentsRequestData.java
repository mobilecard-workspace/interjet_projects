/**
 * GetBookingPaymentsRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GetBookingPaymentsRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator;

    private java.lang.Boolean getCurrentState;

    public GetBookingPaymentsRequestData() {
    }

    public GetBookingPaymentsRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator,
           java.lang.Boolean getCurrentState) {
           this.getByRecordLocator = getByRecordLocator;
           this.getCurrentState = getCurrentState;
    }


    /**
     * Gets the getByRecordLocator value for this GetBookingPaymentsRequestData.
     * 
     * @return getByRecordLocator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getGetByRecordLocator() {
        return getByRecordLocator;
    }


    /**
     * Sets the getByRecordLocator value for this GetBookingPaymentsRequestData.
     * 
     * @param getByRecordLocator
     */
    public void setGetByRecordLocator(com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator) {
        this.getByRecordLocator = getByRecordLocator;
    }


    /**
     * Gets the getCurrentState value for this GetBookingPaymentsRequestData.
     * 
     * @return getCurrentState
     */
    public java.lang.Boolean getGetCurrentState() {
        return getCurrentState;
    }


    /**
     * Sets the getCurrentState value for this GetBookingPaymentsRequestData.
     * 
     * @param getCurrentState
     */
    public void setGetCurrentState(java.lang.Boolean getCurrentState) {
        this.getCurrentState = getCurrentState;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBookingPaymentsRequestData)) return false;
        GetBookingPaymentsRequestData other = (GetBookingPaymentsRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getByRecordLocator==null && other.getGetByRecordLocator()==null) || 
             (this.getByRecordLocator!=null &&
              this.getByRecordLocator.equals(other.getGetByRecordLocator()))) &&
            ((this.getCurrentState==null && other.getGetCurrentState()==null) || 
             (this.getCurrentState!=null &&
              this.getCurrentState.equals(other.getGetCurrentState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetByRecordLocator() != null) {
            _hashCode += getGetByRecordLocator().hashCode();
        }
        if (getGetCurrentState() != null) {
            _hashCode += getGetCurrentState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBookingPaymentsRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingPaymentsRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getByRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByRecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getCurrentState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetCurrentState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
