/**
 * GetBookingRequestData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class GetBookingRequestData  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingBy getBookingBy;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetByThirdPartyRecordLocator getByThirdPartyRecordLocator;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.GetByID getByID;

    public GetBookingRequestData() {
    }

    public GetBookingRequestData(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingBy getBookingBy,
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetByThirdPartyRecordLocator getByThirdPartyRecordLocator,
           com.navitaire.schemas.WebServices.DataContracts.Booking.GetByID getByID) {
           this.getBookingBy = getBookingBy;
           this.getByRecordLocator = getByRecordLocator;
           this.getByThirdPartyRecordLocator = getByThirdPartyRecordLocator;
           this.getByID = getByID;
    }


    /**
     * Gets the getBookingBy value for this GetBookingRequestData.
     * 
     * @return getBookingBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingBy getGetBookingBy() {
        return getBookingBy;
    }


    /**
     * Sets the getBookingBy value for this GetBookingRequestData.
     * 
     * @param getBookingBy
     */
    public void setGetBookingBy(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingBy getBookingBy) {
        this.getBookingBy = getBookingBy;
    }


    /**
     * Gets the getByRecordLocator value for this GetBookingRequestData.
     * 
     * @return getByRecordLocator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getGetByRecordLocator() {
        return getByRecordLocator;
    }


    /**
     * Sets the getByRecordLocator value for this GetBookingRequestData.
     * 
     * @param getByRecordLocator
     */
    public void setGetByRecordLocator(com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator getByRecordLocator) {
        this.getByRecordLocator = getByRecordLocator;
    }


    /**
     * Gets the getByThirdPartyRecordLocator value for this GetBookingRequestData.
     * 
     * @return getByThirdPartyRecordLocator
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetByThirdPartyRecordLocator getGetByThirdPartyRecordLocator() {
        return getByThirdPartyRecordLocator;
    }


    /**
     * Sets the getByThirdPartyRecordLocator value for this GetBookingRequestData.
     * 
     * @param getByThirdPartyRecordLocator
     */
    public void setGetByThirdPartyRecordLocator(com.navitaire.schemas.WebServices.DataContracts.Booking.GetByThirdPartyRecordLocator getByThirdPartyRecordLocator) {
        this.getByThirdPartyRecordLocator = getByThirdPartyRecordLocator;
    }


    /**
     * Gets the getByID value for this GetBookingRequestData.
     * 
     * @return getByID
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetByID getGetByID() {
        return getByID;
    }


    /**
     * Sets the getByID value for this GetBookingRequestData.
     * 
     * @param getByID
     */
    public void setGetByID(com.navitaire.schemas.WebServices.DataContracts.Booking.GetByID getByID) {
        this.getByID = getByID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBookingRequestData)) return false;
        GetBookingRequestData other = (GetBookingRequestData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getBookingBy==null && other.getGetBookingBy()==null) || 
             (this.getBookingBy!=null &&
              this.getBookingBy.equals(other.getGetBookingBy()))) &&
            ((this.getByRecordLocator==null && other.getGetByRecordLocator()==null) || 
             (this.getByRecordLocator!=null &&
              this.getByRecordLocator.equals(other.getGetByRecordLocator()))) &&
            ((this.getByThirdPartyRecordLocator==null && other.getGetByThirdPartyRecordLocator()==null) || 
             (this.getByThirdPartyRecordLocator!=null &&
              this.getByThirdPartyRecordLocator.equals(other.getGetByThirdPartyRecordLocator()))) &&
            ((this.getByID==null && other.getGetByID()==null) || 
             (this.getByID!=null &&
              this.getByID.equals(other.getGetByID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetBookingBy() != null) {
            _hashCode += getGetBookingBy().hashCode();
        }
        if (getGetByRecordLocator() != null) {
            _hashCode += getGetByRecordLocator().hashCode();
        }
        if (getGetByThirdPartyRecordLocator() != null) {
            _hashCode += getGetByThirdPartyRecordLocator().hashCode();
        }
        if (getGetByID() != null) {
            _hashCode += getGetByID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBookingRequestData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingRequestData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getBookingBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "GetBookingBy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getByRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByRecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getByThirdPartyRecordLocator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByThirdPartyRecordLocator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByThirdPartyRecordLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getByID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByID"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
