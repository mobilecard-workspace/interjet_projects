/**
 * AddPaymentToBookingResponseData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class AddPaymentToBookingResponseData  extends com.navitaire.schemas.WebServices.DataContracts.Booking.ServiceMessage  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Booking.ValidationPayment validationPayment;

    public AddPaymentToBookingResponseData() {
    }

    public AddPaymentToBookingResponseData(
           com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[] otherServiceInfoList,
           com.navitaire.schemas.WebServices.DataContracts.Booking.ValidationPayment validationPayment) {
        super(
            otherServiceInfoList);
        this.validationPayment = validationPayment;
    }


    /**
     * Gets the validationPayment value for this AddPaymentToBookingResponseData.
     * 
     * @return validationPayment
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.ValidationPayment getValidationPayment() {
        return validationPayment;
    }


    /**
     * Sets the validationPayment value for this AddPaymentToBookingResponseData.
     * 
     * @param validationPayment
     */
    public void setValidationPayment(com.navitaire.schemas.WebServices.DataContracts.Booking.ValidationPayment validationPayment) {
        this.validationPayment = validationPayment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddPaymentToBookingResponseData)) return false;
        AddPaymentToBookingResponseData other = (AddPaymentToBookingResponseData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.validationPayment==null && other.getValidationPayment()==null) || 
             (this.validationPayment!=null &&
              this.validationPayment.equals(other.getValidationPayment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getValidationPayment() != null) {
            _hashCode += getValidationPayment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddPaymentToBookingResponseData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AddPaymentToBookingResponseData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationPayment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationPayment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationPayment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
