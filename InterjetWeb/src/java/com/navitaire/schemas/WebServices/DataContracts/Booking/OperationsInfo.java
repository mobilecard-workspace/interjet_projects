/**
 * OperationsInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class OperationsInfo  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String actualArrivalGate;

    private java.lang.String actualDepartureGate;

    private java.util.Calendar actualOffBlockTime;

    private java.util.Calendar actualOnBlockTime;

    private java.util.Calendar actualTouchDownTime;

    private java.util.Calendar airborneTime;

    private java.lang.String arrivalGate;

    private java.lang.String arrivalNote;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ArrivalStatus arrivalStatus;

    private java.lang.String baggageClaim;

    private java.lang.String departureGate;

    private java.lang.String departureNote;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DepartureStatus departureStatus;

    private java.util.Calendar ETA;

    private java.util.Calendar ETD;

    private java.util.Calendar STA;

    private java.util.Calendar STD;

    private java.lang.String tailNumber;

    public OperationsInfo() {
    }

    public OperationsInfo(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String actualArrivalGate,
           java.lang.String actualDepartureGate,
           java.util.Calendar actualOffBlockTime,
           java.util.Calendar actualOnBlockTime,
           java.util.Calendar actualTouchDownTime,
           java.util.Calendar airborneTime,
           java.lang.String arrivalGate,
           java.lang.String arrivalNote,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ArrivalStatus arrivalStatus,
           java.lang.String baggageClaim,
           java.lang.String departureGate,
           java.lang.String departureNote,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DepartureStatus departureStatus,
           java.util.Calendar ETA,
           java.util.Calendar ETD,
           java.util.Calendar STA,
           java.util.Calendar STD,
           java.lang.String tailNumber) {
        super(
            state);
        this.actualArrivalGate = actualArrivalGate;
        this.actualDepartureGate = actualDepartureGate;
        this.actualOffBlockTime = actualOffBlockTime;
        this.actualOnBlockTime = actualOnBlockTime;
        this.actualTouchDownTime = actualTouchDownTime;
        this.airborneTime = airborneTime;
        this.arrivalGate = arrivalGate;
        this.arrivalNote = arrivalNote;
        this.arrivalStatus = arrivalStatus;
        this.baggageClaim = baggageClaim;
        this.departureGate = departureGate;
        this.departureNote = departureNote;
        this.departureStatus = departureStatus;
        this.ETA = ETA;
        this.ETD = ETD;
        this.STA = STA;
        this.STD = STD;
        this.tailNumber = tailNumber;
    }


    /**
     * Gets the actualArrivalGate value for this OperationsInfo.
     * 
     * @return actualArrivalGate
     */
    public java.lang.String getActualArrivalGate() {
        return actualArrivalGate;
    }


    /**
     * Sets the actualArrivalGate value for this OperationsInfo.
     * 
     * @param actualArrivalGate
     */
    public void setActualArrivalGate(java.lang.String actualArrivalGate) {
        this.actualArrivalGate = actualArrivalGate;
    }


    /**
     * Gets the actualDepartureGate value for this OperationsInfo.
     * 
     * @return actualDepartureGate
     */
    public java.lang.String getActualDepartureGate() {
        return actualDepartureGate;
    }


    /**
     * Sets the actualDepartureGate value for this OperationsInfo.
     * 
     * @param actualDepartureGate
     */
    public void setActualDepartureGate(java.lang.String actualDepartureGate) {
        this.actualDepartureGate = actualDepartureGate;
    }


    /**
     * Gets the actualOffBlockTime value for this OperationsInfo.
     * 
     * @return actualOffBlockTime
     */
    public java.util.Calendar getActualOffBlockTime() {
        return actualOffBlockTime;
    }


    /**
     * Sets the actualOffBlockTime value for this OperationsInfo.
     * 
     * @param actualOffBlockTime
     */
    public void setActualOffBlockTime(java.util.Calendar actualOffBlockTime) {
        this.actualOffBlockTime = actualOffBlockTime;
    }


    /**
     * Gets the actualOnBlockTime value for this OperationsInfo.
     * 
     * @return actualOnBlockTime
     */
    public java.util.Calendar getActualOnBlockTime() {
        return actualOnBlockTime;
    }


    /**
     * Sets the actualOnBlockTime value for this OperationsInfo.
     * 
     * @param actualOnBlockTime
     */
    public void setActualOnBlockTime(java.util.Calendar actualOnBlockTime) {
        this.actualOnBlockTime = actualOnBlockTime;
    }


    /**
     * Gets the actualTouchDownTime value for this OperationsInfo.
     * 
     * @return actualTouchDownTime
     */
    public java.util.Calendar getActualTouchDownTime() {
        return actualTouchDownTime;
    }


    /**
     * Sets the actualTouchDownTime value for this OperationsInfo.
     * 
     * @param actualTouchDownTime
     */
    public void setActualTouchDownTime(java.util.Calendar actualTouchDownTime) {
        this.actualTouchDownTime = actualTouchDownTime;
    }


    /**
     * Gets the airborneTime value for this OperationsInfo.
     * 
     * @return airborneTime
     */
    public java.util.Calendar getAirborneTime() {
        return airborneTime;
    }


    /**
     * Sets the airborneTime value for this OperationsInfo.
     * 
     * @param airborneTime
     */
    public void setAirborneTime(java.util.Calendar airborneTime) {
        this.airborneTime = airborneTime;
    }


    /**
     * Gets the arrivalGate value for this OperationsInfo.
     * 
     * @return arrivalGate
     */
    public java.lang.String getArrivalGate() {
        return arrivalGate;
    }


    /**
     * Sets the arrivalGate value for this OperationsInfo.
     * 
     * @param arrivalGate
     */
    public void setArrivalGate(java.lang.String arrivalGate) {
        this.arrivalGate = arrivalGate;
    }


    /**
     * Gets the arrivalNote value for this OperationsInfo.
     * 
     * @return arrivalNote
     */
    public java.lang.String getArrivalNote() {
        return arrivalNote;
    }


    /**
     * Sets the arrivalNote value for this OperationsInfo.
     * 
     * @param arrivalNote
     */
    public void setArrivalNote(java.lang.String arrivalNote) {
        this.arrivalNote = arrivalNote;
    }


    /**
     * Gets the arrivalStatus value for this OperationsInfo.
     * 
     * @return arrivalStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ArrivalStatus getArrivalStatus() {
        return arrivalStatus;
    }


    /**
     * Sets the arrivalStatus value for this OperationsInfo.
     * 
     * @param arrivalStatus
     */
    public void setArrivalStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ArrivalStatus arrivalStatus) {
        this.arrivalStatus = arrivalStatus;
    }


    /**
     * Gets the baggageClaim value for this OperationsInfo.
     * 
     * @return baggageClaim
     */
    public java.lang.String getBaggageClaim() {
        return baggageClaim;
    }


    /**
     * Sets the baggageClaim value for this OperationsInfo.
     * 
     * @param baggageClaim
     */
    public void setBaggageClaim(java.lang.String baggageClaim) {
        this.baggageClaim = baggageClaim;
    }


    /**
     * Gets the departureGate value for this OperationsInfo.
     * 
     * @return departureGate
     */
    public java.lang.String getDepartureGate() {
        return departureGate;
    }


    /**
     * Sets the departureGate value for this OperationsInfo.
     * 
     * @param departureGate
     */
    public void setDepartureGate(java.lang.String departureGate) {
        this.departureGate = departureGate;
    }


    /**
     * Gets the departureNote value for this OperationsInfo.
     * 
     * @return departureNote
     */
    public java.lang.String getDepartureNote() {
        return departureNote;
    }


    /**
     * Sets the departureNote value for this OperationsInfo.
     * 
     * @param departureNote
     */
    public void setDepartureNote(java.lang.String departureNote) {
        this.departureNote = departureNote;
    }


    /**
     * Gets the departureStatus value for this OperationsInfo.
     * 
     * @return departureStatus
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DepartureStatus getDepartureStatus() {
        return departureStatus;
    }


    /**
     * Sets the departureStatus value for this OperationsInfo.
     * 
     * @param departureStatus
     */
    public void setDepartureStatus(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DepartureStatus departureStatus) {
        this.departureStatus = departureStatus;
    }


    /**
     * Gets the ETA value for this OperationsInfo.
     * 
     * @return ETA
     */
    public java.util.Calendar getETA() {
        return ETA;
    }


    /**
     * Sets the ETA value for this OperationsInfo.
     * 
     * @param ETA
     */
    public void setETA(java.util.Calendar ETA) {
        this.ETA = ETA;
    }


    /**
     * Gets the ETD value for this OperationsInfo.
     * 
     * @return ETD
     */
    public java.util.Calendar getETD() {
        return ETD;
    }


    /**
     * Sets the ETD value for this OperationsInfo.
     * 
     * @param ETD
     */
    public void setETD(java.util.Calendar ETD) {
        this.ETD = ETD;
    }


    /**
     * Gets the STA value for this OperationsInfo.
     * 
     * @return STA
     */
    public java.util.Calendar getSTA() {
        return STA;
    }


    /**
     * Sets the STA value for this OperationsInfo.
     * 
     * @param STA
     */
    public void setSTA(java.util.Calendar STA) {
        this.STA = STA;
    }


    /**
     * Gets the STD value for this OperationsInfo.
     * 
     * @return STD
     */
    public java.util.Calendar getSTD() {
        return STD;
    }


    /**
     * Sets the STD value for this OperationsInfo.
     * 
     * @param STD
     */
    public void setSTD(java.util.Calendar STD) {
        this.STD = STD;
    }


    /**
     * Gets the tailNumber value for this OperationsInfo.
     * 
     * @return tailNumber
     */
    public java.lang.String getTailNumber() {
        return tailNumber;
    }


    /**
     * Sets the tailNumber value for this OperationsInfo.
     * 
     * @param tailNumber
     */
    public void setTailNumber(java.lang.String tailNumber) {
        this.tailNumber = tailNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OperationsInfo)) return false;
        OperationsInfo other = (OperationsInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.actualArrivalGate==null && other.getActualArrivalGate()==null) || 
             (this.actualArrivalGate!=null &&
              this.actualArrivalGate.equals(other.getActualArrivalGate()))) &&
            ((this.actualDepartureGate==null && other.getActualDepartureGate()==null) || 
             (this.actualDepartureGate!=null &&
              this.actualDepartureGate.equals(other.getActualDepartureGate()))) &&
            ((this.actualOffBlockTime==null && other.getActualOffBlockTime()==null) || 
             (this.actualOffBlockTime!=null &&
              this.actualOffBlockTime.equals(other.getActualOffBlockTime()))) &&
            ((this.actualOnBlockTime==null && other.getActualOnBlockTime()==null) || 
             (this.actualOnBlockTime!=null &&
              this.actualOnBlockTime.equals(other.getActualOnBlockTime()))) &&
            ((this.actualTouchDownTime==null && other.getActualTouchDownTime()==null) || 
             (this.actualTouchDownTime!=null &&
              this.actualTouchDownTime.equals(other.getActualTouchDownTime()))) &&
            ((this.airborneTime==null && other.getAirborneTime()==null) || 
             (this.airborneTime!=null &&
              this.airborneTime.equals(other.getAirborneTime()))) &&
            ((this.arrivalGate==null && other.getArrivalGate()==null) || 
             (this.arrivalGate!=null &&
              this.arrivalGate.equals(other.getArrivalGate()))) &&
            ((this.arrivalNote==null && other.getArrivalNote()==null) || 
             (this.arrivalNote!=null &&
              this.arrivalNote.equals(other.getArrivalNote()))) &&
            ((this.arrivalStatus==null && other.getArrivalStatus()==null) || 
             (this.arrivalStatus!=null &&
              this.arrivalStatus.equals(other.getArrivalStatus()))) &&
            ((this.baggageClaim==null && other.getBaggageClaim()==null) || 
             (this.baggageClaim!=null &&
              this.baggageClaim.equals(other.getBaggageClaim()))) &&
            ((this.departureGate==null && other.getDepartureGate()==null) || 
             (this.departureGate!=null &&
              this.departureGate.equals(other.getDepartureGate()))) &&
            ((this.departureNote==null && other.getDepartureNote()==null) || 
             (this.departureNote!=null &&
              this.departureNote.equals(other.getDepartureNote()))) &&
            ((this.departureStatus==null && other.getDepartureStatus()==null) || 
             (this.departureStatus!=null &&
              this.departureStatus.equals(other.getDepartureStatus()))) &&
            ((this.ETA==null && other.getETA()==null) || 
             (this.ETA!=null &&
              this.ETA.equals(other.getETA()))) &&
            ((this.ETD==null && other.getETD()==null) || 
             (this.ETD!=null &&
              this.ETD.equals(other.getETD()))) &&
            ((this.STA==null && other.getSTA()==null) || 
             (this.STA!=null &&
              this.STA.equals(other.getSTA()))) &&
            ((this.STD==null && other.getSTD()==null) || 
             (this.STD!=null &&
              this.STD.equals(other.getSTD()))) &&
            ((this.tailNumber==null && other.getTailNumber()==null) || 
             (this.tailNumber!=null &&
              this.tailNumber.equals(other.getTailNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getActualArrivalGate() != null) {
            _hashCode += getActualArrivalGate().hashCode();
        }
        if (getActualDepartureGate() != null) {
            _hashCode += getActualDepartureGate().hashCode();
        }
        if (getActualOffBlockTime() != null) {
            _hashCode += getActualOffBlockTime().hashCode();
        }
        if (getActualOnBlockTime() != null) {
            _hashCode += getActualOnBlockTime().hashCode();
        }
        if (getActualTouchDownTime() != null) {
            _hashCode += getActualTouchDownTime().hashCode();
        }
        if (getAirborneTime() != null) {
            _hashCode += getAirborneTime().hashCode();
        }
        if (getArrivalGate() != null) {
            _hashCode += getArrivalGate().hashCode();
        }
        if (getArrivalNote() != null) {
            _hashCode += getArrivalNote().hashCode();
        }
        if (getArrivalStatus() != null) {
            _hashCode += getArrivalStatus().hashCode();
        }
        if (getBaggageClaim() != null) {
            _hashCode += getBaggageClaim().hashCode();
        }
        if (getDepartureGate() != null) {
            _hashCode += getDepartureGate().hashCode();
        }
        if (getDepartureNote() != null) {
            _hashCode += getDepartureNote().hashCode();
        }
        if (getDepartureStatus() != null) {
            _hashCode += getDepartureStatus().hashCode();
        }
        if (getETA() != null) {
            _hashCode += getETA().hashCode();
        }
        if (getETD() != null) {
            _hashCode += getETD().hashCode();
        }
        if (getSTA() != null) {
            _hashCode += getSTA().hashCode();
        }
        if (getSTD() != null) {
            _hashCode += getSTD().hashCode();
        }
        if (getTailNumber() != null) {
            _hashCode += getTailNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OperationsInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OperationsInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualArrivalGate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActualArrivalGate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualDepartureGate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActualDepartureGate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualOffBlockTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActualOffBlockTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualOnBlockTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActualOnBlockTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualTouchDownTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActualTouchDownTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("airborneTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AirborneTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalGate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalGate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalNote");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalNote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ArrivalStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baggageClaim");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageClaim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureGate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureGate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureNote");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureNote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DepartureStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ETA");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ETA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ETD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ETD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STA");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "STD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tailNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TailNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
