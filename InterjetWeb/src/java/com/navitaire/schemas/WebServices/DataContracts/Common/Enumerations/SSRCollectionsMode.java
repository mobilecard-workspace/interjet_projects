/**
 * SSRCollectionsMode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class SSRCollectionsMode implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SSRCollectionsMode(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _None = "None";
    public static final java.lang.String _Leg = "Leg";
    public static final java.lang.String _Segment = "Segment";
    public static final java.lang.String _All = "All";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final SSRCollectionsMode None = new SSRCollectionsMode(_None);
    public static final SSRCollectionsMode Leg = new SSRCollectionsMode(_Leg);
    public static final SSRCollectionsMode Segment = new SSRCollectionsMode(_Segment);
    public static final SSRCollectionsMode All = new SSRCollectionsMode(_All);
    public static final SSRCollectionsMode Unmapped = new SSRCollectionsMode(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static SSRCollectionsMode fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SSRCollectionsMode enumeration = (SSRCollectionsMode)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SSRCollectionsMode fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SSRCollectionsMode.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SSRCollectionsMode"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
