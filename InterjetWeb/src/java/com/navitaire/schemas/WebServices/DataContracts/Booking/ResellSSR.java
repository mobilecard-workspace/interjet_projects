/**
 * ResellSSR.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class ResellSSR  implements java.io.Serializable {
    private java.lang.Integer journeyNumber;

    private java.lang.Boolean resellSSRs;

    private java.lang.Boolean resellSeatSSRs;

    private java.lang.Boolean waiveSeatFee;

    public ResellSSR() {
    }

    public ResellSSR(
           java.lang.Integer journeyNumber,
           java.lang.Boolean resellSSRs,
           java.lang.Boolean resellSeatSSRs,
           java.lang.Boolean waiveSeatFee) {
           this.journeyNumber = journeyNumber;
           this.resellSSRs = resellSSRs;
           this.resellSeatSSRs = resellSeatSSRs;
           this.waiveSeatFee = waiveSeatFee;
    }


    /**
     * Gets the journeyNumber value for this ResellSSR.
     * 
     * @return journeyNumber
     */
    public java.lang.Integer getJourneyNumber() {
        return journeyNumber;
    }


    /**
     * Sets the journeyNumber value for this ResellSSR.
     * 
     * @param journeyNumber
     */
    public void setJourneyNumber(java.lang.Integer journeyNumber) {
        this.journeyNumber = journeyNumber;
    }


    /**
     * Gets the resellSSRs value for this ResellSSR.
     * 
     * @return resellSSRs
     */
    public java.lang.Boolean getResellSSRs() {
        return resellSSRs;
    }


    /**
     * Sets the resellSSRs value for this ResellSSR.
     * 
     * @param resellSSRs
     */
    public void setResellSSRs(java.lang.Boolean resellSSRs) {
        this.resellSSRs = resellSSRs;
    }


    /**
     * Gets the resellSeatSSRs value for this ResellSSR.
     * 
     * @return resellSeatSSRs
     */
    public java.lang.Boolean getResellSeatSSRs() {
        return resellSeatSSRs;
    }


    /**
     * Sets the resellSeatSSRs value for this ResellSSR.
     * 
     * @param resellSeatSSRs
     */
    public void setResellSeatSSRs(java.lang.Boolean resellSeatSSRs) {
        this.resellSeatSSRs = resellSeatSSRs;
    }


    /**
     * Gets the waiveSeatFee value for this ResellSSR.
     * 
     * @return waiveSeatFee
     */
    public java.lang.Boolean getWaiveSeatFee() {
        return waiveSeatFee;
    }


    /**
     * Sets the waiveSeatFee value for this ResellSSR.
     * 
     * @param waiveSeatFee
     */
    public void setWaiveSeatFee(java.lang.Boolean waiveSeatFee) {
        this.waiveSeatFee = waiveSeatFee;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResellSSR)) return false;
        ResellSSR other = (ResellSSR) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.journeyNumber==null && other.getJourneyNumber()==null) || 
             (this.journeyNumber!=null &&
              this.journeyNumber.equals(other.getJourneyNumber()))) &&
            ((this.resellSSRs==null && other.getResellSSRs()==null) || 
             (this.resellSSRs!=null &&
              this.resellSSRs.equals(other.getResellSSRs()))) &&
            ((this.resellSeatSSRs==null && other.getResellSeatSSRs()==null) || 
             (this.resellSeatSSRs!=null &&
              this.resellSeatSSRs.equals(other.getResellSeatSSRs()))) &&
            ((this.waiveSeatFee==null && other.getWaiveSeatFee()==null) || 
             (this.waiveSeatFee!=null &&
              this.waiveSeatFee.equals(other.getWaiveSeatFee())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getJourneyNumber() != null) {
            _hashCode += getJourneyNumber().hashCode();
        }
        if (getResellSSRs() != null) {
            _hashCode += getResellSSRs().hashCode();
        }
        if (getResellSeatSSRs() != null) {
            _hashCode += getResellSeatSSRs().hashCode();
        }
        if (getWaiveSeatFee() != null) {
            _hashCode += getWaiveSeatFee().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResellSSR.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ResellSSR"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("journeyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resellSSRs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ResellSSRs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resellSeatSSRs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ResellSeatSSRs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waiveSeatFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "WaiveSeatFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
