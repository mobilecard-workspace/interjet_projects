/**
 * BookingComponentCharge.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class BookingComponentCharge  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.Long bookingComponentID;

    private java.lang.Short chargeNumber;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChargeType chargeType;

    private java.lang.String chargeCode;

    private java.lang.String ticketCode;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CollectType collectType;

    private java.lang.String currencyCode;

    private java.math.BigDecimal chargeAmount;

    private java.lang.String chargeDetail;

    private java.lang.String foreignCurrencyCode;

    private java.math.BigDecimal foreignAmount;

    private java.util.Calendar createdDateTime;

    private java.lang.Long createdAgentID;

    public BookingComponentCharge() {
    }

    public BookingComponentCharge(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.Long bookingComponentID,
           java.lang.Short chargeNumber,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChargeType chargeType,
           java.lang.String chargeCode,
           java.lang.String ticketCode,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CollectType collectType,
           java.lang.String currencyCode,
           java.math.BigDecimal chargeAmount,
           java.lang.String chargeDetail,
           java.lang.String foreignCurrencyCode,
           java.math.BigDecimal foreignAmount,
           java.util.Calendar createdDateTime,
           java.lang.Long createdAgentID) {
        super(
            state);
        this.bookingComponentID = bookingComponentID;
        this.chargeNumber = chargeNumber;
        this.chargeType = chargeType;
        this.chargeCode = chargeCode;
        this.ticketCode = ticketCode;
        this.collectType = collectType;
        this.currencyCode = currencyCode;
        this.chargeAmount = chargeAmount;
        this.chargeDetail = chargeDetail;
        this.foreignCurrencyCode = foreignCurrencyCode;
        this.foreignAmount = foreignAmount;
        this.createdDateTime = createdDateTime;
        this.createdAgentID = createdAgentID;
    }


    /**
     * Gets the bookingComponentID value for this BookingComponentCharge.
     * 
     * @return bookingComponentID
     */
    public java.lang.Long getBookingComponentID() {
        return bookingComponentID;
    }


    /**
     * Sets the bookingComponentID value for this BookingComponentCharge.
     * 
     * @param bookingComponentID
     */
    public void setBookingComponentID(java.lang.Long bookingComponentID) {
        this.bookingComponentID = bookingComponentID;
    }


    /**
     * Gets the chargeNumber value for this BookingComponentCharge.
     * 
     * @return chargeNumber
     */
    public java.lang.Short getChargeNumber() {
        return chargeNumber;
    }


    /**
     * Sets the chargeNumber value for this BookingComponentCharge.
     * 
     * @param chargeNumber
     */
    public void setChargeNumber(java.lang.Short chargeNumber) {
        this.chargeNumber = chargeNumber;
    }


    /**
     * Gets the chargeType value for this BookingComponentCharge.
     * 
     * @return chargeType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChargeType getChargeType() {
        return chargeType;
    }


    /**
     * Sets the chargeType value for this BookingComponentCharge.
     * 
     * @param chargeType
     */
    public void setChargeType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChargeType chargeType) {
        this.chargeType = chargeType;
    }


    /**
     * Gets the chargeCode value for this BookingComponentCharge.
     * 
     * @return chargeCode
     */
    public java.lang.String getChargeCode() {
        return chargeCode;
    }


    /**
     * Sets the chargeCode value for this BookingComponentCharge.
     * 
     * @param chargeCode
     */
    public void setChargeCode(java.lang.String chargeCode) {
        this.chargeCode = chargeCode;
    }


    /**
     * Gets the ticketCode value for this BookingComponentCharge.
     * 
     * @return ticketCode
     */
    public java.lang.String getTicketCode() {
        return ticketCode;
    }


    /**
     * Sets the ticketCode value for this BookingComponentCharge.
     * 
     * @param ticketCode
     */
    public void setTicketCode(java.lang.String ticketCode) {
        this.ticketCode = ticketCode;
    }


    /**
     * Gets the collectType value for this BookingComponentCharge.
     * 
     * @return collectType
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CollectType getCollectType() {
        return collectType;
    }


    /**
     * Sets the collectType value for this BookingComponentCharge.
     * 
     * @param collectType
     */
    public void setCollectType(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CollectType collectType) {
        this.collectType = collectType;
    }


    /**
     * Gets the currencyCode value for this BookingComponentCharge.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this BookingComponentCharge.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the chargeAmount value for this BookingComponentCharge.
     * 
     * @return chargeAmount
     */
    public java.math.BigDecimal getChargeAmount() {
        return chargeAmount;
    }


    /**
     * Sets the chargeAmount value for this BookingComponentCharge.
     * 
     * @param chargeAmount
     */
    public void setChargeAmount(java.math.BigDecimal chargeAmount) {
        this.chargeAmount = chargeAmount;
    }


    /**
     * Gets the chargeDetail value for this BookingComponentCharge.
     * 
     * @return chargeDetail
     */
    public java.lang.String getChargeDetail() {
        return chargeDetail;
    }


    /**
     * Sets the chargeDetail value for this BookingComponentCharge.
     * 
     * @param chargeDetail
     */
    public void setChargeDetail(java.lang.String chargeDetail) {
        this.chargeDetail = chargeDetail;
    }


    /**
     * Gets the foreignCurrencyCode value for this BookingComponentCharge.
     * 
     * @return foreignCurrencyCode
     */
    public java.lang.String getForeignCurrencyCode() {
        return foreignCurrencyCode;
    }


    /**
     * Sets the foreignCurrencyCode value for this BookingComponentCharge.
     * 
     * @param foreignCurrencyCode
     */
    public void setForeignCurrencyCode(java.lang.String foreignCurrencyCode) {
        this.foreignCurrencyCode = foreignCurrencyCode;
    }


    /**
     * Gets the foreignAmount value for this BookingComponentCharge.
     * 
     * @return foreignAmount
     */
    public java.math.BigDecimal getForeignAmount() {
        return foreignAmount;
    }


    /**
     * Sets the foreignAmount value for this BookingComponentCharge.
     * 
     * @param foreignAmount
     */
    public void setForeignAmount(java.math.BigDecimal foreignAmount) {
        this.foreignAmount = foreignAmount;
    }


    /**
     * Gets the createdDateTime value for this BookingComponentCharge.
     * 
     * @return createdDateTime
     */
    public java.util.Calendar getCreatedDateTime() {
        return createdDateTime;
    }


    /**
     * Sets the createdDateTime value for this BookingComponentCharge.
     * 
     * @param createdDateTime
     */
    public void setCreatedDateTime(java.util.Calendar createdDateTime) {
        this.createdDateTime = createdDateTime;
    }


    /**
     * Gets the createdAgentID value for this BookingComponentCharge.
     * 
     * @return createdAgentID
     */
    public java.lang.Long getCreatedAgentID() {
        return createdAgentID;
    }


    /**
     * Sets the createdAgentID value for this BookingComponentCharge.
     * 
     * @param createdAgentID
     */
    public void setCreatedAgentID(java.lang.Long createdAgentID) {
        this.createdAgentID = createdAgentID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BookingComponentCharge)) return false;
        BookingComponentCharge other = (BookingComponentCharge) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.bookingComponentID==null && other.getBookingComponentID()==null) || 
             (this.bookingComponentID!=null &&
              this.bookingComponentID.equals(other.getBookingComponentID()))) &&
            ((this.chargeNumber==null && other.getChargeNumber()==null) || 
             (this.chargeNumber!=null &&
              this.chargeNumber.equals(other.getChargeNumber()))) &&
            ((this.chargeType==null && other.getChargeType()==null) || 
             (this.chargeType!=null &&
              this.chargeType.equals(other.getChargeType()))) &&
            ((this.chargeCode==null && other.getChargeCode()==null) || 
             (this.chargeCode!=null &&
              this.chargeCode.equals(other.getChargeCode()))) &&
            ((this.ticketCode==null && other.getTicketCode()==null) || 
             (this.ticketCode!=null &&
              this.ticketCode.equals(other.getTicketCode()))) &&
            ((this.collectType==null && other.getCollectType()==null) || 
             (this.collectType!=null &&
              this.collectType.equals(other.getCollectType()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.chargeAmount==null && other.getChargeAmount()==null) || 
             (this.chargeAmount!=null &&
              this.chargeAmount.equals(other.getChargeAmount()))) &&
            ((this.chargeDetail==null && other.getChargeDetail()==null) || 
             (this.chargeDetail!=null &&
              this.chargeDetail.equals(other.getChargeDetail()))) &&
            ((this.foreignCurrencyCode==null && other.getForeignCurrencyCode()==null) || 
             (this.foreignCurrencyCode!=null &&
              this.foreignCurrencyCode.equals(other.getForeignCurrencyCode()))) &&
            ((this.foreignAmount==null && other.getForeignAmount()==null) || 
             (this.foreignAmount!=null &&
              this.foreignAmount.equals(other.getForeignAmount()))) &&
            ((this.createdDateTime==null && other.getCreatedDateTime()==null) || 
             (this.createdDateTime!=null &&
              this.createdDateTime.equals(other.getCreatedDateTime()))) &&
            ((this.createdAgentID==null && other.getCreatedAgentID()==null) || 
             (this.createdAgentID!=null &&
              this.createdAgentID.equals(other.getCreatedAgentID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBookingComponentID() != null) {
            _hashCode += getBookingComponentID().hashCode();
        }
        if (getChargeNumber() != null) {
            _hashCode += getChargeNumber().hashCode();
        }
        if (getChargeType() != null) {
            _hashCode += getChargeType().hashCode();
        }
        if (getChargeCode() != null) {
            _hashCode += getChargeCode().hashCode();
        }
        if (getTicketCode() != null) {
            _hashCode += getTicketCode().hashCode();
        }
        if (getCollectType() != null) {
            _hashCode += getCollectType().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getChargeAmount() != null) {
            _hashCode += getChargeAmount().hashCode();
        }
        if (getChargeDetail() != null) {
            _hashCode += getChargeDetail().hashCode();
        }
        if (getForeignCurrencyCode() != null) {
            _hashCode += getForeignCurrencyCode().hashCode();
        }
        if (getForeignAmount() != null) {
            _hashCode += getForeignAmount().hashCode();
        }
        if (getCreatedDateTime() != null) {
            _hashCode += getCreatedDateTime().hashCode();
        }
        if (getCreatedAgentID() != null) {
            _hashCode += getCreatedAgentID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BookingComponentCharge.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentCharge"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bookingComponentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChargeNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChargeType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChargeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TicketCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CollectType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "CollectType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChargeAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chargeDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ChargeDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ForeignCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ForeignAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdAgentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreatedAgentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
