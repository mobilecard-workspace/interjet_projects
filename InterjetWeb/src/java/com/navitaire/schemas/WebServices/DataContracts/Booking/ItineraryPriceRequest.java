/**
 * ItineraryPriceRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class ItineraryPriceRequest  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceItineraryBy priceItineraryBy;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SSRRequest SSRRequest;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequestData sellByKeyRequest;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourneyRequestData priceJourneyWithLegsRequest;

    public ItineraryPriceRequest() {
    }

    public ItineraryPriceRequest(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceItineraryBy priceItineraryBy,
           com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SSRRequest SSRRequest,
           com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequestData sellByKeyRequest,
           com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourneyRequestData priceJourneyWithLegsRequest) {
           this.priceItineraryBy = priceItineraryBy;
           this.typeOfSale = typeOfSale;
           this.SSRRequest = SSRRequest;
           this.sellByKeyRequest = sellByKeyRequest;
           this.priceJourneyWithLegsRequest = priceJourneyWithLegsRequest;
    }


    /**
     * Gets the priceItineraryBy value for this ItineraryPriceRequest.
     * 
     * @return priceItineraryBy
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceItineraryBy getPriceItineraryBy() {
        return priceItineraryBy;
    }


    /**
     * Sets the priceItineraryBy value for this ItineraryPriceRequest.
     * 
     * @param priceItineraryBy
     */
    public void setPriceItineraryBy(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceItineraryBy priceItineraryBy) {
        this.priceItineraryBy = priceItineraryBy;
    }


    /**
     * Gets the typeOfSale value for this ItineraryPriceRequest.
     * 
     * @return typeOfSale
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale getTypeOfSale() {
        return typeOfSale;
    }


    /**
     * Sets the typeOfSale value for this ItineraryPriceRequest.
     * 
     * @param typeOfSale
     */
    public void setTypeOfSale(com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale typeOfSale) {
        this.typeOfSale = typeOfSale;
    }


    /**
     * Gets the SSRRequest value for this ItineraryPriceRequest.
     * 
     * @return SSRRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SSRRequest getSSRRequest() {
        return SSRRequest;
    }


    /**
     * Sets the SSRRequest value for this ItineraryPriceRequest.
     * 
     * @param SSRRequest
     */
    public void setSSRRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.SSRRequest SSRRequest) {
        this.SSRRequest = SSRRequest;
    }


    /**
     * Gets the sellByKeyRequest value for this ItineraryPriceRequest.
     * 
     * @return sellByKeyRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequestData getSellByKeyRequest() {
        return sellByKeyRequest;
    }


    /**
     * Sets the sellByKeyRequest value for this ItineraryPriceRequest.
     * 
     * @param sellByKeyRequest
     */
    public void setSellByKeyRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequestData sellByKeyRequest) {
        this.sellByKeyRequest = sellByKeyRequest;
    }


    /**
     * Gets the priceJourneyWithLegsRequest value for this ItineraryPriceRequest.
     * 
     * @return priceJourneyWithLegsRequest
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourneyRequestData getPriceJourneyWithLegsRequest() {
        return priceJourneyWithLegsRequest;
    }


    /**
     * Sets the priceJourneyWithLegsRequest value for this ItineraryPriceRequest.
     * 
     * @param priceJourneyWithLegsRequest
     */
    public void setPriceJourneyWithLegsRequest(com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourneyRequestData priceJourneyWithLegsRequest) {
        this.priceJourneyWithLegsRequest = priceJourneyWithLegsRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ItineraryPriceRequest)) return false;
        ItineraryPriceRequest other = (ItineraryPriceRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.priceItineraryBy==null && other.getPriceItineraryBy()==null) || 
             (this.priceItineraryBy!=null &&
              this.priceItineraryBy.equals(other.getPriceItineraryBy()))) &&
            ((this.typeOfSale==null && other.getTypeOfSale()==null) || 
             (this.typeOfSale!=null &&
              this.typeOfSale.equals(other.getTypeOfSale()))) &&
            ((this.SSRRequest==null && other.getSSRRequest()==null) || 
             (this.SSRRequest!=null &&
              this.SSRRequest.equals(other.getSSRRequest()))) &&
            ((this.sellByKeyRequest==null && other.getSellByKeyRequest()==null) || 
             (this.sellByKeyRequest!=null &&
              this.sellByKeyRequest.equals(other.getSellByKeyRequest()))) &&
            ((this.priceJourneyWithLegsRequest==null && other.getPriceJourneyWithLegsRequest()==null) || 
             (this.priceJourneyWithLegsRequest!=null &&
              this.priceJourneyWithLegsRequest.equals(other.getPriceJourneyWithLegsRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPriceItineraryBy() != null) {
            _hashCode += getPriceItineraryBy().hashCode();
        }
        if (getTypeOfSale() != null) {
            _hashCode += getTypeOfSale().hashCode();
        }
        if (getSSRRequest() != null) {
            _hashCode += getSSRRequest().hashCode();
        }
        if (getSellByKeyRequest() != null) {
            _hashCode += getSellByKeyRequest().hashCode();
        }
        if (getPriceJourneyWithLegsRequest() != null) {
            _hashCode += getPriceJourneyWithLegsRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ItineraryPriceRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ItineraryPriceRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priceItineraryBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceItineraryBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PriceItineraryBy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeOfSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellByKeyRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellByKeyRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyByKeyRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priceJourneyWithLegsRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourneyWithLegsRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourneyRequestData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
