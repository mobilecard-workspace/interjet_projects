/**
 * EquipmentInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class EquipmentInfo  implements java.io.Serializable {
    private java.lang.String arrivalStation;

    private java.lang.String departureStation;

    private java.lang.String equipmentType;

    private java.lang.String equipmentTypeSuffix;

    private java.lang.Integer availableUnits;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.CompartmentInfo[] compartments;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.EquipmentCategory equipmentCategory;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] propertyList;

    private org.apache.axis.types.UnsignedInt[] propertyBits;

    private int[] propertyInts;

    private org.apache.axis.types.UnsignedInt[] propertyBitsInUse;

    private org.apache.axis.types.UnsignedInt[] propertyIntsInUse;

    private org.apache.axis.types.UnsignedInt[] SSRBitsInUse;

    private java.lang.String marketingCode;

    private java.lang.String name;

    private java.util.Calendar propertyTimestamp;

    public EquipmentInfo() {
    }

    public EquipmentInfo(
           java.lang.String arrivalStation,
           java.lang.String departureStation,
           java.lang.String equipmentType,
           java.lang.String equipmentTypeSuffix,
           java.lang.Integer availableUnits,
           com.navitaire.schemas.WebServices.DataContracts.Booking.CompartmentInfo[] compartments,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.EquipmentCategory equipmentCategory,
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] propertyList,
           org.apache.axis.types.UnsignedInt[] propertyBits,
           int[] propertyInts,
           org.apache.axis.types.UnsignedInt[] propertyBitsInUse,
           org.apache.axis.types.UnsignedInt[] propertyIntsInUse,
           org.apache.axis.types.UnsignedInt[] SSRBitsInUse,
           java.lang.String marketingCode,
           java.lang.String name,
           java.util.Calendar propertyTimestamp) {
           this.arrivalStation = arrivalStation;
           this.departureStation = departureStation;
           this.equipmentType = equipmentType;
           this.equipmentTypeSuffix = equipmentTypeSuffix;
           this.availableUnits = availableUnits;
           this.compartments = compartments;
           this.equipmentCategory = equipmentCategory;
           this.propertyList = propertyList;
           this.propertyBits = propertyBits;
           this.propertyInts = propertyInts;
           this.propertyBitsInUse = propertyBitsInUse;
           this.propertyIntsInUse = propertyIntsInUse;
           this.SSRBitsInUse = SSRBitsInUse;
           this.marketingCode = marketingCode;
           this.name = name;
           this.propertyTimestamp = propertyTimestamp;
    }


    /**
     * Gets the arrivalStation value for this EquipmentInfo.
     * 
     * @return arrivalStation
     */
    public java.lang.String getArrivalStation() {
        return arrivalStation;
    }


    /**
     * Sets the arrivalStation value for this EquipmentInfo.
     * 
     * @param arrivalStation
     */
    public void setArrivalStation(java.lang.String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }


    /**
     * Gets the departureStation value for this EquipmentInfo.
     * 
     * @return departureStation
     */
    public java.lang.String getDepartureStation() {
        return departureStation;
    }


    /**
     * Sets the departureStation value for this EquipmentInfo.
     * 
     * @param departureStation
     */
    public void setDepartureStation(java.lang.String departureStation) {
        this.departureStation = departureStation;
    }


    /**
     * Gets the equipmentType value for this EquipmentInfo.
     * 
     * @return equipmentType
     */
    public java.lang.String getEquipmentType() {
        return equipmentType;
    }


    /**
     * Sets the equipmentType value for this EquipmentInfo.
     * 
     * @param equipmentType
     */
    public void setEquipmentType(java.lang.String equipmentType) {
        this.equipmentType = equipmentType;
    }


    /**
     * Gets the equipmentTypeSuffix value for this EquipmentInfo.
     * 
     * @return equipmentTypeSuffix
     */
    public java.lang.String getEquipmentTypeSuffix() {
        return equipmentTypeSuffix;
    }


    /**
     * Sets the equipmentTypeSuffix value for this EquipmentInfo.
     * 
     * @param equipmentTypeSuffix
     */
    public void setEquipmentTypeSuffix(java.lang.String equipmentTypeSuffix) {
        this.equipmentTypeSuffix = equipmentTypeSuffix;
    }


    /**
     * Gets the availableUnits value for this EquipmentInfo.
     * 
     * @return availableUnits
     */
    public java.lang.Integer getAvailableUnits() {
        return availableUnits;
    }


    /**
     * Sets the availableUnits value for this EquipmentInfo.
     * 
     * @param availableUnits
     */
    public void setAvailableUnits(java.lang.Integer availableUnits) {
        this.availableUnits = availableUnits;
    }


    /**
     * Gets the compartments value for this EquipmentInfo.
     * 
     * @return compartments
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.CompartmentInfo[] getCompartments() {
        return compartments;
    }


    /**
     * Sets the compartments value for this EquipmentInfo.
     * 
     * @param compartments
     */
    public void setCompartments(com.navitaire.schemas.WebServices.DataContracts.Booking.CompartmentInfo[] compartments) {
        this.compartments = compartments;
    }


    /**
     * Gets the equipmentCategory value for this EquipmentInfo.
     * 
     * @return equipmentCategory
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.EquipmentCategory getEquipmentCategory() {
        return equipmentCategory;
    }


    /**
     * Sets the equipmentCategory value for this EquipmentInfo.
     * 
     * @param equipmentCategory
     */
    public void setEquipmentCategory(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.EquipmentCategory equipmentCategory) {
        this.equipmentCategory = equipmentCategory;
    }


    /**
     * Gets the propertyList value for this EquipmentInfo.
     * 
     * @return propertyList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] getPropertyList() {
        return propertyList;
    }


    /**
     * Sets the propertyList value for this EquipmentInfo.
     * 
     * @param propertyList
     */
    public void setPropertyList(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] propertyList) {
        this.propertyList = propertyList;
    }


    /**
     * Gets the propertyBits value for this EquipmentInfo.
     * 
     * @return propertyBits
     */
    public org.apache.axis.types.UnsignedInt[] getPropertyBits() {
        return propertyBits;
    }


    /**
     * Sets the propertyBits value for this EquipmentInfo.
     * 
     * @param propertyBits
     */
    public void setPropertyBits(org.apache.axis.types.UnsignedInt[] propertyBits) {
        this.propertyBits = propertyBits;
    }


    /**
     * Gets the propertyInts value for this EquipmentInfo.
     * 
     * @return propertyInts
     */
    public int[] getPropertyInts() {
        return propertyInts;
    }


    /**
     * Sets the propertyInts value for this EquipmentInfo.
     * 
     * @param propertyInts
     */
    public void setPropertyInts(int[] propertyInts) {
        this.propertyInts = propertyInts;
    }


    /**
     * Gets the propertyBitsInUse value for this EquipmentInfo.
     * 
     * @return propertyBitsInUse
     */
    public org.apache.axis.types.UnsignedInt[] getPropertyBitsInUse() {
        return propertyBitsInUse;
    }


    /**
     * Sets the propertyBitsInUse value for this EquipmentInfo.
     * 
     * @param propertyBitsInUse
     */
    public void setPropertyBitsInUse(org.apache.axis.types.UnsignedInt[] propertyBitsInUse) {
        this.propertyBitsInUse = propertyBitsInUse;
    }


    /**
     * Gets the propertyIntsInUse value for this EquipmentInfo.
     * 
     * @return propertyIntsInUse
     */
    public org.apache.axis.types.UnsignedInt[] getPropertyIntsInUse() {
        return propertyIntsInUse;
    }


    /**
     * Sets the propertyIntsInUse value for this EquipmentInfo.
     * 
     * @param propertyIntsInUse
     */
    public void setPropertyIntsInUse(org.apache.axis.types.UnsignedInt[] propertyIntsInUse) {
        this.propertyIntsInUse = propertyIntsInUse;
    }


    /**
     * Gets the SSRBitsInUse value for this EquipmentInfo.
     * 
     * @return SSRBitsInUse
     */
    public org.apache.axis.types.UnsignedInt[] getSSRBitsInUse() {
        return SSRBitsInUse;
    }


    /**
     * Sets the SSRBitsInUse value for this EquipmentInfo.
     * 
     * @param SSRBitsInUse
     */
    public void setSSRBitsInUse(org.apache.axis.types.UnsignedInt[] SSRBitsInUse) {
        this.SSRBitsInUse = SSRBitsInUse;
    }


    /**
     * Gets the marketingCode value for this EquipmentInfo.
     * 
     * @return marketingCode
     */
    public java.lang.String getMarketingCode() {
        return marketingCode;
    }


    /**
     * Sets the marketingCode value for this EquipmentInfo.
     * 
     * @param marketingCode
     */
    public void setMarketingCode(java.lang.String marketingCode) {
        this.marketingCode = marketingCode;
    }


    /**
     * Gets the name value for this EquipmentInfo.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this EquipmentInfo.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the propertyTimestamp value for this EquipmentInfo.
     * 
     * @return propertyTimestamp
     */
    public java.util.Calendar getPropertyTimestamp() {
        return propertyTimestamp;
    }


    /**
     * Sets the propertyTimestamp value for this EquipmentInfo.
     * 
     * @param propertyTimestamp
     */
    public void setPropertyTimestamp(java.util.Calendar propertyTimestamp) {
        this.propertyTimestamp = propertyTimestamp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EquipmentInfo)) return false;
        EquipmentInfo other = (EquipmentInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.arrivalStation==null && other.getArrivalStation()==null) || 
             (this.arrivalStation!=null &&
              this.arrivalStation.equals(other.getArrivalStation()))) &&
            ((this.departureStation==null && other.getDepartureStation()==null) || 
             (this.departureStation!=null &&
              this.departureStation.equals(other.getDepartureStation()))) &&
            ((this.equipmentType==null && other.getEquipmentType()==null) || 
             (this.equipmentType!=null &&
              this.equipmentType.equals(other.getEquipmentType()))) &&
            ((this.equipmentTypeSuffix==null && other.getEquipmentTypeSuffix()==null) || 
             (this.equipmentTypeSuffix!=null &&
              this.equipmentTypeSuffix.equals(other.getEquipmentTypeSuffix()))) &&
            ((this.availableUnits==null && other.getAvailableUnits()==null) || 
             (this.availableUnits!=null &&
              this.availableUnits.equals(other.getAvailableUnits()))) &&
            ((this.compartments==null && other.getCompartments()==null) || 
             (this.compartments!=null &&
              java.util.Arrays.equals(this.compartments, other.getCompartments()))) &&
            ((this.equipmentCategory==null && other.getEquipmentCategory()==null) || 
             (this.equipmentCategory!=null &&
              this.equipmentCategory.equals(other.getEquipmentCategory()))) &&
            ((this.propertyList==null && other.getPropertyList()==null) || 
             (this.propertyList!=null &&
              java.util.Arrays.equals(this.propertyList, other.getPropertyList()))) &&
            ((this.propertyBits==null && other.getPropertyBits()==null) || 
             (this.propertyBits!=null &&
              java.util.Arrays.equals(this.propertyBits, other.getPropertyBits()))) &&
            ((this.propertyInts==null && other.getPropertyInts()==null) || 
             (this.propertyInts!=null &&
              java.util.Arrays.equals(this.propertyInts, other.getPropertyInts()))) &&
            ((this.propertyBitsInUse==null && other.getPropertyBitsInUse()==null) || 
             (this.propertyBitsInUse!=null &&
              java.util.Arrays.equals(this.propertyBitsInUse, other.getPropertyBitsInUse()))) &&
            ((this.propertyIntsInUse==null && other.getPropertyIntsInUse()==null) || 
             (this.propertyIntsInUse!=null &&
              java.util.Arrays.equals(this.propertyIntsInUse, other.getPropertyIntsInUse()))) &&
            ((this.SSRBitsInUse==null && other.getSSRBitsInUse()==null) || 
             (this.SSRBitsInUse!=null &&
              java.util.Arrays.equals(this.SSRBitsInUse, other.getSSRBitsInUse()))) &&
            ((this.marketingCode==null && other.getMarketingCode()==null) || 
             (this.marketingCode!=null &&
              this.marketingCode.equals(other.getMarketingCode()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.propertyTimestamp==null && other.getPropertyTimestamp()==null) || 
             (this.propertyTimestamp!=null &&
              this.propertyTimestamp.equals(other.getPropertyTimestamp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getArrivalStation() != null) {
            _hashCode += getArrivalStation().hashCode();
        }
        if (getDepartureStation() != null) {
            _hashCode += getDepartureStation().hashCode();
        }
        if (getEquipmentType() != null) {
            _hashCode += getEquipmentType().hashCode();
        }
        if (getEquipmentTypeSuffix() != null) {
            _hashCode += getEquipmentTypeSuffix().hashCode();
        }
        if (getAvailableUnits() != null) {
            _hashCode += getAvailableUnits().hashCode();
        }
        if (getCompartments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCompartments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCompartments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEquipmentCategory() != null) {
            _hashCode += getEquipmentCategory().hashCode();
        }
        if (getPropertyList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyBits() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyBits());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyBits(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyInts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyInts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyInts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyBitsInUse() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyBitsInUse());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyBitsInUse(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyIntsInUse() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyIntsInUse());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyIntsInUse(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSSRBitsInUse() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSSRBitsInUse());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSSRBitsInUse(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMarketingCode() != null) {
            _hashCode += getMarketingCode().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getPropertyTimestamp() != null) {
            _hashCode += getPropertyTimestamp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EquipmentInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrivalStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departureStation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DepartureStation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentTypeSuffix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentTypeSuffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableUnits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailableUnits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("compartments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Compartments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentInfo"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("equipmentCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "EquipmentCategory"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyBits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyBits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "unsignedInt"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyInts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyInts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyBitsInUse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyBitsInUse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "unsignedInt"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyIntsInUse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyIntsInUse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "unsignedInt"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRBitsInUse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRBitsInUse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "unsignedInt"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketingCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MarketingCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyTimestamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyTimestamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
