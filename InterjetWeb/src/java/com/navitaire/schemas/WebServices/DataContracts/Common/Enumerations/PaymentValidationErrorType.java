/**
 * PaymentValidationErrorType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class PaymentValidationErrorType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PaymentValidationErrorType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Unknown = "Unknown";
    public static final java.lang.String _Other = "Other";
    public static final java.lang.String _AccountNumber = "AccountNumber";
    public static final java.lang.String _Amount = "Amount";
    public static final java.lang.String _ExpirationDate = "ExpirationDate";
    public static final java.lang.String _RestrictionHours = "RestrictionHours";
    public static final java.lang.String _MissingAccountNumber = "MissingAccountNumber";
    public static final java.lang.String _MissingExpirationDate = "MissingExpirationDate";
    public static final java.lang.String _PaymentSystemUnavailable = "PaymentSystemUnavailable";
    public static final java.lang.String _MissingParentPaymentID = "MissingParentPaymentID";
    public static final java.lang.String _InProcessPaymentChanged = "InProcessPaymentChanged";
    public static final java.lang.String _InvalidNumberOfInstallments = "InvalidNumberOfInstallments";
    public static final java.lang.String _CreditShellCommentRequired = "CreditShellCommentRequired";
    public static final java.lang.String _NoQuotedCurrencyProvided = "NoQuotedCurrencyProvided";
    public static final java.lang.String _NoBaseCurrencyForBooking = "NoBaseCurrencyForBooking";
    public static final java.lang.String _QuotedCurrencyDoesNotMatchBaseCurrency = "QuotedCurrencyDoesNotMatchBaseCurrency";
    public static final java.lang.String _QuotedRefundAmountNotLessThanZero = "QuotedRefundAmountNotLessThanZero";
    public static final java.lang.String _QuotedPaymentAmountIsLessThanZero = "QuotedPaymentAmountIsLessThanZero";
    public static final java.lang.String _RoleCodeNotFound = "RoleCodeNotFound";
    public static final java.lang.String _UnknownOrInactivePaymentMethod = "UnknownOrInactivePaymentMethod";
    public static final java.lang.String _DepositPaymentsNotAllowedForPaymentMethod = "DepositPaymentsNotAllowedForPaymentMethod";
    public static final java.lang.String _UnableToRetrieveRoleCodeSettings = "UnableToRetrieveRoleCodeSettings";
    public static final java.lang.String _DepositPaymentsNotAllowedForRole = "DepositPaymentsNotAllowedForRole";
    public static final java.lang.String _PaymentMethodNotAllowedForRole = "PaymentMethodNotAllowedForRole";
    public static final java.lang.String _InvalidAccountNumberLength = "InvalidAccountNumberLength";
    public static final java.lang.String _PaymentTextIsRequired = "PaymentTextIsRequired";
    public static final java.lang.String _InvalidPaymentTextLength = "InvalidPaymentTextLength";
    public static final java.lang.String _InvalidMiscPaymentFieldLength = "InvalidMiscPaymentFieldLength";
    public static final java.lang.String _MiscPaymentFieldRequired = "MiscPaymentFieldRequired";
    public static final java.lang.String _BookingCurrencyIsInvalidForSkyPay = "BookingCurrencyIsInvalidForSkyPay";
    public static final java.lang.String _SkyPayExceptionThrown = "SkyPayExceptionThrown";
    public static final java.lang.String _InvalidAccountNumberForPaymentMethod = "InvalidAccountNumberForPaymentMethod";
    public static final java.lang.String _InvalidELVTransaction = "InvalidELVTransaction";
    public static final java.lang.String _BlackListedCard = "BlackListedCard";
    public static final java.lang.String _InvalidPaymentAddress = "InvalidPaymentAddress";
    public static final java.lang.String _InvalidSecurityCode = "InvalidSecurityCode";
    public static final java.lang.String _InvalidCurrencyCode = "InvalidCurrencyCode";
    public static final java.lang.String _InvalidAmount = "InvalidAmount";
    public static final java.lang.String _PossibleFraud = "PossibleFraud";
    public static final java.lang.String _InvalidCustomerAccount = "InvalidCustomerAccount";
    public static final java.lang.String _AccountHolderIsNotAnAgency = "AccountHolderIsNotAnAgency";
    public static final java.lang.String _InvalidStartDate = "InvalidStartDate";
    public static final java.lang.String _InvalidInitialPaymentStatus = "InvalidInitialPaymentStatus";
    public static final java.lang.String _PaymentCurrencyMustMatchBookingCurrency = "PaymentCurrencyMustMatchBookingCurrency";
    public static final java.lang.String _CollectedAmountMustMatchPaymentAmount = "CollectedAmountMustMatchPaymentAmount";
    public static final java.lang.String _RefundsNotAllowedUsingThisPaymentMethod = "RefundsNotAllowedUsingThisPaymentMethod";
    public static final java.lang.String _CreditShellAmountGreaterThanOrEqualToZero = "CreditShellAmountGreaterThanOrEqualToZero";
    public static final java.lang.String _CreditFileAmountLessThanOrEqualToZero = "CreditFileAmountLessThanOrEqualToZero";
    public static final java.lang.String _InvalidPrepaidApprovalCodeLength = "InvalidPrepaidApprovalCodeLength";
    public static final java.lang.String _AccountNumberFailedModulousCheck = "AccountNumberFailedModulousCheck";
    public static final java.lang.String _NoExternalRatesAvailable = "NoExternalRatesAvailable";
    public static final java.lang.String _ExternalCurrencyConversion = "ExternalCurrencyConversion";
    public static final java.lang.String _StoredCardSecurityViolation = "StoredCardSecurityViolation";
    public static final java.lang.String _AccountNumberDecryptionFailure = "AccountNumberDecryptionFailure";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final PaymentValidationErrorType Unknown = new PaymentValidationErrorType(_Unknown);
    public static final PaymentValidationErrorType Other = new PaymentValidationErrorType(_Other);
    public static final PaymentValidationErrorType AccountNumber = new PaymentValidationErrorType(_AccountNumber);
    public static final PaymentValidationErrorType Amount = new PaymentValidationErrorType(_Amount);
    public static final PaymentValidationErrorType ExpirationDate = new PaymentValidationErrorType(_ExpirationDate);
    public static final PaymentValidationErrorType RestrictionHours = new PaymentValidationErrorType(_RestrictionHours);
    public static final PaymentValidationErrorType MissingAccountNumber = new PaymentValidationErrorType(_MissingAccountNumber);
    public static final PaymentValidationErrorType MissingExpirationDate = new PaymentValidationErrorType(_MissingExpirationDate);
    public static final PaymentValidationErrorType PaymentSystemUnavailable = new PaymentValidationErrorType(_PaymentSystemUnavailable);
    public static final PaymentValidationErrorType MissingParentPaymentID = new PaymentValidationErrorType(_MissingParentPaymentID);
    public static final PaymentValidationErrorType InProcessPaymentChanged = new PaymentValidationErrorType(_InProcessPaymentChanged);
    public static final PaymentValidationErrorType InvalidNumberOfInstallments = new PaymentValidationErrorType(_InvalidNumberOfInstallments);
    public static final PaymentValidationErrorType CreditShellCommentRequired = new PaymentValidationErrorType(_CreditShellCommentRequired);
    public static final PaymentValidationErrorType NoQuotedCurrencyProvided = new PaymentValidationErrorType(_NoQuotedCurrencyProvided);
    public static final PaymentValidationErrorType NoBaseCurrencyForBooking = new PaymentValidationErrorType(_NoBaseCurrencyForBooking);
    public static final PaymentValidationErrorType QuotedCurrencyDoesNotMatchBaseCurrency = new PaymentValidationErrorType(_QuotedCurrencyDoesNotMatchBaseCurrency);
    public static final PaymentValidationErrorType QuotedRefundAmountNotLessThanZero = new PaymentValidationErrorType(_QuotedRefundAmountNotLessThanZero);
    public static final PaymentValidationErrorType QuotedPaymentAmountIsLessThanZero = new PaymentValidationErrorType(_QuotedPaymentAmountIsLessThanZero);
    public static final PaymentValidationErrorType RoleCodeNotFound = new PaymentValidationErrorType(_RoleCodeNotFound);
    public static final PaymentValidationErrorType UnknownOrInactivePaymentMethod = new PaymentValidationErrorType(_UnknownOrInactivePaymentMethod);
    public static final PaymentValidationErrorType DepositPaymentsNotAllowedForPaymentMethod = new PaymentValidationErrorType(_DepositPaymentsNotAllowedForPaymentMethod);
    public static final PaymentValidationErrorType UnableToRetrieveRoleCodeSettings = new PaymentValidationErrorType(_UnableToRetrieveRoleCodeSettings);
    public static final PaymentValidationErrorType DepositPaymentsNotAllowedForRole = new PaymentValidationErrorType(_DepositPaymentsNotAllowedForRole);
    public static final PaymentValidationErrorType PaymentMethodNotAllowedForRole = new PaymentValidationErrorType(_PaymentMethodNotAllowedForRole);
    public static final PaymentValidationErrorType InvalidAccountNumberLength = new PaymentValidationErrorType(_InvalidAccountNumberLength);
    public static final PaymentValidationErrorType PaymentTextIsRequired = new PaymentValidationErrorType(_PaymentTextIsRequired);
    public static final PaymentValidationErrorType InvalidPaymentTextLength = new PaymentValidationErrorType(_InvalidPaymentTextLength);
    public static final PaymentValidationErrorType InvalidMiscPaymentFieldLength = new PaymentValidationErrorType(_InvalidMiscPaymentFieldLength);
    public static final PaymentValidationErrorType MiscPaymentFieldRequired = new PaymentValidationErrorType(_MiscPaymentFieldRequired);
    public static final PaymentValidationErrorType BookingCurrencyIsInvalidForSkyPay = new PaymentValidationErrorType(_BookingCurrencyIsInvalidForSkyPay);
    public static final PaymentValidationErrorType SkyPayExceptionThrown = new PaymentValidationErrorType(_SkyPayExceptionThrown);
    public static final PaymentValidationErrorType InvalidAccountNumberForPaymentMethod = new PaymentValidationErrorType(_InvalidAccountNumberForPaymentMethod);
    public static final PaymentValidationErrorType InvalidELVTransaction = new PaymentValidationErrorType(_InvalidELVTransaction);
    public static final PaymentValidationErrorType BlackListedCard = new PaymentValidationErrorType(_BlackListedCard);
    public static final PaymentValidationErrorType InvalidPaymentAddress = new PaymentValidationErrorType(_InvalidPaymentAddress);
    public static final PaymentValidationErrorType InvalidSecurityCode = new PaymentValidationErrorType(_InvalidSecurityCode);
    public static final PaymentValidationErrorType InvalidCurrencyCode = new PaymentValidationErrorType(_InvalidCurrencyCode);
    public static final PaymentValidationErrorType InvalidAmount = new PaymentValidationErrorType(_InvalidAmount);
    public static final PaymentValidationErrorType PossibleFraud = new PaymentValidationErrorType(_PossibleFraud);
    public static final PaymentValidationErrorType InvalidCustomerAccount = new PaymentValidationErrorType(_InvalidCustomerAccount);
    public static final PaymentValidationErrorType AccountHolderIsNotAnAgency = new PaymentValidationErrorType(_AccountHolderIsNotAnAgency);
    public static final PaymentValidationErrorType InvalidStartDate = new PaymentValidationErrorType(_InvalidStartDate);
    public static final PaymentValidationErrorType InvalidInitialPaymentStatus = new PaymentValidationErrorType(_InvalidInitialPaymentStatus);
    public static final PaymentValidationErrorType PaymentCurrencyMustMatchBookingCurrency = new PaymentValidationErrorType(_PaymentCurrencyMustMatchBookingCurrency);
    public static final PaymentValidationErrorType CollectedAmountMustMatchPaymentAmount = new PaymentValidationErrorType(_CollectedAmountMustMatchPaymentAmount);
    public static final PaymentValidationErrorType RefundsNotAllowedUsingThisPaymentMethod = new PaymentValidationErrorType(_RefundsNotAllowedUsingThisPaymentMethod);
    public static final PaymentValidationErrorType CreditShellAmountGreaterThanOrEqualToZero = new PaymentValidationErrorType(_CreditShellAmountGreaterThanOrEqualToZero);
    public static final PaymentValidationErrorType CreditFileAmountLessThanOrEqualToZero = new PaymentValidationErrorType(_CreditFileAmountLessThanOrEqualToZero);
    public static final PaymentValidationErrorType InvalidPrepaidApprovalCodeLength = new PaymentValidationErrorType(_InvalidPrepaidApprovalCodeLength);
    public static final PaymentValidationErrorType AccountNumberFailedModulousCheck = new PaymentValidationErrorType(_AccountNumberFailedModulousCheck);
    public static final PaymentValidationErrorType NoExternalRatesAvailable = new PaymentValidationErrorType(_NoExternalRatesAvailable);
    public static final PaymentValidationErrorType ExternalCurrencyConversion = new PaymentValidationErrorType(_ExternalCurrencyConversion);
    public static final PaymentValidationErrorType StoredCardSecurityViolation = new PaymentValidationErrorType(_StoredCardSecurityViolation);
    public static final PaymentValidationErrorType AccountNumberDecryptionFailure = new PaymentValidationErrorType(_AccountNumberDecryptionFailure);
    public static final PaymentValidationErrorType Unmapped = new PaymentValidationErrorType(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static PaymentValidationErrorType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        PaymentValidationErrorType enumeration = (PaymentValidationErrorType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static PaymentValidationErrorType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentValidationErrorType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentValidationErrorType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
