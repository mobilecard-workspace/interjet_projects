/**
 * PaymentMethodType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class PaymentMethodType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PaymentMethodType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _PrePaid = "PrePaid";
    public static final java.lang.String _ExternalAccount = "ExternalAccount";
    public static final java.lang.String _AgencyAccount = "AgencyAccount";
    public static final java.lang.String _CustomerAccount = "CustomerAccount";
    public static final java.lang.String _Voucher = "Voucher";
    public static final java.lang.String _Loyalty = "Loyalty";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final PaymentMethodType PrePaid = new PaymentMethodType(_PrePaid);
    public static final PaymentMethodType ExternalAccount = new PaymentMethodType(_ExternalAccount);
    public static final PaymentMethodType AgencyAccount = new PaymentMethodType(_AgencyAccount);
    public static final PaymentMethodType CustomerAccount = new PaymentMethodType(_CustomerAccount);
    public static final PaymentMethodType Voucher = new PaymentMethodType(_Voucher);
    public static final PaymentMethodType Loyalty = new PaymentMethodType(_Loyalty);
    public static final PaymentMethodType Unmapped = new PaymentMethodType(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static PaymentMethodType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        PaymentMethodType enumeration = (PaymentMethodType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static PaymentMethodType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentMethodType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentMethodType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
