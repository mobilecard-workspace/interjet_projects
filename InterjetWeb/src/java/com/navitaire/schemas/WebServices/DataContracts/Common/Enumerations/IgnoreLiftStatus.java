/**
 * IgnoreLiftStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations;

public class IgnoreLiftStatus implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected IgnoreLiftStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _IgnoreNotAllowed = "IgnoreNotAllowed";
    public static final java.lang.String _IgnoreCheckin = "IgnoreCheckin";
    public static final java.lang.String _IgnoreBoarded = "IgnoreBoarded";
    public static final java.lang.String _Unmapped = "Unmapped";
    public static final IgnoreLiftStatus IgnoreNotAllowed = new IgnoreLiftStatus(_IgnoreNotAllowed);
    public static final IgnoreLiftStatus IgnoreCheckin = new IgnoreLiftStatus(_IgnoreCheckin);
    public static final IgnoreLiftStatus IgnoreBoarded = new IgnoreLiftStatus(_IgnoreBoarded);
    public static final IgnoreLiftStatus Unmapped = new IgnoreLiftStatus(_Unmapped);
    public java.lang.String getValue() { return _value_;}
    public static IgnoreLiftStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        IgnoreLiftStatus enumeration = (IgnoreLiftStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static IgnoreLiftStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IgnoreLiftStatus.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "IgnoreLiftStatus"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
