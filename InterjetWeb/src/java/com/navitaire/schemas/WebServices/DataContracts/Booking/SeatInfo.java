/**
 * SeatInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class SeatInfo  implements java.io.Serializable {
    private java.lang.Boolean assignable;

    private java.lang.Integer cabotageLevel;

    private java.lang.Integer carAvailableUnits;

    private java.lang.String compartmentDesignator;

    private java.lang.Integer seatSet;

    private java.lang.Integer criterionWeight;

    private java.lang.Integer seatSetAvailableUnits;

    private java.lang.String SSRSeatMapCode;

    private java.lang.Short seatAngle;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAvailability seatAvailability;

    private java.lang.String seatDesignator;

    private java.lang.String seatType;

    private java.lang.Short x;

    private java.lang.Short y;

    private com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] propertyList;

    private java.lang.String[] SSRPermissions;

    private org.apache.axis.types.UnsignedInt[] SSRPermissionBits;

    private org.apache.axis.types.UnsignedInt[] propertyBits;

    private int[] propertyInts;

    private java.lang.String travelClassCode;

    private java.util.Calendar propertyTimestamp;

    private java.lang.Short seatGroup;

    private java.lang.Short zone;

    private java.lang.Short height;

    private java.lang.Short width;

    private java.lang.Short priority;

    private java.lang.String text;

    private java.lang.Integer ODPenalty;

    private java.lang.String terminalDisplayCharacter;

    private java.lang.Boolean premiumSeatIndicator;

    public SeatInfo() {
    }

    public SeatInfo(
           java.lang.Boolean assignable,
           java.lang.Integer cabotageLevel,
           java.lang.Integer carAvailableUnits,
           java.lang.String compartmentDesignator,
           java.lang.Integer seatSet,
           java.lang.Integer criterionWeight,
           java.lang.Integer seatSetAvailableUnits,
           java.lang.String SSRSeatMapCode,
           java.lang.Short seatAngle,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAvailability seatAvailability,
           java.lang.String seatDesignator,
           java.lang.String seatType,
           java.lang.Short x,
           java.lang.Short y,
           com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] propertyList,
           java.lang.String[] SSRPermissions,
           org.apache.axis.types.UnsignedInt[] SSRPermissionBits,
           org.apache.axis.types.UnsignedInt[] propertyBits,
           int[] propertyInts,
           java.lang.String travelClassCode,
           java.util.Calendar propertyTimestamp,
           java.lang.Short seatGroup,
           java.lang.Short zone,
           java.lang.Short height,
           java.lang.Short width,
           java.lang.Short priority,
           java.lang.String text,
           java.lang.Integer ODPenalty,
           java.lang.String terminalDisplayCharacter,
           java.lang.Boolean premiumSeatIndicator) {
           this.assignable = assignable;
           this.cabotageLevel = cabotageLevel;
           this.carAvailableUnits = carAvailableUnits;
           this.compartmentDesignator = compartmentDesignator;
           this.seatSet = seatSet;
           this.criterionWeight = criterionWeight;
           this.seatSetAvailableUnits = seatSetAvailableUnits;
           this.SSRSeatMapCode = SSRSeatMapCode;
           this.seatAngle = seatAngle;
           this.seatAvailability = seatAvailability;
           this.seatDesignator = seatDesignator;
           this.seatType = seatType;
           this.x = x;
           this.y = y;
           this.propertyList = propertyList;
           this.SSRPermissions = SSRPermissions;
           this.SSRPermissionBits = SSRPermissionBits;
           this.propertyBits = propertyBits;
           this.propertyInts = propertyInts;
           this.travelClassCode = travelClassCode;
           this.propertyTimestamp = propertyTimestamp;
           this.seatGroup = seatGroup;
           this.zone = zone;
           this.height = height;
           this.width = width;
           this.priority = priority;
           this.text = text;
           this.ODPenalty = ODPenalty;
           this.terminalDisplayCharacter = terminalDisplayCharacter;
           this.premiumSeatIndicator = premiumSeatIndicator;
    }


    /**
     * Gets the assignable value for this SeatInfo.
     * 
     * @return assignable
     */
    public java.lang.Boolean getAssignable() {
        return assignable;
    }


    /**
     * Sets the assignable value for this SeatInfo.
     * 
     * @param assignable
     */
    public void setAssignable(java.lang.Boolean assignable) {
        this.assignable = assignable;
    }


    /**
     * Gets the cabotageLevel value for this SeatInfo.
     * 
     * @return cabotageLevel
     */
    public java.lang.Integer getCabotageLevel() {
        return cabotageLevel;
    }


    /**
     * Sets the cabotageLevel value for this SeatInfo.
     * 
     * @param cabotageLevel
     */
    public void setCabotageLevel(java.lang.Integer cabotageLevel) {
        this.cabotageLevel = cabotageLevel;
    }


    /**
     * Gets the carAvailableUnits value for this SeatInfo.
     * 
     * @return carAvailableUnits
     */
    public java.lang.Integer getCarAvailableUnits() {
        return carAvailableUnits;
    }


    /**
     * Sets the carAvailableUnits value for this SeatInfo.
     * 
     * @param carAvailableUnits
     */
    public void setCarAvailableUnits(java.lang.Integer carAvailableUnits) {
        this.carAvailableUnits = carAvailableUnits;
    }


    /**
     * Gets the compartmentDesignator value for this SeatInfo.
     * 
     * @return compartmentDesignator
     */
    public java.lang.String getCompartmentDesignator() {
        return compartmentDesignator;
    }


    /**
     * Sets the compartmentDesignator value for this SeatInfo.
     * 
     * @param compartmentDesignator
     */
    public void setCompartmentDesignator(java.lang.String compartmentDesignator) {
        this.compartmentDesignator = compartmentDesignator;
    }


    /**
     * Gets the seatSet value for this SeatInfo.
     * 
     * @return seatSet
     */
    public java.lang.Integer getSeatSet() {
        return seatSet;
    }


    /**
     * Sets the seatSet value for this SeatInfo.
     * 
     * @param seatSet
     */
    public void setSeatSet(java.lang.Integer seatSet) {
        this.seatSet = seatSet;
    }


    /**
     * Gets the criterionWeight value for this SeatInfo.
     * 
     * @return criterionWeight
     */
    public java.lang.Integer getCriterionWeight() {
        return criterionWeight;
    }


    /**
     * Sets the criterionWeight value for this SeatInfo.
     * 
     * @param criterionWeight
     */
    public void setCriterionWeight(java.lang.Integer criterionWeight) {
        this.criterionWeight = criterionWeight;
    }


    /**
     * Gets the seatSetAvailableUnits value for this SeatInfo.
     * 
     * @return seatSetAvailableUnits
     */
    public java.lang.Integer getSeatSetAvailableUnits() {
        return seatSetAvailableUnits;
    }


    /**
     * Sets the seatSetAvailableUnits value for this SeatInfo.
     * 
     * @param seatSetAvailableUnits
     */
    public void setSeatSetAvailableUnits(java.lang.Integer seatSetAvailableUnits) {
        this.seatSetAvailableUnits = seatSetAvailableUnits;
    }


    /**
     * Gets the SSRSeatMapCode value for this SeatInfo.
     * 
     * @return SSRSeatMapCode
     */
    public java.lang.String getSSRSeatMapCode() {
        return SSRSeatMapCode;
    }


    /**
     * Sets the SSRSeatMapCode value for this SeatInfo.
     * 
     * @param SSRSeatMapCode
     */
    public void setSSRSeatMapCode(java.lang.String SSRSeatMapCode) {
        this.SSRSeatMapCode = SSRSeatMapCode;
    }


    /**
     * Gets the seatAngle value for this SeatInfo.
     * 
     * @return seatAngle
     */
    public java.lang.Short getSeatAngle() {
        return seatAngle;
    }


    /**
     * Sets the seatAngle value for this SeatInfo.
     * 
     * @param seatAngle
     */
    public void setSeatAngle(java.lang.Short seatAngle) {
        this.seatAngle = seatAngle;
    }


    /**
     * Gets the seatAvailability value for this SeatInfo.
     * 
     * @return seatAvailability
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAvailability getSeatAvailability() {
        return seatAvailability;
    }


    /**
     * Sets the seatAvailability value for this SeatInfo.
     * 
     * @param seatAvailability
     */
    public void setSeatAvailability(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAvailability seatAvailability) {
        this.seatAvailability = seatAvailability;
    }


    /**
     * Gets the seatDesignator value for this SeatInfo.
     * 
     * @return seatDesignator
     */
    public java.lang.String getSeatDesignator() {
        return seatDesignator;
    }


    /**
     * Sets the seatDesignator value for this SeatInfo.
     * 
     * @param seatDesignator
     */
    public void setSeatDesignator(java.lang.String seatDesignator) {
        this.seatDesignator = seatDesignator;
    }


    /**
     * Gets the seatType value for this SeatInfo.
     * 
     * @return seatType
     */
    public java.lang.String getSeatType() {
        return seatType;
    }


    /**
     * Sets the seatType value for this SeatInfo.
     * 
     * @param seatType
     */
    public void setSeatType(java.lang.String seatType) {
        this.seatType = seatType;
    }


    /**
     * Gets the x value for this SeatInfo.
     * 
     * @return x
     */
    public java.lang.Short getX() {
        return x;
    }


    /**
     * Sets the x value for this SeatInfo.
     * 
     * @param x
     */
    public void setX(java.lang.Short x) {
        this.x = x;
    }


    /**
     * Gets the y value for this SeatInfo.
     * 
     * @return y
     */
    public java.lang.Short getY() {
        return y;
    }


    /**
     * Sets the y value for this SeatInfo.
     * 
     * @param y
     */
    public void setY(java.lang.Short y) {
        this.y = y;
    }


    /**
     * Gets the propertyList value for this SeatInfo.
     * 
     * @return propertyList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] getPropertyList() {
        return propertyList;
    }


    /**
     * Sets the propertyList value for this SeatInfo.
     * 
     * @param propertyList
     */
    public void setPropertyList(com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[] propertyList) {
        this.propertyList = propertyList;
    }


    /**
     * Gets the SSRPermissions value for this SeatInfo.
     * 
     * @return SSRPermissions
     */
    public java.lang.String[] getSSRPermissions() {
        return SSRPermissions;
    }


    /**
     * Sets the SSRPermissions value for this SeatInfo.
     * 
     * @param SSRPermissions
     */
    public void setSSRPermissions(java.lang.String[] SSRPermissions) {
        this.SSRPermissions = SSRPermissions;
    }


    /**
     * Gets the SSRPermissionBits value for this SeatInfo.
     * 
     * @return SSRPermissionBits
     */
    public org.apache.axis.types.UnsignedInt[] getSSRPermissionBits() {
        return SSRPermissionBits;
    }


    /**
     * Sets the SSRPermissionBits value for this SeatInfo.
     * 
     * @param SSRPermissionBits
     */
    public void setSSRPermissionBits(org.apache.axis.types.UnsignedInt[] SSRPermissionBits) {
        this.SSRPermissionBits = SSRPermissionBits;
    }


    /**
     * Gets the propertyBits value for this SeatInfo.
     * 
     * @return propertyBits
     */
    public org.apache.axis.types.UnsignedInt[] getPropertyBits() {
        return propertyBits;
    }


    /**
     * Sets the propertyBits value for this SeatInfo.
     * 
     * @param propertyBits
     */
    public void setPropertyBits(org.apache.axis.types.UnsignedInt[] propertyBits) {
        this.propertyBits = propertyBits;
    }


    /**
     * Gets the propertyInts value for this SeatInfo.
     * 
     * @return propertyInts
     */
    public int[] getPropertyInts() {
        return propertyInts;
    }


    /**
     * Sets the propertyInts value for this SeatInfo.
     * 
     * @param propertyInts
     */
    public void setPropertyInts(int[] propertyInts) {
        this.propertyInts = propertyInts;
    }


    /**
     * Gets the travelClassCode value for this SeatInfo.
     * 
     * @return travelClassCode
     */
    public java.lang.String getTravelClassCode() {
        return travelClassCode;
    }


    /**
     * Sets the travelClassCode value for this SeatInfo.
     * 
     * @param travelClassCode
     */
    public void setTravelClassCode(java.lang.String travelClassCode) {
        this.travelClassCode = travelClassCode;
    }


    /**
     * Gets the propertyTimestamp value for this SeatInfo.
     * 
     * @return propertyTimestamp
     */
    public java.util.Calendar getPropertyTimestamp() {
        return propertyTimestamp;
    }


    /**
     * Sets the propertyTimestamp value for this SeatInfo.
     * 
     * @param propertyTimestamp
     */
    public void setPropertyTimestamp(java.util.Calendar propertyTimestamp) {
        this.propertyTimestamp = propertyTimestamp;
    }


    /**
     * Gets the seatGroup value for this SeatInfo.
     * 
     * @return seatGroup
     */
    public java.lang.Short getSeatGroup() {
        return seatGroup;
    }


    /**
     * Sets the seatGroup value for this SeatInfo.
     * 
     * @param seatGroup
     */
    public void setSeatGroup(java.lang.Short seatGroup) {
        this.seatGroup = seatGroup;
    }


    /**
     * Gets the zone value for this SeatInfo.
     * 
     * @return zone
     */
    public java.lang.Short getZone() {
        return zone;
    }


    /**
     * Sets the zone value for this SeatInfo.
     * 
     * @param zone
     */
    public void setZone(java.lang.Short zone) {
        this.zone = zone;
    }


    /**
     * Gets the height value for this SeatInfo.
     * 
     * @return height
     */
    public java.lang.Short getHeight() {
        return height;
    }


    /**
     * Sets the height value for this SeatInfo.
     * 
     * @param height
     */
    public void setHeight(java.lang.Short height) {
        this.height = height;
    }


    /**
     * Gets the width value for this SeatInfo.
     * 
     * @return width
     */
    public java.lang.Short getWidth() {
        return width;
    }


    /**
     * Sets the width value for this SeatInfo.
     * 
     * @param width
     */
    public void setWidth(java.lang.Short width) {
        this.width = width;
    }


    /**
     * Gets the priority value for this SeatInfo.
     * 
     * @return priority
     */
    public java.lang.Short getPriority() {
        return priority;
    }


    /**
     * Sets the priority value for this SeatInfo.
     * 
     * @param priority
     */
    public void setPriority(java.lang.Short priority) {
        this.priority = priority;
    }


    /**
     * Gets the text value for this SeatInfo.
     * 
     * @return text
     */
    public java.lang.String getText() {
        return text;
    }


    /**
     * Sets the text value for this SeatInfo.
     * 
     * @param text
     */
    public void setText(java.lang.String text) {
        this.text = text;
    }


    /**
     * Gets the ODPenalty value for this SeatInfo.
     * 
     * @return ODPenalty
     */
    public java.lang.Integer getODPenalty() {
        return ODPenalty;
    }


    /**
     * Sets the ODPenalty value for this SeatInfo.
     * 
     * @param ODPenalty
     */
    public void setODPenalty(java.lang.Integer ODPenalty) {
        this.ODPenalty = ODPenalty;
    }


    /**
     * Gets the terminalDisplayCharacter value for this SeatInfo.
     * 
     * @return terminalDisplayCharacter
     */
    public java.lang.String getTerminalDisplayCharacter() {
        return terminalDisplayCharacter;
    }


    /**
     * Sets the terminalDisplayCharacter value for this SeatInfo.
     * 
     * @param terminalDisplayCharacter
     */
    public void setTerminalDisplayCharacter(java.lang.String terminalDisplayCharacter) {
        this.terminalDisplayCharacter = terminalDisplayCharacter;
    }


    /**
     * Gets the premiumSeatIndicator value for this SeatInfo.
     * 
     * @return premiumSeatIndicator
     */
    public java.lang.Boolean getPremiumSeatIndicator() {
        return premiumSeatIndicator;
    }


    /**
     * Sets the premiumSeatIndicator value for this SeatInfo.
     * 
     * @param premiumSeatIndicator
     */
    public void setPremiumSeatIndicator(java.lang.Boolean premiumSeatIndicator) {
        this.premiumSeatIndicator = premiumSeatIndicator;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SeatInfo)) return false;
        SeatInfo other = (SeatInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.assignable==null && other.getAssignable()==null) || 
             (this.assignable!=null &&
              this.assignable.equals(other.getAssignable()))) &&
            ((this.cabotageLevel==null && other.getCabotageLevel()==null) || 
             (this.cabotageLevel!=null &&
              this.cabotageLevel.equals(other.getCabotageLevel()))) &&
            ((this.carAvailableUnits==null && other.getCarAvailableUnits()==null) || 
             (this.carAvailableUnits!=null &&
              this.carAvailableUnits.equals(other.getCarAvailableUnits()))) &&
            ((this.compartmentDesignator==null && other.getCompartmentDesignator()==null) || 
             (this.compartmentDesignator!=null &&
              this.compartmentDesignator.equals(other.getCompartmentDesignator()))) &&
            ((this.seatSet==null && other.getSeatSet()==null) || 
             (this.seatSet!=null &&
              this.seatSet.equals(other.getSeatSet()))) &&
            ((this.criterionWeight==null && other.getCriterionWeight()==null) || 
             (this.criterionWeight!=null &&
              this.criterionWeight.equals(other.getCriterionWeight()))) &&
            ((this.seatSetAvailableUnits==null && other.getSeatSetAvailableUnits()==null) || 
             (this.seatSetAvailableUnits!=null &&
              this.seatSetAvailableUnits.equals(other.getSeatSetAvailableUnits()))) &&
            ((this.SSRSeatMapCode==null && other.getSSRSeatMapCode()==null) || 
             (this.SSRSeatMapCode!=null &&
              this.SSRSeatMapCode.equals(other.getSSRSeatMapCode()))) &&
            ((this.seatAngle==null && other.getSeatAngle()==null) || 
             (this.seatAngle!=null &&
              this.seatAngle.equals(other.getSeatAngle()))) &&
            ((this.seatAvailability==null && other.getSeatAvailability()==null) || 
             (this.seatAvailability!=null &&
              this.seatAvailability.equals(other.getSeatAvailability()))) &&
            ((this.seatDesignator==null && other.getSeatDesignator()==null) || 
             (this.seatDesignator!=null &&
              this.seatDesignator.equals(other.getSeatDesignator()))) &&
            ((this.seatType==null && other.getSeatType()==null) || 
             (this.seatType!=null &&
              this.seatType.equals(other.getSeatType()))) &&
            ((this.x==null && other.getX()==null) || 
             (this.x!=null &&
              this.x.equals(other.getX()))) &&
            ((this.y==null && other.getY()==null) || 
             (this.y!=null &&
              this.y.equals(other.getY()))) &&
            ((this.propertyList==null && other.getPropertyList()==null) || 
             (this.propertyList!=null &&
              java.util.Arrays.equals(this.propertyList, other.getPropertyList()))) &&
            ((this.SSRPermissions==null && other.getSSRPermissions()==null) || 
             (this.SSRPermissions!=null &&
              java.util.Arrays.equals(this.SSRPermissions, other.getSSRPermissions()))) &&
            ((this.SSRPermissionBits==null && other.getSSRPermissionBits()==null) || 
             (this.SSRPermissionBits!=null &&
              java.util.Arrays.equals(this.SSRPermissionBits, other.getSSRPermissionBits()))) &&
            ((this.propertyBits==null && other.getPropertyBits()==null) || 
             (this.propertyBits!=null &&
              java.util.Arrays.equals(this.propertyBits, other.getPropertyBits()))) &&
            ((this.propertyInts==null && other.getPropertyInts()==null) || 
             (this.propertyInts!=null &&
              java.util.Arrays.equals(this.propertyInts, other.getPropertyInts()))) &&
            ((this.travelClassCode==null && other.getTravelClassCode()==null) || 
             (this.travelClassCode!=null &&
              this.travelClassCode.equals(other.getTravelClassCode()))) &&
            ((this.propertyTimestamp==null && other.getPropertyTimestamp()==null) || 
             (this.propertyTimestamp!=null &&
              this.propertyTimestamp.equals(other.getPropertyTimestamp()))) &&
            ((this.seatGroup==null && other.getSeatGroup()==null) || 
             (this.seatGroup!=null &&
              this.seatGroup.equals(other.getSeatGroup()))) &&
            ((this.zone==null && other.getZone()==null) || 
             (this.zone!=null &&
              this.zone.equals(other.getZone()))) &&
            ((this.height==null && other.getHeight()==null) || 
             (this.height!=null &&
              this.height.equals(other.getHeight()))) &&
            ((this.width==null && other.getWidth()==null) || 
             (this.width!=null &&
              this.width.equals(other.getWidth()))) &&
            ((this.priority==null && other.getPriority()==null) || 
             (this.priority!=null &&
              this.priority.equals(other.getPriority()))) &&
            ((this.text==null && other.getText()==null) || 
             (this.text!=null &&
              this.text.equals(other.getText()))) &&
            ((this.ODPenalty==null && other.getODPenalty()==null) || 
             (this.ODPenalty!=null &&
              this.ODPenalty.equals(other.getODPenalty()))) &&
            ((this.terminalDisplayCharacter==null && other.getTerminalDisplayCharacter()==null) || 
             (this.terminalDisplayCharacter!=null &&
              this.terminalDisplayCharacter.equals(other.getTerminalDisplayCharacter()))) &&
            ((this.premiumSeatIndicator==null && other.getPremiumSeatIndicator()==null) || 
             (this.premiumSeatIndicator!=null &&
              this.premiumSeatIndicator.equals(other.getPremiumSeatIndicator())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAssignable() != null) {
            _hashCode += getAssignable().hashCode();
        }
        if (getCabotageLevel() != null) {
            _hashCode += getCabotageLevel().hashCode();
        }
        if (getCarAvailableUnits() != null) {
            _hashCode += getCarAvailableUnits().hashCode();
        }
        if (getCompartmentDesignator() != null) {
            _hashCode += getCompartmentDesignator().hashCode();
        }
        if (getSeatSet() != null) {
            _hashCode += getSeatSet().hashCode();
        }
        if (getCriterionWeight() != null) {
            _hashCode += getCriterionWeight().hashCode();
        }
        if (getSeatSetAvailableUnits() != null) {
            _hashCode += getSeatSetAvailableUnits().hashCode();
        }
        if (getSSRSeatMapCode() != null) {
            _hashCode += getSSRSeatMapCode().hashCode();
        }
        if (getSeatAngle() != null) {
            _hashCode += getSeatAngle().hashCode();
        }
        if (getSeatAvailability() != null) {
            _hashCode += getSeatAvailability().hashCode();
        }
        if (getSeatDesignator() != null) {
            _hashCode += getSeatDesignator().hashCode();
        }
        if (getSeatType() != null) {
            _hashCode += getSeatType().hashCode();
        }
        if (getX() != null) {
            _hashCode += getX().hashCode();
        }
        if (getY() != null) {
            _hashCode += getY().hashCode();
        }
        if (getPropertyList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSSRPermissions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSSRPermissions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSSRPermissions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSSRPermissionBits() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSSRPermissionBits());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSSRPermissionBits(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyBits() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyBits());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyBits(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPropertyInts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPropertyInts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPropertyInts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTravelClassCode() != null) {
            _hashCode += getTravelClassCode().hashCode();
        }
        if (getPropertyTimestamp() != null) {
            _hashCode += getPropertyTimestamp().hashCode();
        }
        if (getSeatGroup() != null) {
            _hashCode += getSeatGroup().hashCode();
        }
        if (getZone() != null) {
            _hashCode += getZone().hashCode();
        }
        if (getHeight() != null) {
            _hashCode += getHeight().hashCode();
        }
        if (getWidth() != null) {
            _hashCode += getWidth().hashCode();
        }
        if (getPriority() != null) {
            _hashCode += getPriority().hashCode();
        }
        if (getText() != null) {
            _hashCode += getText().hashCode();
        }
        if (getODPenalty() != null) {
            _hashCode += getODPenalty().hashCode();
        }
        if (getTerminalDisplayCharacter() != null) {
            _hashCode += getTerminalDisplayCharacter().hashCode();
        }
        if (getPremiumSeatIndicator() != null) {
            _hashCode += getPremiumSeatIndicator().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SeatInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assignable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Assignable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cabotageLevel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CabotageLevel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carAvailableUnits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CarAvailableUnits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("compartmentDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatSet");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatSet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criterionWeight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CriterionWeight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatSetAvailableUnits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatSetAvailableUnits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRSeatMapCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRSeatMapCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatAngle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAngle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatAvailability");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailability"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SeatAvailability"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatDesignator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatDesignator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("x");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "X"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("y");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Y"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRPermissions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRPermissions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRPermissionBits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRPermissionBits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "unsignedInt"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyBits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyBits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "unsignedInt"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyInts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyInts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("travelClassCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelClassCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyTimestamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PropertyTimestamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seatGroup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatGroup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Zone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("height");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Height"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("width");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Width"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priority");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Priority"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("text");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Text"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ODPenalty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ODPenalty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminalDisplayCharacter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TerminalDisplayCharacter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("premiumSeatIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PremiumSeatIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
