/**
 * OrderHandling.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class OrderHandling  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem handlingCharge;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderDiscount orderDiscount;

    public OrderHandling() {
    }

    public OrderHandling(
           com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem handlingCharge,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderDiscount orderDiscount) {
           this.handlingCharge = handlingCharge;
           this.orderDiscount = orderDiscount;
    }


    /**
     * Gets the handlingCharge value for this OrderHandling.
     * 
     * @return handlingCharge
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem getHandlingCharge() {
        return handlingCharge;
    }


    /**
     * Sets the handlingCharge value for this OrderHandling.
     * 
     * @param handlingCharge
     */
    public void setHandlingCharge(com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem handlingCharge) {
        this.handlingCharge = handlingCharge;
    }


    /**
     * Gets the orderDiscount value for this OrderHandling.
     * 
     * @return orderDiscount
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderDiscount getOrderDiscount() {
        return orderDiscount;
    }


    /**
     * Sets the orderDiscount value for this OrderHandling.
     * 
     * @param orderDiscount
     */
    public void setOrderDiscount(com.navitaire.schemas.WebServices.DataContracts.Common.OrderDiscount orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderHandling)) return false;
        OrderHandling other = (OrderHandling) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.handlingCharge==null && other.getHandlingCharge()==null) || 
             (this.handlingCharge!=null &&
              this.handlingCharge.equals(other.getHandlingCharge()))) &&
            ((this.orderDiscount==null && other.getOrderDiscount()==null) || 
             (this.orderDiscount!=null &&
              this.orderDiscount.equals(other.getOrderDiscount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHandlingCharge() != null) {
            _hashCode += getHandlingCharge().hashCode();
        }
        if (getOrderDiscount() != null) {
            _hashCode += getOrderDiscount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderHandling.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderHandling"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("handlingCharge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "HandlingCharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ChargeableItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderDiscount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderDiscount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderDiscount"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
