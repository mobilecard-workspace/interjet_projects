/**
 * PaxScore.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class PaxScore  implements java.io.Serializable {
    private java.lang.Integer score;

    private java.lang.Short passengerNumber;

    private java.lang.String actionStatusCode;

    private java.lang.String guestValueCode;

    public PaxScore() {
    }

    public PaxScore(
           java.lang.Integer score,
           java.lang.Short passengerNumber,
           java.lang.String actionStatusCode,
           java.lang.String guestValueCode) {
           this.score = score;
           this.passengerNumber = passengerNumber;
           this.actionStatusCode = actionStatusCode;
           this.guestValueCode = guestValueCode;
    }


    /**
     * Gets the score value for this PaxScore.
     * 
     * @return score
     */
    public java.lang.Integer getScore() {
        return score;
    }


    /**
     * Sets the score value for this PaxScore.
     * 
     * @param score
     */
    public void setScore(java.lang.Integer score) {
        this.score = score;
    }


    /**
     * Gets the passengerNumber value for this PaxScore.
     * 
     * @return passengerNumber
     */
    public java.lang.Short getPassengerNumber() {
        return passengerNumber;
    }


    /**
     * Sets the passengerNumber value for this PaxScore.
     * 
     * @param passengerNumber
     */
    public void setPassengerNumber(java.lang.Short passengerNumber) {
        this.passengerNumber = passengerNumber;
    }


    /**
     * Gets the actionStatusCode value for this PaxScore.
     * 
     * @return actionStatusCode
     */
    public java.lang.String getActionStatusCode() {
        return actionStatusCode;
    }


    /**
     * Sets the actionStatusCode value for this PaxScore.
     * 
     * @param actionStatusCode
     */
    public void setActionStatusCode(java.lang.String actionStatusCode) {
        this.actionStatusCode = actionStatusCode;
    }


    /**
     * Gets the guestValueCode value for this PaxScore.
     * 
     * @return guestValueCode
     */
    public java.lang.String getGuestValueCode() {
        return guestValueCode;
    }


    /**
     * Sets the guestValueCode value for this PaxScore.
     * 
     * @param guestValueCode
     */
    public void setGuestValueCode(java.lang.String guestValueCode) {
        this.guestValueCode = guestValueCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaxScore)) return false;
        PaxScore other = (PaxScore) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.score==null && other.getScore()==null) || 
             (this.score!=null &&
              this.score.equals(other.getScore()))) &&
            ((this.passengerNumber==null && other.getPassengerNumber()==null) || 
             (this.passengerNumber!=null &&
              this.passengerNumber.equals(other.getPassengerNumber()))) &&
            ((this.actionStatusCode==null && other.getActionStatusCode()==null) || 
             (this.actionStatusCode!=null &&
              this.actionStatusCode.equals(other.getActionStatusCode()))) &&
            ((this.guestValueCode==null && other.getGuestValueCode()==null) || 
             (this.guestValueCode!=null &&
              this.guestValueCode.equals(other.getGuestValueCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getScore() != null) {
            _hashCode += getScore().hashCode();
        }
        if (getPassengerNumber() != null) {
            _hashCode += getPassengerNumber().hashCode();
        }
        if (getActionStatusCode() != null) {
            _hashCode += getActionStatusCode().hashCode();
        }
        if (getGuestValueCode() != null) {
            _hashCode += getGuestValueCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaxScore.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxScore"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("score");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Score"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ActionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("guestValueCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GuestValueCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
