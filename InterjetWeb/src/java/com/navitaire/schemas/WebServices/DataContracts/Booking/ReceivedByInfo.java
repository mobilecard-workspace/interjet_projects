/**
 * ReceivedByInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class ReceivedByInfo  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String receivedBy;

    private java.lang.String receivedReference;

    private java.lang.String referralCode;

    private java.lang.String latestReceivedBy;

    private java.lang.String latestReceivedReference;

    public ReceivedByInfo() {
    }

    public ReceivedByInfo(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String receivedBy,
           java.lang.String receivedReference,
           java.lang.String referralCode,
           java.lang.String latestReceivedBy,
           java.lang.String latestReceivedReference) {
        super(
            state);
        this.receivedBy = receivedBy;
        this.receivedReference = receivedReference;
        this.referralCode = referralCode;
        this.latestReceivedBy = latestReceivedBy;
        this.latestReceivedReference = latestReceivedReference;
    }


    /**
     * Gets the receivedBy value for this ReceivedByInfo.
     * 
     * @return receivedBy
     */
    public java.lang.String getReceivedBy() {
        return receivedBy;
    }


    /**
     * Sets the receivedBy value for this ReceivedByInfo.
     * 
     * @param receivedBy
     */
    public void setReceivedBy(java.lang.String receivedBy) {
        this.receivedBy = receivedBy;
    }


    /**
     * Gets the receivedReference value for this ReceivedByInfo.
     * 
     * @return receivedReference
     */
    public java.lang.String getReceivedReference() {
        return receivedReference;
    }


    /**
     * Sets the receivedReference value for this ReceivedByInfo.
     * 
     * @param receivedReference
     */
    public void setReceivedReference(java.lang.String receivedReference) {
        this.receivedReference = receivedReference;
    }


    /**
     * Gets the referralCode value for this ReceivedByInfo.
     * 
     * @return referralCode
     */
    public java.lang.String getReferralCode() {
        return referralCode;
    }


    /**
     * Sets the referralCode value for this ReceivedByInfo.
     * 
     * @param referralCode
     */
    public void setReferralCode(java.lang.String referralCode) {
        this.referralCode = referralCode;
    }


    /**
     * Gets the latestReceivedBy value for this ReceivedByInfo.
     * 
     * @return latestReceivedBy
     */
    public java.lang.String getLatestReceivedBy() {
        return latestReceivedBy;
    }


    /**
     * Sets the latestReceivedBy value for this ReceivedByInfo.
     * 
     * @param latestReceivedBy
     */
    public void setLatestReceivedBy(java.lang.String latestReceivedBy) {
        this.latestReceivedBy = latestReceivedBy;
    }


    /**
     * Gets the latestReceivedReference value for this ReceivedByInfo.
     * 
     * @return latestReceivedReference
     */
    public java.lang.String getLatestReceivedReference() {
        return latestReceivedReference;
    }


    /**
     * Sets the latestReceivedReference value for this ReceivedByInfo.
     * 
     * @param latestReceivedReference
     */
    public void setLatestReceivedReference(java.lang.String latestReceivedReference) {
        this.latestReceivedReference = latestReceivedReference;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReceivedByInfo)) return false;
        ReceivedByInfo other = (ReceivedByInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.receivedBy==null && other.getReceivedBy()==null) || 
             (this.receivedBy!=null &&
              this.receivedBy.equals(other.getReceivedBy()))) &&
            ((this.receivedReference==null && other.getReceivedReference()==null) || 
             (this.receivedReference!=null &&
              this.receivedReference.equals(other.getReceivedReference()))) &&
            ((this.referralCode==null && other.getReferralCode()==null) || 
             (this.referralCode!=null &&
              this.referralCode.equals(other.getReferralCode()))) &&
            ((this.latestReceivedBy==null && other.getLatestReceivedBy()==null) || 
             (this.latestReceivedBy!=null &&
              this.latestReceivedBy.equals(other.getLatestReceivedBy()))) &&
            ((this.latestReceivedReference==null && other.getLatestReceivedReference()==null) || 
             (this.latestReceivedReference!=null &&
              this.latestReceivedReference.equals(other.getLatestReceivedReference())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getReceivedBy() != null) {
            _hashCode += getReceivedBy().hashCode();
        }
        if (getReceivedReference() != null) {
            _hashCode += getReceivedReference().hashCode();
        }
        if (getReferralCode() != null) {
            _hashCode += getReferralCode().hashCode();
        }
        if (getLatestReceivedBy() != null) {
            _hashCode += getLatestReceivedBy().hashCode();
        }
        if (getLatestReceivedReference() != null) {
            _hashCode += getLatestReceivedReference().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReceivedByInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedByInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receivedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receivedReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referralCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReferralCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latestReceivedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LatestReceivedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latestReceivedReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LatestReceivedReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
