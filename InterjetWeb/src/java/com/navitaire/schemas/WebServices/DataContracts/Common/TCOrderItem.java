/**
 * TCOrderItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Common;

public class TCOrderItem  extends com.navitaire.schemas.WebServices.DataContracts.Common.TCResponse  implements java.io.Serializable {
    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderPayment payment;

    private java.lang.String CRL;

    private java.lang.String supplierCode;

    private java.lang.String corpDiscountCode;

    private java.lang.Integer ruleSetID;

    private java.lang.String ratingCode;

    private java.lang.String departmentCode;

    private java.lang.Boolean allowsSku;

    private java.lang.Boolean allowsGiftWrap;

    private java.math.BigDecimal basePrice;

    private java.math.BigDecimal preTaxTotalNow;

    private java.math.BigDecimal preTaxTotalLater;

    private java.math.BigDecimal taxTotalLater;

    private java.math.BigDecimal taxTotalNow;

    private java.math.BigDecimal totalLater;

    private java.math.BigDecimal totalNow;

    private java.math.BigDecimal basePriceTotal;

    private java.lang.String descriptionLong;

    private java.math.BigDecimal displayPrice;

    private java.math.BigDecimal displayPriceTotal;

    private java.lang.String thumbFilename;

    private java.math.BigDecimal markupAmount;

    private java.math.BigDecimal markupAmountTotal;

    private java.lang.Boolean newFlag;

    private java.lang.Boolean activeStatus;

    private java.math.BigDecimal feesTotal;

    private java.lang.String field1;

    private java.lang.String field2;

    private java.lang.String field3;

    private java.lang.String field4;

    private java.lang.String field5;

    private java.util.Calendar skuExpectedDate;

    private java.lang.String orderId;

    private java.lang.Integer itemSequence;

    private java.lang.String catalogCode;

    private java.lang.String vendorCode;

    private java.lang.String categoryCode;

    private java.lang.String itemTypeCode;

    private java.lang.String itemId;

    private java.lang.Integer skuId;

    private java.lang.Boolean isDueNow;

    private java.lang.String externalSkuId;

    private java.lang.String externalSkuCatalogId;

    private java.util.Calendar purchaseDate;

    private java.util.Calendar usageDate;

    private java.lang.String itemDescription;

    private java.lang.String skuDescription;

    private java.lang.Boolean giftWrapped;

    private java.lang.String giftWrapMessage;

    private java.lang.String roleCodeSupplierPortal;

    private com.navitaire.schemas.WebServices.DataContracts.Common.TCWarning[] warningList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions[] termsConditionsList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions[] cancellationPolicies;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Participant[] participantList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderCustomer orderCustomer;

    private java.lang.Boolean taxExempt;

    private java.lang.String orderItemStatusCode;

    private java.lang.Boolean taxAtUnitPrice;

    private java.lang.String currencyCode;

    private java.lang.String promoCode;

    private java.lang.String sourceCode;

    private java.lang.String contentType;

    private java.lang.Integer quantity;

    private java.math.BigDecimal listPrice;

    private java.math.BigDecimal listPriceTotal;

    private java.math.BigDecimal discountAmount;

    private java.math.BigDecimal discountAmountTotal;

    private java.math.BigDecimal personalizationPriceTotal;

    private java.math.BigDecimal handlingCharge;

    private java.math.BigDecimal handlingChargeTotal;

    private java.math.BigDecimal handlingDiscount;

    private java.math.BigDecimal handlingDiscountTotal;

    private java.math.BigDecimal discountedHandlingChargeTotal;

    private java.math.BigDecimal discountedListPrice;

    private java.math.BigDecimal discountedListPriceTotal;

    private java.math.BigDecimal taxableTotal;

    private java.lang.Integer taxRate;

    private java.math.BigDecimal servicesTotal;

    private java.math.BigDecimal total;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderHandling orderHandling;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemAddress orderItemAddress;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemPersonalization[] orderItemPersonalizationList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemSkuDetail[] orderItemSkuDetailValueList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemNote[] orderItemNoteList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocator[] orderItemLocatorList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Fee[] orderItemFeeList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocation[] orderItemLocationList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemParameter[] orderItemParameterList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemStatusHistory[] orderItemStatusHistoryList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentOptions paymentOption;

    private int[] orderItemOrderPaymentList;

    private com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemElement[] orderItemElementList;

    private java.lang.String cancellationNumber;

    private java.util.Calendar cancellationDate;

    private java.lang.String comparisonKey;

    public TCOrderItem() {
    }

    public TCOrderItem(
           java.lang.String companyCode,
           java.lang.String XML,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderPayment payment,
           java.lang.String CRL,
           java.lang.String supplierCode,
           java.lang.String corpDiscountCode,
           java.lang.Integer ruleSetID,
           java.lang.String ratingCode,
           java.lang.String departmentCode,
           java.lang.Boolean allowsSku,
           java.lang.Boolean allowsGiftWrap,
           java.math.BigDecimal basePrice,
           java.math.BigDecimal preTaxTotalNow,
           java.math.BigDecimal preTaxTotalLater,
           java.math.BigDecimal taxTotalLater,
           java.math.BigDecimal taxTotalNow,
           java.math.BigDecimal totalLater,
           java.math.BigDecimal totalNow,
           java.math.BigDecimal basePriceTotal,
           java.lang.String descriptionLong,
           java.math.BigDecimal displayPrice,
           java.math.BigDecimal displayPriceTotal,
           java.lang.String thumbFilename,
           java.math.BigDecimal markupAmount,
           java.math.BigDecimal markupAmountTotal,
           java.lang.Boolean newFlag,
           java.lang.Boolean activeStatus,
           java.math.BigDecimal feesTotal,
           java.lang.String field1,
           java.lang.String field2,
           java.lang.String field3,
           java.lang.String field4,
           java.lang.String field5,
           java.util.Calendar skuExpectedDate,
           java.lang.String orderId,
           java.lang.Integer itemSequence,
           java.lang.String catalogCode,
           java.lang.String vendorCode,
           java.lang.String categoryCode,
           java.lang.String itemTypeCode,
           java.lang.String itemId,
           java.lang.Integer skuId,
           java.lang.Boolean isDueNow,
           java.lang.String externalSkuId,
           java.lang.String externalSkuCatalogId,
           java.util.Calendar purchaseDate,
           java.util.Calendar usageDate,
           java.lang.String itemDescription,
           java.lang.String skuDescription,
           java.lang.Boolean giftWrapped,
           java.lang.String giftWrapMessage,
           java.lang.String roleCodeSupplierPortal,
           com.navitaire.schemas.WebServices.DataContracts.Common.TCWarning[] warningList,
           com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions[] termsConditionsList,
           com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions[] cancellationPolicies,
           com.navitaire.schemas.WebServices.DataContracts.Common.Participant[] participantList,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderCustomer orderCustomer,
           java.lang.Boolean taxExempt,
           java.lang.String orderItemStatusCode,
           java.lang.Boolean taxAtUnitPrice,
           java.lang.String currencyCode,
           java.lang.String promoCode,
           java.lang.String sourceCode,
           java.lang.String contentType,
           java.lang.Integer quantity,
           java.math.BigDecimal listPrice,
           java.math.BigDecimal listPriceTotal,
           java.math.BigDecimal discountAmount,
           java.math.BigDecimal discountAmountTotal,
           java.math.BigDecimal personalizationPriceTotal,
           java.math.BigDecimal handlingCharge,
           java.math.BigDecimal handlingChargeTotal,
           java.math.BigDecimal handlingDiscount,
           java.math.BigDecimal handlingDiscountTotal,
           java.math.BigDecimal discountedHandlingChargeTotal,
           java.math.BigDecimal discountedListPrice,
           java.math.BigDecimal discountedListPriceTotal,
           java.math.BigDecimal taxableTotal,
           java.lang.Integer taxRate,
           java.math.BigDecimal servicesTotal,
           java.math.BigDecimal total,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderHandling orderHandling,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemAddress orderItemAddress,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemPersonalization[] orderItemPersonalizationList,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemSkuDetail[] orderItemSkuDetailValueList,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemNote[] orderItemNoteList,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocator[] orderItemLocatorList,
           com.navitaire.schemas.WebServices.DataContracts.Common.Fee[] orderItemFeeList,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocation[] orderItemLocationList,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemParameter[] orderItemParameterList,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemStatusHistory[] orderItemStatusHistoryList,
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentOptions paymentOption,
           int[] orderItemOrderPaymentList,
           com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemElement[] orderItemElementList,
           java.lang.String cancellationNumber,
           java.util.Calendar cancellationDate,
           java.lang.String comparisonKey) {
        super(
            companyCode,
            XML);
        this.payment = payment;
        this.CRL = CRL;
        this.supplierCode = supplierCode;
        this.corpDiscountCode = corpDiscountCode;
        this.ruleSetID = ruleSetID;
        this.ratingCode = ratingCode;
        this.departmentCode = departmentCode;
        this.allowsSku = allowsSku;
        this.allowsGiftWrap = allowsGiftWrap;
        this.basePrice = basePrice;
        this.preTaxTotalNow = preTaxTotalNow;
        this.preTaxTotalLater = preTaxTotalLater;
        this.taxTotalLater = taxTotalLater;
        this.taxTotalNow = taxTotalNow;
        this.totalLater = totalLater;
        this.totalNow = totalNow;
        this.basePriceTotal = basePriceTotal;
        this.descriptionLong = descriptionLong;
        this.displayPrice = displayPrice;
        this.displayPriceTotal = displayPriceTotal;
        this.thumbFilename = thumbFilename;
        this.markupAmount = markupAmount;
        this.markupAmountTotal = markupAmountTotal;
        this.newFlag = newFlag;
        this.activeStatus = activeStatus;
        this.feesTotal = feesTotal;
        this.field1 = field1;
        this.field2 = field2;
        this.field3 = field3;
        this.field4 = field4;
        this.field5 = field5;
        this.skuExpectedDate = skuExpectedDate;
        this.orderId = orderId;
        this.itemSequence = itemSequence;
        this.catalogCode = catalogCode;
        this.vendorCode = vendorCode;
        this.categoryCode = categoryCode;
        this.itemTypeCode = itemTypeCode;
        this.itemId = itemId;
        this.skuId = skuId;
        this.isDueNow = isDueNow;
        this.externalSkuId = externalSkuId;
        this.externalSkuCatalogId = externalSkuCatalogId;
        this.purchaseDate = purchaseDate;
        this.usageDate = usageDate;
        this.itemDescription = itemDescription;
        this.skuDescription = skuDescription;
        this.giftWrapped = giftWrapped;
        this.giftWrapMessage = giftWrapMessage;
        this.roleCodeSupplierPortal = roleCodeSupplierPortal;
        this.warningList = warningList;
        this.termsConditionsList = termsConditionsList;
        this.cancellationPolicies = cancellationPolicies;
        this.participantList = participantList;
        this.orderCustomer = orderCustomer;
        this.taxExempt = taxExempt;
        this.orderItemStatusCode = orderItemStatusCode;
        this.taxAtUnitPrice = taxAtUnitPrice;
        this.currencyCode = currencyCode;
        this.promoCode = promoCode;
        this.sourceCode = sourceCode;
        this.contentType = contentType;
        this.quantity = quantity;
        this.listPrice = listPrice;
        this.listPriceTotal = listPriceTotal;
        this.discountAmount = discountAmount;
        this.discountAmountTotal = discountAmountTotal;
        this.personalizationPriceTotal = personalizationPriceTotal;
        this.handlingCharge = handlingCharge;
        this.handlingChargeTotal = handlingChargeTotal;
        this.handlingDiscount = handlingDiscount;
        this.handlingDiscountTotal = handlingDiscountTotal;
        this.discountedHandlingChargeTotal = discountedHandlingChargeTotal;
        this.discountedListPrice = discountedListPrice;
        this.discountedListPriceTotal = discountedListPriceTotal;
        this.taxableTotal = taxableTotal;
        this.taxRate = taxRate;
        this.servicesTotal = servicesTotal;
        this.total = total;
        this.orderHandling = orderHandling;
        this.orderItemAddress = orderItemAddress;
        this.orderItemPersonalizationList = orderItemPersonalizationList;
        this.orderItemSkuDetailValueList = orderItemSkuDetailValueList;
        this.orderItemNoteList = orderItemNoteList;
        this.orderItemLocatorList = orderItemLocatorList;
        this.orderItemFeeList = orderItemFeeList;
        this.orderItemLocationList = orderItemLocationList;
        this.orderItemParameterList = orderItemParameterList;
        this.orderItemStatusHistoryList = orderItemStatusHistoryList;
        this.paymentOption = paymentOption;
        this.orderItemOrderPaymentList = orderItemOrderPaymentList;
        this.orderItemElementList = orderItemElementList;
        this.cancellationNumber = cancellationNumber;
        this.cancellationDate = cancellationDate;
        this.comparisonKey = comparisonKey;
    }


    /**
     * Gets the payment value for this TCOrderItem.
     * 
     * @return payment
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderPayment getPayment() {
        return payment;
    }


    /**
     * Sets the payment value for this TCOrderItem.
     * 
     * @param payment
     */
    public void setPayment(com.navitaire.schemas.WebServices.DataContracts.Common.OrderPayment payment) {
        this.payment = payment;
    }


    /**
     * Gets the CRL value for this TCOrderItem.
     * 
     * @return CRL
     */
    public java.lang.String getCRL() {
        return CRL;
    }


    /**
     * Sets the CRL value for this TCOrderItem.
     * 
     * @param CRL
     */
    public void setCRL(java.lang.String CRL) {
        this.CRL = CRL;
    }


    /**
     * Gets the supplierCode value for this TCOrderItem.
     * 
     * @return supplierCode
     */
    public java.lang.String getSupplierCode() {
        return supplierCode;
    }


    /**
     * Sets the supplierCode value for this TCOrderItem.
     * 
     * @param supplierCode
     */
    public void setSupplierCode(java.lang.String supplierCode) {
        this.supplierCode = supplierCode;
    }


    /**
     * Gets the corpDiscountCode value for this TCOrderItem.
     * 
     * @return corpDiscountCode
     */
    public java.lang.String getCorpDiscountCode() {
        return corpDiscountCode;
    }


    /**
     * Sets the corpDiscountCode value for this TCOrderItem.
     * 
     * @param corpDiscountCode
     */
    public void setCorpDiscountCode(java.lang.String corpDiscountCode) {
        this.corpDiscountCode = corpDiscountCode;
    }


    /**
     * Gets the ruleSetID value for this TCOrderItem.
     * 
     * @return ruleSetID
     */
    public java.lang.Integer getRuleSetID() {
        return ruleSetID;
    }


    /**
     * Sets the ruleSetID value for this TCOrderItem.
     * 
     * @param ruleSetID
     */
    public void setRuleSetID(java.lang.Integer ruleSetID) {
        this.ruleSetID = ruleSetID;
    }


    /**
     * Gets the ratingCode value for this TCOrderItem.
     * 
     * @return ratingCode
     */
    public java.lang.String getRatingCode() {
        return ratingCode;
    }


    /**
     * Sets the ratingCode value for this TCOrderItem.
     * 
     * @param ratingCode
     */
    public void setRatingCode(java.lang.String ratingCode) {
        this.ratingCode = ratingCode;
    }


    /**
     * Gets the departmentCode value for this TCOrderItem.
     * 
     * @return departmentCode
     */
    public java.lang.String getDepartmentCode() {
        return departmentCode;
    }


    /**
     * Sets the departmentCode value for this TCOrderItem.
     * 
     * @param departmentCode
     */
    public void setDepartmentCode(java.lang.String departmentCode) {
        this.departmentCode = departmentCode;
    }


    /**
     * Gets the allowsSku value for this TCOrderItem.
     * 
     * @return allowsSku
     */
    public java.lang.Boolean getAllowsSku() {
        return allowsSku;
    }


    /**
     * Sets the allowsSku value for this TCOrderItem.
     * 
     * @param allowsSku
     */
    public void setAllowsSku(java.lang.Boolean allowsSku) {
        this.allowsSku = allowsSku;
    }


    /**
     * Gets the allowsGiftWrap value for this TCOrderItem.
     * 
     * @return allowsGiftWrap
     */
    public java.lang.Boolean getAllowsGiftWrap() {
        return allowsGiftWrap;
    }


    /**
     * Sets the allowsGiftWrap value for this TCOrderItem.
     * 
     * @param allowsGiftWrap
     */
    public void setAllowsGiftWrap(java.lang.Boolean allowsGiftWrap) {
        this.allowsGiftWrap = allowsGiftWrap;
    }


    /**
     * Gets the basePrice value for this TCOrderItem.
     * 
     * @return basePrice
     */
    public java.math.BigDecimal getBasePrice() {
        return basePrice;
    }


    /**
     * Sets the basePrice value for this TCOrderItem.
     * 
     * @param basePrice
     */
    public void setBasePrice(java.math.BigDecimal basePrice) {
        this.basePrice = basePrice;
    }


    /**
     * Gets the preTaxTotalNow value for this TCOrderItem.
     * 
     * @return preTaxTotalNow
     */
    public java.math.BigDecimal getPreTaxTotalNow() {
        return preTaxTotalNow;
    }


    /**
     * Sets the preTaxTotalNow value for this TCOrderItem.
     * 
     * @param preTaxTotalNow
     */
    public void setPreTaxTotalNow(java.math.BigDecimal preTaxTotalNow) {
        this.preTaxTotalNow = preTaxTotalNow;
    }


    /**
     * Gets the preTaxTotalLater value for this TCOrderItem.
     * 
     * @return preTaxTotalLater
     */
    public java.math.BigDecimal getPreTaxTotalLater() {
        return preTaxTotalLater;
    }


    /**
     * Sets the preTaxTotalLater value for this TCOrderItem.
     * 
     * @param preTaxTotalLater
     */
    public void setPreTaxTotalLater(java.math.BigDecimal preTaxTotalLater) {
        this.preTaxTotalLater = preTaxTotalLater;
    }


    /**
     * Gets the taxTotalLater value for this TCOrderItem.
     * 
     * @return taxTotalLater
     */
    public java.math.BigDecimal getTaxTotalLater() {
        return taxTotalLater;
    }


    /**
     * Sets the taxTotalLater value for this TCOrderItem.
     * 
     * @param taxTotalLater
     */
    public void setTaxTotalLater(java.math.BigDecimal taxTotalLater) {
        this.taxTotalLater = taxTotalLater;
    }


    /**
     * Gets the taxTotalNow value for this TCOrderItem.
     * 
     * @return taxTotalNow
     */
    public java.math.BigDecimal getTaxTotalNow() {
        return taxTotalNow;
    }


    /**
     * Sets the taxTotalNow value for this TCOrderItem.
     * 
     * @param taxTotalNow
     */
    public void setTaxTotalNow(java.math.BigDecimal taxTotalNow) {
        this.taxTotalNow = taxTotalNow;
    }


    /**
     * Gets the totalLater value for this TCOrderItem.
     * 
     * @return totalLater
     */
    public java.math.BigDecimal getTotalLater() {
        return totalLater;
    }


    /**
     * Sets the totalLater value for this TCOrderItem.
     * 
     * @param totalLater
     */
    public void setTotalLater(java.math.BigDecimal totalLater) {
        this.totalLater = totalLater;
    }


    /**
     * Gets the totalNow value for this TCOrderItem.
     * 
     * @return totalNow
     */
    public java.math.BigDecimal getTotalNow() {
        return totalNow;
    }


    /**
     * Sets the totalNow value for this TCOrderItem.
     * 
     * @param totalNow
     */
    public void setTotalNow(java.math.BigDecimal totalNow) {
        this.totalNow = totalNow;
    }


    /**
     * Gets the basePriceTotal value for this TCOrderItem.
     * 
     * @return basePriceTotal
     */
    public java.math.BigDecimal getBasePriceTotal() {
        return basePriceTotal;
    }


    /**
     * Sets the basePriceTotal value for this TCOrderItem.
     * 
     * @param basePriceTotal
     */
    public void setBasePriceTotal(java.math.BigDecimal basePriceTotal) {
        this.basePriceTotal = basePriceTotal;
    }


    /**
     * Gets the descriptionLong value for this TCOrderItem.
     * 
     * @return descriptionLong
     */
    public java.lang.String getDescriptionLong() {
        return descriptionLong;
    }


    /**
     * Sets the descriptionLong value for this TCOrderItem.
     * 
     * @param descriptionLong
     */
    public void setDescriptionLong(java.lang.String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }


    /**
     * Gets the displayPrice value for this TCOrderItem.
     * 
     * @return displayPrice
     */
    public java.math.BigDecimal getDisplayPrice() {
        return displayPrice;
    }


    /**
     * Sets the displayPrice value for this TCOrderItem.
     * 
     * @param displayPrice
     */
    public void setDisplayPrice(java.math.BigDecimal displayPrice) {
        this.displayPrice = displayPrice;
    }


    /**
     * Gets the displayPriceTotal value for this TCOrderItem.
     * 
     * @return displayPriceTotal
     */
    public java.math.BigDecimal getDisplayPriceTotal() {
        return displayPriceTotal;
    }


    /**
     * Sets the displayPriceTotal value for this TCOrderItem.
     * 
     * @param displayPriceTotal
     */
    public void setDisplayPriceTotal(java.math.BigDecimal displayPriceTotal) {
        this.displayPriceTotal = displayPriceTotal;
    }


    /**
     * Gets the thumbFilename value for this TCOrderItem.
     * 
     * @return thumbFilename
     */
    public java.lang.String getThumbFilename() {
        return thumbFilename;
    }


    /**
     * Sets the thumbFilename value for this TCOrderItem.
     * 
     * @param thumbFilename
     */
    public void setThumbFilename(java.lang.String thumbFilename) {
        this.thumbFilename = thumbFilename;
    }


    /**
     * Gets the markupAmount value for this TCOrderItem.
     * 
     * @return markupAmount
     */
    public java.math.BigDecimal getMarkupAmount() {
        return markupAmount;
    }


    /**
     * Sets the markupAmount value for this TCOrderItem.
     * 
     * @param markupAmount
     */
    public void setMarkupAmount(java.math.BigDecimal markupAmount) {
        this.markupAmount = markupAmount;
    }


    /**
     * Gets the markupAmountTotal value for this TCOrderItem.
     * 
     * @return markupAmountTotal
     */
    public java.math.BigDecimal getMarkupAmountTotal() {
        return markupAmountTotal;
    }


    /**
     * Sets the markupAmountTotal value for this TCOrderItem.
     * 
     * @param markupAmountTotal
     */
    public void setMarkupAmountTotal(java.math.BigDecimal markupAmountTotal) {
        this.markupAmountTotal = markupAmountTotal;
    }


    /**
     * Gets the newFlag value for this TCOrderItem.
     * 
     * @return newFlag
     */
    public java.lang.Boolean getNewFlag() {
        return newFlag;
    }


    /**
     * Sets the newFlag value for this TCOrderItem.
     * 
     * @param newFlag
     */
    public void setNewFlag(java.lang.Boolean newFlag) {
        this.newFlag = newFlag;
    }


    /**
     * Gets the activeStatus value for this TCOrderItem.
     * 
     * @return activeStatus
     */
    public java.lang.Boolean getActiveStatus() {
        return activeStatus;
    }


    /**
     * Sets the activeStatus value for this TCOrderItem.
     * 
     * @param activeStatus
     */
    public void setActiveStatus(java.lang.Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }


    /**
     * Gets the feesTotal value for this TCOrderItem.
     * 
     * @return feesTotal
     */
    public java.math.BigDecimal getFeesTotal() {
        return feesTotal;
    }


    /**
     * Sets the feesTotal value for this TCOrderItem.
     * 
     * @param feesTotal
     */
    public void setFeesTotal(java.math.BigDecimal feesTotal) {
        this.feesTotal = feesTotal;
    }


    /**
     * Gets the field1 value for this TCOrderItem.
     * 
     * @return field1
     */
    public java.lang.String getField1() {
        return field1;
    }


    /**
     * Sets the field1 value for this TCOrderItem.
     * 
     * @param field1
     */
    public void setField1(java.lang.String field1) {
        this.field1 = field1;
    }


    /**
     * Gets the field2 value for this TCOrderItem.
     * 
     * @return field2
     */
    public java.lang.String getField2() {
        return field2;
    }


    /**
     * Sets the field2 value for this TCOrderItem.
     * 
     * @param field2
     */
    public void setField2(java.lang.String field2) {
        this.field2 = field2;
    }


    /**
     * Gets the field3 value for this TCOrderItem.
     * 
     * @return field3
     */
    public java.lang.String getField3() {
        return field3;
    }


    /**
     * Sets the field3 value for this TCOrderItem.
     * 
     * @param field3
     */
    public void setField3(java.lang.String field3) {
        this.field3 = field3;
    }


    /**
     * Gets the field4 value for this TCOrderItem.
     * 
     * @return field4
     */
    public java.lang.String getField4() {
        return field4;
    }


    /**
     * Sets the field4 value for this TCOrderItem.
     * 
     * @param field4
     */
    public void setField4(java.lang.String field4) {
        this.field4 = field4;
    }


    /**
     * Gets the field5 value for this TCOrderItem.
     * 
     * @return field5
     */
    public java.lang.String getField5() {
        return field5;
    }


    /**
     * Sets the field5 value for this TCOrderItem.
     * 
     * @param field5
     */
    public void setField5(java.lang.String field5) {
        this.field5 = field5;
    }


    /**
     * Gets the skuExpectedDate value for this TCOrderItem.
     * 
     * @return skuExpectedDate
     */
    public java.util.Calendar getSkuExpectedDate() {
        return skuExpectedDate;
    }


    /**
     * Sets the skuExpectedDate value for this TCOrderItem.
     * 
     * @param skuExpectedDate
     */
    public void setSkuExpectedDate(java.util.Calendar skuExpectedDate) {
        this.skuExpectedDate = skuExpectedDate;
    }


    /**
     * Gets the orderId value for this TCOrderItem.
     * 
     * @return orderId
     */
    public java.lang.String getOrderId() {
        return orderId;
    }


    /**
     * Sets the orderId value for this TCOrderItem.
     * 
     * @param orderId
     */
    public void setOrderId(java.lang.String orderId) {
        this.orderId = orderId;
    }


    /**
     * Gets the itemSequence value for this TCOrderItem.
     * 
     * @return itemSequence
     */
    public java.lang.Integer getItemSequence() {
        return itemSequence;
    }


    /**
     * Sets the itemSequence value for this TCOrderItem.
     * 
     * @param itemSequence
     */
    public void setItemSequence(java.lang.Integer itemSequence) {
        this.itemSequence = itemSequence;
    }


    /**
     * Gets the catalogCode value for this TCOrderItem.
     * 
     * @return catalogCode
     */
    public java.lang.String getCatalogCode() {
        return catalogCode;
    }


    /**
     * Sets the catalogCode value for this TCOrderItem.
     * 
     * @param catalogCode
     */
    public void setCatalogCode(java.lang.String catalogCode) {
        this.catalogCode = catalogCode;
    }


    /**
     * Gets the vendorCode value for this TCOrderItem.
     * 
     * @return vendorCode
     */
    public java.lang.String getVendorCode() {
        return vendorCode;
    }


    /**
     * Sets the vendorCode value for this TCOrderItem.
     * 
     * @param vendorCode
     */
    public void setVendorCode(java.lang.String vendorCode) {
        this.vendorCode = vendorCode;
    }


    /**
     * Gets the categoryCode value for this TCOrderItem.
     * 
     * @return categoryCode
     */
    public java.lang.String getCategoryCode() {
        return categoryCode;
    }


    /**
     * Sets the categoryCode value for this TCOrderItem.
     * 
     * @param categoryCode
     */
    public void setCategoryCode(java.lang.String categoryCode) {
        this.categoryCode = categoryCode;
    }


    /**
     * Gets the itemTypeCode value for this TCOrderItem.
     * 
     * @return itemTypeCode
     */
    public java.lang.String getItemTypeCode() {
        return itemTypeCode;
    }


    /**
     * Sets the itemTypeCode value for this TCOrderItem.
     * 
     * @param itemTypeCode
     */
    public void setItemTypeCode(java.lang.String itemTypeCode) {
        this.itemTypeCode = itemTypeCode;
    }


    /**
     * Gets the itemId value for this TCOrderItem.
     * 
     * @return itemId
     */
    public java.lang.String getItemId() {
        return itemId;
    }


    /**
     * Sets the itemId value for this TCOrderItem.
     * 
     * @param itemId
     */
    public void setItemId(java.lang.String itemId) {
        this.itemId = itemId;
    }


    /**
     * Gets the skuId value for this TCOrderItem.
     * 
     * @return skuId
     */
    public java.lang.Integer getSkuId() {
        return skuId;
    }


    /**
     * Sets the skuId value for this TCOrderItem.
     * 
     * @param skuId
     */
    public void setSkuId(java.lang.Integer skuId) {
        this.skuId = skuId;
    }


    /**
     * Gets the isDueNow value for this TCOrderItem.
     * 
     * @return isDueNow
     */
    public java.lang.Boolean getIsDueNow() {
        return isDueNow;
    }


    /**
     * Sets the isDueNow value for this TCOrderItem.
     * 
     * @param isDueNow
     */
    public void setIsDueNow(java.lang.Boolean isDueNow) {
        this.isDueNow = isDueNow;
    }


    /**
     * Gets the externalSkuId value for this TCOrderItem.
     * 
     * @return externalSkuId
     */
    public java.lang.String getExternalSkuId() {
        return externalSkuId;
    }


    /**
     * Sets the externalSkuId value for this TCOrderItem.
     * 
     * @param externalSkuId
     */
    public void setExternalSkuId(java.lang.String externalSkuId) {
        this.externalSkuId = externalSkuId;
    }


    /**
     * Gets the externalSkuCatalogId value for this TCOrderItem.
     * 
     * @return externalSkuCatalogId
     */
    public java.lang.String getExternalSkuCatalogId() {
        return externalSkuCatalogId;
    }


    /**
     * Sets the externalSkuCatalogId value for this TCOrderItem.
     * 
     * @param externalSkuCatalogId
     */
    public void setExternalSkuCatalogId(java.lang.String externalSkuCatalogId) {
        this.externalSkuCatalogId = externalSkuCatalogId;
    }


    /**
     * Gets the purchaseDate value for this TCOrderItem.
     * 
     * @return purchaseDate
     */
    public java.util.Calendar getPurchaseDate() {
        return purchaseDate;
    }


    /**
     * Sets the purchaseDate value for this TCOrderItem.
     * 
     * @param purchaseDate
     */
    public void setPurchaseDate(java.util.Calendar purchaseDate) {
        this.purchaseDate = purchaseDate;
    }


    /**
     * Gets the usageDate value for this TCOrderItem.
     * 
     * @return usageDate
     */
    public java.util.Calendar getUsageDate() {
        return usageDate;
    }


    /**
     * Sets the usageDate value for this TCOrderItem.
     * 
     * @param usageDate
     */
    public void setUsageDate(java.util.Calendar usageDate) {
        this.usageDate = usageDate;
    }


    /**
     * Gets the itemDescription value for this TCOrderItem.
     * 
     * @return itemDescription
     */
    public java.lang.String getItemDescription() {
        return itemDescription;
    }


    /**
     * Sets the itemDescription value for this TCOrderItem.
     * 
     * @param itemDescription
     */
    public void setItemDescription(java.lang.String itemDescription) {
        this.itemDescription = itemDescription;
    }


    /**
     * Gets the skuDescription value for this TCOrderItem.
     * 
     * @return skuDescription
     */
    public java.lang.String getSkuDescription() {
        return skuDescription;
    }


    /**
     * Sets the skuDescription value for this TCOrderItem.
     * 
     * @param skuDescription
     */
    public void setSkuDescription(java.lang.String skuDescription) {
        this.skuDescription = skuDescription;
    }


    /**
     * Gets the giftWrapped value for this TCOrderItem.
     * 
     * @return giftWrapped
     */
    public java.lang.Boolean getGiftWrapped() {
        return giftWrapped;
    }


    /**
     * Sets the giftWrapped value for this TCOrderItem.
     * 
     * @param giftWrapped
     */
    public void setGiftWrapped(java.lang.Boolean giftWrapped) {
        this.giftWrapped = giftWrapped;
    }


    /**
     * Gets the giftWrapMessage value for this TCOrderItem.
     * 
     * @return giftWrapMessage
     */
    public java.lang.String getGiftWrapMessage() {
        return giftWrapMessage;
    }


    /**
     * Sets the giftWrapMessage value for this TCOrderItem.
     * 
     * @param giftWrapMessage
     */
    public void setGiftWrapMessage(java.lang.String giftWrapMessage) {
        this.giftWrapMessage = giftWrapMessage;
    }


    /**
     * Gets the roleCodeSupplierPortal value for this TCOrderItem.
     * 
     * @return roleCodeSupplierPortal
     */
    public java.lang.String getRoleCodeSupplierPortal() {
        return roleCodeSupplierPortal;
    }


    /**
     * Sets the roleCodeSupplierPortal value for this TCOrderItem.
     * 
     * @param roleCodeSupplierPortal
     */
    public void setRoleCodeSupplierPortal(java.lang.String roleCodeSupplierPortal) {
        this.roleCodeSupplierPortal = roleCodeSupplierPortal;
    }


    /**
     * Gets the warningList value for this TCOrderItem.
     * 
     * @return warningList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.TCWarning[] getWarningList() {
        return warningList;
    }


    /**
     * Sets the warningList value for this TCOrderItem.
     * 
     * @param warningList
     */
    public void setWarningList(com.navitaire.schemas.WebServices.DataContracts.Common.TCWarning[] warningList) {
        this.warningList = warningList;
    }


    /**
     * Gets the termsConditionsList value for this TCOrderItem.
     * 
     * @return termsConditionsList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions[] getTermsConditionsList() {
        return termsConditionsList;
    }


    /**
     * Sets the termsConditionsList value for this TCOrderItem.
     * 
     * @param termsConditionsList
     */
    public void setTermsConditionsList(com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions[] termsConditionsList) {
        this.termsConditionsList = termsConditionsList;
    }


    /**
     * Gets the cancellationPolicies value for this TCOrderItem.
     * 
     * @return cancellationPolicies
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions[] getCancellationPolicies() {
        return cancellationPolicies;
    }


    /**
     * Sets the cancellationPolicies value for this TCOrderItem.
     * 
     * @param cancellationPolicies
     */
    public void setCancellationPolicies(com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions[] cancellationPolicies) {
        this.cancellationPolicies = cancellationPolicies;
    }


    /**
     * Gets the participantList value for this TCOrderItem.
     * 
     * @return participantList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Participant[] getParticipantList() {
        return participantList;
    }


    /**
     * Sets the participantList value for this TCOrderItem.
     * 
     * @param participantList
     */
    public void setParticipantList(com.navitaire.schemas.WebServices.DataContracts.Common.Participant[] participantList) {
        this.participantList = participantList;
    }


    /**
     * Gets the orderCustomer value for this TCOrderItem.
     * 
     * @return orderCustomer
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderCustomer getOrderCustomer() {
        return orderCustomer;
    }


    /**
     * Sets the orderCustomer value for this TCOrderItem.
     * 
     * @param orderCustomer
     */
    public void setOrderCustomer(com.navitaire.schemas.WebServices.DataContracts.Common.OrderCustomer orderCustomer) {
        this.orderCustomer = orderCustomer;
    }


    /**
     * Gets the taxExempt value for this TCOrderItem.
     * 
     * @return taxExempt
     */
    public java.lang.Boolean getTaxExempt() {
        return taxExempt;
    }


    /**
     * Sets the taxExempt value for this TCOrderItem.
     * 
     * @param taxExempt
     */
    public void setTaxExempt(java.lang.Boolean taxExempt) {
        this.taxExempt = taxExempt;
    }


    /**
     * Gets the orderItemStatusCode value for this TCOrderItem.
     * 
     * @return orderItemStatusCode
     */
    public java.lang.String getOrderItemStatusCode() {
        return orderItemStatusCode;
    }


    /**
     * Sets the orderItemStatusCode value for this TCOrderItem.
     * 
     * @param orderItemStatusCode
     */
    public void setOrderItemStatusCode(java.lang.String orderItemStatusCode) {
        this.orderItemStatusCode = orderItemStatusCode;
    }


    /**
     * Gets the taxAtUnitPrice value for this TCOrderItem.
     * 
     * @return taxAtUnitPrice
     */
    public java.lang.Boolean getTaxAtUnitPrice() {
        return taxAtUnitPrice;
    }


    /**
     * Sets the taxAtUnitPrice value for this TCOrderItem.
     * 
     * @param taxAtUnitPrice
     */
    public void setTaxAtUnitPrice(java.lang.Boolean taxAtUnitPrice) {
        this.taxAtUnitPrice = taxAtUnitPrice;
    }


    /**
     * Gets the currencyCode value for this TCOrderItem.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this TCOrderItem.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the promoCode value for this TCOrderItem.
     * 
     * @return promoCode
     */
    public java.lang.String getPromoCode() {
        return promoCode;
    }


    /**
     * Sets the promoCode value for this TCOrderItem.
     * 
     * @param promoCode
     */
    public void setPromoCode(java.lang.String promoCode) {
        this.promoCode = promoCode;
    }


    /**
     * Gets the sourceCode value for this TCOrderItem.
     * 
     * @return sourceCode
     */
    public java.lang.String getSourceCode() {
        return sourceCode;
    }


    /**
     * Sets the sourceCode value for this TCOrderItem.
     * 
     * @param sourceCode
     */
    public void setSourceCode(java.lang.String sourceCode) {
        this.sourceCode = sourceCode;
    }


    /**
     * Gets the contentType value for this TCOrderItem.
     * 
     * @return contentType
     */
    public java.lang.String getContentType() {
        return contentType;
    }


    /**
     * Sets the contentType value for this TCOrderItem.
     * 
     * @param contentType
     */
    public void setContentType(java.lang.String contentType) {
        this.contentType = contentType;
    }


    /**
     * Gets the quantity value for this TCOrderItem.
     * 
     * @return quantity
     */
    public java.lang.Integer getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this TCOrderItem.
     * 
     * @param quantity
     */
    public void setQuantity(java.lang.Integer quantity) {
        this.quantity = quantity;
    }


    /**
     * Gets the listPrice value for this TCOrderItem.
     * 
     * @return listPrice
     */
    public java.math.BigDecimal getListPrice() {
        return listPrice;
    }


    /**
     * Sets the listPrice value for this TCOrderItem.
     * 
     * @param listPrice
     */
    public void setListPrice(java.math.BigDecimal listPrice) {
        this.listPrice = listPrice;
    }


    /**
     * Gets the listPriceTotal value for this TCOrderItem.
     * 
     * @return listPriceTotal
     */
    public java.math.BigDecimal getListPriceTotal() {
        return listPriceTotal;
    }


    /**
     * Sets the listPriceTotal value for this TCOrderItem.
     * 
     * @param listPriceTotal
     */
    public void setListPriceTotal(java.math.BigDecimal listPriceTotal) {
        this.listPriceTotal = listPriceTotal;
    }


    /**
     * Gets the discountAmount value for this TCOrderItem.
     * 
     * @return discountAmount
     */
    public java.math.BigDecimal getDiscountAmount() {
        return discountAmount;
    }


    /**
     * Sets the discountAmount value for this TCOrderItem.
     * 
     * @param discountAmount
     */
    public void setDiscountAmount(java.math.BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }


    /**
     * Gets the discountAmountTotal value for this TCOrderItem.
     * 
     * @return discountAmountTotal
     */
    public java.math.BigDecimal getDiscountAmountTotal() {
        return discountAmountTotal;
    }


    /**
     * Sets the discountAmountTotal value for this TCOrderItem.
     * 
     * @param discountAmountTotal
     */
    public void setDiscountAmountTotal(java.math.BigDecimal discountAmountTotal) {
        this.discountAmountTotal = discountAmountTotal;
    }


    /**
     * Gets the personalizationPriceTotal value for this TCOrderItem.
     * 
     * @return personalizationPriceTotal
     */
    public java.math.BigDecimal getPersonalizationPriceTotal() {
        return personalizationPriceTotal;
    }


    /**
     * Sets the personalizationPriceTotal value for this TCOrderItem.
     * 
     * @param personalizationPriceTotal
     */
    public void setPersonalizationPriceTotal(java.math.BigDecimal personalizationPriceTotal) {
        this.personalizationPriceTotal = personalizationPriceTotal;
    }


    /**
     * Gets the handlingCharge value for this TCOrderItem.
     * 
     * @return handlingCharge
     */
    public java.math.BigDecimal getHandlingCharge() {
        return handlingCharge;
    }


    /**
     * Sets the handlingCharge value for this TCOrderItem.
     * 
     * @param handlingCharge
     */
    public void setHandlingCharge(java.math.BigDecimal handlingCharge) {
        this.handlingCharge = handlingCharge;
    }


    /**
     * Gets the handlingChargeTotal value for this TCOrderItem.
     * 
     * @return handlingChargeTotal
     */
    public java.math.BigDecimal getHandlingChargeTotal() {
        return handlingChargeTotal;
    }


    /**
     * Sets the handlingChargeTotal value for this TCOrderItem.
     * 
     * @param handlingChargeTotal
     */
    public void setHandlingChargeTotal(java.math.BigDecimal handlingChargeTotal) {
        this.handlingChargeTotal = handlingChargeTotal;
    }


    /**
     * Gets the handlingDiscount value for this TCOrderItem.
     * 
     * @return handlingDiscount
     */
    public java.math.BigDecimal getHandlingDiscount() {
        return handlingDiscount;
    }


    /**
     * Sets the handlingDiscount value for this TCOrderItem.
     * 
     * @param handlingDiscount
     */
    public void setHandlingDiscount(java.math.BigDecimal handlingDiscount) {
        this.handlingDiscount = handlingDiscount;
    }


    /**
     * Gets the handlingDiscountTotal value for this TCOrderItem.
     * 
     * @return handlingDiscountTotal
     */
    public java.math.BigDecimal getHandlingDiscountTotal() {
        return handlingDiscountTotal;
    }


    /**
     * Sets the handlingDiscountTotal value for this TCOrderItem.
     * 
     * @param handlingDiscountTotal
     */
    public void setHandlingDiscountTotal(java.math.BigDecimal handlingDiscountTotal) {
        this.handlingDiscountTotal = handlingDiscountTotal;
    }


    /**
     * Gets the discountedHandlingChargeTotal value for this TCOrderItem.
     * 
     * @return discountedHandlingChargeTotal
     */
    public java.math.BigDecimal getDiscountedHandlingChargeTotal() {
        return discountedHandlingChargeTotal;
    }


    /**
     * Sets the discountedHandlingChargeTotal value for this TCOrderItem.
     * 
     * @param discountedHandlingChargeTotal
     */
    public void setDiscountedHandlingChargeTotal(java.math.BigDecimal discountedHandlingChargeTotal) {
        this.discountedHandlingChargeTotal = discountedHandlingChargeTotal;
    }


    /**
     * Gets the discountedListPrice value for this TCOrderItem.
     * 
     * @return discountedListPrice
     */
    public java.math.BigDecimal getDiscountedListPrice() {
        return discountedListPrice;
    }


    /**
     * Sets the discountedListPrice value for this TCOrderItem.
     * 
     * @param discountedListPrice
     */
    public void setDiscountedListPrice(java.math.BigDecimal discountedListPrice) {
        this.discountedListPrice = discountedListPrice;
    }


    /**
     * Gets the discountedListPriceTotal value for this TCOrderItem.
     * 
     * @return discountedListPriceTotal
     */
    public java.math.BigDecimal getDiscountedListPriceTotal() {
        return discountedListPriceTotal;
    }


    /**
     * Sets the discountedListPriceTotal value for this TCOrderItem.
     * 
     * @param discountedListPriceTotal
     */
    public void setDiscountedListPriceTotal(java.math.BigDecimal discountedListPriceTotal) {
        this.discountedListPriceTotal = discountedListPriceTotal;
    }


    /**
     * Gets the taxableTotal value for this TCOrderItem.
     * 
     * @return taxableTotal
     */
    public java.math.BigDecimal getTaxableTotal() {
        return taxableTotal;
    }


    /**
     * Sets the taxableTotal value for this TCOrderItem.
     * 
     * @param taxableTotal
     */
    public void setTaxableTotal(java.math.BigDecimal taxableTotal) {
        this.taxableTotal = taxableTotal;
    }


    /**
     * Gets the taxRate value for this TCOrderItem.
     * 
     * @return taxRate
     */
    public java.lang.Integer getTaxRate() {
        return taxRate;
    }


    /**
     * Sets the taxRate value for this TCOrderItem.
     * 
     * @param taxRate
     */
    public void setTaxRate(java.lang.Integer taxRate) {
        this.taxRate = taxRate;
    }


    /**
     * Gets the servicesTotal value for this TCOrderItem.
     * 
     * @return servicesTotal
     */
    public java.math.BigDecimal getServicesTotal() {
        return servicesTotal;
    }


    /**
     * Sets the servicesTotal value for this TCOrderItem.
     * 
     * @param servicesTotal
     */
    public void setServicesTotal(java.math.BigDecimal servicesTotal) {
        this.servicesTotal = servicesTotal;
    }


    /**
     * Gets the total value for this TCOrderItem.
     * 
     * @return total
     */
    public java.math.BigDecimal getTotal() {
        return total;
    }


    /**
     * Sets the total value for this TCOrderItem.
     * 
     * @param total
     */
    public void setTotal(java.math.BigDecimal total) {
        this.total = total;
    }


    /**
     * Gets the orderHandling value for this TCOrderItem.
     * 
     * @return orderHandling
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderHandling getOrderHandling() {
        return orderHandling;
    }


    /**
     * Sets the orderHandling value for this TCOrderItem.
     * 
     * @param orderHandling
     */
    public void setOrderHandling(com.navitaire.schemas.WebServices.DataContracts.Common.OrderHandling orderHandling) {
        this.orderHandling = orderHandling;
    }


    /**
     * Gets the orderItemAddress value for this TCOrderItem.
     * 
     * @return orderItemAddress
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemAddress getOrderItemAddress() {
        return orderItemAddress;
    }


    /**
     * Sets the orderItemAddress value for this TCOrderItem.
     * 
     * @param orderItemAddress
     */
    public void setOrderItemAddress(com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemAddress orderItemAddress) {
        this.orderItemAddress = orderItemAddress;
    }


    /**
     * Gets the orderItemPersonalizationList value for this TCOrderItem.
     * 
     * @return orderItemPersonalizationList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemPersonalization[] getOrderItemPersonalizationList() {
        return orderItemPersonalizationList;
    }


    /**
     * Sets the orderItemPersonalizationList value for this TCOrderItem.
     * 
     * @param orderItemPersonalizationList
     */
    public void setOrderItemPersonalizationList(com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemPersonalization[] orderItemPersonalizationList) {
        this.orderItemPersonalizationList = orderItemPersonalizationList;
    }


    /**
     * Gets the orderItemSkuDetailValueList value for this TCOrderItem.
     * 
     * @return orderItemSkuDetailValueList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemSkuDetail[] getOrderItemSkuDetailValueList() {
        return orderItemSkuDetailValueList;
    }


    /**
     * Sets the orderItemSkuDetailValueList value for this TCOrderItem.
     * 
     * @param orderItemSkuDetailValueList
     */
    public void setOrderItemSkuDetailValueList(com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemSkuDetail[] orderItemSkuDetailValueList) {
        this.orderItemSkuDetailValueList = orderItemSkuDetailValueList;
    }


    /**
     * Gets the orderItemNoteList value for this TCOrderItem.
     * 
     * @return orderItemNoteList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemNote[] getOrderItemNoteList() {
        return orderItemNoteList;
    }


    /**
     * Sets the orderItemNoteList value for this TCOrderItem.
     * 
     * @param orderItemNoteList
     */
    public void setOrderItemNoteList(com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemNote[] orderItemNoteList) {
        this.orderItemNoteList = orderItemNoteList;
    }


    /**
     * Gets the orderItemLocatorList value for this TCOrderItem.
     * 
     * @return orderItemLocatorList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocator[] getOrderItemLocatorList() {
        return orderItemLocatorList;
    }


    /**
     * Sets the orderItemLocatorList value for this TCOrderItem.
     * 
     * @param orderItemLocatorList
     */
    public void setOrderItemLocatorList(com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocator[] orderItemLocatorList) {
        this.orderItemLocatorList = orderItemLocatorList;
    }


    /**
     * Gets the orderItemFeeList value for this TCOrderItem.
     * 
     * @return orderItemFeeList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Fee[] getOrderItemFeeList() {
        return orderItemFeeList;
    }


    /**
     * Sets the orderItemFeeList value for this TCOrderItem.
     * 
     * @param orderItemFeeList
     */
    public void setOrderItemFeeList(com.navitaire.schemas.WebServices.DataContracts.Common.Fee[] orderItemFeeList) {
        this.orderItemFeeList = orderItemFeeList;
    }


    /**
     * Gets the orderItemLocationList value for this TCOrderItem.
     * 
     * @return orderItemLocationList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocation[] getOrderItemLocationList() {
        return orderItemLocationList;
    }


    /**
     * Sets the orderItemLocationList value for this TCOrderItem.
     * 
     * @param orderItemLocationList
     */
    public void setOrderItemLocationList(com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocation[] orderItemLocationList) {
        this.orderItemLocationList = orderItemLocationList;
    }


    /**
     * Gets the orderItemParameterList value for this TCOrderItem.
     * 
     * @return orderItemParameterList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemParameter[] getOrderItemParameterList() {
        return orderItemParameterList;
    }


    /**
     * Sets the orderItemParameterList value for this TCOrderItem.
     * 
     * @param orderItemParameterList
     */
    public void setOrderItemParameterList(com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemParameter[] orderItemParameterList) {
        this.orderItemParameterList = orderItemParameterList;
    }


    /**
     * Gets the orderItemStatusHistoryList value for this TCOrderItem.
     * 
     * @return orderItemStatusHistoryList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemStatusHistory[] getOrderItemStatusHistoryList() {
        return orderItemStatusHistoryList;
    }


    /**
     * Sets the orderItemStatusHistoryList value for this TCOrderItem.
     * 
     * @param orderItemStatusHistoryList
     */
    public void setOrderItemStatusHistoryList(com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemStatusHistory[] orderItemStatusHistoryList) {
        this.orderItemStatusHistoryList = orderItemStatusHistoryList;
    }


    /**
     * Gets the paymentOption value for this TCOrderItem.
     * 
     * @return paymentOption
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentOptions getPaymentOption() {
        return paymentOption;
    }


    /**
     * Sets the paymentOption value for this TCOrderItem.
     * 
     * @param paymentOption
     */
    public void setPaymentOption(com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentOptions paymentOption) {
        this.paymentOption = paymentOption;
    }


    /**
     * Gets the orderItemOrderPaymentList value for this TCOrderItem.
     * 
     * @return orderItemOrderPaymentList
     */
    public int[] getOrderItemOrderPaymentList() {
        return orderItemOrderPaymentList;
    }


    /**
     * Sets the orderItemOrderPaymentList value for this TCOrderItem.
     * 
     * @param orderItemOrderPaymentList
     */
    public void setOrderItemOrderPaymentList(int[] orderItemOrderPaymentList) {
        this.orderItemOrderPaymentList = orderItemOrderPaymentList;
    }


    /**
     * Gets the orderItemElementList value for this TCOrderItem.
     * 
     * @return orderItemElementList
     */
    public com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemElement[] getOrderItemElementList() {
        return orderItemElementList;
    }


    /**
     * Sets the orderItemElementList value for this TCOrderItem.
     * 
     * @param orderItemElementList
     */
    public void setOrderItemElementList(com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemElement[] orderItemElementList) {
        this.orderItemElementList = orderItemElementList;
    }


    /**
     * Gets the cancellationNumber value for this TCOrderItem.
     * 
     * @return cancellationNumber
     */
    public java.lang.String getCancellationNumber() {
        return cancellationNumber;
    }


    /**
     * Sets the cancellationNumber value for this TCOrderItem.
     * 
     * @param cancellationNumber
     */
    public void setCancellationNumber(java.lang.String cancellationNumber) {
        this.cancellationNumber = cancellationNumber;
    }


    /**
     * Gets the cancellationDate value for this TCOrderItem.
     * 
     * @return cancellationDate
     */
    public java.util.Calendar getCancellationDate() {
        return cancellationDate;
    }


    /**
     * Sets the cancellationDate value for this TCOrderItem.
     * 
     * @param cancellationDate
     */
    public void setCancellationDate(java.util.Calendar cancellationDate) {
        this.cancellationDate = cancellationDate;
    }


    /**
     * Gets the comparisonKey value for this TCOrderItem.
     * 
     * @return comparisonKey
     */
    public java.lang.String getComparisonKey() {
        return comparisonKey;
    }


    /**
     * Sets the comparisonKey value for this TCOrderItem.
     * 
     * @param comparisonKey
     */
    public void setComparisonKey(java.lang.String comparisonKey) {
        this.comparisonKey = comparisonKey;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TCOrderItem)) return false;
        TCOrderItem other = (TCOrderItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.payment==null && other.getPayment()==null) || 
             (this.payment!=null &&
              this.payment.equals(other.getPayment()))) &&
            ((this.CRL==null && other.getCRL()==null) || 
             (this.CRL!=null &&
              this.CRL.equals(other.getCRL()))) &&
            ((this.supplierCode==null && other.getSupplierCode()==null) || 
             (this.supplierCode!=null &&
              this.supplierCode.equals(other.getSupplierCode()))) &&
            ((this.corpDiscountCode==null && other.getCorpDiscountCode()==null) || 
             (this.corpDiscountCode!=null &&
              this.corpDiscountCode.equals(other.getCorpDiscountCode()))) &&
            ((this.ruleSetID==null && other.getRuleSetID()==null) || 
             (this.ruleSetID!=null &&
              this.ruleSetID.equals(other.getRuleSetID()))) &&
            ((this.ratingCode==null && other.getRatingCode()==null) || 
             (this.ratingCode!=null &&
              this.ratingCode.equals(other.getRatingCode()))) &&
            ((this.departmentCode==null && other.getDepartmentCode()==null) || 
             (this.departmentCode!=null &&
              this.departmentCode.equals(other.getDepartmentCode()))) &&
            ((this.allowsSku==null && other.getAllowsSku()==null) || 
             (this.allowsSku!=null &&
              this.allowsSku.equals(other.getAllowsSku()))) &&
            ((this.allowsGiftWrap==null && other.getAllowsGiftWrap()==null) || 
             (this.allowsGiftWrap!=null &&
              this.allowsGiftWrap.equals(other.getAllowsGiftWrap()))) &&
            ((this.basePrice==null && other.getBasePrice()==null) || 
             (this.basePrice!=null &&
              this.basePrice.equals(other.getBasePrice()))) &&
            ((this.preTaxTotalNow==null && other.getPreTaxTotalNow()==null) || 
             (this.preTaxTotalNow!=null &&
              this.preTaxTotalNow.equals(other.getPreTaxTotalNow()))) &&
            ((this.preTaxTotalLater==null && other.getPreTaxTotalLater()==null) || 
             (this.preTaxTotalLater!=null &&
              this.preTaxTotalLater.equals(other.getPreTaxTotalLater()))) &&
            ((this.taxTotalLater==null && other.getTaxTotalLater()==null) || 
             (this.taxTotalLater!=null &&
              this.taxTotalLater.equals(other.getTaxTotalLater()))) &&
            ((this.taxTotalNow==null && other.getTaxTotalNow()==null) || 
             (this.taxTotalNow!=null &&
              this.taxTotalNow.equals(other.getTaxTotalNow()))) &&
            ((this.totalLater==null && other.getTotalLater()==null) || 
             (this.totalLater!=null &&
              this.totalLater.equals(other.getTotalLater()))) &&
            ((this.totalNow==null && other.getTotalNow()==null) || 
             (this.totalNow!=null &&
              this.totalNow.equals(other.getTotalNow()))) &&
            ((this.basePriceTotal==null && other.getBasePriceTotal()==null) || 
             (this.basePriceTotal!=null &&
              this.basePriceTotal.equals(other.getBasePriceTotal()))) &&
            ((this.descriptionLong==null && other.getDescriptionLong()==null) || 
             (this.descriptionLong!=null &&
              this.descriptionLong.equals(other.getDescriptionLong()))) &&
            ((this.displayPrice==null && other.getDisplayPrice()==null) || 
             (this.displayPrice!=null &&
              this.displayPrice.equals(other.getDisplayPrice()))) &&
            ((this.displayPriceTotal==null && other.getDisplayPriceTotal()==null) || 
             (this.displayPriceTotal!=null &&
              this.displayPriceTotal.equals(other.getDisplayPriceTotal()))) &&
            ((this.thumbFilename==null && other.getThumbFilename()==null) || 
             (this.thumbFilename!=null &&
              this.thumbFilename.equals(other.getThumbFilename()))) &&
            ((this.markupAmount==null && other.getMarkupAmount()==null) || 
             (this.markupAmount!=null &&
              this.markupAmount.equals(other.getMarkupAmount()))) &&
            ((this.markupAmountTotal==null && other.getMarkupAmountTotal()==null) || 
             (this.markupAmountTotal!=null &&
              this.markupAmountTotal.equals(other.getMarkupAmountTotal()))) &&
            ((this.newFlag==null && other.getNewFlag()==null) || 
             (this.newFlag!=null &&
              this.newFlag.equals(other.getNewFlag()))) &&
            ((this.activeStatus==null && other.getActiveStatus()==null) || 
             (this.activeStatus!=null &&
              this.activeStatus.equals(other.getActiveStatus()))) &&
            ((this.feesTotal==null && other.getFeesTotal()==null) || 
             (this.feesTotal!=null &&
              this.feesTotal.equals(other.getFeesTotal()))) &&
            ((this.field1==null && other.getField1()==null) || 
             (this.field1!=null &&
              this.field1.equals(other.getField1()))) &&
            ((this.field2==null && other.getField2()==null) || 
             (this.field2!=null &&
              this.field2.equals(other.getField2()))) &&
            ((this.field3==null && other.getField3()==null) || 
             (this.field3!=null &&
              this.field3.equals(other.getField3()))) &&
            ((this.field4==null && other.getField4()==null) || 
             (this.field4!=null &&
              this.field4.equals(other.getField4()))) &&
            ((this.field5==null && other.getField5()==null) || 
             (this.field5!=null &&
              this.field5.equals(other.getField5()))) &&
            ((this.skuExpectedDate==null && other.getSkuExpectedDate()==null) || 
             (this.skuExpectedDate!=null &&
              this.skuExpectedDate.equals(other.getSkuExpectedDate()))) &&
            ((this.orderId==null && other.getOrderId()==null) || 
             (this.orderId!=null &&
              this.orderId.equals(other.getOrderId()))) &&
            ((this.itemSequence==null && other.getItemSequence()==null) || 
             (this.itemSequence!=null &&
              this.itemSequence.equals(other.getItemSequence()))) &&
            ((this.catalogCode==null && other.getCatalogCode()==null) || 
             (this.catalogCode!=null &&
              this.catalogCode.equals(other.getCatalogCode()))) &&
            ((this.vendorCode==null && other.getVendorCode()==null) || 
             (this.vendorCode!=null &&
              this.vendorCode.equals(other.getVendorCode()))) &&
            ((this.categoryCode==null && other.getCategoryCode()==null) || 
             (this.categoryCode!=null &&
              this.categoryCode.equals(other.getCategoryCode()))) &&
            ((this.itemTypeCode==null && other.getItemTypeCode()==null) || 
             (this.itemTypeCode!=null &&
              this.itemTypeCode.equals(other.getItemTypeCode()))) &&
            ((this.itemId==null && other.getItemId()==null) || 
             (this.itemId!=null &&
              this.itemId.equals(other.getItemId()))) &&
            ((this.skuId==null && other.getSkuId()==null) || 
             (this.skuId!=null &&
              this.skuId.equals(other.getSkuId()))) &&
            ((this.isDueNow==null && other.getIsDueNow()==null) || 
             (this.isDueNow!=null &&
              this.isDueNow.equals(other.getIsDueNow()))) &&
            ((this.externalSkuId==null && other.getExternalSkuId()==null) || 
             (this.externalSkuId!=null &&
              this.externalSkuId.equals(other.getExternalSkuId()))) &&
            ((this.externalSkuCatalogId==null && other.getExternalSkuCatalogId()==null) || 
             (this.externalSkuCatalogId!=null &&
              this.externalSkuCatalogId.equals(other.getExternalSkuCatalogId()))) &&
            ((this.purchaseDate==null && other.getPurchaseDate()==null) || 
             (this.purchaseDate!=null &&
              this.purchaseDate.equals(other.getPurchaseDate()))) &&
            ((this.usageDate==null && other.getUsageDate()==null) || 
             (this.usageDate!=null &&
              this.usageDate.equals(other.getUsageDate()))) &&
            ((this.itemDescription==null && other.getItemDescription()==null) || 
             (this.itemDescription!=null &&
              this.itemDescription.equals(other.getItemDescription()))) &&
            ((this.skuDescription==null && other.getSkuDescription()==null) || 
             (this.skuDescription!=null &&
              this.skuDescription.equals(other.getSkuDescription()))) &&
            ((this.giftWrapped==null && other.getGiftWrapped()==null) || 
             (this.giftWrapped!=null &&
              this.giftWrapped.equals(other.getGiftWrapped()))) &&
            ((this.giftWrapMessage==null && other.getGiftWrapMessage()==null) || 
             (this.giftWrapMessage!=null &&
              this.giftWrapMessage.equals(other.getGiftWrapMessage()))) &&
            ((this.roleCodeSupplierPortal==null && other.getRoleCodeSupplierPortal()==null) || 
             (this.roleCodeSupplierPortal!=null &&
              this.roleCodeSupplierPortal.equals(other.getRoleCodeSupplierPortal()))) &&
            ((this.warningList==null && other.getWarningList()==null) || 
             (this.warningList!=null &&
              java.util.Arrays.equals(this.warningList, other.getWarningList()))) &&
            ((this.termsConditionsList==null && other.getTermsConditionsList()==null) || 
             (this.termsConditionsList!=null &&
              java.util.Arrays.equals(this.termsConditionsList, other.getTermsConditionsList()))) &&
            ((this.cancellationPolicies==null && other.getCancellationPolicies()==null) || 
             (this.cancellationPolicies!=null &&
              java.util.Arrays.equals(this.cancellationPolicies, other.getCancellationPolicies()))) &&
            ((this.participantList==null && other.getParticipantList()==null) || 
             (this.participantList!=null &&
              java.util.Arrays.equals(this.participantList, other.getParticipantList()))) &&
            ((this.orderCustomer==null && other.getOrderCustomer()==null) || 
             (this.orderCustomer!=null &&
              this.orderCustomer.equals(other.getOrderCustomer()))) &&
            ((this.taxExempt==null && other.getTaxExempt()==null) || 
             (this.taxExempt!=null &&
              this.taxExempt.equals(other.getTaxExempt()))) &&
            ((this.orderItemStatusCode==null && other.getOrderItemStatusCode()==null) || 
             (this.orderItemStatusCode!=null &&
              this.orderItemStatusCode.equals(other.getOrderItemStatusCode()))) &&
            ((this.taxAtUnitPrice==null && other.getTaxAtUnitPrice()==null) || 
             (this.taxAtUnitPrice!=null &&
              this.taxAtUnitPrice.equals(other.getTaxAtUnitPrice()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.promoCode==null && other.getPromoCode()==null) || 
             (this.promoCode!=null &&
              this.promoCode.equals(other.getPromoCode()))) &&
            ((this.sourceCode==null && other.getSourceCode()==null) || 
             (this.sourceCode!=null &&
              this.sourceCode.equals(other.getSourceCode()))) &&
            ((this.contentType==null && other.getContentType()==null) || 
             (this.contentType!=null &&
              this.contentType.equals(other.getContentType()))) &&
            ((this.quantity==null && other.getQuantity()==null) || 
             (this.quantity!=null &&
              this.quantity.equals(other.getQuantity()))) &&
            ((this.listPrice==null && other.getListPrice()==null) || 
             (this.listPrice!=null &&
              this.listPrice.equals(other.getListPrice()))) &&
            ((this.listPriceTotal==null && other.getListPriceTotal()==null) || 
             (this.listPriceTotal!=null &&
              this.listPriceTotal.equals(other.getListPriceTotal()))) &&
            ((this.discountAmount==null && other.getDiscountAmount()==null) || 
             (this.discountAmount!=null &&
              this.discountAmount.equals(other.getDiscountAmount()))) &&
            ((this.discountAmountTotal==null && other.getDiscountAmountTotal()==null) || 
             (this.discountAmountTotal!=null &&
              this.discountAmountTotal.equals(other.getDiscountAmountTotal()))) &&
            ((this.personalizationPriceTotal==null && other.getPersonalizationPriceTotal()==null) || 
             (this.personalizationPriceTotal!=null &&
              this.personalizationPriceTotal.equals(other.getPersonalizationPriceTotal()))) &&
            ((this.handlingCharge==null && other.getHandlingCharge()==null) || 
             (this.handlingCharge!=null &&
              this.handlingCharge.equals(other.getHandlingCharge()))) &&
            ((this.handlingChargeTotal==null && other.getHandlingChargeTotal()==null) || 
             (this.handlingChargeTotal!=null &&
              this.handlingChargeTotal.equals(other.getHandlingChargeTotal()))) &&
            ((this.handlingDiscount==null && other.getHandlingDiscount()==null) || 
             (this.handlingDiscount!=null &&
              this.handlingDiscount.equals(other.getHandlingDiscount()))) &&
            ((this.handlingDiscountTotal==null && other.getHandlingDiscountTotal()==null) || 
             (this.handlingDiscountTotal!=null &&
              this.handlingDiscountTotal.equals(other.getHandlingDiscountTotal()))) &&
            ((this.discountedHandlingChargeTotal==null && other.getDiscountedHandlingChargeTotal()==null) || 
             (this.discountedHandlingChargeTotal!=null &&
              this.discountedHandlingChargeTotal.equals(other.getDiscountedHandlingChargeTotal()))) &&
            ((this.discountedListPrice==null && other.getDiscountedListPrice()==null) || 
             (this.discountedListPrice!=null &&
              this.discountedListPrice.equals(other.getDiscountedListPrice()))) &&
            ((this.discountedListPriceTotal==null && other.getDiscountedListPriceTotal()==null) || 
             (this.discountedListPriceTotal!=null &&
              this.discountedListPriceTotal.equals(other.getDiscountedListPriceTotal()))) &&
            ((this.taxableTotal==null && other.getTaxableTotal()==null) || 
             (this.taxableTotal!=null &&
              this.taxableTotal.equals(other.getTaxableTotal()))) &&
            ((this.taxRate==null && other.getTaxRate()==null) || 
             (this.taxRate!=null &&
              this.taxRate.equals(other.getTaxRate()))) &&
            ((this.servicesTotal==null && other.getServicesTotal()==null) || 
             (this.servicesTotal!=null &&
              this.servicesTotal.equals(other.getServicesTotal()))) &&
            ((this.total==null && other.getTotal()==null) || 
             (this.total!=null &&
              this.total.equals(other.getTotal()))) &&
            ((this.orderHandling==null && other.getOrderHandling()==null) || 
             (this.orderHandling!=null &&
              this.orderHandling.equals(other.getOrderHandling()))) &&
            ((this.orderItemAddress==null && other.getOrderItemAddress()==null) || 
             (this.orderItemAddress!=null &&
              this.orderItemAddress.equals(other.getOrderItemAddress()))) &&
            ((this.orderItemPersonalizationList==null && other.getOrderItemPersonalizationList()==null) || 
             (this.orderItemPersonalizationList!=null &&
              java.util.Arrays.equals(this.orderItemPersonalizationList, other.getOrderItemPersonalizationList()))) &&
            ((this.orderItemSkuDetailValueList==null && other.getOrderItemSkuDetailValueList()==null) || 
             (this.orderItemSkuDetailValueList!=null &&
              java.util.Arrays.equals(this.orderItemSkuDetailValueList, other.getOrderItemSkuDetailValueList()))) &&
            ((this.orderItemNoteList==null && other.getOrderItemNoteList()==null) || 
             (this.orderItemNoteList!=null &&
              java.util.Arrays.equals(this.orderItemNoteList, other.getOrderItemNoteList()))) &&
            ((this.orderItemLocatorList==null && other.getOrderItemLocatorList()==null) || 
             (this.orderItemLocatorList!=null &&
              java.util.Arrays.equals(this.orderItemLocatorList, other.getOrderItemLocatorList()))) &&
            ((this.orderItemFeeList==null && other.getOrderItemFeeList()==null) || 
             (this.orderItemFeeList!=null &&
              java.util.Arrays.equals(this.orderItemFeeList, other.getOrderItemFeeList()))) &&
            ((this.orderItemLocationList==null && other.getOrderItemLocationList()==null) || 
             (this.orderItemLocationList!=null &&
              java.util.Arrays.equals(this.orderItemLocationList, other.getOrderItemLocationList()))) &&
            ((this.orderItemParameterList==null && other.getOrderItemParameterList()==null) || 
             (this.orderItemParameterList!=null &&
              java.util.Arrays.equals(this.orderItemParameterList, other.getOrderItemParameterList()))) &&
            ((this.orderItemStatusHistoryList==null && other.getOrderItemStatusHistoryList()==null) || 
             (this.orderItemStatusHistoryList!=null &&
              java.util.Arrays.equals(this.orderItemStatusHistoryList, other.getOrderItemStatusHistoryList()))) &&
            ((this.paymentOption==null && other.getPaymentOption()==null) || 
             (this.paymentOption!=null &&
              this.paymentOption.equals(other.getPaymentOption()))) &&
            ((this.orderItemOrderPaymentList==null && other.getOrderItemOrderPaymentList()==null) || 
             (this.orderItemOrderPaymentList!=null &&
              java.util.Arrays.equals(this.orderItemOrderPaymentList, other.getOrderItemOrderPaymentList()))) &&
            ((this.orderItemElementList==null && other.getOrderItemElementList()==null) || 
             (this.orderItemElementList!=null &&
              java.util.Arrays.equals(this.orderItemElementList, other.getOrderItemElementList()))) &&
            ((this.cancellationNumber==null && other.getCancellationNumber()==null) || 
             (this.cancellationNumber!=null &&
              this.cancellationNumber.equals(other.getCancellationNumber()))) &&
            ((this.cancellationDate==null && other.getCancellationDate()==null) || 
             (this.cancellationDate!=null &&
              this.cancellationDate.equals(other.getCancellationDate()))) &&
            ((this.comparisonKey==null && other.getComparisonKey()==null) || 
             (this.comparisonKey!=null &&
              this.comparisonKey.equals(other.getComparisonKey())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPayment() != null) {
            _hashCode += getPayment().hashCode();
        }
        if (getCRL() != null) {
            _hashCode += getCRL().hashCode();
        }
        if (getSupplierCode() != null) {
            _hashCode += getSupplierCode().hashCode();
        }
        if (getCorpDiscountCode() != null) {
            _hashCode += getCorpDiscountCode().hashCode();
        }
        if (getRuleSetID() != null) {
            _hashCode += getRuleSetID().hashCode();
        }
        if (getRatingCode() != null) {
            _hashCode += getRatingCode().hashCode();
        }
        if (getDepartmentCode() != null) {
            _hashCode += getDepartmentCode().hashCode();
        }
        if (getAllowsSku() != null) {
            _hashCode += getAllowsSku().hashCode();
        }
        if (getAllowsGiftWrap() != null) {
            _hashCode += getAllowsGiftWrap().hashCode();
        }
        if (getBasePrice() != null) {
            _hashCode += getBasePrice().hashCode();
        }
        if (getPreTaxTotalNow() != null) {
            _hashCode += getPreTaxTotalNow().hashCode();
        }
        if (getPreTaxTotalLater() != null) {
            _hashCode += getPreTaxTotalLater().hashCode();
        }
        if (getTaxTotalLater() != null) {
            _hashCode += getTaxTotalLater().hashCode();
        }
        if (getTaxTotalNow() != null) {
            _hashCode += getTaxTotalNow().hashCode();
        }
        if (getTotalLater() != null) {
            _hashCode += getTotalLater().hashCode();
        }
        if (getTotalNow() != null) {
            _hashCode += getTotalNow().hashCode();
        }
        if (getBasePriceTotal() != null) {
            _hashCode += getBasePriceTotal().hashCode();
        }
        if (getDescriptionLong() != null) {
            _hashCode += getDescriptionLong().hashCode();
        }
        if (getDisplayPrice() != null) {
            _hashCode += getDisplayPrice().hashCode();
        }
        if (getDisplayPriceTotal() != null) {
            _hashCode += getDisplayPriceTotal().hashCode();
        }
        if (getThumbFilename() != null) {
            _hashCode += getThumbFilename().hashCode();
        }
        if (getMarkupAmount() != null) {
            _hashCode += getMarkupAmount().hashCode();
        }
        if (getMarkupAmountTotal() != null) {
            _hashCode += getMarkupAmountTotal().hashCode();
        }
        if (getNewFlag() != null) {
            _hashCode += getNewFlag().hashCode();
        }
        if (getActiveStatus() != null) {
            _hashCode += getActiveStatus().hashCode();
        }
        if (getFeesTotal() != null) {
            _hashCode += getFeesTotal().hashCode();
        }
        if (getField1() != null) {
            _hashCode += getField1().hashCode();
        }
        if (getField2() != null) {
            _hashCode += getField2().hashCode();
        }
        if (getField3() != null) {
            _hashCode += getField3().hashCode();
        }
        if (getField4() != null) {
            _hashCode += getField4().hashCode();
        }
        if (getField5() != null) {
            _hashCode += getField5().hashCode();
        }
        if (getSkuExpectedDate() != null) {
            _hashCode += getSkuExpectedDate().hashCode();
        }
        if (getOrderId() != null) {
            _hashCode += getOrderId().hashCode();
        }
        if (getItemSequence() != null) {
            _hashCode += getItemSequence().hashCode();
        }
        if (getCatalogCode() != null) {
            _hashCode += getCatalogCode().hashCode();
        }
        if (getVendorCode() != null) {
            _hashCode += getVendorCode().hashCode();
        }
        if (getCategoryCode() != null) {
            _hashCode += getCategoryCode().hashCode();
        }
        if (getItemTypeCode() != null) {
            _hashCode += getItemTypeCode().hashCode();
        }
        if (getItemId() != null) {
            _hashCode += getItemId().hashCode();
        }
        if (getSkuId() != null) {
            _hashCode += getSkuId().hashCode();
        }
        if (getIsDueNow() != null) {
            _hashCode += getIsDueNow().hashCode();
        }
        if (getExternalSkuId() != null) {
            _hashCode += getExternalSkuId().hashCode();
        }
        if (getExternalSkuCatalogId() != null) {
            _hashCode += getExternalSkuCatalogId().hashCode();
        }
        if (getPurchaseDate() != null) {
            _hashCode += getPurchaseDate().hashCode();
        }
        if (getUsageDate() != null) {
            _hashCode += getUsageDate().hashCode();
        }
        if (getItemDescription() != null) {
            _hashCode += getItemDescription().hashCode();
        }
        if (getSkuDescription() != null) {
            _hashCode += getSkuDescription().hashCode();
        }
        if (getGiftWrapped() != null) {
            _hashCode += getGiftWrapped().hashCode();
        }
        if (getGiftWrapMessage() != null) {
            _hashCode += getGiftWrapMessage().hashCode();
        }
        if (getRoleCodeSupplierPortal() != null) {
            _hashCode += getRoleCodeSupplierPortal().hashCode();
        }
        if (getWarningList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWarningList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWarningList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTermsConditionsList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTermsConditionsList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTermsConditionsList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCancellationPolicies() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCancellationPolicies());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCancellationPolicies(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getParticipantList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getParticipantList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getParticipantList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOrderCustomer() != null) {
            _hashCode += getOrderCustomer().hashCode();
        }
        if (getTaxExempt() != null) {
            _hashCode += getTaxExempt().hashCode();
        }
        if (getOrderItemStatusCode() != null) {
            _hashCode += getOrderItemStatusCode().hashCode();
        }
        if (getTaxAtUnitPrice() != null) {
            _hashCode += getTaxAtUnitPrice().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getPromoCode() != null) {
            _hashCode += getPromoCode().hashCode();
        }
        if (getSourceCode() != null) {
            _hashCode += getSourceCode().hashCode();
        }
        if (getContentType() != null) {
            _hashCode += getContentType().hashCode();
        }
        if (getQuantity() != null) {
            _hashCode += getQuantity().hashCode();
        }
        if (getListPrice() != null) {
            _hashCode += getListPrice().hashCode();
        }
        if (getListPriceTotal() != null) {
            _hashCode += getListPriceTotal().hashCode();
        }
        if (getDiscountAmount() != null) {
            _hashCode += getDiscountAmount().hashCode();
        }
        if (getDiscountAmountTotal() != null) {
            _hashCode += getDiscountAmountTotal().hashCode();
        }
        if (getPersonalizationPriceTotal() != null) {
            _hashCode += getPersonalizationPriceTotal().hashCode();
        }
        if (getHandlingCharge() != null) {
            _hashCode += getHandlingCharge().hashCode();
        }
        if (getHandlingChargeTotal() != null) {
            _hashCode += getHandlingChargeTotal().hashCode();
        }
        if (getHandlingDiscount() != null) {
            _hashCode += getHandlingDiscount().hashCode();
        }
        if (getHandlingDiscountTotal() != null) {
            _hashCode += getHandlingDiscountTotal().hashCode();
        }
        if (getDiscountedHandlingChargeTotal() != null) {
            _hashCode += getDiscountedHandlingChargeTotal().hashCode();
        }
        if (getDiscountedListPrice() != null) {
            _hashCode += getDiscountedListPrice().hashCode();
        }
        if (getDiscountedListPriceTotal() != null) {
            _hashCode += getDiscountedListPriceTotal().hashCode();
        }
        if (getTaxableTotal() != null) {
            _hashCode += getTaxableTotal().hashCode();
        }
        if (getTaxRate() != null) {
            _hashCode += getTaxRate().hashCode();
        }
        if (getServicesTotal() != null) {
            _hashCode += getServicesTotal().hashCode();
        }
        if (getTotal() != null) {
            _hashCode += getTotal().hashCode();
        }
        if (getOrderHandling() != null) {
            _hashCode += getOrderHandling().hashCode();
        }
        if (getOrderItemAddress() != null) {
            _hashCode += getOrderItemAddress().hashCode();
        }
        if (getOrderItemPersonalizationList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemPersonalizationList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemPersonalizationList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOrderItemSkuDetailValueList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemSkuDetailValueList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemSkuDetailValueList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOrderItemNoteList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemNoteList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemNoteList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOrderItemLocatorList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemLocatorList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemLocatorList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOrderItemFeeList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemFeeList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemFeeList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOrderItemLocationList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemLocationList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemLocationList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOrderItemParameterList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemParameterList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemParameterList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOrderItemStatusHistoryList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemStatusHistoryList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemStatusHistoryList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaymentOption() != null) {
            _hashCode += getPaymentOption().hashCode();
        }
        if (getOrderItemOrderPaymentList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemOrderPaymentList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemOrderPaymentList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOrderItemElementList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrderItemElementList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrderItemElementList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCancellationNumber() != null) {
            _hashCode += getCancellationNumber().hashCode();
        }
        if (getCancellationDate() != null) {
            _hashCode += getCancellationDate().hashCode();
        }
        if (getComparisonKey() != null) {
            _hashCode += getComparisonKey().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TCOrderItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCOrderItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Payment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderPayment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CRL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CRL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("supplierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SupplierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("corpDiscountCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CorpDiscountCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleSetID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RuleSetID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ratingCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RatingCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departmentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DepartmentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowsSku");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "AllowsSku"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowsGiftWrap");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "AllowsGiftWrap"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basePrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "BasePrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preTaxTotalNow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PreTaxTotalNow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preTaxTotalLater");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PreTaxTotalLater"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxTotalLater");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TaxTotalLater"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxTotalNow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TaxTotalNow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalLater");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TotalLater"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalNow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TotalNow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basePriceTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "BasePriceTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descriptionLong");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DescriptionLong"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DisplayPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayPriceTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DisplayPriceTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("thumbFilename");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ThumbFilename"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("markupAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "MarkupAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("markupAmountTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "MarkupAmountTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "NewFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activeStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ActiveStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feesTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FeesTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field5");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Field5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skuExpectedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SkuExpectedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ItemSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catalogCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CatalogCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "VendorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CategoryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ItemTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ItemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skuId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SkuId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isDueNow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "IsDueNow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalSkuId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ExternalSkuId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalSkuCatalogId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ExternalSkuCatalogId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchaseDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PurchaseDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usageDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "UsageDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ItemDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skuDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SkuDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftWrapped");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "GiftWrapped"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftWrapMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "GiftWrapMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roleCodeSupplierPortal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RoleCodeSupplierPortal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("warningList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "WarningList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCWarning"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCWarning"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("termsConditionsList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditionsList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditions"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditions"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancellationPolicies");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CancellationPolicies"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditions"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditions"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("participantList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ParticipantList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Participant"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Participant"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderCustomer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderCustomer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderCustomer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxExempt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TaxExempt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAtUnitPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TaxAtUnitPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PromoCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "SourceCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ContentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ListPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listPriceTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ListPriceTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DiscountAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountAmountTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DiscountAmountTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personalizationPriceTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PersonalizationPriceTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("handlingCharge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "HandlingCharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("handlingChargeTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "HandlingChargeTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("handlingDiscount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "HandlingDiscount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("handlingDiscountTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "HandlingDiscountTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountedHandlingChargeTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DiscountedHandlingChargeTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountedListPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DiscountedListPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountedListPriceTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DiscountedListPriceTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxableTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TaxableTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TaxRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("servicesTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ServicesTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderHandling");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderHandling"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderHandling"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemAddress"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemPersonalizationList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemPersonalizationList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemPersonalization"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemPersonalization"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemSkuDetailValueList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemSkuDetailValueList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemSkuDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemSkuDetail"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemNoteList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemNoteList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemNote"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemNote"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemLocatorList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocatorList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocator"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemFeeList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemFeeList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Fee"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Fee"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemLocationList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocationList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocation"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemParameterList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemParameterList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemParameter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemParameter"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemStatusHistoryList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemStatusHistoryList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemStatusHistory"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemStatusHistory"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentOption");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PaymentOption"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentOptions"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemOrderPaymentList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemOrderPaymentList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderItemElementList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemElementList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemElement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemElement"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancellationNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CancellationNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancellationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "CancellationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comparisonKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ComparisonKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
