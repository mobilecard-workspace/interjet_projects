/**
 * TypeOfSale.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices.DataContracts.Booking;

public class TypeOfSale  extends com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage  implements java.io.Serializable {
    private java.lang.String paxResidentCountry;

    private java.lang.String promotionCode;

    private java.lang.String[] fareTypes;

    public TypeOfSale() {
    }

    public TypeOfSale(
           com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState state,
           java.lang.String paxResidentCountry,
           java.lang.String promotionCode,
           java.lang.String[] fareTypes) {
        super(
            state);
        this.paxResidentCountry = paxResidentCountry;
        this.promotionCode = promotionCode;
        this.fareTypes = fareTypes;
    }


    /**
     * Gets the paxResidentCountry value for this TypeOfSale.
     * 
     * @return paxResidentCountry
     */
    public java.lang.String getPaxResidentCountry() {
        return paxResidentCountry;
    }


    /**
     * Sets the paxResidentCountry value for this TypeOfSale.
     * 
     * @param paxResidentCountry
     */
    public void setPaxResidentCountry(java.lang.String paxResidentCountry) {
        this.paxResidentCountry = paxResidentCountry;
    }


    /**
     * Gets the promotionCode value for this TypeOfSale.
     * 
     * @return promotionCode
     */
    public java.lang.String getPromotionCode() {
        return promotionCode;
    }


    /**
     * Sets the promotionCode value for this TypeOfSale.
     * 
     * @param promotionCode
     */
    public void setPromotionCode(java.lang.String promotionCode) {
        this.promotionCode = promotionCode;
    }


    /**
     * Gets the fareTypes value for this TypeOfSale.
     * 
     * @return fareTypes
     */
    public java.lang.String[] getFareTypes() {
        return fareTypes;
    }


    /**
     * Sets the fareTypes value for this TypeOfSale.
     * 
     * @param fareTypes
     */
    public void setFareTypes(java.lang.String[] fareTypes) {
        this.fareTypes = fareTypes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TypeOfSale)) return false;
        TypeOfSale other = (TypeOfSale) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.paxResidentCountry==null && other.getPaxResidentCountry()==null) || 
             (this.paxResidentCountry!=null &&
              this.paxResidentCountry.equals(other.getPaxResidentCountry()))) &&
            ((this.promotionCode==null && other.getPromotionCode()==null) || 
             (this.promotionCode!=null &&
              this.promotionCode.equals(other.getPromotionCode()))) &&
            ((this.fareTypes==null && other.getFareTypes()==null) || 
             (this.fareTypes!=null &&
              java.util.Arrays.equals(this.fareTypes, other.getFareTypes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPaxResidentCountry() != null) {
            _hashCode += getPaxResidentCountry().hashCode();
        }
        if (getPromotionCode() != null) {
            _hashCode += getPromotionCode().hashCode();
        }
        if (getFareTypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFareTypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFareTypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TypeOfSale.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paxResidentCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxResidentCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promotionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PromotionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fareTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
