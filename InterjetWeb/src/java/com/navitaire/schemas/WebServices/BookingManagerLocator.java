/**
 * BookingManagerLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices;

public class BookingManagerLocator extends org.apache.axis.client.Service implements com.navitaire.schemas.WebServices.BookingManager {

    public BookingManagerLocator() {
    }


    public BookingManagerLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BookingManagerLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BasicHttpBinding_IBookingManager
    private java.lang.String BasicHttpBinding_IBookingManager_address = "https://ijr3xapi.navitaire.com/BookingManager.svc";

    public java.lang.String getBasicHttpBinding_IBookingManagerAddress() {
        return BasicHttpBinding_IBookingManager_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BasicHttpBinding_IBookingManagerWSDDServiceName = "BasicHttpBinding_IBookingManager";

    public java.lang.String getBasicHttpBinding_IBookingManagerWSDDServiceName() {
        return BasicHttpBinding_IBookingManagerWSDDServiceName;
    }

    public void setBasicHttpBinding_IBookingManagerWSDDServiceName(java.lang.String name) {
        BasicHttpBinding_IBookingManagerWSDDServiceName = name;
    }

    public com.navitaire.schemas.WebServices.IBookingManager getBasicHttpBinding_IBookingManager() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BasicHttpBinding_IBookingManager_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBasicHttpBinding_IBookingManager(endpoint);
    }

    public com.navitaire.schemas.WebServices.IBookingManager getBasicHttpBinding_IBookingManager(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.navitaire.schemas.WebServices.BasicHttpBinding_IBookingManagerStub _stub = new com.navitaire.schemas.WebServices.BasicHttpBinding_IBookingManagerStub(portAddress, this);
            _stub.setPortName(getBasicHttpBinding_IBookingManagerWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBasicHttpBinding_IBookingManagerEndpointAddress(java.lang.String address) {
        BasicHttpBinding_IBookingManager_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.navitaire.schemas.WebServices.IBookingManager.class.isAssignableFrom(serviceEndpointInterface)) {
                com.navitaire.schemas.WebServices.BasicHttpBinding_IBookingManagerStub _stub = new com.navitaire.schemas.WebServices.BasicHttpBinding_IBookingManagerStub(new java.net.URL(BasicHttpBinding_IBookingManager_address), this);
                _stub.setPortName(getBasicHttpBinding_IBookingManagerWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BasicHttpBinding_IBookingManager".equals(inputPortName)) {
            return getBasicHttpBinding_IBookingManager();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices", "BookingManager");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices", "BasicHttpBinding_IBookingManager"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BasicHttpBinding_IBookingManager".equals(portName)) {
            setBasicHttpBinding_IBookingManagerEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
