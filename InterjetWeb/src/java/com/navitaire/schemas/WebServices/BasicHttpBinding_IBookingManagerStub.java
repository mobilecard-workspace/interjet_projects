/**
 * BasicHttpBinding_IBookingManagerStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.navitaire.schemas.WebServices;

public class BasicHttpBinding_IBookingManagerStub extends org.apache.axis.client.Stub implements com.navitaire.schemas.WebServices.IBookingManager {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[64];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
        _initOperationDesc5();
        _initOperationDesc6();
        _initOperationDesc7();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateFeeStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdateFeeStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateFeeStatusRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateFeeStatusRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        oper.setReturnClass(boolean.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Succeeded"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateTickets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdateTicketsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateTicketsRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateTicketsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateTicketsResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateTicketsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdateTicketsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Upgrade");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpgradeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpgradeRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpgradeRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpgradeResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpgradeResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpgradeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Downgrade");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "DowngradeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DowngradeRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DowngradeRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DowngradeResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DowngradeResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "DowngradeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetUpgradeAvailability");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetUpgradeAvailabilityRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetUpgradeAvailabilityRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetUpgradeAvailabilityRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetUpgradeAvailabilityResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetUpgradeAvailabilityResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetUpgradeAvailabilityResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Clear");
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ChangeSourcePointOfSale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "ChangeSourcePointOfSaleRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ChangeSourcePointOfSaleRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ChangeSourcePointOfSaleRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ChangeSourcePointOfSaleResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ChangeSourcePointOfSaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "ChangeSourcePointOfSaleResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("MoveJourneyBookings");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyBookingsRequestData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyBookingsRequestData"), com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyBookingsRequestData.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyBookingsResponseData"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyBookingsResponseData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyBookingsResponseData"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("MoveJourney");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyByKeyRequestData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyByKeyRequestData"), com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyByKeyRequestData.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingUpdateResponseData"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.DataContracts.Booking.BookingUpdateResponseData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingUpdateResponseData"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPostCommitResults");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        oper.setReturnClass(boolean.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ContinuePulling"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SendItinerary");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices", "RecordLocatorReqData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetEquipmentProperties");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetEquipmentPropertiesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetEquipmentPropertiesRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetEquipmentPropertiesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetEquipmentPropertiesResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetEquipmentPropertiesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetEquipmentPropertiesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("OverrideFee");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "OverrideFeeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">OverrideFeeRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.OverrideFeeRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">OverrideFeeResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.OverrideFeeResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "OverrideFeeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdatePassengers");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdatePassengersRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePassengersRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePassengersRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePassengerResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePassengerResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdatePassengerResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateSSR");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdateSSRRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateSSRRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateSSRRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateSSRResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateSSRResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdateSSRResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdatePrice");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdatePriceRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePriceRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePriceRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePriceResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePriceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdatePriceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CaptureBaggageEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "CaptureBaggageEventRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CaptureBaggageEventRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CaptureBaggageEventRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CaptureBaggageEventResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CaptureBaggageEventResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "CaptureBaggageEventResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("FindBaggageEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBaggageEventRequestData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBaggageEventRequestData"), com.navitaire.schemas.WebServices.DataContracts.Booking.FindBaggageEventRequestData.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBaggageEventResponseData"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.DataContracts.Booking.FindBaggageEventResponseData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBaggageEventResponseData"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CalculateGuestValues");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "CalculateGuestValuesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CalculateGuestValuesRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CalculateGuestValuesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CalculateGuestValuesResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CalculateGuestValuesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "CalculateGuestValuesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ScorePassengers");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "ScorePassengersRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ScorePassengersRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ScorePassengersRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ScorePassengersResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ScorePassengersResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "ScorePassengersResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BookingCommit");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "BookingCommitRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">BookingCommitRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.BookingCommitRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">BookingCommitResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.BookingCommitResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "BookingCommitResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ResellSSR");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "ResellSSRRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ResellSSRRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ResellSSRRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ResellSSRResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ResellSSRResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "ResellSSRResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateContacts");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdateContactsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateContactsRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateContactsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateContactsResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateContactsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdateContactsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateBookingComponentData");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdateBookingComponentDataRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateBookingComponentDataRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateBookingComponentDataRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateBookingComponentDataResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateBookingComponentDataResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UpdateBookingComponentDataResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TravelCommerceSell");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "TravelCommerceSellRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceSellRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceSellRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceSellResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceSellResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "TravelCommerceSellResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TravelCommerceCancelService");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "TravelCommerceCancelServiceRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceCancelServiceRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceCancelServiceRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceCancelServiceResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceCancelServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "TravelCommerceCancelServiceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBookingStatelessFiltered");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingStatelessFilteredRequestData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingStatelessFilteredRequestData"), com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingStatelessFilteredRequestData.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingStatelessFilteredResponseData"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingStatelessFilteredResponseData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingStatelessFilteredResponseData"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBookingFromStateFiltered");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFromStateFilteredRequestData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFromStateFilteredRequestData"), com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFromStateFilteredRequestData.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFromStateFilteredResponseData"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFromStateFilteredResponseData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFromStateFilteredResponseData"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAvailability");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetAvailabilityRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetAvailabilityRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetAvailabilityRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetAvailabilityByTripResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetAvailabilityByTripResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetAvailabilityByTripResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLowFareAvailability");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetLowFareAvailabilityRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareAvailabilityRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAvailabilityRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareAvailabilityResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAvailabilityResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetLowFareAvailabilityResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLowFareAVSMessage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetLowFareAVSMessageRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareAVSMessageRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAVSMessageRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareAVSMessageResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAVSMessageResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetLowFareAVSMessageResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLowFareTripAvailability");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetLowFareTripAvailabilityRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareTripAvailabilityRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareTripAvailabilityRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareTripAvailabilityResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareTripAvailabilityResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetLowFareTripAvailabilityResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMoveAvailability");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetMoveAvailabilityRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveAvailabilityRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveAvailabilityRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveAvailabilityResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveAvailabilityResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetMoveAvailabilityResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMoveFeePrice");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetMoveFeePriceRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveFeePriceRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveFeePriceRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveFeePriceResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveFeePriceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetMoveFeePriceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SeparateSegmentByEquipment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segment"), com.navitaire.schemas.WebServices.DataContracts.Booking.Segment.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeparateSegmentByEquipmentResponseData"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.DataContracts.Booking.SeparateSegmentByEquipmentResponseData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegList"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetSSRAvailability");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetSSRAvailabilityRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSSRAvailabilityRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSSRAvailabilityResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetSSRAvailabilityResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[35] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetSSRAvailabilityForBooking");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetSSRAvailabilityForBookingRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSSRAvailabilityForBookingRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityForBookingRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSSRAvailabilityForBookingResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityForBookingResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetSSRAvailabilityForBookingResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[36] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("FindBooking");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingRequestData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingRequestData"), com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingRequestData.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingResponseData"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingResponseData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingRespData"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[37] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBooking");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[38] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBookingFromState");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices", "GetBookingFromStateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices", ">GetBookingFromStateRequest"), com.navitaire.schemas.WebServices.GetBookingFromStateRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingFromStateResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingFromStateResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingFromStateResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[39] = oper;

    }

    private static void _initOperationDesc5(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBookingHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingHistoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingHistoryRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingHistoryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingHistoryResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingHistoryResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingHistoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[40] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBookingBaggage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingBaggageRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingBaggageRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingBaggageRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingBaggageResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingBaggageResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingBaggageResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[41] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AcceptScheduleChanges");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AcceptScheduleChangesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AcceptScheduleChangesRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AcceptScheduleChangesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[42] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddBookingComments");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AddBookingCommentsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddBookingCommentsRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddBookingCommentsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddBookingCommentsResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddBookingCommentsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AddBookingCommentsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[43] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetRecordLocatorList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetRecordLocatorListRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetRecordLocatorListRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetRecordLocatorListRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetRecordLocatorListResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetRecordLocatorListResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetRecordLocatorListResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[44] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Cancel");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "CancelRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CancelRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CancelRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CancelResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CancelResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "CancelResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[45] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Divide");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "DivideRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DivideRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DivideRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DivideResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DivideResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "DivideResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[46] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("FareOverride");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "FareOverrideRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">FareOverrideRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.FareOverrideRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">FareOverrideResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.FareOverrideResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "FareOverrideResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[47] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetBookingPayments");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingPaymentsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingPaymentsRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingPaymentsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingPaymentsResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingPaymentsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetBookingPaymentsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[48] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddPaymentToBooking");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AddPaymentToBookingRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddPaymentToBookingRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddPaymentToBookingResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AddPaymentToBookingResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[49] = oper;

    }

    private static void _initOperationDesc6(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddInProcessPaymentToBooking");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AddInProcessPaymentToBookingRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddInProcessPaymentToBookingRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddInProcessPaymentToBookingRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddInProcessPaymentToBookingResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddInProcessPaymentToBookingResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AddInProcessPaymentToBookingResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[50] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ApplyPromotion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "ApplyPromotionRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ApplyPromotionRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ApplyPromotionRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ApplyPromotionResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ApplyPromotionResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "ApplyPromotionResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[51] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CancelInProcessPayment");
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[52] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DCCQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "DCCQueryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DCCQueryRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCQueryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DCCQueryResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCQueryResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "DCCQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[53] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AcceptDCCOffer");
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DCCPaymentResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "DCCPaymentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[54] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RejectDCCOffer");
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DCCPaymentResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "DCCPaymentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[55] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DCCNotOffered");
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DCCPaymentResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "DCCPaymentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[56] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPaymentFeePrice");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetPaymentFeePriceRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetPaymentFeePriceRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetPaymentFeePriceRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetPaymentFeePriceResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetPaymentFeePriceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetPaymentFeePriceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[57] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetSeatAvailability");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetSeatAvailabilityRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSeatAvailabilityRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSeatAvailabilityRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSeatAvailabilityResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSeatAvailabilityResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "GetSeatAvailabilityResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[58] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AssignSeats");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AssignSeatsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AssignSeatsRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AssignSeatsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AssignSeatsResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AssignSeatsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "AssignSeatsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[59] = oper;

    }

    private static void _initOperationDesc7(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UnassignSeats");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UnassignSeatsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UnassignSeatsRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UnassignSeatsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UnassignSeatsResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UnassignSeatsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "UnassignSeatsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[60] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Commit");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "CommitRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CommitRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CommitResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "CommitResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[61] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetItineraryPrice");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "PriceItineraryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">PriceItineraryRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.PriceItineraryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">PriceItineraryResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.PriceItineraryResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "PriceItineraryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[62] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Sell");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "SellRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">SellRequest"), com.navitaire.schemas.WebServices.ServiceContracts.BookingService.SellRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">SellResponse"));
        oper.setReturnClass(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.SellResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", "SellResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[63] = oper;

    }

    public BasicHttpBinding_IBookingManagerStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public BasicHttpBinding_IBookingManagerStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public BasicHttpBinding_IBookingManagerStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
        addBindings2();
        addBindings3();
        addBindings4();
        addBindings5();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LowFareFlightFilter");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LowFareFlightFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations", "LoyaltyFilter");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.Navitaire_NewSkies_WebServices_DataContracts_Common_Enumerations.LoyaltyFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System.Collections.Generic", "ArrayOfKeyValuePairOfstringstring");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.System_Collections_Generic.KeyValuePairOfstringstring[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System.Collections.Generic", "KeyValuePairOfstringstring");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System.Collections.Generic", "KeyValuePairOfstringstring");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System.Collections.Generic", "KeyValuePairOfstringstring");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.System_Collections_Generic.KeyValuePairOfstringstring.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfchar");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "char");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfint");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOflong");
            cachedSerQNames.add(qName);
            cls = long[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "long");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfshort");
            cachedSerQNames.add(qName);
            cls = short[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "short");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfunsignedInt");
            cachedSerQNames.add(qName);
            cls = org.apache.axis.types.UnsignedInt[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "unsignedInt");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
            cachedSerQNames.add(qName);
            cls = int.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AcceptScheduleChangesRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AcceptScheduleChangesRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AddBookingCommentsRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AddBookingCommentsRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AddPaymentToBookingRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AddPaymentToBookingRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AddPaymentToBookingResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AddPaymentToBookingResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AgencyAccount");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AgencyAccount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFareRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFareRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ApplyPromotionRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.ApplyPromotionRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfAlternateLowFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFare[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFare");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFare");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfAlternateLowFareRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AlternateLowFareRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFareRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AlternateLowFareRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfArrayOfJourneyDateMarket");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.JourneyDateMarket[][].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfJourneyDateMarket");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfJourneyDateMarket");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfArrayOfLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[][].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfLeg");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfLeg");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfAvailabilityStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityStatus[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityStatus");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityStatus");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfAvailablePaxSSR");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AvailablePaxSSR[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailablePaxSSR");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailablePaxSSR");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBaggageEvent");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BaggageEvent[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageEvent");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageEvent");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingClassAVS");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingClassAVS[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingClassAVS");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingClassAVS");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingComment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComment");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingComponent");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponent");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponent");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingComponentCharge");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponentCharge[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentCharge");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentCharge");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingContact");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContact");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContact");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingHistory");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHistory[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHistory");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHistory");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingName");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingName");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingName");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingPaymentTransfer");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingPaymentTransfer[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingPaymentTransfer");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingPaymentTransfer");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingQueueInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingQueueInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingQueueInfo");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingQueueInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfBookingServiceCharge");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingServiceCharge");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingServiceCharge");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfCompartmentInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CompartmentInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentInfo");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDateFlightAVS");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightAVS[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightAVS");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightAVS");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDateFlightBookingClass");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightBookingClass[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightBookingClass");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightBookingClass");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDateFlightFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFare[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFare");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFare");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDateFlightFares");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFares[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFares");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFares");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDateFlightLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLeg[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLeg");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLeg");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDateFlightLowFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLowFare[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLowFare");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLowFare");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDateMarketAVS");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketAVS[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketAVS");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketAVS");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDateMarketLowFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketLowFare[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketLowFare");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketLowFare");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDateMarketSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketSegment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketSegment");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketSegment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfDowngradeSegmentRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DowngradeSegmentRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DowngradeSegmentRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DowngradeSegmentRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfEquipmentDeviation");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviation");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviation");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfEquipmentInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentInfo");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfEquipmentProperty");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfEquipmentRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfEquipmentResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentResponse[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentResponse");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentResponse");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Fare[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Fare");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Fare");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfFeeStatusRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FeeStatusRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeStatusRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeStatusRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfFindBookingData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingData[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingData");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfInventorySegmentSSRNest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.InventorySegmentSSRNest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InventorySegmentSSRNest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InventorySegmentSSRNest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfJourney");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Journey[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfJourneyDateMarket");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.JourneyDateMarket[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneyDateMarket");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneyDateMarket");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Leg[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Leg");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Leg");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfLegClass");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LegClass[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegClass");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegClass");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfLegKey");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegKey");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegKey");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfLegNest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LegNest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegNest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegNest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfLegSSR");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LegSSR[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegSSR");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegSSR");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfLowFareAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAvailabilityRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfLowFareAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAvailabilityResponse[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityResponse");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityResponse");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfMoveBookingResult");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveBookingResult[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveBookingResult");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveBookingResult");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfMoveFeePriceItem");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceItem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceItem");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassenger");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassengerAddress");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerAddress[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerAddress");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerAddress");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassengerBag");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerBag[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerBag");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerBag");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassengerFee");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassengerInfant");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfant");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfant");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassengerInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfo");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassengerProgram");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerProgram");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerProgram");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassengerScore");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScore[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScore");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScore");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassengerTravelDocument");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTravelDocument[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTravelDocument");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTravelDocument");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPassengerTypeInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTypeInfo");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTypeInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxBag");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxBag[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxBag");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxBag");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxFare[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxFare");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxFare");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxPriceType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceType");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxScore");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxScore[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxScore");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxScore");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxSeat");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeat[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeat");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeat");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxSeatPreference");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSegment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSegment");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSegment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxSSR");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSR[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSR");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSR");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxSSRPrice");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSRPrice[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSRPrice");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSRPrice");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxTicket");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicket[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicket");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicket");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaxTicketUpdate");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicketUpdate[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicketUpdate");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicketUpdate");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPayment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Payment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payment");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaymentAddress");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddress");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddress");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaymentField");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentField");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentField");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPaymentValidationError");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentValidationError[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentValidationError");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentValidationError");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPriceJourney");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourney[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourney");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourney");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPriceLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PriceLeg[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceLeg");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceLeg");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfPriceSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PriceSegment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceSegment");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceSegment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSeatAvailabilityLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeatAvailabilityLeg[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailabilityLeg");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailabilityLeg");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSeatGroupPassengerFee");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeatGroupPassengerFee[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatGroupPassengerFee");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatGroupPassengerFee");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSeatInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeatInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatInfo");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Segment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segment");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSegmentSeatRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSeatRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSeatRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSeatRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSegmentSSRRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSSRRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSSRRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSSRRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSegmentTicketRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentTicketRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentTicketRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentTicketRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSellJourney");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourney[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourney");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourney");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSellKeyList");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellKeyList");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellKeyList");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSellSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellSegment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellSegment");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellSegment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSSRLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SSRLeg[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRLeg");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRLeg");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfSSRSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SSRSegment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRSegment");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRSegment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfUpgrade");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Upgrade[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Upgrade");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Upgrade");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfUpgradeSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeSegment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegment");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ArrayOfUpgradeSegmentRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeSegmentRequest[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegmentRequest");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegmentRequest");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailabilityStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AvailabilityStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailableFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AvailableFare.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "AvailablePaxSSR");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.AvailablePaxSSR.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BaggageEvent");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BaggageEvent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Booking");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Booking.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingByBookingID");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByBookingID.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingByRecordLocator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingByRecordLocator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingClassAVS");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingClassAVS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingCommitRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingCommitRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponent");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingComponentCharge");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingComponentCharge.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingContact");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingContact.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHistory");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHistory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingHold");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingHold.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingName");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingPaymentTransfer");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingPaymentTransfer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingQueueInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingQueueInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingServiceCharge");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingServiceCharge.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingSum");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingSum.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "BookingUpdateResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.BookingUpdateResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelFee");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CancelFee.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelJourney");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CancelJourney.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelJourneyRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CancelJourneyRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CancelRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CancelSSR");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CancelSSR.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CaptureBaggageEventRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CaptureBaggageEventRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CaptureBaggageEventResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CaptureBaggageEventResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CommitRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CommitRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CompartmentInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CompartmentInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreditFile");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CreditFile.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "CreditShell");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.CreditShell.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightAVS");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightAVS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightBookingClass");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightBookingClass.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFare.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightFares");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightFares.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLeg.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateFlightLowFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateFlightLowFare.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketAVS");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketAVS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketLowFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketLowFare.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DateMarketSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DateMarketSegment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DCC");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DCC.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DCCQueryRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DCCQueryRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DivideRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DivideRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DowngradeRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DowngradeRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "DowngradeSegmentRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.DowngradeSegmentRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentDeviation");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentDeviation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentListRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentListRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentListResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentListResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentProperty");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentPropertyTypeCodesLookup");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentPropertyTypeCodesLookup.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "EquipmentResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.EquipmentResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Error");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Error.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Fare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Fare.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FareOverrideRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FareOverrideRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FeeRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FeeStatusRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FeeStatusRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Filter");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Filter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBaggageEventRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindBaggageEventRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBaggageEventResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindBaggageEventResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindBookingResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByAgencyNumber");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgencyNumber.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByAgentID");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentID.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByAgentName");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByAgentName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByBagTag");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBagTag.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByBookingDate");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByBookingDate.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByContact");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContact.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByContactCustomerNumber");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByContactCustomerNumber.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByCreditCardNumber");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCreditCardNumber.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByCustomer");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByCustomerNumber");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByCustomerNumber.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByEmailAddress");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByEmailAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByName");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByPhoneNumber");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByPhoneNumber.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByRecordLocator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByRecordLocator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByThirdPartyRecordLocator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByThirdPartyRecordLocator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "FindByTravelDocument");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.FindByTravelDocument.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingBaggageRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingBaggageRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFilter");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFromStateFilteredRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFromStateFilteredRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingFromStateFilteredResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFromStateFilteredResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings2() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingHistoryRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingHistoryRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingHistoryResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingHistoryResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingPaymentResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingPaymentResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingPaymentsRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingPaymentsRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingStatelessFilteredRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingStatelessFilteredRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetBookingStatelessFilteredResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingStatelessFilteredResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByID");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetByID.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByRecordLocator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GetByThirdPartyRecordLocator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GetByThirdPartyRecordLocator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "GuestValuesRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.GuestValuesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "InventorySegmentSSRNest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.InventorySegmentSSRNest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ItineraryPriceRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.ItineraryPriceRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Journey");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Journey.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "JourneyDateMarket");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.JourneyDateMarket.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Leg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Leg.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegClass");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LegClass.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LegInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegKey");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LegKey.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegNest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LegNest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LegSSR");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LegSSR.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAVSMessageRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAVSMessageRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareAVSMessageResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareAVSMessageResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareTripAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareTripAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "LowFareTripAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.LowFareTripAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MCCRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MCCRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveBookingResult");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveBookingResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceItem");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveFeePriceResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveFeePriceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyBookingsRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyBookingsRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyBookingsResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyBookingsResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "MoveJourneyByKeyRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyByKeyRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OperationsInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.OperationsInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "OrderItem");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.OrderItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Passenger");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Passenger.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerAddress");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerBag");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerBag.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerFee");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerFee.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfant");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfant.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerProgram");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerProgram.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScore");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScore.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScoresRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScoresRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerScoresResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerScoresResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTravelDocument");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTravelDocument.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PassengerTypeInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PassengerTypeInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxBag");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxBag.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxFare.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxPriceType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxPriceType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxScore");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxScore.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeat");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeat.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSeatPreference");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSeatPreference.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSegment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSR");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSR.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxSSRPrice");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxSSRPrice.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicket");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicket.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaxTicketUpdate");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaxTicketUpdate.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Payment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Payment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentAddress");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentFeePriceRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentFeePriceRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentFeePriceResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentFeePriceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentField");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentValidationError");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentValidationError.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PaymentVoucher");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentVoucher.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourney");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourney.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceJourneyRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PriceJourneyRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PriceLeg.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PriceRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "PriceSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.PriceSegment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ReceivedByInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.ReceivedByInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "RecordLocatorListRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.RecordLocatorListRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ResellSSR");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.ResellSSR.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailabilityLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeatAvailabilityLeg.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeatAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeatAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatGroupPassengerFee");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeatGroupPassengerFee.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatInfo");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeatInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeatSellRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeatSellRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Segment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Segment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSeatRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSeatRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentSSRRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentSSRRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SegmentTicketRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SegmentTicketRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellFare");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellFare.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellFee");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellFee.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellFeeRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellFeeRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourney");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourney.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyByKeyRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyByKeyRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyByKeyRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellJourneyRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellJourneyRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellKeyList");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellKeyList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellSegment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SellSSR");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SellSSR.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings3() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SeparateSegmentByEquipmentResponseData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SeparateSegmentByEquipmentResponseData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ServiceMessage");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.ServiceMessage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRAvailabilityForBookingRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SSRAvailabilityForBookingRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRAvailabilityForBookingResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SSRAvailabilityForBookingResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SSRAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SSRAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRLeg");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SSRLeg.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SSRRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "SSRSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.SSRSegment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Success");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Success.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ThreeDSecure");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecure.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ThreeDSecureRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.ThreeDSecureRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TicketRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.TicketRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Time");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Time.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TravelCommerceSellRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.TravelCommerceSellRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TripAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.TripAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TripAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.TripAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "TypeOfSale");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.TypeOfSale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateBookingComponentDataRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateBookingComponentDataRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateContactsRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateContactsRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateFeeStatusRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateFeeStatusRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdatePassengersRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpdatePassengersRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateSourcePointOfSaleRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateSourcePointOfSaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpdateType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpdateType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Upgrade");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Upgrade.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeRequestData");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeRequestData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeSegment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "UpgradeSegmentRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.UpgradeSegmentRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "ValidationPayment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.ValidationPayment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Booking", "Warning");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Booking.Warning.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AOSFeeType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AOSFeeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ArrayOfJourneySortKey");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "JourneySortKey");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "JourneySortKey");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ArrivalStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ArrivalStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AuthorizationStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AuthorizationStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AvailabilityFilter");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "AvailabilityType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.AvailabilityType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BaggageStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BaggageStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingBy");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingBy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingPaymentStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingPaymentStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingProfileStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingProfileStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "BookingStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.BookingStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "CancelBy");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CancelBy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChannelType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChannelType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChargeType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ChargeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ClassStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ClassStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "CollectType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CollectType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "CommentType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.CommentType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DCCStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DCCStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DepartureStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DepartureStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DistributionOption");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DistributionOption.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DivideAction");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DivideAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "DOW");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.DOW.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "EquipmentCategory");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.EquipmentCategory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FareApplicationType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareApplicationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FareClassControl");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareClassControl.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FareRuleFilter");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareRuleFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FareStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FareStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FeeType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FeeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FindBookingBy");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FindBookingBy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FlightType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlightType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "FlyAheadOfferStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.FlyAheadOfferStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "Gender");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.Gender.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "GetBookingBy");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingBy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "GetBookingFilteredBy");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingFilteredBy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "IgnoreLiftStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.IgnoreLiftStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "InboundOutbound");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.InboundOutbound.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "JourneySortKey");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.JourneySortKey.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "LegStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LegStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "LiftStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.LiftStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "MessageState");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MessageState.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "MovePassengerJourneyType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.MovePassengerJourneyType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "NestType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.NestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "NotificationPreference");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.NotificationPreference.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "OSISeverity");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.OSISeverity.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaidStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaidStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ParticipantDataUseStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.ParticipantDataUseStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentMethodType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentMethodType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentOptions");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentOptions.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentReferenceType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentReferenceType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PaymentValidationErrorType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PaymentValidationErrorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PriceItineraryBy");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceItineraryBy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PriceStatus");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PriceStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "PricingType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.PricingType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "QueueAction");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "QueueEventType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueEventType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "QueueMode");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.QueueMode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "RequestPaymentMethodType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.RequestPaymentMethodType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SeatAssignmentMode");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAssignmentMode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SeatAvailability");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatAvailability.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SeatPreference");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SeatPreference.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SellBy");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellBy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SellFeeType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SellFeeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SSRCollectionsMode");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.SSRCollectionsMode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "TripType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplelistsf);
            cachedDeserFactories.add(simplelistdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "UnitHoldType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.UnitHoldType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "WeightCategory");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightCategory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "WeightType");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.WeightType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfFee");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Fee[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Fee");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Fee");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }
    private void addBindings4() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfOrderItemElement");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemElement");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemElement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfOrderItemLocation");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocation[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocation");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocation");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfOrderItemLocator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocator[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocator");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocator");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfOrderItemNote");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemNote[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemNote");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemNote");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfOrderItemParameter");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemParameter[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemParameter");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemParameter");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfOrderItemPersonalization");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemPersonalization[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemPersonalization");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemPersonalization");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfOrderItemSkuDetail");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemSkuDetail[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemSkuDetail");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemSkuDetail");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfOrderItemStatusHistory");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemStatusHistory[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemStatusHistory");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemStatusHistory");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfOtherServiceInformation");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OtherServiceInformation");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OtherServiceInformation");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfParticipant");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Participant[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Participant");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Participant");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfRecordLocator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfTCOrderItem");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCOrderItem");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCOrderItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfTCWarning");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.TCWarning[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCWarning");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCWarning");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ArrayOfTermsConditions");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditions");
            qName2 = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditions");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "ChargeableItem");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.ChargeableItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "DiscountTarget");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.DiscountTarget.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Fee");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Fee.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "FlightDesignator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.FlightDesignator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderCustomer");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderCustomer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderDiscount");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderDiscount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderHandling");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderHandling.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemAddress");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemElement");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocation");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemLocator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemLocator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemNote");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemNote.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemParameter");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemParameter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemPersonalization");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemPersonalization.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemSkuDetail");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemSkuDetail.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderItemStatusHistory");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderItemStatusHistory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OrderPayment");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OrderPayment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "OtherServiceInformation");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.OtherServiceInformation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "Participant");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.Participant.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "PointOfSale");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.PointOfSale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RecordLocator");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.RecordLocator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfdateTime");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfdateTime.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "RequestedFieldOfstring");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.RequestedFieldOfstring.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "StateMessage");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.StateMessage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCOrderItem");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.TCOrderItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.TCResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TCWarning");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.TCWarning.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/Common", "TermsConditions");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.Common.TermsConditions.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/DataContracts/TravelCommerce", "ServiceCancelRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.DataContracts.TravelCommerce.ServiceCancelRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AcceptScheduleChangesRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AcceptScheduleChangesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddBookingCommentsRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddBookingCommentsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddBookingCommentsResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddBookingCommentsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddInProcessPaymentToBookingRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddInProcessPaymentToBookingRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddInProcessPaymentToBookingResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddInProcessPaymentToBookingResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddPaymentToBookingRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AddPaymentToBookingResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ApplyPromotionRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ApplyPromotionRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ApplyPromotionResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ApplyPromotionResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AssignSeatsRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AssignSeatsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">AssignSeatsResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AssignSeatsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">BookingCommitRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.BookingCommitRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">BookingCommitResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.BookingCommitResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CalculateGuestValuesRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CalculateGuestValuesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CalculateGuestValuesResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CalculateGuestValuesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CancelRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CancelRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CancelResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CancelResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CaptureBaggageEventRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CaptureBaggageEventRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CaptureBaggageEventResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CaptureBaggageEventResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ChangeSourcePointOfSaleRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ChangeSourcePointOfSaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ChangeSourcePointOfSaleResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ChangeSourcePointOfSaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CommitRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">CommitResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DCCPaymentResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DCCQueryRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCQueryRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DCCQueryResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCQueryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DivideRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DivideRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DivideResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DivideResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DowngradeRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DowngradeRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">DowngradeResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DowngradeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">FareOverrideRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.FareOverrideRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">FareOverrideResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.FareOverrideResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetAvailabilityByTripResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetAvailabilityByTripResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingBaggageRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingBaggageRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingBaggageResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingBaggageResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingFromStateResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingFromStateResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingHistoryRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingHistoryRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingHistoryResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingHistoryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingPaymentsRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingPaymentsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingPaymentsResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingPaymentsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetBookingResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetEquipmentPropertiesRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetEquipmentPropertiesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetEquipmentPropertiesResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetEquipmentPropertiesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareAVSMessageRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAVSMessageRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareAVSMessageResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAVSMessageResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareTripAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareTripAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetLowFareTripAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareTripAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveFeePriceRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveFeePriceRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetMoveFeePriceResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveFeePriceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetPaymentFeePriceRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetPaymentFeePriceRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetPaymentFeePriceResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetPaymentFeePriceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings5() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetRecordLocatorListRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetRecordLocatorListRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetRecordLocatorListResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetRecordLocatorListResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSeatAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSeatAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSeatAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSeatAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSSRAvailabilityForBookingRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityForBookingRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSSRAvailabilityForBookingResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityForBookingResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSSRAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetSSRAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetUpgradeAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetUpgradeAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">GetUpgradeAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetUpgradeAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">OverrideFeeRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.OverrideFeeRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">OverrideFeeResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.OverrideFeeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">PriceItineraryRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.PriceItineraryRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">PriceItineraryResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.PriceItineraryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ResellSSRRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ResellSSRRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ResellSSRResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ResellSSRResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ScorePassengersRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ScorePassengersRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">ScorePassengersResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ScorePassengersResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">SellRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.SellRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">SellResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.SellResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceCancelServiceRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceCancelServiceRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceCancelServiceResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceCancelServiceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceSellRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceSellRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">TravelCommerceSellResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceSellResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UnassignSeatsRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UnassignSeatsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UnassignSeatsResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UnassignSeatsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateBookingComponentDataRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateBookingComponentDataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateBookingComponentDataResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateBookingComponentDataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateContactsRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateContactsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateContactsResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateContactsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateFeeStatusRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateFeeStatusRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePassengerResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePassengerResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePassengersRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePassengersRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePriceRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePriceRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdatePriceResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePriceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateSSRRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateSSRRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateSSRResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateSSRResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateTicketsRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateTicketsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpdateTicketsResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateTicketsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpgradeRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpgradeRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService", ">UpgradeResponse");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpgradeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.navitaire.com/WebServices", ">GetBookingFromStateRequest");
            cachedSerQNames.add(qName);
            cls = com.navitaire.schemas.WebServices.GetBookingFromStateRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public boolean updateFeeStatus(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateFeeStatusRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/UpdateFeeStatus");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateFeeStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Boolean) _resp).booleanValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Boolean) org.apache.axis.utils.JavaUtils.convert(_resp, boolean.class)).booleanValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateTicketsResponse updateTickets(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateTicketsRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/UpdateTickets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateTickets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateTicketsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateTicketsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateTicketsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpgradeResponse upgrade(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpgradeRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/Upgrade");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Upgrade"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpgradeResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpgradeResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpgradeResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DowngradeResponse downgrade(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DowngradeRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/Downgrade");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Downgrade"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DowngradeResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DowngradeResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DowngradeResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetUpgradeAvailabilityResponse getUpgradeAvailability(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetUpgradeAvailabilityRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetUpgradeAvailability");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetUpgradeAvailability"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetUpgradeAvailabilityResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetUpgradeAvailabilityResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetUpgradeAvailabilityResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void clear() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/Clear");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Clear"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ChangeSourcePointOfSaleResponse changeSourcePointOfSale(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ChangeSourcePointOfSaleRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/ChangeSourcePointOfSale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ChangeSourcePointOfSale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ChangeSourcePointOfSaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ChangeSourcePointOfSaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ChangeSourcePointOfSaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyBookingsResponseData moveJourneyBookings(com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyBookingsRequestData moveJourneyBookingsRequestData) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/MoveJourneyBookings");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "MoveJourneyBookings"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {moveJourneyBookingsRequestData});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyBookingsResponseData) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyBookingsResponseData) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyBookingsResponseData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.DataContracts.Booking.BookingUpdateResponseData moveJourney(com.navitaire.schemas.WebServices.DataContracts.Booking.MoveJourneyByKeyRequestData moveJourneyByKeyRequestData) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/MoveJourney");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "MoveJourney"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {moveJourneyByKeyRequestData});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.BookingUpdateResponseData) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.BookingUpdateResponseData) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.DataContracts.Booking.BookingUpdateResponseData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public boolean getPostCommitResults() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetPostCommitResults");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetPostCommitResults"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Boolean) _resp).booleanValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Boolean) org.apache.axis.utils.JavaUtils.convert(_resp, boolean.class)).booleanValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void sendItinerary(java.lang.String recordLocatorReqData) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/SendItinerary");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "SendItinerary"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {recordLocatorReqData});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetEquipmentPropertiesResponse getEquipmentProperties(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetEquipmentPropertiesRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetEquipmentProperties");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetEquipmentProperties"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetEquipmentPropertiesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetEquipmentPropertiesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetEquipmentPropertiesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.OverrideFeeResponse overrideFee(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.OverrideFeeRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/OverrideFee");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "OverrideFee"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.OverrideFeeResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.OverrideFeeResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.OverrideFeeResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePassengerResponse updatePassengers(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePassengersRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/UpdatePassengers");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdatePassengers"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePassengerResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePassengerResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePassengerResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateSSRResponse updateSSR(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateSSRRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/UpdateSSR");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateSSR"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateSSRResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateSSRResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateSSRResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePriceResponse updatePrice(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePriceRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/UpdatePrice");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdatePrice"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePriceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePriceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdatePriceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CaptureBaggageEventResponse captureBaggageEvent(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CaptureBaggageEventRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/CaptureBaggageEvent");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CaptureBaggageEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CaptureBaggageEventResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CaptureBaggageEventResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CaptureBaggageEventResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindBaggageEventResponseData findBaggageEvent(com.navitaire.schemas.WebServices.DataContracts.Booking.FindBaggageEventRequestData findBaggageEventRequestData) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/FindBaggageEvent");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "FindBaggageEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {findBaggageEventRequestData});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.FindBaggageEventResponseData) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.FindBaggageEventResponseData) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.DataContracts.Booking.FindBaggageEventResponseData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CalculateGuestValuesResponse calculateGuestValues(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CalculateGuestValuesRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/CalculateGuestValues");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CalculateGuestValues"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CalculateGuestValuesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CalculateGuestValuesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CalculateGuestValuesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ScorePassengersResponse scorePassengers(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ScorePassengersRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/ScorePassengers");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ScorePassengers"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ScorePassengersResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ScorePassengersResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ScorePassengersResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.BookingCommitResponse bookingCommit(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.BookingCommitRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/BookingCommit");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "BookingCommit"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.BookingCommitResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.BookingCommitResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.BookingCommitResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ResellSSRResponse resellSSR(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ResellSSRRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/ResellSSR");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ResellSSR"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ResellSSRResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ResellSSRResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ResellSSRResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateContactsResponse updateContacts(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateContactsRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/UpdateContacts");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateContacts"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateContactsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateContactsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateContactsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateBookingComponentDataResponse updateBookingComponentData(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateBookingComponentDataRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/UpdateBookingComponentData");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateBookingComponentData"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateBookingComponentDataResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateBookingComponentDataResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UpdateBookingComponentDataResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceSellResponse travelCommerceSell(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceSellRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/TravelCommerceSell");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "TravelCommerceSell"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceSellResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceSellResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceSellResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceCancelServiceResponse travelCommerceCancelService(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceCancelServiceRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/TravelCommerceCancelService");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "TravelCommerceCancelService"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceCancelServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceCancelServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.TravelCommerceCancelServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingStatelessFilteredResponseData getBookingStatelessFiltered(com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingStatelessFilteredRequestData getBookingStatelessFilteredRequestData) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetBookingStatelessFiltered");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetBookingStatelessFiltered"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getBookingStatelessFilteredRequestData});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingStatelessFilteredResponseData) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingStatelessFilteredResponseData) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingStatelessFilteredResponseData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFromStateFilteredResponseData getBookingFromStateFiltered(com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFromStateFilteredRequestData getBookingFromStateFilteredRequestData) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetBookingFromStateFiltered");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetBookingFromStateFiltered"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getBookingFromStateFilteredRequestData});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFromStateFilteredResponseData) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFromStateFilteredResponseData) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingFromStateFilteredResponseData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetAvailabilityByTripResponse getAvailability(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetAvailabilityRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetAvailability");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAvailability"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetAvailabilityByTripResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetAvailabilityByTripResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetAvailabilityByTripResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAvailabilityResponse getLowFareAvailability(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAvailabilityRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetLowFareAvailability");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetLowFareAvailability"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAvailabilityResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAvailabilityResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAvailabilityResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAVSMessageResponse getLowFareAVSMessage(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAVSMessageRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetLowFareAVSMessage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetLowFareAVSMessage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAVSMessageResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAVSMessageResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareAVSMessageResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareTripAvailabilityResponse getLowFareTripAvailability(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareTripAvailabilityRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetLowFareTripAvailability");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetLowFareTripAvailability"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareTripAvailabilityResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareTripAvailabilityResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetLowFareTripAvailabilityResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveAvailabilityResponse getMoveAvailability(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveAvailabilityRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetMoveAvailability");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetMoveAvailability"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveAvailabilityResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveAvailabilityResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveAvailabilityResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveFeePriceResponse getMoveFeePrice(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveFeePriceRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetMoveFeePrice");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetMoveFeePrice"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveFeePriceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveFeePriceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetMoveFeePriceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.DataContracts.Booking.SeparateSegmentByEquipmentResponseData separateSegmentByEquipment(com.navitaire.schemas.WebServices.DataContracts.Booking.Segment segmentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/SeparateSegmentByEquipment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "SeparateSegmentByEquipment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segmentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.SeparateSegmentByEquipmentResponseData) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.SeparateSegmentByEquipmentResponseData) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.DataContracts.Booking.SeparateSegmentByEquipmentResponseData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityResponse getSSRAvailability(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetSSRAvailability");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetSSRAvailability"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityForBookingResponse getSSRAvailabilityForBooking(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityForBookingRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[36]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetSSRAvailabilityForBooking");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetSSRAvailabilityForBooking"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityForBookingResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityForBookingResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSSRAvailabilityForBookingResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingResponseData findBooking(com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingRequestData findBookingRequestData) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[37]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/FindBooking");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "FindBooking"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {findBookingRequestData});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingResponseData) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingResponseData) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.DataContracts.Booking.FindBookingResponseData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingResponse getBooking(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[38]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetBooking");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetBooking"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingFromStateResponse getBookingFromState(com.navitaire.schemas.WebServices.GetBookingFromStateRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[39]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetBookingFromState");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetBookingFromState"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingFromStateResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingFromStateResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingFromStateResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingHistoryResponse getBookingHistory(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingHistoryRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[40]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetBookingHistory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetBookingHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingHistoryResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingHistoryResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingHistoryResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingBaggageResponse getBookingBaggage(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingBaggageRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[41]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetBookingBaggage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetBookingBaggage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingBaggageResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingBaggageResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingBaggageResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void acceptScheduleChanges(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AcceptScheduleChangesRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[42]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/AcceptScheduleChanges");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AcceptScheduleChanges"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddBookingCommentsResponse addBookingComments(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddBookingCommentsRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[43]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/AddBookingComments");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddBookingComments"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddBookingCommentsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddBookingCommentsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddBookingCommentsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetRecordLocatorListResponse getRecordLocatorList(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetRecordLocatorListRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[44]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetRecordLocatorList");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetRecordLocatorList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetRecordLocatorListResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetRecordLocatorListResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetRecordLocatorListResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CancelResponse cancel(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CancelRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[45]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/Cancel");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Cancel"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CancelResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CancelResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CancelResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DivideResponse divide(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DivideRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[46]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/Divide");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Divide"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DivideResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DivideResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DivideResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.FareOverrideResponse fareOverride(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.FareOverrideRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[47]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/FareOverride");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "FareOverride"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.FareOverrideResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.FareOverrideResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.FareOverrideResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingPaymentsResponse getBookingPayments(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingPaymentsRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[48]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetBookingPayments");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetBookingPayments"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingPaymentsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingPaymentsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingPaymentsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingResponse addPaymentToBooking(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[49]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/AddPaymentToBooking");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddPaymentToBooking"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddInProcessPaymentToBookingResponse addInProcessPaymentToBooking(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddInProcessPaymentToBookingRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[50]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/AddInProcessPaymentToBooking");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddInProcessPaymentToBooking"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddInProcessPaymentToBookingResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddInProcessPaymentToBookingResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddInProcessPaymentToBookingResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ApplyPromotionResponse applyPromotion(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ApplyPromotionRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[51]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/ApplyPromotion");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ApplyPromotion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ApplyPromotionResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ApplyPromotionResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.ApplyPromotionResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void cancelInProcessPayment() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[52]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/CancelInProcessPayment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "CancelInProcessPayment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCQueryResponse DCCQuery(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCQueryRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[53]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/DCCQuery");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DCCQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCQueryResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCQueryResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCQueryResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse acceptDCCOffer() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[54]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/AcceptDCCOffer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AcceptDCCOffer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse rejectDCCOffer() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[55]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/RejectDCCOffer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RejectDCCOffer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse DCCNotOffered() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[56]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/DCCNotOffered");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DCCNotOffered"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.DCCPaymentResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetPaymentFeePriceResponse getPaymentFeePrice(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetPaymentFeePriceRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[57]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetPaymentFeePrice");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetPaymentFeePrice"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetPaymentFeePriceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetPaymentFeePriceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetPaymentFeePriceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSeatAvailabilityResponse getSeatAvailability(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSeatAvailabilityRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[58]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetSeatAvailability");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetSeatAvailability"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSeatAvailabilityResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSeatAvailabilityResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetSeatAvailabilityResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AssignSeatsResponse assignSeats(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AssignSeatsRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[59]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/AssignSeats");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AssignSeats"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AssignSeatsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AssignSeatsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AssignSeatsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UnassignSeatsResponse unassignSeats(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UnassignSeatsRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[60]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/UnassignSeats");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UnassignSeats"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UnassignSeatsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UnassignSeatsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.UnassignSeatsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitResponse commit(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[61]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/Commit");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Commit"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.PriceItineraryResponse getItineraryPrice(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.PriceItineraryRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[62]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/GetItineraryPrice");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetItineraryPrice"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.PriceItineraryResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.PriceItineraryResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.PriceItineraryResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.navitaire.schemas.WebServices.ServiceContracts.BookingService.SellResponse sell(com.navitaire.schemas.WebServices.ServiceContracts.BookingService.SellRequest parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[63]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://schemas.navitaire.com/WebServices/IBookingManager/Sell");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Sell"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.SellResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.navitaire.schemas.WebServices.ServiceContracts.BookingService.SellResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.navitaire.schemas.WebServices.ServiceContracts.BookingService.SellResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
