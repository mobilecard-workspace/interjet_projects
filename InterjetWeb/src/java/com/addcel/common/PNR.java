package com.addcel.common;

import com.navitaire.schemas.WebServices.DataContracts.Booking.Booking;

public class PNR {
    private String pnr;
    private String id;
    private String monto;
    private String moneda;
    private String status;
    private String holdTime;
    private String holdRestante;
    private Booking booking;

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(String holdTime) {
        this.holdTime = holdTime;
    }

    public String getHoldRestante() {
        return holdRestante;
    }

    public void setHoldRestante(String holdRestante) {
        this.holdRestante = holdRestante;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}