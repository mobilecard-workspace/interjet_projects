package com.addcel.common;

import org.apache.log4j.Logger;

import com.addcel.interjet.TransactionControl;
import com.addcel.interjet.wsmanager.InterjetManager;
import com.addcel.tools.util.ConstantesInterjet;
import com.addcel.tools.util.Util;
import com.google.gson.Gson;
import com.navitaire.schemas.WebServices.DataContracts.Booking.Booking;

public class TransaccionManager {
    
    Logger log = Logger.getLogger(TransaccionManager.class);
    
    public String getInterjetTransaction(String user, String pnr, String monto) {
        log.info("getInterjetTransaction user: " + user + " pnr: " + pnr + " monto: " + monto);
        TransaccionDTO transaccion = new TransaccionDTO();
        String id = Util.generarLlave();
        
        transaccion.setIdN(id);
        transaccion.setUser(user);
        transaccion.setPnr(pnr);
        transaccion.setMonto(monto);
        
        if (TransactionControl.getInterjetTransaction(transaccion)) {
            log.info("Se guardo la informacion de forma correcta. id = " + id);
            return "{\"id\":\"" + id + "\"}";
        }
        else {
            log.info("No se guardo la informacion en getInterjetTransaction");
            return "";
        }
    }
    
    public String getTransactionByPNR(String transaccion) {
        return TransactionControl.getTransactionByPNR(transaccion);
    }
    
    public String getID(String user, String pnr, String monto, String mod) {
        TransaccionDTO transaccion = new TransaccionDTO();
        String id = Util.generarLlave();
        
        transaccion.setIdN(id);
        transaccion.setUser(user);
        transaccion.setPnr(pnr);
        transaccion.setMonto(monto);
        transaccion.setModulo(mod);
        
        if (TransactionControl.getID(transaccion))
            return "{\"id\":\"" + id + "\"}";
        else
            return "";
    }
    
    public String getRef(String id) {
        return TransactionControl.getRef(id);
    }
    
    public String setCommit(String transaccion, String pnr, String auth, String num) {
    	String json = "";
    	boolean error = false;
        if (ConstantesInterjet.AMBIENTE_ACTUAL.equals(ConstantesInterjet.AMBIENTE_PROD)) {
        	InterjetManager manager = new InterjetManager();
            Booking booking = manager.getBooking(pnr);        
            error = manager.setCommit(pnr, transaccion, auth, num, booking);
		} else if (ConstantesInterjet.AMBIENTE_ACTUAL.equals(ConstantesInterjet.AMBIENTE_QA)) {
			log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Entrando ambiente QA");
			error = true;
		}
        
        if (error) {
            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NO HAY ERRORES EN EL setCommit");
            json = "{true}";
        } else {
            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>HAY ERRORES EN EL setCommit");
            json = "{false}";
        }
        return json;
    }
    
    public String getPNR(String pnr) {
        try {
            log.info("Llega a getPNR" + pnr);
            log.info(">>>>>>>> Llego a getPRN de TransaccionManager con PNR " + pnr);
            Gson gson = new Gson();
            InterjetManager iManager = new InterjetManager();
            String json = gson.toJson(iManager.getPNR(pnr));
            log.info("getPNR json " + json);
            return json;
        } catch (Exception ex) {
            log.error("Error en getPNR" + ex);
            return null;
        }
    }
    
    public String getPNR2(String pnr) {
        try {
            log.info("Llega a getPNR2 " + pnr);
            log.info(">>>>>>>> Llego a getPRN2 de TransaccionManager con PNR " + pnr);
            Gson gson = new Gson();
            InterjetManager iManager = new InterjetManager();
            String json = gson.toJson(iManager.getPNR2(pnr));
            log.info("getPNR2 json " + json);
            return json;
        } catch (Exception ex) {
            log.error("Error en getPNR2" + ex);
            return null;
        }
    }
    
    public String getPNR3(String pnr) {
    	PNRMobile pnr3 = null;
    	InterjetManager iManager = null;
    	Gson gson = new Gson();
        String json = "";
        try {
            log.info("Llega a getPNR3 " + pnr);
            log.info(">>>>>>>> Llego a getPRN3 de TransaccionManager con PNR " + pnr);
            if (ConstantesInterjet.AMBIENTE_ACTUAL.equals(ConstantesInterjet.AMBIENTE_PROD)) {
            	iManager = new InterjetManager();
            	pnr3 = iManager.getPNR3(pnr);
			} else if (ConstantesInterjet.AMBIENTE_ACTUAL.equals(ConstantesInterjet.AMBIENTE_QA)) {
				pnr3 = new PNRMobile();
				pnr3.setMonto(String.valueOf((int)(Math.random()*(2000-1500+1)+1500)));
				pnr3.setStatus("1");
				pnr3.setPnr(pnr);
			}
            
            
            if (pnr3 == null) {
                pnr3 = new PNRMobile();
                pnr3.setMonto("");
                pnr3.setPnr("");
                pnr3.setStatus("");
            }
            
            json = gson.toJson(pnr3);
            log.info("getPNR3 json " + json);
            return json;
        } catch (Exception ex) {
            log.error("Error en getPNR3" + ex);
            return null;
        }
    }
}