/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.common;

import com.addcel.tools.util.Crypto;

/**
 *
 * @author ADCL1
 */
public class MCUser {
    
    private String userId;
    private String userLogin;
    private String userPwd;
    private String userNombre;
    private String userApellido;
    private String userEstatus;
    private String userTdc;
    private String userVig;
    private String userTipoTarjeta;

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the userLogin
     */
    public String getUserLogin() {
        return userLogin;
    }

    /**
     * @param userLogin the userLogin to set
     */
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    /**
     * @return the userNombre
     */
    public String getUserNombre() {
        return userNombre;
    }

    /**
     * @param userNombre the userNombre to set
     */
    public void setUserNombre(String userNombre) {
        this.userNombre = userNombre;
    }

    /**
     * @return the userApellido
     */
    public String getUserApellido() {
        return userApellido;
    }

    /**
     * @param userApellido the userApellido to set
     */
    public void setUserApellido(String userApellido) {
        this.userApellido = userApellido;
    }

    /**
     * @return the userEstatus
     */
    public String getUserEstatus() {
        return userEstatus;
    }

    /**
     * @param userEstatus the userEstatus to set
     */
    public void setUserEstatus(String userEstatus) {
        this.userEstatus = userEstatus;
    }

    /**
     * @return the userTdc
     */
    public String getUserTdc() {
        return userTdc;
    }

    /**
     * @param userTdc the userTdc to set
     */
    public void setUserTdc(String userTdc) {        
        this.userTdc = Crypto.aesDecrypt(userTdc,"5525963513");
    }

    /**
     * @return the userVig
     */
    public String getUserVig() {
        return userVig;
    }

    /**
     * @param userVig the userVig to set
     */
    public void setUserVig(String userVig) {
        this.userVig = userVig;
    }

    /**
     * @return the userTipoTarjeta
     */
    public String getUserTipoTarjeta() {
        return userTipoTarjeta;
    }

    /**
     * @param userTipoTarjeta the userTipoTarjeta to set
     */
    public void setUserTipoTarjeta(String userTipoTarjeta) {
        this.userTipoTarjeta = userTipoTarjeta;
    }
    
    
    
    
    
}
