/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.common;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author Salvador
 */
public class HttpRequest {
    
    public String responseString;
    
    public String httpResponse(String targetURL, String urlParameters)
    {
        URL url;
        HttpURLConnection connection = null;  
        String httpResponse ="";
        
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", 
            "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + 
            Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");  

            connection.setUseCaches (false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                connection.getOutputStream ());
            wr.writeBytes (urlParameters);
            wr.flush ();
            wr.close ();

            //Get Response	
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer(); 
            while((line = rd.readLine()) != null) {
                //response.append(line);
                //response.append('\r');
                httpResponse += line;
            }
            rd.close();
            //return response.toString();
            return httpResponse;

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        } finally {

            if(connection != null) {
                connection.disconnect(); 
            }
        }
        
    }
    
}
