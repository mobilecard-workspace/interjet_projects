package com.addcel.interjet.wsmanager;

import java.rmi.RemoteException;

import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import com.addcel.common.PNR;
import com.addcel.common.PNRMobile;
import com.addcel.tools.util.ConstantesInterjet;
import com.navitaire.schemas.WebServices.BasicHttpBinding_IBookingManagerStub;
import com.navitaire.schemas.WebServices.SessionManager;
import com.navitaire.schemas.WebServices.SessionManagerLocator;
import com.navitaire.schemas.WebServices.DataContracts.Booking.AddPaymentToBookingRequestData;
import com.navitaire.schemas.WebServices.DataContracts.Booking.Booking;
import com.navitaire.schemas.WebServices.DataContracts.Booking.BookingSum;
import com.navitaire.schemas.WebServices.DataContracts.Booking.CommitRequestData;
import com.navitaire.schemas.WebServices.DataContracts.Booking.GetBookingRequestData;
import com.navitaire.schemas.WebServices.DataContracts.Booking.GetByRecordLocator;
import com.navitaire.schemas.WebServices.DataContracts.Booking.PaymentField;
import com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.GetBookingBy;
import com.navitaire.schemas.WebServices.DataContracts.Common.Enumerations.RequestPaymentMethodType;
import com.navitaire.schemas.WebServices.DataContracts.Session.LogonRequestData;
import com.navitaire.schemas.WebServices.ServiceContracts.BookingService.AddPaymentToBookingRequest;
import com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitRequest;
import com.navitaire.schemas.WebServices.ServiceContracts.BookingService.CommitResponse;
import com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingRequest;
import com.navitaire.schemas.WebServices.ServiceContracts.BookingService.GetBookingResponse;
import com.navitaire.schemas.WebServices.ServiceContracts.SessionService.LogonRequest;

public class InterjetManager {
    
    Logger log = Logger.getLogger(InterjetManager.class);
    
    private GetBookingRequest gbr;
    private GetBookingRequestData gbrd;
    private BasicHttpBinding_IBookingManagerStub bmc;
    private SessionManager sessionWS;
    private LogonRequest logonRequest;
    
    private String signature;
    
    private String agentName = "webapi@addcel.com";
    private String domainCode = "WWW";
    private String locationCode = "web";
    private String password = "addcel-01";
    private String roleCode = "ROB";
    
    public InterjetManager() {
        log.info(">>>>>>Entro al constructor de IntejetManager");
        try {
            gbr = new GetBookingRequest();
            gbrd = new GetBookingRequestData(); 
           
            sessionWS = new SessionManagerLocator();
            
            logonRequest = new LogonRequest();
            LogonRequestData log = new LogonRequestData();
            
            log.setAgentName(agentName);
            log.setDomainCode(domainCode);
            log.setLocationCode(locationCode);
            log.setPassword(password);
            log.setRoleCode(roleCode);
            
            logonRequest.setLogonRequestData(log);
            
            signature = sessionWS.getBasicHttpBinding_ISessionManager().logon(logonRequest).getSignature();        
            
            bmc = new BasicHttpBinding_IBookingManagerStub();
            bmc._setProperty(BasicHttpBinding_IBookingManagerStub.ENDPOINT_ADDRESS_PROPERTY, "https://ijr3xapi.navitaire.com/BookingManager.svc");
            bmc.setHeader("http://schemas.navitaire.com/WebServices", "Signature", signature);
        } catch (AxisFault ex) {
            log.error("Error al inicializar el servicio ", ex);
        } catch (RemoteException ex) {
            log.error("Error al inicializar el servicio ", ex);
        } catch (Exception ex) {
            log.error("Error al inicializar el servicio ", ex);
        }
    }
    
    public PNRMobile getPNR3(String pnr) {
        log.info(">>>>>>Llego a la peticion del servicio getPNR3");
        Booking book = null;
        PNRMobile pnrMobile = null;
        try {
            if (pnr != null && pnr.trim().length() > 0) {
            	pnrMobile = new PNRMobile();
                pnrMobile.setPnr(pnr);
                
            	if (ConstantesInterjet.AMBIENTE_ACTUAL.equals(ConstantesInterjet.AMBIENTE_PROD)) {
            		book = getBooking(pnr);
            		pnrMobile.setMonto(book.getBookingSum().getBalanceDue().toString());
                    pnrMobile.setStatus(book.getBookingInfo().getBookingStatus().toString());
    			} else if (ConstantesInterjet.AMBIENTE_ACTUAL.equals(ConstantesInterjet.AMBIENTE_QA)) {
    				pnrMobile.setMonto(String.valueOf((int)(Math.random()*(200-50+1)+50)));
                    pnrMobile.setStatus("1");
    			}
                return pnrMobile;
            } else{
            	return null;
            }
        } catch (Exception ex) {
            log.error("Error en getPNR3 ", ex);
            return null;
        }
    }
    
    public PNRMobile getPNR2(String pnr) {
        log.info(">>>>>>Llego a la peticion del servicio getPNR2");
        try {
            if (pnr != null && pnr.trim().length() > 0) {
                Booking book = getBooking(pnr);
            
                PNRMobile pnrMobile = new PNRMobile();
            
                pnrMobile.setPnr(pnr);
                pnrMobile.setMonto(book.getBookingSum().getBalanceDue().toString());
                pnrMobile.setStatus(book.getBookingInfo().getBookingStatus().toString());
            
                return pnrMobile;
        } else
            return null;
        } catch (Exception ex) {
            log.error("Error en getPNR2 ", ex);
            return null;
        }
    }
    
    public PNR getPNR(String pnr) {
        log.info(">>>>>>Llego a la peticion del servicio getPNR2");
        try {
            if (pnr != null && pnr.trim().length() > 0) {
                Booking book = getBooking(pnr);
                
                PNR myPNR = new PNR();
                myPNR.setPnr(pnr);
                myPNR.setId(book.getBookingID().toString());
                myPNR.setMonto(book.getBookingSum().getBalanceDue().toString());
                myPNR.setMoneda(book.getCurrencyCode());
                myPNR.setStatus(book.getBookingInfo().getBookingStatus().toString());
                myPNR.setHoldTime(Util.fechaFormateada(book.getBookingHold().getHoldDateTime()));
                myPNR.setHoldRestante(Util.holdRestante(book.getBookingHold().getHoldDateTime()));
            
                return myPNR;
        } else
            return null;
        } catch (Exception ex) {
            log.error("Error en getPNR ", ex);
            return null;
        }
    }

    public boolean setCommit(String pnr, String transaccion, String auth, String num, Booking book) {
        try {
            PaymentField field = new PaymentField();
            PaymentField[] fields = new PaymentField[1];
            
            AddPaymentToBookingRequestData paymentRequestData = new AddPaymentToBookingRequestData();
            paymentRequestData.setPaymentMethodCode("AC");
            paymentRequestData.setPaymentMethodType(RequestPaymentMethodType.PrePaid);
            
            paymentRequestData.setQuotedAmount(book.getBookingSum().getBalanceDue());
            paymentRequestData.setQuotedCurrencyCode(book.getCurrencyCode());
            
            paymentRequestData.setPaymentText(num + "|" + auth);
            
            paymentRequestData.setExpiration(book.getBookingInfo().getExpiredDate());
            
            paymentRequestData.setAccountNumber(num);
            
            field.setFieldName("AMT");
            field.setFieldValue(book.getBookingSum().getBalanceDue().toString());
            fields[0] = field;
            
            paymentRequestData.setPaymentFields(fields);
            
            CommitRequest cr = new CommitRequest();
            CommitRequestData crd = new CommitRequestData();
            crd.setBooking(book);
            crd.setDistributeToContacts(Boolean.TRUE);
            crd.setChangeHoldDateTime(Boolean.FALSE);
            cr.setBookingRequest(crd);
            
            bmc.addPaymentToBooking(new AddPaymentToBookingRequest(paymentRequestData));
            
            CommitResponse response = bmc.commit(cr);
            log.info(">>>>>>>>>>>booooking " + com.addcel.tools.util.Util.toString(book));
            log.info(">>>>status booking = " + book.getBookingInfo().getBookingStatus().getValue());
            log.info(">>>>status booking = " + book.getBookingInfo().getBookingStatus().getValue());
            log.info(">>>>>>>>>>>setCommit succes " + com.addcel.tools.util.Util.toString(response.getBookingUpdateResponseData().getSuccess()));
            log.info(">>>>>>>>>>>setCommit error " + com.addcel.tools.util.Util.toString(response.getBookingUpdateResponseData().getError()));
            log.info(">>>>>>>>>>>setCommit response " + com.addcel.tools.util.Util.toString(response));
            return response.getBookingUpdateResponseData().getError() == null ;
        } catch (RemoteException ex) {
            log.error("Error en setCommit ", ex);
            return false;
        }
    }
    
    public Booking getBooking(String pnr) {
        Booking book = null;
        log.info(">>>>>>Recuperando booking");
        try {
            gbrd.setGetBookingBy(GetBookingBy.RecordLocator);
            GetByRecordLocator getby = new GetByRecordLocator();
            getby.setRecordLocator(pnr);
            gbrd.setGetByRecordLocator(getby);
            gbr.setGetBookingReqData(gbrd);
            GetBookingResponse gbresp = bmc.getBooking(gbr);
            book = gbresp.getBooking();
        } catch (RemoteException ex) {
            log.error("Error en getBooking ", ex);
        }
        
        return book;
    }
}