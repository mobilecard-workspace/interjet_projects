
package com.addcel.interjet.wsmanager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Util {
    	public static String fechaFormateada(Calendar cal) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return sdf.format(cal.getTime());
	}
	
	public static String holdRestante(Calendar cal) {
		Date holdTimeDate = null;
		Date actual = null;
		
		holdTimeDate = cal.getTime();
		actual = Calendar.getInstance().getTime();
		
		long dif = holdTimeDate.getTime() - actual.getTime();
		long difSec = dif / 1000;
		return String.valueOf(difSec);
	}
}
