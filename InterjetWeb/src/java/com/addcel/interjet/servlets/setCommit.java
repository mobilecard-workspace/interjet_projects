package com.addcel.interjet.servlets;

import com.addcel.common.TransaccionManager;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

public class setCommit extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        Logger log = Logger.getLogger(setCommit.class);
        try {
            String transaccion = request.getParameter("transaccion");
            String pnr = request.getParameter("pnr");
            String auth = request.getParameter("auth");
            String num = request.getParameter("num");
            
            log.info("Entrando al servlet setCommit con los valores transaccion = " + transaccion + " pnr " + pnr +
                    " auth " + auth + " num " + num);
            
            if (transaccion != null && transaccion.trim().length() > 0 
                    && pnr != null && pnr.trim().length() > 0
                    && auth != null && auth.trim().length() > 0 
                    && num != null && num.trim().length() > 0) {
                
                out.print(new TransaccionManager().setCommit(transaccion, pnr, auth, num));
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
