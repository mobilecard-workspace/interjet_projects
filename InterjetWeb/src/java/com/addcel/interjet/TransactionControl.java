/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.interjet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.addcel.common.BitacoraDTO;
import com.addcel.common.MCUser;
import com.addcel.common.TransaccionDTO;
import com.addcel.tools.util.DatabaseImpl;
import com.addcel.tools.util.DateUtil;

/**
 *
 * @author ADDCEL13
 */
public class TransactionControl {
    private static final Logger LOG = Logger.getLogger(TransactionControl.class.getName());
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TransactionControl.class);
    
    static int transactionId=0;
    
//    public static void updateTransaction(int transactionId,  ProsaResponse response)
//    {
//        TransactionControl.updateTransaction(transactionId,  response,"");
//    }
    
    
    public static boolean login(String userId, String password)
    {
        DatabaseImpl ds = new DatabaseImpl("mobilecard");
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        String usuario=null;
        boolean ret = false;
        
        if(userId.equals("sburgara") && password.equals("burgaras"))
            return true;
        else
            return false;
        
            /*
        try
        {
            //Encriptando Password
            System.out.println("INTENTANDO: "+userId + "<-> "+ password);
            password = Crypto.aesEncrypt(password);
            System.out.println("RESULTA: " + password);
            
            conn = ds.getConnection();
            st = conn.createStatement();


            //Obteniendo ultimo id
            query = "select id_usuario "
                    + "from t_usuarios "
                    + "where usr_login='"+userId+"' and usr_pwd='"+password+"'";
            rs = st.executeQuery(query);
            System.out.println(query);
            if(rs.next())
            {
                usuario = rs.getString(1);                
            }
            if(usuario==null || usuario.equals(""))
                ret = false;
            else
                ret = true;
                
        }
        catch(Exception e)
        {
            System.out.println("DB: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                
                //Statement
                if(st!=null && st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("DB: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("DB: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        return ret;
        */
    }
    
    public static MCUser getData(String userId)
    {
        DatabaseImpl ds = new DatabaseImpl("mobilecard");
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        MCUser mobileCardUser = null;
        
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();


            //Obteniendo ultimo id
            query = "select id_usuario,usr_login,usr_nombre, usr_apellido, usr_materno, usr_tdc_numero, usr_tdc_vigencia, desc_tipo_tarjeta "
                    + "from t_usuarios left join t_tipo_tarjeta on t_usuarios.id_tipo_tarjeta=t_tipo_tarjeta.id_tipo_tarjeta "
                    + "where id_usr_status = 1 and usr_login='"+userId+"'";
            rs = st.executeQuery(query);
            System.out.println(query);
            if(rs.next())
            {
                mobileCardUser =  new MCUser();
                mobileCardUser.setUserId(rs.getString(1));
                mobileCardUser.setUserLogin(rs.getString(2));
                mobileCardUser.setUserNombre(rs.getString(3));
                mobileCardUser.setUserApellido(rs.getString(4));
                //mobileCardUser.setUserId(rs.getString(5)); Apellido materno
                mobileCardUser.setUserTdc(rs.getString(6));
                mobileCardUser.setUserVig(rs.getString(7));
                mobileCardUser.setUserTipoTarjeta(rs.getString(8));
                
            }
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                
                //Statement
                if(st!=null && st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        return mobileCardUser;
    
    }
    
     public static long insertaBitacora(BitacoraDTO bitacora){
        DatabaseImpl di;
        Connection conexion = null;
        long idBitacora=0;
        try {
            di = new DatabaseImpl("mobilecard");
            conexion = di.getConnection();           
            String query="INSERT INTO t_bitacora "
                    + "(id_usuario,id_proveedor,id_producto,bit_fecha,bit_hora,bit_concepto,bit_cargo,bit_status,tarjeta_compra) "
                    + "values(?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = conexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, bitacora.getIdUsuario());ps.setInt(2, bitacora.getIdProveedor());
            ps.setString(3, bitacora.getIdProducto());ps.setDate(4,new java.sql.Date(bitacora.getBitFecha().getTime()));
            ps.setTimestamp(5,new java.sql.Timestamp( bitacora.getBitHora().getTime()));ps.setString(6, bitacora.getBitConcepto());
            ps.setString(7, bitacora.getBitCargo());ps.setString(8, bitacora.getBitStatus());
            ps.setString(9, bitacora.getTarjetaCompra());
            int executeUpdate = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if(rs!=null && rs.next()){
               idBitacora =rs.getLong(1);               
               rs.close();
            }            
            ps.close();           
            conexion.close();
         
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return idBitacora;
    }
    
    public static int actualizaBitacora(BitacoraDTO bitacora){
        DatabaseImpl di;
        Connection conexion;
        int update=0;
        try {
            di = new DatabaseImpl("mobilecard");
            conexion = di.getConnection();
            String query="UPDATE t_bitacora SET "
                    + "bit_fecha=?,bit_hora=?,bit_concepto=?,bit_status=?, bit_no_autorizacion=? WHERE id_bitacora=?";
            PreparedStatement ps = conexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);  
            
            ps.setDate(1,new java.sql.Date(bitacora.getBitFecha().getTime()));ps.setTimestamp(2,new java.sql.Timestamp( bitacora.getBitHora().getTime()));
            ps.setString(3, bitacora.getBitConcepto());ps.setString(4,bitacora.getBitStatus());            
            ps.setString(5, bitacora.getBitNoAutorizacion());ps.setString(6, bitacora.getIdBitacora());
            update = ps.executeUpdate();
            ps.close();
            conexion.close();   
        
            LOG.log(Level.INFO,"===> idBitacora", bitacora.getIdBitacora());
        
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return update;
    }     
    
    
    /************************************************************
     *                  INICIO DE CAMBIOS
     * Metodos para sustituir las llamadas a los servidios de .net
     *************************************************************/
    public static boolean getInterjetTransaction(TransaccionDTO transaccion) {
        boolean guardo = false;
        String sql = "INSERT INTO "
                    + "ijet_transaccion "
                    + "("
                        + "transaccionIdn, "
                        + "transaccionUser, "
                        + "transaccionPnr, "
                        + "transaccionFch, "
                        + "transaccionMonto"
                    + ") "
                    + "VALUES (?, ?, ?, ?, ?)";
        try {
            DatabaseImpl dbImpl = new DatabaseImpl("mobilecard");
            Connection cnn = null;
            PreparedStatement ps = null;
            try {
                cnn = dbImpl.getConnection();
                ps = cnn.prepareStatement(sql);
                
                ps.setString(1, transaccion.getIdN());
                ps.setString(2, transaccion.getUser());
                ps.setString(3, transaccion.getPnr());
                ps.setString(4, DateUtil.getDate());
                ps.setString(5, transaccion.getMonto());
                
                ps.execute();
                guardo = true;
            } catch (SQLException ex) {
                log.error("Error al guardar en getInterjetTransaction " + ex);
            } catch (ClassNotFoundException ex) {
                log.error("Error al guardar en getInterjetTransaction " + ex);
            } finally {
                if (cnn != null)
                    cnn.close();
                if (ps != null) 
                    ps.close();
            }
        } catch (Exception ex) {
            log.error("Error al guardar en getInterjetTransaction " + ex);
        }
        
        return guardo;
    }

    public static String getTransactionByPNR(String transaccion) {
        String sql = "SELECT transaccionPnr FROM ijet_transaccion WHERE transaccionIdn = ?";
        String pnr = "";
        try {
            DatabaseImpl dbImpl = new DatabaseImpl("mobilecard");
            Connection cnn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                cnn = dbImpl.getConnection();
                ps = cnn.prepareStatement(sql);
                ps.setString(1, transaccion);
                rs = ps.executeQuery();
                
                if (rs.next())
                    pnr = rs.getString("transaccionPnr");
                
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                LOG.log(Level.SEVERE, null, ex);
            } finally {
                if (cnn != null)
                    cnn.close();
                if (ps != null) 
                    ps.close();
                if (rs != null)
                    rs.close();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        
        return pnr;
    }
    
    public static boolean getID(TransaccionDTO transaccion) {
        boolean guardo = false;
        String sql = "INSERT INTO "
                    + "ijet_transaccion "
                    + "("
                        + "transaccionIdn, "
                        + "transaccionUser, "
                        + "transaccionPnr, "
                        + "transaccionFch, "
                        + "transaccionMonto, "
                        + "transaccionModulo"
                    + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?)";
        try {
            DatabaseImpl dbImpl = new DatabaseImpl("mobilecard");
            Connection cnn = null;
            PreparedStatement ps = null;
            try {
                cnn = dbImpl.getConnection();
                ps = cnn.prepareStatement(sql);
                
                ps.setString(1, transaccion.getIdN());
                ps.setString(2, transaccion.getUser());
                ps.setString(3, transaccion.getPnr());
                ps.setString(4, DateUtil.getDate());
                ps.setString(5, transaccion.getMonto());
                ps.setString(6, transaccion.getModulo());
                
                ps.execute();
                guardo = true;
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                LOG.log(Level.SEVERE, null, ex);
            } finally {
                if (cnn != null)
                    cnn.close();
                if (ps != null) 
                    ps.close();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        
        return guardo;
    }
    
    public static String getRef(String id) {
        String sql = "SELECT transaccionPnr FROM ijet_transaccion WHERE transaccionIdn = ?";
        String pnr = "";
        try {
            DatabaseImpl dbImpl = new DatabaseImpl("mobilecard");
            Connection cnn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                cnn = dbImpl.getConnection();
                ps = cnn.prepareStatement(sql);
                ps.setString(1, id);
                rs = ps.executeQuery();
                
                if (rs.next())
                    pnr = rs.getString("transaccionPnr");
                
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                LOG.log(Level.SEVERE, null, ex);
            } finally {
                if (cnn != null)
                    cnn.close();
                if (ps != null) 
                    ps.close();
                if (rs != null)
                    rs.close();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        
        return pnr;
    }
    /**********************************
     *          FIN DE CAMBIOS
     *********************************/
}
