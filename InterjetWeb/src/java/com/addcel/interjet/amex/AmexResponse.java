/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.interjet.amex;

/**
 *
 * @author ADDCEL13
 */
public class AmexResponse {
    private String transaction;
    private String code;
    private String dsc;
    private String error;
    private String errorDsc;
    private String approval;

    /**
     * @return the transaction
     */
    public String getTransaction() {
        return transaction;
    }

    /**
     * @param transaction the transaction to set
     */
    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the dsc
     */
    public String getDsc() {
        return dsc;
    }

    /**
     * @param dsc the dsc to set
     */
    public void setDsc(String dsc) {
        this.dsc = dsc;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return the errorDsc
     */
    public String getErrorDsc() {
        return errorDsc;
    }

    /**
     * @param errorDsc the errorDsc to set
     */
    public void setErrorDsc(String errorDsc) {
        this.errorDsc = errorDsc;
    }

    /**
     * @return the approval
     */
    public String getApproval() {
        return approval;
    }

    /**
     * @param approval the approval to set
     */
    public void setApproval(String approval) {
        this.approval = approval;
    }
    
}
